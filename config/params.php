<?php
$rbac = require __DIR__ . '/rbac.php';

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'adminSessionTimeoutSeconds'=>86400,
    'magento_root_path'=>"/opt/web/magento4",
    'rbac_permissions'=>$rbac,
    'authcode' =>
        [
            'secret_key' => ')(*&)(JOIJOIYO^(*&YUFGUFUTRE^FGVL908jk+{}{?:<:<K*Y&^*YU&(UOIMJ',
            '_start_slat' => '(*UPjq29e0iokLKJ:J%^_)(IOK_+{POIXU@P(*U)(RCIT{PV<KP(#*)(*%(&%(*&@(*&',
            '_end_slat' => ')(_)*)(IPOJIU^(*&^*&T&))___L{P&^^%$%$%GFVGKJ:LOIoi345poi09f781}\\"?><%&O:::P',
        ],
    'frame_html_root_path'=>'/data/magento446/pub',
    'ES_HOST'=>[
        [
            'host' => 'vpc-pgweb-odphvhsw2gp5e2vufzd22mp6ni.us-east-1.es.amazonaws.com',
            'port' => '443',
            'scheme' => 'https',
            'user' => 'pgesuser',
            'pass' => 'FF839Lpws!drfctgazs'
        ]
    ]
];
