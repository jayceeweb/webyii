<?php
return [
    [
        'name'=>'createPost',
        'description'=>'Create a post'
    ],
    [
        'name'=>'updatePost',
        'description'=>'Update post',
    ],
    [
        'name'=>'updateOwnPost',
        'description'=>'Update own post',
        'rule_class'=>'app\modules\manager\rbac\AuthorRule',
        'children'=>[
            [
                'type'=>'permission',
                'name'=>'updatePost',
            ]
        ]
    ],
    [
        'name'=>'blacklistManager',
        'description'=>'黑名单管理',
    ],
    [
        'name'=>'couponManager',
        'description'=>'coupon查询',
    ],
    [
        'name'=>'scoreManager',
        'description'=>'score管理',
    ],
    [
        'name'=>'urlManager',
        'description'=>'url管理',
    ],
];