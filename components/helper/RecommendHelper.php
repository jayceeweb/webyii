<?php
namespace app\components\helper;

use Yii;
use app\components\helper\DbHelper;
use app\models\CoreConfigData;

class RecommendHelper {
    private $recommend_exclude_skus=[];

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    public function get_customer_id_by_email($email) {
        $sql=sprintf("select entity_id from customer_entity where email='%s'",$email);
        $customer_id=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
        return $customer_id;
    }

    public function get_all_product_urls() {
        $sql="SELECT entity_id,request_path FROM url_rewrite WHERE entity_type='product' AND sitemap=1";
        $urls=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $ret=[];
        foreach ($urls as $url) {
            $ret[$url["entity_id"]]=$url;
        }
        return $ret;
    }

    public function get_customer_recommend_products($customer_id){
        $t1=\app\components\helper\TimezoneHelper::msectime();

        $server_url=CoreConfigData::getBaseUrl();
        $static_url=CoreConfigData::getStaticUrl();
        $product_urls=$this->get_all_product_urls();
        $recommend_list=[];
        $recommend_exclude_sku_sql="";
        $i = 0;
        foreach ($this->recommend_exclude_skus as $recommend_exclude_sku) {
            $i = $i + 1;
            if ($i == 1) {
                $recommend_exclude_sku_sql = sprintf("product.sku!='%s'", $recommend_exclude_sku);
            } else {
                $recommend_exclude_sku_sql = $recommend_exclude_sku_sql.sprintf(" AND product.sku!='%s'",$recommend_exclude_sku);
            }
        }
        if ($recommend_exclude_sku_sql!="") {
            $recommend_exclude_sku_sql = sprintf("and (%s)",$recommend_exclude_sku_sql);
        }
        //读取购物车、wishlist中有用的信息，用于筛选
        if (!empty($customer_id)) {
            //购物车数据
            $sql = "SELECT link.parent_id AS configurable_id,quote_item.product_id AS simple_id,quote_item.`name`,product.sku,stock.qty,stock.is_in_stock,product.price as product_price,product.image
FROM quote_item 
INNER JOIN catalog_product_flat_1 product ON quote_item.product_id=product.entity_id
INNER JOIN `quote` q ON quote_item.`quote_id`=q.entity_id
INNER JOIN cataloginventory_stock_item stock ON stock.product_id=product.entity_id
INNER JOIN `catalog_product_super_link` link ON link.product_id=product.entity_id
WHERE q.customer_id=%s
AND product.attribute_set_id=13
AND q.`is_active`=1
AND stock.website_id=0
AND product.type_id='simple'
%s
AND quote_item.`created_at`>DATE_SUB(NOW(), INTERVAL 180 DAY)
AND stock.`is_in_stock`=1 AND stock.`qty`>=30
ORDER BY quote_item.`created_at` DESC,stock.qty DESC";
            $sql=sprintf($sql,$customer_id,$recommend_exclude_sku_sql);
            $quote_item_list=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();

            $t2=\app\components\helper\TimezoneHelper::msectime();
//            echo sprintf("RecommendHelper.get_customer_recommend_products.t2-t1=%s".PHP_EOL,$t2-$t1);

            //wishlist数据
            $sql = "SELECT link.parent_id AS configurable_id,item.product_id AS simple_id,p1.`value` AS `name`,product.sku,stock.qty,stock.is_in_stock,product.price as product_price,product.image
FROM wishlist_item item
INNER JOIN wishlist main ON main.`wishlist_id`=item.`wishlist_id`
INNER JOIN `profile` p ON p.`profile_id`=main.`profile_id`
INNER JOIN catalog_product_flat_1 product ON item.product_id=product.entity_id
INNER JOIN catalog_product_entity_varchar p1 ON p1.entity_id=item.product_id
INNER JOIN cataloginventory_stock_item stock ON stock.product_id=item.product_id
INNER JOIN `catalog_product_super_link` link ON link.product_id=item.product_id
WHERE p.`customer_id`=%s
AND product.attribute_set_id=13
AND stock.website_id=0
AND product.`type_id`='simple'
AND p1.`attribute_id`=(SELECT attribute_id FROM eav_attribute WHERE attribute_code='name' AND entity_type_id=4)
%s
AND item.`added_at`>DATE_SUB(NOW(), INTERVAL 180 DAY)
AND stock.`is_in_stock`=1 AND stock.`qty`>=30
ORDER BY item.`added_at` DESC,stock.qty DESC";
            $sql=sprintf($sql,$customer_id,$recommend_exclude_sku_sql);
            $wishlist_item_list=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();

            $t3=\app\components\helper\TimezoneHelper::msectime();
//            echo sprintf("RecommendHelper.get_customer_recommend_products.t3-t2=%s".PHP_EOL,$t3-$t2);

            //share and save数据
            $sql = "SELECT link.parent_id AS configurable_id,item.product_id AS simple_id,p1.`value` AS `name`,product.sku,stock.qty,stock.is_in_stock,product.price as product_price,product.image
FROM share_discount item
INNER JOIN catalog_product_flat_1 product ON product.entity_id=item.product_id
INNER JOIN catalog_product_entity_varchar p1 ON p1.entity_id=item.product_id
INNER JOIN cataloginventory_stock_item stock ON stock.product_id=item.product_id
INNER JOIN `catalog_product_super_link` link ON link.product_id=item.product_id
WHERE item.`customer_id`=%s
AND product.attribute_set_id=13
AND stock.website_id=0
AND product.`type_id`='simple'
AND p1.`attribute_id`=(SELECT attribute_id FROM eav_attribute WHERE attribute_code='name' AND entity_type_id=4)
%s
AND item.`from_time`>UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 180 DAY))
AND stock.`is_in_stock`=1 AND stock.`qty`>=30
ORDER BY item.`from_time` DESC,stock.qty DESC";
            $sql=sprintf($sql,$customer_id,$recommend_exclude_sku_sql);
            $share_list=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $t4=\app\components\helper\TimezoneHelper::msectime();
//            echo sprintf("RecommendHelper.get_customer_recommend_products.t4-t3=%s".PHP_EOL,$t4-$t3);

            # 推荐结果变量
            $quote_item_list_recomment_count = 0;
            $wishlist_item_list_count = 0;
            $share_list_recomment_count = 0;
            $recommend_list_keys = [];
            for($i=0;$i<4;$i++) {
                if (count($quote_item_list) > $i && count($recommend_list) < 4 && $quote_item_list_recomment_count < 2) {
                    if (!in_array($quote_item_list[$i]["simple_id"],$recommend_list_keys)) {
                        $recommend_list[]=$quote_item_list[$i];
                        $quote_item_list_recomment_count=$quote_item_list_recomment_count+1;
                        $recommend_list_keys[$quote_item_list[$i]["simple_id"]] = 1;
                    }
                }
                if (count($wishlist_item_list) > $i && count($recommend_list) < 4 && $wishlist_item_list_count < 2) {
                    if (!in_array($wishlist_item_list[$i]["simple_id"],$recommend_list_keys)) {
                        $recommend_list[]=$wishlist_item_list[$i];
                        $wishlist_item_list_count=$wishlist_item_list_count+1;
                        $recommend_list_keys[$wishlist_item_list[$i]["simple_id"]] = 1;
                    }
                }
                if (count($share_list) > $i && count($recommend_list) < 4 && $share_list_recomment_count < 2) {
                    if (!in_array($share_list[$i]["simple_id"],$recommend_list_keys)) {
                        $recommend_list[]=$share_list[$i];
                        $share_list_recomment_count=$share_list_recomment_count+1;
                        $recommend_list_keys[$share_list[$i]["simple_id"]] = 1;
                    }
                }
            }
            $t5=\app\components\helper\TimezoneHelper::msectime();
//            echo sprintf("RecommendHelper.get_customer_recommend_products.t5-t4=%s".PHP_EOL,$t5-$t4);
            if (count($recommend_list) < 4) {
                $so_recomment_list = $this->get_sales_order_recomment( 4 - count($recommend_list));
                foreach($so_recomment_list as $so_recomment) {
                    $recommend_list[]=$so_recomment;
                }
            }
            $t6=\app\components\helper\TimezoneHelper::msectime();
//            echo sprintf("RecommendHelper.get_customer_recommend_products.t6-t5=%s".PHP_EOL,$t6-$t5);
        } else {
            $s1=\app\components\helper\TimezoneHelper::msectime();
//            echo sprintf("RecommendHelper.get_customer_recommend_products.t6-t5=%s".PHP_EOL,$t6-$t5);
            $recommend_list = [];
            $so_recomment_list = $this->get_sales_order_recomment(4);
            foreach($so_recomment_list as $so_recomment) {
                $recommend_list[]=$so_recomment;
            }
            $s2=\app\components\helper\TimezoneHelper::msectime();
//            echo sprintf("RecommendHelper.get_customer_recommend_products.s2-s1=%s".PHP_EOL,$s2-$s1);
        }
        $ret=[];
        foreach($recommend_list as $recommend) {
            $recommend["url"] = $server_url ."/". $product_urls[$recommend["configurable_id"]]["request_path"] . "?s=" . $recommend["sku"];
            $recommend["qty"] = (int)$recommend["qty"];
            $recommend["image"] = $static_url . "/media/catalog/product/cache/230x115" . $recommend["image"];
            $recommend["product_price"] = round($recommend["product_price"],2);
            $ret[]=$recommend;
        }
        return $ret;
    }

    private function get_sales_order_recomment($limit) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $sql = "select * from recommend_product ORDER BY RAND() limit %s";
        $sql=sprintf($sql,$limit);
        $so_list=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("RecommendHelper.get_sales_order_recomment.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $so_list;
    }
}
