<?php
namespace app\components\helper;

use Yii;
use app\components\magento\MagentoRedis;
use app\components\magento\RedisSession;

class CustomerHelper
{
    private $objectManager;

    public function __construct() {
        $this->objectManager=\Magento\Framework\App\ObjectManager::getInstance();
    }

    public function getPersistentSession() {
        $session=$this->objectManager->get("\Magento\Persistent\Helper\Session")->getSession();
        $persistentSession=json_decode(json_encode($session->load(session_id())->getData()),true);
        return $persistentSession;
    }

    public function getLoginSession() {
        $magento_session=Yii::$container->get("app\components\magento\RedisSession");
        $customerData=$magento_session->read(session_id());
        return $customerData;
    }

    public function getSessionCustomerId() {
        $customer_id=0;
        $customerData=$this->getLoginSession();
        if (empty($customerData) || !isset($customerData["customer_base"])) {
            $persistentSession=$this->getPersistentSession();
            if (isset($persistentSession["customer_id"])) {
                $customer_id=$persistentSession["customer_id"];
                $customerSession=$this->objectManager->get('\Magento\Customer\Model\Session');
                $customerSession->loginById($customer_id);
            }
        } else {
            if(isset($customerData["customer_base"]["customer_id"])){
                $customer_id=$customerData["customer_base"]["customer_id"];
            }else{
                $customer_id=0;
            }
        }
        if (empty($customer_id)) {
            $customer_id=0;
        }
        return $customer_id;
    }

    public function getCustomerById($customer_id){
        $customer=null;

        if (!empty($customer_id)) {
            $customer = $this->objectManager->create('Magento\Customer\Model\Customer')->load($customer_id);
        }
        return $customer;
    }

    public function isDoCustomerLogin() {

    }

    public function loadCustomer($customer_id) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customer_id)->toArray();
        return $customer;
    }

    public function getCurrentQuote() {
        $currentQuote=null;
        $customer_id=$this->getSessionCustomerId();
        if (!empty($customer_id)) {
            $session=$this->getLoginSession();
            if (isset($session["checkout"]["quote_id_1"])) {
                $quote_id=$session["checkout"]["quote_id_1"];
                $currentQuote=$this->objectManager->get("\Magento\Quote\Model\Quote")->load($quote_id);
            } else {
                $quote_id=\app\components\helper\DbHelper::getSlaveDb()->createCommand("select entity_id from `quote` where is_active=1 and customer_id=:customer_id",["customer_id"=>$customer_id])->queryScalar();
                $currentQuote=$this->objectManager->get("\Magento\Quote\Model\Quote")->load($quote_id);
            }
        }
        return $currentQuote;
    }

    public function renderHtml(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //gtm
        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $gtm_value = $_scopeConfig->getValue('googletagmanager/general/account',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $collection_enable = $_scopeConfig->getValue('config/general/scraper_enable',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $collection_api_url = $_scopeConfig->getValue('config/general/scraper_address_web',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $ret=[
            "is_php"=>0,
            "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
            "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
            "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
            "gtm_value"=>$gtm_value,
            "collection_enable"=>$collection_enable,
            "collection_api_url"=>$collection_api_url
        ];
        return $ret;
    }
}