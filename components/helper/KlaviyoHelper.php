<?php
namespace app\components\helper;

use Yii;
use GuzzleHttp\Client as GuzzleClient;
use app\components\helper\DbHelper;

class KlaviyoHelper {
    private $_objectManager;
    private $_scopeConfig;

    private $public_api_key;
    private $private_api_key;
    private $default_klaviyo_list;

    public function __construct(){
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_scopeConfig = $this->_objectManager->create("Magento\Framework\App\Config");
        $this->public_api_key = $this->_scopeConfig->getValue('klaviyo_reclaim_general/general/public_api_key');
        $this->private_api_key = $this->_scopeConfig->getValue('klaviyo_reclaim_general/general/private_api_key');
//        $this->private_api_key=$this->_objectManager->get("Magento\Framework\Encryption\EncryptorInterface")->decrypt($this->private_api_key);//无需再解密
        $this->default_klaviyo_list = $this->_scopeConfig->getValue('config/general/default_klaviyo_list');

        //生产系统，临时处理
//        $this->public_api_key = "PWf4ND";
//        $this->private_api_key = "pk_e63dc6a5dee5d1566815afeaa69a1e72fd";
//        $this->default_klaviyo_list = "XLkVPF";
        //end
    }

    public function getPublicApiKey() {
        return $this->public_api_key;
    }

    public function getPrivateApiKey() {
        return $this->private_api_key;
    }

    public function createOrUpdateContract($profiles,$cfg=[],$list="") {
        try {
            $data=[
                "api_key"=>$this->private_api_key,
                "profiles"=>$profiles
            ];
            $client = new GuzzleClient();
            if (empty($list)) {
                $url=sprintf("https://a.klaviyo.com/api/v2/list/%s/members",$this->default_klaviyo_list);
            } else {
                $url=sprintf("https://a.klaviyo.com/api/v2/list/%s/members",$list);
            }

            $res = $client->request('POST', $url,[
                'timeout' => (isset($cfg["timeout"]))? $cfg["timeout"]:60,
                'json'=>$data
            ]);
            $response=$res->getBody()->getContents();
            $ret=json_decode($response,true);
            return $ret;
        } catch (\Exception $e) {
            echo $e->getMessage().PHP_EOL;
            return $this->createOrUpdateContract($profiles,$cfg,$list);
        }
    }

    public function track($event,$customer_properties=[],$properties=[],$cfg=[]) {
        try {
            $d=[
                "time"=>time(),
                "token"=>$this->public_api_key,
                "customer_properties"=>$customer_properties,
                "properties"=>$properties,
                "event"=>$event
            ];
            $client = new GuzzleClient();
            $url="https://a.klaviyo.com/api/track?data=".base64_encode(json_encode($d));
            $res = $client->request('GET', $url,[
                'timeout' => (isset($cfg["timeout"]))? $cfg["timeout"]:60
            ]);
            $response=$res->getBody()->getContents();
            $ret=json_decode($response,true);
            return $ret;
        } catch (\Exception $e) {
            return $this->track($event,$customer_properties,$properties,$cfg);
        }
    }

    public function identify($customer_properties=[],$cfg=[]) {
        try {
            $d=[
                "token"=>$this->public_api_key,
                "properties"=>$customer_properties,
            ];
            $client = new GuzzleClient();
            $url="https://a.klaviyo.com/api/identify?data=".base64_encode(json_encode($d));
            $res = $client->request('GET', $url,[
                'timeout' => (isset($cfg["timeout"]))? $cfg["timeout"]:60
            ]);
            $response=$res->getBody()->getContents();
            $ret=json_decode($response,true);
            return $ret;
        } catch (\Exception $e) {
            return $this->identify($customer_properties,$cfg);
        }
    }

    public function load_maropost_info($email) {
        $ret=["contract_clicks_cnt"=>0,"contract_opens_cnt"=>0];
        $content=DbHelper::getWebReadDb()->createCommand("SELECT content from pg_maropost_data where email=:email")->bindParam(":email",$email)->queryScalar();
        if ($content) {
            $content_arr=json_decode($content,true);
            $contract_clicks_cnt=0;
            if (count($content_arr["contract_clicks_data"])>0) {
                foreach ($content_arr["contract_clicks_data"] as $key => $contract_clicks_data) {
                    $contract_clicks_cnt=$contract_clicks_cnt+count($contract_clicks_data);
                }
            }
            $contract_opens_cnt=0;
            if (count($content_arr["contract_opens_data"])>0) {
                foreach ($content_arr["contract_opens_data"] as $key => $contract_clicks_data) {
                    $contract_opens_cnt=$contract_opens_cnt+count($contract_clicks_data);
                }
            }
            $ret=[
                "email"=>$email,
                "contract_clicks_cnt"=>$contract_clicks_cnt,
                "contract_clicks_recent_date"=> count($content_arr["contract_clicks_data"])>0?  date('Y-m-d H:i:s',strtotime($content_arr["contract_clicks_data"][0][0]["recorded_at"])):"",
                "contract_opens_cnt"=>$contract_opens_cnt,
                "contract_opens_recent_date"=> count($content_arr["contract_opens_data"])>0? date('Y-m-d H:i:s',strtotime($content_arr["contract_opens_data"][0][0]["recorded_at"])):"",
                "last_visit_time"=>isset($content_arr["contract_for_email"]["last_visit_time"])? date('Y-m-d H:i:s',strtotime($content_arr["contract_for_email"]["last_visit_time"])):""
            ];
        } else {
            $ret=[
                "email"=>$email,
                "contract_clicks_cnt"=>0,
                "contract_clicks_recent_date"=> "",
                "contract_opens_cnt"=>0,
                "contract_opens_recent_date"=> "",
                "last_visit_time"=>""
            ];
        }
        return $ret;
    }
}