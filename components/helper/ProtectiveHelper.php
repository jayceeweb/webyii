<?php
namespace app\components\helper;

use Yii;
use app\models\CatalogProductFlat1;
use app\models\CatalogProductSuperLink;
use app\components\helper\MagentoProductHelper;

class ProtectiveHelper
{
    /**
     * 根据id取simple
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getSimpleObject($simple_id) {
        return MagentoProductHelper::getProductObject($simple_id)["data"];
    }

    /**
     * 根据simple_id返回configurable
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getConfigurableObject($simple_id) {
        return MagentoProductHelper::getParentProductObject($simple_id)["data"];
    }

    public function getAllSimples($configurable_id) {
        $products=MagentoProductHelper::getUsedProductObjects($configurable_id);
        return $products;
    }

    public function renderHtml($simple_id) {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //当前simple
        $currentSimpleData = $this->getSimpleObject($simple_id);
        $currentSimpleJson = (\yii\helpers\Json::encode($currentSimpleData));
        //当前configurable
        $configableData = $this->getConfigurableObject($simple_id);
        $configurable_id=$configableData['entity_id'];
        //simples
        $simpleDatas = $this->getAllSimples($configableData['entity_id']);
        $simple_ids=[];
        foreach ($simpleDatas as $simpleData) {
            $simple_ids[]=$simpleData["entity_id"];
        }
        //图片
        $simple_product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($simple_id);
        $configurable_product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($configurable_id);
        $images = Yii::$container->get("app\models\CatalogProductFlat1")->getSimpleImages($simple_id);
        //add 返回reviews列表
        $reviews=$objectManager->get("Pg\CatalogExtend\Block\Product\View2")->getReviews($configurable_id);
        $review_ids=[];
        foreach ($reviews["data"] as $review) {
            $review_ids[]=$review["reviews_id"];
        }
        $review_ids_str=implode(',', $review_ids);
        $support_share_and_save=$objectManager->get("Pg\CatalogExtend\Block\Product\View2")->supportShareAndSave($simple_ids);
        $_is_shared=$objectManager->get("Pg\CatalogExtend\Block\Product\View2")->is_shared_by_simple_ids($simple_ids);
        $_first_simple=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($simple_ids[0]);
        $_share_price=number_format(round($_first_simple->getFinalSharePrice(),2),2,".","");
        $_final_price=number_format(round($configurable_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),2),2,".","");//价格
        $tag_list = Yii::$container->get("app\models\PgProductTag")->get_tags_by_product_id($configurable_id);
        $tags=[];
        foreach ($tag_list as $key=>$tag) {
            $tags[]=$tag["tag"];
        }
        $all_valid_tags=Yii::$container->get("app\models\PgProductTag")->get_tags_url($tags);
        $amazonHelper=$objectManager->get("\Amazon\Core\Helper\Data");
        $amazonpay_config = [
            "secret_key"  => $amazonHelper->getSecretKey(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "access_key"  => $amazonHelper->getAccessKey(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "merchant_id" => $amazonHelper->getMerchantId(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "region"      => $amazonHelper->getRegion(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "sandbox"     => $amazonHelper->isSandboxEnabled(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "client_id"   => $amazonHelper->getClientId(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default")
        ];
        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $gtm_value = $_scopeConfig->getValue('googletagmanager/general/account',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $collection_enable = $_scopeConfig->getValue('config/general/scraper_enable',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $collection_api_url = $_scopeConfig->getValue('config/general/scraper_address_web',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $data=[
            "simple_id"=>$simple_id,
            "id"=>$simple_id,
            "configurable_id"=>$configurable_id,
            "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
            "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
            "simple_url"=>Yii::$container->get("app\models\UrlRewrite")->getSimpleUrl($simple_id),
            "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
            "support_share_and_save"=>$support_share_and_save,
            "is_share_and_save"=> ($support_share_and_save && $_is_shared) ? true:false,
            "_is_shared"=>$_is_shared,
            "_share_price"=>$_share_price,
            "_final_price"=>$_final_price,
            "currentSimpleData"=>$currentSimpleData,
            "currentSimpleJson"=>$currentSimpleJson,
            "configableData"=>$configableData,
            "simpleDatas"=>$simpleDatas,
//            "product"=>$simple_product,
            "images"=>$images,
            "reviews"=>$reviews,
            "review_ids_str"=>$review_ids_str,
            "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
            "tags"=>$all_valid_tags,
            "amazonpay_config"=>$amazonpay_config,
            "gtm_value"=>$gtm_value,
            "collection_enable"=>$collection_enable,
            "collection_api_url"=>$collection_api_url
        ];
        return $data;
    }
}