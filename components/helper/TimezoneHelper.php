<?php
namespace app\components\helper;

use Yii;

class TimezoneHelper
{
    public static function getTime() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $localeDate = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magento\Framework\Stdlib\DateTime\TimezoneInterface::class);
        return $localeDate->date()->format('Y-m-d H:i:s');
    }

    public static function getDate() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $localeDate = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magento\Framework\Stdlib\DateTime\TimezoneInterface::class);
        return $localeDate->date()->format('Y-m-d');
    }

    public static function msectime() {
        list($msec, $sec) = explode(' ', microtime());
        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }

    public static function timestamp(){
        return time();
    }
}