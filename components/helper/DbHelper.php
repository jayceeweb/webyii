<?php
namespace app\components\helper;

use Yii;

class DbHelper {
    public static function getWebReadWriteDb()
    {
        return Yii::$app->db;
    }

    public static function getWebReadDb()
    {
        return Yii::$app->get('webdbread');
    }

    public static function getSlaveDb() {
        $db=Yii::$app->db->getSlave(false);
        return Yii::$app->db->getSlave();
    }

    public static function getMasterDb(){
        return static::getWebReadWriteDb();
    }

    public static function getCollectionDb()
    {
        return Yii::$app->get('collectiondb');
    }
}
