<?php
namespace app\components\helper;

use app\components\helper\MagentoCacheHelper;
use app\models\PgLensType;
use Monolog\Handler\NullHandlerTest;
use Yii;
use app\models\CatalogProductSuperLink;
use app\models\UrlRewrite;

class MagentoProductHelper
{
    public static $relatedProducts=[];
    public static $parentProductObject=[];
    public static $frameSimpleUrls=[];

    public static function convertProductToArray($data) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//        $d=json_decode($p->toJson(),true);
        $p = $objectManager->get("Pg\CatalogExtend\Model\Product\Interceptor");
        $p->fromArray($data);
        $d=$data;
        $d["entity_id"]=$p->getData("entity_id");
        $d["attribute_set_id"]=$p->getData("attribute_set_id");
        $d["type_id"]=$p->getData("type_id");
        $d["sku"]=$p->getData("sku");
        $d["created_at"]=$p->getData("created_at");
        $d["updated_at"]=$p->getData("updated_at");
        $d["name"]=$p->getData("name");
        $d["meta_title"]=$p->getData("meta_title");
        $d["meta_description"]=$p->getData("meta_description");
        $d["image"]=$p->getData("image");
        $d["small_image"]=$p->getData("small_image");
        $d["thumbnail"]=$p->getData("thumbnail");
        $d["url_key"]=$p->getData("url_key");
        $d["swatch_image"]=$p->getData("swatch_image");
        $d["min_pd"]=$p->getData("min_pd");
        $d["max_pd"]=$p->getData("max_pd");
        $d["stock_sku"]=$p->getData("stock_sku");
        $d["color_details"]=$p->getData("color_details");
        $dp=$objectManager->get("Pg\CatalogExtend\Helper\Product")->calcProductPrice($p);
        $d["price"]=$dp["price"];
        $d["special_price"]=$dp["special_price"];
        $d["special_from_date"]=$dp["special_from_date"];
        $d["special_to_date"]=$dp["special_to_date"];
        $d["sales_price"]=$dp["sales_price"];
        $cache=MagentoCacheHelper::getInstance();
        $d["color_value"]=$cache->get_magento_enum_value("color",$p->getData("color"));
        $d["frame_shape_value"]=$cache->get_magento_enum_value("frame_shape",$p->getData("frame_shape"));
        $d["frame_size_value"]=$cache->get_magento_enum_value("frame_size",$p->getData("frame_size"));
        $d["rim_type_value"]=$cache->get_magento_enum_value("rim_type",$p->getData("rim_type"));
        $d["material_value"]=$cache->get_magento_enum_value("material",$p->getData("material"));
        $d["nose_pad_value"]=$cache->get_magento_yesno_value($p->getData("nose_pad"),"No");
        $d["has_spring_hinges_value"]=$cache->get_magento_yesno_value($p->getData("has_spring_hinges"),"No");
        $d["lens_width_value"]=$cache->get_magento_enum_value("lens_width",$p->getData("lens_width"));
        $d["bridge_value"]=$cache->get_magento_enum_value("bridge",$p->getData("bridge"));
        $d["temple_length_value"]=$cache->get_magento_enum_value("temple_length",$p->getData("temple_length"));
        $d["frame_width_value"]=$cache->get_magento_enum_value("frame_width",$p->getData("frame_width"));
        $d["lens_height_value"]=$cache->get_magento_enum_value("lens_height",$p->getData("lens_height"));
        $d["frame_weight_value"]=$cache->get_magento_enum_value("frame_weight",$p->getData("frame_weight"));
        $d["support_progressive_value"]=$cache->get_magento_enum_value("support_progressive",$p->getData("support_progressive"));
        $d["recommend_value"]=$cache->get_magento_enum_value("recommend",$p->getData("recommend"));
        $d["age_range_value"]=$cache->get_magento_enum_value("age_range",$p->getData("age_range"));
        $d["min_sph_value"]=$cache->get_magento_enum_value("min_sph",$p->getData("min_sph"));
        $d["max_sph_value"]=$cache->get_magento_enum_value("max_sph",$p->getData("max_sph"));
        $d["other_colors_value"]=$cache->get_magento_enum_value("other_colors",$p->getData("other_colors"));
        $d["nose_bridge_value"]=$cache->get_magento_enum_value("nose_bridge",$p->getData("nose_bridge"));
        $d["support_rx"]=$p->getSupportRx();
        if ($d["support_rx"]=="" || $d["support_rx"]==null || $d["support_rx"]==1) {
            $d["support_rx"]=1;
        } else {
            $d["support_rx"]=0;
        }
        if (empty($p->getData("is_clip_on"))) {
            $d["is_clip_on"]=0;
        } else {
            $d["is_clip_on"]=$p->getData("is_clip_on");
        }
        if ($p->getData("try_on")=="1") {
            $d["try_on"]=1;
        } else {
            $d["try_on"]=0;
        }
        $d["clipon"]=$p->getData("clipon");
        if ($p->getData("only_blue_light_blocking")=="1") {
            $d["only_blue_light_blocking"]=1;
        } else {
            $d["only_blue_light_blocking"]=0;
        }
        $support_progressive_value = strtolower($cache->get_magento_enum_value("support_progressive",$p->getData("support_progressive")));
        $d["support_progressive_value"]=$support_progressive_value;
        $ret=["data"=>$d,"obj"=>$p];
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.convertProductToArray.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }
    public static function getProductObjectBySku($sku) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cache=MagentoCacheHelper::getInstance();
        $key=sprintf("MAGENTO_PRODUCT_CACHE_BY_KEY_%s",md5($sku));
        if ($cache->hasCache($key)) {
            $data=$cache->getCache($key);
            $data=json_decode($data,true);
        } else {
            $p=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->get($sku);
            $data=json_decode($p->toJson(),true);
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getProductObjectBySku.t2-t1=%s".PHP_EOL,$t2-$t1);
        return static::convertProductToArray($data);
    }

    public static function getProductObject($product_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cache=MagentoCacheHelper::getInstance();
        $key=sprintf("MAGENTO_PRODUCT_CACHE_BY_ID_%s",md5($product_id));
        if ($cache->hasCache($key)) {
            $data=$cache->getCache($key);
            $data=json_decode($data,true);
        } else {
            $p=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($product_id);
            $data=json_decode($p->toJson(),true);
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getProductObject.t2-t1=%s".PHP_EOL,$t2-$t1);
        return static::convertProductToArray($data);
    }

    public static function getSimpleProductObject($simple_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $p=static::getProductObject($simple_id);
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getSimpleProductObject.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $p;
    }

    public static function getFrameSimpleUrls($category_id,$currentSimpleData,$additional_info,$frame_group,$tag="") {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $key=md5(sprintf("%s_%s_%s_%s_%s",$category_id,json_encode($currentSimpleData),json_encode($additional_info),$frame_group,$tag));

        $cache=MagentoCacheHelper::getInstance();
        $hasCache=$cache->hasCache($key);
        if($hasCache) {
            $ret=$cache->getCache($key);
        } else {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            if (strtolower($tag)=="grant") {
                $entity_type="pg_grant_frame";
            } else {
                $entity_type="pg_frame";
            }
            $support_progressive_value=$currentSimpleData["support_progressive_value"];
            if ($support_progressive_value=="yes") {
                $support_progressive=1;
            } else {
                $support_progressive=0;
            }
            $productHelper=$objectManager->get("Pg\CatalogExtend\Helper\Product");
            $param=[
                "entity_id"=>$currentSimpleData["entity_id"],
                "sku"=>$currentSimpleData["sku"],
                "category_id"=>$category_id,
                "rim_type_value"=>$currentSimpleData["rim_type_value"],
                "support_rx"=>$currentSimpleData["support_rx"],
                "support_progressive"=>$support_progressive,
                "tag"=>$tag,
                "attribute_set_id"=>$currentSimpleData["attribute_set_id"],
                "only_blue_light_blocking"=>$currentSimpleData["only_blue_light_blocking"],
            ];
            $simple_frame_group=$productHelper->get_frame_group_by_rim_type($param);
            $lens_type_rows=$productHelper->get_product_lens_types($simple_frame_group);
            foreach ($lens_type_rows as $lens_type_row) {
                $ALL_LENS_TYPE[]=$lens_type_row["lens_type"];
            }
            foreach ($ALL_LENS_TYPE as $lens_type_key=>$v_lens_type) {
                $simple_urls[$v_lens_type]["url"]=Yii::$container->get("app\models\UrlRewrite")->getSimpleFrameUrl($currentSimpleData["entity_id"],$v_lens_type,$entity_type);
                $antireflective_product_info=$additional_info["antireflective_product_info"];
                if (strtolower($v_lens_type)==strtolower("blue_non_rx")) {
                    //蓝光价格
                    $blocker_product_info=$additional_info["blocker_product_info"];
                    $simple_urls[$v_lens_type]["price"]=number_format($currentSimpleData["price"]+$blocker_product_info["price"]+$antireflective_product_info["price"],2);
                    $simple_urls[$v_lens_type]["sales_price"]=number_format($currentSimpleData["sales_price"]+$blocker_product_info["sales_price"]+$antireflective_product_info["sales_price"],2);
                } else if (strtolower($v_lens_type)==strtolower("Progressive")) {
                    //渐进价格
                    $progressive_product_info=$additional_info["progressive_product_info"];
                    $simple_urls[$v_lens_type]["price"]=number_format($currentSimpleData["price"]+$progressive_product_info["price"]+$antireflective_product_info["price"],2);
                    $simple_urls[$v_lens_type]["sales_price"]=number_format($currentSimpleData["sales_price"]+$progressive_product_info["sales_price"]+$antireflective_product_info["sales_price"],2);
                } else if (strtolower($v_lens_type)==strtolower("Bifocal")) {
                    //平顶双光价格
                    $bifocal_product_info=$additional_info["bifocal_product_info"];
                    $simple_urls[$v_lens_type]["price"]=number_format($currentSimpleData["price"]+$bifocal_product_info["price"]+$antireflective_product_info["price"],2);
                    $simple_urls[$v_lens_type]["sales_price"]=number_format($currentSimpleData["sales_price"]+$bifocal_product_info["sales_price"]+$antireflective_product_info["sales_price"],2);
                }
                //sjc
                else if (strtolower($v_lens_type)==strtolower("frame_only")) {
                    //frame_only价格
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $product_helper=$objectManager->get("\Pg\CatalogExtend\Helper\Product");
                    $finalPrice=$product_helper->getFramePrice($currentSimpleData["entity_id"]);
                    $simple_urls[$v_lens_type]["price"]=number_format($finalPrice,2);
                    $simple_urls[$v_lens_type]["sales_price"]=number_format($finalPrice,2);
                }
//                end
                else {
                    $simple_urls[$v_lens_type]["price"]=number_format($currentSimpleData["price"],2);
                    $simple_urls[$v_lens_type]["sales_price"]=number_format($currentSimpleData["sales_price"],2);
                }
                $pg_lens_type_data=PgLensType::getDataByLensTypeAndFrameGroup($v_lens_type,$frame_group)->getAttributes();
                if ($currentSimpleData["is_clip_on"]) {
                    $price_included=$pg_lens_type_data["clipon_price_included"];
                } else {
                    $price_included=$pg_lens_type_data["price_included"];
                }
                $url_rewrite_path_do=UrlRewrite::getByPath($simple_urls[$v_lens_type]["url"]);
                if (empty($url_rewrite_path_do)) {
                    $monitor_log=$objectManager->create("Pg\Monitor\Log");
                    $monitor_log->log_start("url_rewrite_error");
                    $data=[
                        "request"=>$_REQUEST,
                        "simple_urls"=>$simple_urls,
                        "v_lens_type"=>$v_lens_type,
                        "param"=>$param,
                        "entity_type"=>$entity_type,
                        "simple_frame_group"=>$simple_frame_group,
                        "url"=>$simple_urls[$v_lens_type]["url"]
                    ];
                    $monitor_log->log($data);
                    $monitor_log->log_stop();
                }
                $url_rewrite_do=$url_rewrite_path_do->getAttributes();
                $simple_urls[$v_lens_type]["price_included"]=$price_included;
                $simple_urls[$v_lens_type]["dest_sku"]=$url_rewrite_do["dest_sku"];
                $simple_urls[$v_lens_type]["dest_name"]=$url_rewrite_do["dest_name"];
                $simple_urls[$v_lens_type]["meta_title"]=$url_rewrite_do["meta_title"];
                $simple_urls[$v_lens_type]["meta_keywords"]=$url_rewrite_do["meta_keywords"];
                $simple_urls[$v_lens_type]["meta_description"]=str_replace('"','',$url_rewrite_do["meta_description"]);
                $simple_urls[$v_lens_type]["page_description"]=str_replace('"','',$url_rewrite_do["page_description"]);
                $simple_urls[$v_lens_type]["first_description"]=str_replace('"','',$url_rewrite_do["first_description"]);
                $simple_urls[$v_lens_type]["second_description"]=str_replace('"','',$url_rewrite_do["second_description"]);
                $simple_urls[$v_lens_type]["name"]=$pg_lens_type_data["name"];
                $simple_urls[$v_lens_type]["mark"]=$pg_lens_type_data["mark"];
                $simple_urls[$v_lens_type]["category_id"]=$category_id;
                $simple_urls[$v_lens_type]["stock_sku"]=$currentSimpleData["stock_sku"];
                $simple_urls[$v_lens_type]["frame_group"]=$frame_group;
                $simple_urls[$v_lens_type]["relate_product"]=MagentoProductHelper::get_relate_product($category_id,$currentSimpleData,$frame_group,$tag,$v_lens_type);
            }
            $cache->setCache($key,$simple_urls);
            $ret=$simple_urls;
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getFrameSimpleUrls.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    public static function getFrameSimpleUrl($simple_id,$tag="",$lens_type="") {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        if (strtolower($tag)=="grant") {
            $entity_type="pg_grant_frame";
        } else {
            $entity_type="pg_frame";
        }
        $simple_url=Yii::$container->get("app\models\UrlRewrite")->getSimpleFrameUrl($simple_id,$lens_type,$entity_type);
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getFrameSimpleUrl.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $simple_url;
    }

    public static function getParentProductObject($simple_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $ret=null;
        $key=sprintf("getParentProductObject_%s",$simple_id);
        $cache=MagentoCacheHelper::getInstance();
        if($cache->hasCache($key)) {
            $ret=$cache->getCache($key);
        } else {
            $parent=CatalogProductSuperLink::findOne(["product_id"=>$simple_id]);
            if(!empty($parent)) {
                $aprent_id=$parent->getAttribute("parent_id");
                $p=static::getProductObject($aprent_id);
                $ret=$p;
            } else {
                $ret=null;
            }
            $cache->setCache($key,$ret);
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getParentProductObject.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    public static function getParentProductId($simple_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $ret=0;
        $parent=CatalogProductSuperLink::findOne(["product_id"=>$simple_id]);
        if(!empty($parent)) {
            $aprent_id=$parent->getAttribute("parent_id");
            $ret=$aprent_id;
        } else {
            $ret=0;
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getParentProductId.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    public static function getUsedProductObjects($configurable_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $ret=[];
        $childs=CatalogProductSuperLink::findAll(["parent_id"=>$configurable_id]);
        foreach ($childs as $key=>$child) {
            $d=static::getSimpleProductObject($child->getAttribute("product_id"))["data"];
            $d["url"]=Yii::$container->get("app\models\UrlRewrite")->getSimpleUrl($d["entity_id"]);
            $ret[]=$d;
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getUsedProductObjects.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    /**
     * 根据configurable返回frame子产品内容
     * @param $configurable_id
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function getSimpleFrameProductObjects($configurable_id,$category_id,$additional_info,$tag="") {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $ret=[];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sql=sprintf("select sp.product_id 
from catalog_product_super_link sp 
inner join catalog_product_flat_all_1 f1 on f1.entity_id=sp.product_id 
INNER JOIN catalog_product_entity_int simple_attr on simple_attr.entity_id=f1.entity_id and simple_attr.attribute_id=(SELECT attribute_id FROM eav_attribute WHERE entity_type_id=4 AND attribute_code='status') 
where sp.parent_id=%s and simple_attr.value=1 AND simple_attr.`store_id`=0",$configurable_id);
        $childs=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($childs as $key=>$child) {
            $d=static::getSimpleProductObject($child["product_id"])["data"];
            $support_progressive_value=$d["support_progressive_value"];
            if ($support_progressive_value=="yes") {
                $support_progressive=1;
            } else {
                $support_progressive=0;
            }
            $productHelper=$objectManager->get("Pg\CatalogExtend\Helper\Product");
            $param=[
                "entity_id"=>$d["entity_id"],
                "sku"=>$d["sku"],
                "category_id"=>$category_id,
                "rim_type_value"=>$d["rim_type_value"],
                "support_rx"=>$d["support_rx"],
                "support_progressive"=>$support_progressive,
                "tag"=>$tag,
                "attribute_set_id"=>$d["attribute_set_id"],
                "only_blue_light_blocking"=>$d['only_blue_light_blocking'],
            ];
            $simple_frame_group=$productHelper->get_frame_group_by_rim_type($param);
            $simple_urls=MagentoProductHelper::getFrameSimpleUrls($category_id,$d,$additional_info,$simple_frame_group,$tag);
            $d["simple_urls"]=$simple_urls;
            foreach ($d["simple_urls"] as $lens_type=>$simple_url) {
                $d["simple_urls"][$lens_type]["relate_product"]=MagentoProductHelper::get_relate_product($category_id,$d,$simple_frame_group,$tag,$lens_type,$additional_info);
            }
            $ret[]=$d;
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getSimpleFrameProductObjects.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    /**
     * 蓝光镜片信息
     * @param $current_product
     * @return mixed
     */
    public static function getBlockerProductInfo($current_product,$param_info) {
        $t1=\app\components\helper\TimezoneHelper::msectime();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $reading_sku_sb592_configurable = $_scopeConfig->getValue('config/general/reading_glasses_sb592_configurable',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $reading_sku_sb592_array = explode(',', $reading_sku_sb592_configurable);
        $sku=$current_product["sku"];
        if(in_array($sku, $reading_sku_sb592_array)) {
            $product_info=$param_info["dp_data"]["LSB592"];
        } else {
            $product_info=$param_info["dp_data"]["LSB591"];
        }

        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getBlockerProductInfo.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $product_info;
    }

    /**
     * 渐进镜片信息
     * @param $current_product
     * @return mixed
     */
    public static function getProgressiveProductInfo($current_product,$param_info) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $rim_type_value=$current_product["rim_type_value"];
        if (strtolower($rim_type_value)==strtolower("Full Rim") || strtolower($rim_type_value)==strtolower("Semi Rimless")) {
            $product_info=$param_info["dp_data"]["LFC56"];
        } else {
            $product_info=$param_info["dp_data"]["LFC59"];
        }

        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getBlockerProductInfo.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $product_info;
    }

    /**
     * 平顶双光产品价格信息
     * @param $current_product
     * @return mixed
     */
    public static function getBifocalProductInfo($current_product,$param_info) {
        $t1=\app\components\helper\TimezoneHelper::msectime();

        $rim_type_value=$current_product["rim_type_value"];
        if (strtolower($rim_type_value)==strtolower("Full Rim") || strtolower($rim_type_value)==strtolower("Semi Rimless")) {
            $product_info=$param_info["dp_data"]["LRD50"];
        } else {
            $product_info=$param_info["dp_data"]["LRD59"];
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getBifocalProductInfo.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $product_info;
    }

    /**
     * VCAR工艺价格信息
     * @param $current_product
     * @return mixed
     */
    public static function getAntiReflectiveProductInfo($current_product,$param_info) {
        $t1=\app\components\helper\TimezoneHelper::msectime();

        $product_info=$product_info=$param_info["dp_data"]["VCAR"];

        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getAntiReflectiveProductInfo.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $product_info;
    }

    public static function get_tags_url($tags) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $tag_str="";
        $tag_urls=[];
        if (count($tags)>0) {
            $i=0;
            foreach ($tags as $key=>$tag) {
                if ($i==0) {
                    $tag_str=sprintf("'%s'",$tag["tag"]);
                } else {
                    $tag_str=sprintf('%s,\'%s\'',$tag_str,$tag["tag"]);
                }
                $i=$i+1;
            }
            $sql=sprintf("SELECT `name`,base_url_canonical FROM `pg_tag_html` WHERE `name` IN (%s) ORDER BY category_id DESC",$tag_str);
            $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $tag_urls=[];
            foreach ($rows as $row) {
                if(!isset($tag_urls[$row["name"]])) {
                    $tag_urls[]=['name'=>$row["name"],'url'=>$row["base_url_canonical"]];
                }
            }
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.get_tags_url.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $tag_urls;
    }

    public static function getConfigurableTags($configurable_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $ret=[];
        $sql=sprintf("SELECT tag FROM `pg_product_tag` WHERE product_id=%s",$configurable_id);
        $tags=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $all_valid_tags=static::get_tags_url($tags);
        foreach ($all_valid_tags as $key=>$tag) {
            $ret[]=$tag;
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getConfigurableTags.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    public static function getRelatedProducts($simple_stock_sku,$category_id,$lens_type) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $sql=sprintf("SELECT product.`entity_id`,product.`sku`,cpev1.`value` AS `name`,cpev3.`value` AS stock_sku,
category.`category_id`,parent.`parent_id`,parent_product.`sku` AS configurable_sku
FROM `catalog_product_entity` product
INNER JOIN catalog_product_entity_varchar cpev1 ON cpev1.`entity_id`=product.`entity_id` AND cpev1.`attribute_id`=(SELECT attribute_id FROM eav_attribute WHERE attribute_code='name' AND entity_type_id=4)
INNER JOIN catalog_product_entity_int cpiv2 ON cpiv2.`entity_id`=product.`entity_id` AND cpiv2.`attribute_id`=(SELECT attribute_id FROM eav_attribute WHERE attribute_code='status' AND entity_type_id=4)
INNER JOIN catalog_product_entity_varchar cpev3 ON cpev3.`entity_id`=product.`entity_id` AND cpev3.`attribute_id`=(SELECT attribute_id FROM eav_attribute WHERE attribute_code='stock_sku' AND entity_type_id=4)
INNER JOIN catalog_product_super_link parent ON parent.`product_id`=product.`entity_id`
INNER JOIN catalog_category_product category ON category.`product_id`=parent.`parent_id`
INNER JOIN catalog_product_entity parent_product ON parent_product.`entity_id`=parent.`parent_id`
WHERE cpev3.value='%s' AND category.`category_id`=%s AND cpiv2.`value`=1",$simple_stock_sku,$category_id);
        $key=md5($sql);
        $cache=MagentoCacheHelper::getInstance();
        if($cache->hasCache($key)) {
            $data=$cache->getCache($key);
        } else {
            $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $cache->setCache($key,$data);
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.getRelatedProducts.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $data;
    }

    public static function get_relate_product($category_id,$product,$simple_frame_group,$tag,$lens_type) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $relateProduct=[];
        $stock_sku=$product["stock_sku"];
        $cfg=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["PRODUCT_CATEGORY"];
        if ($category_id==$cfg["EYEGLASSES_WOMEN"] || $category_id==$cfg["EYEGLASSES_MEN"]) {
            //如果为6,8时，判断是否有太阳镜
            if ($lens_type=="Blue_Non_Rx") {
                $relateProduct=[];
            } else {
                if ($category_id==$cfg["EYEGLASSES_WOMEN"]) {
                    $relateProduct=static::getRelatedProducts($stock_sku,$cfg["SUNGLASSES_WOMEN"],$lens_type);
                } else if ($category_id==$cfg["EYEGLASSES_MEN"]) {
                    $relateProduct=static::getRelatedProducts($stock_sku,$cfg["SUNGLASSES_MEN"],$lens_type);
                }
            }
        } else if ($category_id==$cfg["SUNGLASSES_WOMEN"] || $category_id==$cfg["SUNGLASSES_MEN"]) {
            //如果为16,17时，判断是否有普通眼镜
            if ($category_id==$cfg["SUNGLASSES_WOMEN"]) {
                $relateProduct=static::getRelatedProducts($stock_sku,$cfg["EYEGLASSES_WOMEN"],$lens_type);
            } else if ($category_id==$cfg["SUNGLASSES_MEN"]) {
                $relateProduct=static::getRelatedProducts($stock_sku,$cfg["EYEGLASSES_MEN"],$lens_type);
            }
        }
        if (!empty($relateProduct)) {
            $relateProduct["url"]=static::getFrameSimpleUrl($relateProduct["entity_id"],$tag,$lens_type);
        } else {
            $relateProduct=[];
        }
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("MagentoProductHelper.get_relate_product.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $relateProduct;
    }

    public static function calcProductPriceByArray($p) {
        $d=[];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $d["price"]=number_format($p["price"],2);
        $d["special_price"]=number_format($p["special_price"],2);
        $d["special_from_date"]=$p["special_from_date"];
        $d["special_to_date"]=$p["special_to_date"];
        $d["sales_price"]=number_format($p["price"],2);
        if ($d["special_price"]==null) {
            //没有定义special_price
            $d["sales_price"]=$d["price"];
        } else {
            //定义了special_price
            $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
            $now=$localeDate->date()->format('Y-m-d');
            $special_from_date=date('Y-m-d',strtotime($d["special_from_date"]));
            $special_to_date=date('Y-m-d',strtotime($d["special_to_date"]));
            if (
                ($special_from_date <= $special_to_date && $special_from_date>$now)
                || ($special_from_date <= $special_to_date && $special_to_date<$now)
                || ($special_from_date >= $special_to_date && $special_from_date > $now)
            ) {
                //不在活动期
                $d["sales_price"]=number_format($p["price"],2);
            } else {
                $d["sales_price"]=number_format($d["special_price"],2);
            }
        }
        return $d;
    }
}