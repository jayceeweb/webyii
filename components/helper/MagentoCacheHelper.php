<?php
namespace app\components\helper;

use Yii;
use app\components\helper\TimezoneHelper;

class MagentoCacheHelper {
    public static $static;

    public static function getInstance() {
        if (empty(MagentoCacheHelper::$static)) {
            MagentoCacheHelper::$static=new MagentoCacheHelper();
        }
        return MagentoCacheHelper::$static;
    }

    public function get_key($code) {
        $key=sprintf("yii_cache_hash_%s",$code);
        return $key;
    }

    public function get_enum_color_list($code) {
        $sql=sprintf("SELECT
  `main_table`.`option_id`, `tsv`.`value` AS 'code', `swatch_table`.`value` AS 'code_option',`swatch_table`.type
FROM
  `eav_attribute_option` AS main_table
    LEFT JOIN `eav_attribute_option_value` AS tsv ON `tsv`.`option_id` = `main_table`.`option_id` AND `tsv`.`store_id` = 0
    LEFT JOIN `eav_attribute_option_swatch` AS swatch_table ON `swatch_table`.`option_id` = `main_table`.`option_id` AND `swatch_table`.`store_id` = 0
WHERE
  (`attribute_id` = (select attribute_id from eav_attribute where attribute_code='%s')) AND (`tsv`.`store_id` = 0)
ORDER BY `main_table`.`option_id` ASC",$code);
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $rows;
    }

    public function cache_magento_enum($code) {
        $key=$this->get_key($code);
        $cache=Yii::$app->redis_cache;
        $source=$cache->del($key);
        $rows=$this->get_enum_color_list($code);
        $cache_contents=[];
        foreach ($rows as $row) {
            $cache_contents[$row["option_id"]]=$row;
        }
        $cache_data=[
            "data"=>$cache_contents,
            "cache_time"=>TimezoneHelper::timestamp(),
        ];
        $cache->setex($key,7200,json_encode($cache_data));
        return $source;
    }

    public function get_magento_enum_value($code,$field) {
        $ret="";
        $key=$this->get_key($code);
        $cache=Yii::$app->redis_cache;
        if ($cache->exists($key))
        {
            $cache_data=json_decode($cache->get($key),true);
        } else {
            $this->cache_magento_enum($code);
            $cache_data=json_decode($cache->get($key),true);
        }
        $cache_contents=$cache_data["data"];
        if ($field!="") {
            $field_ids=explode(",",$field);
            $index=0;
            foreach ($field_ids as $field_id) {
                $index=$index+1;
                $data=$cache_contents[$field_id];
                $val=$data["code"];
                if ($index==1) {
                    $ret=$val;
                } else {
                    $ret=$ret.",".$val;
                }
            }
        }
        return $ret;
    }

    public function get_magento_yesno_value($value,$default="No") {
        if (!empty($value)) {
            if ($value==1) {
                return "Yes";
            } else {
                return "No";
            }
        } else {
            return $default;
        }
    }

    public function setCache($key,$data,$expire=7200) {
        $key=$this->get_key($key);
        $cache=Yii::$app->redis_cache;
        $cache_data=[
            "data"=>$data,
            "cache_time"=>TimezoneHelper::timestamp(),
        ];
        $cache->setex($key,$expire,json_encode($cache_data));
    }

    public function getCache($key) {
        $key=$this->get_key($key);
        $cache=Yii::$app->redis_cache;
        $cache_data=json_decode($cache->get($key),true);
        $data=$cache_data["data"];
        return $data;
    }

    public function hasCache($key,$expire=7200) {
        $ret=false;
        $key=$this->get_key($key);
        $cache=Yii::$app->redis_cache;
        if ($cache->exists($key))
        {
            $cache_data=json_decode($cache->get($key),true);
            if ( !isset($cache_data["cache_time"])
                || ((TimezoneHelper::timestamp()-$cache_data["cache_time"]) > $expire)
            ) {
                $ret=false;
            } else {
                $ret=true;
            }
        } else {
            $ret=false;
        }
        return $ret;
    }
}
