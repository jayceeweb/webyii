<?php
namespace app\components\helper;

use Elasticsearch\ClientBuilder;

class ESHelper
{
    private static $instance = null;

    private $esClient = null;

    /**
     * 为了简化测试，本测试默认只操作一个Index，一个Type，
     * 所以这里固定为 megacorp和employee
     */
    private $index = 'megacorp';
    private $type = 'employee';

    //构造器私有化:禁止从类外部实例化
    private function __construct(){}

    //克隆方法私有化:禁止从外部克隆对象
    private function __clone(){}

    public static function getInstance()
    {
        //检测当前类属性$instance是否已经保存了当前类的实例
        if (self::$instance == null) {
            //如果没有,则创建当前类的实例
            self::$instance = new self();
        }
        //如果已经有了当前类实例,就直接返回,不要重复创建类实例
        return self::$instance;
    }

    public function getES()
    {
//        $hosts = [
//            '127.0.0.1:9200'
//        ];
//        $hosts=[
//            [
//                'host' => 'vpc-pgweb-odphvhsw2gp5e2vufzd22mp6ni.us-east-1.es.amazonaws.com',
//                'port' => '443',
//                'scheme' => 'https',
//                'user' => 'pgesuser',
//                'pass' => 'FF839Lpws!drfctgazs'
//            ]
//        ];
//        $hosts=[
//            [
//                'host' => '127.0.0.1',
//                'port' => '9200'
//            ]
//        ];

        $hosts=\Yii::$app->params["ES_HOST"];

//        $credentials = new \Aws\Credentials\Credentials('AKIAZ5DU34VYZNKPI7FG', 'gjhPdgyEtQQkP8WqfgiTUb0ibx7DISFcUo/vQucg');

//        $provider = \Aws\Credentials\CredentialProvider::defaultProvider();
//        $credentials = call_user_func($provider)->wait();
//        $signature = new \Aws\Signature\SignatureV4('es', 'us-east-1');
//
//        $middleware = new \Wizacha\Middleware\AwsSignatureMiddleware($credentials, $signature);
//        $defaultHandler = \Elasticsearch\ClientBuilder::defaultHandler();
//        $awsHandler = $middleware($defaultHandler);

        $this->esClient = \Elasticsearch\ClientBuilder::create()
            ->setRetries(2)
//            ->setHandler($awsHandler)
            ->setHosts($hosts)
            ->build();
        return $this->esClient;
    }

    /**
     * 删除一个文档
     * @param $id
     * @return array
     */
    public function delteDoc($param) {
        $params = [
            'index' => $param["index"],
            'type' => $param["type"],
            'id' =>$param["id"]
        ];
        return $this->esClient->delete($params);
    }

    /**
     * 搜索文档，query是查询条件
     * @param array $query
     * @param int $from
     * @param int $size
     * @return array
     */
    public function search($param,$query = [], $from = 0, $size = 5) {
        $params = [
            'index' => $param["index"],
            'type' => $param["type"],
            '_source' => ['first_name','age'], // 请求指定的字段
            'body' => array_merge([
                'from' => $from,
                'size' => $size
            ],$query)
        ];
        return $this->esClient->search($params);
    }

    /**
     * 一次获取多个文档
     * @param $ids
     * @return array
     */
    public function getDocs($param) {
        $params = [
            'index' => $param["index"],
            'type' => $param["type"],
            'body' => ['ids' => $param["ids"]]
        ];
        return $this->esClient->mget($params);
    }

    /**
     * 获取单个文档
     * @param $id
     * @return array
     */
    public function getDoc($param) {
        $params = [
            'index' => $param["index"],
            'type' => $param["type"],
            'id' =>$param["id"]
        ];
        return $this->esClient->get($params);
    }

    /**
     * 更新一个文档
     * @param $id
     * @return array
     */
    public function updateDoc($param) {
        $params = [
            'index' => $param["index"],
            'type' => $param["type"],
            'id' =>$param["id"],
            'body' => $param["body"]
        ];
        return $this->esClient->update($params);
    }

    /**
     * 添加一个文档到 Index 的Type中
     * @param array $body
     * @return void
     */
    public function putDoc($param) {
        $params = [
            'index' => $param["index"],
            'type' => $param["type"],
            'id' => $param["id"], #可以手动指定id，也可以不指定随机生成
            'body' => $param["body"]
        ];
        $this->esClient->index($params);
    }

    /**
     * 删除所有的 Index
     */
    public function delAllIndex() {
        $indexList = $this->esStatus()['indices'];
        foreach ($indexList as $item => $index) {
            $this->delIndex();
        }
    }

    /**
     * 获取 ES 的状态信息，包括index 列表
     * @return array
     */
    public function esStatus() {
        return $this->esClient->indices()->stats();
    }

    /**
     * 创建一个索引 Index （非关系型数据库里面那个索引，而是关系型数据里面的数据库的意思）
     * @return void
     */
    public function createIndex($param) {
        $this->delIndex();
        $params = [
            'index' => $param["index"],
            'body' => $param["body"]
        ];
        $this->esClient->indices()->create($params);
    }

    /**
     * 检查Index 是否存在
     * @return bool
     */
    public function checkIndexExists($param) {
        $params = [
            'index' => $param["index"]
        ];
        return $this->esClient->indices()->exists($params);
    }

    /**
     * 删除一个Index
     * @return void
     */
    public function delIndex($param) {
        $params = [
            'index' => $param["index"]
        ];
        if ($this->checkIndexExists()) {
            $this->esClient->indices()->delete($params);
        }
    }

    /**
     * 获取Index的文档模板信息
     * @return array
     */
    public function getMapping($param) {
        $params = [
            'index' => $param["index"]
        ];
        return $this->esClient->indices()->getMapping($params);
    }

    /**
     * 创建文档模板
     * @return void
     */
    public function createMapping($param) {
        $this->createIndex();
        $params = [
            'index' => $param["index"],
            'type' => $param["type"],
            'body' => $param["body"]
        ];
        $this->esClient->indices()->putMapping($params);
    }

}