<?php
namespace app\components\helper;

use app\models\UrlRewrite;
use Yii;
use app\models\PgLensType;

class FrameHelper
{
    /**
     * 根据id取simple
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getSimpleObject($simple_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $ret=MagentoProductHelper::getProductObject($simple_id)["data"];
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("FrameHelper.getSimpleObject.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    /**
     * 根据simple_id返回configurable
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getConfigurableObject($simple_id) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $ret=MagentoProductHelper::getParentProductObject($simple_id)["data"];
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("FrameHelper.getConfigurableObject.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }

    public function getAllSimples($configurable_id,$category_id,$additional_info,$tag="") {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $products=MagentoProductHelper::getSimpleFrameProductObjects($configurable_id,$category_id,$additional_info,$tag);
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("FrameHelper.getAllSimples.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $products;
    }

    public function renderHtml($simple_rewrite,$param_info) {
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //当前simple
        $simple_id=$simple_rewrite["simple_id"];
        $lens_type=$simple_rewrite["lens_type"];
        $frame_group=$simple_rewrite["frame_group"];
        $category_id=$simple_rewrite["category_id"];
        $param_tag=$simple_rewrite["tag"];
        if (strtolower($param_tag)=="grant") {
            $entity_type="pg_grant_frame";
        } else {
            $entity_type="pg_frame";
        }
        $simple_url=Yii::$container->get("app\models\UrlRewrite")->getSimpleFrameUrl($simple_id,$lens_type,$entity_type);
        $currentSimpleData = $this->getSimpleObject($simple_id);
        $currentSimpleJson = json_encode($currentSimpleData);
        //当前configurable
        $configableData = $this->getConfigurableObject($simple_id);
        $configurable_id=$configableData['entity_id'];
        //simples

        $progressive_product_info=MagentoProductHelper::getProgressiveProductInfo($currentSimpleData,$param_info);
        $bifocal_product_info=MagentoProductHelper::getBifocalProductInfo($currentSimpleData,$param_info);
        $antireflective_product_info=MagentoProductHelper::getAntiReflectiveProductInfo($currentSimpleData,$param_info);
        $blocker_product_info=MagentoProductHelper::getBlockerProductInfo($currentSimpleData,$param_info);

        $additional_info=[
            'progressive_product_info'=>$progressive_product_info,
            'bifocal_product_info'=>$bifocal_product_info,
            'antireflective_product_info'=>$antireflective_product_info,
            'blocker_product_info'=>$blocker_product_info,
        ];
        $simpleDatas = $this->getAllSimples($configableData['entity_id'],$category_id,$additional_info,$param_tag);
        $simple_ids=[];
        foreach ($simpleDatas as $simpleData) {
            $simple_ids[]=$simpleData["entity_id"];
        }
        //图片
        $simple_product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($simple_id);
        $configurable_product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($configurable_id);
        $images = Yii::$container->get("app\models\CatalogProductFlat1")->getSimpleImages($simple_id);

        //add 返回reviews列表
        $reviews=$objectManager->get("Pg\CatalogExtend\Block\Product\View2")->getReviews($configurable_id);
        $review_ids=[];
        foreach ($reviews["data"] as $review) {
            $review_ids[]=$review["reviews_id"];
        }
        $review_ids_str=implode(',', $review_ids);
        $support_share_and_save=$objectManager->get("Pg\CatalogExtend\Block\Product\View2")->supportShareAndSave($simple_ids);
        $_is_shared=$objectManager->get("Pg\CatalogExtend\Block\Product\View2")->is_shared_by_simple_ids($simple_ids);
        $_first_simple=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($simple_ids[0]);
        $_share_price=number_format(round($_first_simple->getFinalSharePrice(),2),2,".","");
        $_final_price=number_format(round($configurable_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(),2),2,".","");//价格
        $tag_list = Yii::$container->get("app\models\PgProductTag")->get_tags_by_product_id($configurable_id);
        $tags=[];
        foreach ($tag_list as $key=>$tag) {
            $tags[]=$tag["tag"];
        }
        $all_valid_tags=Yii::$container->get("app\models\PgProductTag")->get_tags_url($tags);
        $amazonHelper=$objectManager->get("\Amazon\Core\Helper\Data");
        $amazonpay_config = [
            "secret_key"  => $amazonHelper->getSecretKey(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "access_key"  => $amazonHelper->getAccessKey(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "merchant_id" => $amazonHelper->getMerchantId(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "region"      => $amazonHelper->getRegion(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "sandbox"     => $amazonHelper->isSandboxEnabled(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "client_id"   => $amazonHelper->getClientId(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default")
        ];
        $simple_urls=MagentoProductHelper::getFrameSimpleUrls($category_id,$currentSimpleData,$additional_info,$frame_group,$param_tag);
        //重置$currentSimpleData变量中产品价格，如果是蓝光，增加蓝光价格
        if ($lens_type=="blue_non_rx") {
            //如果是蓝光
            $currentSimpleData["price"]=$simple_urls["blue_non_rx"]["price"];
            $currentSimpleData["sales_price"]=$simple_urls["blue_non_rx"]["sales_price"];
            $currentSimpleData["dest_name"]=$simple_urls["blue_non_rx"]["dest_name"];
            $currentSimpleData["dest_sku"]=$simple_urls["blue_non_rx"]["dest_sku"];
        }
        if (!isset($simple_urls[$lens_type])) {
            return null;
        }
        $currentSimpleData["simple_url"]=$simple_urls[$lens_type];

        //太阳镜、普通眼镜判断
        $eyeglasses_category_ids = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["eyeglasses_category_ids"];
        $sunglasses_category_ids = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["sunglasses_category_ids"];
        $sunglasses_kids_category_ids = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["sunglasses_kids_category_ids"];
        if(in_array($category_id, $eyeglasses_category_ids)) {
            $catetory_type = 'eyeglasses';
        } else if(in_array($category_id, $sunglasses_category_ids)) {
            $catetory_type = 'sunglasses';
        }
        if (in_array($category_id,$sunglasses_kids_category_ids)) {
            //儿童太阳镜
            $is_sunglasses_kids=true;
        } else {
            $is_sunglasses_kids=false;
        }
        //计算当前最终价格
        if($lens_type=="blue_non_rx") {
            $sales_price=number_format($simple_urls["blue_non_rx"]["sales_price"],2);
        } else {
            $sales_price=number_format($currentSimpleData["sales_price"],2);
        }
        $price_included=PgLensType::getPriceIncluded($lens_type,$frame_group,$currentSimpleData["is_clip_on"]);
        $dest_name=UrlRewrite::getByPath($simple_url)->getAttributes()["dest_name"];
        $dest_sku=UrlRewrite::getByPath($simple_url)->getAttributes()["dest_sku"];
        //PDP tags
        $tags=MagentoProductHelper::getConfigurableTags($configurable_id);
        //夹片处理
        $clipons=[];
        if (($currentSimpleData["is_clip_on"]==1 || $configableData["is_clip_on"]==1) && !empty($currentSimpleData["clipon"])) {
            $clipon_skus=explode(",",$currentSimpleData["clipon"]);
            foreach ($clipon_skus as $clipon_sku) {
                $clipon_product=MagentoProductHelper::getProductObjectBySku($clipon_sku)["data"];
                $used_simples=MagentoProductHelper::getUsedProductObjects($clipon_product["entity_id"]);
                foreach ($used_simples as $used_simple) {
                    $clipon=[
                        "configurable_id"=>$clipon_product["entity_id"],
                        "simple_id"=>$used_simple["entity_id"],
                        "sku"=>$used_simple["sku"],
                        "name"=>$used_simple["name"],
                        "price"=>$used_simple["price"],
                        "special_price"=>$used_simple["special_price"],
                        "special_from_date"=>$used_simple["special_from_date"],
                        "special_to_date"=>$used_simple["special_to_date"],
                        "sales_price"=>$used_simple["sales_price"],
                        "thumbnail"=>$used_simple["thumbnail"],
                        "color"=>$used_simple["color"],
                        "color_value"=>$used_simple["color_value"],
                    ];
                    $clipons[]=$clipon;
                }
            }
        }
        //普通眼镜与太阳镜互通
        $currentSimpleData["relate_product"]=MagentoProductHelper::get_relate_product($category_id,$currentSimpleData,$frame_group,$param_tag,$lens_type,$additional_info);
        //gtm
        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $gtm_value = $_scopeConfig->getValue('googletagmanager/general/account',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $collection_enable = $_scopeConfig->getValue('config/general/scraper_enable',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $collection_api_url = $_scopeConfig->getValue('config/general/scraper_address_web',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        $ret=[
            "simple_id"=>$simple_id,
            "lens_type"=>$lens_type,
            "frame_group"=>$frame_group,
            "configurable_id"=>$configurable_id,
            "param_tag"=>$param_tag,
            "is_php"=>$param_tag=="grant"? 1:0,
            "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
            "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
            "simple_urls"=>$simple_urls,
            "simple_url"=>Yii::$container->get("app\models\UrlRewrite")->getSimpleFrameUrl($simple_id,$lens_type,$entity_type),
            "dest_name"=>$dest_name,
            "dest_sku"=>$dest_sku,
            "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
            "support_share_and_save"=>$support_share_and_save,
            "is_share_and_save"=> ($support_share_and_save && $_is_shared) ? true:false,
            "_is_shared"=>$_is_shared,
            "_share_price"=>$_share_price,
            "_final_price"=>$_final_price,
            "sales_price"=>$sales_price,
            "currentSimpleData"=>$currentSimpleData,
            "currentSimpleJson"=>$currentSimpleJson,
            "configableData"=>$configableData,
            "simpleDatas"=>$simpleDatas,
            "product"=>$simple_product,
            "images"=>$images,
            "reviews"=>$reviews,
            "review_ids_str"=>$review_ids_str,
            "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
            "tags"=>$all_valid_tags,
            "amazonpay_config"=>$amazonpay_config,
            "blocker_product_info"=>$blocker_product_info,
            "category_id"=>$category_id,
            "catetory_type"=>$catetory_type,
            "is_sunglasses_kids"=>$is_sunglasses_kids,
            "price_included"=>$price_included,
            "tags"=>$tags,
            "clipons"=>$clipons,
            "gtm_value"=>$gtm_value,
            "collection_enable"=>$collection_enable,
            "collection_api_url"=>$collection_api_url
        ];
        $t2=\app\components\helper\TimezoneHelper::msectime();
//        echo sprintf("FrameHelper.renderHtml.t2-t1=%s".PHP_EOL,$t2-$t1);
        return $ret;
    }
}