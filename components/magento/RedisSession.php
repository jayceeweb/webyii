<?php
namespace app\components\magento;

use Yii;

class RedisSession {
    private function _decodeData($data) {
        switch (substr($data,0,4)) {
            // asking the data which library it uses allows for transparent changes of libraries
            case ':sn:': $data = snappy_uncompress(substr($data,4)); break;
            case ':lz:': $data = lzf_decompress(substr($data,4)); break;
            case ':l4:': $data = lz4_uncompress(substr($data,4)); break;
            case ':gz:': $data = gzuncompress(substr($data,4)); break;
        }
        return $data;
    }

    private static function unserialize_php($session_data) {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            if (!strstr(substr($session_data, $offset), "|")) {
                throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
            }
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }

    private function unserialize_session($val) {
        $result = array();

        // prefixing with semicolon to make it easier to write the regular expression
        $val = ';' . $val;

        // regularexpression to find the keys
        $keyreg = '/;([^|{}"]+)\|/';

        // find all keys
        $matches = array();
        preg_match_all($keyreg, $val, $matches);

        // only go further if we found some keys
        if (isset($matches[1])) {
            $keys = $matches[1];

            // find the values by splitting the input on the key regular expression
            $values = preg_split($keyreg, $val);

            // unshift the first value since it's always empty (due to our semicolon prefix)
            if (count($values) > 1) {
                array_shift($values);
            }

            // combine the $keys and $values
            $result = array_combine($keys, $values);
        }

        return $result;
    }

    private function unserializesession( $data )
    {
        if(  strlen( $data) == 0)
        {
            return array();
        }

        // match all the session keys and offsets
        preg_match_all('/(^|;|\})([a-zA-Z0-9_]+)\|/i', $data, $matchesarray, PREG_OFFSET_CAPTURE);

        $returnArray = array();

        $lastOffset = null;
        $currentKey = '';
        foreach ( $matchesarray[2] as $value )
        {
            $offset = $value[1];
            if(!is_null( $lastOffset))
            {
                $valueText = substr($data, $lastOffset, $offset - $lastOffset );
                $returnArray[$currentKey] = unserialize($valueText);
            }
            $currentKey = $value[0];

            $lastOffset = $offset + strlen( $currentKey )+1;
        }

        $valueText = substr($data, $lastOffset );
        $returnArray[$currentKey] = unserialize($valueText);

        return $returnArray;
    }

    private function decode_session($sess_string)
    {
        // save current session data
        //   and flush $_SESSION array
        $old = $_SESSION;
        $_SESSION = array();

        // try to decode passed string
        $ret = session_decode($sess_string);
        if (!$ret) {
            // if passed string is not session data,
            //   retrieve saved (old) session data
            //   and return false
            $_SESSION = array();
            $_SESSION = $old;

            return false;
        }

        // save decoded session data to sess_array
        //   and flush $_SESSION array
        $sess_array = $_SESSION;
        $_SESSION = array();

        // restore old session data
        $_SESSION = $old;

        // return decoded session data
        return $sess_array;
    }

    public function serialize_php($session) {
        $ret="";
        foreach ($session as $key=>$value) {
            $ret=$ret.$key."|".serialize($value).";";
        }
        return $ret;
    }

    public function read($sessionId)
    {
        $sessionId="sess_".$sessionId;
        $data=Yii::$app->magento_redis_session->HMGET($sessionId,"data");
        if (count($data)==1) {
            $sessionData=$data[0];
            $d=$this->_decodeData($sessionData);
            try {
                return $this->unserialize_php($d);
            } catch (\Exception $e) {
                try {
                    return $this->unserialize_session($d);
                } catch (\Exception $e2) {
                    try {
                        return $this->unserializesession($d);
                    } catch (\Exception $e3) {
                        try {
                            return $this->decode_session($d);
                        } catch (\Exception $e4) {
                            return [];
                        }
                    }
                }
            }
        }
        return [];
    }
}