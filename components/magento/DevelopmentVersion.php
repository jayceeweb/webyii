<?php
namespace app\components\magento;


class DevelopmentVersion
{
    public static function getVersion() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $deploymentVersion=$objectManager->get("\Magento\Framework\App\View\Deployment\Version");
        $version=sprintf('version%s',$deploymentVersion->getValue());
        return $version;
    }
}