<?php
namespace app\components\magento;

use Yii;

class MagentoRedis {
    public function load($identifier) {
        $configDirPath=Yii::$app->params["magento_root_path"]."/app/etc/";
        $idPrefix = substr(md5($configDirPath), 0, 3) . '_';
        $key="zc:k:".$idPrefix.$identifier;
        $datas=Yii::$app->magento_redis->hgetall($key);

        $params=[];
        $index=0;
        $param_name="";
        foreach ($datas as $key=>$val) {
            if ($index % 2 == 0) {
                $param_name=$val;
                $params[$param_name]="";
            } else {
                $params[$param_name]=$val;
            }
            $index=$index+1;
        }
        return $params;
    }

    public function parseParams($params) {
        return [
            "tags"=>$params["t"],
            "data"=>$params["d"],
            "specificLifetime"=>$params["m"],
        ];
    }
}