<?php
namespace app\components\magento;

use Yii;
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

class MagentoAutoload
{
    public static $instance=null;

    public function loadMagentoFrameword() {
        if (static::$instance==null) {
            static::$instance=new MagentoAutoload();
            $autoload_php_file=Yii::$app->params["magento_root_path"]."/app/bootstrap.php";
            require_once $autoload_php_file;
            $params = $_SERVER;
            $params[Bootstrap::INIT_PARAM_FILESYSTEM_DIR_PATHS] = [
                DirectoryList::PUB => [DirectoryList::URL_PATH => ''],
                DirectoryList::MEDIA => [DirectoryList::URL_PATH => 'media'],
                DirectoryList::STATIC_VIEW => [DirectoryList::URL_PATH => 'static'],
                DirectoryList::UPLOAD => [DirectoryList::URL_PATH => 'media/upload'],
            ];
            $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $params);
            $app = $bootstrap->createApplication('Magento\Framework\App\Http');
            $bootstrap->runAsYii($app);

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            try {
                if (empty($objectManager->get("\Magento\Framework\App\State")->getAreaCode())) {
                    $objectManager->get("\Magento\Framework\App\State")->setAreaCode('frontend');
                }
            } catch (\Exception $e) {
                $objectManager->get("\Magento\Framework\App\State")->setAreaCode('frontend');
            }
            //初始化用户登录session
            $event=$objectManager->get("\Magento\Framework\Event");
            $request=$objectManager->create('\Magento\Framework\App\RequestInterface');
            $event->setData("request",$request);
            $observer=$objectManager->get("\Magento\Framework\Event\Observer");
            $observer->setEvent($event);
            $objectManager->get("Pg\PersistentExtend\Observer\EmulateCustomerObserver")->execute($observer);
        }
    }
}