<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property string $profile_id Profile ID
 * @property int $customer_id Customer ID
 * @property string|null $nickname Nick Name
 * @property string|null $relationship Relationship
 * @property string $created_at Created At
 * @property string|null $updated_at Updated At
 * @property string|null $last_order_date Last Order Date
 * @property int|null $default_prescription Default Prescription
 * @property int|null $is_active is Active
 * @property int|null $birthyear Birthyear
 * @property int|null $gender Gender
 * @property int|null $age_group Age Group
 * @property string|null $portrait Portrait
 * @property int|null $filled
 *
 * @property OrderImage[] $orderImages
 * @property PrescriptionEntity[] $prescriptionEntities
 * @property CustomerEntity $customer
 * @property ProfileImage[] $profileImages
 * @property SalesOrderItem[] $salesOrderItems
 * @property Wishlist[] $wishlists
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profile_id'], 'required'],
            [['customer_id', 'default_prescription', 'is_active', 'birthyear', 'gender', 'age_group', 'filled'], 'integer'],
            [['created_at', 'updated_at', 'last_order_date'], 'safe'],
            [['portrait'], 'string'],
            [['profile_id'], 'string', 'max' => 15],
            [['nickname', 'relationship'], 'string', 'max' => 255],
            [['profile_id'], 'unique'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerEntity::className(), 'targetAttribute' => ['customer_id' => 'entity_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'profile_id' => 'Profile ID',
            'customer_id' => 'Customer ID',
            'nickname' => 'Nick Name',
            'relationship' => 'Relationship',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'last_order_date' => 'Last Order Date',
            'default_prescription' => 'Default Prescription',
            'is_active' => 'is Active',
            'birthyear' => 'Birthyear',
            'gender' => 'Gender',
            'age_group' => 'Age Group',
            'portrait' => 'Portrait',
            'filled' => 'Filled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderImages()
    {
        return $this->hasMany(OrderImage::className(), ['profile_id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptionEntities()
    {
        return $this->hasMany(PrescriptionEntity::className(), ['parent_id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerEntity::className(), ['entity_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileImages()
    {
        return $this->hasMany(ProfileImage::className(), ['profile_id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrderItems()
    {
        return $this->hasMany(SalesOrderItem::className(), ['profile_id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWishlists()
    {
        return $this->hasMany(Wishlist::className(), ['profile_id' => 'profile_id']);
    }
}