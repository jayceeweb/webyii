<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "url_rewrite".
 *
 * @property int $id Customer Insurance Id
 * @property string $biz_type Ratio type
 * @property string $biz_connect Email zip/code
 * @property int|0 $cart_ratio Cart ratio 70%
 * @property int|0 $freight_ratio Freight ratio 70%
 * @property int|0 compel Is it mandatory to buy insurance
 *
 * @property CatalogUrlRewriteProductCategory[] $catalogUrlRewriteProductCategories
 */
class CustomerInsurance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_insurance';
    }


    //孙进超 20/7/2
    public static function getListSQL() {
        $sql="select * from customer_insurance";
        return $sql;
    }

    //获取customer_insurance数据并分页
    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            if(!empty($v)){
                if($c == 'biz_type'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if($c == 'biz_connect'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'cart_ratio_start'){
                    $query->andWhere(sprintf("cart_ratio >= :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'cart_ratio_end'){
                    $query->andWhere(sprintf("cart_ratio <= :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'freight_ratio_start'){
                    $query->andWhere(sprintf("freight_ratio >= :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'freight_ratio_end'){
                    $query->andWhere(sprintf("freight_ratio <= :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'compel'){
                    if($v == 2){
                        $v = 0;
                    }
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }
    //获取customer_insurance数据
    public static function getListCount($condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if(!empty($v)){
                if($c == 'biz_type'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if($c == 'biz_connect'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'cart_ratio'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if($c == 'freight_ratio'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if($c == 'compel'){
                    if($v == 2){
                        $v = 0;
                    }
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $data=$query->count();
        return $data;
    }
    public static function addRatio($d){
        $keys = "(";
        $values = "(";
        foreach ($d as $key => $value) {
            $keys .= "`$key`,";
            $values .= "'$value',";
        }
        $keys = rtrim($keys,",");
        $values = rtrim($values,",");
        $keys .= ")";
        $values .= ")";
        $sql = "insert into customer_insurance $keys values $values";
        Yii::$app->db->createCommand($sql)->execute();
    }

    public static function updateRatio($d){
        $id = $d["id"];
        unset($d["id"]);
        $condition = "";
        foreach ($d as $key => $value) {
            $condition .= "$key='$value',";
        }
        $condition = rtrim($condition,",");
        $sql = "update customer_insurance set $condition where id = $id";
        Yii::$app->db->createCommand($sql)->execute();
    }

    public static function deleteRatio($id){
        $sql = "delete from customer_insurance where id = $id";
        Yii::$app->db->createCommand($sql)->execute();
    }
    public static function getListType(){
        $sql = "select DISTINCT(biz_type) from customer_insurance";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $data;
    }
}
