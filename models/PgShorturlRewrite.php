<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_shorturl_rewrite".
 *
 * @property int $id
 * @property int $rewrite_type 0:直接跳转;1:正则跳转
 * @property string|null $ori_uri
 * @property string|null $rewrite_url
 * @property int $is_active 0:未启用;1:启用
 * @property string|null $rewrite_code
 * @property string|null $log_format
 * @property string $create_at
 * @property string $update_at
 * @property string|null $create_admin_id 创建者名称
 * @property string|null $update_admin_id 修改者名称
 */
class PgShorturlRewrite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_shorturl_rewrite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rewrite_type', 'is_active'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['ori_uri'], 'string', 'max' => 128],
            [['rewrite_url'], 'string', 'max' => 256],
            [['rewrite_code', 'create_admin_id', 'update_admin_id'], 'string', 'max' => 32],
            [['log_format'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rewrite_type' => 'Rewrite Type',
            'ori_uri' => 'Ori Uri',
            'rewrite_url' => 'Rewrite Url',
            'is_active' => 'Is Active',
            'rewrite_code' => 'Rewrite Code',
            'log_format' => 'Log Format',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'create_admin_id' => 'Create Admin ID',
            'update_admin_id' => 'Update Admin ID',
        ];
    }

    //孙进超 20/9/1
    public static function getListSQL() {
        $sql="select * FROM `pg_shorturl_rewrite`";
        return $sql;
    }

    //获取shorturl_rewrite数据并分页
    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'is_active'){
                    $query->andWhere(sprintf("is_active = :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'rewrite_type'){
                    $query->andWhere(sprintf("rewrite_type = :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'rewrite_code'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'ori_uri'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if ($c == 'rewrite_url'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
            }
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }
    //获取shorturl_rewrite数据数量
    public static function getListCount($condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'is_active'){
                    $query->andWhere(sprintf("is_active = :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'rewrite_type'){
                    $query->andWhere(sprintf("rewrite_type = :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'rewrite_code'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'ori_uri'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if ($c == 'rewrite_url'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
            }
        }
        $query->addParams($params);
        $data=$query->count();
        return $data;
    }

//    数据修改
    public static function active($id,$is_active){
        if($is_active == 1){
            $is_active = 0;
        }else{
            $is_active = 1;
        }
        $update_name = Yii::$app->admin->getIdentity()->getAttribute('username');
        $sql = "update pg_shorturl_rewrite set is_active = $is_active , update_admin_id = '$update_name' where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
    }
//    数据修改
    public static function deleteByid($id){
        $sql = "delete from pg_shorturl_rewrite where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
    }

    public static function fetchOne($id){
        $sql = "select id,rewrite_type,ori_uri,rewrite_url,is_active,rewrite_code,log_format from pg_shorturl_rewrite where id = $id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $data;
    }


    //    数据修改
    public static function toupdate($data=[]){
        $id = $data['id'];
        unset($data['id']);
        $vals = '';
        foreach ($data as $key => $val){
            $vals .= '`'.$key.'`=\''.$val.'\',';
        }
        $vals = rtrim($vals,',');
        $sql = "update pg_shorturl_rewrite set $vals where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
//    数据添加
    public static function toinsert($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into pg_shorturl_rewrite($keys) values ($vals)";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
}