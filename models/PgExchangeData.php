<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_exchange_data".
 *
 * @property int $exchange_id
 * @property string|null $exchange_code
 * @property int|null $customer_id
 * @property string|null $data_content
 * @property string $created_at
 */
class PgExchangeData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_exchange_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['data_content'], 'string'],
            [['created_at'], 'safe'],
            [['exchange_code'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'exchange_id' => 'Exchange ID',
            'exchange_code' => 'Exchange Code',
            'customer_id' => 'Customer ID',
            'data_content' => 'Data Content',
            'created_at' => 'Created At',
        ];
    }
}