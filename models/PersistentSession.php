<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "persistent_session".
 *
 * @property int $persistent_id Session id
 * @property string $key Unique cookie key
 * @property int|null $customer_id Customer id
 * @property int $website_id Website ID
 * @property string|null $info Session Data
 * @property string $updated_at Updated At
 * @property int|null $is_secure Is Secure
 *
 * @property CustomerEntity $customer
 * @property StoreWebsite $website
 */
class PersistentSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'persistent_session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['customer_id', 'website_id', 'is_secure'], 'integer'],
            [['info'], 'string'],
            [['updated_at'], 'safe'],
            [['key'], 'string', 'max' => 50],
            [['key'], 'unique'],
            [['customer_id'], 'unique'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerEntity::className(), 'targetAttribute' => ['customer_id' => 'entity_id']],
            [['website_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreWebsite::className(), 'targetAttribute' => ['website_id' => 'website_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'persistent_id' => 'Persistent ID',
            'key' => 'Key',
            'customer_id' => 'Customer ID',
            'website_id' => 'Website ID',
            'info' => 'Info',
            'updated_at' => 'Updated At',
            'is_secure' => 'Is Secure',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerEntity::className(), ['entity_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite()
    {
        return $this->hasOne(StoreWebsite::className(), ['website_id' => 'website_id']);
    }

    public function load_by_key($key){
        return \app\components\helper\DbHelper::getSlaveDb()->createCommand("SELECT `persistent_session`.*, `customer`.* FROM `persistent_session`
 INNER JOIN `customer_entity` AS `customer` ON customer.entity_id = persistent_session.customer_id 
 WHERE (`persistent_session`.`key`=:key) 
 AND (persistent_session.updated_at >= :updated_at)",["key"=>$key,"updated_at"=>date("Y-m-d H:i:s", strtotime("-1 year"))])->queryOne();
    }

    public function loadByCookieKey($key = null)
    {
        $ret=null;
        if (null === $key) {
            $key = $_COOKIE["persistent_shopping_cart"];
        }
        if ($key) {
            $ret=$this->load_by_key($key);
        }
        return $ret;
    }
}
