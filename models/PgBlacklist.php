<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_blacklist".
 *
 * @property int $id
 * @property string $type_id all_email
 * @property string|null $v_value
 * @property string $created_at
 * @property string|null $created_reason
 */
class PgBlacklist extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_blacklist';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id'], 'required'],
            [['created_at','start_at','end_at'], 'safe'],
            [['type_id'], 'string', 'max' => 32],
            [['email', 'created_reason'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'email' => 'Email',
            'created_at' => 'Created At',
            'created_reason' => 'Created Reason',
        ];
    }

    public static function getListCount($condition=[]) {
        $query = (new \yii\db\Query())->select('*')->from(['main' => "pg_blacklist"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if (!empty($v)) {
                if ($c=="email") {
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                } else {
                    $query->andWhere(sprintf("%s=:%s)",$c,$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $cnt=$query->count();
        return $cnt;
    }

    public static function getList($page=1,$pagecount=10,$condition=[]) {
        $query = (new \yii\db\Query())->select('*')->from(['main' => "pg_blacklist"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            if ($c=="email") {
                $query->andWhere(sprintf("%s like :%s",$c,$c));
                $params[":".$c]="%%".$v."%%";
            } else {
                $query->andWhere(sprintf("%s=:%s)",$c,$c));
                $params[":".$c]=$v;
            }
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }
}