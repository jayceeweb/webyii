<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_shouji_tongju".
 *
 * @property int $entity_id
 * @property int|null $admin_id
 * @property int|null $prescription_id
 * @property int|null $frame_sku
 * @property float|null $left_tonggao
 * @property float|null $left_tongkuan
 * @property float|null $right_tonggao
 * @property float|null $right_tongkuan
 * @property string $created_at
 */
class PgShoujiTongju extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_shouji_tongju';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_id', 'prescription_id'], 'integer'],
            [['frame_sku'], 'required'],
            [['left_tonggao', 'left_tongkuan', 'right_tonggao', 'right_tongkuan'], 'number'],
            [['created_at'], 'safe'],
            [['frame_sku'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'admin_id' => 'Admin ID',
            'prescription_id' => 'Prescription ID',
            'frame_sku' => 'Frame Sku',
            'left_tonggao' => 'Left Tonggao',
            'left_tongkuan' => 'Left Tongkuan',
            'right_tonggao' => 'Right Tonggao',
            'right_tongkuan' => 'Right Tongkuan',
            'created_at' => 'Created At',
        ];
    }


    public static function getListCount($condition=[]) {
        $cnt=static::getListFilter($condition)->count();
        return $cnt;
    }

    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $query=static::getListFilter($condition)
            ->offset(($page -1) * $pagecount)
            ->orderBy(["entity_id"=>SORT_DESC])
            ->limit($pagecount);
        $data=$query->all();
        return $data;
    }

    public static function getListFilter($condition=[]) {
        $query=(new \yii\db\Query())->select(['p.*','c.prescription_name'])->from(["p"=>"pg_shouji_tongju"])
            ->innerJoin(["c"=>"pg_shouji_prescription"],"c.entity_id=p.`prescription_id`");
        $params=[];
        foreach ($condition as $c=>$v) {
            if ($c == 'sku' && !empty($v)) {
                $query->andWhere(sprintf("p.frame_sku like '%%s%'", $v));
            }
        }
        return $query;
    }
}