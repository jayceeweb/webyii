<?php
namespace app\models;

use app\models\PgScore;
use Yii;
use app\models\SalesOrder;
use Maropost\Api\Contacts;
use GuzzleHttp\Client as GuzzleClient;

/**
 * This is the model class for table "pg_score_bill".
 *
 * @property int $id
 * @property int $customer_id
 * @property string|null $type_id so/login/register
 * @property string|null $src_type sales_order
 * @property int $src_id
 * @property int $operation
 * @property int $score
 * @property string|null $additional_data
 * @property string|null $message
 * @property int $score_status 0:未生效;1:生效
 * @property string $created_at
 * @property string $active_time
 * @property string $bill_time
 * @property string $note
 * @property int $review
 * @property string $review_user
 * @property string $review_datetime
 * @property int $remake_code
 * @property string $delievered_time
 * @property string $is_deleted
 * @property string $deleted_datetime
 */
class PgScoreBill extends \yii\db\ActiveRecord
{
    public static $has_add_sales_order_stmt=false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_score_bill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'src_id', 'score_status','score','operation','review','remake_code','is_deleted'], 'integer'],
            [['additional_data','note','review_user'], 'string'],
            [['score_status'], 'required'],
            [['created_at', 'active_time','bill_time','review_datetime','delievered_time','deleted_datetime'], 'safe'],
            [['type_id', 'src_type'], 'string', 'max' => 32],
            [['message'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'type_id' => 'Type ID',
            'src_type' => 'Src Type',
            'src_id' => 'Src ID',
            'operation'=>'operation',
            'score' => 'Score',
            'additional_data' => 'Additional Data',
            'message' => 'Message',
            'score_status' => 'Score Status',
            'created_at' => 'Created At',
            'active_time' => 'Active Time',
            'bill_time' => 'Bill Time',
            'note' => 'Note',
            'review' => 'Review',
            'remake_code' => 'Remake Code',
            'review_user' => 'Review User',
            'review_datetime' => 'Review Datetime',
            'delievered_time' => 'Delievered Time',
            'is_deleted'=>'Is Deleted',
            'deleted_datetime'=>'Deleted Datetime'
        ];
    }

    public function beforeSave($insert)
    {
        $ret=parent::beforeSave($insert);
        if ($ret) {
            if (!empty($this->src_type) && !empty($this->src_id)) {
                if ($this->src_type=="sales_order") {
                    $sql=sprintf("select count(0) as cnt from sales_order where entity_id=%s",$this->src_id);
                    $cnt=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
                } else if ($this->src_type=="customer_entity") {
                    $sql=sprintf("select count(0) as cnt from customer_entity where entity_id=%s",$this->src_id);
                    $cnt=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
                } else if ($this->src_type=="review") {
                    $cnt=1;
                } else if ($this->src_type=="activity") {
                    $cnt=1;
                } else {
                    $cnt=0;
                }
                if ($cnt>0) {
                    return true;
                }
            }
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $ret=parent::afterSave($insert,$changedAttributes);
        if (!empty($this->customer_id)) {
            static::calcCustomerScore($this->customer_id);
        } else {
            return false;
        }
        return $ret;
    }

    public function checkSaveError() {
        $ret=[];
        if (!empty($this->customer_id)) {
            if (empty($this->id)) {
                //新增
                $sql=sprintf("SELECT * FROM pg_score WHERE entity_id=%s",$this->customer_id);
                $row=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                if ($row["total_score"]+$this->score<0 || $row["all_score"]+$this->score<0) {
                    $ret=["code"=>-2,"message"=>"total score is less the 0"];
                } else {
                    $ret=["code"=>0,"message"=>"success"];
                }
            }
        } else {
            $ret=["code"=>-1,"message"=>"customer_id is null"];
        }
        return $ret;
    }

    public static function getListSQL() {
//        $sql="select sal.`discount_amount`,bill.*,cus.`email`,cugroup.`customer_group_code` as group_id from `pg_score_bill` bill LEFT JOIN customer_entity cus on bill.customer_id = cus.`entity_id` LEFT JOIN sales_order sal on bill.src_id=sal.entity_id LEFT JOIN customer_group cugroup on cus.`group_id` = cugroup.`customer_group_id`";
        $sql="select por.remake_order,sal.`discount_amount`,sal.increment_id,bill.*,cus.`email`,cugroup.`customer_group_code` as group_id,
sal.state,cus.enabled_score,cus.confirm_email,cus.confirm_email_confirm,cus.confirm_date,
ps.recommended,ps.permissions,ps.recommend_date 
from `pg_score_bill` bill
LEFT JOIN customer_entity cus on bill.customer_id = cus.`entity_id` 
LEFT JOIN sales_order sal on bill.src_id=sal.entity_id 
LEFT JOIN customer_group cugroup on cus.`group_id` = cugroup.`customer_group_id`  
LEFT JOIN pg_order_remake as por on sal.increment_id = por.order_number  
INNER JOIN pg_score ps on ps.entity_id=bill.customer_id 
GROUP BY bill.id";
        return $sql;
    }

    public static function addListSalesOrderStmt(\yii\db\Query $query){
        $can_add=true;
        foreach ($query->join as $j) {
            if (key($j[1])=="sal") {
                $can_add=false;
            }
        }
        if ($can_add) {
            $query->addSelect(['sal.`discount_amount`','sal.increment_id','sal.state','sal.coupon_code']);
            $query->innerJoin(["sal"=>"sales_order"],"bill.src_id=sal.entity_id and bill.src_type='sales_order'");
        }
    }

    public static function getListFilter($condition=[]) {
        $query=(new \yii\db\Query())->select([
            'bill.*',
            'cugroup.`customer_group_code` as group_id',
            'cus.enabled_score','cus.confirm_email','cus.confirm_email_confirm','cus.confirm_date','cus.`email`',
            'ps.recommended','ps.permissions','ps.recommend_date'
        ])->from(["bill"=>"pg_score_bill"])
            ->innerJoin(["cus"=>"customer_entity"],"bill.customer_id = cus.`entity_id`")
            ->innerJoin(["cugroup"=>"customer_group"],"cus.`group_id` = cugroup.`customer_group_id`")
            ->innerJoin(["ps"=>"pg_score"],"ps.entity_id=bill.customer_id");
        $params=[];
        foreach ($condition as $c=>$v) {
            if ($c == 'email' && !empty($v)) {
                $query->andWhere(sprintf("cus.email = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'src_type' && !empty($v)) {
                $query->andWhere(sprintf("bill.src_type = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'src_id' && !empty($v)) {
                static::addListSalesOrderStmt($query);
                $query->andWhere(sprintf("bill.src_id = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'discount_amount' && ($v == "1" || $v == "2")) {
                static::addListSalesOrderStmt($query);
                if ($v == "1") {
                    $query->andWhere(sprintf("sal.`discount_amount` < :%s", $c));
                    $params[":" . $c] = 0;
                } else if ($v == "2") {
                    $query->andWhere(sprintf("sal.`discount_amount` = :%s", $c));
                    $params[":" . $c] = 0;
                }
            }
            if ($c == 'expect' && $v == 1) {
                $c = 'score';
                $query->andWhere(sprintf("bill.score != :%s", $c));
                $params[":" . $c] = 0;
            }
            if ($c == 'review' && $v == 1) {
                $query->andWhere(sprintf("bill.review = :%s", $c));
                $params[":" . $c] = 1;
            } else if ($c == 'review' && $v == 2) {
                $query->andWhere(sprintf("bill.review != :%s", $c));
                $params[":" . $c] = 1;
            }
            if ($c == 'is_remake' && ($v == "1" || $v == "2")) {
                static::addListSalesOrderStmt($query);
                if ($v == "1") {
                    $query->andWhere(sprintf("sal.entity_id in (select so.entity_id from pg_order_remake por inner join sales_order so on so.increment_id=por.order_number) "));
                } else if ($v == "2") {
                    $query->andWhere(sprintf("sal.entity_id not in (select so.entity_id from pg_order_remake por inner join sales_order so on so.increment_id=por.order_number) "));
                }
            }
            if ($c=="remake_order_number" && trim($v)!="") {
                static::addListSalesOrderStmt($query);
                $query->andWhere(sprintf("sal.increment_id in (select order_number from pg_order_remake where remake_order=:remake_order_number)"));
                $params[":remake_order_number"] = $v;
            }
            if ($c == 'score_status' && $v != "") {
                $query->andWhere(sprintf("bill.score_status = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'active_start_time' && $v != "") {
                $query->andWhere(sprintf("bill.active_time >= :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'active_end_time' && $v != "") {
                $query->andWhere(sprintf("bill.active_time <= :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'start_bill_time' && $v != "") {
                $query->andWhere(sprintf("bill.bill_time >= :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'end_bill_time' && $v != "") {
                $query->andWhere(sprintf("bill.bill_time <= :%s", $c));
                $params[":" . $c] = $v;
            }
            if (($c == 'is_delievered') && ($v == "0")) {
                $query->andWhere(sprintf("bill.delievered_time is null", $c));
            } else if ($c == 'is_delievered' && $v == "1") {
                $query->andWhere(sprintf("bill.delievered_time is not null", $c));
            }
            if ($c == 'start_delievered_time' && $v != "") {
                $query->andWhere(sprintf("bill.delievered_time >= :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'end_delievered_time' && $v != "") {
                $query->andWhere(sprintf("bill.delievered_time <= :%s", $c));
                $params[":" . $c] = $v;
            }
            if($c == 'customer_id' && $v!= ""){
                $query->andWhere(sprintf("bill.customer_id = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'is_recommend' && ($v == "0" || $v=="1")) {
                $query->andWhere(sprintf("ps.recommended = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'is_email_confirm' && ($v == "0" || $v=="1")) {
                $query->andWhere(sprintf("cus.confirm_email_confirm = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'is_enabled_score' && ($v == "0" || $v=="1")) {
                $query->andWhere(sprintf("cus.enabled_score = :%s", $c));
                $params[":" . $c] = $v;
            }
            if ($c == 'has_coupon_code' && ($v == "0" || $v == "1")) {
                static::addListSalesOrderStmt($query);
                if ($v == "0") {
                    $query->andWhere(sprintf("sal.coupon_code is null"));
                }
                if ($v == "1") {
                    $query->andWhere(sprintf("sal.coupon_code is not null"));
                }
            }
            if ($c == 'coupon_code' && trim($v)!="") {
                static::addListSalesOrderStmt($query);
                $v = strtolower($v);
                $query->andWhere(sprintf("lower(sal.coupon_code) like :coupon_code"));
                $params[":coupon_code"] = "%%".$v."%%";
            }
            if ($c == 'is_deleted') {
                if($v == 1){
                    $query->andWhere(sprintf("is_deleted = :is_deleted"));
                    $params[":is_deleted"] = $v;
                }else if($v == 2){
                    $query->andWhere(sprintf("is_deleted = 0"));
                }
            }
        }
        $query->addParams($params);
        return $query;
    }

    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $query=static::getListFilter($condition)
            ->offset(($page -1) * $pagecount)
            ->orderBy(["confirm_date"=>SORT_DESC,"id"=>SORT_DESC])
            ->limit($pagecount);
        $data=$query->all();
        return $data;
    }

    public static function getExcelList($condition=[]) {
        $query=static::getListFilter($condition)
            ->orderBy(["confirm_date"=>SORT_DESC,"id"=>SORT_DESC]);
        $data=$query->all();
        return $data;
    }

    public static function getListCount($condition=[]) {
        $cnt=static::getListFilter($condition)->count();
        return $cnt;
    }

    public static function getIncrementId($src_id){
        $so=SalesOrder::findOne(["entity_id"=>$src_id]);
        return $so['increment_id'];
    }

    public static function getSrcObject($src) {
        $src_type=$src["src_type"];
        $src_id=$src["src_id"];
        $obj=["src_type"=>"sales_order","src_obj"=>["increment_id"=>""]];
        if ($src_type=="sales_order" || $src_type=="sales_point_for") {
            $so=SalesOrder::findOne(["entity_id"=>$src_id]);
            if (!empty($so)) {
                $obj=["src_type"=>"sales_order","src_obj"=>$so->getAttributes()];
            }
        }
        return $obj;
    }

    public static function getSrcType(){
        $sql = "select DISTINCT(src_type) from pg_score_bill";
        $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $data;
    }

    public static function getConfigurableName($src){
        $src = json_decode($src,true);
        if(isset($src['review']['reviews_id'])){
            $reviews_id = $src['review']['reviews_id'];
            $sql = "select configable_id from pg_reviews where reviews_id = $reviews_id";
            $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $product_id = $data['configable_id'];
            $sql = "select `name` from catalog_product_flat_1 where entity_id = $product_id";
            $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            return $data['name'];
        }
        return "";
    }

    public static function updateReview($src_id,$arr){
        $sql = "update pg_score_bill set ";
        foreach ($arr as $key => $value){
            $sql .= '`'.$key.'`=\''.$value.'\',';
        }
        $sql = rtrim($sql,',');
        $sql .= " where id = $src_id";
        $data=Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }

    public static function getReObject($src_id){
        $sql = "select por.remake_order,por.item_id from sales_order as so LEFT JOIN pg_order_remake as por on so.increment_id = por.order_number where so.entity_id = '$src_id'";
        $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $data;
    }

    public static function soScoreActive($src_id) {
        //积分用户
        $sql=sprintf("SELECT customer_id FROM sales_order WHERE entity_id=%s",$src_id);
        $customer_id=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
        $ret = '';
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $now=$localeDate->date()->format('Y-m-d H:i:s');
        $sql = sprintf("select count(0) as count from pg_score_bill where src_id = %s",$src_id);
        $count = \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->queryOne();
        if($count['count'] == 0){
            return $ret;
        }
        //先生效，然后计算总积分
        $active_update_sql=sprintf("update pg_score_bill set score_status=1,active_time=now() WHERE src_type='sales_order' AND src_id=%s AND score_status=0",$src_id);
        \app\components\helper\DbHelper::getMasterDb()->createCommand($active_update_sql)->execute();

        $ret=static::calcCustomerScore($customer_id);

        //add sunjinchao 销售订单积分激活发送邮件
        $scopeConfig=$objectManager->get("\Magento\Framework\App\Config\ScopeConfigInterface");
        $service_url = $scopeConfig->getValue('config/general/sales_order_points_activate_sending_mail',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
        if($service_url == 1){
            $sql = "select increment_id,sales_order.customer_id,sum(score) as score from pg_score_bill INNER JOIN sales_order on sales_order.entity_id = pg_score_bill.src_id and sales_order.entity_id = $src_id and pg_score_bill.src_type='sales_order' GROUP BY increment_id";
            $datas = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $increment_id = $datas['increment_id'];
            $entity_id = $datas['customer_id'];
            $score = $datas['score'];
            $sql = "select group_id from customer_entity where entity_id = $entity_id";
            $group_id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $group_id = $group_id['group_id'];
            $POINTS_CUSTOMER_GROUP_ID=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["POINTS_CUSTOMER_GROUP_ID"];
            if($group_id == $POINTS_CUSTOMER_GROUP_ID){
                //判断是不是积分会员
                if($score > 0){
                    //判断当前订单是否比0多
                    PgScoreBill::SendActiveEmail($increment_id);
                }
            }
        }

        return $ret;
    }

    public static function soScoreActiveByIncrementId($increment_id) {
        $sql=sprintf("select entity_id from sales_order where increment_id='%s'",$increment_id);
        $src_id=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
        if (!empty($src_id)) {
            return static::soScoreActive($src_id);
        } else {
            return -1;
        }
    }

    /**
     * 计算用户积分
     * @param $customer_id
     * @throws \yii\db\Exception
     */
    public static function calcCustomerScore($customer_id) {
        //总积分
        $sql=sprintf("select sum(score) as total_score from pg_score_bill where customer_id=%s and score_status=1 and is_deleted = 0",$customer_id);
        $total_score=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();

        $sql=sprintf("select sum(score) as all_score from pg_score_bill where is_deleted = 0 and customer_id=%s",$customer_id);
        $all_score=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
        if (empty($total_score)) {
            $total_score=0;
        }
        if (empty($all_score)) {
            $all_score=0;
        }
        //计入用户总积分
        $pgScore=PgScore::findOne(["entity_id"=>$customer_id]);
        if (empty($pgScore)) {
            $pgScore=new PgScore();
            $pgScore->entity_id=$customer_id;
            $pgScore->total_score=$total_score;
            $pgScore->all_score=$all_score;
            $pgScore->save();
        } else {
            $pgScore->total_score=round($total_score);
            $pgScore->all_score=$all_score;
            $pgScore->save();
        }
        return 0;
    }

    public static function soReturnScoreActive($id,$src_id) {
        //积分用户
        $sql=sprintf("SELECT customer_id FROM sales_order WHERE entity_id=%s",$src_id);
        $customer_id=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();

        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $now=$localeDate->date()->format('Y-m-d H:i:s');

        //先生效所有此订单的赠送积分，然后计算总积分
        $active_update_sql=sprintf("update pg_score_bill set score_status=1,active_time='$now' WHERE src_type='sales_order' and src_id=%s AND score_status=0",$src_id);
        Yii::$app->db->createCommand($active_update_sql)->execute();

        $ret=static::calcCustomerScore($customer_id);
    }

    public static function customerScoreActive($id) {
        //积分用户
        $sql=sprintf("SELECT customer_id FROM pg_score_bill WHERE id=%s",$id);
        $customer_id=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();

        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $now=$localeDate->date()->format('Y-m-d H:i:s');

        //先生效，然后计算总积分
        $active_update_sql=sprintf("update pg_score_bill set score_status=1,active_time='$now' WHERE id=%s AND score_status=0",$id);
        Yii::$app->db->createCommand($active_update_sql)->execute();

        $ret=static::calcCustomerScore($customer_id);
    }

    /**
     * 积分生效
     * @param $id
     */
    public static function scoreActive($id) {
        $ret=-1;
        $bill=static::findOne(["id"=>$id]);
        if ($bill->src_type=="sales_order" && !empty($bill->src_id) && $bill->score>=0) {
            //订单积分生效
            $ret=static::soScoreActive($bill->src_id);
        } else if ($bill->src_type=="sales_order" && !empty($bill->src_id) && $bill->score<0) {
            $ret=static::soReturnScoreActive($bill->id,$bill->src_id);
        } else if ($bill->src_type=="customer_entity") {
            $ret=static::customerScoreActive($bill->id);
        } else if ($bill->src_type=="review") {
            $ret=static::customerScoreActive($bill->id);
        }
        
        return $ret;
    }



    public static function deleteBill($id,$deletecode,$now){
        $sql = "update pg_score_bill set is_deleted = $deletecode,deleted_datetime = '$now' where id = $id";
        Yii::$app->db->createCommand($sql)->execute();
        $sql = "select customer_id from pg_score_bill where id = $id";
        $customer_id = Yii::$app->db->createCommand($sql)->queryOne();
        $customer_id = $customer_id['customer_id'];
        static::calcCustomerScore($customer_id);
    }
    public static function updateDelievered($id,$delievered_time){
        $sql = "update pg_score_bill set delievered_time = '$delievered_time' where id = $id";
        Yii::$app->db->createCommand($sql)->execute();
    }


    public static function SendActiveEmail($data){
//        100071804
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        //邮件配置
        $ACCOUNT_ID=2160;
        $AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
        $REWARD_MEMBERS_LIST_ID=3;
        $increment_id = $data;
        $sql = "SELECT psb.src_type, so.increment_id, sum(psb.score) as score, ce.firstname, ce.entity_id, lastname, email, ps.all_score, total_score, (6000 - total_score) AS diff_score FROM sales_order AS so INNER JOIN pg_score_bill AS psb ON psb.src_id = so.entity_id INNER JOIN customer_entity AS ce ON so.customer_id = ce.entity_id INNER JOIN pg_score AS ps ON ps.entity_id = so.customer_id AND psb.src_type = 'sales_order' AND so.increment_id = $increment_id  GROUP BY so.increment_id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        //模拟
        //发送邮件
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dr=$objectManager->get("Magento\Framework\Filesystem\DirectoryList");
        $document_root=$dr->getRoot();
        include_once $document_root."/lib/internal/Pg/Maropost/tests/autoloader.php";
        $contacts=new Contacts($ACCOUNT_ID, $AUTHTOKEN);
        $email = $data["email"];
        $porder_score = $data["score"];
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $customer_id = $data["entity_id"];
        $Available_now = $data["total_score"];
        $Total_points = $data["all_score"];
        $diff_score = $data["diff_score"];
        $increment_id = $data["increment_id"];
        $recommend_list=Yii::$container->get("app\components\helper\RecommendHelper")->get_customer_recommend_products($customer_id);
        $data=[
            "avail_total_points"=>$Available_now,
            "total_points"=>$Total_points,
            "tmp_so_increment_id"=>$increment_id,
            "tmp_so_gaving_points"=>$porder_score,
        ];
//            for ($key = 0;$key<4;$key++) {
//                $data["recommend_product_image".($key+1)]='';
//                $data["recommend_product_name".($key+1)]='';
//                $data["recommend_product_price".($key+1)]='';
//                $data["recommend_product_url".($key+1)]='';
//            }
        $recommend_count = 0;
        if(count($recommend_list)>=4){
            $recommend_count = 4;
        }else if(count($recommend_list)<4 && count($recommend_list)>=2){
            $recommend_count = 2;
        }else{
            $recommend_count = 0;
        }
        for ($key = 0;$key<$recommend_count;$key++){
            $data["recommend_product_image".($key+1)]=$recommend_list[$key]["image"];
            $data["recommend_product_name".($key+1)]=$recommend_list[$key]["name"];
            $data["recommend_product_price".($key+1)]=$recommend_list[$key]["product_price"];
            $data["recommend_product_url".($key+1)]=$recommend_list[$key]["url"];
        }
        if($diff_score > 0){
            $data["the_integral_of_the_difference"] = 6000-$Available_now;
        }
        //add lee 2020.12.4 增加klaviyo
        $customer_properties=array_merge(['$email'=>$email,'$first_name'=>$firstname,'$last_name'=>$lastname],$data);
        $properties=$data;
        if($Available_now >= 6000 ){
            $err_no=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("Rewards active more than 6000",$customer_properties,$properties);
        } else {
            $err_no=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("Rewards active less than 6000",$customer_properties,$properties);
        }
        //end

        //delete lee 2020.12.4 修改为klaviyo
//        $contacts->createOrUpdateForList($REWARD_MEMBERS_LIST_ID,$email,$firstname,$lastname,null,null,null,$data);
//        $json_send_data = json_encode($data);
//        $client = new GuzzleClient();
//        if($Available_now >=6000 ){
//            $url=sprintf("https://api.maropost.com/accounts/2160/journeys/22/trigger/27579871241115.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$email);
//        }else if($Available_now < 6000 ){
//            $url=sprintf("https://api.maropost.com/accounts/2160/journeys/23/trigger/27579871241115.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$email);
//        }
//        $res = $client->request('POST', $url);
//        $err_no=$res->getBody()->getContents();
//        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
//        $magento_autoload->loadMagentoFrameword();
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
//        $now=$localeDate->date()->format('Y-m-d H:i:s');
//        $sql = "INSERT INTO `send_email_code` (`action`, `customer_id`, `email`, `correlation_data`, `parameter`, `api_url`, `email_send_time`, `response`) VALUES ('sales_order_active', '$customer_id', '$email', '$increment_id', '$json_send_data', '$url', '$now', '$err_no')";
//        $code = Yii::$app->db->createCommand($sql)->execute();
        //end
        return 0;
    }
}
