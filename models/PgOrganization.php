<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_s3_file".
 *
 * @property int $id
 * @property string $content_type
 * @property string|null $obj_type
 * @property int $obj_id
 * @property int $customer_id
 * @property int $profile_id
 * @property string|null $profile_name
 * @property string|null $sku
 * @property string|null $additional_data
 * @property string|null $server_domain
 * @property string|null $file_path
 * @property string|null $created_at
 */
class PgOrganization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_organization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id', 'org_type', 'is_active'], 'integer'],
            [['org_name','street','city','region','postcode','country','telephone','fax'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'org_type' => 'Org Type',
            'org_name' => 'Org Name',
            'street' => 'Street',
            'city' => 'City',
            'region' => 'Region',
            'postcode' => 'Postcode',
            'country' => 'Country',
            'telephone' => 'Telephone',
            'is_active' => 'Is Active',
            'fax' => 'Fax',
            'created_at' => 'Created At',
        ];
    }


    //孙进超 20/8/25
    public static function getListSQL() {
        $sql="select po.entity_id,po.org_type,po.org_name,po.org_code,po.org_description,po.street,po.city,po.region,po.postcode,po.country,po.telephone,po.is_active,po.fax,po.created_at from pg_organization as po";
        return $sql;
    }

    //获取pg_organization数据并分页
    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'is_active'){
                    $query->andWhere(sprintf("is_active = :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'org_type'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if($c == 'org_name'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'telephone'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
            }
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }
    //获取urlrewrite数据数量
    public static function getListCount($condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'is_active'){
                    $query->andWhere(sprintf("is_active = :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'org_type'){
                    $query->andWhere(sprintf("%s = :%s",$c,$c));
                    $params[":".$c]=$v;
                }
                if($c == 'org_name'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'telephone'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
            }
        }
        $query->addParams($params);
        $data=$query->count();
        return $data;
    }

//    数据修改
    public static function active($entity_id,$is_active){
        if($is_active == 1){
            $is_active = 0;
        }else{
            $is_active = 1;
        }
        $sql = "update pg_organization set is_active = $is_active where entity_id = $entity_id";
        $data = Yii::$app->db->createCommand($sql)->execute();
    }
    public static function fetchOne($entity_id){
        $sql = "select * from pg_organization where entity_id = $entity_id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $data;
    }
    public static function org_name(){
        $sql = "select DISTINCT(org_name),entity_id from pg_organization";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $data;
    }
    public static function region_name(){
        $sql = "select region_id,default_name from directory_country_region where country_id = 'US'";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $data;
    }
    public static function region_detail($region_id){
        $sql = "select country_id,default_name from directory_country_region where region_id = $region_id and country_id = 'US'";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $data;
    }
    //    数据修改
    public static function toupdate($data=[]){
        $entity_id = $data['entity_id'];
        unset($data['entity_id']);
        $vals = '';
        foreach ($data as $key => $val){
            $vals .= '`'.$key.'`=\''.$val.'\',';
        }
        $vals = rtrim($vals,',');
        $sql = "update pg_organization set $vals where entity_id = $entity_id";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
//    数据添加
    public static function toinsert($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into pg_organization($keys) values ($vals)";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
}