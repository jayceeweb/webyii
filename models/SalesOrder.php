<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "sales_order".
 *
 * @property int $entity_id Entity Id
 * @property string|null $increment_id Increment Id
 * @property string|null $state State
 * @property string|null $status Status
 * @property string|null $coupon_code Coupon Code
 * @property string|null $protect_code Protect Code
 * @property string|null $shipping_description Shipping Description
 * @property int|null $is_virtual Is Virtual
 * @property int|null $store_id Store Id
 * @property int|null $customer_id Customer Id
 * @property float|null $base_discount_amount Base Discount Amount
 * @property float|null $base_discount_canceled Base Discount Canceled
 * @property float|null $base_discount_invoiced Base Discount Invoiced
 * @property float|null $base_discount_refunded Base Discount Refunded
 * @property float|null $base_grand_total Base Grand Total
 * @property float|null $base_shipping_amount Base Shipping Amount
 * @property float|null $base_shipping_canceled Base Shipping Canceled
 * @property float|null $base_shipping_invoiced Base Shipping Invoiced
 * @property float|null $base_shipping_refunded Base Shipping Refunded
 * @property float|null $base_shipping_tax_amount Base Shipping Tax Amount
 * @property float|null $base_shipping_tax_refunded Base Shipping Tax Refunded
 * @property float|null $base_subtotal Base Subtotal
 * @property float|null $base_subtotal_canceled Base Subtotal Canceled
 * @property float|null $base_subtotal_invoiced Base Subtotal Invoiced
 * @property float|null $base_subtotal_refunded Base Subtotal Refunded
 * @property float|null $base_tax_amount Base Tax Amount
 * @property float|null $base_tax_canceled Base Tax Canceled
 * @property float|null $base_tax_invoiced Base Tax Invoiced
 * @property float|null $base_tax_refunded Base Tax Refunded
 * @property float|null $base_to_global_rate Base To Global Rate
 * @property float|null $base_to_order_rate Base To Order Rate
 * @property float|null $base_total_canceled Base Total Canceled
 * @property float|null $base_total_invoiced Base Total Invoiced
 * @property float|null $base_total_invoiced_cost Base Total Invoiced Cost
 * @property float|null $base_total_offline_refunded Base Total Offline Refunded
 * @property float|null $base_total_online_refunded Base Total Online Refunded
 * @property float|null $base_total_paid Base Total Paid
 * @property float|null $base_total_qty_ordered Base Total Qty Ordered
 * @property float|null $base_total_refunded Base Total Refunded
 * @property float|null $discount_amount Discount Amount
 * @property float|null $discount_canceled Discount Canceled
 * @property float|null $discount_invoiced Discount Invoiced
 * @property float|null $discount_refunded Discount Refunded
 * @property float|null $grand_total Grand Total
 * @property float|null $shipping_amount Shipping Amount
 * @property float|null $shipping_canceled Shipping Canceled
 * @property float|null $shipping_invoiced Shipping Invoiced
 * @property float|null $shipping_refunded Shipping Refunded
 * @property float|null $shipping_tax_amount Shipping Tax Amount
 * @property float|null $shipping_tax_refunded Shipping Tax Refunded
 * @property float|null $store_to_base_rate Store To Base Rate
 * @property float|null $store_to_order_rate Store To Order Rate
 * @property float|null $subtotal Subtotal
 * @property float|null $subtotal_canceled Subtotal Canceled
 * @property float|null $subtotal_invoiced Subtotal Invoiced
 * @property float|null $subtotal_refunded Subtotal Refunded
 * @property float|null $tax_amount Tax Amount
 * @property float|null $tax_canceled Tax Canceled
 * @property float|null $tax_invoiced Tax Invoiced
 * @property float|null $tax_refunded Tax Refunded
 * @property float|null $total_canceled Total Canceled
 * @property float|null $total_invoiced Total Invoiced
 * @property float|null $total_offline_refunded Total Offline Refunded
 * @property float|null $total_online_refunded Total Online Refunded
 * @property float|null $total_paid Total Paid
 * @property float|null $total_qty_ordered Total Qty Ordered
 * @property float|null $total_refunded Total Refunded
 * @property int|null $can_ship_partially Can Ship Partially
 * @property int|null $can_ship_partially_item Can Ship Partially Item
 * @property int|null $customer_is_guest Customer Is Guest
 * @property int|null $customer_note_notify Customer Note Notify
 * @property int|null $billing_address_id Billing Address Id
 * @property int|null $customer_group_id Customer Group Id
 * @property int|null $edit_increment Edit Increment
 * @property int|null $email_sent Email Sent
 * @property int|null $send_email Send Email
 * @property int|null $forced_shipment_with_invoice Forced Do Shipment With Invoice
 * @property int|null $payment_auth_expiration Payment Authorization Expiration
 * @property int|null $quote_address_id Quote Address Id
 * @property int|null $quote_id Quote Id
 * @property int|null $shipping_address_id Shipping Address Id
 * @property float|null $adjustment_negative Adjustment Negative
 * @property float|null $adjustment_positive Adjustment Positive
 * @property float|null $base_adjustment_negative Base Adjustment Negative
 * @property float|null $base_adjustment_positive Base Adjustment Positive
 * @property float|null $base_shipping_discount_amount Base Shipping Discount Amount
 * @property float|null $base_subtotal_incl_tax Base Subtotal Incl Tax
 * @property float|null $base_total_due Base Total Due
 * @property float|null $payment_authorization_amount Payment Authorization Amount
 * @property float|null $shipping_discount_amount Shipping Discount Amount
 * @property float|null $subtotal_incl_tax Subtotal Incl Tax
 * @property float|null $total_due Total Due
 * @property float|null $weight Weight
 * @property string|null $customer_dob Customer Dob
 * @property string|null $applied_rule_ids Applied Rule Ids
 * @property string|null $base_currency_code Base Currency Code
 * @property string|null $customer_email Customer Email
 * @property string|null $customer_firstname Customer Firstname
 * @property string|null $customer_lastname Customer Lastname
 * @property string|null $customer_middlename Customer Middlename
 * @property string|null $customer_prefix Customer Prefix
 * @property string|null $customer_suffix Customer Suffix
 * @property string|null $customer_taxvat Customer Taxvat
 * @property string|null $discount_description Discount Description
 * @property string|null $ext_customer_id Ext Customer Id
 * @property string|null $ext_order_id Ext Order Id
 * @property string|null $global_currency_code Global Currency Code
 * @property string|null $hold_before_state Hold Before State
 * @property string|null $hold_before_status Hold Before Status
 * @property string|null $order_currency_code Order Currency Code
 * @property string|null $original_increment_id Original Increment Id
 * @property string|null $relation_child_id Relation Child Id
 * @property string|null $relation_child_real_id Relation Child Real Id
 * @property string|null $relation_parent_id Relation Parent Id
 * @property string|null $relation_parent_real_id Relation Parent Real Id
 * @property string|null $remote_ip Remote Ip
 * @property string|null $shipping_method Shipping Method
 * @property string|null $store_currency_code Store Currency Code
 * @property string|null $store_name Store Name
 * @property string|null $x_forwarded_for X Forwarded For
 * @property string|null $customer_note Customer Note
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 * @property int $total_item_count Total Item Count
 * @property int|null $customer_gender Customer Gender
 * @property float|null $discount_tax_compensation_amount Discount Tax Compensation Amount
 * @property float|null $base_discount_tax_compensation_amount Base Discount Tax Compensation Amount
 * @property float|null $shipping_discount_tax_compensation_amount Shipping Discount Tax Compensation Amount
 * @property float|null $base_shipping_discount_tax_compensation_amnt Base Shipping Discount Tax Compensation Amount
 * @property float|null $discount_tax_compensation_invoiced Discount Tax Compensation Invoiced
 * @property float|null $base_discount_tax_compensation_invoiced Base Discount Tax Compensation Invoiced
 * @property float|null $discount_tax_compensation_refunded Discount Tax Compensation Refunded
 * @property float|null $base_discount_tax_compensation_refunded Base Discount Tax Compensation Refunded
 * @property float|null $shipping_incl_tax Shipping Incl Tax
 * @property float|null $base_shipping_incl_tax Base Shipping Incl Tax
 * @property string|null $coupon_rule_name Coupon Sales Rule Name
 * @property int|null $gift_message_id Gift Message Id
 * @property int|null $paypal_ipn_customer_notified Paypal Ipn Customer Notified
 * @property int|null $mailchimp_abandonedcart_flag Retrieved from Mailchimp
 * @property string|null $mailchimp_campaign_id Campaign
 * @property string|null $mailchimp_landing_page Landing Page
 * @property int|null $mailchimp_flag Retrieved from Mailchimp
 * @property string|null $targeted_date Targeted Date
 * @property string|null $promised_date Promised Date
 * @property string|null $estimated_date Estimated Date
 * @property string|null $relation_email email
 * @property string|null $relation_phone phone
 * @property int|null $relation_checked relation checked
 * @property string|null $relation_add_date relation add date
 * @property int|null $address_verify_status
 * @property int|null $is_php
 * @property int|null $is_clone
 * @property string|null $clone_options
 * @property float|null $fee Fee
 * @property int|null $has_warranty
 * @property float|null $warranty
 * @property float|null $row_total_without_warranty
 * @property string|null $substate
 * @property string|null $shipped_content
 * @property string|null $promised_ship_content
 * @property string|null $shipped_date
 * @property string|null $promised_ship_date
 * @property int|null $clone_order_id
 * @property string|null $clone_order_number
 *
 * @property AmazonSalesOrder $amazonSalesOrder
 * @property DownloadableLinkPurchased[] $downloadableLinkPurchaseds
 * @property OrderImage[] $orderImages
 * @property PaypalBillingAgreementOrder[] $paypalBillingAgreementOrders
 * @property PaypalBillingAgreement[] $agreements
 * @property SalesCreditmemo[] $salesCreditmemos
 * @property SalesInvoice[] $salesInvoices
 * @property CustomerEntity $customer
 * @property Store $store
 * @property SalesOrderAddress[] $salesOrderAddresses
 * @property SalesOrderItem[] $salesOrderItems
 * @property SalesOrderPayment[] $salesOrderPayments
 * @property SalesOrderStatusHistory[] $salesOrderStatusHistories
 * @property SalesPaymentTransaction[] $salesPaymentTransactions
 * @property SalesShipment[] $salesShipments
 */
class SalesOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sales_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_virtual', 'store_id', 'customer_id', 'can_ship_partially', 'can_ship_partially_item', 'customer_is_guest', 'customer_note_notify', 'billing_address_id', 'customer_group_id', 'edit_increment', 'email_sent', 'send_email', 'forced_shipment_with_invoice', 'payment_auth_expiration', 'quote_address_id', 'quote_id', 'shipping_address_id', 'total_item_count', 'customer_gender', 'gift_message_id', 'paypal_ipn_customer_notified', 'mailchimp_abandonedcart_flag', 'mailchimp_flag', 'relation_checked', 'address_verify_status', 'is_php', 'is_clone', 'has_warranty', 'clone_order_id'], 'integer'],
            [['base_discount_amount', 'base_discount_canceled', 'base_discount_invoiced', 'base_discount_refunded', 'base_grand_total', 'base_shipping_amount', 'base_shipping_canceled', 'base_shipping_invoiced', 'base_shipping_refunded', 'base_shipping_tax_amount', 'base_shipping_tax_refunded', 'base_subtotal', 'base_subtotal_canceled', 'base_subtotal_invoiced', 'base_subtotal_refunded', 'base_tax_amount', 'base_tax_canceled', 'base_tax_invoiced', 'base_tax_refunded', 'base_to_global_rate', 'base_to_order_rate', 'base_total_canceled', 'base_total_invoiced', 'base_total_invoiced_cost', 'base_total_offline_refunded', 'base_total_online_refunded', 'base_total_paid', 'base_total_qty_ordered', 'base_total_refunded', 'discount_amount', 'discount_canceled', 'discount_invoiced', 'discount_refunded', 'grand_total', 'shipping_amount', 'shipping_canceled', 'shipping_invoiced', 'shipping_refunded', 'shipping_tax_amount', 'shipping_tax_refunded', 'store_to_base_rate', 'store_to_order_rate', 'subtotal', 'subtotal_canceled', 'subtotal_invoiced', 'subtotal_refunded', 'tax_amount', 'tax_canceled', 'tax_invoiced', 'tax_refunded', 'total_canceled', 'total_invoiced', 'total_offline_refunded', 'total_online_refunded', 'total_paid', 'total_qty_ordered', 'total_refunded', 'adjustment_negative', 'adjustment_positive', 'base_adjustment_negative', 'base_adjustment_positive', 'base_shipping_discount_amount', 'base_subtotal_incl_tax', 'base_total_due', 'payment_authorization_amount', 'shipping_discount_amount', 'subtotal_incl_tax', 'total_due', 'weight', 'discount_tax_compensation_amount', 'base_discount_tax_compensation_amount', 'shipping_discount_tax_compensation_amount', 'base_shipping_discount_tax_compensation_amnt', 'discount_tax_compensation_invoiced', 'base_discount_tax_compensation_invoiced', 'discount_tax_compensation_refunded', 'base_discount_tax_compensation_refunded', 'shipping_incl_tax', 'base_shipping_incl_tax', 'fee', 'warranty', 'row_total_without_warranty'], 'number'],
            [['customer_dob', 'created_at', 'updated_at', 'targeted_date', 'promised_date', 'estimated_date', 'relation_add_date', 'shipped_date', 'promised_ship_date'], 'safe'],
            [['customer_note', 'mailchimp_landing_page', 'clone_options'], 'string'],
            [['increment_id', 'state', 'status', 'customer_prefix', 'customer_suffix', 'customer_taxvat', 'ext_customer_id', 'ext_order_id', 'hold_before_state', 'hold_before_status', 'original_increment_id', 'relation_child_id', 'relation_child_real_id', 'relation_parent_id', 'relation_parent_real_id', 'remote_ip', 'shipping_method', 'store_name', 'x_forwarded_for', 'substate'], 'string', 'max' => 32],
            [['coupon_code', 'protect_code', 'shipping_description', 'discount_description', 'coupon_rule_name'], 'string', 'max' => 255],
            [['applied_rule_ids', 'customer_email', 'customer_firstname', 'customer_lastname', 'customer_middlename', 'relation_email', 'relation_phone'], 'string', 'max' => 128],
            [['base_currency_code', 'global_currency_code', 'order_currency_code', 'store_currency_code'], 'string', 'max' => 3],
            [['mailchimp_campaign_id'], 'string', 'max' => 16],
            [['shipped_content', 'promised_ship_content', 'clone_order_number'], 'string', 'max' => 256],
            [['increment_id', 'store_id'], 'unique', 'targetAttribute' => ['increment_id', 'store_id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerEntity::className(), 'targetAttribute' => ['customer_id' => 'entity_id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'store_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'increment_id' => 'Increment ID',
            'state' => 'State',
            'status' => 'Status',
            'coupon_code' => 'Coupon Code',
            'protect_code' => 'Protect Code',
            'shipping_description' => 'Shipping Description',
            'is_virtual' => 'Is Virtual',
            'store_id' => 'Store ID',
            'customer_id' => 'Customer ID',
            'base_discount_amount' => 'Base Discount Amount',
            'base_discount_canceled' => 'Base Discount Canceled',
            'base_discount_invoiced' => 'Base Discount Invoiced',
            'base_discount_refunded' => 'Base Discount Refunded',
            'base_grand_total' => 'Base Grand Total',
            'base_shipping_amount' => 'Base Shipping Amount',
            'base_shipping_canceled' => 'Base Shipping Canceled',
            'base_shipping_invoiced' => 'Base Shipping Invoiced',
            'base_shipping_refunded' => 'Base Shipping Refunded',
            'base_shipping_tax_amount' => 'Base Shipping Tax Amount',
            'base_shipping_tax_refunded' => 'Base Shipping Tax Refunded',
            'base_subtotal' => 'Base Subtotal',
            'base_subtotal_canceled' => 'Base Subtotal Canceled',
            'base_subtotal_invoiced' => 'Base Subtotal Invoiced',
            'base_subtotal_refunded' => 'Base Subtotal Refunded',
            'base_tax_amount' => 'Base Tax Amount',
            'base_tax_canceled' => 'Base Tax Canceled',
            'base_tax_invoiced' => 'Base Tax Invoiced',
            'base_tax_refunded' => 'Base Tax Refunded',
            'base_to_global_rate' => 'Base To Global Rate',
            'base_to_order_rate' => 'Base To Order Rate',
            'base_total_canceled' => 'Base Total Canceled',
            'base_total_invoiced' => 'Base Total Invoiced',
            'base_total_invoiced_cost' => 'Base Total Invoiced Cost',
            'base_total_offline_refunded' => 'Base Total Offline Refunded',
            'base_total_online_refunded' => 'Base Total Online Refunded',
            'base_total_paid' => 'Base Total Paid',
            'base_total_qty_ordered' => 'Base Total Qty Ordered',
            'base_total_refunded' => 'Base Total Refunded',
            'discount_amount' => 'Discount Amount',
            'discount_canceled' => 'Discount Canceled',
            'discount_invoiced' => 'Discount Invoiced',
            'discount_refunded' => 'Discount Refunded',
            'grand_total' => 'Grand Total',
            'shipping_amount' => 'Shipping Amount',
            'shipping_canceled' => 'Shipping Canceled',
            'shipping_invoiced' => 'Shipping Invoiced',
            'shipping_refunded' => 'Shipping Refunded',
            'shipping_tax_amount' => 'Shipping Tax Amount',
            'shipping_tax_refunded' => 'Shipping Tax Refunded',
            'store_to_base_rate' => 'Store To Base Rate',
            'store_to_order_rate' => 'Store To Order Rate',
            'subtotal' => 'Subtotal',
            'subtotal_canceled' => 'Subtotal Canceled',
            'subtotal_invoiced' => 'Subtotal Invoiced',
            'subtotal_refunded' => 'Subtotal Refunded',
            'tax_amount' => 'Tax Amount',
            'tax_canceled' => 'Tax Canceled',
            'tax_invoiced' => 'Tax Invoiced',
            'tax_refunded' => 'Tax Refunded',
            'total_canceled' => 'Total Canceled',
            'total_invoiced' => 'Total Invoiced',
            'total_offline_refunded' => 'Total Offline Refunded',
            'total_online_refunded' => 'Total Online Refunded',
            'total_paid' => 'Total Paid',
            'total_qty_ordered' => 'Total Qty Ordered',
            'total_refunded' => 'Total Refunded',
            'can_ship_partially' => 'Can Ship Partially',
            'can_ship_partially_item' => 'Can Ship Partially Item',
            'customer_is_guest' => 'Customer Is Guest',
            'customer_note_notify' => 'Customer Note Notify',
            'billing_address_id' => 'Billing Address ID',
            'customer_group_id' => 'Customer Group ID',
            'edit_increment' => 'Edit Increment',
            'email_sent' => 'Email Sent',
            'send_email' => 'Send Email',
            'forced_shipment_with_invoice' => 'Forced Shipment With Invoice',
            'payment_auth_expiration' => 'Payment Auth Expiration',
            'quote_address_id' => 'Quote Address ID',
            'quote_id' => 'Quote ID',
            'shipping_address_id' => 'Shipping Address ID',
            'adjustment_negative' => 'Adjustment Negative',
            'adjustment_positive' => 'Adjustment Positive',
            'base_adjustment_negative' => 'Base Adjustment Negative',
            'base_adjustment_positive' => 'Base Adjustment Positive',
            'base_shipping_discount_amount' => 'Base Shipping Discount Amount',
            'base_subtotal_incl_tax' => 'Base Subtotal Incl Tax',
            'base_total_due' => 'Base Total Due',
            'payment_authorization_amount' => 'Payment Authorization Amount',
            'shipping_discount_amount' => 'Shipping Discount Amount',
            'subtotal_incl_tax' => 'Subtotal Incl Tax',
            'total_due' => 'Total Due',
            'weight' => 'Weight',
            'customer_dob' => 'Customer Dob',
            'applied_rule_ids' => 'Applied Rule Ids',
            'base_currency_code' => 'Base Currency Code',
            'customer_email' => 'Customer Email',
            'customer_firstname' => 'Customer Firstname',
            'customer_lastname' => 'Customer Lastname',
            'customer_middlename' => 'Customer Middlename',
            'customer_prefix' => 'Customer Prefix',
            'customer_suffix' => 'Customer Suffix',
            'customer_taxvat' => 'Customer Taxvat',
            'discount_description' => 'Discount Description',
            'ext_customer_id' => 'Ext Customer ID',
            'ext_order_id' => 'Ext Order ID',
            'global_currency_code' => 'Global Currency Code',
            'hold_before_state' => 'Hold Before State',
            'hold_before_status' => 'Hold Before Status',
            'order_currency_code' => 'Order Currency Code',
            'original_increment_id' => 'Original Increment ID',
            'relation_child_id' => 'Relation Child ID',
            'relation_child_real_id' => 'Relation Child Real ID',
            'relation_parent_id' => 'Relation Parent ID',
            'relation_parent_real_id' => 'Relation Parent Real ID',
            'remote_ip' => 'Remote Ip',
            'shipping_method' => 'Shipping Method',
            'store_currency_code' => 'Store Currency Code',
            'store_name' => 'Store Name',
            'x_forwarded_for' => 'X Forwarded For',
            'customer_note' => 'Customer Note',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'total_item_count' => 'Total Item Count',
            'customer_gender' => 'Customer Gender',
            'discount_tax_compensation_amount' => 'Discount Tax Compensation Amount',
            'base_discount_tax_compensation_amount' => 'Base Discount Tax Compensation Amount',
            'shipping_discount_tax_compensation_amount' => 'Shipping Discount Tax Compensation Amount',
            'base_shipping_discount_tax_compensation_amnt' => 'Base Shipping Discount Tax Compensation Amnt',
            'discount_tax_compensation_invoiced' => 'Discount Tax Compensation Invoiced',
            'base_discount_tax_compensation_invoiced' => 'Base Discount Tax Compensation Invoiced',
            'discount_tax_compensation_refunded' => 'Discount Tax Compensation Refunded',
            'base_discount_tax_compensation_refunded' => 'Base Discount Tax Compensation Refunded',
            'shipping_incl_tax' => 'Shipping Incl Tax',
            'base_shipping_incl_tax' => 'Base Shipping Incl Tax',
            'coupon_rule_name' => 'Coupon Rule Name',
            'gift_message_id' => 'Gift Message ID',
            'paypal_ipn_customer_notified' => 'Paypal Ipn Customer Notified',
            'mailchimp_abandonedcart_flag' => 'Mailchimp Abandonedcart Flag',
            'mailchimp_campaign_id' => 'Mailchimp Campaign ID',
            'mailchimp_landing_page' => 'Mailchimp Landing Page',
            'mailchimp_flag' => 'Mailchimp Flag',
            'targeted_date' => 'Targeted Date',
            'promised_date' => 'Promised Date',
            'estimated_date' => 'Estimated Date',
            'relation_email' => 'Relation Email',
            'relation_phone' => 'Relation Phone',
            'relation_checked' => 'Relation Checked',
            'relation_add_date' => 'Relation Add Date',
            'address_verify_status' => 'Address Verify Status',
            'is_php' => 'Is Php',
            'is_clone' => 'Is Clone',
            'clone_options' => 'Clone Options',
            'fee' => 'Fee',
            'has_warranty' => 'Has Warranty',
            'warranty' => 'Warranty',
            'row_total_without_warranty' => 'Row Total Without Warranty',
            'substate' => 'Substate',
            'shipped_content' => 'Shipped Content',
            'promised_ship_content' => 'Promised Ship Content',
            'shipped_date' => 'Shipped Date',
            'promised_ship_date' => 'Promised Ship Date',
            'clone_order_id' => 'Clone Order ID',
            'clone_order_number' => 'Clone Order Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmazonSalesOrder()
    {
        return $this->hasOne(AmazonSalesOrder::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDownloadableLinkPurchaseds()
    {
        return $this->hasMany(DownloadableLinkPurchased::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderImages()
    {
        return $this->hasMany(OrderImage::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaypalBillingAgreementOrders()
    {
        return $this->hasMany(PaypalBillingAgreementOrder::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(PaypalBillingAgreement::className(), ['agreement_id' => 'agreement_id'])->viaTable('paypal_billing_agreement_order', ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesCreditmemos()
    {
        return $this->hasMany(SalesCreditmemo::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesInvoices()
    {
        return $this->hasMany(SalesInvoice::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerEntity::className(), ['entity_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['store_id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrderAddresses()
    {
        return $this->hasMany(SalesOrderAddress::className(), ['parent_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrderItems()
    {
        return $this->hasMany(SalesOrderItem::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrderPayments()
    {
        return $this->hasMany(SalesOrderPayment::className(), ['parent_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrderStatusHistories()
    {
        return $this->hasMany(SalesOrderStatusHistory::className(), ['parent_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesPaymentTransactions()
    {
        return $this->hasMany(SalesPaymentTransaction::className(), ['order_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesShipments()
    {
        return $this->hasMany(SalesShipment::className(), ['order_id' => 'entity_id']);
    }

    public static function getSalesOrderByCoupon($coupon,$start_order_time='00-00-00',$end_order_time='00-00-00'){
        if($end_order_time){
            $sql=sprintf("SELECT entity_id,state,coupon_code,shipping_description,customer_id,discount_amount,grand_total,shipping_amount,subtotal,total_qty_ordered,subtotal_refunded,total_paid,total_canceled,total_qty_ordered,total_refunded,quote_id,increment_id,customer_email,customer_firstname,customer_lastname,customer_gender,shipping_method,total_item_count,has_warranty,warranty,'so'
FROM sales_order WHERE coupon_code = '%s'  and created_at >= '%s' and created_at <= '%s'",$coupon,$start_order_time,$end_order_time);
        }else{
            $sql=sprintf("SELECT entity_id,state,coupon_code,shipping_description,customer_id,discount_amount,grand_total,shipping_amount,subtotal,total_qty_ordered,subtotal_refunded,total_paid,total_canceled,total_qty_ordered,total_refunded,quote_id,increment_id,customer_email,customer_firstname,customer_lastname,customer_gender,shipping_method,total_item_count,has_warranty,warranty,'so'
FROM sales_order WHERE coupon_code = '%s'  and created_at >= '%s'",$coupon,$start_order_time);
        }
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $data;
    }
}