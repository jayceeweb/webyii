<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "eav_attribute_set".
 *
 * @property int $attribute_set_id Attribute Set Id
 * @property int $entity_type_id Entity Type Id
 * @property string|null $attribute_set_name Attribute Set Name
 * @property int $sort_order Sort Order
 *
 * @property CatalogProductEntity[] $catalogProductEntities
 * @property EavAttributeGroup[] $eavAttributeGroups
 * @property EavEntityType $entityType
 */
class EavAttributeSet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eav_attribute_set';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_type_id', 'sort_order'], 'integer'],
            [['attribute_set_name'], 'string', 'max' => 255],
            [['entity_type_id', 'attribute_set_name'], 'unique', 'targetAttribute' => ['entity_type_id', 'attribute_set_name']],
            [['entity_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => EavEntityType::className(), 'targetAttribute' => ['entity_type_id' => 'entity_type_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'attribute_set_id' => 'Attribute Set ID',
            'entity_type_id' => 'Entity Type ID',
            'attribute_set_name' => 'Attribute Set Name',
            'sort_order' => 'Sort Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProductEntities()
    {
        return $this->hasMany(CatalogProductEntity::className(), ['attribute_set_id' => 'attribute_set_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEavAttributeGroups()
    {
        return $this->hasMany(EavAttributeGroup::className(), ['attribute_set_id' => 'attribute_set_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityType()
    {
        return $this->hasOne(EavEntityType::className(), ['entity_type_id' => 'entity_type_id']);
    }

    public static function getAllAttributeSets() {
        $sql="select * from eav_attribute_set where entity_type_id=4 order by sort_order desc";
        return \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
    }
}