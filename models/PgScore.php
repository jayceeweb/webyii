<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_score".
 *
 * @property int $entity_id
 * @property int $total_score
 */
class PgScore extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_score';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'required'],
            [['entity_id', 'total_score','all_score'], 'integer'],
            [['entity_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'total_score' => 'Total Score',
            'all_score' => 'All Score',
        ];
    }


    public static function getListSQL() {
        $sql="SELECT score.`total_score`,score.`recommended`,score.`recommend_date`,score.`permissions`,score.`all_score`,cus.`email`,cus.`confirm_email_confirm`,cus.`confirm_date`,cus.`entity_id`,cus.enabled_score,cus.confirm_email,(select  max(created_at) from sales_order so where  discount_amount < 0 and so.customer_id = cus.entity_id ) as new_discount_order_time
FROM customer_entity cus
LEFT JOIN `pg_score` score  ON cus.`entity_id`=score.entity_id";
        return $sql;
    }

    public static function revise($customer_id){
        $sql = "select sum(score) as all_score from pg_score_bill where customer_id = $customer_id and is_deleted = 0";
        $all_score = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        $all_score = $all_score['all_score'];
        $sql = "select sum(score) as total_score from pg_score_bill where customer_id = $customer_id and score_status = 1 and is_deleted = 0";
        $total_score = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        $total_score = $total_score['total_score'];
        $sql = "INSERT into pg_score(entity_id, all_score, total_score) values ('$customer_id','$all_score','$total_score') ON DUPLICATE KEY UPDATE all_score=VALUES(all_score) , total_score=VALUES(total_score)";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }

    public static function recommend($customer_id){
        $sql = "select count(*) as count from pg_score where entity_id = $customer_id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        if($data['count'] == 0){
            $sql = "insert into pg_score (`entity_id`,`recommended`,`recommend_date`) values ($customer_id,1,now())";
        }else{
            $sql = "update pg_score set recommended = 1,recommend_date = now() where entity_id = $customer_id";
        }
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }

    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $order_by = array();
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v){
                if($c == 'email'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'recommended' || $c == 'confirm_email_confirm'|| $c == 'enabled_score'){
                    if($v == 1){
                        $query->andWhere(sprintf("%s = :%s",$c,$c));
                        $params[":".$c]=$v;
                    }else if($v == 2){
                        $query->andWhere(sprintf("%s != :%s",$c,$c));
                        $params[":".$c]=1;
                    }
                }
                if ($c == 'discount_amount' && $v == 1) {
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where discount_amount < 0)"));
                    $order_by = ["new_discount_order_time"=>SORT_DESC];
                }
                if ($c == 'is_clone' && $v == 1) {
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where is_clone = 1)"));
                }
                if ($c == 'confirmstart_time') {
                    $query->andWhere(sprintf("confirm_date >= :%s", $c));
                    $params[":" . $c] = $v;
                }
                if ($c == 'confirmend_time'){
                    $query->andWhere(sprintf("confirm_date <= :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'recommendstart_time'){
                    $query->andWhere(sprintf("recommend_date >= :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'recommendend_time'){
                    $query->andWhere(sprintf("recommend_date <= :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'discountamountstart_time'){
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where discount_amount < 0 and created_at >= :%s)",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'discountamountend_time'){
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where discount_amount < 0 and created_at <= :%s)",$c));
                    $params[":".$c]=$v;
                }
            }
        }
        if(!$order_by){
            $order_by = ["entity_id"=>SORT_DESC];
        }
        $query->orderBy($order_by);
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }

    public static function getListCount($condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v){
                if($c == 'email'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'recommended' || $c == 'confirm_email_confirm'|| $c == 'enabled_score'){
                    if($v == 1){
                        $query->andWhere(sprintf("%s = :%s",$c,$c));
                        $params[":".$c]=$v;
                    }else if($v == 2){
                        $query->andWhere(sprintf("%s != :%s",$c,$c));
                        $params[":".$c]=1;
                    }
                }
                if ($c == 'discount_amount' && $v == 1) {
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where discount_amount < 0)"));
                }
                if ($c == 'confirmstart_time') {
                    $query->andWhere(sprintf("confirm_date >= :%s", $c));
                    $params[":" . $c] = $v;
                }
                if ($c == 'is_clone' && $v == 1) {
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where is_clone = 1)"));
                }
                if ($c == 'confirmend_time'){
                    $query->andWhere(sprintf("confirm_date <= :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'recommendstart_time'){
                    $query->andWhere(sprintf("recommend_date >= :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'recommendend_time'){
                    $query->andWhere(sprintf("recommend_date <= :%s",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'discountamountstart_time'){
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where discount_amount < 0 and created_at >= :%s)",$c));
                    $params[":".$c]=$v;
                }
                if ($c == 'discountamountend_time'){
                    $query->andWhere(sprintf(" `entity_id` in (select customer_id from sales_order where discount_amount < 0 and created_at <= :%s)",$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $cnt=$query->count();
        return $cnt;
    }

    public static function count() {
        $sql = "SELECT sum(case when score.recommended=1 then 1 else 0 end )as count_recommended,sum(case when cus.confirm_email_confirm=1 then 1 else 0 end )as count_confirm_email_confirm,sum(case when cus.enabled_score=1 then 1 else 0 end )as count_enabled_score FROM `pg_score` score right JOIN customer_entity cus ON cus.`entity_id`=score.entity_id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $data;
    }
}