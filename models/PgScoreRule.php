<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_score_rule".
 *
 * @property int $id
 * @property int $sku_id
 * @property string $create_at
 * @property int $create_admin_user_id
 * @property int $create_user_id
 * @property string $update_at
 * @property int $update_user_id
 */
class PgScoreRule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_score_rule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sku_id', 'create_admin_user_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku_id' => 'Sku ID',
            'create_at' => 'Create At',
            'create_admin_user_id' => 'Create Admin User ID',
            'create_user_id' => 'Create User ID',
            'update_at' => 'Update At',
            'update_user_id' => 'Update User ID',
        ];
    }

    public static function initScoreRule() {
        $sql="INSERT INTO pg_score_rule(sku_id) SELECT entity_id FROM catalog_product_flat_1 WHERE entity_id NOT IN (SELECT sku_id FROM `pg_score_rule`)";
        $command = Yii::$app->db->creatCommand($sql);
        $command->execute();

        $sql="INSERT INTO pg_score_rule_detail (score_rule_id,customer_group_id)
SELECT * FROM (SELECT r.id,g.customer_group_id 
FROM customer_group g,pg_score_rule r) main
WHERE NOT EXISTS (SELECT 0 FROM pg_score_rule_detail WHERE score_rule_id=main.id AND customer_group_id=main.customer_group_id)
";
        $command->execute();
        return 0;
    }

    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=CatalogProductFlat1::getListSQLByTypeIds(["simple","virtual"]);
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            $query->andWhere(sprintf("%s like :%s",$c,$c));
            $params[":".$c]="%%".$v."%%";
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }

    public static function getListCount($condition=[]) {
        $sql=CatalogProductFlat1::getListSQLByTypeIds(["simple","virtual"]);
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if (!empty($v)) {
                $query->andWhere(sprintf("%s like :%s",$c,$c));
                $params[":".$c]="%%".$v."%%";
            }
        }
        $query->addParams($params);
        $cnt=$query->count();
        return $cnt;
    }
}