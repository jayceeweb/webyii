<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_product_tag_item".
 *
 * @property int $item_id
 * @property int $tag_id
 * @property int $product_id
 * @property int $tag_position
 */
class PgProductTagItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_product_tag_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tag_id', 'product_id', 'tag_position'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'tag_id' => 'Tag ID',
            'product_id' => 'Product ID',
            'tag_position' => 'Tag Position',
        ];
    }
}