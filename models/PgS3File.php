<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_s3_file".
 *
 * @property int $id
 * @property string $content_type
 * @property string|null $obj_type
 * @property int $obj_id
 * @property int $customer_id
 * @property int $profile_id
 * @property string|null $profile_name
 * @property string|null $sku
 * @property string|null $additional_data
 * @property string|null $server_domain
 * @property string|null $file_path
 * @property string|null $created_at
 */
class PgS3File extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_s3_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['obj_id', 'customer_id', 'profile_id'], 'integer'],
            [['additional_data'], 'string'],
            [['created_at'], 'safe'],
            [['content_type','sku'], 'string', 'max' => 32],
            [['obj_type'], 'string', 'max' => 64],
            [['profile_name', 'file_path'], 'string', 'max' => 128],
            [['server_domain'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content_type' => 'Content Type',
            'obj_type' => 'Obj Type',
            'obj_id' => 'Obj ID',
            'customer_id' => 'Customer ID',
            'profile_id' => 'Profile ID',
            'profile_name' => 'Profile Name',
            'sku' => 'Sku',
            'additional_data' => 'Additional Data',
            'server_domain' => 'Server Domain',
            'file_path' => 'File Path',
            'created_at' => 'Created At',
        ];
    }
}