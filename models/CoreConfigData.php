<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "core_config_data".
 *
 * @property int $config_id Config Id
 * @property string $scope Config Scope
 * @property int $scope_id Config Scope Id
 * @property string $path Config Path
 * @property string|null $value Config Value
 */
class CoreConfigData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_config_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['scope_id'], 'integer'],
            [['value'], 'string'],
            [['scope'], 'string', 'max' => 8],
            [['path'], 'string', 'max' => 255],
            [['scope', 'scope_id', 'path'], 'unique', 'targetAttribute' => ['scope', 'scope_id', 'path']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'config_id' => 'Config ID',
            'scope' => 'Scope',
            'scope_id' => 'Scope ID',
            'path' => 'Path',
            'value' => 'Value',
        ];
    }

    public static function getValue($path,$scope="") {
        $search=["path"=>$path];
        if ($scope!="") {
            $search["scope"]=$scope;
        }
        $v=static::findOne($search);
        if (!empty($v)) {
            return $v->getAttribute("value");
        } else {
            return null;
        }
    }

    public static function getValueByPath($path,$scope_id="",$scope="")
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        if ($scope_id!="" && $scope!="") {
            $value = $_scopeConfig->getValue($path,$scope_id,$scope);
        } else {
            $value = $_scopeConfig->getValue($path);
        }
        return $value;
    }

    public static function getStaticUrl()
    {
        $secureURL=static::getValueByPath("config/general/static_url");
        if(substr(trim($secureURL), -1) == "/") {
            $secureURL = substr($secureURL,0,strlen($secureURL)-1);
        }
        return $secureURL;
    }

    public static function getBaseUrl()
    {
        $secureURL=static::getValueByPath("web/secure/base_url");
        if(substr(trim($secureURL), -1) == "/") {
            $secureURL = substr($secureURL,0,strlen($secureURL)-1);
        }
        return $secureURL;
    }

    public static function getJSfileVersion()
    {
        $value=static::getValueByPath("config/general/config_plp_javascript_version");
        return $value;
    }

    public static function getMedia()
    {
        $value=static::getValueByPath("web/secure/base_media_url");
        return $value;
    }
}
