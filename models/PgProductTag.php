<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_product_tag".
 *
 * @property int $id
 * @property int $product_id
 * @property string $tag
 * @property int $tag_position
 */
class PgProductTag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_product_tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'tag_position'], 'integer'],
            [['tag'], 'required'],
            [['tag'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'tag' => 'Tag',
            'tag_position' => 'Tag Position',
        ];
    }

    public static function get_tags_by_product_id($configurable_id) {
        $tags = \app\components\helper\DbHelper::getSlaveDb()->createCommand("SELECT * FROM `pg_product_tag` WHERE product_id=".$configurable_id)->queryAll();
        return $tags;
    }
    
    public static function get_product_tag_by_tag($configurable_id,$tag) {
        $tags = \app\components\helper\DbHelper::getSlaveDb()->createCommand("SELECT * FROM `pg_product_tag` WHERE product_id=".$configurable_id." and tag='".$tag."'")->queryOne();
        return $tags;
    }

    public static function get_tags_url($tags) {
        $tag_str="";
        $tag_urls=[];
        if (count($tags)>0) {
            $i=0;
            foreach ($tags as $key=>$tag) {
                if ($i==0) {
                    $tag_str=sprintf("'%s'",$tag);
                } else {
                    $tag_str=sprintf('%s,\'%s\'',$tag_str,$tag);
                }
                $i=$i+1;
            }
            $sql=sprintf("SELECT `name`,base_url_canonical FROM `pg_tag_html` WHERE `name` IN (%s) ORDER BY category_id DESC",$tag_str);
            $tags = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $tag_urls=[];
            foreach ($tags as $key=>$tag) {
                if(!isset($tag_urls[$tag["name"]])) {
                    if (!empty($tag["base_url_canonical"])) {
                        $tag_urls[]=['name'=>$tag["name"],'url'=>$tag["base_url_canonical"]];
                    }
                }
            }
        }
        return $tag_urls;
    }
}