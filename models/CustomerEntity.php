<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_entity".
 *
 * @property int $entity_id Entity Id
 * @property int|null $website_id Website Id
 * @property string|null $email Email
 * @property int $group_id Group Id
 * @property string|null $increment_id Increment Id
 * @property int|null $store_id Store Id
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 * @property int $is_active Is Active
 * @property int $disable_auto_group_change Disable automatic group change based on VAT ID
 * @property string|null $created_in Created From
 * @property string|null $prefix Prefix
 * @property string|null $firstname First Name
 * @property string|null $middlename Middle Name/Initial
 * @property string|null $lastname Last Name
 * @property string|null $suffix Suffix
 * @property string|null $dob Date of Birth
 * @property string|null $password_hash Password_hash
 * @property string|null $rp_token Reset password token
 * @property string|null $rp_token_created_at Reset password token creation time
 * @property int|null $default_billing Default Billing Address
 * @property int|null $default_shipping Default Shipping Address
 * @property string|null $taxvat Tax/VAT Number
 * @property string|null $confirmation Is Confirmed
 * @property int|null $gender Gender
 * @property int|null $failures_num Failure Number
 * @property string|null $first_failure First Failure
 * @property string|null $lock_expires Lock Expiration Date
 * @property int $default_profile Default Profile
 * @property string|null $thirdparty Third Party
 * @property string|null $relation_email
 * @property string|null $relation_add_date
 * @property string|null $relation_phone
 * @property int|null $relation_checked
 * @property int|null $address_verify_status
 * @property string|null $stripe_customer_id Stripe Customer ID
 * @property string|null $password_hash_old
 * @property int $enabled_score
 * @property string|null $confirm_email
 * @property int $confirm_email_confirm
 *
 * @property AmazonCustomer $amazonCustomer
 * @property CatalogCompareItem[] $catalogCompareItems
 * @property CustomerAddressEntity[] $customerAddressEntities
 * @property Store $store
 * @property StoreWebsite $website
 * @property CustomerEntityDatetime[] $customerEntityDatetimes
 * @property EavAttribute[] $attributes0
 * @property CustomerEntityDecimal[] $customerEntityDecimals
 * @property EavAttribute[] $attributes1
 * @property CustomerEntityInt[] $customerEntityInts
 * @property EavAttribute[] $attributes2
 * @property CustomerEntityText[] $customerEntityTexts
 * @property EavAttribute[] $attributes3
 * @property CustomerEntityVarchar[] $customerEntityVarchars
 * @property EavAttribute[] $attributes4
 * @property DownloadableLinkPurchased[] $downloadableLinkPurchaseds
 * @property OauthToken[] $oauthTokens
 * @property PaypalBillingAgreement[] $paypalBillingAgreements
 * @property PersistentSession $persistentSession
 * @property PlumrocketSocialloginAccount[] $plumrocketSocialloginAccounts
 * @property ProductAlertPrice[] $productAlertPrices
 * @property ProductAlertStock[] $productAlertStocks
 * @property Profile[] $profiles
 * @property ReportComparedProductIndex[] $reportComparedProductIndices
 * @property CatalogProductEntity[] $products
 * @property ReportViewedProductIndex[] $reportViewedProductIndices
 * @property CatalogProductEntity[] $products0
 * @property ReviewDetail[] $reviewDetails
 * @property SalesOrder[] $salesOrders
 * @property SalesruleCouponUsage[] $salesruleCouponUsages
 * @property SalesruleCoupon[] $coupons
 * @property SalesruleCustomer[] $salesruleCustomers
 * @property ShareDiscount[] $shareDiscounts
 * @property VaultPaymentToken[] $vaultPaymentTokens
 */
class CustomerEntity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_entity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['website_id', 'group_id', 'store_id', 'is_active', 'disable_auto_group_change', 'default_billing', 'default_shipping', 'gender', 'failures_num', 'default_profile', 'relation_checked', 'address_verify_status', 'enabled_score', 'confirm_email_confirm'], 'integer'],
            [['created_at', 'updated_at', 'dob', 'rp_token_created_at', 'first_failure', 'lock_expires', 'relation_add_date'], 'safe'],
            [['default_profile'], 'required'],
            [['stripe_customer_id'], 'string'],
            [['email', 'created_in', 'firstname', 'middlename', 'lastname'], 'string', 'max' => 255],
            [['increment_id', 'taxvat'], 'string', 'max' => 50],
            [['prefix', 'suffix'], 'string', 'max' => 40],
            [['password_hash', 'rp_token', 'relation_email', 'relation_phone', 'password_hash_old', 'confirm_email'], 'string', 'max' => 128],
            [['confirmation'], 'string', 'max' => 64],
            [['thirdparty'], 'string', 'max' => 15],
            [['email', 'website_id'], 'unique', 'targetAttribute' => ['email', 'website_id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'store_id']],
            [['website_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreWebsite::className(), 'targetAttribute' => ['website_id' => 'website_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'website_id' => 'Website ID',
            'email' => 'Email',
            'group_id' => 'Group ID',
            'increment_id' => 'Increment ID',
            'store_id' => 'Store ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_active' => 'Is Active',
            'disable_auto_group_change' => 'Disable Auto Group Change',
            'created_in' => 'Created In',
            'prefix' => 'Prefix',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'lastname' => 'Lastname',
            'suffix' => 'Suffix',
            'dob' => 'Dob',
            'password_hash' => 'Password Hash',
            'rp_token' => 'Rp Token',
            'rp_token_created_at' => 'Rp Token Created At',
            'default_billing' => 'Default Billing',
            'default_shipping' => 'Default Shipping',
            'taxvat' => 'Taxvat',
            'confirmation' => 'Confirmation',
            'gender' => 'Gender',
            'failures_num' => 'Failures Num',
            'first_failure' => 'First Failure',
            'lock_expires' => 'Lock Expires',
            'default_profile' => 'Default Profile',
            'thirdparty' => 'Thirdparty',
            'relation_email' => 'Relation Email',
            'relation_add_date' => 'Relation Add Date',
            'relation_phone' => 'Relation Phone',
            'relation_checked' => 'Relation Checked',
            'address_verify_status' => 'Address Verify Status',
            'stripe_customer_id' => 'Stripe Customer ID',
            'password_hash_old' => 'Password Hash Old',
            'enabled_score' => 'Enabled Score',
            'confirm_email' => 'Confirm Email',
            'confirm_email_confirm' => 'Confirm Email Confirm',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmazonCustomer()
    {
        return $this->hasOne(AmazonCustomer::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogCompareItems()
    {
        return $this->hasMany(CatalogCompareItem::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerAddressEntities()
    {
        return $this->hasMany(CustomerAddressEntity::className(), ['parent_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['store_id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebsite()
    {
        return $this->hasOne(StoreWebsite::className(), ['website_id' => 'website_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerEntityDatetimes()
    {
        return $this->hasMany(CustomerEntityDatetime::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes0()
    {
        return $this->hasMany(EavAttribute::className(), ['attribute_id' => 'attribute_id'])->viaTable('customer_entity_datetime', ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerEntityDecimals()
    {
        return $this->hasMany(CustomerEntityDecimal::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes1()
    {
        return $this->hasMany(EavAttribute::className(), ['attribute_id' => 'attribute_id'])->viaTable('customer_entity_decimal', ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerEntityInts()
    {
        return $this->hasMany(CustomerEntityInt::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes2()
    {
        return $this->hasMany(EavAttribute::className(), ['attribute_id' => 'attribute_id'])->viaTable('customer_entity_int', ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerEntityTexts()
    {
        return $this->hasMany(CustomerEntityText::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes3()
    {
        return $this->hasMany(EavAttribute::className(), ['attribute_id' => 'attribute_id'])->viaTable('customer_entity_text', ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerEntityVarchars()
    {
        return $this->hasMany(CustomerEntityVarchar::className(), ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes4()
    {
        return $this->hasMany(EavAttribute::className(), ['attribute_id' => 'attribute_id'])->viaTable('customer_entity_varchar', ['entity_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDownloadableLinkPurchaseds()
    {
        return $this->hasMany(DownloadableLinkPurchased::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOauthTokens()
    {
        return $this->hasMany(OauthToken::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaypalBillingAgreements()
    {
        return $this->hasMany(PaypalBillingAgreement::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersistentSession()
    {
        return $this->hasOne(PersistentSession::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlumrocketSocialloginAccounts()
    {
        return $this->hasMany(PlumrocketSocialloginAccount::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAlertPrices()
    {
        return $this->hasMany(ProductAlertPrice::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAlertStocks()
    {
        return $this->hasMany(ProductAlertStock::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportComparedProductIndices()
    {
        return $this->hasMany(ReportComparedProductIndex::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(CatalogProductEntity::className(), ['entity_id' => 'product_id'])->viaTable('report_compared_product_index', ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportViewedProductIndices()
    {
        return $this->hasMany(ReportViewedProductIndex::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts0()
    {
        return $this->hasMany(CatalogProductEntity::className(), ['entity_id' => 'product_id'])->viaTable('report_viewed_product_index', ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewDetails()
    {
        return $this->hasMany(ReviewDetail::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesOrders()
    {
        return $this->hasMany(SalesOrder::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesruleCouponUsages()
    {
        return $this->hasMany(SalesruleCouponUsage::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoupons()
    {
        return $this->hasMany(SalesruleCoupon::className(), ['coupon_id' => 'coupon_id'])->viaTable('salesrule_coupon_usage', ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesruleCustomers()
    {
        return $this->hasMany(SalesruleCustomer::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShareDiscounts()
    {
        return $this->hasMany(ShareDiscount::className(), ['customer_id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVaultPaymentTokens()
    {
        return $this->hasMany(VaultPaymentToken::className(), ['customer_id' => 'entity_id']);
    }
}