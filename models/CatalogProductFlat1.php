<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "catalog_product_flat_all_1".
 *
 * @property int $entity_id Entity Id
 * @property int $attribute_set_id Attribute Set ID
 * @property string $type_id Type Id
 * @property int|null $age_range age_range column
 * @property string|null $age_range_value age_range column
 * @property int|null $bridge bridge column
 * @property string|null $bridge_value bridge column
 * @property int|null $color color column
 * @property string|null $color_value color column
 * @property string|null $color_details color_details
 * @property float|null $cost cost
 * @property string $created_at created_at
 * @property string|null $description description
 * @property int|null $frame_shape frame_shape column
 * @property string|null $frame_shape_value frame_shape column
 * @property int|null $frame_size frame_size column
 * @property string|null $frame_size_value frame_size column
 * @property int|null $frame_type frame_type column
 * @property string|null $frame_type_value frame_type column
 * @property int|null $frame_weight frame_weight column
 * @property string|null $frame_weight_value frame_weight column
 * @property int|null $frame_width frame_width column
 * @property string|null $frame_width_value frame_width column
 * @property int|null $gift_message_available gift_message_available column
 * @property int|null $gt_default_color gt_default_color column
 * @property string|null $gt_default_color_value gt_default_color column
 * @property int $has_options has_options
 * @property int|null $has_spring_hinges has_spring_hinges column
 * @property string|null $image image
 * @property string|null $image_label image_label
 * @property int|null $is_clip_on is_clip_on column
 * @property int|null $lens_height lens_height column
 * @property string|null $lens_height_value lens_height column
 * @property int|null $lens_width lens_width column
 * @property string|null $lens_width_value lens_width column
 * @property int|null $links_exist links_exist
 * @property int|null $links_purchased_separately links_purchased_separately
 * @property int|null $material material column
 * @property string|null $material_value material column
 * @property string|null $max_pd max_pd
 * @property int|null $max_sph max_sph column
 * @property string|null $max_sph_value max_sph column
 * @property string|null $meta_description meta_description
 * @property string|null $meta_keyword meta_keyword
 * @property string|null $meta_title meta_title
 * @property string|null $min_pd min_pd
 * @property int|null $min_sph min_sph column
 * @property string|null $min_sph_value min_sph column
 * @property float|null $msrp msrp
 * @property string|null $msrp_display_actual_price_type msrp_display_actual_price_type
 * @property string|null $name name
 * @property string|null $news_from_date news_from_date
 * @property string|null $news_to_date news_to_date
 * @property int|null $nose_bridge nose_bridge column
 * @property string|null $nose_bridge_value nose_bridge column
 * @property int|null $nose_pad nose_pad column
 * @property string|null $other_colors other_colors column
 * @property float|null $price price
 * @property int|null $price_type price_type
 * @property int|null $price_view Bundle Price View price_view column
 * @property int|null $recommend recommend column
 * @property string|null $recommend_value recommend column
 * @property int $required_options required_options
 * @property int|null $rim_type rim_type column
 * @property string|null $rim_type_value rim_type column
 * @property string|null $short_description short_description
 * @property int|null $simeple_material simeple_material column
 * @property string|null $simeple_material_value simeple_material column
 * @property string|null $sku sku
 * @property int|null $sku_type sku_type
 * @property string|null $small_image small_image
 * @property string|null $small_image_label small_image_label
 * @property string|null $special_from_date special_from_date
 * @property float|null $special_price special_price
 * @property string|null $special_to_date special_to_date
 * @property int|null $standard_lens_option standard_lens_option column
 * @property string|null $stock_sku stock_sku
 * @property int|null $st_default_color st_default_color column
 * @property string|null $st_default_color_value st_default_color column
 * @property int|null $support_php support_php column
 * @property int|null $support_progressive support_progressive column
 * @property string|null $support_progressive_value support_progressive column
 * @property int|null $support_rx support_rx column
 * @property string|null $swatch_image swatch_image
 * @property int|null $sw_featured sw_featured column
 * @property int|null $tax_class_id tax_class_id tax column
 * @property int|null $temple_length temple_length column
 * @property string|null $temple_length_value temple_length column
 * @property string|null $thumbnail thumbnail
 * @property string|null $thumbnail_label thumbnail_label
 * @property string $updated_at updated_at
 * @property string|null $url_key url_key
 * @property string|null $url_path url_path
 * @property int|null $visibility Catalog Product Visibility visibility column
 * @property float|null $weight weight
 * @property int|null $weight_type weight_type
 * @property int|null $only_blue_light_blocking only_blue_light_blocking
 */
class CatalogProductFlat1 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_product_flat_all_1';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'required'],
            [['entity_id', 'attribute_set_id', 'age_range', 'bridge', 'color', 'frame_shape', 'frame_size', 'frame_type', 'frame_weight', 'frame_width', 'gift_message_available', 'gt_default_color', 'has_options', 'has_spring_hinges', 'is_clip_on', 'lens_height', 'lens_width', 'links_exist', 'links_purchased_separately', 'material', 'max_sph', 'min_sph', 'nose_bridge', 'nose_pad', 'price_type', 'price_view', 'recommend', 'required_options', 'rim_type', 'simeple_material', 'sku_type', 'standard_lens_option', 'st_default_color', 'support_php', 'support_progressive', 'support_rx', 'sw_featured', 'tax_class_id', 'temple_length', 'visibility', 'weight_type','only_blue_light_blocking'], 'integer'],
            [['cost', 'msrp', 'price', 'special_price', 'weight'], 'number'],
            [['created_at', 'news_from_date', 'news_to_date', 'special_from_date', 'special_to_date', 'updated_at'], 'safe'],
            [['description', 'meta_keyword', 'msrp_display_actual_price_type', 'short_description'], 'string'],
            [['type_id'], 'string', 'max' => 32],
            [['age_range_value', 'bridge_value', 'color_value', 'color_details','frame_shape_value', 'frame_size_value', 'frame_type_value', 'frame_weight_value', 'frame_width_value', 'gt_default_color_value', 'image', 'image_label', 'lens_height_value', 'lens_width_value', 'material_value', 'max_pd', 'max_sph_value', 'meta_description', 'meta_title', 'min_pd', 'min_sph_value', 'name', 'nose_bridge_value', 'other_colors', 'recommend_value', 'rim_type_value', 'simeple_material_value', 'small_image', 'small_image_label', 'stock_sku', 'st_default_color_value', 'support_progressive_value', 'swatch_image', 'temple_length_value', 'thumbnail', 'thumbnail_label', 'url_key', 'url_path'], 'string', 'max' => 255],
            [['sku'], 'string', 'max' => 64],
            [['entity_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'attribute_set_id' => 'Attribute Set ID',
            'type_id' => 'Type ID',
            'age_range' => 'Age Range',
            'age_range_value' => 'Age Range Value',
            'bridge' => 'Bridge',
            'bridge_value' => 'Bridge Value',
            'color' => 'Color',
            'color_value' => 'Color Value',
            'color_details' => 'Color Details',
            'cost' => 'Cost',
            'created_at' => 'Created At',
            'description' => 'Description',
            'frame_shape' => 'Frame Shape',
            'frame_shape_value' => 'Frame Shape Value',
            'frame_size' => 'Frame Size',
            'frame_size_value' => 'Frame Size Value',
            'frame_type' => 'Frame Type',
            'frame_type_value' => 'Frame Type Value',
            'frame_weight' => 'Frame Weight',
            'frame_weight_value' => 'Frame Weight Value',
            'frame_width' => 'Frame Width',
            'frame_width_value' => 'Frame Width Value',
            'gift_message_available' => 'Gift Message Available',
            'gt_default_color' => 'Gt Default Color',
            'gt_default_color_value' => 'Gt Default Color Value',
            'has_options' => 'Has Options',
            'has_spring_hinges' => 'Has Spring Hinges',
            'image' => 'Image',
            'image_label' => 'Image Label',
            'is_clip_on' => 'Is Clip On',
            'lens_height' => 'Lens Height',
            'lens_height_value' => 'Lens Height Value',
            'lens_width' => 'Lens Width',
            'lens_width_value' => 'Lens Width Value',
            'links_exist' => 'Links Exist',
            'links_purchased_separately' => 'Links Purchased Separately',
            'material' => 'Material',
            'material_value' => 'Material Value',
            'max_pd' => 'Max Pd',
            'max_sph' => 'Max Sph',
            'max_sph_value' => 'Max Sph Value',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'meta_title' => 'Meta Title',
            'min_pd' => 'Min Pd',
            'min_sph' => 'Min Sph',
            'min_sph_value' => 'Min Sph Value',
            'msrp' => 'Msrp',
            'msrp_display_actual_price_type' => 'Msrp Display Actual Price Type',
            'name' => 'Name',
            'news_from_date' => 'News From Date',
            'news_to_date' => 'News To Date',
            'nose_bridge' => 'Nose Bridge',
            'nose_bridge_value' => 'Nose Bridge Value',
            'nose_pad' => 'Nose Pad',
            'other_colors' => 'Other Colors',
            'price' => 'Price',
            'price_type' => 'Price Type',
            'price_view' => 'Price View',
            'recommend' => 'Recommend',
            'recommend_value' => 'Recommend Value',
            'required_options' => 'Required Options',
            'rim_type' => 'Rim Type',
            'rim_type_value' => 'Rim Type Value',
            'short_description' => 'Short Description',
            'simeple_material' => 'Simeple Material',
            'simeple_material_value' => 'Simeple Material Value',
            'sku' => 'Sku',
            'sku_type' => 'Sku Type',
            'small_image' => 'Small Image',
            'small_image_label' => 'Small Image Label',
            'special_from_date' => 'Special From Date',
            'special_price' => 'Special Price',
            'special_to_date' => 'Special To Date',
            'standard_lens_option' => 'Standard Lens Option',
            'stock_sku' => 'Stock Sku',
            'st_default_color' => 'St Default Color',
            'st_default_color_value' => 'St Default Color Value',
            'support_php' => 'Support Php',
            'support_progressive' => 'Support Progressive',
            'support_progressive_value' => 'Support Progressive Value',
            'support_rx' => 'Support Rx',
            'swatch_image' => 'Swatch Image',
            'sw_featured' => 'Sw Featured',
            'tax_class_id' => 'Tax Class ID',
            'temple_length' => 'Temple Length',
            'temple_length_value' => 'Temple Length Value',
            'thumbnail' => 'Thumbnail',
            'thumbnail_label' => 'Thumbnail Label',
            'updated_at' => 'Updated At',
            'url_key' => 'Url Key',
            'url_path' => 'Url Path',
            'visibility' => 'Visibility',
            'weight' => 'Weight',
            'weight_type' => 'Weight Type',
            'only_blue_light_blocking' => 'Only Blue Light Blocking'
        ];
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getDataById($id) {
        return static::find()->where(['entity_id' => $id])->one();
    }

    public static function getAllSimples($configurable_id) {
        $ret=[];
        $simpleDatas = static::find()
            ->leftJoin('catalog_product_super_link', 'catalog_product_flat_all_1.entity_id = catalog_product_super_link.product_id')
            ->where(['parent_id' => $configurable_id])->all();
        foreach ($simpleDatas as $simpleData) {
            $d=$simpleData->toArray();
            $d["url"]=Yii::$container->get("app\models\UrlRewrite")->getSimpleUrl($d["entity_id"]);
            $ret[]=$d;
        }
        return $ret;
    }

    public static function getAllSimplesBySimpleData($configurable_id) {
        $sql="SELECT
  `e`.`entity_id`,
  `e`.`sku`,
  `link_table`.`parent_id`,
  `stock_table`.`qty`,
  `simple_prod`.`color`,
  `simple_prod`.`color_value`,
  `simple_prod`.`name`,
  `simple_prod`.`price`,
  `simple_prod`.`special_price`,
  `simple_prod`.`price_type`,
  `simple_prod`.`short_description`,
  `simple_prod`.`image`,
  `simple_prod`.`url_key`,
  `simple_prod`.`stock_sku`,
  `simple_prod`.`support_php`,
  if (`simple_prod`.`support_php` = 1,'Yes','No') as `support_php_value`,
  if (`simple_prod`.`only_blue_light_blocking` = 1,1,0) as `only_blue_light_blocking`
FROM
  `catalog_product_entity` AS e
    INNER JOIN `catalog_product_super_link` AS link_table ON `link_table`.`product_id` = `e`.`entity_id`
    INNER JOIN `cataloginventory_stock_item` AS stock_table ON `stock_table`.`product_id` = `e`.`entity_id`
    INNER JOIN `catalog_product_flat_all_1` AS simple_prod ON `simple_prod`.`entity_id` = `e`.`entity_id`
    INNER JOIN `catalog_product_entity_int` AS ext ON `ext`.`entity_id` = `link_table`.`product_id` AND store_id=0 AND ext.`attribute_id`=(select attribute_id from eav_attribute where entity_type_id=4 and attribute_code='status') AND ext.`value`=1
WHERE
stock_table.is_in_stock=1 and `link_table`.`parent_id` = %s";
        $sql=sprintf($sql,implode(",",$configurable_id));
        $query = (new \yii\db\Query())->select('*')
            ->from(['main' => "(".$sql.")"]);
        $results=$query->all();
        return $results;
    }

    public static function getAllSimplesByTag($configurable_id,$tag) {
        $sql="SELECT
  `e`.`entity_id`,
  `e`.`sku`,
  `link_table`.`parent_id`,
  `stock_table`.`qty`,
  `simple_prod`.`color`,
  `simple_prod`.`color_value`,
  `simple_prod`.`name`,
  `simple_prod`.`price`,
  `simple_prod`.`special_price`,
  `simple_prod`.`price_type`,
  `simple_prod`.`short_description`,
  `simple_prod`.`image`,
  `simple_prod`.`url_key`,
  `simple_prod`.`stock_sku`,
  `simple_prod`.`support_php`,
  if (`simple_prod`.`support_php` = 1,'Yes','No') as `support_php_value`,
  if (`simple_prod`.`only_blue_light_blocking` = 1,1,0) as `only_blue_light_blocking`,
  IFNULL(item.`tag_position`,0) AS tag_position
FROM
  `catalog_product_entity` AS e
    INNER JOIN `catalog_product_super_link` AS link_table ON `link_table`.`product_id` = `e`.`entity_id`
    INNER JOIN `cataloginventory_stock_item` AS stock_table ON `stock_table`.`product_id` = `e`.`entity_id`
    INNER JOIN `catalog_product_flat_all_1` AS simple_prod ON `simple_prod`.`entity_id` = `e`.`entity_id`
    INNER JOIN `catalog_product_entity_int` AS ext ON `ext`.`entity_id` = `link_table`.`product_id` AND store_id=0 AND ext.`attribute_id`=(select attribute_id from eav_attribute where entity_type_id=4 and attribute_code='status') AND ext.`value`=1
    INNER JOIN `pg_product_tag` tag ON tag.`product_id`=link_table.`parent_id` AND tag.`tag`='5.95'
    LEFT JOIN pg_product_tag_item item ON item.`tag_id`=tag.`id` AND item.`product_id`=simple_prod.`entity_id`
WHERE
stock_table.is_in_stock=1 and `link_table`.`parent_id` = %s
ORDER BY item.`tag_position` ASC";
        $sql=sprintf($sql,implode(",",$configurable_id));
        $query = (new \yii\db\Query())->select('*')
            ->from(['main' => "(".$sql.")"]);
        $results=$query->all();
        return $results;
    }

    public static function getSimpleLinkDatas($configurable_id) {
        $simpleLinkDatas = (new \yii\db\Query())->select('catalog_product_flat_all_1.*')
            ->from('catalog_product_super_link')
            ->leftJoin('catalog_product_flat_all_1', 'catalog_product_flat_all_1.entity_id = catalog_product_super_link.product_id')
            ->where(['parent_id' => $configurable_id])->all();
        return $simpleLinkDatas;
    }

    public static function getSimpleImages($simple_id) {
        $images = \app\components\helper\DbHelper::getSlaveDb()->createCommand("SELECT
`main`.`value` AS 'file'
FROM
`catalog_product_entity_media_gallery` AS main
INNER JOIN `catalog_product_entity_media_gallery_value_to_entity` AS entity ON `main`.`value_id` = `entity`.`value_id`
left join eav_attribute as env on env.attribute_id = main.attribute_id
WHERE
(`env`.`attribute_code` = 'media_gallery') AND (`main`.`disabled` = 0) AND (`entity`.`entity_id` = '".$simple_id."')")->queryAll();
        return $images;
    }

    public static function get_configurable_sql() {
        $sql="SELECT
`e`.`entity_id`,
`e`.`attribute_set_id`,
`e`.`type_id`,
`e`.`sku`,
`cat_index`.`category_id`,
`cat_index`.`position` AS `cat_index_position`,
`e`.`name`,
`e`.`short_description`,
`e`.`special_price`,
`e`.`image`,
`e`.`small_image`,
`e`.`thumbnail`,
`e`.`url_key`,
`e`.`bridge`,
`e`.`bridge_value`,
`e`.`temple_length`,
`e`.`temple_length_value`,
`e`.`rim_type`,
`e`.`rim_type_value`,
`e`.`frame_shape`,
`e`.`frame_shape_value`,
`e`.`frame_type`,
`e`.`frame_type_value`,
`e`.`min_pd`,
`e`.`max_pd`,
`e`.`min_sph`,
`e`.`min_sph_value`,
`e`.`max_sph`,
`e`.`max_sph_value`,
`e`.`frame_weight`,
`e`.`frame_weight_value`,
if (`e`.`support_progressive` is not null,`e`.`support_progressive`,0) as `support_progressive`,
if (`e`.`support_progressive_value` is not null,`e`.`support_progressive_value`,'No') as `support_progressive_value`,
`e`.`lens_width`,
`e`.`lens_width_value`,
`e`.`lens_height`,
`e`.`lens_height_value`,
`e`.`frame_size`,
`e`.`frame_size_value`,
`e`.`simeple_material`,
`e`.`simeple_material_value`,
`e`.`material`,
`e`.`material_value`,
`e`.`frame_width`,
`e`.`frame_width_value`,
`e`.`other_colors`,
`e`.`age_range`,
`e`.`age_range_value`,
`e`.`nose_bridge`,
`e`.`nose_bridge_value`,
`e`.`color_details`,
`e`.`created_at`,
`e`.`sw_featured`,
`e`.`featured_position`,
`e`.`sun_featured`,
`e`.`sun_featured_position`,
`e`.`trendy`,
`e`.`trendy_position`,
if (`e`.`has_spring_hinges` = 1,'Yes','No') as `has_spring_hinges`,
if (`e`.`nose_pad` = 1,'Yes','No') as `nose_pad`,
if (`e`.`is_clip_on` = 1,'Yes','No') as `is_clip_on`,
if (`e`.`support_rx` = 1,'Yes','No') as `support_rx`,
if (`e`.`color_changing` = 1,'Yes','No') as `color_changing`,
if (`e`.`recommend` is not null,`e`.`recommend`,0) as `recommend`,
if (`e`.`recommend_value` is not null,`e`.`recommend_value`,'No') as `recommend_value`,
`price_index`.`price`,
`price_index`.`final_price`,
IF(`price_index`.`tier_price` IS NOT NULL, LEAST(`price_index`.`min_price`, `price_index`.`tier_price`), `price_index`.`min_price`) AS 'minimal_price',
`price_index`.`min_price`,
`price_index`.`max_price`,
`price_index`.`customer_group_id`,
0 as relevance
FROM
`catalog_product_flat_all_1` AS e
INNER JOIN `catalog_category_product_index` AS cat_index ON `cat_index`.`product_id` = `e`.`entity_id` AND `cat_index`.`store_id` = 1 AND `cat_index`.`visibility` IN (2, 4) 
INNER JOIN `catalog_product_index_price` AS price_index ON `price_index`.`entity_id` = `e`.`entity_id` AND `price_index`.`website_id` = '1' AND `price_index`.`customer_group_id` = 0
INNER JOIN catalog_product_entity AS product on  e.entity_id = product.entity_id
INNER JOIN cataloginventory_stock_item AS stock ON stock.product_id=e.`entity_id`
WHERE
e.`entity_id` IN (SELECT entity_id FROM `catalog_product_entity_int` WHERE attribute_id=(select attribute_id from eav_attribute where entity_type_id=4 and attribute_code='status') AND store_id=0 AND `value`=1) 
AND e.`entity_id` IN (SELECT `link_table`.parent_id FROM `catalog_product_entity` AS e 
INNER JOIN `catalog_product_super_link` AS link_table ON `link_table`.`product_id` = `e`.`entity_id` 
INNER JOIN `catalog_product_entity_int` AS ext ON `ext`.`entity_id` = `link_table`.`product_id` 
INNER JOIN cataloginventory_stock_item AS stock ON stock.product_id=`link_table`.`product_id`
AND attribute_id=(select attribute_id from eav_attribute where entity_type_id=4 and attribute_code='status') AND ext.`value`=1 AND stock.is_in_stock=1)
AND stock.is_in_stock=1";
        return $sql;
    }

    public static function getAllInStockConfigurableProducts($page=0,$pagecount=10,$condition=[]) {
        $all_product_sql = static::get_configurable_sql();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $profile_category_ids=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["profile_category_ids"];
        if (isset($condition["tag"])) {
            $sql = sprintf("select main.*,tag.`tag`,tag.`tag_position` from (%s) main INNER JOIN pg_product_tag tag ON tag.product_id=main.entity_id WHERE category_id in (%s)",$all_product_sql,implode(",",$profile_category_ids));
        } else {
            $sql = sprintf("select main.* from (%s) main WHERE category_id in (%s)",$all_product_sql,implode(",",$profile_category_ids));
        }
        $query = (new \yii\db\Query())->select('*')
            ->from(['main' => "(".$sql.")"])
            ->offset($page * $pagecount)->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            if ($c!="tag") {
                $query->andWhere(sprintf("%s like :%s",$c,$c));
                $params[":".$c]="%%".$v."%%";
            } else {
                $query->andWhere(sprintf("%s = :%s",$c,$c));
                $params[":".$c]=$v;
            }
        }
        $query->addParams($params);
        if (isset($condition["tag"])) {
            $query->orderBy(['tag_position' => SORT_ASC]);
        }
        $results=$query->all();
        return $results;
    }

    public static function getAllInStockConfigurableProductsCount($condition=[]) {
        $all_product_sql = static::get_configurable_sql();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $profile_category_ids=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["profile_category_ids"];
        $sql = sprintf("select * from (%s) main WHERE category_id in (%s)",$all_product_sql,implode(",",$profile_category_ids));
        $query = (new \yii\db\Query())->select('*')
            ->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if ($c!="tag") {
                $query->andWhere(sprintf("%s like :%s",$c,$c));
                $params[":".$c]="%%".$v."%%";
            } else {
                $query->andWhere(sprintf("entity_id in (select product_id from pg_product_tag where tag=:tag)",$c,$c));
                $params[":".$c]=$v;
            }
        }
        $query->addParams($params);
        $cnt=$query->count();
        return $cnt;
    }

    public static function getListSQLByTypeIds($type_ids) {
        $type_ids_str="";
        for($i=0;$i<count($type_ids);$i++) {
            $type_id=$type_ids[$i];
            if ($i==0) {
                $type_ids_str="'".$type_id."'";
            } else {
                $type_ids_str=$type_ids_str.",'".$type_id."'";
            }
        }
        $sql="SELECT 1 AS `status`, `e`.`entity_id`, `e`.`attribute_set_id`, `e`.`type_id`, `e`.`created_at`, `e`.`updated_at`, `e`.`sku`, `e`.`stock_sku`, `e`.`name`, `e`.`short_description`, `e`.`special_price`, `e`.`special_from_date`, `e`.`special_to_date`, `e`.`meta_title`, `e`.`meta_keyword`, `e`.`meta_description`, `e`.`image`, `e`.`small_image`, `e`.`thumbnail`, `e`.`color`, `e`.`color_value`, `e`.`news_from_date`, `e`.`news_to_date`, `e`.`required_options`, `e`.`image_label`, `e`.`small_image_label`, `e`.`thumbnail_label`, `e`.`url_key`, `e`.`msrp`, `e`.`msrp_display_actual_price_type`, `e`.`links_purchased_separately`, `e`.`links_exist`, `e`.`price_type`, `e`.`weight_type`, `e`.`price_view`, `e`.`swatch_image`, `e`.`tax_class_id`, `e`.`other_colors`, `e`.`nose_pad`, `e`.`rim_type`, `e`.`rim_type_value`, `e`.`frame_shape`, `e`.`frame_shape_value`, `e`.`frame_type`, `e`.`frame_type_value`, `e`.`material`, `e`.`material_value`, `e`.`min_pd`, `e`.`max_pd`, `e`.`sw_featured`, `e`.`frame_weight`, `e`.`frame_weight_value`, `e`.`support_progressive`, `e`.`support_progressive_value`, `e`.`nose_bridge`, `e`.`nose_bridge_value`, `e`.`bridge`, `e`.`bridge_value`, `e`.`lens_width`, `e`.`lens_width_value`, `e`.`lens_height`, `e`.`lens_height_value`, `e`.`frame_width`, `e`.`frame_width_value`, `e`.`temple_length`, `e`.`temple_length_value`, `e`.`frame_size`, `e`.`frame_size_value`, `e`.`min_sph`, `e`.`min_sph_value`, `e`.`max_sph`, `e`.`max_sph_value`, `e`.`simeple_material`, `e`.`simeple_material_value`, `e`.`recommend`, `e`.`recommend_value`, `e`.`color_details`, `e`.`support_rx`, `e`.`has_spring_hinges`, `e`.`age_range`, `e`.`age_range_value`, `e`.`is_clip_on`, `e`.`retired`, `e`.`clipon`, `e`.`restockable_in_days`, `e`.`trendy`, `e`.`trendy_position`, `e`.`access_high`, `e`.`access_length`, `e`.`access_width`, `e`.`color_changing`, `e`.`clip_total_width`, `e`.`clip_lens_height`, `e`.`clip_lens_width`, `e`.`clip_nasal_distance`, `e`.`access_material`, `e`.`access_specifications`, `e`.`clip_middle_beam`, `e`.`low_bridge_fit`, `price_index`.`price`, `price_index`.`final_price`, IF(price_index.tier_price IS NOT NULL, LEAST(price_index.min_price, price_index.tier_price), price_index.min_price) AS `minimal_price`, `price_index`.`min_price`, `price_index`.`max_price`, `price_index`.`tier_price` 
FROM `catalog_product_flat_all_1` AS `e`
INNER JOIN `catalog_product_index_price` AS `price_index` ON price_index.entity_id = e.entity_id AND price_index.website_id = '1' AND price_index.customer_group_id = 0
AND e.type_id in ($type_ids_str)";
        return $sql;
    }
}
