<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_s3_file".
 *
 * @property int $id
 * @property string $content_type
 * @property string|null $obj_type
 * @property int $obj_id
 * @property int $customer_id
 * @property int $profile_id
 * @property string|null $profile_name
 * @property string|null $sku
 * @property string|null $additional_data
 * @property string|null $server_domain
 * @property string|null $file_path
 * @property string|null $created_at
 */
class PgCouponCodeInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_organization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'org_id', 'region_id','is_active'], 'integer'],
            [['coupon_code','firstname','lastname','street','city','region','country_id','postcode','telephone','create_admin_id','update_admin_id'], 'string'],
            [['create_at','update_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coupon_code' => 'Coupon Code',
            'org_id' => 'Org ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'street' => 'Street',
            'city' => 'City',
            'region' => 'Region',
            'region_id' => 'Region ID',
            'country_id' => 'Country ID',
            'postcode' => 'Postcode',
            'telephone' => 'Telephone',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'create_admin_id' => 'Create Admin ID',
            'update_admin_id' => 'Update Admin ID',
            'is_active' => 'Is Active',
        ];
    }


    //孙进超 20/8/25
    public static function getListSQL() {
        $sql="SELECT pcci.id, pcci.coupon_code, pcci.org_id, pcci.firstname, pcci.lastname, pcci.street, pcci.city, pcci.region, pcci.region_id, pcci.country_id, pcci.postcode, pcci.telephone, pcci.create_at, pcci.update_at, pcci.create_admin_id, pcci.update_admin_id, pcci.is_active, po.entity_id AS po_entity_id, po.org_type AS po_org_type, po.org_name AS po_org_name, po.org_code AS po_org_code, po.org_description AS po_org_description, po.street AS po_street, po.city AS po_city, po.region AS po_region, po.postcode AS po_postcode, po.country AS po_country, po.telephone AS po_telephone, po.is_active AS po_is_active, po.fax AS po_fax, po.created_at AS po_created_at FROM pg_coupon_code_info AS pcci INNER JOIN pg_organization AS po GROUP BY pcci.id";
        return $sql;
    }

    //获取pg_organization数据并分页
    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'is_active'){
                    $query->andWhere(sprintf("is_active = :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'telephone'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'coupon_code'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'org_name'){
                    $query->andWhere(sprintf("po_entity_id = :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'region_id'){
                    $query->andWhere(sprintf("region_id = :%s",$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }
    //获取urlrewrite数据数量
    public static function getListCount($condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'is_active'){
                    $query->andWhere(sprintf("is_active = :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'telephone'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'coupon_code'){
                    $query->andWhere(sprintf("%s like :%s",$c,$c));
                    $params[":".$c]="%%".$v."%%";
                }
                if($c == 'org_name'){
                    $query->andWhere(sprintf("po_entity_id = :%s",$c));
                    $params[":".$c]=$v;
                }
                if($c == 'region_id'){
                    $query->andWhere(sprintf("region_id = :%s",$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $data=$query->count();
        return $data;
    }

//    数据修改
    public static function active($id,$is_active){
        if($is_active == 1){
            $is_active = 0;
        }else{
            $is_active = 1;
        }
        $sql = "update pg_coupon_code_info set is_active = $is_active where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
    }
    public static function fetchOne($id){
        $sql = "select * from pg_coupon_code_info where id = $id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $data;
    }
    //    数据修改
    public static function toupdate($data=[]){
        $id = $data['id'];
        unset($data['id']);
        $vals = '';
        foreach ($data as $key => $val){
            $vals .= '`'.$key.'`=\''.$val.'\',';
        }
        $vals = rtrim($vals,',');
        $sql = "update pg_coupon_code_info set $vals where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
//    数据添加
    public static function toinsert($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into pg_coupon_code_info($keys) values ($vals)";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
}