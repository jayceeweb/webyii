<?php
namespace app\models;

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_score_rule_detail".
 *
 * @property int $item_id
 * @property int $score_rule_id
 * @property int $customer_group_id
 * @property int $list_score
 * @property int $special_score
 * @property int $birthday_score
 * @property string|null $from_date
 * @property string|null $to_date
 * @property string $created_at
 */
class PgScoreRuleDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_score_rule_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['score_rule_id', 'customer_group_id', 'list_score', 'special_score', 'birthday_score'], 'integer'],
            [['from_date', 'to_date', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'score_rule_id' => 'Score Rule ID',
            'customer_group_id' => 'Customer Group ID',
            'list_score' => 'List Score',
            'special_score' => 'Special Score',
            'birthday_score' => 'Birthday Score',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'created_at' => 'Created At',
        ];
    }

    public static function getRuleDetailBySkuId($sku_id) {
        $sql=sprintf("SELECT detail.*,g.customer_group_code FROM `pg_score_rule_detail` detail
INNER JOIN `customer_group` g ON g.`customer_group_id`=detail.`customer_group_id`
INNER JOIN pg_score_rule score ON score.id=detail.score_rule_id
WHERE score.`sku_id`=%s",$sku_id);
        $list=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $list;
    }
}

