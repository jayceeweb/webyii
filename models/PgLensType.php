<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_lens_type".
 *
 * @property int $id
 * @property string|null $lens_type
 * @property string|null $lens_type_code
 * @property string|null $frame_group 0:1:rimless;2:full rim;3:semi rimless;4:sunglasses rimless
 * @property string|null $name
 * @property string|null $mark
 * @property string|null $lens_type_route
 * @property string|null $lens_type_dest_name
 * @property string|null $facebook_lens_type_dest_name
 * @property string|null $lens_type_dest_sku
 * @property string|null $price_included
 * @property string|null $clipon_price_included
 */
class PgLensType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_lens_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_included','clipon_price_included'], 'string'],
            [['lens_type', 'lens_type_code', 'mark','lens_type_dest_name','facebook_lens_type_dest_name','lens_type_dest_sku'], 'string', 'max' => 64],
            [['frame_group', 'name', 'lens_type_route'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lens_type' => 'Lens Type',
            'lens_type_code' => 'Lens Type Code',
            'frame_group' => 'Frame Group',
            'name' => 'Name',
            'mark' => 'Mark',
            'lens_type_route' => 'Lens Type Route',
            'lens_type_dest_name' => 'Len Type Dest Name',
            'facebook_lens_type_dest_name' => 'Facebook Len Type Dest Name',
            'lens_type_dest_sku' => 'Len Type Dest Sku',
            'price_included' => 'Price Included',
            'clipon_price_included'=>'Clipon Price Included'
        ];
    }

    public static function getDataByLensTypeAndFrameGroup($lens_type,$frame_group) {
        $data=static::findOne(["lens_type_code"=>$lens_type,"frame_group"=>$frame_group]);
        return $data;
    }

    public static function getPriceIncluded($lens_type,$frame_group,$is_clip_on=false) {
        $d=static::getDataByLensTypeAndFrameGroup($lens_type,$frame_group);
        if ($is_clip_on) {
            return $d["clipon_price_included"];
        } else {
            return $d["price_included"];
        }
    }

    //孙进超 20/3/31
    public static function getListSQL() {
        $sql="SELECT * FROM `pg_lens_type`";
        return $sql;
    }

    //获取lenstype数据并分页
    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->orderBy(["id"=>SORT_DESC])->limit($pagecount);
        $params=[];
        foreach ($condition as $c=>$v) {
            if(!empty($v)){
                $query->andWhere(sprintf("%s = :%s",$c,$c));
                $params[":".$c]=$v;
            }
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }

//    获取数据个数
    public static function getListCount($condition=[]) {
        $sql=static::getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $params=[];
        foreach ($condition as $c=>$v) {
            if(!empty($v)){
                $query->andWhere(sprintf("%s = :%s",$c,$c));
                $params[":".$c]=$v;
            }
        }
        $query->addParams($params);
        $data=$query->count();
        return $data;
    }
//    根据前台所返回的数组获取分类下的数据
    public static function getentityType($arr){
        $data = array();
        foreach ($arr as $key => $val){
            $sql="select DISTINCT($val) from pg_lens_type";
            $data[$val]=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        }
        return $data;
    }
//    数据修改
    public static function toupdate($data=[]){
        $id = $data['id'];
        unset($data['id']);
        $vals = '';
        foreach ($data as $key => $val){
            $vals .= '`'.$key.'`=\''.$val.'\',';
        }
        $vals = rtrim($vals,',');
        $sql = "update pg_lens_type set $vals where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
//    数据添加
    public static function toinsert($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into pg_lens_type($keys) values ($vals)";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
//    数据删除
    public function todelete($id){
        $sql = "delete from pg_lens_type where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
}