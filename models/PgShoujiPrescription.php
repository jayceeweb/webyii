<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pg_shouji_prescription".
 *
 * @property int $entity_id Entity Id
 * @property int $admin_id
 * @property string|null $prescription_name Prescription Name
 * @property float|null $rsph Rsph
 * @property float|null $lsph Lsph
 * @property float|null $rcyl Rcyl
 * @property float|null $lcyl Lcyl
 * @property int|null $rax Rax
 * @property int|null $lax Lax
 * @property float|null $rpri Right Prism
 * @property float|null $lpri Left Prism
 * @property string|null $rbase Right Base
 * @property string|null $lbase Left Base
 * @property float|null $radd Rgiht Add
 * @property float|null $ladd Left Addd
 * @property float|null $pd PD
 * @property int|null $single_pd Single Pd
 * @property float|null $rpd Right Pd
 * @property float|null $lpd Left Pd
 * @property string $created_at Created At
 * @property string $updated_at Updated At
 * @property float|null $rpri_1 Right Prism1
 * @property float|null $lpri_1 Left Prism1
 * @property string|null $rbase_1 Right Base1
 * @property string|null $lbase_1 Left Base1
 */
class PgShoujiPrescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pg_shouji_prescription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_id', 'rax', 'lax', 'single_pd'], 'integer'],
            [['rsph', 'lsph', 'rcyl', 'lcyl', 'rpri', 'lpri', 'radd', 'ladd', 'pd', 'rpd', 'lpd', 'rpri_1', 'lpri_1'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['prescription_name', 'rbase', 'lbase', 'rbase_1', 'lbase_1'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_id' => 'Entity ID',
            'admin_id' => 'Admin ID',
            'prescription_name' => 'Prescription Name',
            'rsph' => 'Rsph',
            'lsph' => 'Lsph',
            'rcyl' => 'Rcyl',
            'lcyl' => 'Lcyl',
            'rax' => 'Rax',
            'lax' => 'Lax',
            'rpri' => 'Rpri',
            'lpri' => 'Lpri',
            'rbase' => 'Rbase',
            'lbase' => 'Lbase',
            'radd' => 'Radd',
            'ladd' => 'Ladd',
            'pd' => 'Pd',
            'single_pd' => 'Single Pd',
            'rpd' => 'Rpd',
            'lpd' => 'Lpd',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'rpri_1' => 'Rpri 1',
            'lpri_1' => 'Lpri 1',
            'rbase_1' => 'Rbase 1',
            'lbase_1' => 'Lbase 1',
        ];
    }

    public static function getListCount($condition=[]) {
        $cnt=static::getListFilter($condition)->count();
        return $cnt;
    }

    public static function getList($page=0,$pagecount=10,$condition=[]) {
        $query=static::getListFilter($condition)
            ->offset(($page -1) * $pagecount)
            ->orderBy(["entity_id"=>SORT_DESC])
            ->limit($pagecount);
        $data=$query->all();
        return $data;
    }

    public static function getListFilter($condition=[]) {
        $query=(new \yii\db\Query())->select(['p.*'])->from(["p"=>"pg_shouji_prescription"]);
        $query->andWhere(sprintf(sprintf("p.admin_id=%s", Yii::$app->admin->getId())));
        return $query;
    }
}