<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_group".
 *
 * @property int $customer_group_id Customer Group Id
 * @property string $customer_group_code Customer Group Code
 * @property int $tax_class_id Tax Class Id
 *
 * @property CatalogProductBundlePriceIndex[] $catalogProductBundlePriceIndices
 * @property CatalogProductEntityTierPrice[] $catalogProductEntityTierPrices
 * @property CatalogProductIndexPrice[] $catalogProductIndexPrices
 * @property CatalogProductIndexTierPrice[] $catalogProductIndexTierPrices
 * @property CatalogruleCustomerGroup[] $catalogruleCustomerGroups
 * @property Catalogrule[] $rules
 * @property CatalogruleGroupWebsite[] $catalogruleGroupWebsites
 * @property SalesruleCustomerGroup[] $salesruleCustomerGroups
 * @property Salesrule[] $rules0
 * @property SalesruleProductAttribute[] $salesruleProductAttributes
 */
class CustomerGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_group_code'], 'required'],
            [['tax_class_id'], 'integer'],
            [['customer_group_code'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customer_group_id' => 'Customer Group ID',
            'customer_group_code' => 'Customer Group Code',
            'tax_class_id' => 'Tax Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProductBundlePriceIndices()
    {
        return $this->hasMany(CatalogProductBundlePriceIndex::className(), ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProductEntityTierPrices()
    {
        return $this->hasMany(CatalogProductEntityTierPrice::className(), ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProductIndexPrices()
    {
        return $this->hasMany(CatalogProductIndexPrice::className(), ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProductIndexTierPrices()
    {
        return $this->hasMany(CatalogProductIndexTierPrice::className(), ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogruleCustomerGroups()
    {
        return $this->hasMany(CatalogruleCustomerGroup::className(), ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRules()
    {
        return $this->hasMany(Catalogrule::className(), ['rule_id' => 'rule_id'])->viaTable('catalogrule_customer_group', ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogruleGroupWebsites()
    {
        return $this->hasMany(CatalogruleGroupWebsite::className(), ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesruleCustomerGroups()
    {
        return $this->hasMany(SalesruleCustomerGroup::className(), ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRules0()
    {
        return $this->hasMany(Salesrule::className(), ['rule_id' => 'rule_id'])->viaTable('salesrule_customer_group', ['customer_group_id' => 'customer_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesruleProductAttributes()
    {
        return $this->hasMany(SalesruleProductAttribute::className(), ['customer_group_id' => 'customer_group_id']);
    }
}