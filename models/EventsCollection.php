<?php
/**
 * 数据埋点具体事件数据模型
 */
namespace app\models;

use Yii;
use app\components\helper\DbHelper;

/**
 * This is the model class for table "pg_score".
 *
 * @property int $entity_id
 * @property int $total_score
 */
class EventsCollection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events_collection';
    }

    public static function getDb()
    {
        return Yii::$app->collectiondb;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_id'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'customer_id' => 'Customer Id'
        ];
    }


    //新增数据
    public static function addData($data) {
        $db = DbHelper::getCollectionDb()->createCommand();
        return $db->insert(self::tableName(), $data)->execute();
    }

    //修改数据
    public static function editDate($id, $data) {
        $condition = "id=:id";
        $params = [':id'=>$id];
        $db = DbHelper::getCollectionDb()->createCommand();
        return $db->update(self::tableName(), $data, $condition, $params)->execute();
    }

}
?>