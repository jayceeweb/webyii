<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "catalog_product_super_link".
 *
 * @property int $link_id Link ID
 * @property int $product_id Product ID
 * @property int $parent_id Parent ID
 *
 * @property CatalogProductEntity $parent
 * @property CatalogProductEntity $product
 */
class CatalogProductSuperLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_product_super_link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'parent_id'], 'integer'],
            [['product_id', 'parent_id'], 'unique', 'targetAttribute' => ['product_id', 'parent_id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogProductEntity::className(), 'targetAttribute' => ['parent_id' => 'entity_id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogProductEntity::className(), 'targetAttribute' => ['product_id' => 'entity_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'link_id' => 'Link ID',
            'product_id' => 'Product ID',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CatalogProductEntity::className(), ['entity_id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(CatalogProductEntity::className(), ['entity_id' => 'product_id']);
    }

    public function getSimplesData() {
        return $this->hasMany(CatalogProductFlat1::className(), ['entity_id' => 'parent_id']);
    }

    public static function getDataByProductId($productId) {
        return static::find()->where(['product_id' => $productId])->one();
    }
}
