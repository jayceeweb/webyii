<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
// echo $collection_enable;
?>
<!DOCTYPE html>
<html lang="en-US" class>
<head>
    <?php
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
    echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("anti_flicker")->toHtml();
    ?>
    <!-- ======= Basic configuration ======= -->
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport"
          content="width=device-width,
                   initial-scale=1.0,
                   maximum-scale=1.0,
                   user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- ======= Basic configuration End ======= -->

    <!-- ======= Web Description Configuration ====== -->
        <meta name="Description" content="<?php echo $currentSimpleData["meta_description"];?>"/>
        <meta name="robots" content="INDEX,FOLLOW"/>
        <meta property="og:url" content="<?php echo $base_url; ?>/<?php echo $simple_url;?>"/>
        <meta property="og:description" content="<?php echo $currentSimpleData["meta_description"];?>"/>
        <meta property="og:title" content="<?php echo $currentSimpleData["meta_title"];?>"/>
        <meta name="keywords" content="<?php echo $currentSimpleData["meta_keyword"];?>">
        <title><?php echo $currentSimpleData["meta_title"];?></title>
    <!-- ======= Web Description Configuration End ====== -->

    <!-- ======= Css File Import ====== -->
<!--    <link rel="stylesheet" href="--><?php //echo $static_url; ?><!--/assets2/resource/version--><?php //echo $js_version; ?><!--/pdp-summary/fonts/pdp-revision/fontawesome/css/all.min.css">-->
    <link href="/resource/header/fonts/fontawesome/css/all.min.css" type="text/css" rel="stylesheet">
    <link rel="canonical" href="<?php echo "/".$simple_url; ?>"/>
    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/media/favicon/stores/1/favIcon.ico?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/icon-fonts/css/porto-icons.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/icon-fonts/css/porto-icons-codes.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/bootstrap.min.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/swiper.min.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/new_view.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/new_media.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/notes.css?v=<?php echo $js_version; ?>"> <!--Advertisement-->
    <link rel="stylesheet" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/reviews.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/awesomeFont.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/advertising.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/fonts/iconfont.css?v=<?php echo $js_version; ?>">
    <!-- ======= Css File Import End ====== -->

    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/footer_new.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/max-width-700.css?v=<?php echo $js_version; ?>" media="screen and (max-width:990px)">
    <!--新footer引入css-->
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/footer_2020.css?v=<?php echo $js_version; ?>">
    <!--新footer引入css ENd-->

    <script src="<?php echo $static_url; ?>/assets2/resource/js/jquery-1.12.3.min.js?v=<?php echo $js_version; ?>"></script>
    <?php
    echo $resultLayoutFactory->create()->getLayout()
        ->setCacheable(false)
        ->createBlock('Pg\CatalogExtend\Block\Klaviyo')
        ->setTemplate('Pg_CatalogExtend::klaviyo.phtml')
        ->toHtml();
    ?>
    <style>
        .social-icons li {
            width: auto !important;
            margin-left: 20px;
        }
        .social-icons li {
            float: left;
        }
        .social-icons>li>a {
            background-color: transparent !important;
            border: 1px solid #ccc !important;
        }
        .social-icons li a {
            border: 2px solid #9e9e9e;
            text-decoration: none;
            border-radius: 50%;
            padding: 6px;
            height: 30px;
            width: 30px;
            display: block;
            position: relative;
        }
        .social-icons li a em {
            position: absolute;
            left: 2px;
            top: 5px;
        }
        .porto-icon-facebook:before, .porto-icon-twitter:before, .porto-icon-instagram:before, .porto-icon-pinterest:before {
            font-size: 18px;
            color: #ccc;
        }
        .social-icons li:nth-child(1) a:hover {
            background-color: #3c599b !important;
            color: #fff;
        }
        .social-icons li:nth-child(2) a:hover {
            background-color: #1ca8e3 !important;
            color: #fff;
        }
        .social-icons li:nth-child(3) a:hover {
            background-color: #c867e0 !important;
            color: #fff;
        }
        .social-icons li:nth-child(4) a:hover {
            background-color: #e40404;
            color: #fff;
        }
    </style>
</head>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','<?php echo $gtm_value ?>');</script>
<body>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm_value ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- === load === -->
<div class="loading-mask hide" data-role="loader" style="display: block;" id="loading-loade">
    <img alt="load..." src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/images/loader-2.gif"><p>
</div>
<!-- === load End ===-->
<!-- ===== Comment filling area ====== -->
<div id="fix-fluid">
    <div class="container-fluid reveiws-container">
        <font style="vertical-align: inherit;" class="close-fluid">×</font>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-h3">
                <h3>Review the <span class="zx-name">Oval Glasses</span> <span class="zx-sku"></span></h3>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-price">
                <p id="zx-price"></p>
                <div></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <img src="/media/catalog/product/<?php echo $currentSimpleData['thumbnail']?>" alt="" class="imgs"
                >
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 reveiws-frame">
                <h6>Tell us about this frame.</h6>
                <!--<p>Now that you’ve had a chance to test-drive your recently purchased Pg, we’d love to know your thoughts. Please focus on what you liked or didn’t like about this frame. Your feedback will help other Pg customers who are interested in the same product.</p>-->
            </div>
        </div>
        <div class="reviews-conent">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses1">
                    How would you rate your glasses? <span>*</span>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 reveiws-wjx">
                    <ul data-required="true" class="acs_rating_holder acs_js_rating_holder">
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-1" data-value="1">
                            <span class="fa fa-star-o fas"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-2" data-value="2">
                            <span class="fa fa-star-o fas"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-3" data-value="3">
                            <span class="fa fa-star-o fas"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-4" data-value="4">
                            <span class="fa fa-star-o fas"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-5" data-value="5">
                            <span class="fa fa-star-o fas"></span></li>
                    </ul>
                    <span class="product_to">Click to rate this product.</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses2">
                    Your Review Headline <span>*</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-productInput">
                    <textarea name="" id="YourReview" cols="30"  placeholder="e.g. Excellent product..." rows="10"></textarea>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-show">
                    <span id="reveiws-btn"> Show me an example </span>
                    <i class="fa fa-chevron-down" id="upDown"></i>
                    <div class="acs-headline-examples" style="">
                        <p>"Don't judge a book by its cover"</p>
                        <p>"I would buy this product again and again"</p>
                        <p>"Too much whiz and not enough bang"</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses3">
                    Describe your overall product experience <span>*</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-textarea">
                    <textarea name="" id="describe" cols="30" rows="10"  data-maxlength="5000" placeholder="Describe your overall product experience (Required)" style="resize:none" ></textarea>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-words">
                     <span>
                        Characters Remaining:
                         <a id="DescribeNum">5000</a>
                     </span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses4">
                    Would you recommend this product to a friend?
                    <span>*</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reveiws-yN">
                    <fieldset class="acs_fieldset acs_fieldset_click">
                                <span val="yes">
                                        <span class="fa fa-thumbs-o-up" id="reveiws-box"></span>
                                        <a class="rec0 rec">Yes</a>
                                </span>
                        <span val="no">
                                <span class="fa fa-thumbs-o-down" id="reveiws-box"></span>
                                <a  class="rec1">No</a>
                            </span>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 reveiws-nickname">
                <div >
                    <p>Nickname
                        <a class="zx-xh">*</a>
                    </p>
                    <textarea name=""  cols="30" rows="10" id="Nickname"></textarea>
                    <!--                <textarea type="text" >-->
                    <span>Avoid using your full name. (Required)</span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 reveiws-nickname">
                <div>
                    <p>Email Address
                        <a class="zx-xh">*</a>
                    </p>
                    <textarea name="" id="Email_S" cols="30" rows="10"></textarea>
                    <!--                <input type="text" id="">-->
                    <span id="errTips">This will only be used for notification purposes</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-submit">
                <button id="reveiws-cancel">CANCEL</button>
                <button id="reveiws-sub">SUBMIT</button>
            </div>
        </div>
    </div>
</div>
<!-- ===== Comment filling area End====== -->

<!-- ====== reviews Success Popup ====== -->
<div class="revSuccess"></div>
<!-- ====== reviews Success Popup End ====== -->

<!--Advertisement Start-->
<div class="message">
    <div id="message_show">
        <div class='message_container'>
            <div class="message_two_child owl-carousel owl-theme">

            </div>
        </div>
    </div>
</div>
<!--Advertisement End-->
<?php
//<!-- ======= Head Lntroduction ======= -->
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>
<div class="breadcrumbs" id="breadcrumbs" style="z-index:99999">
    <ul class="items">
        <li class="item home item_home">
            <a href="<?php echo $base_url; ?>" title="Go to Home Page">
                Home
            </a>
            <i class="iconfont icon-arrow-right-copy header_icon"></i>
        </li>
        <li class="item category6">
            <a href="<?php echo $base_url; ?>/accessories.html" title="">
                Accessories
            </a>
            <i class="iconfont icon-arrow-right-copy header_icon"></i>
        </li>
        <li class="item product">
            <a title="">
                <?php echo $currentSimpleData['name'];?>
            </a>
        </li>
    </ul>
</div>

<div class="minicart-bg1"></div>
<!-- ======= Head Lntroduction End ======= -->



<a href="javascript:;" id="totop"><span class="iconfont totop_back"></span></a>
<a href="javascript:;" id="botop"><em class="porto-icon-up-open"></em></a>
<div class="assigned"></div>

<div class="row">
    <div class="col-md-8 box">
        <div class="swiper-container swiper-container-one">
            <div class="swiper-wrapper">
                <?php foreach ($images as $image):?>
                <div class="swiper-slide simple_slide">
                    <img src="<?php echo ('/media/catalog/product'.$image['file'])?>" alt="">
                </div>
                <?php endforeach;?>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
    <div class="col-md-4 box2" itemscope itemtype="http://schema.org/Product">
        <h3>
            <?php echo $currentSimpleData['name'];?>
            <!-- Load Facebook SDK for JavaScript -->
            <!--facekbook like button-->
            <span id="fb-root"></span>
            <!-- <script>(function(d, s, id) {-->
            <!-- var js, fjs = d.getElementsByTagName(s)[0];-->
            <!-- if (d.getElementById(id)) return;-->
            <!-- js = d.createElement(s); js.id = id;-->
            <!-- js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";-->
            <!--  fjs.parentNode.insertBefore(js, fjs);-->
            <!-- }(document, 'script', 'facebook-jssdk'));</script>-->
            <script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0"></script>
            <span class="fb-like padding-left15" data-href=""
                  data-layout="button_count" data-action="like" data-size="small" data-show-faces="true"
                  data-share="false" style="display: inline-block; line-height: 1"></span>
        </h3>
        <span class="Frame_mobel">Accessory Color:</span>
        <div class="FrameColor" entryId="<?php echo $currentSimpleData["entity_id"];?>">
            <?php $product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($id);$qty = $product->getData()['quantity_and_stock_status']['qty'];?>
            <span class="Frame_pc">Accessory Color:</span>
            <?php foreach ($simpleDatas as $simpleData): ?>
            <ul entryId="<?php echo $simpleData["entity_id"];?>"
                product="<?php echo $configableData['entity_id'];?>"
                color_id="<?php echo $simpleData["color"];?>"
                thumbnail="/media/catalog/product/<?php echo $simpleData["thumbnail"];?>"
                name = "<?php echo $simpleData["name"];?>"
                price="<?php echo $simpleData["price"];?>"
                special_price="<?php echo $simpleData["special_price"];?>"
                sku = "<?php echo $simpleData["sku"];?>"
                url = "<?php echo $base_url; ?>/<?php echo $simpleData["url"];?>"
                stock = "<?php
                    $isStock = $product->getData()['quantity_and_stock_status']['is_in_stock'];
                    if($isStock == 1) {
                        echo "IN STOCK";
                    }else{
                        echo "OUT OF STOCK";
                    }
                    ?>"
                class="fram_list">
                <?php if($currentSimpleData['entity_id'] == $simpleData["entity_id"]):?>
                    <div class="FrameColor_select">
                        <img src="/media/catalog/product/cache/230x115<?php echo str_replace("\\",'',$simpleData['image']) ?>" alt="">
                    </div>
                    <p class="selector_color"><?php echo $simpleData['color_value']?></p>
                <?php else:?>
                    <div>
                        <img src="/media/catalog/product/cache/230x115<?php echo str_replace("\\",'',$simpleData['image']) ?>" alt="">
                    </div>
                    <p><?php echo $simpleData['color_value']?></p>
                <?php endif;?>
            </ul>
            <?php endforeach; ?>
        </div>
        <?php $product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($id);$isStock = $product->getData()['quantity_and_stock_status']['is_in_stock'];?>
        <div class="stock_s">
            <li class="stock">OUT OF STOCK</li>
            <li>
                <b>SKU# </b>
                <span class="cur_sku" itemprop="sku"><?php echo $currentSimpleData['sku']?></span>
            </li>
            <div class="stock_price">
                <?php if(
                    $currentSimpleData['special_price'] &&
                    $currentSimpleData['special_price'] < $currentSimpleData['price'] &&
                    ceil($currentSimpleData['special_price']) != 0
                ):?>
                <s>$<?php echo number_format(round($currentSimpleData['price'],2),2,".","")?></s>
                    <span>$<?php echo number_format(round($currentSimpleData['special_price'],2),2,".","")?></span>
                <?php else:?>
                    <span>$<?php echo number_format(round($currentSimpleData['price'],2),2,".","")?></span>
                <?php endif;?>
        </div>
        <div class="sm">
            <?php echo $simpleData['description']?>
        </div>

        <div class="save_addTo">
            <div id="save_x" class="js-order-ws" currentid="<?php echo $currentSimpleData['entity_id'];?>">
                Save
            </div>
            <div class="add_to_cart" id="orderRx">Add To Cart</div>
            <p id="tip_save">Item has been removed for NEW NAME</p>
        </div>
        <div class="share_parent">
            <span>Share:</span>
            <span class="fb-share  porto-icon-facebook fb-share_class"></span>
            <span class="tw-share porto-icon-twitter tw-share_class"></span>
            <span class="pint-share porto-icon-pinterest pint-share_class"></span>
        </div>
        <!-- === Add to REVIEWS === -->
        <div class="border-top border-top-none-lg bg-gray-lightest padding-top-30 padding-left-10-xs padding-right-10-xs padding-bottom-20 padding-left-15-sm padding-right-15-sm">
            <div class="row margin-0">
                <div class="pdp-info col-md-6 col-lg-12 clearfix">
                    <h1 itemprop="name" class="col-xs-12 col-sm-10 col-md-12 color-gray-darker padding-left-0 padding-right-15 padding-top-0 padding-bottom-10 padding-left-0-lg padding-right-30-lg margin-top-0 productName center-block-sm pull-none-sm h2" style="opacity: 0"><?php echo $currentSimpleData['name'];?></h1>
                    <div class="visible-sm-table bg-white padding-15-xs margin-bottom-15-xs padding-15-sm margin-bottom-15-sm col-xs-12 col-sm-10 col-md-12 center-block-sm pull-none-sm margin-bottom-20-sm padding-left-30-lg padding-right-30-lg padding-top-25-lg padding-bottom-25-lg margin-bottom-25-lg">
                        <div class="visible-sm-table-cell margin-bottom-10 padding-right-20 padding-right-30-sm padding-right-50-md font-light color-gray-darker pdp-name-price">
                            <?php
                                $price = "";
                                if(
                                    $simpleData['special_price'] &&
                                    $simpleData['special_price'] < $simpleData['price'] &&
                                    ceil($simpleData['special_price']) != 0
                                ){
                                        $price = number_format(round($simpleData['special_price'],2),2,".","");
                                }else {
                                        $price = number_format(round($simpleData['price'], 2), 2, ".", "");

                                }
                            ?>

                            <span itemscope="" itemprop="offers" itemtype="http://schema.org/Offer" class="hidden">
                                <span itemprop="priceCurrency" content="USD">$</span>
                                <span id="oldPrice" itemprop="price" content="<?php echo $price;?>"></span>
                                <span itemprop="priceCurrency" content="USD">$</span>
                            </span>
                        </div>
                        <div itemprop="description" class="visible-sm-table-cell pdp-name-subcont sku-upsell-text padding-left-20-md"></div>
                    </div>
                </div>
                <script type="text/javascript">var ratingScore="<?php echo $reviews["total_rating"];?>"; var ratingCount="<?php echo $reviews["cnt"]; ?>"</script>

                <?php if($reviews["cnt"]!=0): ?>
                    <span class="hidden" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" style="display:none;">
                        <span itemprop="ratingValue" content="<?php echo $reviews["total_rating"];?>"></span>
                        <span itemprop="reviewCount"><?php echo $reviews["cnt"]; ?></span>
                    </span>
                <?php endif; ?>


                <div class="reviews-Tz" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
                    <span class="hidden" itemprop="author" style="opacity:0;">Payne_Customer</span>
                    <div class="pdp-social-share margin-top25-m clearfix">
                        <span class="share-title share_click pg-text-font1-6rem" itemprop="description">
                            REVIEWS <span class="reviews_title">(<?php echo $reviews["cnt"]; ?>)</span>
                        </span>
                        <div data-rating="<?php echo $reviews["total_rating"];?>" itemprop="reviewRating" class="rates font-larger-lg" style="display:none"><?php $i_rate = 1; $j_rate = $reviews["total_rating"];?>
                            <span itemprop="worstRating" class="fa fa-star margin-right-5 font-medium margin-top-0 margin-bottom-0 <?php echo $objectManager->get("Pg\CatalogExtend\Block\Product\View2")->getRate($i_rate++, $j_rate);?>"></span>
                            <span itemprop="ratingValue" class="fa fa-star margin-right-5 font-medium margin-top-0 margin-bottom-0 <?php echo $objectManager->get("Pg\CatalogExtend\Block\Product\View2")->getRate($i_rate++, $j_rate);?>"></span>
                            <span itemprop="ratingValue" class="fa fa-star margin-right-5 font-medium margin-top-0 margin-bottom-0 <?php echo $objectManager->get("Pg\CatalogExtend\Block\Product\View2")->getRate($i_rate++, $j_rate);?>"></span>
                            <span itemprop="ratingValue" class="fa fa-star margin-right-5 font-medium margin-top-0 margin-bottom-0 <?php echo $objectManager->get("Pg\CatalogExtend\Block\Product\View2")->getRate($i_rate++, $j_rate);?>"></span>
                            <span itemprop="bestRating" class="fa fa-star margin-right-5 font-medium margin-top-0 margin-bottom-0 <?php echo $objectManager->get("Pg\CatalogExtend\Block\Product\View2")->getRate($i_rate++, $j_rate);?>"></span>
                        </div>
                        <div id="reviews_share" class="share_click PDP-share-social-list showing-inline padding-top10 padding-right10  padding-top0-m text-left-m">
                            <div class="Stars"
                                 style="--rating:<?php echo $reviews["total_rating"]; ?>" aria-label="Rating of this product is 2.3 out of 5.">
                                <script src="/assets2/resource/js/edito.js"></script>
                            </div>
                        </div>
                    </div>
                    <div class="pdp-social-share margin-top25-m clearfix" style="margin-top:4px">
                        <span class="reviews_span">
                            <?php if($reviews["cnt"]<=0):?>
                                Be the first to
                            <?php endif; ?>
                            <a class="reviews_a">Write A Review</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- === Add to REVIEWS End === -->
    </div>
</div>
</div>

<!-- ======= Recommend Start ======= -->
<div class="swiper-container recommend" id="recommend_3">
    <h2>You May Also Like</h2>
    <div class="swiper-wrapper" id="append_recommend">
    </div>
    <div class="swiper-button-prev prev_click swiper-button-hide"></div>
    <div class="swiper-button-next next_click swiper-button-hide"></div>
</div>
<!-- ======= Recommend End ======= -->

<div class="row_details details_pc">
    <div class=""  id="Detail">
        <div class="product data items">
            <div class="data item title active bus-active"
                 id="tab-label-additional">
                <div class="h_g h_g_one"></div>
                <a class="data switch"
                   id="tab-label-additional-title" style="color:#000">
                    Details </a>
                <div class="bus-downB bus-down-active" id="bus-downB_one"></div>
            </div>
            <div class="data item title active bus-active" id="tab-label-additional_reviews">
                <div class="h_g h_g_two"></div>
                <a class="data switch"
                   id="tab-label-additional-title" style="color:#000">
                    Reviews </a>
                <div class="bus-downB" id="bus-downB_two"></div>
            </div>
            <div class="data item content" id="additional">
                <div  class="reveiews-additional reveiews-item">
                    <table>
                        <tr>
                            <h2 class="specif">SPECIFICATIONS</h2>
                            <li class="specif_li">
                                <?php if(isset($simpleData['access_length'])):?>
                                    <span class="dimen">Dimensions - Length:</span>
                                    <a><?php echo $simpleData['access_length'];?>"</a>
                                <?php endif;?>
                                <?php if(isset($simpleData['access_width'])):?>
                                    <span class="dimen">Width:</span>
                                    <a><?php echo $simpleData['access_width'];?>"</a>
                                <?php endif;?>
                                <?php if(isset($simpleData['access_high'])):?>
                                    <span class="dimen">Height:</span>
                                    <a><?php echo $simpleData['access_high'];?>"</a>
                                <?php endif;?>
                                <?php if(isset($simpleData['clip_lens_height'])):?>
                                    <span class="dimen">lens_height:</span>
                                    <a><?php echo $simpleData['clip_lens_height'];?>"</a>
                                <?php endif;?>
                                <?php if(isset($simpleData['clip_lens_width'])):?>
                                    <span class="dimen">lens_width:</span>
                                    <a><?php echo $simpleData['clip_lens_width'];?>"</a>
                                <?php endif;?>
                                <?php if(isset($simpleData['clip_total_width'])):?>
                                    <span class="dimen">total_width:</span>
                                    <a><?php echo $simpleData['clip_total_width'];?>"</a>
                                <?php endif;?>
<!--                                --><?php //if(isset($simpleData['weight'])):?>
<!--                                    <span class="dimen">Weight:</span>-->
<!--                                    <a>--><?php //echo (float)$simpleData['weight'];?><!--g</a>-->
<!--                                --><?php //endif;?>
                            </li>
                            <li class="specif_li">
                                <?php if(isset($currentSimpleData['access_material'])):?>
                                    <span class="dimen">Material:</span>
                                    <a>
                                        <?php echo $currentSimpleData['access_material'];?>
                                    </a>
                                <?php endif;?>
                                <?php if(isset($currentSimpleData['access_specifications'])):?>
                                    <span class="dimen">Specifications:</span>
                                    <a>
                                        <?php echo $currentSimpleData['access_specifications'];?>
                                    </a>
                                <?php endif;?>
                                <?php if(isset($currentSimpleData['clip_middle_beam'])):?>
                                    <span class="dimen">Middle_Beam:</span>
                                    <a>
                                        <?php echo $currentSimpleData['clip_middle_beam'];?>
                                    </a>
                                <?php endif;?>
                                <?php if(isset($currentSimpleData['clip_nasal_distance'])):?>
                                    <span class="dimen">Nasal_Distance:</span>
                                    <a>
                                        <?php echo $currentSimpleData['clip_nasal_distance'];?>
                                    </a>
                                <?php endif;?>
                            </li>
                        </tr>
                    </table>
                </div>
                <div  class="reveiews-additional" id="yes_revies">
                    <button class="btn-secondary rsr-write-a-review-btn btn-Write mobel_Write">Write a Review</button>
                    <div class="ok_reviews">
                        <span>Thanks for your review ! </span>
                        <span>Your review will be live in a few days.</span>
                    </div>
                    <div class="container-fluid zx-container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="zx-score"> OVERALL SCORE</span>
                                <span class="zx-5sE">
                                        <span class="zx-sort"><?php echo $reviews["total_rating"]; ?></span>
                                        <span class="star-ratings__top___3IeBI zx-rating">
                                           <div class="Stars" style="--rating:<?php echo $reviews["total_rating"]; ?>" aria-label="Rating of this product is 2.3 out of 5.">
                                                <script src="<?php echo $static_url; ?>/assets2/resource/js/edito.js"></script>
                                            </div>
                                        </span>
                                    </span>
                                <span class="rsr-top-row-div___WfUnu font-italic">Based on
                                            <span class="rsr-total-reviews"><?php echo $reviews["cnt"]; ?></span> reviews</span>
                                <button class="btn-secondary rsr-write-a-review-btn Write_a_Review pc_review">Write a Review</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zx-box_tt">
                                <div class="con z_box">
                                    <div class="circle">
                                        <div class="pie_left"><div class="left_circle"></div></div>
                                        <div class="pie_right"><div class="right_circle"></div></div>
                                        <div class="mask"><i class="fa fa-thumbs-o-up fa___2LJPU fa_circle fa_circle"></i><span style="display: none;"><?php echo $reviews["rate_will_recommend"]; ?></span></div>
                                    </div>
                                </div>
                                <div class="z_box">
                                    <h1 class="zx-bfb"><?php echo $reviews["rate_will_recommend"]; ?>%</h1>
                                </div>
                                <div class="z_box">
                                    <h5 class="zx-would">WOULD RECOMMEND TO A FRIEND.</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zx-box_title">
                                <div class="zx-box_ri">
                                                    <span class="margin-right-10 margin-right-20-md rsr-span" id="left_bou">
                                                        <i class="arrow___3_bRi arrow-left___1BoUt" aria-hidden="true"></i>
                                                    </span>
                                    <div class="zx_bodU">
                                        <span id="pages"><?php echo $reviews["reviewspage"]; ?></span>
                                        of
                                        <span id="count_Sum"></span>
                                    </div>
                                    <a class="rsr-pagination-btn rsr-pagination-btn--right" aria-disabled="false" id="right_bou">
                                        <i class="arrow___3_bRi arrow-right___10RxA" aria-hidden="true" ></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row zx-row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="zx-div">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div  class="reveiews-additional" id="No_revies">
                    <div class="ok_reviews">
                        <span>Thanks for your review ! </span>
                        <span>Your review will be live in a few days.</span>
                    </div>
                    <!--如果当前页面没有评论执行：-->
                    <div class="no-reviews___31TJc rsr-no-reviews">
                        <h1 class="font-black color-gray-darker text-uppercase">
                            There are no customer reviews yet
                        </h1>
                        <p>Be the first to write a review</p>
                        <button class="btn-secondary rsr-write-a-review-btn btn-Write">Write a Review</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--为了控制数据采集开关  luanjue start -->
<input type="hidden" id="smartCollectUrl" value="<?php echo $collection_api_url.'/webyiiapi/data-collection/push-new.html'; ?>" />
<input type="hidden" id="smartCollectOn" value="<?php echo $collection_enable; ?>" />
<!--为了控制数据采集开关  luanjue end -->

<!-- ======= Footer Lntroduction ======= -->
<!--footer放置处-->
    <?php
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
    echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("new_footer")->toHtml();
    ?>
<!--footer放置处 End-->
<!-- ======= Footer Lntroduction End ======= -->
</body>
<script>
    var simple=<?php echo $currentSimpleData['entity_id']?>;
    <?php foreach($simpleDatas as $simpleData):?>
    var simple_<?php echo $simpleData['entity_id']; ?> = <?php $product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($simpleData['entity_id']);$product->setData("url",Yii::$container->get("app\models\UrlRewrite")->getSimpleUrl($simpleData['entity_id']));echo json_encode($product->getData());?>;
    <?php isset($v2_i) ? ($v2_i .= ','.$simpleData['entity_id']) : ($v2_i = $simpleData['entity_id']) ?>
    <?php endforeach; ?>
    var current_simple = <?php $product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($id);echo json_encode($product->getData());?>;
    var configurable = <?php echo \yii\helpers\Json::encode($configableData);?>;
    var simpleids_string = '<?php echo $v2_i; ?>'

    //   ==== Get reiews PageCount ====
    var pagecount = "<?php echo $reviews["pagecount"]; ?>";
    var firstpage_review_ids='<?php echo $review_ids_str; ?>';
    var simple_url = "<?php echo $base_url; ?>/<?php echo $simpleData["url"];?>"
    var _share_price = "<?php echo $_share_price;?>";
    var _final_price = "<?php echo $_final_price;?>";
    var _is_shared = "<?php echo $_is_shared;?>";
    var support_share_and_save = "<?php echo $support_share_and_save;?>";
    var thumbnail_url = "<?php echo $static_url; ?>/media/catalog/product/<?php echo $currentSimpleData['thumbnail'];?>";
    var base_Url = "<?php echo $static_url; ?>";
    window.console = window.console || function(t) {};
    if (document.location.search.match(/type=embed/gi)) {
        window.parent.postMessage("resize", "*");
    }




    //获取当前的profile对象(sdd-2019,9,6)
//    var current_profile = {
//        id:"<?php //echo $block->getCurrentProfileId(); ?>//",
//        name:"<?php //echo $block->getCurrentProfile()->getNickname(); ?>//"
//    };
//
//    console.log(current_profile);
</script>

<!-- google recommended测试  start -->
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/googleRecommend.js"></script>
<!-- google recommended测试  end -->

<!-- ======= JS ile Import ====== -->
<!--  新引入文件 数据采集  luanjue   start -->
<?php if($collection_enable == 1) { ?>
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/smart.sync.js"></script>
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/new_collection.pdp_access.js"></script>
<?php } ?>
<!--  新引入文件 数据采集  luanjue   end -->
<script src="<?php echo $static_url; ?>/assets2/resource/js/social.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/js/pdp-social-share.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/pdp-summary/js/pdp-revision/swiper.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/resource2/js/notes.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo $static_url; ?>/resource2/js/jquery/jquery.cookie.min.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/js/save.js?v=<?php echo $js_version; ?>"></script>

<script src="<?php echo $static_url; ?>/assets2/resource/js/new_swiper.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/js/add_toCart.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/js/reviews.js?v=<?php echo $js_version; ?>"></script>
<!--新footer引入js-->
<script src="<?php echo $static_url; ?>/assets2/resource/js/footer.js?v=<?php echo $js_version; ?>"></script>
<!--新footer引入js End -->
<script async="async" type='text/javascript' src="<?php echo $objectManager->get('\Pg\Learning\Block\Page\Banner')->getCoreHelper()->getWidgetUrl(); ?>"></script>
<!-- ======= JS ile Import End ====== -->


<script type="text/javascript">
    /*  for google Recommend   detail-page-view  start   */
    setTimeout(function() {
        // console.log(window.loginCustomer.customer.current_profile_id, "------------pdp配件页面");
        var googleUserInfo = {
            'visitorId': getLocalDeviceID
        };
        var googleUserID = window.loginCustomer.customer.current_profile_id;
        if(googleUserID) {
            googleUserInfo.userId = googleUserID;
        }
        var accessData_session = getAccessData();
        // dataLayer = dataLayer || [];
        dataLayer.push({
            'event':'detail-page-view',
            'automl': {
                'eventType': 'detail-page-view',
                'userInfo': googleUserInfo,
                'productEventDetail': {
                    'productDetails': [{
                        'id': accessData_session['sku'],
                    }]
                }
            }
        });
    }, 3000);
    /*  for google Recommend   detail-page-view  end   */

    $("body").on("click","#orderRx",function() {
        /*  for google Recommend API  add-to-cart  start   */
        var googleUserInfo = {
            'visitorId': getLocalDeviceID
        };
        var googleUserID = window.loginCustomer.customer.current_profile_id;
        if(googleUserID) {
            googleUserInfo.userId = googleUserID;
        }
        var accessSku = $(".FrameColor_select").parent().attr("sku");
        // dataLayer = dataLayer || [];
        dataLayer.push({
            'event':'add-to-cart',
            'automl': {
                'eventType': 'add-to-cart',
                'userInfo': googleUserInfo,
                'productEventDetail': {
                    'productDetails': [{
                        'id': accessSku,
                        'quantity': 1
                    }]
                }
            }
        });
        /*  for google Recommend API  add-to-cart  end   */
    });
</script>

</html>
