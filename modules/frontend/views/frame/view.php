<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
$isGoogleGTMEnabled = $objectManager->get("\MagePal\GoogleTagManager\Helper\Data")->isEnabled();
// echo $collection_enable;

$csd_simple_url = $currentSimpleData["simple_url"];
//echo print_r($currentSimpleData,true);exit;
//$currentSimpleData["media_gallery"]["images"][12346]["value_id"];
// echo $currentSimpleData["retired"];
$isRetired = $currentSimpleData["retired"];
$isInstock = json_encode($currentSimpleData['quantity_and_stock_status']['is_in_stock']);
// echo $isInstock;
if($isInstock=="true") {
    $ljIsInstock = "IN STOCK";
}else {
    if($isRetired == "1") {
        $ljIsInstock = "RETIRED";
    }else{
        $ljIsInstock = "OUT OF STOCK";
    }
}
#json-ld
$price = $currentSimpleData["simple_url"]["price"];
if($currentSimpleData["simple_url"]["price"]>$currentSimpleData["simple_url"]["sales_price"]){
    $price = $currentSimpleData["simple_url"]["sales_price"];
}

#Three segments for description
$str_description = "first_description,second_description,meta_description";
$str_description = explode(",",$str_description);
$url = "";
foreach ($str_description as $key => $value){
    if(trim($csd_simple_url["$value"])){
        $url.=$csd_simple_url["$value"]." ";
    }
}
$url = rtrim($url," ");
$meta_description = $url;
$configableData_data=array();
$configableData = (array)$configableData;
$str = "support_progressive_value,
            recommend_value,
            is_clip_on,
            nose_pad,
            nose_bridge_value,
            has_spring_hinges_value,
            lens_width_value,
            bridge_value,
            temple_length_value,
            frame_size_value,
            frame_width_value,
            lens_height_value,
            frame_weight_value,
            frame_shape_value,
            rim_type_value,
            material_value,
            min_pd,
            max_pd,
            min_sph_value,
            max_sph_value,
            other_colors_value,
            age_range_value";
$str = explode(",",$str);
foreach ($str as $key => $val){
    $val=trim($val);
    $configableData_data[$val] = $configableData[$val];
}
//Get the current glasses type ID
$magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
$magento_autoload->loadMagentoFrameword();
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$current_category_id=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["PRODUCT_CATEGORY"];
//Get the current glasses type ID ENd

$type_name="";
$type_url_name="";
if($category_id==$current_category_id["EYEGLASSES_WOMEN"] || $category_id==$current_category_id["SUNGLASSES_WOMEN"]) {
    $type_name = "Women";
    $type_url_name = "women";
}elseif($category_id==$current_category_id["EYEGLASSES_MEN"] || $category_id==$current_category_id["SUNGLASSES_MEN"]) {
    $type_name = "Men";
     $type_url_name = "men";
}elseif($category_id==$current_category_id["EYEGLASSES_KIDS"] || $category_id==$current_category_id["SUNGLASSES_KIDS"]) {
    $type_name = "Kids";
    $type_url_name = "kids";
}
$scopeConfig = $objectManager->create("Magento\Framework\App\Config");
$facebookAppId = $scopeConfig->getValue("psloginfree/facebook/application_id");
// print_r($configableData);exit;
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <script>
        var static_url = "<?php echo $static_url;?>";
    </script>
    <?php
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
    echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("anti_flicker")->toHtml(); ?>
    <title><?php echo $dest_name; ?> | Payne Glasses</title>
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-transfrom">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <meta name="applicable-device" content="pc,mobel">
    <meta name="description" content="<?php echo $meta_description; ?>"/>
    <meta name="keywords" content="<?php echo $currentSimpleData["simple_url"]["meta_keywords"]?>"/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <meta property="og:url" content="<?php echo $base_url;?>/<?php echo $simple_url;?>"/>
    <meta property="og:title" content="<?php echo $dest_name; ?> | Payne Glasses"/>
    <meta property="og:image" content="<?php echo $static_url;?>/media/catalog/product<?php echo $configableData["image"];?>" />
    <meta property="og:type" content="product" />
    <meta property="og:site_name" content="<?php echo $dest_name; ?>" />
    <meta property="og:description" content="<?php echo $meta_description; ?>"/>
    <!--Introduction of Css Start-->
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/jquery-1.12.3.min.js"></script>
    <script src="<?php echo $static_url; ?>/static/version<?php echo $js_version; ?>/frontend/Pg/payne/en_US/jquery/jquery.cookie.js"></script>
    <?php
    echo $resultLayoutFactory->create()->getLayout()
        ->setCacheable(false)
        ->createBlock('Pg\CatalogExtend\Block\Klaviyo')
        ->setTemplate('Pg_CatalogExtend::klaviyo.phtml')
        ->toHtml();
    ?>

    <script>
        var dataLayer = dataLayer?dataLayer:[];
        //无法获取pmg数据补充 Start
        // $.ajax({
        //     type:"GET",
        //     async:false,
        //     url:"/e/customer/account/DatalayerCustomer",
        //     success: function (result) {
        //         var results = JSON.parse(result);
        //         dataLayer.push({"new_data":results.data});
        //     }
        // });
        // dataLayer无法获取pmg数据补充 ENd
    </script>
    <link href="/resource/header/fonts/fontawesome/css/all.min.css" type="text/css" rel="stylesheet">
    <link rel="canonical" href="<?php echo $base_url?>/<?php echo $simple_url?>"/>
    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/media/favicon/stores/1/favIcon.ico">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/custom.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/swiper.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/ui.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/footer_2020.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/fonts/pdp-revision/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/fonts/iconfont.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/notes.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/reviews.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/detail.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/awesomeFont.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/advertising.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/icon-fonts/css/porto-icons.css">
    <!--    <link rel="stylesheet" href="--><?php //echo $static_url; ?><!--/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/icon-fonts/css/porto-icons-codes.css?v=--><?php //echo $js_version; ?><!--">-->
    <!--Introduction of Css End-->

    <!--  新引入文件 数据采集  start -->
    <link rel="preload" as="style" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/collect.css" />
    <link rel="stylesheet" load_last="true" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/collect.css" />
    <!--  新引入文件 数据采集  end -->

    <!-- 引入 facebook -->
    <script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0"></script>

    <!--    用户历史足迹 history  luanjue  start  -->
    <link rel="stylesheet" load_last="true" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/history_pdp_webyii.css" />
    <!--    用户历史足迹 history  luanjue  end  -->

</head>
<script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Product",
         "brand": "payne",
         "mpn":"<?php echo $dest_sku;?>",
        "name": "<?php echo $dest_name; ?>",
        "image": "<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $currentSimpleData["image"];?>",
        "description": "<?php echo $meta_description;?>",
        "sku": "<?php echo $dest_sku;?>",
        "color": "<?php echo $currentSimpleData["color_value"];?>",
        <?php if($category_id==$current_category_id["EYEGLASSES_WOMEN"] || $category_id==$current_category_id["SUNGLASSES_WOMEN"]):?>
         "audience": {
            "@type": "PeopleAudience",
            "suggestedGender": "female"
        },
        <?php elseif($category_id==$current_category_id["EYEGLASSES_MEN"] || $category_id==$current_category_id["SUNGLASSES_MEN"]):?>
        "audience": {
            "@type": "PeopleAudience",
            "suggestedGender": "male"
        },
       <?php endif;?>
        "material":"<?php echo $currentSimpleData["material_value"];?>",
        "review": {
            "@type": "Review",
            "reviewRating": {
                "@type": "Rating",
                "ratingValue": "<?php if($reviews["total_rating"]):?><?php echo $reviews["total_rating"];?><?php else:?>5<?php endif;?>",
                "bestRating": "5"
            },
            "author": {
                "@type": "Person",
                "name": "Payne"
            }
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "<?php if($reviews["total_rating"]):?><?php echo $reviews["total_rating"];?><?php else:?>5<?php endif;?>",
            "reviewCount": "<?php if($reviews["cnt"]):?><?php echo $reviews["cnt"];?><?php else:?>1<?php endif;?>"
        },
        "offers": {
            "@type": "Offer",
            "url": "<?php echo $base_url;?>/<?php echo $simple_url;?>",
            "priceCurrency": "USD",
            "priceValidUntil":"<?php echo date("Y",strtotime("+1 year"));?>-12-12",
            "price": "<?php echo $price;?>",
            "sku": "<?php echo $dest_sku;?>",
            <?php if($currentSimpleData["quantity_and_stock_status"]["is_in_stock"]):?>
            "availability": "https://schema.org/InStock",
             <?php else:?>
             "availability": "https://schema.org/OutOfStock",
             <?php endif;?>
            "seller": {
                "@type": "Organization",
                "name": "Payne"
            }
        }
    }
</script>
<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "name": "Home",
        "item": "<?php echo $base_url;?>"
      },{
        "@type": "ListItem",
        "position": 2,
        "name": "<?php if($catetory_type == 'eyeglasses'): ?>
                    Eyeglasses
                <?php elseif ($catetory_type == 'sunglasses'): ?>
                    Sunglasses
                <?php else: ?>
                    Error
                <?php endif; ?>",
        "item": "<?php echo $base_url;?>/<?php echo $simple_url;?>"
      },{
        "@type": "ListItem",
        "position": 3,
        "name": "<?php echo $type_name?>",
        "item": "<?php echo $base_url;?>/<?php echo $catetory_type?>/<?php echo $type_url_name?>.html"
      },{
        "@type": "ListItem",
        "position": 4,
        "name": "<?php echo $dest_name; ?>",
        "item": "<?php echo $base_url;?>/<?php echo $simple_url;?>"
      }]
    }
</script>
<script>
    <?php foreach($simpleDatas as $simpleData):?>
    <?php isset($v2_i) ? ($v2_i .= ','.$simpleData['entity_id']) : ($v2_i = $simpleData['entity_id']) ?>
    <?php endforeach; ?>
    // <!-Get the current profile object-->
    var simpleids_string = '<?php echo $v2_i; ?>';
    //<!--//Get the list item of the current profile(sdd-2019,9,6)-->
    var profileList = '[]';
    // <!--//Current login URL address-->
    var loginUrl="/customer/account/login/referer/<?php echo base64_encode($base_url."/".$simple_url); ?>/";
    window.console = window.console || function(t) {};
    if (document.location.search.match(/type=embed/gi)) {
        window.parent.postMessage("resize", "*");
    }
    //blue parames
    var blocker_sku="<?php echo $blocker_product_info["sku"]; ?>";
    var blocker_entity_id="<?php echo $blocker_product_info["entity_id"]; ?>";
    var blocker_name="<?php echo $blocker_product_info["name"]; ?>";
    var blocker_original_price = "<?php echo $blocker_product_info["price"];?>";
    var blocker_price="<?php echo $blocker_product_info["special_price"]; ?>";
    var current_simple_price = "<?php echo $currentSimpleData["simple_url"]["price"];?>";
    var current_simple_distSku = "<?php echo $currentSimpleData["simple_url"]["dest_sku"];?>";
    var current_simple_sales_price = "<?php echo $currentSimpleData["simple_url"]["sales_price"];?>";
    var current_simple_sales_name = "<?php echo $currentSimpleData["simple_url"]["dest_name"];?>";
    var current_simple_color = "<?php echo $currentSimpleData["color"];?>";
    var current_simple_entity_id = "<?php echo $currentSimpleData["entity_id"];?>";
    //E-commerce enhancement
    var isGoogleGTMEnabled="<?php if ($isGoogleGTMEnabled) {echo "1";} else {echo "0";} ?>";
    var current_simple_id = "<?php echo $simple_id;?>";
    var is_color_changing = <?php if (isset($currentSimpleData["color_changing"]) && $currentSimpleData["color_changing"]) {echo "true";} else {echo "false";} ?>;
    var is_low_bridge_fit = <?php if (isset($currentSimpleData["low_bridge_fit"]) && $currentSimpleData["low_bridge_fit"]) {echo "true";} else {echo "false";} ?>;
    var nose_bridge_value = "<?php echo $currentSimpleData["nose_bridge_value"]; ?>";
    var support_progressive_value = "<?php echo $currentSimpleData["support_progressive_value"]; ?>";
    var is_support_rx = <?php if ($currentSimpleData["support_rx"]) {echo "true";} else {echo "false";} ?>;
    var created_at = "<?php echo $currentSimpleData["created_at"]; ?>";
    window.dataLayer = window.dataLayer || [];
</script>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','<?php echo $gtm_value ?>');</script>
<body configableData='<?php echo json_encode($configableData_data);?>' id="webYi_customerInfo">
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm_value ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


<!--Lens Guide  Tips Statr-->
<!--    <div class="LensGuide hide">-->
<!--        <div class="LensGuide_box">-->
<!--            <span class="glyphicon glyphicon-remove LensGuide_box_close" aria-hidden="true"></span>-->
<!--            <ul>-->
<!--                <h2>HOW TO BUY GLASSES ONLINE?</h2>-->
<!--                <p>Lenses are the most important part of your eyeglasses because they help correct your eyes and provide clear vision. The lens type you choose is quite important to your comfort when wearing glasses.</p>-->
<!--                <p>This guide will help you learn about different types of eyeglass lenses and help you choose the lenses that fit you best.</p>-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <h2>Prescription Glasses (Single Vision)</h2>-->
<!--                <p>Single-vision prescription glasses are the most common type of prescription glasses. Single vision lenses are spectacle lenses with only one dioptric power and are used to correct vision at only one specified distance (power distance or reading spectacles).</p>-->
<!--                <p class="mobel_lin">They are appropriate for people who need vision correction for one viewing distance only – whether farther or shorter distances (nearsighted or farsightedness), so that they can have a clear vision for things like driving or reading.</p>-->
<!--                <a href="/blog/how-do-i-order-my-prescription-glasses-online/">Learn More ></a>-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <img src="--><?php //echo $static_url;?><!--/assets2/resource/pdp-summary/images/LensGuide_1.png?v=--><?php //echo $js_version; ?><!--" alt="">-->
<!--            </ul>-->
<!--            <ul><h2>Progressive Glasses (Multi-Focal)</h2>-->
<!--                  <p>Progressive glasses eliminate the distinct divisions between different prescriptions. Progressive lenses, also called multifocal lenses, are no-line multifocal eyeglass lenses that allow you to do close-up work (like reading a book), middle-distance work (like using a computer), or distance viewing (like driving) without needing to change your glasses.</p>-->
<!--                  <p>They are appropriate for people who need corrective lenses to see both distant and close-up objects. And progressive glasses usually cost a little more than single-vision glasses.</p>-->
<!--                  <p class="mobel_lin">*ADD(NV-ADD) is required for progressive glasses.</p>-->
<!--                  <a href="/page/multifocal-progressive-page/index.html">Learn More ></a>-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <img src="--><?php //echo $static_url;?><!--/assets2/resource/pdp-summary/images/LensGuide_2.png?v=--><?php //echo $js_version; ?><!--" alt="">-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <h2>Lined Bifocal Glasses</h2>-->
<!--                <p>Bifocal glasses have two viewing areas divided by a visible line. The top viewing area of the glasses will be for your distance vision. Below the line, in a smaller viewing area will be your reading segment.</p>-->
<!--                <p>Bifocal glasses can be worn full-time because they can help you see both distant and close up objects, but some people may dislike the noticeable line on their lenses.</p>-->
<!--                <p class="mobel_lin">*ADD(NV-ADD) is required for progressive glasses.</p>-->
<!--                <a href="/eyeglasses/bifocal-glasses.html">Learn More ></a>-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <img src="--><?php //echo $static_url;?><!--/assets2/resource/pdp-summary/images/LensGuide_3.png?v=--><?php //echo $js_version; ?><!--" alt="">-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <h2>Non-Prescription (Plano) Glasses</h2>-->
<!--                <p>Non-prescription glasses are glasses that have no degree in the lenses and have no visual correcting power.</p>-->
<!--                <p>They are appropriate for people who have perfect vision don't need prescription glasses to correct their vision, or people who choose to wear glasses just for fashion purposes.</p>-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <h2>Reading / Bifocal Reading Glasses</h2>-->
<!--                <p>Reading glasses usually have non-prescription lenses at top of the lenses for distance, and reading power at the bottom for near reading. You can choose Regular Progressive Readers if you are an experienced wearer; or Beginner's Progressive Readers if you are a beginner, or lined bifocal reading glasses and single-vision reading glasses.</p>-->
<!--                <p>They are typically appropriate for those with presbyopia, the age-related eye condition that decreases a person's near vision. The need for reading glasses usually increases with age.</p>-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <img src="--><?php //echo $static_url;?><!--/assets2/resource/pdp-summary/images/LensGuide_4.png?v=--><?php //echo $js_version; ?><!--" alt="">-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <h2>Blue Light Blocking Glasses (Non-Rx)</h2>-->
<!--                <p>Blue light glasses are glasses with specially-crafted blue light blocking lenses that can block or absorb blue light given off from digital screens. Blue light glasses help protect your eyes from glare and help reduce potential damage to your retina from prolonged exposure to blue light.</p>-->
<!--                <p class="mobel_lin">They are appropriate for people who spent a lot of hours in front of digital devices, especially for kids because they may be at higher risk for blue light retinal damage than adults.</p>-->
<!--                <a href="/blue-light-glasses">Learn More ></a>-->
<!--            </ul>-->
<!--            <ul>-->
<!--                <h2>Frame Only</h2>-->
<!--                <p>Frame only glasses are glasses without lenses.</p>-->
<!--                <p>They are appropriate for people who wear glasses just for fun or to look stylish.</p>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
<!--Lens Guide  Tips End-->

<!--tryon Start-->
<div class="tryon_class">
    <span class="glyphicon glyphicon-remove close_pd_select" aria-hidden="true"></span>
    <div class="modal-dialog" id="modal-tryon" style="padding-top:0px !important;">
        <div class="select_pd">
            <p class="pd_xz_class pd_xz_top">Please select the current PD</p>
            <div>
                <div class="dan_pd_tryon">
                    <select name="" id="tryon_selected">
                    </select>
                </div>
            </div>
            <p class="pd_xz_class pd_xz_bottom">Currently selected PD:</p>
        </div>
        <div class="photograph hide">
            <span class="glyphicon glyphicon-remove close_photograph" aria-hidden="true"></span>
            <h4 class="image_Recorder">IMAGE RECORDER</h4>
            <div class="photograph_readius">
                <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/photograph_checked.png"
                     alt="photograph_checked">
            </div>
            <div class="djs_readius hide">
                5
            </div>
            <p class="clickOn_the frist_clickOn_the">Click on the button above to start the capture.</p>
            <p class="clickOn_the">After you clicked the button, you will have 5 seconds to prepare yourself.</p>
        </div>
        <div class="modal-content" id="tryon-content">
            <div class="tryon_box_fex">
                <div class="menu_class">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <ul class="menu_list hide">
<!--                    menu_list hide hide-->
                    <li>
                        <div class="pd_slide_show">
                            <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Adjustments.png"
                                 class="Adjustments_img"
                                 alt="Adjustments">Adjustments
                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                        </div>
                        <ul class="pd_slide hide">
                            <li>
                                <p class="centimeters">Position :</p>
                                <div class="display_fiex_scroll">
                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Position.png"
                                         class="position_img positionRotation_img_pc"
                                         alt="Position">
                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Position_mobel.png"
                                         class="position_img positionRotation_img_mobel"
                                         alt="Position">
                                    <div class="scroll pc_Adjustments" id="scroll">
                                        <div class="bar" id="bar">

                                        </div>
                                        <div class="mask" id="mask"></div>
                                    </div>
                                    <ul class="lanren mobel_Adjustments">
                                        <li>
                                            <div class="scale_panel">
                                                <div class="scale" id="touch_bar">
                                                    <span id="btn"></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <p class="centimeters">Rotation :</p>
                                <div class="display_fiex_scroll">
                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Rotation.png"
                                         class="Rotation_img positionRotation_img_pc"
                                         alt="Rotation">
                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Rotation_mobel.png"
                                         class="Rotation_img positionRotation_img_mobel"
                                         alt="Rotation">
                                    <div class="scroll pc_Adjustments" id="scroll1">
                                        <div class="bar" id="bar1">

                                        </div>
                                        <div class="mask" id="mask1"></div>
                                    </div>
                                    <ul class="lanren mobel_Adjustments">
                                        <li class="">
                                            <div class="scale_panel">
                                                <div class="scale" id="touch_bar1">
                                                    <span id="btn1"></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div class="">
                            <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Maximum.png"
                                 class="Adjustments_img"
                                 alt="Adjustments">Maximum users
                        </div>
                        <div class="Maximum_users">
                            <div class="Maximum_users_frist">1</div>
                            <div>2</div>
                            <div>3</div>
                        </div>
                    </li>
                    <li class="anti_mirror_dioptry_Pho_thickness">
                        <div class="pd_slide_Anti">
                            <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Anti_reflective_coating.png"
                                 class="Adjustments_img"
                                 alt="Anti_reflective_coating">Anti reflective coating
                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                        </div>
                        <ul class="pd_slide1 hide">
                            <li>
                                <div class="form-check" id="formCheck">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1" id="reflective">
                                        Apply anti reflective coating
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </li>
<!--                    <li class="anti_mirror_dioptry_Pho_thickness">-->
<!--                        <div class="pd_slide_Mirror">-->
<!--                            <img src="--><?php //echo $static_url;?><!--/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Mirror_Customize.png?v=--><?php //echo $js_version; ?><!--"-->
<!--                                 class="Adjustments_img"-->
<!--                                 alt="Mirror_Customize">Mirror Customize-->
<!--                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>-->
<!--                        </div>-->
<!--                        <ul class="pd_slide2 hide">-->
<!--                            <li>-->
<!--                                <div color="G">-->
<!--                                    <div>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div color="B">-->
<!--                                    <div>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div color="E">-->
<!--                                    <div></div>-->
<!--                                </div>-->
<!--                                <div class="Mirror_class">-->
<!--                                    <div></div>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->
                    <li class="anti_mirror_dioptry_Pho_thickness">
                        <div class="pd_slide_Dioptry">
                            <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Dioptry.png"
                                 class="Adjustments_img"
                                 alt="Dioptry"> Dioptry
                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                        </div>
                        <div class="display_fiex_scroll pd_slide_Dioptry_show hide">
<!--                            hide-->
                            <div class="scroll pc_Adjustments" id="scroll2">
                                <div class="bar" id="bar2">

                                </div>
                                <div class="mask" id="mask2"></div>
                            </div>
                            <ul class="lanren mobel_Adjustments mirror_mobel">
                                <li>
                                    <div class="scale_panel">
                                        <div class="scale" id="touch_bar2">
                                            <span id="btn2"></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="anti_mirror_dioptry_Pho_thickness">
                        <div class="pd_slide_Photochromic">
                            <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Photochromic.png"
                                 class="Adjustments_img"
                                 alt="Photochromic"> Photochromic lenses
                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                        </div>
                        <ul class="pd_slide3 hide">
                            <li>
                                <div opctiy_galsses="0">
                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/GoodMorning_checked.png" alt="tryon_img_icon">
                                </div>
                                <div opctiy_galsses="0.5">
                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/in.png" alt="tryon_img_icon">
                                </div>
                                <div opctiy_galsses="1">
                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/night.png" alt="tryon_img_icon">
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="anti_mirror_dioptry_Pho_thickness">
                        <div class="pd_slide_Thickness">
                            <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/Thickness.png"
                                 class="Adjustments_img"
                                 alt="Dioptry"> Thickness Correction
                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                        </div>
                        <div class="display_fiex_scroll pd_slide_Thickness_show hide">
                            <div class="scroll pc_Adjustments" id="scroll3">
                                <div class="bar" id="bar3">

                                </div>
                                <div class="mask" id="mask3"></div>
                            </div>
                            <ul class="lanren mobel_Adjustments mirror_mobel">
                                <li>
                                    <div class="scale_panel">
                                        <div class="scale" id="touch_bar3">
                                            <span id="btn3"></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <div class="close_tryon">
                    <p class="close_tryon_pc">close tryon</p>
                    <span class="glyphicon glyphicon-remove close_tryon_mobel" aria-hidden="true"></span>
                </div>
                <div class="set_Ipd" id="pd_xz_class">
                    <p class="set_pd_pc">SET PD</p>
                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/setPd.png"
                         class="img_size set_pd_mobel"
                         alt="set_pd">
                </div>
                  <!-- [Interim notes for measuring PD]-->
<!--                <div class="set_Ipd Measuring_PD" id="Measuring_PD">-->
<!--                    <p class="set_pd_pc">MEAAURING PD</p>-->
<!--                    <img src="--><?php //echo $static_url;?><!--/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/meaauring_Pd.png?v=--><?php //echo $js_version; ?><!--"-->
<!--                         class="img_size set_pd_mobel"-->
<!--                         alt="set_pd">-->
<!--                </div>-->
                <div id="photograph_glasses" class="icon_glasses icon_glasses1" title="Take a picture for try-on">
                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/photograph.png"
                         class="img_size"
                         alt="tryon_img_icon">
                </div>
                <div id="close_glasses" class="icon_glasses" title="Remove the glasses">
                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/glasses.png"
                         class="img_size"
                         alt="tryon_img_icon">
                </div>
                <div id="fp_tryon_one" class="icon_glasses" title="Single Frame">
                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/fp_tryon_one.png"
                         class="img_size"
                         alt="tryon_img_icon">
                </div>
                <div id="fp_tryon" class="icon_glasses" title="Frame comparison">
                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/fp_tryon_two_checked.png"
                         class="img_size set_pd_pc"
                         alt="tryon_img_icon">
                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/fp_tryon_two_mobel_checked.png"
                         class="img_size set_pd_mobel"
                         alt="tryon_img_icon">
                </div>
                <span class="share_f" facebookAppId="<?php echo $facebookAppId; ?>">f</span>
            </div>
            <div class="select_tryon">
                <p>Enter your pupillary distance (PD) for an even better fit</p>
                <div class="d_pd">
                    <div class="select" style="margin-top:0px;outline:none;border:1px solid #BBBBBB;border-radius:4px;position:relative;">
                        <select name="pd" id="pd_select" style="height:40px;width:100%;-webkit-appearance:none;appearance:none;border:none;font-size:18px;padding:0px 10px;display:block;-webkit-box-sizing:border-box;box-sizing:border-box;background-color: #FFFFFF;color:#333333;border-radius:4px;">
                        </select>
                    </div>
                </div>
                <div class="next next_jx" id="next_tryon">NEXT</div>
                <div class="try_without" id="try_without_pd">Try without PD</div>
                <p style="color: #888;font-size: 16px;">Camera must be enabled for Try-On</p>
            </div>
            <div class="tryon_box tryon_box_fp">

            </div>
            <div class="tryon_box_fex_bottom">
                <div class="swiper-father" id="tryon-father">
                    <div class="swiper-container" id="swiper-container_tryon">
                        <div class="swiper-wrapper">
                            <?php foreach ($simpleDatas as $simple): ?>
                                <?php if($simple["try_on"]==1):?>
                                    <div class="swiper-slide <?php if($simple["try_on"]!=1):?>tryon_no_class<?php endif;?>"
                                         stock_sku="<?php echo $simple["stock_sku"];?>"
                                         sku="<?php echo $simple["sku"];?>">
                                        <img src="<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $simple["small_image"];?>" alt="<?php echo $simple["name"];?>" imgs="<?php echo $simple["small_image"];?>">
                                    </div>
                                <?php endif;?>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <div class="swiper-button-prev swiper-button-prev1"></div>
                    <div class="swiper-button-next swiper-button-next1"></div>
                </div>

                <!--  Interim notes for measuring PD-->
<!--                <ul class="box_class_pd hide">-->
<!--                    <li id="start_PD">start</li>-->
<!--                    <li id="stop_PD">stop</li>-->
<!--                </ul>-->
            </div>
        </div>
        <div class="smak">
            <p class="Preparing">Preparing glasses for try-on</p>
            <div class="cssload-container">
                <div class="cssload-speeding-wheel"></div>
            </div>
        </div>
    </div>
</div>
<!--tryon End-->
<a href="javascript:;" id="botop"><em class="porto-icon-up-open"></em></a>
<!--clipon start-->
<div class="clipon_class">
    <div class="modal-dialog" id="modal-dialog1" style="padding-top:0px !important;">
        <div class="modal-header">
            <button type="button" class="close clipon_close" data-dismiss="modal" aria-hidden="true">
                ×
            </button>
            <h4 class="modal-title " id="myModalLabel">
                ADD YOUR ACCESSORIES
            </h4>
        </div>
        <div class="modal-content">
            <div class="modal-body">
                <div id="popup-modal" class="popup-modal" style="display: block;z-index:11;">
                    <div class="pdp-order btn-group dropup" style="display: block">
                        <?php foreach($clipons as $clipons_list):?>
                            <div class="modal-clip col-12 clearfix">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <img src="<?php echo $static_url;?>/media/catalog/product/cache/600x300<?php echo $clipons_list["thumbnail"]?>" alt="<?php echo $clipons_list["name"]?>">
                                </div>
                                <div class="modal-clip-content col-md-8 col-sm-8  col-xs-12 clearfix">
                                    <div class="modal-clip-sku  col-md-4 col-sm-6 col-xs-6">
                                        SKU #:<span><?php echo $clipons_list["sku"]?></span></div>
                                    <div class="modal-clip-price col-md-3 col-sm-6 col-xs-6">
                                        <?php if($clipons_list["special_price"]!="0" && $clipons_list["special_price"]):?>
                                            <del style="color:#ccc;">
                                                $<?php echo $clipons_list["special_price"]?>
                                            </del>
                                            <span style="font-weight: bold">
                                                     $<?php echo $clipons_list["price"]?>
                                                </span>
                                        <?php else:?>
                                            <span style="font-weight: bold">
                                                     $<?php echo $clipons_list["price"]?>
                                                </span>
                                        <?php endif;?>
                                    </div>
                                    <div class="modal-clip-num  col-md-2 col-sm-6 col-xs-6">
                                        <select name="modal-clip-number" id="modal-clip-number_1034" class="modal-clip-number modal-clip-select">
                                        </select>
                                    </div>
                                    <div class="modal-cart col-md-3 col-sm-6 col-xs-6">
                                        <button id="clipCard_1034" configid="<?php echo $clipons_list["configurable_id"]?>" simpleid="<?php echo $clipons_list["simple_id"]?>" color="<?php echo $clipons_list["color"]?>" style="border-radius: 5px;" class="button action continue primary clip_on_btn1 btn btn-warning">Add to cart</button>
                                    </div>
                                    <div class="col-12 modal_hint" style="display: none">
                                        <span class="bg-success  padding5">
                                            You've added this accessory to cart
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-close clipon_close" data-dismiss="modal">Close
            </button>
            <button type="button" class="btn btn-warning" id="btn-cart">
                <a style="color:#fff;text-decoration: none;" href="<?php echo $base_url?>/checkout/cart">
                    Go to cart
                </a>
            </button>
        </div>
    </div>
</div>
<!--clipon End-->
<!--loader-->
<div class="loading-jz" data-role="loader">
    <div class="loader-img">
        <img alt="Loading..." src="<?php echo $static_url; ?>/static/version1571822590/frontend/Pg/payne/en_US/images/loader-2.gif">
    </div>
</div>
<!--loader ENd-->
<!--Advertisement Start-->
<div class="message">
    <div id="message_show">
        <div class='message_container'>
            <div class="message_two_child owl-carousel owl-theme">

            </div>
        </div>
    </div>
</div>
<!--Advertisement End-->
<?php
//<!-- ======= Head Lntroduction ======= -->
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>
<div class="breadcrumbs" id="breadcrumbs" style="z-index:99999">
    <ul class="items">
        <li class="item home item_home">
            <a href="<?php echo $base_url?>" title="Go to Home Page" class="hover_undu">
                Home
            </a>
            <i class="iconfont icon-arrow-right-copy header_icon"></i>
        </li>
        <li class="item category5">
                <?php if($catetory_type == 'eyeglasses'): ?>
                <a title="" href="<?php echo $base_url?>/eyeglasses" class="hover_undu">
                    Eyeglasses
                </a>
                <?php elseif ($catetory_type == 'sunglasses'): ?>
                <a title="" href="<?php echo $base_url?>/sunglasses" class="hover_undu">
                    Sunglasses
                <a title="">
                <?php else: ?>
                    Error
                <?php endif; ?>
            <i class="iconfont icon-arrow-right-copy header_icon"></i>
        </li>
        <li class="item category6">
            <a href="<?php echo $base_url?>/<?php echo $catetory_type?>/<?php echo $type_url_name?>.html" title="" class="hover_undu" id="category_ele" category="<?php echo $type_name;?>">
                <?php echo $type_name?>
            </a>
            <i class="iconfont icon-arrow-right-copy header_icon"></i>
        </li>
        <li class="item product product_name">
            <a title="" class="simple_name">
                <?php echo $dest_name; ?>
            </a>
        </li>
    </ul>
</div>
<!-- ===== Comment filling area ====== -->
<div id="fix-fluid" >
    <div class="container-fluid reveiws-container"
         sku="<?php echo $currentSimpleData["sku"]?>"
         name="<?php echo $currentSimpleData["name"]?>"
         entry_id="<?php echo $currentSimpleData["entity_id"]?>"
         price="<?php echo $currentSimpleData["price"]?>"
         color="<?php echo $currentSimpleData["color"]?>"
         simple_url="<?php echo $simple_url?>"
    >
        <font style="vertical-align: inherit;" class="close-fluid">×</font>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-h3">
                <h3>Review the <span class="zx-name simple_name"><?php echo $dest_name; ?></span> <span class="zx-sku sku_text"><?php echo $dest_sku; ?></span></h3>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-price">
                <div style="display:inline-block" id="price_text" class="price_text">
                    <?php if($currentSimpleData["simple_url"]["sales_price"]!="0.00" && $currentSimpleData["simple_url"]["sales_price"] && $currentSimpleData["simple_url"]["sales_price"]!=$currentSimpleData["simple_url"]["price"]):?>
                        <del class="del_price price_class">
                            $<?php echo $currentSimpleData["simple_url"]["price"]; ?>
                        </del>
                        <div class="price h4 price_class">
                            $<?php echo $currentSimpleData["simple_url"]["sales_price"]; ?>
                        </div>
                    <?php else:?>
                        <div class="price h4 price_class">
                            $<?php echo $currentSimpleData["simple_url"]["price"]; ?>
                        </div>
                    <?php endif;?>
                </div>
                <div class="div_bor"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <img src="<?php echo $static_url;?>/media/catalog/product<?php echo $currentSimpleData['thumbnail']?>" alt="<?php echo $currentSimpleData['name']?>" class="imgs"
                >
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 reveiws-frame">
                <h6>Tell us about this frame.</h6>
                <!--<p>Now that you’ve had a chance to test-drive your recently purchased Pg, we’d love to know your thoughts. Please focus on what you liked or didn’t like about this frame. Your feedback will help other Pg customers who are interested in the same product.</p>-->
            </div>
        </div>
        <div class="reviews-conent">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses1">
                    How would you rate your glasses? <span>*</span>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 reveiws-wjx">
                    <ul data-required="true" class="acs_rating_holder acs_js_rating_holder">
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-1" data-value="1">
                            <span class="fa fa-star-o fas fas_review"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-2" data-value="2">
                            <span class="fa fa-star-o fas fas_review"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-3" data-value="3">
                            <span class="fa fa-star-o fas fas_review"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-4" data-value="4">
                            <span class="fa fa-star-o fas fas_review"></span></li>
                        <li class="acs_rank acs_js_rating_box star-rating-button" data-name="overall-vote-5" data-value="5">
                            <span class="fa fa-star-o fas fas_review"></span></li>
                    </ul>
                    <span class="product_to">Click to rate this product.</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses2">
                    Your Review Headline <span>*</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-productInput">
                    <textarea name="" id="YourReview" cols="30"  placeholder="e.g. Excellent product..." rows="10"></textarea>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-show">
                    <span id="reveiws-btn"> Show me an example </span>
                    <i class="fa fa-chevron-down" id="upDown"></i>
                    <div class="acs-headline-examples" style="">
                        <p>"Don't judge a book by its cover"</p>
                        <p>"I would buy this product again and again"</p>
                        <p>"Too much whiz and not enough bang"</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses3">
                    Describe your overall product experience <span>*</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-textarea">
                    <textarea name="" id="describe" cols="30" rows="10"  data-maxlength="5000" placeholder="Describe your overall product experience (Required)" style="resize:none" ></textarea>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-words">
                     <span>
                        Characters Remaining:
                         <a id="DescribeNum">5000</a>
                     </span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12 reveiws-galsses reveiws-galsses4">
                    Would you recommend this product to a friend?
                    <span>*</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reveiws-yN">
                    <fieldset class="acs_fieldset acs_fieldset_click">
                                <span val="yes">
                                        <span class="fa fa-thumbs-o-up" id="reveiws-box"></span>
                                        <a class="rec0 rec">Yes</a>
                                </span>
                        <span val="no">
                                <span class="fa fa-thumbs-o-down" id="reveiws-box"></span>
                                <a  class="rec1">No</a>
                            </span>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 reveiws-nickname">
                <div >
                    <p>Nickname
                        <a class="zx-xh">*</a>
                    </p>
                    <textarea name=""  cols="30" rows="10" id="Nickname"></textarea>
                    <!--                <textarea type="text" >-->
                    <span>Avoid using your full name. (Required)</span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 reveiws-nickname">
                <div>
                    <p>Email Address
                        <a class="zx-xh">*</a>
                    </p>
                    <textarea name="" id="Email_S" cols="30" rows="10"></textarea>
                    <!--                <input type="text" id="">-->
                    <span id="errTips">This will only be used for notification purposes</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 reveiws-submit">
                <button id="reveiws-cancel">CANCEL</button>
                <button id="reveiws-sub">SUBMIT</button>
            </div>
        </div>
    </div>
</div>
<div>

</div>
<div class="assigned"></div>
<!-- ===== Comment filling area End====== -->
<div class="minicart-bg1"></div>
<!-- ======= Head Lntroduction End ======= -->
<!-- Header End -->
<div class="klgErr"></div>
<section class="padding-y dest_name" >
    <div class="" id="ts_ele"></div>
    <div class="container container1">
        <h1 class="title simple_name"><?php echo $dest_name; ?></h1>
    </div>
</section>
<section class="padding-y" style="position: relative" id="paddingTop">
    <div class="container container1" id="container_box">
        <div class="row">
            <aside class="col-lg-8  col-xs-12 padd-top mobel_product pdp_box">
                <div class="swiper-container gallery-top" id="swiper_top">
                    <div class="swiper-button-next swiper-button-next-simple swiper-button-white" id="next_ie"></div>
                    <ul class="swiper-wrapper" id="sildWarp">
                        <?php if(isset($currentSimpleData["first_front"]) and $currentSimpleData["first_front"] !='no_selection'):?>
                            <li class="swiper-slide swiper-slide-visible">
                                <img src="<?php echo $static_url;?>/media/catalog/product<?php echo $currentSimple_img =$currentSimpleData["first_front"];?>"  alt="<?php echo $dest_name; ?>"/>
                            </li>
                        <?php endif;?>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product<?php echo $currentSimple_img = $currentSimpleData["back"];?>"  alt="<?php echo $dest_name; ?>"/>
                        </li>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product<?php echo $currentSimple_img = $currentSimpleData["front"];?>"  alt="<?php echo $dest_name; ?>"/>
                        </li>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product<?php echo $currentSimple_img = $currentSimpleData["left"];?>"  alt="<?php echo $dest_name; ?>"/>
                        </li>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product<?php echo $currentSimple_img = $currentSimpleData["right"];?>"  alt="<?php echo $dest_name; ?>"/>
                        </li>
                    </ul>
                    <div class="swiper-button-prev swiper-button-prev-simple swiper-button-white" id="prev_ie"></div>
                </div>
                <div class="swiper-container gallery-thumbs" id="swiper_bottom">
                    <ul class="swiper-wrapper" id="min_sildWarp">
                        <?php if(isset($currentSimpleData["first_front"]) and $currentSimpleData["first_front"] !='no_selection'):?>
                            <li class="swiper-slide swiper-slide-visible">
                                <img src="<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $currentSimple_img = $currentSimpleData["first_front"];?>" alt="<?php echo $dest_name; ?>"/>
                            </li>
                        <?php endif;?>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $currentSimple_img = $currentSimpleData["back"];?>" alt="<?php echo $dest_name; ?>"/>
                        </li>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $currentSimple_img = $currentSimpleData["front"];?>" alt="<?php echo $dest_name; ?>"/>
                        </li>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $currentSimple_img = $currentSimpleData["left"];?>" alt="<?php echo $dest_name; ?>"/>
                        </li>
                        <li class="swiper-slide swiper-slide-visible">
                            <img src="<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $currentSimple_img = $currentSimpleData["right"];?>" alt="<?php echo $dest_name; ?>"/>
                        </li>
                    </ul>
                </div>
                <div class="pc_dittis">
                    <div class="bt_class container container1 bot_px">
                        <p class="bt_class">Details</p>
                    </div>
                    <div class="container container1" id="details_class">
                        <div class="row details_class">
                            <div class="col-lg-12  col-xs-12">
                                <div class="tab-content">
                                    <div class="tab-pane tab-pane-show active" id="parameters">
                                        <div class="float_right table mb-3 table-striped">
                                            <ul>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/frame-width.png" alt="Frame Width">
                                                    Frame Width: <strong><?php echo $configableData["frame_width_value"]?> mm</strong> </li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/bridge.png" alt="Bridge">
                                                    Bridge: <strong><?php echo $configableData["bridge_value"]?> mm</strong> </li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/lens-width.png" alt="Lens Width">
                                                    Lens Width: <strong><?php echo $configableData["lens_width_value"]?> mm</strong> </li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/lens-height.png" alt="Lens Height">
                                                    Lens Height: <strong><?php echo $configableData["lens_height_value"]?> mm</strong> </li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/temple-length.png" alt="Temple Length">
                                                    Temple Length: <strong><?php echo $configableData["temple_length_value"]?> mm</strong> </li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/frame-weight.png" alt="Weight">
                                                    Weight: <strong><?php echo $configableData["frame_weight_value"]?> g</strong>  </li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/material.png" alt="Material">
                                                    Material: <strong><?php echo $configableData["material_value"]?></strong></li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/pd-range.png" alt="Good for PD">
                                                    Good for PD: <strong><?php echo $configableData["min_pd"]?>-<?php echo $configableData["max_pd"]?>  mm</strong></li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/frame-size.png" alt="Frame Fit">
                                                    Size: <strong> <?php echo $configableData["lens_width_value"]?> | <?php echo $configableData["bridge_value"]?> | <?php echo $configableData["temple_length_value"]?>  </strong>
                                                </li>
                                                <li>
                                                    <?php
                                                    $progressive = "";
                                                    $support_progressive_value = $configableData["support_progressive_value"];
                                                    $support_progressive_value = strtolower($support_progressive_value);
                                                    $recommend_value = $configableData["recommend_value"];
                                                    $recommend_value = strtolower($recommend_value);
                                                    if ($support_progressive_value=="yes" && $recommend_value == "yes") {
                                                        $progressive = "Recommended";
                                                    }else if($support_progressive_value=="yes") {
                                                        $progressive = "Supported";
                                                    } else {
                                                        $progressive = "No";
                                                    }
                                                    ?>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/support-progressive.png"
                                                         progressive = "<?php echo $progressive;?>"
                                                         alt="Progressive" class="Progressive_val_img">
                                                    <p id="Progressive_val" support_progressive="<?php echo $progressive;?>">
                                                        Progressive/Bifocal: <strong><?php echo $progressive;?></strong>
                                                    </p>
                                                </li>
                                                <li id="nose_pad_value" nose_pad_value="<?php echo $configableData["nose_pad_value"]?>">
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/nosepad.png" alt="Nose Pad">
                                                    Nose Pads: <strong><?php echo $configableData["nose_pad_value"]?></strong>
                                                </li>
                                                <li>
                                                    <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/spring_hinges.png" alt="Has Spring Hinges">
                                                    Has Spring Hinges: <strong><?php echo $configableData["has_spring_hinges_value"]?></strong>
                                                </li>
                                                <?php if($category_id == $current_category_id["EYEGLASSES_KIDS"] || $category_id==$current_category_id["SUNGLASSES_KIDS"]): ?>
                                                    <?php if($configableData_data["age_range_value"]):?>
                                                        <li style="width:100%">
                                                            <img src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/recommended_for.png" alt="Has Spring Hinges">
                                                            Recommended For: <strong><?php echo $configableData_data["age_range_value"]?></strong>
                                                        </li>
                                                    <?php endif;?>
                                                <?php endif;?>
                                                <?php if(count($tags)>=1):?>
                                                    <li style="width:100%" id="tags">
                                                        <img src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/tag.png" alt="Frame Width">
                                                        Tags:
                                                        <?php foreach($tags as $tag_list):?>
                                                            <a href="<?php echo $tag_list["url"]?>">
                                                                <?php echo $tag_list["name"]?>
                                                            </a>
                                                        <?php endforeach?>
                                                    </li>
                                                <?php endif;?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <main class="col-lg-4  col-xs-12">
                <article class="info-detail">
                    <!--REVIEWS-->
                    <div class="<?php if($reviews["cnt"]<=0):?>reviews_class_wdl<?php else:?>reviews_class<?php endif;?>">
                        <h6 class="title-control bt_class" style="margin-bottom: 10px;margin: 0px;"> REVIEWS
                            <span style="color: #f90">(<?php echo $reviews["cnt"]?>)</span>
                        </h6>
                        <div id="reviews_share" class="share_click PDP-share-social-list showing-inline padding-top10 padding-right10  padding-top0-m text-left-m"
                             style="display: block;margin: 10px 0px;">
                            <div style="<?php if($reviews["cnt"]>0):?>display: flex;width:208px;justify-content: space-between;<?php endif;?>margin-top: 10px;">
                                <div class="Stars"
                                     style="--rating:<?php echo $reviews["total_rating"]; ?>;" aria-label="Rating of this product is 2.3 out of 5.">
                                </div>
                                <p style="margin-top:-5px">
                                    <?php if($reviews["cnt"]<=0):?>
                                        Be the first to Write A Review
                                    <?php endif;?>
                                    <a href="javascript:;" class="ml-3 link-color review-pop-show" style="margin-left: 0px!important;margin-top:8px">Write A Review </a>
                                </p>
                            </div>
                        </div>

                    </div>
                    <!--REVIEWS End-->
                    <h6 class="title-control bt_class frame_color">Frame color</h6>


                    <nav class="pdp-color" style="" id="pdp-color_list" lens_type="<?php echo $lens_type;?>" color_value="<?php echo $currentSimpleData["color_value"]?>">
                        <?php foreach ($simpleDatas as $simple): ?>
                            <div class="btn-color-parent pdp_div select_li select_li_frame"
                                 stock_sku="<?php echo $simple["stock_sku"]?>"
                                 flag="<?php if(isset($simple["try_on"]) && $simple["try_on"]):?>true<?php endif;?>"
                            >
                                <a style="cursor: pointer;
                                <?php if($catetory_type!="eyeglasses"):?>
                                    <?php if((isset($simple["color_details"]))):?>
                                        margin-bottom: 10px;
                                    <?php endif;?>
                                <?php endif;?>
                                    " class="btn-color btn-color-click <?php if($simple["entity_id"]==$currentSimpleData["entity_id"]):?>active<?php endif;?>"
                                    <?php $arr=array();?>
                                    <?php if(isset($simple["first_front"])):?>
                                        <?php $arr[]=$simple["first_front"]?>
                                    <?php endif; ?>
                                    <?php $arr[]=$simple["back"]?>
                                    <?php $arr[]=$simple["front"]?>
                                    <?php $arr[]=$simple["left"]?>
                                    <?php $arr[]=$simple["right"]?>
                                    <?php if(isset($simple["simple_urls"]["Blue_Non_Rx"])):?>
                                        blue_price="<?php echo $simple["simple_urls"]["Blue_Non_Rx"]["price"]?>"
                                        blue_sales_price="<?php echo $simple["simple_urls"]["Blue_Non_Rx"]["sales_price"]?>"
                                    <?php endif;?>
                                   frame_group="
                                            <?php if($simple["sku"]=="14209G07" || $simple["sku"]=="24209G07" ):?>
                                                only_blue_light_blocking
                                            <?php else:?>full_rim<?php endif;?>"
                                   base_url="<?php echo $base_url;?>"
                                   data_img='<?php echo json_encode($arr);?>'
                                   entity_id="<?php echo $simple["entity_id"]?>"
                                   review_sku='<?php echo $simple["sku"]?>'
                                   review_color="<?php echo $simple["color"]?>"
                                   thumbnail="<?php echo $base_url;?>/media/catalog/product<?php echo $simple["thumbnail"]?>"
                                   price="<?php echo $simple["price"]?>"
                                   special_price="<?php echo $simple["special_price"]?>"
                                   simple_product_id="<?php echo $simple["entity_id"]?>"
                                   simple_product_color="<?php echo $simple["color"]?>"
                                   color_value = '<?php echo $simple["color_value"];?>'
                                   name="<?php echo $simple["name"]?>"
                                    <?php if(isset($simple["color_details"])):?>
                                        color_details="<?php echo $simple["color_details"]?>"
                                    <?php endif;?>
                                    <?php $arr=array()?>
                                    <?php $arr_url=array()?>
                                    <?php $description=array()?>
                                    <?php $price_list=array()?>
                                    <?php $sales_price_list=array()?>
                                    <?php $relate_product=""?>
                                    <?php foreach ($simple["simple_urls"] as $key=>$val): ?>
                                        <?php $arr[$key][] = $val["dest_name"]?>
                                        <?php $arr[$key][] = $val["dest_sku"]?>
                                        <?php $arr_url[$key] = $val["url"]?>
                                        <?php $price_list[$key] = $val["price"]?>
                                        <?php $sales_price_list[$key] = $val["sales_price"]?>
                                        <?php if($val["relate_product"] && $val["relate_product"]!=[]):?>
                                            <?php $relate_product[$key] = $val["relate_product"]?>
                                        <?php endif;?>
                                        <?php $description[$key] = str_replace("'"," ", $val['page_description'])?>
                                        <?php if($lens_type==$key):?>
                                            <?php $url=$val["url"]?>
                                        <?php endif;?>
                                    <?php endforeach; ?>
                                   ull="<?php echo $base_url;?>/<?php if (isset($url)) {echo $url;}?>"
                                   current_sku='<?php echo json_encode($arr)?>'
                                   current_url_list='<?php echo json_encode($arr_url)?>'
                                   page_description='<?php echo json_encode($description);?>'
                                   relate_product='<?php echo json_encode($relate_product);?>'
                                   price_list='<?php echo json_encode($price_list);?>'
                                   sales_price_list='<?php echo json_encode($sales_price_list);?>'
                                >
                                    <img data-num="0" src="<?php echo $static_url;?>/media/catalog/product/cache/230x115<?php echo $simple["small_image"];?>" alt="<?php echo $simple["name"];?>" imgs="<?php echo $simple["small_image"];?>">
                                </a>
                                <p class="btn-color-parent-p select_Color"></p>
                            </div>
                        <?php endforeach; ?>
                    </nav>

                    <div class="color_details_append">
                        <?php if(isset($currentSimpleData["color_details"])):?>
                            <?php if($currentSimpleData["color_details"]):?>
                                <!--<h6 class="title-control bt_class Color_Details" style="opacity: 0">Color Details:</h6>-->
                                <p style="margin-bottom: 10px;margin-top:10px;font-size: 15px;" class="box_Color"><span class="Color_Details_bt"></span> <span id="Color_Details"><?php echo $currentSimpleData["color_details"]?></span></p>
                            <?php endif;?>
                        <?php endif;?>
                    </div>
                    <div class="color_details_appends">
                        <?php if(isset($currentSimpleData["color_details"])):?>
                        <?php else:?>
                            <p class="btn-color-parent-p btn-color-parent-p-left"> <?php echo $currentSimpleData["color_value"]; ?></p>
                        <?php endif;?>
                    </div>
                    <h6 class="title-control bt_class Lenstype">
                        <div class="lensType_left">Lens type</div>
                        <div class="right_class" id="right_class_btn">
                            <a href="<?php echo $base_url?>/knowledge-center/how-to-buy-glasses-online" class="ljlensGuideText">Lens Guide</a>
                        </div>
                    </h6>
                        <div class="choose-wrap mb-4"
                         special_price="<?php echo $currentSimpleData["special_price"]?>"
                         entity_id="<?php echo $currentSimpleData["entity_id"]?>"
                         base_url="<?php echo $base_url;?>"
                         dest_name="<?php echo $dest_name; ?>"
                         stock_sku="<?php echo $currentSimpleData["stock_sku"]?>"
                         Color_Details="<?php if(isset($currentSimpleData["color_details"])):?><?php echo $currentSimpleData["color_details"];?><?php endif;?>" simple_name="<?php echo $currentSimpleData["name"]; ?>">
                        <?php foreach($simple_urls as $key=>$val):?>

                            <?php if($currentSimpleData["sku"]=="14209G07" || $currentSimpleData["sku"]=="24209G07" ):?>
                                <?php if($key=="Blue_Non_Rx"):?>
                                    <label class="js-check custom-control custom-radio active"
                                           simple_url="<?php echo $val["url"];?>"
                                           lens-type="<?php echo $key;?>"
                                           frame_group="
                                                    <?php if($currentSimpleData["sku"]=="14209G07" || $currentSimpleData["sku"]=="24209G07" ):?>
                                                        only_blue_light_blocking
                                                    <?php else:?>full_rim<?php endif;?>"
                                           price="<?php echo $val["price"];?>"
                                           sales_price="<?php echo $val["sales_price"];?>"
                                           dest_sku = "<?php echo $val["dest_sku"];?>"
                                           dest_name = "<?php echo $val["dest_name"];?>"
                                           included = '<?php echo $val["price_included"];?>'
                                           page_description="<?php echo $val["page_description"];?>"
                                        <?php if($val["relate_product"] && $val["relate_product"]!=[]):?>
                                            relate_product = '<?php echo json_encode($val["relate_product"])?>'
                                        <?php endif;?>
                                    >
                                        <input class="custom-control-input" type="radio" name="lenses" <?php if($lens_type==$key):?>checked<?php endif;?>/>
                                        <div class="custom-control-label">
                                            <?php echo $val["name"];?>
                                        </div>
                                    </label>
                                <?php else:?>
                                    <label class="js-check custom-control custom-radio active hide"
                                           simple_url="<?php echo $val["url"];?>"
                                           lens-type="<?php echo $key;?>"
                                           frame_group="
                                                    <?php if( $currentSimpleData["sku"]=="14209G07" || $currentSimpleData["sku"]=="24209G07" ):?>
                                                        only_blue_light_blocking
                                                    <?php else:?>full_rim<?php endif;?>"
                                           price="<?php echo $val["price"];?>"
                                           sales_price="<?php echo $val["sales_price"];?>"
                                           dest_sku = "<?php echo $val["dest_sku"];?>"
                                           dest_name = "<?php echo $val["dest_name"];?>"
                                           included = '<?php echo $val["price_included"];?>'
                                           page_description="<?php echo $val["page_description"];?>"
                                        <?php if($val["relate_product"] && $val["relate_product"]!=[]):?>
                                            relate_product = '<?php echo json_encode($val["relate_product"])?>'
                                        <?php endif;?>
                                    >
                                        <input class="custom-control-input" type="radio" name="lenses" <?php if($lens_type==$key):?>checked<?php endif;?>/>
                                        <div class="custom-control-label">
                                            <?php echo $val["name"];?>
                                        </div>
                                    </label>
                                <?php endif;?>
                            <?php else:?>
                                <label class="js-check custom-control custom-radio <?php if($lens_type==$key):?>active<?php endif;?>"
                                       simple_url="<?php echo $val["url"];?>"
                                       lens-type="<?php echo $key;?>"
                                       frame_group="
                                                <?php if( $currentSimpleData["sku"]=="14209G07" || $currentSimpleData["sku"]=="24209G07" ):?>
                                                    only_blue_light_blocking
                                                <?php else:?>full_rim<?php endif;?>"
                                       price="<?php echo $val["price"];?>"
                                       sales_price="<?php echo $val["sales_price"];?>"
                                       dest_sku = "<?php echo $val["dest_sku"];?>"
                                       dest_name = "<?php echo $val["dest_name"];?>"
                                       included = '<?php echo $val["price_included"];?>'
                                       page_description="<?php echo $val["page_description"];?>"
                                    <?php if($val["relate_product"] && $val["relate_product"]!=[]):?>
                                        relate_product = '<?php echo json_encode($val["relate_product"])?>'
                                    <?php endif;?>
                                >
                                    <input class="custom-control-input" type="radio" name="lenses" <?php if($lens_type==$key):?>checked<?php endif;?>/>
                                    <div class="custom-control-label">
                                        <?php echo $val["name"];?>
                                    </div>
                                </label>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                    <?php foreach($simple_urls as $key=>$val1):?>
                        <?php if($key==$lens_type):?>
                            <div class="row-sm relate_class  <?php if(!$val1["relate_product"]):?>hide<?php endif;?>" style="position: relative"
                                 id="relate_product">
                                <?php if(json_encode($val1["relate_product"])!="[]" && json_encode($val1["relate_product"])):?>
                                    <?php if($catetory_type=="eyeglasses"):?>
                                        <a href="<?php echo $base_url?>/<?php echo $val1["relate_product"]["url"]?>">Order as Sunglasses</a>
                                    <?php else:?>
                                        <a href="<?php echo $base_url?>/<?php echo $val1["relate_product"]["url"]?>">Order as Eyeglasses</a>
                                    <?php endif;?>
                                <?php else:?>
                                    <?php if($catetory_type=="eyeglasses"):?>
                                        <a href="<?php echo $base_url?>">Order as Sunglasses</a>
                                    <?php else:?>
                                        <a href="<?php echo $base_url?>">Order as Eyeglasses</a>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                        <?php endif;?>
                    <?php endforeach;?>
                    <?php if($currentSimpleData["sku"]=="65320Y09"):?>
                        <div class="GetThisPair">
                            Get This Pair Free, With Purchase Of Any Blue Block Glasses
                        </div>
                    <?php endif;?>
                    <div class="mb-2 price_mobel">
                        <div style="display: inline-block" id="price_text" class="price_text price_sku">
                            <?php if($currentSimpleData["simple_url"]["sales_price"]!="0.00" && $currentSimpleData["simple_url"]["sales_price"] && $currentSimpleData["simple_url"]["sales_price"]!=$currentSimpleData["simple_url"]["price"]):?>
                                <?php if($lens_type!="Blue_Non_Rx"):?>
                                    <p class="from_class">From: </p>
                                <?php endif;?>
                                <del class="del_price price_class">
                                    $<?php echo $currentSimpleData["simple_url"]["price"]; ?>
                                </del>
                                <div class="price h4 price_class">
                                    $<?php echo $currentSimpleData["simple_url"]["sales_price"]; ?>
                                </div>
                            <?php else:?>
                                <?php if($lens_type!="Blue_Non_Rx"):?>
                                    <a class="from_class_a">From: </a>
                                <?php endif;?>
                                <div class="price h4 price_class">
                                    $<?php echo $currentSimpleData["simple_url"]["price"]; ?>
                                </div>
                            <?php endif;?>
                        </div>
                        <div class="sku price_sku">
                            <span class="OUT_OF_STOCK stock_text right_class" configurable_id="<?php echo $configurable_id?>" current_entity_id="<?php echo $currentSimpleData["entity_id"]?>">IN STOCK</span>
                            <p class="right_class"><span class="ele_sku">SKU#</span><span class="sku_text sku_text_attr" dest_sku="<?php echo $dest_sku; ?>"><?php echo $dest_sku; ?></span></p>
                        </div>
                    </div>
                    <!--THIS PRICE INCLUDES:-->
                    <h6 class="" style="text-decoration: underline" id="What_included">What's included?</h6>
                    <h6 class="title-control bt_class mobel_includes" id="includes_class"> This price includes:</h6>
                    <ul class="list-check mobel_includes">
                        <?php $arr_included = json_decode($price_included)?>
                        <?php if($arr_included):?>
                            <?php foreach($arr_included as $included):?>
                                <li>
                                    <?php echo $included;?>
                                </li>
                            <?php endforeach;?>
                        <?php endif;?>
                    </ul>
                    <!--THIS PRICE INCLUDES End-->
                    <?php
                    $btn_name="Order Glasses";
                    switch ($lens_type) {
                        case 'Non_Rx':
                            $btn_name = "Order Non-RX Glasses";
                            break;
                        case 'Reading':
                            $btn_name = "Select Reading Power";
                            break;
                        default:
                            $btn_name = "Order Glasses";
                            break;
                    }
                    ?>
                    <div class="box_fix" id="box_fix">
                        <a href="javascript:;" id="orderRx"  class="btn btn-block mb-3  btn-primary orderRx"
                           ljstock="<?php echo $ljIsInstock ?>"
                           configure_product_id="<?php echo $configableData['entity_id']; ?>"
                           simple_product_id="<?php echo $currentSimpleData['entity_id']; ?>"
                           configure_product_sku="<?php echo $configableData['sku']; ?>"
                           simple_product_color="<?php echo $currentSimpleData['color']; ?>"
                           category_id="<?php echo $category_id; ?>"
                           stock_sku="<?php echo $currentSimpleData['stock_sku']; ?>"
                           lens-type="<?php echo $lens_type;?>"
                           catetory_type="<?php echo $catetory_type?>"
                           text="<?php if($is_php==0):?><?php echo $btn_name;?><?php else:;?>Apply For RX Glasses<?php endif;?>"
                           type_click="<?php if($is_php!=0):?>ApplyFor<?php endif;?>"
                           is_php="<?php if($is_php!=0):?>true<?php endif;?>"
                           frame_group_href="<?php echo $frame_group;?>"
                           frame_group_s="
                                    <?php if($currentSimpleData["sku"]=="14209G07" || $currentSimpleData["sku"]=="24209G07" ):?>
                                                only_blue_light_blocking
                                            <?php else:?>full_rim<?php endif;?>"
                        >
                            <?php if($is_php==0):?>
                                <?php echo $btn_name;?>
                            <?php else:?>
                                Apply For RX Glasses
                            <?php endif;?>
                        </a>
                        <?php if($currentSimpleData["support_rx"]==0):?>
                            <p class="norx_class">This frame is too large to accommodate a prescription lens!</p>
                        <?php endif;?>
                        <?php if(count($clipons)):?>
                            <div class="row-sm" style="position: relative" id="clip_ons_click">
                                Order Addl. Clip-Ons
                            </div>
                        <?php endif;?>
                        <div class="row-sm" style="position: relative">
                            <div class="col">
                                <a class="btn btn-block btn-light js-order-ws" id="save_x"
                                   currentid="<?php echo $currentSimpleData['entity_id'];?>">
                                    <i  class="fas fa-heart"></i>
                                    Save
                                </a>
                            </div>
                            <!--Try on -->
                            <div class="col btn-Try-class">
                                <a class="btn btn-block btn-light"
                                   style="<?php if(!$currentSimpleData['try_on']): ?>
                                       background-color:#fff;
                                       border-color:#eaeaea;
                                       color:#212529;
                                   <?php endif; ?>"
                                   id="btn-Try"
                                   currnt_sku="<?php echo $currentSimpleData["stock_sku"]?>"
                                   sku="<?php echo $currentSimpleData["sku"]?>"
                                   catetory_type="<?php echo $catetory_type;?>"
                                   flag_tryon="<?php if(!$currentSimpleData['try_on']): ?>false<?php else: ?>true<?php endif; ?>"
                                >
                                    <i  class="fas fa-glasses"></i>
                                    Try on
                                </a>
                            </div>
                            <p id="tip_save">Item has been removed for NEW NAME</p>
                        </div>


                    </div>
                    <div class="mb-3 mt-3 share_data">
                        <span class="title-control mr-3 bt_class" id="share_data"
                              configure_product_id="<?php echo $configableData['entity_id']; ?>"
                              configure_product_name="<?php echo $configableData['name']; ?>"
                              _is_shared = "<?php echo $_is_shared;?>";
                              _share_price = "<?php echo $_share_price;?>";
                              _final_price = "<?php echo $_final_price;?>";
                              thumbnail_url = "<?php echo $static_url; ?>/media/catalog/product<?php echo $currentSimpleData['thumbnail'];?>";
                              support_share_and_save = "<?php echo $support_share_and_save;?>";

                        >share</span>
                        <a  class="btn btn-icon rounded-circle btn-facebook fb-share  porto-icon-facebook fb-share_class">
                        </a>
                        <a class="btn btn-icon rounded-circle btn-twitter tw-share porto-icon-twitter tw-share_class">
                        </a>
                        <a class="btn btn-icon rounded-circle btn-google pint-share porto-icon-pinterest pint-share_class">
                        </a>
                        <div class="link_class">
                            <span id="fb-root"></span>
                            <script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0"></script>
                            <span class="fb-like padding-left15" data-href=""
                                  data-layout="button_count" data-action="like" data-size="small" data-show-faces="true"
                                  data-share="false" style="display: inline-block; line-height: 1">
                                </span>
                        </div>
                    </div>
                </article>
            </main>
        </div>
    </div>
</section>


<!-- ======= Recommend Start ======= -->
<section class="padding-top">
<div class="swiper-container recommend container container1" id="recommend_3">
    <h2 class="hide default_show">You May Also Like</h2>
    <div class="swiper-wrapper" id="append_recommend">
    </div>
    <div class="swiper-button-prev prev_click swiper-button-hide hide default_show"></div>
    <div class="swiper-button-next next_click swiper-button-hide hide default_show"></div>
</div>
</section>
<!-- ======= Recommend ENd ======= -->

<!--description-->
<section class="padding-y padding-top description_mobel">
    <div class="bt_class container container1 bot_px">
        <p class="bt_class">description</p>
    </div>
    <div class="description container container1">
        <?php
        if (isset($currentSimpleData["description"])) {
            echo $currentSimpleData["description"];
        }
        ?>
    </div>
</section>
<!--description End-->
<!--Dtails start-->
<section class="padding-y padding-top" id="mobel_ditles">
</section>
<!--Dtails End-->
<!--Reviews-->
<section class="padding-y pdp-parameters padding-top padding-reviews">
    <div class="bt_class container container1 bot_px">
        <p class="bt_class Reviews_mobel">Reviews</p>
    </div>
    <div class="container container1 reviews_bgc">
        <p class="succes_review hide">Thanks for your review ! Your review will be live in a few days.</p>
        <div class="row">
            <div class="col-lg-12  col-xs-12">
                <div class="tab-content">
                    <div class="tab-pane reviews zx-container tab-pane-show" id="reviews">
                        <?php if($reviews["cnt"]<=0):?>
                            <!--reviews Display not logged in-->
                            <div class="mt-3">
                                <p class="title-control mb-3">THERE ARE NO CUSTOMER REVIEWS YET</p>
                                <p class="mb-3">Be the first to write a review</p>
                                <a href="javascript:;" class="mb-3 btn btn-outline-primary review-pop-show">Write A Review</a>
                            </div>
                        <?php else:?>
                            <!--reviews Signed in to show-->
                            <div class="ydl_reviews">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <span class="zx-score"> OVERALL SCORE</span>
                                        <span class="zx-5sE">
                                        <span class="zx-sort"><?php echo $reviews["total_rating"]; ?></span>
                                        <span class="star-ratings__top___3IeBI zx-rating">
                                           <div class="Stars" style="--rating:<?php echo $reviews["total_rating"]; ?>" aria-label="Rating of this product is 2.3 out of 5.">
                                            </div>
                                        </span>
                                    </span>
                                        <span class="rsr-top-row-div___WfUnu font-italic">Based on
                                            <span class="rsr-total-reviews"><?php echo $reviews["cnt"]; ?></span> reviews</span>
                                        <button class="btn-secondary rsr-write-a-review-btn Write_a_Review pc_review review-pop-show">Write a Review</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zx-box_tt">
                                        <div class="con z_box">
                                            <div class="circle">
                                                <div class="pie_left"><div class="left_circle"></div></div>
                                                <div class="pie_right"><div class="right_circle"></div></div>
                                                <div class="mask"><i class="fa fa-thumbs-o-up fa___2LJPU fa_circle fa_circle"></i><span style="display: none;"><?php echo $reviews["rate_will_recommend"]; ?></span></div>
                                            </div>
                                        </div>
                                        <div class="z_box">
                                            <h3 class="zx-bfb"><?php echo $reviews["rate_will_recommend"]; ?>%</h3>
                                        </div>
                                        <div class="z_box">
                                            <h5 class="zx-would">WOULD RECOMMEND TO A FRIEND.</h5>
                                        </div>
                                    </div>
                                </div>

                                <div id="reviews_list_box" class='' style='margin-top:-33px'>
                                    <!--                                    <div class="smak_review">-->
                                    <!--                                        <img alt="Loading..." src="--><?php //echo $static_url; ?><!--/static/version1571822590/frontend/Pg/payne/en_US/images/loader-2.gif">-->
                                    <!--                                    </div>-->
                                    <div id="reviews_list">

                                        <?php foreach($reviews['data'] as $reviews_list):?>
                                            <div class="" style="margin-top: 10px;width: 100%">
                                                <article class="item-comment box data_list_reviews"
                                                         is_like="<?php echo $reviews_list["is_like"];?>"
                                                         likes_num="<?php echo $reviews_list["likes_num"];?>"
                                                         reviews_id="<?php echo $reviews_list["reviews_id"];?>"
                                                         style="position:relative;" padding-bottom="">
                                                    <div class="icontext align-items-start">
                                                        <img src="https://static.payneglasses.com/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/images/avatar.png" class="icon avatar-md">
                                                        <div class="text">
                                                            <h6 class="mb-1">
                                                                <?php echo $reviews_list["subject"]?>
                                                                <small class="text-muted"> By <?php echo $reviews_list["nickname"]?> <?php echo $reviews_list["post_date_html"]?></small>
                                                            </h6><span class="zx-sort"><?php echo $reviews_list["review_rating"]?></span>
                                                            <ul class="rating-stars">
                                                                <li style="width:<?php echo $reviews_list["review_rating"]*20 ?>%" class="stars-active xx_class">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                </li>
                                                                <li class="xx_class">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                </li>
                                                                <span>color: <?php echo $reviews_list["color"];?></span>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="mt-3">
                                                        <p><?php echo $reviews_list["post_content"];?></p>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php endforeach;?>

                                    </div>
                                    <?php if($reviews["pagecount"]>1):?>
                                        <div class="review_show_more" reviews_page = "<?php echo $reviews["pagecount"]; ?>">show more</div>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Reviews End-->
<!-- facebook分享 -->
<div class="faceImgBox">
    <span class="closeFace">×</span>
    <img class="faceImg" src="" alt="" />
    <i class="btn btn-icon rounded-circle btn-facebook porto-icon-facebook share_f_i"></i>
</div>


<!-- ======= History Start   luanjue ======= -->
<div id="recentlyViewed">
    <div id="historyTitle">
        <h2>Your Recently Viewed</h2>
    </div>
    <div class="container container_style_LJ">
        <div class="row row_LJ" id="append_history">

        </div>
    </div>
</div>
<!-- ======= History end   luanjue ======= -->


<!--为了控制数据采集开关  luanjue start -->
<input type="hidden" id="smartCollectUrl" value="<?php echo $collection_api_url.'/webyiiapi/data-collection/push-new.html'; ?>" />
<input type="hidden" id="smartCollectOn" value="<?php echo $collection_enable; ?>" />
<!--为了控制数据采集开关  luanjue end -->

<div id="pixlee_container"></div>
<script type="text/javascript">
    window.PixleeAsyncInit = function() { 
        Pixlee.init({apiKey:'7QHns49Pbq2NGgD87pAL'});
        Pixlee.addProductWidget({accountId:1559, widgetId:1998478, skuId:"<?php echo $dest_sku; ?>"});};
</script> 
<script src="https://assets.pxlecdn.com/assets/pixlee_widget_1_0_0.js"></script>

<!--footer HTML Start-->
<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("new_footer")->toHtml(); ?>
<!--footer HTML End-->
<!--Introduction of JS Start-->



<!-- google recommended测试  start -->
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/googleRecommend.js"></script>
<!-- google recommended测试  end -->

<!--  新引入文件 数据采集  luanjue  start -->
<?php if($collection_enable == 1) { ?>
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/smart.sync.js"></script>
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/new_collection.pdp.js"></script>
<?php } ?>
<!--  新引入文件 数据采集  luanjue  end -->
<script src="<?php echo $static_url;?>/assets2/resource/version<?php echo $js_version; ?>/js/edito.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/social.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/pdp-revision/detail.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/save.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/js/pdp-social-share.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/bootstrap.min.js"></script>
<script src="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/notes.js"></script>
<script src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/pdp-revision/swiper.js"></script>
<script src="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/layer.js?v=<?php echo $js_version;?>"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/js/footer.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/reviews.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon_js/try-on-api-core.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon_js/try-on-api-webcams.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon_js/try-on-api-tracking.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon_js/try-on-api-rendering.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon_js/try-on-api-user-analysis.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon_js/try-on-api-frame-recommendation.js"></script>
<!--<script src="--><?php //echo $static_url; ?><!--/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon_js/try-on-api-calibration.js?v=--><?php //echo $js_version; ?><!--"></script>-->

<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/tryon.js"></script>
<!--Introduction of JS End-->
<script src="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/user_behavior_analysis.js"></script> <!--data acquisition-->
<!--<script async="async" src='https://static-na.payments-amazon.com/OffAmazonPayments/us/sandbox/js/Widgets.js'>-->
<script async="async" type='text/javascript' src="<?php echo $objectManager->get('\Pg\Learning\Block\Page\Banner')->getCoreHelper()->getWidgetUrl(); ?>"></script>


<!--    用户历史足迹 history  luanjue  start  -->
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/history_pdp.js"></script>
<!--    用户历史足迹 history  luanjue  end  -->

<script type="text/javascript">
    /*    for google Recommend  detail-page-view  start   */
    setTimeout(function() {
        // console.log(window.loginCustomer.customer.current_profile_id, "------------pdp页面");
        var googleUserInfo = {
            'visitorId': getLocalDeviceID
        };
        var googleUserID = window.loginCustomer.customer.current_profile_id;
        if(googleUserID) {
            googleUserInfo.userId = googleUserID;
        }
        var ljpdp_Data_session = getDataFun();
        // dataLayer = dataLayer || [];
        dataLayer.push({
            'event':'detail-page-view',
            'automl': {
                'eventType': 'detail-page-view',
                'userInfo': googleUserInfo,
                'productEventDetail': {
                    'productDetails': [{
                        "id": ljpdp_Data_session['sku'],
                    }]
                }
            }
        });
    }, 3000);
    /*    for google Recommend  detail-page-view  end   */

    $("body").on("click","#orderRx",function() {
        /*  for google Recommend AI  add-to-cart  start   */
        var googleUserInfo = {
            'visitorId': getLocalDeviceID
        };
        var googleUserID = window.loginCustomer.customer.current_profile_id;
        if(googleUserID) {
            googleUserInfo.userId = googleUserID;
        }
        var ljpdp_Data_session = getDataFun();
        if($(this).attr("lens-type") == "Frame_Only") {
            // dataLayer = dataLayer || [];
            dataLayer.push({
                'event':'add-to-cart',
                'automl': {
                    'eventType': 'add-to-cart',
                    'userInfo': googleUserInfo,
                    'productEventDetail': {
                        'productDetails': [{
                            'id': ljpdp_Data_session['sku'],
                            'quantity': 1
                        }]
                    }
                }
            });
        }
        /*  for google Recommend API  add-to-cart  end   */
    });
</script>


<script>
    /*  Klaviyo Viewed Producted 产品采集  luanjue start   */
    var nowUrl = window.location.href;
    var klaviyoSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
    var klaviyoPrice = $("#fix-fluid div.container-fluid.reveiws-container").attr("price");
    var klaviyoEntityID = $("#fix-fluid div.container-fluid.reveiws-container").attr("entry_id");
    var klaviyoName = $("#fix-fluid div.container-fluid.reveiws-container").attr("name");
    var _learnq = _learnq || [];
    _learnq.push(['track', 'Viewed Product', {
        'Last viewed:' : nowUrl,
        'sku' : klaviyoSku,
        'entity_id' : klaviyoEntityID,
        'price' : klaviyoPrice,
        'name' : klaviyoName
    }]);
    /*  Klaviyo Viewed Producted 产品采集  luanjue start   */

    /****  Klaviyo事件  点击order glasses采集（统一为add to cart事件）   start            *  */
    $("body").on("click","#orderRx",function() {
        _learnq.push(['track', 'Add To Cart', {
            'Last viewed:' : nowUrl,
            'sku' : klaviyoSku,
            'entity_id' : klaviyoEntityID,
            'price' : klaviyoPrice,
            'name' : klaviyoName
        }]);
    });
    /*   Klaviyo事件  点击order glasses采集（统一为add to cart事件）    end      */
</script>


</body>
</html>
