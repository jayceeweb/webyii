<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<table width="200" border="1">
    <tbody>
    <tr>
        <td colspan="3">
            Sunglasses that do not support RX： <a href="https://www.payneglasses.com/sunglasses/women/swan-round-3582/black-non-rx-45419K09.html" target="_blank">https://www.payneglasses.com/sunglasses/women/swan-round-3582/black-non-rx-45419K09.html</a><br><br>

            PHP glasses: <a href="https://www.payneglasses.com/eyeglasses/women/mari-round-447/black-prescription-grant-15713G17.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/mari-round-447/black-prescription-grant-15713G17.html</a><br><br>

            Glasses with clip-on: <a href="https://www.payneglasses.com/eyeglasses/women/zuma-rectangle-3717/black-prescription-19311K08.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/zuma-rectangle-3717/black-prescription-19311K08.html</a><br><br>

            Sports glasses: <a href="https://www.payneglasses.com/eyeglasses/kids/beckham-sports-goggles-6502/black-prescription-grant-39264K09.html" target="_blank">https://www.payneglasses.com/eyeglasses/kids/beckham-sports-goggles-6502/black-prescription-grant-39264K09.html</a><br><br>

            Frame with both sunglasses and eyeglasses: <a href="https://www.payneglasses.com/eyeglasses/women/reyna-classic-square-520/red-bifocal-11205R07.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/reyna-classic-square-520/red-bifocal-11205R07.html</a><br><br>

            Frame with reviews: <a href="https://www.payneglasses.com/eyeglasses/women/rogan-square-6508/green-prescription-11319E09.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/rogan-square-6508/green-prescription-11319E09.html</a><br><br>

            Frame with color details: <a href="https://www.payneglasses.com/sunglasses/women/hayworth-rectangle-7080/black-prescription-41321K09G.html" target="_blank">https://www.payneglasses.com/sunglasses/women/hayworth-rectangle-7080/black-prescription-41321K09G.html</a><br><br>

            Out of stock frames: <a href="https://www.payneglasses.com/eyeglasses/women/dwight-aviator-6364/gray-prescription-16714G09.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/dwight-aviator-6364/gray-prescription-16714G09.html</a><br><br>

            Glasses with tag link: <a href="https://www.payneglasses.com/eyeglasses/women/ravine-oval-4299/black-prescription-11311K09.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/ravine-oval-4299/black-prescription-11311K09.html</a><br><br>

            Color changing glasses: <a href="https://www.payneglasses.com/eyeglasses/women/huron-square-6020/gray-prescription-16302G09.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/huron-square-6020/gray-prescription-16302G09.html</a><br><br>

            Try on eyeglasses: <a href="https://www.payneglasses.com/sunglasses/women/hayworth-rectangle-7086/blue-prescription-41321U09G.html" target="_blank">https://www.payneglasses.com/sunglasses/women/hayworth-rectangle-7086/blue-prescription-41321U09G.html</a><br><br>

            Try on sunglasses: <a href="https://www.payneglasses.com/eyeglasses/women/neville-rectangle-5460/brown-prescription-11313B09.html" target="_blank">https://www.payneglasses.com/eyeglasses/women/neville-rectangle-5460/brown-prescription-11313B09.html</a><br><br>
        </td>
    </tr>
    <?php foreach ($list as $row): ?>
    <tr>
        <td nowrap><a href="https://rxgl.co/<?php echo $row['r_code']; ?>" target="_blank"><?php echo $row['r_code']; ?></a></td>
        <td nowrap><a href="/<?php echo $row['request_path']; ?>" target="_blank"><?php echo $row['request_path']; ?></a></td>
        <td nowrap><a href="/<?php echo $row['request_path']; ?>?json=1" target="_blank"><?php echo $row['request_path']; ?>?json=1</a></td>
        <td nowrap><?php if ($row['is_in_stock']): ?>In Stock<?php else: ?>Out Of Stock<?php endif; ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</body>
</html>