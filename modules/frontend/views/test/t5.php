<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php $this->registerCsrfMetaTags() ?>
    <title>无标题文档</title>
</head>

<body>
<form method="post">
    <input type="hidden" name="_csrf" value="<?php echo Yii::$app->request->csrfToken; ?>" />
    <input type="hidden" name="action" value="rewards invitation" />
    <table width="300" border="1">
        <tbody>
        <tr>
            <td width="5%">事件ID:</td>
            <td>rewards invitation</td>
            <td width="20%">在Flow中配置此事件ID</td>
        </tr>
        <tr>
            <td nowrap>用户属性：</td>
            <td><textarea cols="100" rows="15" name="customer_properties">{
    "$email": "zhuqiuying@zhijingoptical.com",
    "$first_name": "lih",
    "$last_name": "haifeng",
    "email": "zhuqiuying@zhijingoptical.com",
    "first_name": "lih",
    "last_name": "haifeng",
    "customer_id_auth_code": "Yjc0M2Y1WTJUM2tLV1NJSmdyMVd2Yjg5ZW03bVROeWRoZVQwM0s5MGhBPT0=",
    "email_auth_code": "ZWYwNjBTem12Y2JVVFdZUHVyK2FZdUNTd0xWbHF4MU1kdGd0MTZlL0xxQ3RDWnB6NzNXTWhSeFN0RGR5Ym52MzRFeEw3dz09",
    "register_date": "2020-12-20",
    "so_customer_id": 225,
    "tmp_rewards_invitation_id": 87,
    "tmp_rewards_invitation_id_auth_code": "NDAwN3hMZ2hXUnZ1SnlsTjBOT1JNSlJ5bjE5QmMvQTBwWFllNnhzMG1RPT0=",
    "email_expiration_time":"2020-12-27"
}</textarea>
            </td>
            <td nowrap>
                $email表示要发送邮件的用户名<br>
                $first_name可修改<br>
                $last_name可修改<br>
            </td>
        </tr>
        <tr>
            <td>事件属性：</td>
            <td><textarea cols="100" rows="15" name="properties">{
    "$email": "zhuqiuying@zhijingoptical.com",
    "$first_name": "lih",
    "$last_name": "haifeng",
    "email": "zhuqiuying@zhijingoptical.com",
    "first_name": "lih",
    "last_name": "haifeng",
    "customer_id_auth_code": "Yjc0M2Y1WTJUM2tLV1NJSmdyMVd2Yjg5ZW03bVROeWRoZVQwM0s5MGhBPT0=",
    "email_auth_code": "ZWYwNjBTem12Y2JVVFdZUHVyK2FZdUNTd0xWbHF4MU1kdGd0MTZlL0xxQ3RDWnB6NzNXTWhSeFN0RGR5Ym52MzRFeEw3dz09",
    "register_date": "2020-12-20",
    "so_customer_id": 225,
    "tmp_rewards_invitation_id": 87,
    "tmp_rewards_invitation_id_auth_code": "NDAwN3hMZ2hXUnZ1SnlsTjBOT1JNSlJ5bjE5QmMvQTBwWFllNnhzMG1RPT0=",
    "email_expiration_time":"12/27/2020"
}</textarea></td>
            <td></td>
        </tr>
        <tr>
            <td>
                执行结果
            </td>
            <td><?php echo $result; ?></td>
            <td>当结果为1时表示成功</td>
        </tr>
        <tr>
            <td colspan="3" align="right">
                <button>提交</button>
            </td>
        </tr>
        </tbody>
    </table>
    <br>

</form>
</body>
</html>
