<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
$static_url=$data["static_url"];
$js_version=$data["js_version"];
$magento_version=$data["magento_version"];
$keyword = $data["data"]["keyword"];
$current_url_q = $data["data"]["current_url_q"];

$data["data"]["current_url_q"]["sort"] = '';
$current_url_q["sort"]="";
$sort_null=http_build_query($current_url_q);
$current_url_q["sort"]="price_asc";
$sort_price_asc=http_build_query($current_url_q);
$current_url_q["sort"]="price_desc";
$sort_price_desc=http_build_query($current_url_q);
$current_url_q["sort"]="new_asc";
$sort_new_asc=http_build_query($current_url_q);
$current_url_q["sort"]="name_asc";
$sort_name_asc=http_build_query($current_url_q);

// echo $sort_name_asc;exit;   keyword=oval&page=1&gategory=eye_glasses&Availability=In+Stock&sort=name_asc
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="description" content=" Find cheap deals on Payne Glasses for <?php echo $keyword; ?>, including eyeglasses, sunglasses, prescription glasses and glasses frames."/>
    <meta name="keywords" content=" <?php echo $keyword; ?>, prescription glasses, eyeglasses, cheap glasses, glasses online"/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Search Results for <?php echo $keyword; ?> (Page <?php echo $data['data']['page'] ?>) | Payne Glasses</title>
    <link rel="icon" type="image/x-icon" href="<?php echo $static_url; ?>/media/favicon/stores/1/logo_small_110.jpg"/>
    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/media/favicon/stores/1/favIcon.ico"/>

    <link rel="preload" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/css/styles-m.min.css" as="style" />
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/css/styles-m.min.css"/>

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/bootstrap/css/bootstrap.min.css"/>

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/bootstrap/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/bootstrap/css/bootstrap-theme.min.css"/>

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/assets/owl.carousel.min.css"/>

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/icon-fonts/css/porto-icons-codes.min.css"/>
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/icon-fonts/css/porto-icons-codes.min.css"/>

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/icon-fonts/css/animation.min.css"/>
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/icon-fonts/css/animation.min.css"/>

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/fonts/iconfont.css">
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/fonts/iconfont.css">

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/notes.css"> <!--//广告-->
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/notes.css"> <!--//广告-->

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/footer_2020.css">
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/footer_2020.css">

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/fonts/iconfont.css">
    <link rel="stylesheet" type="text/css" load_last="true" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/fonts/iconfont.css">


    <!-- 新引入文件  start -->
    <link rel="preload" as="style" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/swiper-bundle.min.css" />
    <link rel="stylesheet" load_last="true" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/swiper-bundle.min.css" />

    <!--    <link rel="preload" as="style" href="--><?php //echo $static_url; ?><!--/assets2/resource/version--><?php //echo $js_version; ?><!--/new_list/css/idangerous.swiper.css" />-->
    <!--    <link rel="stylesheet" load_last="true" href="--><?php //echo $static_url; ?><!--/assets2/resource/version--><?php //echo $js_version; ?><!--/new_list/css/idangerous.swiper.css" />-->

    <link rel="preload" as="style" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/new_list.css" />
    <link rel="stylesheet" load_last="true" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/new_list.css" />

    <link rel="preload" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/jquery-3.3.1.min.js" as="script" />
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/jquery-3.3.1.min.js"></script>
    <!-- 新引入文件  end -->

</head>
<body>

<!--Advertisement Start-->
<div class="message">
    <div id="message_show">
        <div class='message_container'>
            <div class="message_two_child owl-carousel owl-theme">

            </div>
        </div>
    </div>
</div>
<!--Advertisement End-->

<!--header Start-->
<?php
//<!-- ======= Head Lntroduction ======= -->
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>
<!--header End-->

<div class="breadcrumbs" id="breadcrumbs" style="z-index:99999">
    <ul class="items">
        <li class="home item_home">
            <a href="/" title="Go to Home Page" class="hover_undu">
                Home
            </a>
            <i class="iconfont icon-arrow-right-copy header_icon"></i>
        </li>
        <li class="item category5">
            <span>
                Search results for: '<?php echo $keyword;?>'
            </span>
        </li>
    </ul>
</div>

<!--  List 开始  -->
<!-- 判断是否有数据 没有数据显示No Results  start -->
<?php if(!$data['data']['hits']) {   ?>
    <h1 class="ljSearchForH1">
        No results were found for '<?php echo $keyword; ?>'
    </h1>
<?php }else{ ?>
    <h1 class="ljSearchForH1">
        Search results for: '<?php echo $keyword; ?>'
    </h1>
<?php } ?>
<!-- 判断是否有数据 没有数据显示No Results  end -->


<!--  filter PC start  -->
<div id="ljFilterNav">
    <div style="width:20px;"></div>
    <div class="ljFilterTitle forSelectedActive">
        <input type="hidden" name="Eyeglasses" value="<?php echo isset($_GET['Eyeglasses'])?$_GET['Eyeglasses']:'0'; ?>" />
        <input type="hidden" name="Sunglasses" value="<?php echo isset($_GET['Sunglasses'])?$_GET['Sunglasses']:'0'; ?>" />
        <span class="ljDropdownTitle <?php if((isset($_GET['Eyeglasses'])&&!empty($_GET['Eyeglasses']))||(isset($_GET['Sunglasses'])&&!empty($_GET['Sunglasses']))) { echo "ljSelectedColor"; }else{echo "";} ?>">Category</span>
        <div class="ljTitleDropdownContent">
            <ul class="list-unstyled">
                <li>
                    <a href="" data="1" ljtype="Eyeglasses" <?php if(isset($_GET['Eyeglasses'])){ $arr=explode(",",$_GET['Eyeglasses']);if(in_array("1",$arr)){echo "style='color:#f90'";}} ?> >
                        <span class="ljCheckBox <?php if(isset($_GET['Eyeglasses'])){ $arr=explode(",",$_GET['Eyeglasses']);if(in_array("1",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                        Eyeglasses
                    </a>
                </li>
                <li>
                    <a href="" data="1" ljtype="Sunglasses" <?php if(isset($_GET['Sunglasses'])){ $arr=explode(",",$_GET['Sunglasses']);if(in_array("1",$arr)){echo "style='color:#f90'";}} ?> >
                        <span class="ljCheckBox <?php if(isset($_GET['Sunglasses'])){ $arr=explode(",",$_GET['Sunglasses']);if(in_array("1",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                        Sunglasses
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="ljFilterTitle forSelectedActive">
        <input type="hidden" name="Stock" value="<?php echo isset($_GET['Stock'])?$_GET['Stock']:'0'; ?>" />
        <input type="hidden" name="is_retired" value="<?php echo isset($_GET['is_retired'])?$_GET['is_retired']:'0'; ?>" />
        <span class="ljDropdownTitle <?php echo (isset($_GET['Stock'])&&!empty($_GET['Stock'])) || (isset($_GET['is_retired'])&&!empty($_GET['is_retired'])) ?'ljSelectedColor':''; ?>">Availability</span>
        <div class="ljTitleDropdownContent">
            <ul class="list-unstyled">
                <li>
                    <a href="" data="true" ljtype="Stock" <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("true",$arr)){echo "style='color:#f90'";}} ?> >
                        <span class="ljCheckBox <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("true",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                        In Stock
                    </a>
                </li>
                <li>
                    <a href="" data="false" ljtype="Stock" <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("false",$arr)){echo "style='color:#f90'";}} ?> >
                        <span class="ljCheckBox <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("false",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                        Out Of Stock
                    </a>
                </li>
                <li>
                    <a href="" data="true" ljtype="is_retired" <?php if(isset($_GET['is_retired'])){ $arr=explode(",",$_GET['is_retired']);if(in_array("true",$arr)){echo "style='color:#f90'";}} ?> >
                        <span class="ljCheckBox <?php if(isset($_GET['is_retired'])){ $arr=explode(",",$_GET['is_retired']);if(in_array("true",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                        Retired
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!--  sort排序  PC start  -->
    <div id="sortBy">
        <input type="hidden" name="sort" value="<?php echo isset($_GET['sort'])?$_GET['sort']:'0'; ?>" />
        <div class="ljsortByTitle"> Sort By： </div>
        <div id="ljsortResult">
            <div class="ljIcon">
                <span class="sort-result">Newest</span>
                <span class="sort-open"></span>
            </div>
            <div class="ljSortByContent">
                <ul class="list-unstyled">
                    <li>
                        <a href="#" data="new_asc" ljtype="sort" style="display: block;"> Newest </a>
                    </li>
                    <li>
                        <a href="#" data="price_desc" ljtype="sort" style="display: block;"> Price High to Low </a>
                    </li>
                    <li>
                        <a href="#" data="price_asc" ljtype="sort" style="display: block;"> Price Low to High </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--  sort排序 PC  end  -->
</div>

<!--  sort排序显示 MOBILE start  -->
<div class="ljSortByContent2">
    <span class="bot"></span>
    <span class="top"></span>

    <input type="hidden" name="sort2" value="<?php echo isset($_GET['sort'])?$_GET['sort']:'0'; ?>" />
    <ul class="list-unstyled">
        <li>
            <a href="#" data="new_asc" ljtype="sort"> Newest </a>
        </li>
        <li>
            <a href="#" data="price_desc" ljtype="sort"> Price High to Low </a>
        </li>
        <li>
            <a href="#" data="price_asc" ljtype="sort"> Price Low to High </a>
        </li>
    </ul>
</div>
<!--  sort排序显示 MOBILE end  -->
<!--  filter PC end  -->



<!--  filter MOBILE start  -->
<div id="ljFilterNav_m">
    <div id="ljFilter_m">
        <span> Filter </span>
    </div>
    <div id="sortBy2">
        <div class="ljsortByTitle2">
            <span>Sort By：</span>  <span class="ljIcon"> Newest </span>
        </div>
    </div>
</div>

<div id="ljFilter_m_content">
    <div id="ljFilerTitle_m">
        <div style="width: 160px;">Filter Frames By：</div>
        <span id="ljCloseFilter"> × </span>
    </div>
    <div id="ljFilterContent_m">
        <div class="ljFilterTitle forSelectedActive">
            <input type="hidden" name="Eyeglasses" value="<?php echo isset($_GET['Eyeglasses'])?$_GET['Eyeglasses']:'0'; ?>" />
            <input type="hidden" name="Sunglasses" value="<?php echo isset($_GET['Sunglasses'])?$_GET['Sunglasses']:'0'; ?>" />
            <span class="ljDropdownTitle <?php if((isset($_GET['Eyeglasses'])&&!empty($_GET['Eyeglasses']))||(isset($_GET['Sunglasses'])&&!empty($_GET['Sunglasses']))) { echo "ljSelectedColor"; }else{echo "";} ?>">Category</span>
            <div class="ljTitleDropdownContent">
                <ul class="list-unstyled">
                    <li>
                        <a href="" data="1" ljtype="Eyeglasses" <?php if(isset($_GET['Eyeglasses'])){ $arr=explode(",",$_GET['Eyeglasses']);if(in_array("1",$arr)){echo "style='color:#f90'";}} ?>>
                            <span class="ljCheckBox <?php if(isset($_GET['Eyeglasses'])){ $arr=explode(",",$_GET['Eyeglasses']);if(in_array("1",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                            Eyeglasses
                        </a>
                    </li>
                    <li>
                        <a href="" data="1" ljtype="Sunglasses" <?php if(isset($_GET['Sunglasses'])){ $arr=explode(",",$_GET['Sunglasses']);if(in_array("1",$arr)){echo "style='color:#f90'";}} ?>>
                            <span class="ljCheckBox <?php if(isset($_GET['Sunglasses'])){ $arr=explode(",",$_GET['Sunglasses']);if(in_array("1",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                            Sunglasses
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="ljFilterTitle forSelectedActive">
            <input type="hidden" name="Stock" value="<?php echo isset($_GET['Stock'])?$_GET['Stock']:'0'; ?>" />
            <input type="hidden" name="is_retired" value="<?php echo isset($_GET['is_retired'])?$_GET['is_retired']:'0'; ?>" />
            <span class="ljDropdownTitle <?php echo (isset($_GET['Stock'])&&!empty($_GET['Stock'])) || (isset($_GET['is_retired'])&&empty($_GET['is_retired'])) ? 'ljSelectedColor':''; ?>">Availability</span>
            <div class="ljTitleDropdownContent">
                <ul class="list-unstyled">
                    <li>
                        <a href="" data="true" ljtype="Stock" <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("true",$arr)){echo "style='color:#f90'";}} ?>>
                            <span class="ljCheckBox <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("true",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                            In Stock
                        </a>
                    </li>
                    <li>
                        <a href="" data="false" ljtype="Stock" <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("false",$arr)){echo "style='color:#f90'";}} ?>>
                            <span class="ljCheckBox <?php if(isset($_GET['Stock'])){ $arr=explode(",",$_GET['Stock']);if(in_array("false",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                            Out Of Stock
                        </a>
                    </li>
                    <li>
                        <a href="" data="true" ljtype="is_retired" <?php if(isset($_GET['is_retired'])){ $arr=explode(",",$_GET['is_retired']);if(in_array("true",$arr)){echo "style='color:#f90'";}} ?>>
                            <span class="ljCheckBox <?php if(isset($_GET['is_retired'])){ $arr=explode(",",$_GET['is_retired']);if(in_array("true",$arr)){echo "ljCheckBoxActive";}} ?>"> √ </span>
                            Retired
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div style="height:30px;width:100%;"></div>
    </div>
</div>
<!--  filter MOBILE end  -->


<!-- 页面加载中处理  start -->
<div class="shadow">
    <div class="loading-placeholder">
        <img src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/image/load.gif" />
    </div>
</div>
<!-- 页面加载中处理  end -->

<!--  遮罩阴影  start  -->
<div class="shadow2"></div>
<!--  遮罩阴影  end  -->


<div class="ljContainer">
    <!-- pjax数据 start -->
    <div id="ljListPageContentPjax">
        <?php echo $this->render("@app/modules/frontend/views/search/search_pjax.php",[
            "data"=>$data
        ]);?>
    </div>
    <!-- pjax数据 start -->
</div>
<!--  list结束  -->




<!--footer Start-->
<?php
echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("new_footer")->toHtml(); ?>
<!--footer End-->

</body>
<link rel="preload" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/owl.carousel.min.js" as="script" />
<script src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/owl.carousel.min.js"></script>

<link rel="preload" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/notes.js" as="script" />
<script src="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/notes.js"></script> <!--//广告-->

<!-- 新加载的JS文件 start -->
<link rel="preload" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/jquery.pjax.js" as="script" />
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/jquery.pjax.js"></script>

<link rel="preload" href="<?php echo $static_url; ?>/resource2/js/bootstrap.min.js" as="script" />
<script src="<?php echo $static_url; ?>/resource2/js/bootstrap.min.js"></script>

<link rel="preload" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/swiper.min.js" as="script" />
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/swiper.min.js"></script>

<link rel="preload" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/new_list.js" as="script" />
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/new_list.js"></script>
<!-- 新加载的JS文件 end -->

<link rel="preload" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/footer.js" as="script" />
<script src="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/footer.js"></script>
</html>
