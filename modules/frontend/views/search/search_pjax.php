<?php
$static_url=$data["static_url"];
$js_version=$data["js_version"];
$magento_version=$data["magento_version"];
$keyword = Yii::$app->getRequest()->get('keyword','');

function getPDPUrl($frame_simple_url,$lens_types=["Single","Blue_Non_Rx","Non_Rx","Bifocal"]){
    foreach ($lens_types as $lens_type) {
        if (isset($frame_simple_url[$lens_type])) {
            return $frame_simple_url[$lens_type]["url"];
        }
    }
    return "";
}

// 如果总页数小于当前要跳转到的页数 并且有数据 返回第一页
if($data['data']['pages']<$data['data']['page'] && count($data['data']['hits'])>0) {
    header("Location: ".$data["base_url"]."/search.html?keyword=".$keyword."&page=1");
}
// var_dump($data["base_url"]."baseURL");exit;
?>

<!--   没有数据显示   -->
<?php if(!$data['data']['hits']) {   ?>
    <div class="ljSearchForNodata">
        Please modify your search term or try browsing our store to find what you want.
    </div>
<?php } ?>
<!--   没有数据显示   -->

<div id="ljNowPage" data="<?php echo $data['data']['page']; ?>"></div>
<div id="ljNowKeywords" data="<?php echo $keyword; ?>"></div>

<div class="row ljListPageContent">
    <?php foreach($data['data']['hits'] as $lk=>$list): ?>
        <?php // print_r($list['_source']['simples']);exit; ?>
        <div class="text-center col-xs-12 col-sm-12 col-md-4 col-lg-4 ljShadowBg">
            <div class="ljListNew">
                <?php $kk = $lk+1; ?>
                <div class="swiper-container <?php echo "lj_swiper".$kk; ?>">
                    <div class="swiper-wrapper">
                        <?php foreach($list['_source']['simples'] as $sk=>$sv): ?>
                            <?php
                            if($sv['visible_plp_list']) {
                                $orderUrl = getPDPUrl($sv['frame_simple_url']);
                                ?>
                                <div class="swiper-slide ljSelectImg">
                                    <div class="ljOnlyForSavedData" name="<?php echo $sv['name']; ?>" imgUrl="<?php echo $static_url."/media/catalog/product/cache/600x300".$sv['image']; ?>" price="<?php echo $sv['sales_price']; ?>" linkUrl="<?php echo $orderUrl; ?>" style="display: none;"></div>
                                    <!-- save start  -->
                                    <div class="ljGlassesSave ljSaveContent" skudata="<?php echo $sv['sku'] ?>" data="<?php echo $sv['simple_id'];?>" arr_simple=''>
                                        <div class="ljSave"> Save </div>
                                        <span class="plp-heart-empty"></span>
                                    </div>
                                    <!-- save start  -->

                                    <!-- image start  -->
                                    <?php if($sv['is_new']) { ?>
                                        <div class="ljNewDate">
                                            <img src="<?php echo $static_url; ?>/resource2/img/new.png" alt="<?php echo $sv['name']; ?>" />
                                        </div>
                                    <?php } ?>
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?php echo $data["base_url"].'/'.$orderUrl; ?>" class="ljImageAStyle"><img class="ljLazyImage" src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/image/lazy_white.png" data-src="<?php echo $static_url; ?>/media/catalog/product/cache/600x300<?php echo $sv['image']; ?>" alt="<?php echo $sv['name']; ?>" />
                                                <?php if(!$sv['is_in_stock']){ ?>
                                                    <span class="stock_text">OUT OF STOCK</span>
                                                <?php }else{ ?>
                                                    <span class="stock_text"></span>
                                                <?php } ?>
                                            </a>
                                        </li>
                                        <div class="ljGlassLi">
                                            <li> <div class="ljFontNameSize"> <?php echo $sv['name']; ?> </div> </li>
                                            <li>
                                                <div class="ljFontPriceSize">
                                                    <?php
                                                    $oldPrice = number_format($sv['price'], 2);
                                                    $salesPrice = number_format($sv['sales_price'], 2);
                                                    if($oldPrice != $salesPrice){
                                                        echo '<span class="ljPriceSpan">$'.$oldPrice.'</span>';
                                                    }
                                                    echo "$".$salesPrice;
                                                    ?>
                                                </div>
                                            </li>
                                        </div>
                                    </ul>
                                    <!-- image start  -->

                                    <!-- Order地址 start -->
                                    <a href="<?php echo $data["base_url"].'/'.$orderUrl; ?>">
                                        <div class="ljOrder"> Order </div>
                                    </a>
                                    <!-- Order地址 end -->
                                </div>
                            <?php } ?>
                        <?php endforeach?>
                    </div>

                    <div class="<?php echo 'ljSelectLi'.$kk; ?>"></div>

                    <div class="ljSelectLiTemp">
                        <?php foreach($list['_source']['simples'] as $sk2=>$sv2): ?>
                            <?php // var_dump($sv2['visible_plp_list']);  ?>
                            <?php
                            if($sv2['visible_plp_list']) {
                                if($sv2['color']['type']==1) {
                                    $ljBgUrl = $sv2['color']['color_value'];
                                }else{
                                    $ljBgUrl = "url('".$static_url."/media/attribute/swatch/swatch_image/30x20".$sv2['color']['color_value']."')";
                                }
                                ?>

                                <div class="ljSelectColorList">
                                    <span class="ljSelectColor" style="background:<?php echo $ljBgUrl; ?>;"></span>
                                </div>
                            <?php } ?>
                        <?php endforeach?>
                    </div>

                </div>

            </div>
        </div>

    <?php endforeach?>

</div>


<!--   page分页   -->
<div style="display: flex;justify-content: center;">
    <?php
    echo \yii\widgets\LinkPager::widget([
        'pagination' => $data["data"]["pagination"],
        'firstPageLabel' => false,
        'lastPageLabel' => false,
        'nextPageLabel' => ">>",
        'prevPageLabel' => "<<",
        'hideOnSinglePage'=>false,
        'maxButtonCount' => 5,
        //设置class样式
        'options' => ['class' => 'm-pagination'],
    ]);
    ?>
</div>
<!--   page分页   -->

<?php if($data['data']['hits']) {   ?>
    <div class="ljSearchForH1">
        <?php echo $data['data']['total'];  ?>
        <span id="ljPageTotalResults"> Results </span>
    </div>
<?php } ?>





