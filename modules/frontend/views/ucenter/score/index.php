<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
?>
<!DOCTYPE html>
<html lang="en-US" class>
<head>
    <!-- ======= Basic configuration ======= -->
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport"
          content="width=device-width,
                   initial-scale=1.0,
                   maximum-scale=1.0,
                   user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- ======= Basic configuration End ======= -->

    <!-- ======= Web Description Configuration ====== -->
    <meta name="Description" content=""/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <meta property="og:url" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:title" content=""/>
    <meta name="keywords" content="">
    <title>Score List | Payne Glasses</title>
    <!-- ======= Web Description Configuration End ====== -->

    <!-- ======= Css File Import ====== -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/img/favlcon.ico">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/icon-fonts/css/porto-icons.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/icon-fonts/css/porto-icons-codes.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/css/swiper.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/css/new_view.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/css/new_media.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/notes.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/css/reviews.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/awesomeFont.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/advertising.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/fonts/iconfont.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/css/integral.css">
    <!--新footer引入css-->
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/footer_new.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/max-width-700.css" media="screen and (max-width:990px)">

    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/footer_2020.css">
    <!--新footer引入css ENd-->
    <!-- ======= Css File Import End ====== -->
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/js/jquery-1.12.3.min.js"></script>
</head>
<body>
<!-- === load === -->
<div class="loading-mask hide" data-role="loader" style="display: block;" id="loading-loade">
    <img alt="load..." src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/images/loader-2.gif"><p>
</div>
<!-- === load End ===-->
<!-- ===== Comment filling area ====== -->

<!-- ===== Comment filling area End====== -->

<!-- ====== reviews Success Popup ====== -->
<div class="revSuccess"></div>
<!-- ====== reviews Success Popup End ====== -->


<div class="assigned"></div>


<!--Advertisement Start-->
<div class="message">
    <div id="message_show">
        <div class='message_container'>
            <div class="message_two_child owl-carousel owl-theme">

            </div>
        </div>
    </div>
</div>
<!--Advertisement End-->
<?php
//<!-- ======= Head Lntroduction ======= -->
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>


<a href="javascript:;" id="totop"><span class="iconfont totop_back"></span></a>
<a href="javascript:;" id="botop"><em class="porto-icon-up-open"></em></a>
<div class="assigned"></div>


<div id="maincontent_s">

    <div class="page-title-wrapper">
        <h1 class="page-title page_h1">
            <span class="base pg-base-title" data-ui-id="page-title-wrapper">Your Rewards Summary</span>
        </h1>
    </div>
    <div class="columns">

        <!--右侧积分列表-->
        <div class="table-wrapper orders-recent">
            <table class="data table table-order-items recent" id="my-orders-table">

                <?php if($customer['enabled_score']==1){?>
                    <div class="Ava">
                        <h1>Available now: <?php if($total_score){echo $total_score;}else{echo 0;};?></h1>
                        <p>Total points: <?php if($all_score){echo $all_score;}else{echo 0;};?></p>
                    </div>
                    <?php if(!$rows){?>
                        <p class="Shop_p" style="margin-top: 10px">
                            <a href="/">Shop now to earn points</a></p>
                    <?php } ?>
                <?php }else{?>
                    <div class="Ava">
                        <h1>Available now: 0</h1>
                        <p>Total points: 0</p>
                    </div>
                    <p class="Shop_p" style="margin-top: 10px">There is no activity available at the moment, it takes up to 30 days for your points to appear.</p>
                <?php }?>


                <?php if($customer['enabled_score'] == 1 && $rows):?>
                </div>
                    <p class="Rewards">Rewards Activities </p>
                    <!--            <caption class="table-caption">Score ist</caption>-->
                    <thead>
                    <tr>
                        <th scope="col" class="col id">DATE</th>
                        <th scope="col" class="col date">ACTIVITY</th>
                        <th scope="col" class="col shipping">POINTS</th>
                        <th scope="col" class="col status">STATUS</th>
                        <th scope="col" class="col actions">REFERENCES</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rows as $key=>$row): ?>
                        <tr>
                            <td data-th="Order #" class="col id"><?php echo date('m/d/y',strtotime(date($row["created_at"]))); ?></td>
                            <td data-th="Ship To" class="col shipping">
                                <?php if($row['operation'] == "0"){?>
                                    Earn
                                <?php }else if($row['operation']=="1"){ ?>
                                    <?php if ($row["type_id"]=="sales_point_for"): ?>
                                        Redeem
                                    <?php else: ?>
                                        Deduct
                                    <?php endif; ?>
                                <?php } ?>
                            </td>
                            <td data-th="Ship To" class="col shipping">
                                <?php echo $row["score"]; ?>
                            </td>
                            <td data-th="Status" class="col status">
                                <?php if($row["score_status"]==0): ?>
                                    pending
                                <?php elseif($row["score_status"]==1): ?>
                                    <?php if($row["score"]<0){ ?>
                                        complete
                                    <?php }else{ ?>
                                        active
                                    <?php } ?>
                                <?php endif; ?>
                            </td>
                            <td data-th="Date" class="col date">
                                <?php
                                switch ($row["src_type"]){
                                    case "customer_entity":echo $row["note"];
                                        break;
                                    case "review":echo 'review:'.$row["src_configurable_name"];
                                        break;
                                    case "activity":echo 'activity:'.$row["note"];
                                        break;
                                    default:
                                        echo 'Order:' . $row["src_obj"]["src_obj"]["increment_id"];
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>

        <?php endif;?>



            </table>


            <?php if($customer['enabled_score'] == 1 && $rows):?>
            <div class="jf_lb">
                <ul>
                    <?php foreach ($rows as $key=>$row): ?>
                        <li>
                            <p>
                            <span>
                                <?php if($row['operation'] == "0"){?>
                                    Earn
                                <?php }else if($row['operation']=="1"){ ?>
                                    <?php if ($row["type_id"]=="sales_point_for"): ?>
                                        Redeem
                                    <?php else: ?>
                                        Deduct
                                    <?php endif; ?>
                                <?php } ?>
                            </span>
                                <span>
                                <?php if($row["score_status"]==0): ?>
                                    pending
                                <?php elseif($row["score_status"]==1): ?>
                                    <?php if($row["score"]<0){ ?>
                                        complete
                                    <?php }else{ ?>
                                        active
                                    <?php } ?>
                                <?php endif; ?>
                            </span>
                            </p>
                            <p>
                                <?php echo date('m/d/y',strtotime(date($row["created_at"]))); ?>
                            </p>
                            <div>
                                <?php echo $row["score"]; ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>



            </div>
            <?php endif; ?>



            <?php if($customer['enabled_score'] == 1 && $rows):?>
                    <p class="January">From January 1, 2020, all orders will be converted into points</p>
                <div class="Fenye">
                    <nav aria-label="Page navigation">
                        <?php echo \yii\widgets\LinkPager::widget([
                            'pagination' => $pager,
                        ]) ?>
                    </nav>
                </div>
            <?php endif; ?>
        </div>
        <!-- 左侧CURRENT PROFILE:元素-->

        <?php echo $this->render("@app/modules/frontend/views/ucenter/include/left_menu.php");?>
    </div>
</div>

<!-- ======= Footer Lntroduction ======= -->
<!--footer放置处-->
<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("new_footer")->toHtml();
?>
<!--footer放置处 End-->
<!-- ======= Footer Lntroduction End ======= -->
</body>
<script>
    window.console = window.console || function(t) {};
    if (document.location.search.match(/type=embed/gi)) {
        window.parent.postMessage("resize", "*");
    }
</script>
<!-- ======= JS ile Import ====== -->
<script src="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/js/notes.js"></script>
<script src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/owl.carousel.min.js"></script>
<!--新footer引入js-->
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/js/footer.js"></script>
<!--新footer引入js End -->
<!-- ======= JS ile Import End ====== -->
</html>
