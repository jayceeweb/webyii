<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
?>
<!DOCTYPE html>
<html lang="en-US" class>
<head>
    <!-- ======= Basic configuration ======= -->
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport"
          content="width=device-width,
                   initial-scale=1.0,
                   maximum-scale=1.0,
                   user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- ======= Basic configuration End ======= -->

    <!-- ======= Web Description Configuration ====== -->
    <meta name="Description" content=""/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <meta property="og:url" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:title" content=""/>
    <meta name="keywords" content="">
    <title>Score List | Payne Glasses</title>
    <!-- ======= Web Description Configuration End ====== -->

    <!-- ======= Css File Import ====== -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/assets2/resource/img/favlcon.ico?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/icon-fonts/css/porto-icons.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/icon-fonts/css/porto-icons-codes.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/bootstrap.min.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/swiper.min.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/new_view.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/new_media.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/notes.css?v=<?php echo $js_version; ?>"> <!--Advertisement-->
    <link rel="stylesheet" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/reviews.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/awesomeFont.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/advertising.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/fonts/iconfont.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/integral.css?v=<?php echo $js_version; ?>">
    <!-- ======= Css File Import End ====== -->
    <script src="<?php echo $static_url; ?>/assets2/resource/js/jquery-1.12.3.min.js?v=<?php echo $js_version; ?>"></script>
</head>
<body>
<!-- === load === -->
<div class="loading-mask hide" data-role="loader" style="display: block;" id="loading-loade">
    <img alt="load..." src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/images/loader-2.gif"><p>
</div>
<!-- === load End ===-->
<!-- ===== Comment filling area ====== -->

<!-- ===== Comment filling area End====== -->

<!-- ====== reviews Success Popup ====== -->
<div class="revSuccess"></div>
<!-- ====== reviews Success Popup End ====== -->

<!--Advertisement Start-->
<div class="message">
    <div id="message_show">
        <div class='message_container'>
            <div class="message_two_child owl-carousel owl-theme">

            </div>
        </div>
    </div>
</div>
<!--Advertisement End-->
<?php
//<!-- ======= Head Lntroduction ======= -->
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>


<a href="javascript:;" id="totop"><span class="iconfont totop_back"></span></a>
<a href="javascript:;" id="botop"><em class="porto-icon-up-open"></em></a>
<div class="assigned hide"></div>


<div id="maincontent_s">
this is no this tracking code
</div>

<!-- ======= Footer Lntroduction ======= -->
<?php
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Footer')
    ->setTemplate('Pg_Learning::page/footer.phtml')
    ->toHtml();
?>
<!-- ======= Footer Lntroduction End ======= -->
</body>

</html>
