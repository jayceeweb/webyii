<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

$invitation =Yii::$app->getRequest()->get('invitation','');
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
$this->beginPage()
?>
<!DOCTYPE html>
<html lang="en-US" class>
<head>
    <!-- ======= Basic configuration ======= -->
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport"
          content="width=device-width,
                   initial-scale=1.0,
                   maximum-scale=1.0,
                   user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- ======= Basic configuration End ======= -->

    <!-- ======= Web Description Configuration ====== -->
    <meta name="Description" content=""/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <meta property="og:url" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:title" content=""/>
    <meta name="keywords" content="">
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="<?php echo Yii::$app->request->csrfToken; ?>">
    <title>Score List | Payne Glasses</title>
    <!-- ======= Web Description Configuration End ====== -->

    <!-- ======= Css File Import ====== -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/assets2/resource/img/favlcon.ico?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/icon-fonts/css/porto-icons.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/icon-fonts/css/porto-icons-codes.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/bootstrap.min.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/swiper.min.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/new_view.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/new_media.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/notes.css?v=<?php echo $js_version; ?>"> <!--Advertisement-->
    <link rel="stylesheet" href="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/reviews.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/awesomeFont.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/advertising.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/fonts/iconfont.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/css/integral.css?v=<?php echo $js_version; ?>">
    <!--新footer引入css-->
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/footer_new.css?v=<?php echo $js_version; ?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/max-width-700.css?v=<?php echo $js_version; ?>" media="screen and (max-width:990px)">

    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/css/footer_2020.css?v=<?php echo $js_version; ?>">
    <!--新footer引入css ENd-->
    <!-- ======= Css File Import End ====== -->
    <script src="<?php echo $static_url; ?>/assets2/resource/js/jquery-1.12.3.min.js?v=<?php echo $js_version; ?>"></script>
</head>
<body>
<!-- === load === -->
<div class="loading-mask hide" data-role="loader" style="display: block;" id="loading-loade">
    <img alt="load..." src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/images/loader-2.gif"><p>
</div>
<!-- === load End ===-->
<!-- ===== Comment filling area ====== -->

<!-- ===== Comment filling area End====== -->

<!-- ====== reviews Success Popup ====== -->
<div class="revSuccess"></div>
<!-- ====== reviews Success Popup End ====== -->

<!--Advertisement Start-->
<div class="message">
    <div id="message_show">
        <div class='message_container'>
            <div class="message_two_child owl-carousel owl-theme">

            </div>
        </div>
    </div>
</div>
<!--Advertisement End-->
<?php
//<!-- ======= Head Lntroduction ======= -->
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>


<div class="massges hide">

</div>


<a href="javascript:;" id="totop"><span class="iconfont totop_back"></span></a>
<a href="javascript:;" id="botop"><em class="porto-icon-up-open"></em></a>
<div class="assigned"></div>


<div id="maincontent_s">

    <div class="page-title-wrapper">
        <h1 class="page-title page_h1">
            <span class="base pg-base-title" data-ui-id="page-title-wrapper">Payne Rewards</span>
        </h1>
    </div>
    <div class="columns">
        <div class="table-wrapper orders-recent">
            <!--弹出框-->
            <div class="tck_payne hide">
                <div class="tck_payne_one">
                    <h1>CONFIRM YOUR EMAIL ADDRESS</h1>
                    <p id="rewards">Your rewards membership will not be activated until your email address is confirmed! </p>
                    <p>Please confirm your email address below.  We use this email address as sole communication channel for all of your reward activities, invitation to earn points on writing product reviews, etc.; we take privacy seriously, please review our
                        <a href="<?php echo $static_url; ?>/privacy-policy" id="privacy_lj" target="_blank">privacy policy</a>
                        here for details.</p>
                    
                    <div>
                        <input type="text" id="username" placeholder="email address" disabled="true" value="<?php echo $customer['email'];?>">
                        <span id="err_cw"></span>
                    </div>

                    <div>
                        <button id="Confirm">Confirm</button>
                        <button id="Cancel">Cancel</button>
                    </div>
                    <span class="tck_payne_ch">X</span>
                </div>
            </div>
            <?php if(!$rows['confirm_email']): ?>
                <p class="bt_siez More_glasses" id="intod">More glasses, more rewards, more savings.</p>
                <p style="color: #e03d22;font-size: 16px;font-weight: bold;line-height: 38px; ">New Members Get 2,000 Points - For A Limited Time.</p>
            <?php else: ?>
                <?php if(!$invitation){ ?>
                <?php if($rows['confirm_email_confirm'] !=1 && $rows['enabled_score'] != 1){?>
                <div class="We_have">
                    <p>We have sent a verification email to
                        <span class="user_ipt"><?php echo $rows['confirm_email'];?></span>
                        please click on the link in the email to activate your
                        membership. Please make sure to check your spam/junk folder
                        if you don’t see the verification email.  </p>
                    <p><a href="#" id="jx_ts">Click here</a>
                        to resend another verification email.
                    <p id="ts_cg">your email have been sent successful !!!</p>
                    </p>
                    <a href="#" class="We_have_close">Close</a>
                    <span class="We_have_ch">X</span>
                </div>
                <?php }} ?>
                <p class="bt_siez More_glasses" id="intod">More glasses, more rewards, more savings.</p>
            <?php endif; ?>
            <!--    add sunjinchao 20/10/28   Introducing-->
            <?php
                echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("Payne_Rewards_Introducing")->toHtml();
            ?>

            <?php if($rows['confirm_email_confirm'] == 0): ?>
                <button class="btn_join">Join Now</button>
            <?php endif; ?>

            <!--    add sunjinchao 20/10/28   How_to_use_points-->
            <?php
                echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("Payne_Rewards_How_to_use_points")->toHtml();
            ?>

            <?php if($rows['confirm_email_confirm'] == 0): ?>
                <button class="btn_join">Join Now</button>
            <?php endif; ?>


            <?php
            echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("Payne_Rewards_Join_us")->toHtml();
            ?>

            <?php if($rows['confirm_email_confirm'] == 0): ?>
                <button class="btn_join">Join Now</button>
            <?php endif; ?>
        </div>
        <?php echo $this->render("@app/modules/frontend/views/ucenter/include/left_menu.php");?>
    </div>
</div>

<!-- ======= Footer Lntroduction ======= -->
<!--footer放置处-->
<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("new_footer")->toHtml();
?>
<!--footer放置处 End-->
<!-- ======= Footer Lntroduction End ======= -->
</body>
<script>
    window.console = window.console || function(t) {};
    if (document.location.search.match(/type=embed/gi)) {
        window.parent.postMessage("resize", "*");
    }
</script>
<script>
    var current_user = "<?php echo $customer['email'];?>";
    //获取invitation
    var invitation = "<?php echo $invitation;?>";
</script>
<!-- ======= JS ile Import ====== -->
<script src="<?php echo $static_url; ?>/assets2/resource/js/integral.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/resource2/js/notes.js?v=<?php echo $js_version; ?>"></script>
<script src="<?php echo $static_url; ?>/static/<?php echo $magento_version; ?>/frontend/Pg/payne/en_US/owl.carousel/owl.carousel.min.js"></script>
<!--新footer引入js-->
<script src="<?php echo $static_url; ?>/assets2/resource/js/footer.js?v=<?php echo $js_version; ?>"></script>
<!--新footer引入js End -->
<!-- ======= JS ile Import End ====== -->
</html>
<?php $this->endPage() ?>
