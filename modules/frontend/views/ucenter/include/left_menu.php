<?php
$profiles=Yii::$container->get("app\models\Profile");
$s=Yii::$container->get("app\components\helper\CustomerHelper");
$customer_id=$s->getSessionCustomerId();
$loginSession=$s->getLoginSession();
if (isset($loginSession["customer_base"]["profile_id"])) {
    $profile=$profiles::findOne(["profile_id",$loginSession["customer_base"]["profile_id"]])->getAttributes();
} else {
    $sql=sprintf("select default_profile from customer_entity where entity_id=%s",$customer_id);
    $default_profile=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
    $profile=$profiles::findOne(["profile_id",$default_profile])->getAttributes();
}
$url = Yii::$app->request->getUrl();
$url = str_replace('/frontend/','/',$url);
$customer=$s->loadCustomer($customer_id);
$enabled_score = $customer['enabled_score'];
$confirm_email_confirm = $customer['confirm_email_confirm'];
$entity_id = $customer['entity_id'];
?>
<div class="sidebar sidebar-main">
    <div class="block account-nav account-First">
        <div class="title">
            <strong>CURRENT PROFILE: </strong>
            <strong class="pg-shown pg-text-overflow"><?php echo $profile["nickname"] ?></strong>
        </div>
        <div class="content">
            <nav class="account-nav">
                <ul class="nav items">
                    <li class="nav item">
                        <a href="/profile/manage/edit/">Profile Information</a>
                    </li>
                    <li class="nav item">
                        <a href="/prescription/profile/">Prescriptions</a>
                    </li>
                    <li class="nav item">
                        <a href="/wishlist/">Saved Frames</a>
                    </li>
                    <li class="nav item">
                        <a href="/profile/manage/switchProfile/">Switch Profile</a>
                    </li>
                    <li class="nav item">
                        <a href="/profile/manage/create/">New Person</a></li>
                    <li class="nav item">
                        <a href="/profile/manage/manageProfile/">Manage Profiles</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="block account-nav">
        <div class="title">
            <strong>YOUR ACCOUNT</strong>
        </div>
        <div class="content">
            <nav class="account-nav">
                <ul class="nav items">
                    <li class="nav item">
                        <a <?php if($url == "/customer/account/"){ echo 'id="select_A"';}else{ echo 'href="/customer/account/"';} ?>>Your Dashboard </a>
                    </li>
                    <li class="nav item">
                        <a <?php if($url == "/sales/order/history/"){ echo 'id="select_A"';}else{ echo 'href="/sales/order/history/"';} ?>>Your Orders</a>
                    </li>
                    <li class="nav item">
                        <a <?php if($url == "/customer/group/orders/"){ echo 'id="select_A"';}else{ echo 'href="/customer/group/orders/"';} ?>>Order Group</a>
                    </li>
                    <li class="nav item">
                        <a <?php if($url == "/customer/account/edit/"){ echo 'id="select_A"';}else{ echo 'href="/customer/account/edit/"';} ?>>Account Information</a>
                    </li>
                    <li class="nav item">
                        <a <?php if($url == "/customer/address/"){ echo 'id="select_A"';}else{ echo 'href="/customer/address/"';} ?>>Address Book </a>
                    </li>
                    <li class="nav item">
                        <a <?php if($url == "/review/customer/"){ echo 'id="select_A"';}else{ echo 'href="/review/customer/"';} ?>>Your Reviews</a>
                    </li>
                    <li class="nav item">
                        <a <?php if($url == "/newsletter/manage/"){ echo 'id="select_A"';}else{ echo 'href="/newsletter/manage/"';} ?>>Email Preference </a>
                    </li>
                    <li class="nav item">
                        <a <?php if($url == "/vault/cards/listaction/"){ echo 'id="select_A"';}else{ echo 'href="/vault/cards/listaction/"';} ?>>Stored Payment Methods </a>
                    </li>
                        <li class="nav item">
                            <a <?php if($url == "/ucenter/payne-rewards/index.html"){ echo 'id="select_A"';}else{ echo 'href="/ucenter/payne-rewards/index.html"';} ?>>Payne Rewards</a>
                        </li>
                    <?php if($confirm_email_confirm || $enabled_score){ ?>
                        <li class="nav item">
                            <a <?php if($url == "/ucenter/score/index.html"){ echo 'id="select_A"';}else{ echo 'href="/ucenter/score/index.html"';} ?>>Your Rewards Summary</a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
