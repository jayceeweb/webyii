<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <script>
        var static_url = "<?php echo $static_url;?>";
    </script>
    <title>Forgot Your Password | Payne Glasses</title>
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-transfrom">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <meta name="applicable-device" content="pc,mobel">
    <meta name="description" content="luanjue TODO"/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <!--Introduction of Css Start-->
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/jquery-1.12.3.min.js"></script>
    <!--    <script src="--><?php //echo $static_url; ?><!--/static/version--><?php //echo $js_version; ?><!--/frontend/Pg/payne/en_US/jquery/jquery.cookie.js"></script>-->
    <script>
        var dataLayer = dataLayer?dataLayer:[];
        window.dataLayer = window.dataLayer || [];
    </script>

    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/media/favicon/stores/1/favIcon.ico">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/footer_2020.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/fonts/iconfont.css">

    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/awesomeFont.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/icon-fonts/css/porto-icons.css">
    <!--Introduction of Css End-->

    <!--    新引入样式   start   -->
    <link rel="preload" as="style" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/new_login.css" />
    <link rel="stylesheet" load_last="true" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/new_login.css" />
    <!--    新引入样式   end   -->
</head>


<?php
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>
<!-- Header End -->


<div class="page-main">
    <div class="page-title-wrapper">
        <div class="text-center page-title">Forgot Your Password</div>
    </div>
    <p class="createDesc forgotPassword">
        <span class="descClose">×</span>
        <span id="forgotPasswordDesc"></span>
    </p>

    <div class="login-container col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 36px;">
        <div class="row" style="padding-bottom: 36px;">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p class="createDesc">
                    Please enter your email address below to receive a password reset link.
                </p>

                <form name="formLogin" action="/customer/account/forgotpasswordpost/" method="post" id="forgot-form">
                    <div class="field">
                        <label class="label" for="email"><span>Email</span></label>
                        <div class="control">
                            <input type="email" name="email" id="email_address" class="input-text" title="Email" placeholder="Email" />
                            <p class="errorMessage hiddenLJ">
                                Please enter a valid email address (Ex: johndoe@domain.com).
                            </p>
                        </div>
                    </div>

                    <div class="creatButton" style="float:left;">
                        <a href="javascript:void(0);">Reset My Password</a>
                    </div>
                </form>

            </div>

        </div>
    </div>
    <div style="clear: both;"></div>
</div>

<!--    加载中....     -->
<div class="loading-mask" data-role="loader" id="loading">
    <img alt="Loading..." src="<?php echo $static_url; ?>/static/version<?php echo $js_version; ?>/frontend/Pg/payne/en_US/images/loader-2.gif">
</div>

<script>
    $("body").on("click",".creatButton",function(e) {
        var email_address = $("#email_address").val();
        var reg = /^[0-9a-zA-Z_.-]+[@][0-9a-zA-Z_.-]+([.][a-zA-Z]+){1,2}$/;
        if((email_address.length <= 0) || (!reg.test(email_address))) {
            if($("#email_address").next().hasClass("hiddenLJ")) {
                $("#email_address").next().removeClass("hiddenLJ");
            }
            $("#email_address").addClass("mage-error");
            $("#email_address").focus();
        }else{
            $("#email_address").next().addClass("hiddenLJ");
            $("#email_address").removeClass("mage-error");

            $.ajax({
                url: "/customer/account/forgotpasswordpost/",
                type: 'post',
                data: { "email": email_address },
                beforeSend:function(){
                    // alert('远程调用开始...');
                    $("#loading").css("display","block");
                },
                success:function(res,textStatus){
                    // alert('开始回调，状态文本值：'+textStatus+' 返回数据：'+res);
                    var resObj = JSON.parse(res);
                    $(".forgotPassword").css("display","block");
                    $("#forgotPasswordDesc").text(resObj.msg+"About to jump to the login page.");
                    setTimeout(function () {
                        window.location.href = "/frontend/customer/login.html";
                    },5000);
                },
                complete:function(XMLHttpRequest,textStatus){
                    // alert('远程调用成功，状态文本值：'+textStatus);
                    $("#loading").css("display","none");
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){
                    console.log('error...状态文本值：'+textStatus+" 异常信息："+errorThrown);
                }
            });
        }
    });
    $("#email_address").on("blur",function() {
        var email_address = $("#email_address").val();
        var reg = /^[0-9a-zA-Z_.-]+[@][0-9a-zA-Z_.-]+([.][a-zA-Z]+){1,2}$/;
        if((email_address.length <= 0) || (!reg.test(email_address))) {
            if($("#email_address").next().hasClass("hiddenLJ")) {
                $("#email_address").next().removeClass("hiddenLJ");
            }
            $("#email_address").addClass("mage-error");
            $("#email_address").focus();
        }else{
            $("#email_address").next().addClass("hiddenLJ");
            $("#email_address").removeClass("mage-error");
        }
    });
</script>

















<!--footer HTML Start-->
<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("new_footer")->toHtml(); ?>
<!--footer HTML End-->

<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/bootstrap.min.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/js/footer.js"></script>

</body>
</html>
