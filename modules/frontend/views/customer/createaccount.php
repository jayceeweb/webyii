<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <script>
        var static_url = "<?php echo $static_url;?>";
    </script>
    <title>Create New Customer Account | Payne Glasses</title>
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Cache-Control" content="no-transfrom">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <meta name="applicable-device" content="pc,mobel">
    <meta name="description" content="luanjue TODO"/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <!--Introduction of Css Start-->
    <script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/jquery-1.12.3.min.js"></script>
    <!--    <script src="--><?php //echo $static_url; ?><!--/static/version--><?php //echo $js_version; ?><!--/frontend/Pg/payne/en_US/jquery/jquery.cookie.js"></script>-->
    <script>
        var dataLayer = dataLayer?dataLayer:[];
        window.dataLayer = window.dataLayer || [];
    </script>

    <link rel="shortcut icon" type="image/ico" href="<?php echo $static_url; ?>/media/favicon/stores/1/favIcon.ico">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/css/footer_2020.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/fonts/iconfont.css">

    <link rel="stylesheet" href="<?php echo $static_url; ?>/resource2/version<?php echo $js_version; ?>/css/awesomeFont.css">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/icon-fonts/css/porto-icons.css">
    <!--Introduction of Css End-->

    <!--    新引入样式   start   -->
    <link rel="preload" as="style" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/new_login.css" />
    <link rel="stylesheet" load_last="true" href="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/css/new_login.css" />
    <!--    新引入样式   end   -->

    <!--    第三方登录  start    -->
    <!--    亚马逊登录     -->
    <script type='text/javascript'>
        window.onAmazonLoginReady = function () {
            amazon.Login.setClientId('<?php echo $amazonClientId; ?>');
        };
    </script>
    <script type='text/javascript' src='<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/OffAmazonPayments/us/js/Widgets.js'></script>

    <!--    facebook\twitter    -->
    <script>
        var width = 650;
        var height = 350;
        var left = 300;
        var top = 300;

        var params = [
            'resizable=yes',
            'scrollbars=no',
            'toolbar=no',
            'menubar=no',
            'location=no',
            'directories=no',
            'status=yes',
            'width='+ width,
            'height='+ height,
            'left='+ left,
            'top='+ top
        ];

        function login(obj) {
            var win = null;

            var params = [
                'resizable=yes',
                'scrollbars=no',
                'toolbar=no',
                'menubar=no',
                'location=no',
                'directories=no',
                'status=yes'
            ];
            if(win) {
                alert("12344");
                win.close();
            }

            var href = $(obj).find("a").attr("data-href");
            href = href+"refresh/"+ (new Date()).getTime()+'/state/{"static_param":"1"}';
            win = window.open(href, 'pslogin_popup', params.join(','));
            win.focus();
        }

    </script>

    <!--    第三方登录  end    -->
</head>


<?php
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();
?>
<!-- Header End -->


<div class="page-main">
    <div class="page-title-wrapper">
        <div class="text-center page-title">Create New Customer Account</div>
    </div>

    <!--    第三方注册   -->
    <form style="display:none;" class="form" action="https://www.payneglasses.com/pslogin/account/loginPost/" method="post" id="pslogin-login-form">
        <input id="pslogin-login-referer" name="referer" type="hidden" value="<?php if(isset($_GET['referer'])){ echo $_GET['referer']; }else{ echo $referer; }   ?>">
        <input id="pslogin-login-submit" type="submit" value="">
    </form>

    <div class="thirdLogin">
        <ul class="pslogin-clearfix">
            <li class="pslogin-button amazon" title="">
                <a href="javascript:void(0);">
                    <span class="pslogin-button-auto" id="AmazonLoginButton">
                        <span class="pslogin-button-icon"></span>
                        <span class="pslogin-button-text">Register with Amazon</span>
                    </span>
                </a>
            </li>
            <li class="pslogin-button facebook" title="Login with Facebook" onclick="login(this);">
                <a class="pslogin-button-link pslogin-button-click" rel="nofollow" href="javascript:void(0);" data-href="/pslogin/account/douse/type/facebook/">
                    <span class="pslogin-button-auto  ">
                        <span class="pslogin-button-icon"></span>
                        <span class="pslogin-button-text">Register with Facebook</span>
                    </span>
                </a>
            </li>
            <li class="pslogin-button twitter" title="Login with Twitter" onclick="login(this);">
                <a class="pslogin-button-link pslogin-button-click" rel="nofollow" href="javascript:void(0);" data-href="/pslogin/account/douse/type/twitter/" data-width="630" data-height="650">
                    <span class="pslogin-button-auto  ">
                        <span class="pslogin-button-icon"></span>
                        <span class="pslogin-button-text">Register with Twitter</span>
                    </span>
                </a>
            </li>
        </ul>

        <div style="clear: both;"></div>
    </div>

    <!--    OR   start   -->
    <div style="clear: both;"></div>
    <div>
        <div class="orLeft"></div>
        <div class="orCenter">OR</div>
        <div class="orRight"></div>
    </div>
    <!--    OR   end   -->

    <div class="login-container col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 36px;">
        <div class="row" style="padding-bottom: 36px;">

            <form name="formCreate" action="/customer/account/createpost/" method="post" id="create-form">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="createTitle registerTitle">
                            PERSONAL INFORMATION
                        </div>

                        <input type="hidden" name="referer" value="<?php if(isset($_GET['referer'])){ echo $_GET['referer']; }else{ echo $referer; }   ?>" />
                        <div class="field">
                            <label class="label" for="firstname"><span>First Name</span></label>
                            <div class="control controlFull">
                                <input name="firstname" autocomplete="off"  id="firstname" type="text" maxlength="35" class="input-text" title="First Name" placeholder="First Name" />
                                <p class="errorMessage hiddenLJ">
                                    This is a required field.
                                </p>
                            </div>
                        </div>
                        <div class="field">
                            <label for="lastname" class="label"><span>Last Name</span></label>
                            <div class="control controlFull">
                                <input name="lastname" autocomplete="off" id="lastname" type="text" maxlength="35" class="input-text" title="Last Name" placeholder="Last Name" />
                                <p class="errorMessage hiddenLJ">
                                    This is a required field.
                                </p>
                            </div>
                        </div>
                        <div class="field">
                            <label for="email_address" class="label"><span>Email</span></label>
                            <div class="control controlFull">
                                <input name="email" autocomplete="off" id="email_address" type="email" class="input-text" title="Email" placeholder="Email" />
                                <p class="errorMessage hiddenLJ">
                                    Please enter a valid email address.
                                </p>
                            </div>
                        </div>
                        <div class="field choice newsletter">
                            <input type="checkbox" checked="checked" name="is_subscribed" title="Sign Up for Newsletter" value="1" id="is_subscribed" class="checkbox">
                            <span>Sign Up for Newsletter</span>
                        </div>

                        <div class="creatButton" style="float:left;">
                            <a href="#">Create an Account</a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="createTitle registerTitle">
                            Sign-in Information
                        </div>

                        <div class="field">
                            <label class="label" for="password"><span>Password</span></label>
                            <div class="control controlFull">
                                <input name="password" autocomplete="off"  id="password" type="password" class="input-text " title="Password" placeholder="Password" />
                                <p class="errorMessage hiddenLJ">
                                    This is a required field.
                                </p>
                            </div>
                        </div>
                        <div class="field">
                            <p class="tipText tipLength">
                                <span class="glyphicon glyphicon-ok"></span>
                                6-20 characters
                            </p>
                            <p class="tipText tipRule">
                                <span class="glyphicon glyphicon-ok"></span>
                                At least 1 letter and 1 number
                            </p>
                        </div>
                        <div class="field">
                            <label for="password-confirmation" class="label"><span>Confirm Password</span></label>
                            <div class="control controlFull">
                                <input name="password_confirmation" id="password-confirmation" type="password" maxlength="35" class="input-text" title="Confirm Password" placeholder="Confirm Password" />
                                <p class="errorMessage hiddenLJ">
                                    Please enter the same value again.
                                </p>
                            </div>
                        </div>
                        <div id="remember-me-box" class="field choice persistent">
                            <input type="checkbox" name="persistent_remember_me" class="checkbox" id="remember_me" checked="checked" title="Remember Me" />
                            <span>Remember Me</span>
                        </div>
                    </div>

            </form>
        </div>
    </div>
    <div style="clear: both;"></div>

</div>


<input type="hidden" id="twitterAppId" value="<?php echo $twitterAppId; ?>" />
<!--    加载中....     -->
<div class="loading-mask" data-role="loader" id="loading">
    <img alt="Loading..." src="<?php echo $static_url; ?>/static/version<?php echo $js_version; ?>/frontend/Pg/payne/en_US/images/loader-2.gif">
</div>

<script type='text/javascript' src='<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/new_list/js/new_register.js'></script>
<script>
    // js登录判断逻辑及第三方登录集成都在文件new_register.js中
</script>






<!--footer HTML Start-->
<?php
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("new_footer")->toHtml(); ?>
<!--footer HTML End-->

<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/pdp-summary/js/bootstrap.min.js"></script>
<script src="<?php echo $static_url; ?>/assets2/resource/version<?php echo $js_version; ?>/js/footer.js"></script>

</body>
</html>
