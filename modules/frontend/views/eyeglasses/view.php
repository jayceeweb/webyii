<?php
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;
use Pg\Crypt\Authcode;
use app\models\UrlRewrite;
use app\models\CatalogProductFlat1;
use app\models\CatalogProductSuperLink;

require '/data/magento450/app/bootstrap.php';
$params = $_SERVER;
$params[Bootstrap::INIT_PARAM_FILESYSTEM_DIR_PATHS] = [
    DirectoryList::PUB => [DirectoryList::URL_PATH => ''],
    DirectoryList::MEDIA => [DirectoryList::URL_PATH => 'media'],
    DirectoryList::STATIC_VIEW => [DirectoryList::URL_PATH => 'static'],
    DirectoryList::UPLOAD => [DirectoryList::URL_PATH => 'media/upload'],
];
$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $params);
$app = $bootstrap->createApplication('Magento\Framework\App\Http');
$bootstrap->runAsYii($app);

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
if (empty($objectManager->get("\Magento\Framework\App\State")->getAreaCode())) {
    $objectManager->get("\Magento\Framework\App\State")->setAreaCode('frontend');
}

$resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');

echo $resultLayoutFactory->create()->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId("pdp_footer_js")->toHtml();
?>
<!DOCTYPE html>
<html lang="en-US" class>
<head>
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport"
          content="width=device-width,
                   initial-scale=1.0,
                   maximum-scale=1.0,
                   user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <link rel="stylesheet" href="/assets/resource/icon-fonts/css/porto-icons.css">
    <link rel="stylesheet" href="/assets/resource/icon-fonts/css/porto-icons-codes.css">
    <link rel="stylesheet" href="/assets/resource/css/index_new.css">
    <link rel="stylesheet" href="/assets/resource/css/index_mobile.css">
    <link rel="stylesheet" href="/assets/resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/resource/css/index.css">
    <link rel="stylesheet" href="/assets/resource/css/swiper.min.css">



    <script src="/assets/resource/js/jquery-1.12.3.min.js"></script>
    <script src="/assets/resource/js/swiper.min.js"></script>

    <style>
        *{
            margin: 0;
            padding: 0;
            list-style: none;
            box-sizing: border-box;
        }
        .swiper-container{
            width: 100%;
        }
        .swiper-wrapper .swiper-slide{
            height: 40% !important;
        }
        .swiper-wrapper img{
            width: 100%;
            height: 40%;
        }
        .swiper-container  .swiper-slide{
            padding: 70px;
        }
        .box2{
            height: 500px;
            padding-right:100px;
        }
        .swiper-pagination-bullet {
            width: 140px;
            height: 70px;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 0% !important;
             background: #fff !important;
            opacity: 0.7;
            /* border-radius: 50%; */
            /* box-shadow: 0 0 2px #000; */
            margin: 0 5px;
            outline: 0;
        }
        /*自定义分页器激活时的样式表现*/
        .swiper-pagination-bullet-active {
            opacity: 1;
            border-color: #f90;
        }
        .swiper-button-prev,.swiper-button-next{
            color:#f90;
        }
        .swiper-button-prev:after, .swiper-container-rtl .swiper-button-next:after,
        .swiper-button-next:after, .swiper-container-rtl .swiper-button-prev:after{
            font-size: 30px
        }
        .swiper-pagination-bullet img{
            width: 100%;
        }
        .FrameColor{
            margin-bottom: 125px;
        }
        .FrameColor ul{
            width: 120px;
            float: left;
            margin-right: 10px;
        }
        .FrameColor div{
            width:100%;
            border:1px solid #ccc;
            float: left;
            box-sizing: border-box;
            cursor: pointer;
        }
        .FrameColor div:nth-of-type(1){
            border-color: #f90;
        }
        .FrameColor ul p{
            text-align: center;
        }
        .FrameColor div img{
            width:100%;
        }
        .FrameColor span{
            float: left;
            margin-right: 10px;
            font-weight: 600;
            font-size: 15px;
            padding-top: 18px;
        }
        .stock_s{
            width: 100%;
            margin-bottom: 40px;
        }
        .stock_s li{
            margin-bottom: 10px;
        }
        .stock_price{
            margin-right:10px;
        }
        .stock_price span:nth-of-type(1){
            color: #f90;
            font-size: 30px;
        }
        .stock_price s{
            color: #ccc;
            font-size: 20px;
        }
        .sm{
            margin-top: 10px;

        }
        .save_addTo{
            width: 100%;
            height: 40px;
            display: flex;
            justify-content: space-between;
            margin-top: 30px;
        }
        .save_addTo div{
            width: 45%;
            font-size:15px;
            text-align: center;
            line-height: 36px;
            border:1px solid #ffb454;
            border-radius: 4px;
            font-weight: normal;
            cursor: pointer;
        }
        .save_addTo div:nth-of-type(1){
            color: #ffb454;
        }
        .save_addTo div:nth-of-type(2){
            color: #fff;
            background:#ffb454;
        }
        .save_addTo div:hover{
            background:#f90;
            color: #fff;
            border-color:#f90;
        }
        .share_parent span{
            margin-top: 20px;
            border-radius: 16%;
            font-size: 19px;
            color: #fff;
            display: inline-block;
            cursor: pointer;
            width: 20px;
            margin-right: 10px;
            min-width: 30px;
            text-align: center;
            line-height: 30px;
        }
        .share_parent span:nth-of-type(1){
            color: #000;
            font-size: 17px;
            margin-right: 20px;
        }
        .fb-share_class{
            background-color: #4267b2;
        }
        .tw-share_class{
            background-color: #1ca8e3;
        }
        .pint-share_class{
            background-color: #e40404;
        }
        .fb-share_class:before,.tw-share_class:before,.pint-share_class:before{
            color: #fff !important;
        }
        .row_details{
            padding-left: 50px;
        }

        @media (max-width: 1400px) and (min-width: 992px){
            .FrameColor ul{
                width: 110px;
            }
        }
        @media (max-width: 1360px) and (min-width: 992px){
            .FrameColor ul{
                width: 100px;
            }
            .box2{
                padding-right: 40px;
            }
            .swiper-pagination-bullet{
                width: 100px;
                height: 50px;
            }
        }
        @media (max-width: 1111px) and (min-width: 992px){
            .FrameColor ul{
                width: 80px;
            }
        }

        @media screen and (max-width: 992px) {
            .box2{
                padding-left: 60px;
            }
        }


    </style>
</head>

<body>
<!-- 头部 -->
<?php

echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Banner')
    ->setTemplate('Pg_Learning::page/banner.phtml')
    ->toHtml();

$id = 1031;


//当前simple
$currentSimpleData = CatalogProductFlat1::find()->where(['entity_id' => $id])->one();
$currentSimpleJson = (\yii\helpers\Json::encode($currentSimpleData->attributes));
//print_r($currentSimpleJson);
//print_r($currentSimpleData->__get('entity_id'));
//print_r($currentSimpleData->__get('sku'));

//当前configurable
$configableLinkData = CatalogProductSuperLink::find()->where(['product_id' => $id])->one();
$configableData = CatalogProductFlat1::find()->where(['entity_id' => $configableLinkData->__get('parent_id')])->one();
$configableJson = (\yii\helpers\Json::encode($configableData->attributes));


//$simpleLinkDatas = CatalogProductSuperLink::find()->where(['parent_id' => $configableLinkData->__get('parent_id')])->all();
//foreach ($simpleLinkDatas as $simpleLinkData) {
//    $simpleData = CatalogProductFlat1::find()->where(['entity_id' => $simpleLinkData->__get('product_id')])->one();
//    $simpleJson = (\yii\helpers\Json::encode($simpleData->attributes));
////    print_r($simpleJson);
//}

//simples
$simpleDatas = CatalogProductFlat1::find()
    ->leftJoin('catalog_product_super_link', 'catalog_product_flat_all_1.entity_id = catalog_product_super_link.product_id')
    ->where(['parent_id' => $configableLinkData->__get('parent_id')])->all();
foreach ($simpleDatas as $simpleData) {
    print_r(json_encode($simpleData->attributes));
}











//图片
$simpleLinkDatas = (new \yii\db\Query())->select('catalog_product_flat_all_1.*')
    ->from('catalog_product_super_link')
    ->leftJoin('catalog_product_flat_all_1', 'catalog_product_flat_all_1.entity_id = catalog_product_super_link.product_id')
    ->where(['parent_id' => $configableLinkData->__get('parent_id')])->all();




$images = \app\components\helper\DbHelper::getSlaveDb()->createCommand("SELECT
`main`.`value` AS 'file'
FROM
`catalog_product_entity_media_gallery` AS main
INNER JOIN `catalog_product_entity_media_gallery_value_to_entity` AS entity ON `main`.`value_id` = `entity`.`value_id`
left join eav_attribute as env on env.attribute_id = main.attribute_id
WHERE
(`env`.`attribute_code` = 'media_gallery') AND (`main`.`disabled` = 0) AND (`entity`.`entity_id` = '".$id."')")->queryAll();

print_r($images);

foreach ($images as $image) {
    echo ('https://beta3.payneglasses.com:450/media/catalog/product'.$image['file']) ;
}
?>
<div class="minicart-bg1"></div>
<!-- 头部 End-->


<div class="row">
    <div class="col-md-8 box">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($images as $image):?>
                <div class="swiper-slide">
                    <img src="<?php echo ('/media/catalog/product'.$image['file'])?>" alt="">
                </div>
                <?php endforeach;?>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
    <div class="col-md-4 box2">
        <h3>
            <?php print_r($currentSimpleData->__get('name'))?>
        </h3>
        <div class="FrameColor">
            <span>Frame Color:</span>
            <?php foreach ($simpleDatas as $simpleData): ?>
            <ul>
                <div>
                    <img src="/media/catalog/product/cache/230x115<?php echo str_replace("\\",'',$simpleData->__get('image')) ?>" alt="">
                </div>
                <p><?php echo $simpleData->__get('color_value')?></p>
            </ul>
            <?php endforeach; ?>
        </div>
        <div class="stock_s">
            <li>IN STOCK</li>
            <li>
                <b>SKU#</b>
                17202R10
            </li>
        </div>
        <div class="stock_price">
            <span>$12.90</span>
            <s>$18.90</s>
        </div>
        <div class="sm">
            Don't let this ship sail by without you! The Marina Cat Eye has everything you could ask for in a frame: fun, comfort, and style! Perfect for daily wear as progressives or single vision!
        </div>

        <div class="save_addTo">
            <div>Save</div>
            <div>Add To Cart</div>
        </div>
        <div class="share_parent">
            <span>Share:</span>
            <span class="fb-share  porto-icon-facebook fb-share_class"></span>
            <span class="tw-share porto-icon-twitter tw-share_class"></span>
            <span class="pint-share porto-icon-pinterest pint-share_class"></span>
        </div>
    </div>
</div>


<div class="row row_details">
    <div class="col-md-8"  id="Detail">
        <div class="product data items">
            <div class="data item title active bus-active"
                 id="tab-label-additional">
                <div class="h_g h_g_one"></div>
                <a class="data switch"
                   id="tab-label-additional-title" style="color:#000">
                    Details </a>
                <div class="bus-downB bus-down-active"></div>
            </div>
            <div class="data item content" id="additional">
                <div  class="reveiews-additional">
                    <table>
                        <tr>
                            <li>
                                <img src="https://static.payneglasses.com/static/version1575887035/frontend/Pg/payne/en_US/Magento_Catalog/images/frame-width.png" alt="">
                                <h3>Frame Width</h3>
                                <h4>136 mm</h4>
                            </li>
                            <li>
                                <img src="https://static.payneglasses.com/static/version1575887035/frontend/Pg/payne/en_US/Magento_Catalog/images/lens-height.png" alt="">
                                <h3>Lens Height</h3>
                                <h4>39 mm</h4>
                            </li>
                            <li>
                                <img src="https://static.payneglasses.com/static/version1575887035/frontend/Pg/payne/en_US/Magento_Catalog/images/frame-weight.png" alt="">
                                <h3>Weight</h3>
                                <h4>24g</h4>
                            </li>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!--底部-->
<?php
echo $resultLayoutFactory->create()->getLayout()
    ->createBlock('\Pg\Learning\Block\Page\Footer')
    ->setTemplate('Pg_Learning::page/footer.phtml')
    ->toHtml();
?>
<!--底部 End-->
</body>
<script>
    var arr = ["https://beta3.payneglasses.com:450/media/catalog/product/m/a/main_20_29.jpg",
        "https://beta3.payneglasses.com:450/media/catalog/product/a/n/angle-front_4_21.jpg",
        "https://beta3.payneglasses.com:450/media/catalog/product/a/n/angle-back_4_21.jpg",
        "https://beta3.payneglasses.com:450/media/catalog/product/f/r/front_13_21.jpg"
    ];
    var mySwiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            clickable:true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    var span_List = document.querySelectorAll(".swiper-pagination-bullets span");
        for(var i=0; i<span_List.length; i++){
            var img = document.createElement("img");
            img.src=arr[i];
            console.log(span_List[i]);
            span_List[i].appendChild(img);
        }
        alert("asfasfasdfa");
</script>
</html>
