<?php
namespace app\modules\frontend\controllers;

use app\models\PersistentSession;
use Yii;
use yii\web\Controller;
use yii\web\View;
use yii\data\Pagination;
use app\components\magento\Router;
use app\components\helper\ESHelper;

class SearchController extends BaseController
{
    public $layout = "emptylayout";
    private $all_colors;
    private $frame_shape_values;

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->get_all_frame_shapes();
        $this->get_all_colors();

        $keyword=Yii::$app->request->get("q","");
        $page=Yii::$app->request->get("page",1);
        $size=Yii::$app->request->get("size",60);
        $sort=Yii::$app->request->get("sort","");
        $type=Yii::$app->request->get("type","");
        $debug=Yii::$app->request->get("debug","");
        $result=$this->search($keyword,$page,$size,$sort);
        $customerHelper=Yii::$container->get("app\components\helper\CustomerHelper");
        $session=$customerHelper->getLoginSession();
        $amazonHelper=$objectManager->get("\Amazon\Core\Helper\Data");
        $amazonpay_config = [
            "secret_key"  => $amazonHelper->getSecretKey(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "access_key"  => $amazonHelper->getAccessKey(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "merchant_id" => $amazonHelper->getMerchantId(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "region"      => $amazonHelper->getRegion(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "sandbox"     => $amazonHelper->isSandboxEnabled(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default"),
            "client_id"   => $amazonHelper->getClientId(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, "default")
        ];
        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $data["data"]=[
            "customer_session"=>$session,
            "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
            "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
            "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
            "amazonpay_config"=>$amazonpay_config,
            "gtm_value"=>$_scopeConfig->getValue('googletagmanager/general/account',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default'),
            "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
            "color_list"=>$result["colors"],
            "data"=>$result
        ];
        if (Yii::$app->request->isPjax) {
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
            } else {
                header("Access-Control-Allow-Origin:"."*");
            }
            header("Access-Control-Allow-Methods:*");
            header("Access-Control-Allow-Credentials:true");
            header("Access-Control-Allow-Headers:x-requested-with,content-type");
            header("Expires:-1");
            header("Last-Modified:".gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control:no-store, no-cache, must-revalidate,post-check=0, pre-check=0");
            header("Pragma:no-cache");
            header("Content-type:application/json;charset=utf-8");
            $r=$this->renderPartial("search_pjax",$data);
            return $r;
        } else if (Yii::$app->request->isAjax || strtolower($type)==strtolower("ajax")) {
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
            } else {
                header("Access-Control-Allow-Origin:"."*");
            }
            header("Access-Control-Allow-Methods:*");
            header("Access-Control-Allow-Credentials:true");
            header("Access-Control-Allow-Headers:x-requested-with,content-type");
            header("Expires:-1");
            header("Last-Modified:".gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control:no-store, no-cache, must-revalidate,post-check=0, pre-check=0");
            header("Pragma:no-cache");
            header("Content-type:application/json;charset=utf-8");
            die(json_encode($data));
        } else if ($debug=="1") {
            die(print_r($data,true));
        } else {
            $r=$this->renderPartial("search",$data);
            return $r;
        }
    }

    private function esToArray($record) {
        $result=[
            "hits"=>$record["hits"]["hits"],
            "total"=>$record["hits"]["total"]["value"],
            "aggregations"=>isset($record["aggregations"])? $record["aggregations"]:[]
        ];
        return $result;
    }

    public function getPDPUrl($frame_simple_url,$lens_types=["Single","Blue_Non_Rx","Non_Rx","Bifocal"]){
        foreach ($lens_types as $lens_type) {
            if (isset($frame_simple_url[$lens_type])) {
                return $frame_simple_url[$lens_type];
            }
        }
        return [];
    }

    private function filter_arr($param_name) {
        $t_fss=[];
        $vs=Yii::$app->request->get($param_name,"");
        if (!empty($vs)) {
            $t_fss=[];
            $fss=explode(",",$vs);
            foreach ($fss as $fs) {
                $t_fss[]=$fs;
            }
        }
        return $t_fss;
    }

    private function es_query($keyword,$page,$size,$sort) {
        $query_arr=[
            "query"=>[
                "function_score"=>[
                    "query"=>[
                        "bool"=>[
                            "filter"=>[
                                ["term"=>["product_type"=>"frame"]],
//                                ["term"=>["is_retired"=>"false"]],
//                                ["term"=>["simples.is_retired"=>"false"]],
//                                ["term"=>["simples.is_in_stock"=>"true"]]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        if (!empty($keyword)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["should"]=[
                ["match"=>["name"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match_phrase"=>["description"=>["query"=>$keyword,"boost"=>1]]],
                ["match_phrase"=>["short_description"=>["query"=>$keyword,"boost"=>1]]],
                ["match"=>["material_value"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["category"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["term"=>["sku"=>$keyword]],
                ["match"=>["rim_type_value"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["frame_shape_value"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["frame_type_value"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["frame_size_value"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["simeple_material_value"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["parent_category"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],

                ["match"=>["simples.name"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["term"=>["simples.sku"=>$keyword]],
                ["match"=>["simples.color_value"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["simples.stock_sku"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["simples.description"=>["query"=>$keyword,"boost"=>1]]],
                ["match"=>["simples.short_description"=>["query"=>$keyword,"boost"=>1]]],

                ["match"=>["tags1.tag"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
                ["match"=>["tags2.tag"=>["query"=>$keyword,"fuzziness"=>"0","boost"=>1]]],
            ];
            $query_arr["query"]["function_score"]["query"]["bool"]["minimum_should_match"]=1;
        }

//        $query_arr["query"]["function_score"]["score_mode"]="sum";
//        $query_arr["query"]["function_score"]["script_score"]=[
//            "script"=>"if (doc['category_id'].value==10 ) {return 100;} else {return 100;}"
//        ];
//        $query_arr["query"]["function_score"]["boost_mode"]="replace";

        $query_arr["aggs"]=[
            "frame_shape_values"=>["terms"=>["field"=>"frame_shape","size"=>10000]],
            "colors"=>["terms"=>["field"=>"simples.color_id","size"=>10000]],
            "rim_types"=>["terms"=>["field"=>"rim_type","size"=>10000]],
            "materials"=>["terms"=>["field"=>"material","size"=>10000]],
        ];

        $category=Yii::$app->request->get("Category","");
        if (!empty($category)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["category_id"=>$this->filter_arr("Category")]
            ];
        }
        $stock=Yii::$app->request->get("Stock","");
        if (!empty($stock)
            && (
                $stock=="true" || $stock=="false" ||  $stock=="true,false" || $stock=="false,true"
            )
        ) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["simples.is_in_stock"=>$this->filter_arr("Stock")]
            ];
        } else {
            //如果没有库存参数，则默认为需要有库存
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["simples.is_in_stock"=>["true"]]
            ];
        }

        $is_retired=Yii::$app->request->get("is_retired","");
        if (!empty($is_retired)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["simples.is_retired"=>$this->filter_arr("is_retired")]
            ];
        } else {
            //如果没有retired参数，默认为没有retired
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["simples.is_retired"=>["false"]]
            ];
        }
        $frameShape=Yii::$app->request->get("FrameShape","");
        if (!empty($frameShape)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["frame_shape_value"=>$this->filter_arr("FrameShape")]
            ];
        }
        $is_clipon=Yii::$app->request->get("ClipOn","");
        if (!empty($is_clipon) && strtolower($is_clipon)=="true") {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["is_clip_on"=>$this->filter_arr("ClipOn")]
            ];
        }
        $has_NosePads=Yii::$app->request->get("NosePads","");
        if (!empty($has_NosePads) && strtolower($has_NosePads)=="true") {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["nose_pad"=>$this->filter_arr("NosePads")]
            ];
        }
        $has_SpringHinges=Yii::$app->request->get("SpringHinges","");
        if (!empty($has_SpringHinges) && strtolower($has_SpringHinges)=="true") {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["has_spring_hinges"=>$this->filter_arr("SpringHinges")]
            ];
        }
        $color_changing=Yii::$app->request->get("ColorChanging","");
        if (!empty($color_changing) && strtolower($color_changing)=="true") {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["color_changing"=>$this->filter_arr("ColorChanging")]
            ];
        }
        $low_bridge_fit=Yii::$app->request->get("LowBridgeFit","");
        if (!empty($low_bridge_fit) && strtolower($low_bridge_fit)=="true") {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["low_bridge_fit"=>$this->filter_arr("LowBridgeFit")]
            ];
        }
        $is_goggles=Yii::$app->request->get("SportsGlasses","");
        if (!empty($is_goggles) && strtolower($is_goggles)=="true") {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["is_goggles"=>$this->filter_arr("SportsGlasses")]
            ];
        }
        $try_on=Yii::$app->request->get("Try-OnAvailable","");
        if (!empty($try_on) && strtolower($try_on)=="true") {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["simples.try_on"=>$this->filter_arr("Try-OnAvailable")]
            ];
        }
        $rim_type=Yii::$app->request->get("RimType","");
        if (!empty($rim_type)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["rim_type_value"=>$this->filter_arr("RimType")]
            ];
        }
        $material=Yii::$app->request->get("Material","");
        if (!empty($material)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["material_value"=>$this->filter_arr("Material")]
            ];
        }
        $color=Yii::$app->request->get("Color","");
        if (!empty($color)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["simples.color_value"=>$this->filter_arr("Color")]
            ];
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $eyeglasses_category_ids = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["eyeglasses_category_ids"];
        $sunglasses_category_ids = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["sunglasses_category_ids"];
        $eyeglasses=Yii::$app->request->get("Eyeglasses","");
        $glasses_ids=[];
        if (!empty($eyeglasses)) {
            $glasses_ids=array_merge($glasses_ids,$eyeglasses_category_ids);
        }
        $sunglasses=Yii::$app->request->get("Sunglasses","");
        if (!empty($sunglasses)) {
            $glasses_ids=array_merge($glasses_ids,$sunglasses_category_ids);
        }
        if (!empty($glasses_ids)) {
            $query_arr["query"]["function_score"]["query"]["bool"]["filter"][]=[
                "terms"=>["category_id"=>$glasses_ids]
            ];
        }
        if (!empty($sort)) {
            if (strtolower($sort)==strtolower("price_asc")) {
                $query_arr["sort"]=[
                    "_script"=>[
                        "type"=>"number",
                        "script"=>[
                            "source"=>"if (doc['category_id'].value==6 || doc['category_id'].value==8 || doc['category_id'].value==10) {return 0;} else {return 1;}"
                        ],
                        "order"=>"asc"
                    ],
                    "simples.sales_price"=>["order"=>"asc"]
                ];
            } if (strtolower($sort)==strtolower("price_desc")) {
                $query_arr["sort"]=[
                    "_script"=>[
                        "type"=>"number",
                        "script"=>[
                            "source"=>"if (doc['category_id'].value==6 || doc['category_id'].value==8 || doc['category_id'].value==10) {return 0;} else {return 1;}"
                        ],
                        "order"=>"asc"
                    ],
                    "simples.sales_price"=>["order"=>"desc"]
                ];
            } else if (strtolower($sort)==strtolower("new_asc")) {
                $query_arr["sort"]=[
                    "_script"=>[
                        "type"=>"number",
                        "script"=>[
                            "source"=>"if (doc['category_id'].value==6 || doc['category_id'].value==8 || doc['category_id'].value==10) {return 0;} else {return 1;}"
                        ],
                        "order"=>"asc"
                    ],
                    "create_time"=>["order"=>"asc"]
                ];
            } else if (strtolower($sort)==strtolower("new_desc")) {
                $query_arr["sort"]=[
                    "_script"=>[
                        "type"=>"number",
                        "script"=>[
                            "source"=>"if (doc['category_id'].value==6 || doc['category_id'].value==8 || doc['category_id'].value==10) {return 0;} else {return 1;}"
                        ],
                        "order"=>"asc"
                    ],
                    "create_time"=>["order"=>"desc"]
                ];
            }
        } else {
            $query_arr["sort"]=[
                "_script"=>[
                    "type"=>"number",
                    "script"=>[
                        "source"=>"if (doc['category_id'].value==6 || doc['category_id'].value==8 || doc['category_id'].value==10) {return 0;} else {return 1;}"
                    ],
                    "order"=>"asc"
                ]
            ];
        }
        return json_encode($query_arr);
    }

    private function get_all_colors() {
        $sql="
SELECT
  `main_table`.`option_id`, `tsv`.`value` AS 'code', `swatch_table`.`value` AS 'code_option',`swatch_table`.type
FROM
  `eav_attribute_option` AS main_table
    LEFT JOIN `eav_attribute_option_value` AS tsv ON `tsv`.`option_id` = `main_table`.`option_id` AND `tsv`.`store_id` = 0
    LEFT JOIN `eav_attribute_option_swatch` AS swatch_table ON `swatch_table`.`option_id` = `main_table`.`option_id` AND `swatch_table`.`store_id` = 0
WHERE
  (`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='color')) AND (`tsv`.`store_id` = 0)
ORDER BY `tsv`.`value` ASC";
        $colors=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($colors as $color) {
            $this->all_colors[$color["option_id"]]=$color;
        }
    }

    private function get_all_frame_shapes() {
        $sql="
SELECT
  `main_table`.`option_id`, `tsv`.`value` AS 'frame_shape', `swatch_table`.`value` AS 'frame_shape_value',`swatch_table`.type
FROM
  `eav_attribute_option` AS main_table
    LEFT JOIN `eav_attribute_option_value` AS tsv ON `tsv`.`option_id` = `main_table`.`option_id` AND `tsv`.`store_id` = 0
    LEFT JOIN `eav_attribute_option_swatch` AS swatch_table ON `swatch_table`.`option_id` = `main_table`.`option_id` AND `swatch_table`.`store_id` = 0
WHERE
  (`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='frame_shape')) AND (`tsv`.`store_id` = 0)
ORDER BY `tsv`.`value` ASC";
        $frame_shapes=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($frame_shapes as $frame_shape) {
            $this->frame_shape_values[$frame_shape["option_id"]]=$frame_shape;
        }
    }

    private function search($keyword,$page,$size,$sort) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");

        $esClient=ESHelper::getInstance()->getES();
        $from=($page-1)*$size;
        $query_str=$this->es_query($keyword,$page,$size,$sort);
        $query = json_decode(trim($query_str), true);
        $params = [
            'index' => 'pg_catalog_product',
//            '_source'=>["configurable_id","name","simples.color_value","frame_shape_value","material_value","price","product_type"],
            'body' => $query,
            "from"=>$from,
            "size"=>$size
        ];
        $t1 = \app\components\helper\TimezoneHelper::msectime();
        $results = $esClient->search($params);
        $t2 = \app\components\helper\TimezoneHelper::msectime();
        $d=$this->esToArray($results);
        foreach ($d["hits"] as $k=>$hit) {
            foreach ($hit["_source"]["simples"] as $k2 =>$simple) {
//                $second=floor((strtotime($localeDate->date()->format('Y-m-d H:i:s'))-strtotime($simple["create_time"]))%86400%60);
                $data_diff_days=floor((strtotime($localeDate->date()->format('Y-m-d H:i:s'))-strtotime($simple["create_time"]))/86400);
                $d["hits"][$k]["_source"]["simples"][$k2]["data_diff_days"]=$data_diff_days;
                if ($data_diff_days<=30) {
                    $d["hits"][$k]["_source"]["simples"][$k2]["is_new"]=1;
                } else {
                    $d["hits"][$k]["_source"]["simples"][$k2]["is_new"]=0;
                }
            }
        }
        $data["hits"]=$d["hits"];
        $color_list=[];
        if (isset($d["aggregations"]["colors"]["buckets"])) {
            foreach ($d["aggregations"]["colors"]["buckets"] as $color) {
                $color_list[$color["key"]]=$this->all_colors[$color["key"]];
            }
        }
        $data["colors"]=$this->all_colors;
        $data["frame_shape_values"]=isset($d["aggregations"]["frame_shape_values"]["buckets"])? $d["aggregations"]["frame_shape_values"]["buckets"]:[];
        $data["rim_types"]=isset($d["aggregations"]["rim_types"]["buckets"])? $d["aggregations"]["rim_types"]["buckets"]:[];
        $data["materials"]=isset($d["aggregations"]["materials"]["buckets"])? $d["aggregations"]["materials"]["buckets"]:[];
        $data["query_str"]=$query_str;
        $data["total"]=$d["total"];
        $data["pages"]=ceil($data["total"]/$size);
        $data["page"]=$page;
        $data["from"]=$from;
        $data["size"]=$size;
        $data["keyword"]=$keyword;
        $url=parse_url($_SERVER["REQUEST_URI"]);
        $url["query"]=isset($url["query"])? $url["query"]:"";
        $data["current_url"]="/search.html?".$url["query"];
        $data["newest_url"]=$data["current_url"]."&sort=new_asc";
        $data["price_asc_url"]=$data["current_url"]."&sort=price_asc";
        $data["price_desc_url"]=$data["current_url"]."&sort=price_desc";
        parse_str($url["query"],$current_url_q);
        $data["current_url_q"]=$current_url_q;
        parse_str($url["query"],$current_q);
        $current_q["type"]="ajax";
        $data["current_data_url"]="/search.html?".http_build_query($current_q);
        parse_str($url["query"],$next_q);
        if ($page+1<=$data["total"]) {
            $next_q["page"]=$page+1;
        } else {
            $next_q["page"]=$page;
        }
        $data["next_url"]="/search.html?".http_build_query($next_q);
        $data["next_url_q"]=$next_q;
        $next_q["type"]="ajax";
        $data["next_data_url"]="/search.html?".http_build_query($next_q);
        parse_str($url["query"],$prev_q);
        if ($page-1>=1) {
            $prev_q["page"]=$page-1;
        } else {
            $prev_q["page"]=1;
        }
        $data["prev_url"]="/search.html?".http_build_query($prev_q);
        $data["prev_url_q"]=$prev_q;
        $prev_q["type"]="ajax";
        $data["prev_data_url"]="/search.html?".http_build_query($prev_q);
        $data["timestamp_1"]=$t1;
        $data["timestamp_2"]=$t2;
        $data["times"]=$t2 - $t1;
        $data["pagination"] = new Pagination([
            //总的记录条数
            'totalCount' => $data["total"],
            //分页大小
            'pageSize' => $data["size"],
            //设置地址栏当前页数参数名
            'pageParam' => 'page',
            //设置地址栏分页大小参数名
            'defaultPageSize' => $size,
            'pageSizeParam'=>false,
            'route'=>"/search",
            'validatePage' => true
        ]);
        return $data;
    }

    private function get_enum_color_list($code) {
        $sql=sprintf("SELECT
  `main_table`.`option_id`, `tsv`.`value` AS 'code', `swatch_table`.`value` AS 'code_option',`swatch_table`.type
FROM
  `eav_attribute_option` AS main_table
    LEFT JOIN `eav_attribute_option_value` AS tsv ON `tsv`.`option_id` = `main_table`.`option_id` AND `tsv`.`store_id` = 0
    LEFT JOIN `eav_attribute_option_swatch` AS swatch_table ON `swatch_table`.`option_id` = `main_table`.`option_id` AND `swatch_table`.`store_id` = 0
WHERE
  (`attribute_id` = (select attribute_id from eav_attribute where attribute_code='%s')) AND (`tsv`.`store_id` = 0)
ORDER BY `main_table`.`option_id` ASC",$code);
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $rows;
    }
}
