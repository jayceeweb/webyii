<?php
namespace app\modules\frontend\controllers;

use app\models\UrlRewrite;
use Yii;

class ProtectiveController extends BaseController
{
    public $layout = "emptylayout";

    /**
     * 返回当前simple id
     * @return int
     */
    public function getSimpleId() {
        $id=0;

        $requestPathStr = Yii::$app->request->get('request_path');
        $urlRewriteObj = new UrlRewrite();
        $resSqlObj = $urlRewriteObj->getByPath($requestPathStr);
        if(isset($resSqlObj)) {
            $targetPathStr = $resSqlObj->getAttribute("target_path");
            $targetPathParsedArr = $urlRewriteObj->parseParams($targetPathStr,0);
            $id = $targetPathParsedArr['simple_id'];
        }
        return $id;
    }

    public function renderHtml($simple_id) {
        $json = Yii::$app->request->get('json',"");
        $data=Yii::$container->get("app\components\helper\ProtectiveHelper")->renderHtml($simple_id);
        if (empty($json)) {
            return $this->render("view",$data);
        } else {
            header("Content-type:application/json;charset=utf-8");
            die(json_encode($data));
        }
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionView()
    {
        $simple_id=$this->getSimpleId();
        if(empty($simple_id)) {
            return $this->redirect("/protective.html");
        } else {
            return $this->renderHtml($simple_id);
        }
    }
}
