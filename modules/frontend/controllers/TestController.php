<?php
namespace app\modules\frontend\controllers;

use app\models\PersistentSession;
use app\models\UrlRewrite;
use Yii;
use yii\web\Controller;
use yii\web\View;
use yii\helpers\Url;
use app\components\magento\Router;
use app\components\magento\MagentoRedis;
use app\components\magento\RedisSession;

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;
use Pg\Crypt\Authcode;
use GuzzleHttp\Client as GuzzleClient;

class TestController extends BaseController
{
    public function actionTest() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $c=Yii::$container->get("app\components\helper\CustomerHelper");
        echo $c->getSessionCustomerId()."<br>";
        print_r($c->getLoginSession());
        //        Yii::$app->cache->set("li","li");
//        echo Yii::$app->cache->get("li")."<br>";
//
//        $d=Yii::$app->db->cache(function () {
//           return Yii::$app->db->createCommand("select * from customer_entity where entity_id=3")->queryOne();
//        });
//        print_r($d);
//        Yii::$app->cache->flush();

//        Yii::$app->redis_magento->set('li',"lihaifeng");
//        echo Yii::$app->redis_magento->get("li")."<br>";

//        $m=new MagentoRedis();
//        print_r(unserialize($m->parseParams($m->load("DB_PDO_MYSQL_DDL_QUOTE_1"))["data"]));

//        $m=new RedisSession();
//        print_r($m->load(session_id()));

//        $p=new PersistentSession();
//        $info=$p->loadByCookieKey();
//        print_r(json_decode($info["info"]));
//        print_r($m->read(session_id(),"frontend"));
//        $session=Yii::$app->session;
//        $session->open();
//        print_r($_SESSION);
//        $session->close();
//        $u=new UrlRewrite();
//        $d=$u->parseParams($u->getByPath("eyeglasses/men/brice-browline-28804.html")->getAttribute("target_path"),3);
//        print_r($d);

//        print_r(Yii::$app->db->createCommand("select * from customer_entity where email='lihaifeng832008@163.com'")->queryOne());
    }

    public function actionT1()
    {
        require '/data/magento450/app/bootstrap.php';
        $params = $_SERVER;
        $params[Bootstrap::INIT_PARAM_FILESYSTEM_DIR_PATHS] = [
            DirectoryList::PUB => [DirectoryList::URL_PATH => ''],
            DirectoryList::MEDIA => [DirectoryList::URL_PATH => 'media'],
            DirectoryList::STATIC_VIEW => [DirectoryList::URL_PATH => 'static'],
            DirectoryList::UPLOAD => [DirectoryList::URL_PATH => 'media/upload'],
        ];
        $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $params);
        $app = $bootstrap->createApplication('Magento\Framework\App\Http');
        $bootstrap->runAsYii($app);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if (empty($objectManager->get("\Magento\Framework\App\State")->getAreaCode())) {
            $objectManager->get("\Magento\Framework\App\State")->setAreaCode('frontend');
        }
        $resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
        $layout=$resultLayoutFactory->create()->getLayout()
            ->createBlock('Pg\Learning\Block\Page\Banner')
            ->setTemplate('Pg_Learning::page/banner.phtml');
        $html=$layout->toHtml();
        return $html;
        exit;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $u=$objectManager->get("\Magento\Backend\Model\Url");
        $request=$objectManager->create('\Magento\Framework\App\RequestInterface');
        $response=$objectManager->create("\Magento\Framework\App\Response\Http");
        $p=$request->getParams();

        exit;
        $product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById(199);
        echo json_encode($product->getData());
        exit;
//        $monitor_log=$objectManager->create("Pg\Monitor\Log");
//        $monitor_log->getRedisClient();
//        $monitor_log->log_start("start");
//        $monitor_log->log("log 1234");
//        $monitor_log->log_stop("stop");
//        $monitor_log->log_error("log_error");
//        $monitor_log->log_simple_error("log_simple_error");


//        $cache=$objectManager->get("\Magento\Framework\Config\CacheInterface");
//        $reader=$objectManager->get("Magento\Framework\App\Route\Config\Reader");
//        $areaList=$objectManager->get("\Magento\Framework\App\AreaList");
//        $scope="frontend";
//        $cacheId="frontend::RoutesConfig";
//        $cachedRoutes = unserialize($cache->load("frontend::RoutesConfig"));
//        if (is_array($cachedRoutes)) {
//            $routes=$cachedRoutes;
//        }
//        else{
//            $routers = $reader->read($scope);
//            $routes = $routers[$areaList->getDefaultRouter($scope)]['routes'];
//        }
//        $cache->save(serialize($routes), $cacheId);
//        print_r($routes);exit;

//        $select=$objectManager->get("\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory")->create()
//            ->addAttributeToSelect($objectManager->get("\Magento\Catalog\Block\Product\Context")->getCatalogConfig()->getProductAttributes())
//            ->joinField(
//                'stock_item',
//                'cataloginventory_stock_item',
//                'is_in_stock',
//                'product_id=entity_id',
//                null
//            )
//            ->addAttributeToFilter('stock_sku', "5710K07","in")
//            ->addAttributeToFilter('type_id', "simple")
//            ->getSelect()->columns("at_stock_item.qty");
//        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
//        $list=$connection->fetchAll($select);
//        print_r($list);exit;

//        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
//        $secret=$cfg["secret_key"];
//        $order_id="85335";
//        $href="https://www.payneglasses.com/checkout/onepage/success/?state=".base64_encode(Authcode::encode($order_id,$secret));
//        echo "<a href='".$href."' target='_blank'>href</a>";

//        $product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById(199);
//        echo json_encode($product->getData());

        $oreSession = $objectManager->create('Magento\Framework\Session\SessionManager');
//        $oreSession->setData('obj_root','Magento\Framework\Session\SessionManager');
        echo json_encode($oreSession->getData());
//        exit;

        $session = $objectManager->get('\Magento\Customer\Model\Session');
        echo json_encode($session->getData());exit;

        exit;
    }

    public function actionT2() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $session=Yii::$app->session;
        $session->open();
        $magento_session=Yii::$container->get("app\components\magento\RedisSession");
//        session_start();
//        echo session_id()."<br>";
        $s=$magento_session->read(session_id());
//        print_r($s);

        $session->close();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $objectManager->get('\Magento\Customer\Model\Session');
        print_r(json_decode(json_encode($session->getData()),true));

//        $session = $objectManager->get('\Magento\Persistent\Helper\Session');
//        $session->getSession()->setData("asfdasf","asdfasdfasfdasdfasdf");
//        print_r(json_decode(json_encode($session->getSession()->getData()),true));

        $session = $objectManager->get('\Pg\PersistentExtend\Model\Session');
        $session=$session->loadByCookieKey();
//        $session->setData("asfdasf","asdfasdfasfdasdfasdf");
        $session->save();
//        print_r(json_decode(json_encode($session->getData()),true));
    }

    public function actionUp() {
        $auth = Yii::$app->authManager;


        // 添加 "createPost" 权限
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
//        $auth->add($createPost);

        // 添加 "updatePost" 权限
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
//        $auth->add($updatePost);

//        // 添加 "author" 角色并赋予 "createPost" 权限
        $author = $auth->createRole('author');
//        $auth->add($author);
//        $auth->addChild($author, $createPost);
//
//        // 添加 "admin" 角色并赋予 "updatePost"
//        // 和 "author" 权限
//        $admin = $auth->createRole('admin');
//        $auth->add($admin);
//        $auth->addChild($admin, $updatePost);
//        $auth->addChild($admin, $author);

        // 为用户指派角色。其中 1 和 2 是由 IdentityInterface::getId() 返回的id
        // 通常在你的 User 模型中实现这个函数。
//        $auth->assign($author, 2);
//        $auth->assign($admin, 1);

        $rule = new \app\modules\manager\rbac\AuthorRule;
//        $auth->add($rule);

        // 添加 "updateOwnPost" 权限并与规则关联
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Update own post';
        $updateOwnPost->ruleName = $rule->name;
//        $auth->add($updateOwnPost);

        // "updateOwnPost" 权限将由 "updatePost" 权限使用
//        $auth->addChild($updateOwnPost, $updatePost);

        // 允许 "author" 更新自己的帖子
        $auth->addChild($author, $updateOwnPost);

        echo "OK";
    }

    public function actionT3() {
        $user=\Yii::$app->user;

        if (\Yii::$app->user->can('createPost')) {
            echo "can";
        } else {
            echo "can't";
        }
    }

    public function actionT4() {
        if (Yii::$app->request->isGet) {
            $d=[
                "journels_id"=>4,
                "email"=>"zhangyaqin_0@163.com",
                "error_no"=>""
            ];
            return $this->render('t4',$d);
        } else if (Yii::$app->request->isPost) {
            $action=Yii::$app->getRequest()->post("action");
            if ($action=="journels") {
                $journels_id=Yii::$app->getRequest()->post("journels_id",4);
                $email=Yii::$app->getRequest()->post("email","zhangyaqin_0@163.com");
                $client = new GuzzleClient();
                $url=sprintf("https://api.maropost.com/accounts/%s/journeys/%s/trigger/99949064890551.json?auth_token=%s&email=%s",2160,$journels_id,"TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ",$email);
                $res = $client->request('POST', $url);
                $err_no=$res->getBody()->getContents();
                $d=[
                    "journels_id"=>$journels_id,
                    "email"=>$email,
                    "error_no"=>$err_no
                ];
                return $this->render('t4',$d);
            }
        }
    }

    public function actionT5() {
        if (Yii::$app->request->isGet) {
            return $this->render('t5',["result"=>0]);
        } else if (Yii::$app->request->isPost) {
            $action=Yii::$app->getRequest()->post("action");
            file_put_contents("/lihf/b.txt",sprintf("%s",print_r($action,true)).PHP_EOL,FILE_APPEND);
            if ($action=="rewards invitation") {
                $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
                $magento_autoload->loadMagentoFrameword();
                $customer_properties=json_decode(Yii::$app->getRequest()->post("customer_properties"),true);
                $properties=json_decode(Yii::$app->getRequest()->post("properties"),true);
                $result=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("rewards invitation",$customer_properties,$properties);
                return $this->render('t5',["result"=>$result]);
            }
        }
    }

    public function actionT6() {
        phpinfo();
    }
}