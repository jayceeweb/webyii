<?php
namespace app\modules\frontend\controllers;

use app\models\UrlRewrite;
use Yii;

class AccessoriesController extends BaseController
{
    public $layout = "emptylayout";

    private function test() {
        //        echo json_encode($product->getData());

        //$u=new UrlRewrite();
        //$d=$u->getByPath("no-route");

//        $u = new block();
//        $d = $u->getByPath("no-route");
//        ob_start();
//        echo $this->getViewPath();


//        $resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
//        $layout=$resultLayoutFactory->create()->getLayout()
//            ->createBlock('Pg\Learning\Block\Page\Banner')
//            ->setTemplate('Pg_Learning::page/banner.phtml');
//        $html=$layout->toHtml();

        //        $id = 1031;
//
//        //当前simple
//        $currentSimpleData = CatalogProductFlat1::find()->where(['entity_id' => $id])->one();
//        $currentSimpleJson = (\yii\helpers\Json::encode($currentSimpleData->attributes));
//        //当前configurable
//        $configableLinkData = CatalogProductSuperLink::find()->where(['product_id' => $id])->one();
//        $configableData = CatalogProductFlat1::find()->where(['entity_id' => $configableLinkData->__get('parent_id')])->one();
//        $configableJson = (\yii\helpers\Json::encode($configableData->attributes));
//
//        $simpleLinkDatas = CatalogProductSuperLink::find()->where(['parent_id' => $configableLinkData->__get('parent_id')])->all();
//        foreach ($simpleLinkDatas as $simpleLinkData) {
//            $simpleData = CatalogProductFlat1::find()->where(['entity_id' => $simpleLinkData->__get('product_id')])->one();
//            $simpleJson = (\yii\helpers\Json::encode($simpleData->attributes));
//        }

//        var_dump($sku);
//
//
//        $requestPath = $request->get('request_path');

//        $u=new UrlRewrite();
//        $d=$u->parseParams($u->getByPath("eyeglasses/men/brice-browline-28804.html")->getAttribute("target_path"),3);
//        print_r($d);
    }

    /**
     * 返回当前simple id
     * @return int
     */
    public function getSimpleId() {
        $id=0;

        $requestPathStr = Yii::$app->request->get('request_path');
        $urlRewriteObj = new UrlRewrite();
        $resSqlObj = $urlRewriteObj->getByPath($requestPathStr);
        if(isset($resSqlObj)) {
            $targetPathStr = $resSqlObj->getAttribute("target_path");
            $targetPathParsedArr = $urlRewriteObj->parseParams($targetPathStr,0);
            $id = $targetPathParsedArr['simple_id'];
        }
        return $id;
    }

    public function renderHtml($simple_id) {
        $data=Yii::$container->get("app\components\helper\AccessoriesHelper")->renderHtml($simple_id);
        return $this->render("view",$data);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionView()
    {
        $simple_id=$this->getSimpleId();
        if(empty($simple_id)) {
            return $this->redirect("/accessories.html");
        } else {
            return $this->renderHtml($simple_id);
        }
    }

    public function actionT()
    {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $html=Yii::$app->runAction("frontend/accessories/view");
        exit;
//        echo $res->getBody();exit;

        $content=file_get_contents("/data/magento450/pub/tagapi/1.html");
//        $content=$res->getBody();

        $mpdf = new \Mpdf\Mpdf([
            "autoScriptToLang"=>true,
            "autoLangToFont"=>true,
//            "tempDir"=>$tempDir,
//            "setAutoTopMargin"=>"stretch",
//            "setAutoBottomMargin"=>"stretch",
//            "autoMarginPadding"=>55
        ]);
        $mpdf->WriteHTML($content);
        $mpdf->Output();
        exit;
    }
}
