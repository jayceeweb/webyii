<?php
namespace app\modules\frontend\controllers;
//use app\modules\frontend\controllers\Ba
use app\models\PersistentSession;
use app\models\UrlRewrite;
use Yii;
use yii\web\Controller;
use yii\web\View;
use yii\helpers\Url;
use app\components\magento\Router;
use app\components\magento\MagentoRedis;
use app\components\magento\RedisSession;

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;
use Pg\Crypt\Authcode;

class EyeglassesController extends BaseController
{
    public $layout = "emptylayout";

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionView()
    {
        ob_start();
        echo $this->render("view");
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    public function getBanner() {
        require '/data/magento450/app/bootstrap.php';
        $params = $_SERVER;
        $params[Bootstrap::INIT_PARAM_FILESYSTEM_DIR_PATHS] = [
            DirectoryList::PUB => [DirectoryList::URL_PATH => ''],
            DirectoryList::MEDIA => [DirectoryList::URL_PATH => 'media'],
            DirectoryList::STATIC_VIEW => [DirectoryList::URL_PATH => 'static'],
            DirectoryList::UPLOAD => [DirectoryList::URL_PATH => 'media/upload'],
        ];
        $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $params);
        $app = $bootstrap->createApplication('Magento\Framework\App\Http');
        $bootstrap->runAsYii($app);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if (empty($objectManager->get("\Magento\Framework\App\State")->getAreaCode())) {
            $objectManager->get("\Magento\Framework\App\State")->setAreaCode('frontend');
        }
        $resultLayoutFactory=$objectManager->get('\Magento\Framework\View\Result\LayoutFactory');
        $layout=$resultLayoutFactory->create()->getLayout()
            ->createBlock('Pg\Learning\Block\Page\Banner')
            ->setTemplate('Pg_Learning::page/banner.phtml');
        $html=$layout->toHtml();
        return $html;
    }
}
