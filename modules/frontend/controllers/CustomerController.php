<?php
namespace app\modules\frontend\controllers;

use app\models\CoreConfigData;
use Magento\Directory\Helper\Data;
use Yii;
use Magento\Backend\Block\Admin\FormKey;
/**
 * 用户相关操作
 */
class CustomerController extends BaseController
{
    public $layout = "emptylayout";
    public $formKey;


    /**
     * 登录
     */
    public function actionLogin() {
        $referer = Yii::$app->request->get('referer');
        $referer = !empty($referer) ? $referer : (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/");
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $baseUrl=CoreConfigData::getBaseUrl();
        $httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $baseUrl . '/customer/account';
        $_customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        if($_customerSession->isLoggedIn()){
            return $this->redirect($httpReferer);
        }

        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $isAutocompleteDisabled = !$_scopeConfig->getValue(
            \Magento\Customer\Model\Form::XML_PATH_ENABLE_AUTOCOMPLETE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        //将formKey 设置为cookie存储
        $formKey = $this->registerFormkKeyFromCookie();
        $data=Yii::$container->get("app\components\helper\CustomerHelper")->renderHtml();
        $facebookAppId = $_scopeConfig->getValue('psloginfree/facebook/application_id');
        $localeCode = $_scopeConfig->getValue(
            Data::XML_PATH_DEFAULT_LOCALE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $amazonConfig = $_scopeConfig->getValue('payment/amazon_payment/pwa_enabled');
        $amazonClientId = $_scopeConfig->getValue('payment/amazon_payment/client_id');
        //twitter appid
        $twitterAppId = $_scopeConfig->getValue('psloginfree/twitter/application_id');

        $data['isAutocompleteDisabled'] = $isAutocompleteDisabled;
        $data['formKey'] = $formKey;
        $data['amazonConfig'] = $amazonConfig;
        $data['amazonClientId'] = $amazonClientId;
        $data['facebookAppId'] = $facebookAppId;
        $data['localeCode'] = $localeCode;
        $data['twitterAppId'] = $twitterAppId;
        $data['referer'] = $referer;
        return $this->render("login", $data);
    }

    //注册formkey为cookie和session
    public function registerFormkKeyFromCookie() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $cookieFormKeyObj = $objectManager->get("Magento\Framework\App\PageCache\FormKey");
        $sessionFormKeyObj = $objectManager->get("\Magento\Framework\Data\Form\FormKey");
        $escaperObj = $objectManager->get("\Magento\Framework\Escaper");
        if($cookieFormKeyObj->get()) {
            $this->updateCookieFormKey($cookieFormKeyObj->get());

            $sessionFormKeyObj->set(
                $escaperObj->escapeHtml($cookieFormKeyObj->get())
            );
        }

        return $cookieFormKeyObj->get();
    }

    private function updateCookieFormKey($formKey)
    {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $metaDataObj = $objectManager->get("\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory");
        $sessionObj = $objectManager->get("\Magento\Framework\Session\Config\ConfigInterface");
        $cookieFormKeyObj = $objectManager->get("Magento\Framework\App\PageCache\FormKey");

        $cookieMetadata = $metaDataObj->createPublicCookieMetadata();
        $cookieMetadata->setDomain($sessionObj->getCookieDomain());
        $cookieMetadata->setPath($sessionObj->getCookiePath());
        $cookieMetadata->setDuration($sessionObj->getCookieLifetime());

        $cookieFormKeyObj->set(
            $formKey,
            $cookieMetadata
        );
    }


    //注册
    public function actionCreateaccount() {
        $referer = Yii::$app->request->get('referer');
        $referer = !empty($referer) ? $referer : (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/");
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $baseUrl=CoreConfigData::getBaseUrl();
        $httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $baseUrl . '/';
        $_customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $registrationModel = $objectManager->get("Magento\Customer\Model\Registration");
        if($_customerSession->isLoggedIn()  || !$registrationModel->isAllowed()){
            return $this->redirect($httpReferer);
        }

        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $isAutocompleteDisabled = !$_scopeConfig->getValue(
            \Magento\Customer\Model\Form::XML_PATH_ENABLE_AUTOCOMPLETE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        //将formKey 设置为cookie存储
        $formKey = $this->registerFormkKeyFromCookie();
        $data=Yii::$container->get("app\components\helper\CustomerHelper")->renderHtml();
        $facebookAppId = $_scopeConfig->getValue('psloginfree/facebook/application_id');
        $localeCode = $_scopeConfig->getValue(
            Data::XML_PATH_DEFAULT_LOCALE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $amazonConfig = $_scopeConfig->getValue('payment/amazon_payment/pwa_enabled');
        $amazonClientId = $_scopeConfig->getValue('payment/amazon_payment/client_id');
        //twitter appid
        $twitterAppId = $_scopeConfig->getValue('psloginfree/twitter/application_id');

        $data['isAutocompleteDisabled'] = $isAutocompleteDisabled;
        $data['formKey'] = $formKey;
        $data['amazonConfig'] = $amazonConfig;
        $data['amazonClientId'] = $amazonClientId;
        $data['facebookAppId'] = $facebookAppId;
        $data['localeCode'] = $localeCode;
        $data['twitterAppId'] = $twitterAppId;
        $data['referer'] = $referer;
        return $this->render("createaccount", $data);
    }


    //忘记密码
    public function actionForgotpassword(){
        $referer = Yii::$app->request->get('referer');
        $referer = !empty($referer) ? $referer : (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/");
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $data=Yii::$container->get("app\components\helper\CustomerHelper")->renderHtml();
        $data['referer'] = $referer;
        return $this->render("forgotpassword", $data);
    }
}