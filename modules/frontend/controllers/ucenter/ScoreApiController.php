<?php
namespace app\modules\frontend\controllers\ucenter;

use Yii;
use yii\web\Response;
use app\modules\frontend\controllers\BaseController;

class ScoreApiController extends BaseController
{
    public function actionCheckPoints() {
        $ret=["code"=>1,"message"=>""];
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $helper=Yii::$container->get("app\components\helper\CustomerHelper");
        $customer_id=$helper->getSessionCustomerId();
        if (!empty($customer_id)) {
            $currentQuote=$helper->getCurrentQuote();
            $scoreHelper=$objectManager->get("Pg\CatalogExtend\Helper\PriceScoreHelper");
            if ($currentQuote!=null) {
                $ret=["code"=>0,"message"=>$scoreHelper->checkPointFor($currentQuote,0)];
            }
        }
        Yii::$app->response->format=Response::FORMAT_JSON;
        return $ret;
    }
}