<?php
namespace app\modules\frontend\controllers\ucenter;

use Yii;
use app\modules\frontend\controllers\BaseController;
use app\components\helper\CustomerHelper;
use app\models\PgScore;
use app\models\PgScoreBill;
use yii\base\ActionFilter;
use yii\data\Pagination;
use app\models\CoreConfigData;
use app\models\Profile;
use app\components\helper\AuthcodeHelper;
use GuzzleHttp\Client as GuzzleClient;

class ScoreController extends BaseController
{
    public function actionIndex() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $s=Yii::$container->get("app\components\helper\CustomerHelper");
        $session=Yii::$app->session;
        $session->open();
        $customer_id=$s->getSessionCustomerId();
        if (!empty($customer_id)) {
            $condition=[];
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition["customer_id"]=$customer_id;
            $condition["expect"]=1;
            $condition["is_deleted"]=2;
            $cnt=PgScoreBill::getListCount($condition);
            $rows=PgScoreBill::getList($page,$perpage,$condition);
            foreach ($rows as $k=>$row) {
                $obj=PgScoreBill::getSrcObject(["src_type"=>$row["src_type"],"src_id"=>$row["src_id"]]);
                $rows[$k]["src_obj"]=$obj;
            }
            foreach ($rows as $k=>$row) {
//                $obj=PgScoreBill::getConfigurableName($row["additional_data"]);
                $obj=PgScoreBill::getIncrementId($row["src_id"]);
                $rows[$k]["src_configurable_name"]=$obj;
            }
            $customer=$s->loadCustomer($customer_id);
            $loginSession=$s->getLoginSession();
            if (isset($loginSession["customer_base"]["profile_id"])) {
                $profile=Profile::findOne(["profile_id",$loginSession["customer_base"]["profile_id"]])->getAttributes();
            } else {
                $profile=Profile::findOne(["profile_id",$customer["default_profile"]])->getAttributes();
            }
            $score=PgScore::findOne(["entity_id"=>$customer["entity_id"]]);
            $total_score=$score["total_score"];
            $all_score=$score["all_score"];
            $permissions=$score["permissions"];

            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
                "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
                "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
                "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
                "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
                "customer"=>$customer,
                "profile"=>$profile,
                "total_score"=>$total_score,
                "all_score"=>$all_score,
                "permissions"=>$permissions,
            ];
            return $this->render('index',$d);
        } else {
            $url=sprintf(CoreConfigData::getBaseUrl()."/customer/account/login/referer/%s/",base64_encode(CoreConfigData::getBaseUrl()."/ucenter/score/index.html"));
            return $this->redirect($url);
        }
    }

    public function actionAcceptAgreement() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $email=Yii::$app->getRequest()->get("email");
        $email=AuthcodeHelper::decode_ex($email);
        //模拟登录

        $d=[
            "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
            "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
            "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
            "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
        ];
        return $this->render('accept_agreement',$d);
    }

    public function actionT2() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $d=[
            "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
            "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
            "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
            "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion()
        ];

//        echo "session_id1=".session_id();
//        echo "<br>";
//        print_r($_SESSION);
//        echo "<br>";


//        $session=$objectManager->get("\Magento\Persistent\Helper\Session")->getSession();
//        $a=json_decode(json_encode($session->load(session_id())->getData()),true);
//        print_r($a);

//        $magento_session=Yii::$container->get("app\components\magento\RedisSession");
//        $s=$magento_session->read(session_id());
//        echo "asdfsadf<br>";

        $s=Yii::$container->get("app\components\helper\CustomerHelper");
        $customer_id=$s->getSessionCustomerId();

        echo $customer_id;
    }

    public function actionUserShippingTrack() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $id=Yii::$app->getRequest()->get("id","");
        $monitor_log_merge=new \Pg\Monitor\Log();
        $monitor_log_merge->log_start("user-shipping-track");
        $monitor_log_merge->log(["raw_id"=>$id]);
        $id=AuthcodeHelper::decode_ex($id);
        $monitor_log_merge->log(["id"=>$id]);
        if (!empty($id)) {
            $sql=sprintf("SELECT data_content FROM `pg_exchange_data` WHERE exchange_id=%s",$id);
            $row=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            if (!empty($row)) {
//                $d=[
//                    "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
//                    "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
//                    "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
//                    "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
//                ];
                $data_content=json_decode($row["data_content"],true);
                $ep_public_url=$data_content["data"]["ep_public_url"];
                $client = new GuzzleClient();
                $res = $client->request('GET', $ep_public_url);
                $content=$res->getBody()->getContents();
                $monitor_log_merge->log(["ep_public_url"=>$ep_public_url]);
                $monitor_log_merge->log_stop("");
                return $content;
            }
            //跟踪有问题，目前测试只能跟踪订单，以后修改
//            if (strtolower($row["carrier_code"])=="ups") {
//                $_shipment=$objectManager->get("\Magento\Sales\Model\Order\Shipment\Track")->load($row["entity_id"]);
//            } else {
//                $_shipment=$objectManager->get("\Magento\Sales\Model\Order\Shipment")->load($row["parent_id"]);
//            }
//            $_shipment=$objectManager->get("\Magento\Sales\Model\Order\Shipment")->load($row["parent_id"]);
//            $url=$objectManager->get('\Magento\Shipping\Helper\Data')->getTrackingPopupUrlBySalesModel($_shipment);
//            return "<a target='_blank' href='".$url."'>asdf</a>";
//            $this->redirect($url);
        }
        $d=[
            "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
            "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
            "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
            "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
        ];
        return $this->render('nothistracking',$d);
    }
}