<?php
namespace app\modules\frontend\controllers\ucenter;

use phpDocumentor\Reflection\Element;
use Yii;
use app\modules\frontend\controllers\BaseController;
use app\components\helper\CustomerHelper;
use app\models\PgScore;
use app\models\PgScoreBill;
use yii\data\Pagination;
use app\models\CoreConfigData;
use app\models\Profile;
use app\components\helper\AuthcodeHelper;
use GuzzleHttp\Client as GuzzleClient;
use app\models\PgExchangeData;
use \Maropost\Api\Contacts;

class PayneRewardsController extends BaseController
{
    const LIST_ID=3;
    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";

    const REWARD_MEMBERS_LIST_ID=5;
    private $_objectManager;

    public function actionIndex() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $session=Yii::$app->session;
        $session->open();
        $s=Yii::$container->get("app\components\helper\CustomerHelper");
        $customer_id=$s->getSessionCustomerId();
        if (!empty($customer_id)) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $condition=$customer_id;
            $customer=$s->loadCustomer($customer_id);
            $loginSession=$s->getLoginSession();
            if (isset($loginSession["customer_base"]["profile_id"])) {
                $profile=Profile::findOne(["profile_id",$loginSession["customer_base"]["profile_id"]])->getAttributes();
            } else {
                $profile=Profile::findOne(["profile_id",$customer["default_profile"]])->getAttributes();
            }
            $score=PgScore::findOne(["entity_id"=>$customer["entity_id"]]);
            if (!empty($score)) {
                $total_score=$score["total_score"];
            }
            if (empty($total_score)) {
                $total_score=0;
            }
            $email = $customer["email"];
            $sql = "SELECT * FROM customer_entity where email = '$email'";
            $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $url = Yii::$app->request->hostInfo;
            $d=[
                'condition'=>$condition,
                'rows' => $data,
                "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
                "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
                "js_version"=>Yii::$container->get("app\models\CoreConfigData")->getJSfileVersion(),
                "magento_version"=>Yii::$container->get("app\components\magento\DevelopmentVersion")->getVersion(),
                "customer"=>$customer,
                "profile"=>$profile,
                "total_score"=>$total_score,
                "url" => $url,
            ];
            return $this->render('index',$d);
        } else {
            $url=sprintf(CoreConfigData::getBaseUrl()."/customer/account/login/referer/%s/",base64_encode(CoreConfigData::getBaseUrl()."/ucenter/payne-rewards/index.html"));
            return $this->redirect($url);
        }
    }

    public function authcode_encode($data) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $authcode_secret=$cfg["secret_key"];
        return base64_encode(AuthcodeHelper::encode($data,$authcode_secret));
    }

    //add sunjinchao 2020/2/23 邮箱加入
    public function actionJoin(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $s=Yii::$container->get("app\components\helper\CustomerHelper");
        yii::$app->session->open();
        $customer_id=$s->getSessionCustomerId();
        $confirm_email = Yii::$app->getRequest()->post('confirm_email','');
        $invitation = Yii::$app->getRequest()->post('invitation','');
        $email = Yii::$app->getRequest()->post('email','');

        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$this->_objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $now=$localeDate->date()->format('Y-m-d H:i:s');
        $now_day=$localeDate->date()->format('m/d/Y');
        if (!empty($confirm_email) && !empty($customer_id)) {
            //1\检查customer_entity,confirm_email_confirm 1通过 0未通过
//            $sql = "select recommended from pg_score where entity_id = $customer_id";
//            $recommended = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();

            $sql = "SELECT * FROM customer_entity where email = '$email'";
            $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $confirm_email_confirm = $data['confirm_email_confirm'];
            $enabled_score = $data['enabled_score'];
            //2 验证该用户有没有积分权限
//            if($recommended['recommended'] != 1 && $enabled_score != 1){
//                die(json_encode(["code"=>-4,"ret"=>"Your account is not currently open to this function"]));
//            }
            //3 验证用户是否已经开启积分协议
            if($confirm_email_confirm == 1 && $enabled_score == 1){
                die(json_encode(["code"=>-4,"ret"=>"This mailbox has been registered"]));
            }
            //3 验证用户是否已经同意积分协议
            if($confirm_email_confirm == 1){
                die(json_encode(["code"=>-7,"ret"=>"This register mailbox has been registered"]));
            }
            //4 验证用户是否更改积分协议邮箱

            if($confirm_email != $data['confirm_email']){
                $sql = "update customer_entity set confirm_email='$confirm_email',update_confirm_email_time='$now',confirm_email_operation=1 where email='$email'";
                $code = Yii::$app->db->createCommand($sql)->execute();
                if(!$code){
                    die(json_encode(["code"=>-5,"ret"=>"Mailbox modification failed"]));
                }
            }

            //5 验证用户是否是邀请用户
            if($invitation){
                $sql = "select * from pg_score where entity_id = $customer_id";
                if(\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()){
                    $sql = "update pg_score set recommended=1,permissions = 1 where entity_id = $customer_id";
                }else{
                    $sql = "insert into pg_score(entity_id,recommended,recommend_date,permissions) values ($customer_id,1,'$now',1)";
                }
                Yii::$app->db->createCommand($sql)->execute();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $POINTS_CUSTOMER_GROUP_ID = $objectManager->create("Magento\Framework\App\DeploymentConfig\Reader");
                $group_id = $POINTS_CUSTOMER_GROUP_ID->load("app_env")["POINTS_CUSTOMER_GROUP_ID"];
                $sql = "update customer_entity set group_id=$group_id,confirm_email_confirm=1,confirm_date='$now',enabled_score=1,enabled_score_time = '$now' where entity_id=$customer_id";
                Yii::$app->db->createCommand($sql)->execute();

                //更新用户的用户组数据
                $customerRepository = $this->_objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
                $customer  = $customerRepository->getById($customer_id);
                $customer->setData('confirm_email', $confirm_email);
                $customer->setData('update_confirm_email_time', $now);
                $customer->setData('confirm_email_operation', 1);
                $customer->setData('group_id', $group_id);
                $customer->setData('confirm_email_confirm', 1);
                $customer->setData('confirm_date', $now);
                $customer->setData('enabled_score', 1);
                $customer->setData('enabled_score_time', $now);
                $customerRepository->save($customer);

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $dr=\Magento\Framework\App\ObjectManager::getInstance()->get("Magento\Framework\Filesystem\DirectoryList");
                $document_root=$dr->getRoot();
                //delete lee 2020.12.4 改为klaviyo
//                include_once $document_root."/lib/internal/Pg/Maropost/tests/autoloader.php";
//                $contacts=new Contacts(self::ACCOUNT_ID, self::AUTHTOKEN);
//                $e = $contacts->createOrUpdateForList(self::REWARD_MEMBERS_LIST_ID,$email);
                //end

                //特殊时间内称为会员赠送积分
                $bill = new PgScoreBill();
                $special_time_score = 2000;
                $bill->customer_id=$customer_id;
                $bill->type_id="activity";
                $bill->src_type="activity";
                $bill->src_id=$customer_id;
                $bill->operation=0;
                $bill->score=$special_time_score;
                $bill->score_status=1;
                $bill->note='Join Payne Rewards earn 2,000 extra points';
                $bill->bill_time=date("Y-m-d H:i:s",time());
                $bill->save();
                $bill->calcCustomerScore($customer_id);
                die(json_encode(["code"=>1,"ret"=>'You have successfully joined']));
            }
//            $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
//            $marohelper->loadClass();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customer_id);
            //先同步联系人信息
            $catime = strtotime($now);
            $catime += 86400*7;
            $expiration_now=date('Y-m-d H:i:s',$catime);
            $expiration_now_day=date('m/d/Y',$catime);
            //保存交换数据
            $pgExchangeModel=new PgExchangeData();
            $pgExchangeModel->customer_id=$customer_id;
            $pgExchangeModel->exchange_code="points_join";
            $exchange_data=[
                "email"=>$email,
                "confirm_email"=>$confirm_email,
                "email_send_time"=>$now,
                "expiration_now"=>$expiration_now,
                "tmp_email_send_time_str"=>$now_day,
                "tmp_email_expiration_time_str"=>$expiration_now_day,
            ];
            $pgExchangeModel->data_content=json_encode($exchange_data);
            $pgExchangeModel->save();
            $send_data = [
                "email_auth_code"=>$this->authcode_encode($email),//lihaifeng832008@163.com
                "tmp_confim_email_auth_code"=>$this->authcode_encode($confirm_email),//li@163.com
                "tmp_email_send_time"=>$now,
                "tmp_email_expiration_time"=>$expiration_now,
                "tmp_email_send_time_str"=>$now_day,
                "tmp_email_expiration_time_str"=>$expiration_now_day,
                "tmp_exchange_id_auth_code"=>$this->authcode_encode($pgExchangeModel->getAttribute("exchange_id")),
            ];
            //delete lee 2020.12.4 改为klaviyo
//            $maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
//            $maropost_api->createOrUpdateForList(static::LIST_ID,$confirm_email,$customer->getFirstname(),$customer->getLastname(),null,null,null,$send_data);
//            $client = new GuzzleClient();
//            $url=sprintf("https://api.maropost.com/accounts/2160/journeys/8/trigger/99949064890551.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$confirm_email);
//            $res = $client->request('POST', $url);
//            $err_no=$res->getBody()->getContents();
//            if ($err_no=="Success!") {
//                $err_no=1;
//            }
            //end
            $customer_properties=array_merge(['$email'=>$confirm_email,'$first_name'=>$customer->getFirstname(),'$last_name'=>$customer->getLastname()],$send_data);
            $properties=$send_data;
            $err_no=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("rewards_verify",$customer_properties,$properties);
            $json_send_data = json_encode($send_data);
            $url='';
            $sql = "INSERT INTO `send_email_code` (`action`, `customer_id`, `email`, `correlation_data`, `parameter`, `api_url`, `email_send_time`, `response`) VALUES ('join now', '$customer_id', '$email', '$confirm_email', '$json_send_data', '$url', '$now', '$err_no')";
            $code = Yii::$app->db->createCommand($sql)->execute();
            if (strtolower($err_no)=="1") {
                die(json_encode(["code"=>1,"ret"=>'your email have been sent successful',"data"=>$confirm_email]));
            } else {
                die(json_encode(["code"=>-6,"ret"=>"Email delivery failed"]));
            }
        } else if (empty($confirm_email)) {
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>-1,"ret"=>"not confirm email"]));
        } else if (empty($customer_id)) {
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>-2,"ret"=>"not login"]));
        } else {
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>-3,"ret"=>"error"]));
        }
    }
}