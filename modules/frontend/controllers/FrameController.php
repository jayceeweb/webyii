<?php
namespace app\modules\frontend\controllers;

use app\models\UrlRewrite;
use Yii;
use app\components\magento\Router;

class FrameController extends BaseController
{
    public $layout = "emptylayout";

    /**
     * 返回当前simple id
     * @return int
     */
    public function getSimpleRewrite() {
        $ret=[];

        $requestPathStr = Yii::$app->request->get('request_path');
        $urlRewriteObj = new UrlRewrite();
        $resSqlObj = $urlRewriteObj->getByPath($requestPathStr);
        if(isset($resSqlObj)) {
            $ret=$resSqlObj->getAttributes();
            $targetPathStr = $resSqlObj->getAttribute("target_path");
            $targetPathParsedArr = $urlRewriteObj->parseParams($targetPathStr,0);
            $simple_id = $targetPathParsedArr['simple_id'];
            $lens_type = $targetPathParsedArr['lens_type'];
            $frame_group = $targetPathParsedArr['frame_group'];
            $category_id = $targetPathParsedArr['category_id'];
            $tag = $targetPathParsedArr['tag'];
            $ret["simple_id"]=$simple_id;
            $ret["lens_type"]=$lens_type;
            $ret["frame_group"]=$frame_group;
            $ret["category_id"]=$category_id;
            $ret["tag"]=$tag;
        }
        if (!empty(Yii::$app->request->get("json"))) {
            $ret["params"]["json"]=1;
        }
        return $ret;
    }

    private function cache_to_html($data,$html) {
        $url=$data["currentSimpleData"]["simple_url"]["url"];

        $dr=\Magento\Framework\App\ObjectManager::getInstance()->get("Magento\Framework\Filesystem\DirectoryList");
        $root_path=$dr->getRoot()."/pub";
        $html_file_name=$root_path."/".$url;
        $html_file_path_info=pathinfo($html_file_name);
        if(!file_exists($html_file_path_info["dirname"])) {
            mkdir($html_file_path_info["dirname"],0777,true);
        }
        if (!file_exists($html_file_name)) {
//            file_put_contents($html_file_name,$html);
        }
    }

    public function actionView()
    {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $VCAR=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("VCAR1");
        $LRD50=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LRD50");
        $LRD59=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LRD59");
        $LFC56=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LFC56");
        $LFC59=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LFC59");
        $LSB592=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LSB592");
        $LSB591=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LSB591");
        $dp_data=[
            "VCAR"=>$VCAR,
            "LRD50"=>$LRD50,
            "LRD59"=>$LRD59,
            "LFC56"=>$LFC56,
            "LFC59"=>$LFC59,
            "LSB592"=>$LSB592,
            "LSB591"=>$LSB591,
        ];
        $param_info["dp_data"]=$dp_data;

        $simple_rewrite=$this->getSimpleRewrite();
        if(empty($simple_rewrite["simple_id"])) {
            return $this->redirect("/frame.html");
        } else if (empty($simple_rewrite["params"]["json"])) {
            $cnt=Yii::$app->getDb()->createCommand(sprintf("select count(0) as cnt from catalog_product_flat_1 where entity_id=%s",$simple_rewrite["simple_id"]))->queryScalar();
            if ($cnt==0) {
                return $this->redirect("/frame.html");
            } else {
                try {
                    $data=Yii::$container->get("app\components\helper\FrameHelper")->renderHtml($simple_rewrite,$param_info);
                    $r=$this->renderPartial("view",$data);
                    return $r;
                } catch (\Exception $e) {
                    $monitor_log=$objectManager->create("Pg\Monitor\Log");
                    $monitor_log->log_start("new_pdp_exception");
                    $monitor_log->log(["url_rewrite"=>$simple_rewrite,"exception"=>$e->getMessage()]);
                    $monitor_log->log_stop();
                }
            }
        } else {
            $data=Yii::$container->get("app\components\helper\FrameHelper")->renderHtml($simple_rewrite,$param_info);
            header("Content-type:application/json;charset=utf-8");
            die(json_encode($data));
        }
    }

    public function actionList()
    {
        $sql="select f1.entity_id,ur.r_code,f1.sku,f1.name,ur.request_path,ur.entity_type,ur.target_path,f2.entity_id as parent_entity_id,
f2.sku as parent_sku,f2.name as parent_name,simple_attr.value as simple_enabled,parent_attr.value as parent_enabled,
stock.is_in_stock
from url_rewrite ur
inner join catalog_product_flat_1 f1 on f1.entity_id=ur.entity_id
inner join catalog_product_super_link sp on sp.product_id=ur.entity_id
inner join catalog_product_flat_1 f2 on f2.entity_id=sp.parent_id
INNER JOIN catalog_product_entity_int parent_attr on parent_attr.entity_id=f2.entity_id and parent_attr.attribute_id=(SELECT attribute_id FROM eav_attribute WHERE entity_type_id=4 AND attribute_code='status')
INNER JOIN catalog_product_entity_int simple_attr on simple_attr.entity_id=f1.entity_id and simple_attr.attribute_id=(SELECT attribute_id FROM eav_attribute WHERE entity_type_id=4 AND attribute_code='status')
INNER JOIN cataloginventory_stock_item stock ON stock.product_id=f1.entity_id
where (entity_type='pg_frame' or entity_type='pg_grant_frame' or entity_type='accessories')
and simple_attr.value=1 and parent_attr.value=1 order by stock.is_in_stock desc";
        $list=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $this->render("list",["list"=>$list]);
    }
}
