<?php
namespace app\modules\manager\controllers;
use app\modules\manager\models\AdminUserLogin;

/**
 * Default controller for the `product` module
 */
class WelcomeController extends BaseController
{
    public $layout = "emptylayout";

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('welcome');
    }
}
