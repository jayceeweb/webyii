<?php
namespace app\modules\manager\controllers\organization;

use app\models\PgCouponCodeInfo;
use app\models\PgOrganization;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class CouponCodeController extends BaseController{
//    url-rewrite页面展示加搜索
    public function actionIndex() {
        if (Yii::$app->admin->can('organization')) {
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $is_active=Yii::$app->getRequest()->get('is_active','');
            $condition["is_active"]=$is_active;
            $telephone=Yii::$app->getRequest()->get('telephone','');
            $condition["telephone"]=$telephone;
            $coupon_code=Yii::$app->getRequest()->get('coupon_code','');
            $condition["coupon_code"]=$coupon_code;
            $org_name=Yii::$app->getRequest()->get('org_name','');
            $condition["org_name"]=$org_name;
            $region_id=Yii::$app->getRequest()->get('region_id','');
            $condition["region_id"]=$region_id;
            $rows=PgCouponCodeInfo::getList($page,$perpage,$condition);
            $cnt=PgCouponCodeInfo::getListCount($condition);
            $org_name=PgOrganization::org_name();
            $region_name = PgOrganization::region_name();
            $d=[
                'condition'=>$condition,
                'org_name'=>$org_name,
                'region_name'=>$region_name,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    public function actionIsActive(){
        $id=Yii::$app->getRequest()->post('id','');
        $is_active=Yii::$app->getRequest()->post('is_active','');
        $cnt=PgCouponCodeInfo::active($id,$is_active);
    }
    public function actionUpdateCoupon(){
        $id=Yii::$app->getRequest()->get('id','');
        $data = array();
        if($id){
            $data=PgCouponCodeInfo::fetchOne($id);
        }else{
            $str = 'id,coupon_code,org_id,firstname,lastname,street,city,region,region_id,country_id,postcode,telephone,create_at,update_at,create_admin_id,update_admin_id,is_active';
            $column = explode(',',$str);
            foreach ($column as $key => $value){
                $data[$value] = '';
            }
        }
        $data['org_name'] = PgOrganization::org_name();
        $data['region_name'] = PgOrganization::region_name();
        return $this->render('update',$data);
    }
    public function actionUpdateSave(){
        $id=Yii::$app->getRequest()->post('id','');
        $data = $_POST;
        unset($data['_csrf']);
        $region_id=Yii::$app->getRequest()->post('region_id','');
        $data['region'] = '';
        $data['country_id'] = '';
        if($region_id){
            $region_detail = PgOrganization::region_detail($region_id);
            $region = $region_detail['default_name'];
            $country_id = $region_detail['country_id'];
            $data['region'] = $region;
            $data['country_id'] = $country_id;
        }else{
            $data['region_id'] = 0;
        }
        if($id){
            $data['update_admin_id'] = Yii::$app->admin->getIdentity()->getAttribute('username');
            $data['update_at'] = date("Y-m-d H:i:s",time());
            $cnt=PgCouponCodeInfo::toupdate($data);
        }else{
            unset($data['id']);
            $data['create_admin_id'] = Yii::$app->admin->getIdentity()->getAttribute('username');
            $data['create_at'] = date("Y-m-d H:i:s",time());
            $cnt=PgCouponCodeInfo::toinsert($data);
        }
        return true;
    }
}