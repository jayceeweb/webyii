<?php
namespace app\modules\manager\controllers\organization;

use app\models\PgOrganization;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class OrganizationController extends BaseController{
//    url-rewrite页面展示加搜索
    public function actionIndex() {
        if (Yii::$app->admin->can('organization')) {
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $is_active=Yii::$app->getRequest()->get('is_active','');
            $condition["is_active"]=$is_active;
            $org_type=Yii::$app->getRequest()->get('org_type','');
            $condition["org_type"]=$org_type;
            $org_name=Yii::$app->getRequest()->get('org_name','');
            $condition["org_name"]=$org_name;
            $telephone=Yii::$app->getRequest()->get('telephone','');
            $condition["telephone"]=$telephone;
            $rows=PgOrganization::getList($page,$perpage,$condition);
            $cnt=PgOrganization::getListCount($condition);
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    public function actionIsActive(){
        $entity_id=Yii::$app->getRequest()->post('id','');
        $is_active=Yii::$app->getRequest()->post('is_active','');
        $cnt=PgOrganization::active($entity_id,$is_active);
    }
    public function actionUpdateOrganization(){
        $entity_id=Yii::$app->getRequest()->get('id','');
        $data = array();
        if($entity_id){
            $data=PgOrganization::fetchOne($entity_id);
        }else{
            $str = 'entity_id,org_type,org_name,org_code,org_description,street,city,region,postcode,country,telephone,is_active,fax,created_at';
            $column = explode(',',$str);
            foreach ($column as $key => $value){
                $data[$value] = '';
            }
        }
        $data['region_name'] = PgOrganization::region_name();
        return $this->render('add',$data);
    }
    public function actionUpdateSave(){
        $entity_id=Yii::$app->getRequest()->post('entity_id','');
        $data = $_POST;
        unset($data['_csrf']);
        $data['org_description'] = str_replace('\'','\'\'',$data['org_description']);
        $region_id=Yii::$app->getRequest()->post('region_id','');
        if(!$region_id){
            return false;
        }
        $region_detail = PgOrganization::region_detail($region_id);
        $region = $region_detail['default_name'];
        $country = $region_detail['country_id'];
        $data['region'] = $region;
        unset($data['region_id']);
        $data['country'] = $country;
        if($entity_id){
            $cnt=PgOrganization::toupdate($data);
        }else{
            unset($data['entity_id']);
            $data['created_at'] = date("Y-m-d H:i:s",time());
            $cnt=PgOrganization::toinsert($data);
        }
        return true;
    }
}