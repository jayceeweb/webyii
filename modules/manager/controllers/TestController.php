<?php

namespace app\modules\manager\controllers;
use app\modules\manager\models\AdminUser;
use app\modules\manager\models\AdminUserLogin;
use Yii;
use yii\web\Controller;
use yii\web\View;
use yii\helpers\Url;
use app\components\Encryptor;

/**
 * Default controller for the `product` module
 */
class TestController extends BaseController
{
    public $layout = "emptylayout";

    public function actionT1() {
        $user=\Yii::$app->admin;
//        print_r($user);
//        echo "user_id=".$user->getId()."<br>";
//        if (\Yii::$app->admin->can('updatePost',['post' => ""])) {
//            echo "can";
//        } else {
//            echo "can't";
//        }

        if (\Yii::$app->admin->can('updatePost',['post' => ""])) {
            echo "can";
        } else {
            echo "can't";
        }
    }
}
