<?php
namespace app\modules\manager\controllers;
use app\modules\manager\models\AdminUser;
use app\modules\manager\models\AdminUserLogin;
use Yii;
use yii\web\Controller;
use yii\web\View;
use yii\helpers\Url;

/**
 * Default controller for the `product` module
 */
class DefaultController extends BaseController
{
    public $layout = "emptylayout";

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->admin->isGuest) {
            return $this->redirect(["/manager/default/login"]);
        } else {
            return $this->render('index',[
                'has_blacklistManager'=>\Yii::$app->admin->can('blacklistManager'),
                'has_couponManager'=>\Yii::$app->admin->can('couponManager'),
                'has_scoreManager'=>\Yii::$app->admin->can('scoreManager'),
                'has_urlManager'=>\Yii::$app->admin->can('urlManager'),
                'has_refreshProduct'=>\Yii::$app->admin->can('refreshProduct'),
                'has_user_management'=>\Yii::$app->admin->can('user_management'),
                'has_organization'=>\Yii::$app->admin->can('organization'),
                'password'=>\Yii::$app->admin->can('password'),
            ]);
        }
    }

    public function login(AdminUser $adminuser)
    {
        return Yii::$app->admin->login($adminuser,3600*24*30,$duration=Yii::$app->params['adminSessionTimeoutSeconds']);
    }

    public function actionLogout(){
        Yii::$app->admin->logout();
        return $this->redirect(["/manager/default/login"]);
    }

    public function actionLogin() {
        if (Yii::$app->request->isGet) {
            return $this->render('login');
        } else if (Yii::$app->request->isPost) {
            $username=Yii::$app->request->post("username","");
            $password=Yii::$app->request->post("password","");

            $model = new AdminUser();
            $model->username=$username;
            $model->password=$password;

            $adminuser=AdminUser::find()->where(["username"=>$username])->one();
            if ($adminuser) {
                $p=Yii::$container->get("app\components\Encryptor");
                $checkpassword=$p->isValidHash($password,$adminuser->getAttribute("password"));
                if ($checkpassword) {
                    $adminuser->user_id=$adminuser->getAttribute("user_id");
                    $adminuser->username=$adminuser->getAttribute("username");
                    if ($this->login($adminuser)) {
                        return $this->redirect(["/manager/default/index"]);
                    }
                }
            }
            return $this->redirect(["/manager/default/login"]);
        }
    }

    public function actionNoPermission() {
        echo "asdf";
    }
}
