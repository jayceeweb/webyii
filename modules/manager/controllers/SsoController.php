<?php
namespace app\modules\manager\controllers;
use app\modules\manager\models\AdminUser;
use app\modules\manager\models\AdminUserLogin;
use Yii;
use yii\web\Controller;
use yii\web\View;
use yii\helpers\Url;
use app\components\Encryptor;
use app\components\helper\AuthcodeHelper;

/**
 * Default controller for the `product` module
 */
class SsoController extends BaseController
{
    public $layout = "emptylayout";

    public function actionLogin() {
        $userid=Yii::$app->getRequest()->get("userid");
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $secret=$cfg["secret_key"];
        $userid=AuthcodeHelper::decode(base64_decode($userid),$secret);
        $adminuser=AdminUser::findIdentity($userid);
        Yii::$app->admin->login($adminuser,3600*24*30);
        return $this->redirect(["/manager/default/index"]);
    }
}
