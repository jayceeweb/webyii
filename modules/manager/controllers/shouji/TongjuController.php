<?php
namespace app\modules\manager\controllers\shouji;

use Yii;
use yii\data\Pagination;
use app\modules\manager\controllers\BaseController;
use app\models\PgShoujiPrescription;
use app\models\PgShoujiTongju;

class TongjuController extends BaseController
{
    public function actionIndex() {
        $condition=[];
        $page=Yii::$app->getRequest()->get('page',1);
        $perpage=Yii::$app->getRequest()->get('per-page',10);
        $condition["sku"]=Yii::$app->getRequest()->get('sku','');
        $cnt=PgShoujiTongju::getListCount($condition);
        $rows=PgShoujiTongju::getList($page,$perpage,$condition);
        $d=[
            'condition'=>$condition,
            'rows'=>$rows,
            'rows_cnt'=>$cnt,
            'page_count'=> ceil($cnt/$perpage),
            'page'=>$page,
            'pager'=>new Pagination([
                'totalCount' => $cnt,
                'pageSize' => $perpage,
                'pageSizeParam'=>false,
                'pageParam'=>'page',
            ]),
        ];
        return $this->render('index',$d);
    }

    public function actionAdd() {
        $prescriptions=PgShoujiPrescription::findAll(["admin_id"=>Yii::$app->admin->getId()]);
        return $this->render("add",["prescriptions"=>$prescriptions]);
    }

    public function actionSave() {
        $do=new PgShoujiTongju();
        $do->admin_id=Yii::$app->admin->getId();
        $do->prescription_id=Yii::$app->getRequest()->post('prescription_id',0);
        $do->frame_sku=strtoupper(Yii::$app->getRequest()->post('frame_sku',""));
        $do->left_tonggao=Yii::$app->getRequest()->post('left_tonggao',0);
//        $do->left_tongkuan=Yii::$app->getRequest()->post('left_tongkuan',0);
        $do->right_tonggao=Yii::$app->getRequest()->post('right_tonggao',0);
//        $do->right_tongkuan=Yii::$app->getRequest()->post('right_tongkuan',0);
        $do->save();
        header("Content-type:application/json;charset=utf-8");
        if (!empty($do->entity_id)) {
            die(json_encode(["code"=>0,"ret"=>"OK"]));
        } else {
            die(json_encode(["code"=>-2,"message"=>$do->getErrors()]));
        }
    }

    public function actionRemove() {
        PgShoujiTongju::deleteAll(["entity_id"=>Yii::$app->getRequest()->post('id',0)]);
        header("Content-type:application/json;charset=utf-8");
        die(json_encode(["code"=>0]));
    }
}