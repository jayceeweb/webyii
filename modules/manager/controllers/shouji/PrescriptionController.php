<?php
namespace app\modules\manager\controllers\shouji;

use Yii;
use yii\data\Pagination;
use app\modules\manager\controllers\BaseController;
use app\models\PgShoujiPrescription;
use app\models\PgShoujiTongju;

class PrescriptionController extends BaseController
{
    public function actionIndex() {
        $condition=[];
        $page=Yii::$app->getRequest()->get('page',1);
        $perpage=Yii::$app->getRequest()->get('per-page',10);
        $condition["sku"]=Yii::$app->getRequest()->get('sku','');
        $cnt=PgShoujiPrescription::getListCount($condition);
        $rows=PgShoujiPrescription::getList($page,$perpage,$condition);
        $d=[
            'condition'=>$condition,
            'rows'=>$rows,
            'rows_cnt'=>$cnt,
            'page_count'=> ceil($cnt/$perpage),
            'page'=>$page,
            'pager'=>new Pagination([
                'totalCount' => $cnt,
                'pageSize' => $perpage,
                'pageSizeParam'=>false,
                'pageParam'=>'page',
            ]),
        ];
        return $this->render('index',$d);
    }

    public function actionAdd() {
        return $this->render("add");
    }

    public function actionSave() {
        $do=new PgShoujiPrescription();
        $do->admin_id=Yii::$app->admin->getId();
        $do->prescription_name=Yii::$app->getRequest()->post('prescription_name','');
        $do->rsph=Yii::$app->getRequest()->post('rsph',0);
        $do->lsph=Yii::$app->getRequest()->post('lsph',0);
        $do->rcyl=Yii::$app->getRequest()->post('rcyl',0);
        $do->lcyl=Yii::$app->getRequest()->post('lcyl',0);
        $do->rax=Yii::$app->getRequest()->post('rax',0);
        $do->lax=Yii::$app->getRequest()->post('lax',0);
        $do->rpri=Yii::$app->getRequest()->post('rpri',0);
        $do->lpri=Yii::$app->getRequest()->post('lpri',0);
        $do->rbase=Yii::$app->getRequest()->post('rbase',"");
        $do->lbase=Yii::$app->getRequest()->post('lbase',"");
        $do->radd=Yii::$app->getRequest()->post('radd',0);
        $do->ladd=Yii::$app->getRequest()->post('ladd',0);
        $do->pd=Yii::$app->getRequest()->post('pd',0);
        $do->single_pd=Yii::$app->getRequest()->post('single_pd',0);
        $do->rpd=Yii::$app->getRequest()->post('rpd',0);
        $do->lpd=Yii::$app->getRequest()->post('lpd',0);
        $do->rpri_1=Yii::$app->getRequest()->post('rpri_1',0);
        $do->lpri_1=Yii::$app->getRequest()->post('lpri_1',0);
        $do->rbase_1=Yii::$app->getRequest()->post('rbase_1',"");
        $do->lbase_1=Yii::$app->getRequest()->post('lbase_1',"");
        $do->save();
        header("Content-type:application/json;charset=utf-8");
        if (!empty($do->entity_id)) {
            die(json_encode(["code"=>0,"ret"=>"asdf"]));
        } else {
            die(json_encode(["code"=>-2,"message"=>"param error"]));
        }
    }

    public function actionRemove() {
        PgShoujiPrescription::deleteAll(["entity_id"=>Yii::$app->getRequest()->post('id',0)]);
        header("Content-type:application/json;charset=utf-8");
        die(json_encode(["code"=>0]));
    }
}