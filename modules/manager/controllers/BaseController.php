<?php
namespace app\modules\manager\controllers;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `product` module
 */
class BaseController extends Controller
{
    public $layout = "emptylayout";

    public function beforeAction($action)
    {
        if (Yii::$app->admin->isGuest) {
            if (Yii::$app->controller->route!="manager/default/login" && Yii::$app->controller->route!="manager/sso/login") {
                return $this->redirect(["/manager/default/login"]);
            }
        }

        return parent::beforeAction($action);
    }

    public function init()
    {
        parent::init();
        $a=time();
        Yii::$app->view->on(yii\web\View::EVENT_BEGIN_BODY, function ($a) use ($a) {
//            echo "begin:".print_r($a,true)."<br>";
        });

        Yii::$app->view->on(yii\web\View::EVENT_END_BODY, function ($a) use ($a) {
//            echo "end:".print_r($a,true)."<br>";
        });
    }
}
