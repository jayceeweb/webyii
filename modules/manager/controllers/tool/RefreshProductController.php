<?php
namespace app\modules\manager\controllers\tool;

use app\models\CustomerEntity;
use app\models\PgLensType;
use app\models\PgScoreBill;
use app\models\PgScoreRule;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class RefreshProductController extends BaseController{
    public function actionIndex() {
        if (Yii::$app->admin->can('refreshProduct')) {
            return $this->render('index');
        }
    }
    public function actionRefresh(){
        if (Yii::$app->admin->can('refreshProduct')) {
            exec("ssh ubuntu@10.0.1.198 \"sudo /data/bin/indexer_products.sh\"");
        }
    }
}