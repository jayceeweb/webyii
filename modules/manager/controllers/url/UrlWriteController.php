<?php
namespace app\modules\manager\controllers\url;

use app\models\CustomerEntity;
use app\models\UrlRewrite;
use app\models\PgScoreBill;
use app\models\PgScoreRule;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class UrlWriteController extends BaseController{
//    url-rewrite页面展示加搜索
    public function actionIndex() {
        if (Yii::$app->admin->can('urlManager')) {
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $entity_type=Yii::$app->getRequest()->get('entity_type','pg_frame');
            $dest_name=Yii::$app->getRequest()->get('dest_name','');
            $dest_sku=Yii::$app->getRequest()->get('dest_sku','');
            $name=Yii::$app->getRequest()->get('name','');
            $sku=Yii::$app->getRequest()->get('sku','');
            $condition["entity_type"]=$entity_type;
            $condition["dest_name"]=$dest_name;
            $condition["dest_sku"]=$dest_sku;
            $condition["name"]=$name;
            $condition["sku"]=$sku;
            $rows=UrlRewrite::getList($page,$perpage,$condition);
            $cnt=UrlRewrite::getListCount($condition);
            $type=UrlRewrite::getentityType($condition);
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'type'=>$type,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    //    lenstype数据添加
    public function actionUpdate(){
        if (Yii::$app->admin->can('urlManager')) {
            $url_rewrite_id=Yii::$app->getRequest()->get('url_rewrite_id','');
            $dest_name=Yii::$app->getRequest()->get('dest_name','');
            $meta_title=Yii::$app->getRequest()->get('meta_title','');
            $meta_keywords=Yii::$app->getRequest()->get('meta_keywords','');
            $meta_description=Yii::$app->getRequest()->get('meta_description','');
            $page_description=Yii::$app->getRequest()->get('page_description','');
            $page_description=Yii::$app->getRequest()->get('page_description','');
            $first_description=Yii::$app->getRequest()->get('first_description','');
            $second_description=Yii::$app->getRequest()->get('second_description','');
            $d=[
                'url_rewrite_id'=>$url_rewrite_id,
                'dest_name'=>$dest_name,
                'meta_title'=>$meta_title,
                'meta_keywords'=>$meta_keywords,
                'meta_description'=> $meta_description,
                'page_description'=> $page_description,
                'first_description'=> $first_description,
                'second_description'=> $second_description,
            ];
            return $this->render('update',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    //    lenstype修改数据保存
    public function actionUpdateSave(){
        if (Yii::$app->admin->can('urlManager')) {
            $url_rewrite_id=Yii::$app->getRequest()->post('url_rewrite_id','');
            $dest_name=Yii::$app->getRequest()->post('dest_name','');
            $meta_title=Yii::$app->getRequest()->post('meta_title','');
            $meta_keywords=Yii::$app->getRequest()->post('meta_keywords','');
            $meta_description=Yii::$app->getRequest()->post('meta_description','');
            $page_description=Yii::$app->getRequest()->post('page_description','');
            $first_description=Yii::$app->getRequest()->post('first_description','');
            $second_description=Yii::$app->getRequest()->post('second_description','');
            $d=[
                'url_rewrite_id'=>$url_rewrite_id,
                'dest_name'=>$dest_name,
                'meta_title'=>$meta_title,
                'meta_keywords'=>$meta_keywords,
                'meta_description'=> $meta_description,
                'page_description'=> $page_description,
                'first_description'=> $first_description,
                'second_description'=> $second_description,
            ];
            $cnt=UrlRewrite::toupdate($d);
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0,"ret"=>$cnt]));
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
}