<?php
namespace app\modules\manager\controllers\url;

use app\models\PgShorturlRewrite;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class ShorturlRewriteController extends BaseController{
//    url-rewrite页面展示加搜索
    public function actionIndex() {
        if (Yii::$app->admin->can('urlManager')) {
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $is_active=Yii::$app->getRequest()->get('is_active','');
            $condition["is_active"]=$is_active;
            $rewrite_type=Yii::$app->getRequest()->get('rewrite_type','');
            $condition["rewrite_type"]=$rewrite_type;
            $rewrite_code=Yii::$app->getRequest()->get('rewrite_code','');
            $condition["rewrite_code"]=$rewrite_code;
            $ori_uri=Yii::$app->getRequest()->get('ori_uri','');
            $condition["ori_uri"]=$ori_uri;
            $rewrite_url=Yii::$app->getRequest()->get('rewrite_url','');
            $condition["rewrite_url"]=$rewrite_url;
            $rows=PgShorturlRewrite::getList($page,$perpage,$condition);
            $cnt=PgShorturlRewrite::getListCount($condition);
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    public function actionIsActive(){
        $id=Yii::$app->getRequest()->post('id','');
        $is_active=Yii::$app->getRequest()->post('is_active','');
        $cnt=PgShorturlRewrite::active($id,$is_active);
    }
    public function actionToDelete(){
        $id=Yii::$app->getRequest()->post('id','');
        $cnt=PgShorturlRewrite::deleteByid($id);
    }
    public function actionUpdateShouturl(){
        $id=Yii::$app->getRequest()->get('id','');
        $data = array();
        if($id){
            $data=PgShorturlRewrite::fetchOne($id);
        }else{
            $str = 'id,rewrite_type,ori_uri,rewrite_url,is_active,rewrite_code,log_format';
            $column = explode(',',$str);
            foreach ($column as $key => $value){
                $data[$value] = '';
            }
        }
        return $this->render('update',$data);
    }
    public function actionUpdateSave(){
        $id=Yii::$app->getRequest()->post('id','');
        $data = $_POST;
        unset($data['_csrf']);
        if($id){
            $data['update_admin_id'] = Yii::$app->admin->getIdentity()->getAttribute('username');
            $data['update_at'] = date("Y-m-d H:i:s",time());
            $cnt=PgShorturlRewrite::toupdate($data);
        }else{
            unset($data['id']);
            $data['create_admin_id'] = Yii::$app->admin->getIdentity()->getAttribute('username');
            $data['create_at'] = date("Y-m-d H:i:s",time());
            $cnt=PgShorturlRewrite::toinsert($data);
        }
        return true;
    }
}