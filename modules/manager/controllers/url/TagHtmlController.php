<?php
namespace app\modules\manager\controllers\url;

use app\models\CustomerEntity;
use app\models\UrlRewrite;
use app\models\PgScoreBill;
use app\models\PgScoreRule;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class TagHtmlController extends BaseController{
//    url-rewrite页面展示加搜索
    public function actionIndex() {
        if (Yii::$app->admin->can('urlManager')) {
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $name=Yii::$app->getRequest()->get('name','');
            $condition["name"]=$name;
            $rows=UrlRewrite::getPgTagHtml($page,$perpage,$condition);
            $cnt=UrlRewrite::getPgTagHtmlCount($condition);
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        }
    }
    public function actionAdd(){
        if (Yii::$app->admin->can('urlManager')) {
            $id = Yii::$app->getRequest()->get('id', 0);
            $name = '';
            $category_id = '';
            $banner = '';
            $breadcrumbs_title = '';
            $list_name = '';
            $category_name = '';
            $meta_title = '';
            $meta_description = '';
            $meta_robots = '';
            $meta_keywords = '';
            $base_url_canonical = '';
            $page_num = '';
            $genders = array();
            if ($id != 0) {
                $search = array('id'=>$id);
                $data=UrlRewrite::getPgTagHtmlData($search);
                $name = $data['name'];
                $category_id = $data['category_id'];
                $banner = $data['banner'];
                $breadcrumbs_title = $data['breadcrumbs_title'];
                $list_name = $data['list_name'];
                $category_name = $data['category_name'];
                $meta_title = $data['meta_title'];
                $meta_description = $data['meta_description'];
                $meta_robots = $data['meta_robots'];
                $meta_keywords = $data['meta_keywords'];
                $base_url_canonical = $data['base_url_canonical'];
                $page_num = $data['page_num'];
                $genders = $data['genders'];
                $genders = explode(',', $genders);
            }
            $d = [
                "id" => $id,
                "name" => $name,
                "category_id" => $category_id,
                "banner" => $banner,
                "breadcrumbs_title" => $breadcrumbs_title,
                "list_name" => $list_name,
                "category_name" => $category_name,
                "meta_title" => $meta_title,
                "meta_description" => $meta_description,
                "meta_robots" => $meta_robots,
                "meta_keywords" => $meta_keywords,
                "base_url_canonical" => $base_url_canonical,
                "page_num" => $page_num,
                "genders" => $genders,
            ];
            return $this->render('add', $d);
        }
    }
    public function actionSave(){
        if (Yii::$app->admin->can('urlManager')) {
            $str = 'id,name,category_id,banner,breadcrumbs_title,list_name,category_name,meta_title,meta_description,meta_robots,meta_keywords,base_url_canonical,page_num,genders';
            $str_data = explode(',',$str);
            $id = Yii::$app->getRequest()->post('id', '');
            $name = Yii::$app->getRequest()->post('name', '');
            $category_id = Yii::$app->getRequest()->post('category_id', '');
            $banner = Yii::$app->getRequest()->post('banner', '');
            $breadcrumbs_title = Yii::$app->getRequest()->post('breadcrumbs_title', '');
            $list_name = Yii::$app->getRequest()->post('list_name', '');
            $category_name = Yii::$app->getRequest()->post('category_name', '');
            $meta_title = Yii::$app->getRequest()->post('meta_title', '');
            $meta_description = Yii::$app->getRequest()->post('meta_description', '');
            $meta_robots = Yii::$app->getRequest()->post('meta_robots', '');
            $meta_keywords = Yii::$app->getRequest()->post('meta_keywords', '');
            $base_url_canonical = Yii::$app->getRequest()->post('base_url_canonical', '');
            $page_num = Yii::$app->getRequest()->post('page_num', '');
            $genders = Yii::$app->getRequest()->post('genders', '');
            foreach ($str_data as $key => $value){
                $$value = str_replace('\'','\'\'',$$value);
            }
            $sql = '';
            if ($id != 0) {
                $sql = "update pg_tag_html set `name`='$name',`category_id`='$category_id',`banner`='$banner',`breadcrumbs_title`='$breadcrumbs_title',`list_name`='$list_name',`category_name`='$category_name',`meta_title`='$meta_title',`meta_description`='$meta_description',`meta_robots`='$meta_robots',`meta_keywords`='$meta_keywords',`base_url_canonical`='$base_url_canonical',`page_num`='$page_num',`genders`='$genders' where id=$id";
            } else if ($id == 0) {
                $search = array('banner'=>$banner);
                $data=UrlRewrite::getPgTagHtmlData($search);
                if ($data) {
                    $sql = "update pg_tag_html set `name`='$name',`category_id`='$category_id',`breadcrumbs_title`='$breadcrumbs_title',`list_name`='$list_name',`category_name`='$category_name',`meta_title`='$meta_title',`meta_description`='$meta_description',`meta_robots`='$meta_robots',`meta_keywords`='$meta_keywords',`base_url_canonical`='$base_url_canonical',`page_num`='$page_num',`genders`='$genders' where `banner`='$banner'";
                } else {
                    $sql = "insert into pg_tag_html (`name`,`category_id`,`banner`,`breadcrumbs_title`,`list_name`,`category_name`,`meta_title`,`meta_description`,`meta_robots`,`meta_keywords`,`base_url_canonical`,`page_num`,`genders`) values ('$name','$category_id','$banner','$breadcrumbs_title','$list_name','$category_name','$meta_title','$meta_description','$meta_robots','$meta_keywords','$base_url_canonical','$page_num','$genders')";
                }
            }
            $data = Yii::$app->db->createCommand($sql)->execute();
        }
    }
    public function actionDelete(){
        if (Yii::$app->admin->can('urlManager')) {
            $id = Yii::$app->getRequest()->post('id', '');
            $base_url_canonical = Yii::$app->getRequest()->post('base_url_canonical', '');
            $request_path = ltrim($base_url_canonical, '/');
            $sql = "delete from pg_tag_html where id = $id";
            $data = Yii::$app->db->createCommand($sql)->execute();
            $sql = "delete from url_rewrite where request_path='$request_path'";
            $data = Yii::$app->db->createCommand($sql)->execute();
        }
    }
    public function actionUrlAdd(){
        if (Yii::$app->admin->can('urlManager')) {
            $base_url_canonical = Yii::$app->getRequest()->get('base_url_canonical', '');
            $base_url_canonical = ltrim($base_url_canonical, '/');
            $search = array('request_path'=>$base_url_canonical);
            $data=UrlRewrite::getUrlRewriteData($search);
            $url_rewrite_id = 0;
            $entity_type = '';
            $entity_id = '';
            $request_path = '';
            $target_path = '';
            $redirect_type = '';
            $store_id = '';
            $is_autogenerated = '';
            $sitemap = '';
            if ($data) {
                $url_rewrite_id = $data['url_rewrite_id'];
                $entity_type = $data['entity_type'];
                $entity_id = $data['entity_id'];
                $request_path = $data['request_path'];
                $target_path = $data['target_path'];
                $redirect_type = $data['redirect_type'];
                $store_id = $data['store_id'];
                $is_autogenerated = $data['is_autogenerated'];
                $sitemap = $data['sitemap'];
            }
            $d = [
                "url_rewrite_id" => $url_rewrite_id,
                "entity_type" => $entity_type,
                "entity_id" => $entity_id,
                "request_path" => $request_path,
                "target_path" => $target_path,
                "redirect_type" => $redirect_type,
                "store_id" => $store_id,
                "is_autogenerated" => $is_autogenerated,
                "sitemap" => $sitemap,
            ];
            return $this->render('urladd', $d);
        }
    }
    public function actionUrlSave()
    {
        if (Yii::$app->admin->can('urlManager')) {
            $url_rewrite_id = Yii::$app->getRequest()->post('url_rewrite_id', '');
            $entity_type = Yii::$app->getRequest()->post('entity_type', '');
            $entity_id = Yii::$app->getRequest()->post('entity_id', '');
            $request_path = Yii::$app->getRequest()->post('request_path', '');
            $target_path = Yii::$app->getRequest()->post('target_path', '');
            $redirect_type = Yii::$app->getRequest()->post('redirect_type', '');
            $store_id = Yii::$app->getRequest()->post('store_id', '');
            $is_autogenerated = Yii::$app->getRequest()->post('is_autogenerated', '');
            $sitemap = Yii::$app->getRequest()->post('sitemap', '');
            $sql = "insert into url_rewrite (`entity_type`,`entity_id`,`request_path`,`target_path`,`redirect_type`,`store_id`,`is_autogenerated`) values ('$entity_type','$entity_id','$request_path','$target_path','$redirect_type','$store_id','$is_autogenerated')";
            if ($url_rewrite_id != 0) {
                $sql = "update url_rewrite set `entity_type`='$entity_type',`entity_id`='$entity_id',`request_path`='$request_path',`target_path`='$target_path',`redirect_type`='$redirect_type',`store_id`='$store_id',`is_autogenerated`='$is_autogenerated' where url_rewrite_id=$url_rewrite_id";
            }
            Yii::$app->db->createCommand($sql)->execute();
            if ($sitemap) {
                $sql = "update url_rewrite set `sitemap`=$sitemap where request_path='$request_path'";
                Yii::$app->db->createCommand($sql)->execute();
            }
        }
    }
}