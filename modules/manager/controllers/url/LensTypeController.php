<?php
namespace app\modules\manager\controllers\url;

use app\models\CustomerEntity;
use app\models\PgLensType;
use app\models\PgScoreBill;
use app\models\PgScoreRule;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class LensTypeController extends BaseController{
//    lenstype页面展示加搜索
    public function actionIndex() {
        if (Yii::$app->admin->can('scoreManager')) {
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $type_str = 'lens_type,lens_type_code,frame_group,name,mark,lens_type_route,lens_type_dest_name';
            $type_arr = explode(',', $type_str);
            $type=PgLensType::getentityType($type_arr);
            foreach ($type as $key => $value){
                $condition[$key] = Yii::$app->getRequest()->get($key,'');
            }
            $condition['id'] = Yii::$app->getRequest()->get('id','');
            $rows=PgLensType::getList($page,$perpage,$condition);
            $cnt=PgLensType::getListCount($condition);
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'type'=>$type,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    //    lenstype数据添加和修改
    public function actionAdd(){
        if (Yii::$app->admin->can('urlManager')) {
            $type_str = 'lens_type,lens_type_code,frame_group,name,mark,lens_type_route,lens_type_dest_name,lens_type_dest_sku';
            $type_arr = explode(',', $type_str);
            $id = Yii::$app->getRequest()->get('id','');
            if($id){
                $condition['id'] = $id;
            }
            foreach ($type_arr as $key => $value){
                $condition[$value] = Yii::$app->getRequest()->get($value,'');
            }
            $condition['price_included'] = Yii::$app->getRequest()->get('price_included','');
            $condition['clipon_price_included'] = Yii::$app->getRequest()->get('clipon_price_included','');
            $d = array('type'=>$condition);
            return $this->render('add',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    //    lenstype添加或修改数据保存
    public function actionSave(){
        if (Yii::$app->admin->can('urlManager')) {
            $data = $_POST;
            unset($data['_csrf']);
            if(isset($data['id'])){
                $cnt=PgLensType::toupdate($data);
            }else{
                $cnt=PgLensType::toinsert($data);
            }
            var_dump($cnt);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    //    lenstype数据删除
    public function actionDelete(){
        if (Yii::$app->admin->can('urlManager')) {
            $id = Yii::$app->getRequest()->post('id','');
            $cnt=PgLensType::todelete($id);
            die(json_encode(["code"=>0]));
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
}