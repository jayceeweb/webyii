<?php
namespace app\modules\manager\controllers\magento\coupon;

use Yii;
use app\modules\manager\controllers\BaseController;
use app\components\helper\AuthcodeHelper;

class ListController extends BaseController
{
    public $layout = "emptylayout";

    public function search_coupon_data($condition) {
        if (!empty($condition["coupon"]) || $condition["coupon"]!=0) {
            $sql=sprintf("SELECT entity_id,created_at,updated_at,is_active,customer_id,customer_email,customer_firstname,customer_lastname,customer_gender,items_count,items_qty,grand_total,reserved_order_id,coupon_code,subtotal,has_warranty,warranty,'quote'
FROM `quote`
WHERE coupon_code = '%s'",$condition["coupon"]);
            $quote_list = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();

            if($condition["end_order_time"]){
                $sql=sprintf("SELECT entity_id,state,coupon_code,shipping_description,customer_id,discount_amount,grand_total,shipping_amount,subtotal,total_qty_ordered,subtotal_refunded,total_paid,total_canceled,total_qty_ordered,total_refunded,quote_id,increment_id,customer_email,customer_firstname,customer_lastname,customer_gender,shipping_method,total_item_count,has_warranty,warranty,'so'
FROM sales_order WHERE coupon_code = '%s'  and created_at >= '%s' and created_at <= '%s'",$condition["coupon"],$condition["start_order_time"],$condition["end_order_time"]);
            }else{
                $sql=sprintf("SELECT entity_id,state,coupon_code,shipping_description,customer_id,discount_amount,grand_total,shipping_amount,subtotal,total_qty_ordered,subtotal_refunded,total_paid,total_canceled,total_qty_ordered,total_refunded,quote_id,increment_id,customer_email,customer_firstname,customer_lastname,customer_gender,shipping_method,total_item_count,has_warranty,warranty,'so'
FROM sales_order WHERE coupon_code = '%s'  and created_at >= '%s'",$condition["coupon"],$condition["start_order_time"]);
            }
            $so_list = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();

            $sql=sprintf("SELECT salesrule.`name`,salesrule.`from_date`,salesrule.`to_date`,salesrule.`is_active`,salesrule.`discount_amount`,salesrule.`discount_qty`,salesrule.`times_used` ,salesrule_coupon.`coupon_id`,salesrule_coupon.`code`,salesrule_coupon.`created_at`,salesrule_coupon.`expiration_date`
FROM `salesrule_coupon` 
LEFT JOIN `salesrule` ON salesrule.`rule_id`=salesrule_coupon.`rule_id`
WHERE salesrule_coupon.`code`= '%s'",$condition["coupon"]);
            $coupons = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $coupon_list=[];
            foreach ($coupons as $k=>$coupon) {
                $sql=sprintf("select count(0) as cnt from sales_order where coupon_code='%s'",$coupon["code"]);
                $cnt=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
                $coupon["order_times_used"]=$cnt;
                $coupon_list[]=$coupon;
            }
        } else {
            $quote_list=[];
            $so_list=[];
            $coupon_list=[];
        }
        $ret=["quote_list"=>$quote_list,"so_list"=>$so_list,"coupon_list"=>$coupon_list];
        return $ret;
    }

    public function actionIndex() {
        $condition=[];
        $coupon=Yii::$app->getRequest()->get('coupon','');
        $start_order_time=Yii::$app->getRequest()->get('start_order_time','');
        $end_order_time=Yii::$app->getRequest()->get('end_order_time','');
        $condition["coupon"]=$coupon;
        $condition["start_order_time"]=$start_order_time;
        $condition["end_order_time"]=$end_order_time;
        $data=$this->search_coupon_data($condition);
        $d=[
            'condition'=>$condition,
            'data'=>$data,
        ];
        return $this->render('index',$d);
    }

    public function get_sales_order_item($order_id) {
        $sql=sprintf("SELECT item_id,`name`,sku,qty_ordered,qty_refunded,price,discount_amount,row_total,glasses_prescription_id,profile_id,profile_prescription_id,has_warranty,warranty 
FROM sales_order_item WHERE order_id=%s and parent_item_id is null",$order_id);
        $sales_order_item = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $sales_order_item;
    }

    public function actionViewso() {
        $id=AuthcodeHelper::decode_ex(Yii::$app->getRequest()->get('id',''));
        $rows=$this->get_sales_order_item($id);
        $d=[
            "rows"=>$rows,
        ];
        return $this->render('viewso',$d);
    }

    public function get_quote_item($quote_id) {
        $sql=sprintf("SELECT item_id,product_id,sku,`name`,qty,price,row_total,has_warranty,warranty FROM quote_item WHERE quote_id=%s and parent_item_id is null",$quote_id);
        $sales_order_item = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $sales_order_item;
    }

    public function actionViewquote() {
        $id=AuthcodeHelper::decode_ex(Yii::$app->getRequest()->get('id',''));
        $rows=$this->get_quote_item($id);
        $d=[
            "rows"=>$rows,
        ];
        return $this->render('viewquote',$d);
    }

    public function actionCouponextend() {
        $coupon=Yii::$app->getRequest()->post('coupon','');
        if (!empty($coupon)) {
            $expiration_date=Yii::$container->get("app\components\helper\TimezoneHelper")->getDate();
            $expiration_date=date("Y-m-d", strtotime("+30 days", strtotime($expiration_date)));
            $sql=sprintf("UPDATE salesrule_coupon SET expiration_date='%s' where code='%s'",$expiration_date,$coupon);
            Yii::$app->db->createCommand($sql)->execute();
            $sql=sprintf("UPDATE salesrule SET to_date='%s',is_active=1 WHERE rule_id=(SELECT rule_id FROM salesrule_coupon WHERE `code`='%s')",$expiration_date,$coupon);
            Yii::$app->db->createCommand($sql)->execute();
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0]));
        }
    }
}