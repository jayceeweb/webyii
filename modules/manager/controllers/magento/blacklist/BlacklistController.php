<?php
namespace app\modules\manager\controllers\magento\blacklist;

use Yii;
use app\modules\manager\controllers\BaseController;
use app\models\PgBlacklist;
use yii\data\Pagination;

class BlacklistController extends BaseController
{
    public $layout = "emptylayout";

    public function actionIndex() {
        if (Yii::$app->admin->can('blacklistManager')) {
            $condition=[];
            $email=Yii::$app->getRequest()->get('email','');
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition["email"]=$email;
            $cnt=PgBlacklist::getListCount($condition);
            $rows=PgBlacklist::getList($page,$perpage,$condition);
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ])
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionRemove() {
        if (Yii::$app->admin->can('blacklistManager')) {
            $id=Yii::$app->getRequest()->post('id');
            $ret=PgBlacklist::deleteAll("id=:id",[":id"=>$id]);
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0,"ret"=>$ret]));
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionAdd() {
        if (Yii::$app->admin->can('blacklistManager')) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $start_at=Yii::$container->get("app\components\helper\TimezoneHelper")->getDate();
            $end_at=date("Y-m-d", strtotime("+60 days", strtotime($start_at)));
            return $this->render("add",[
                'start_at'=>$start_at,
                'end_at'=>$end_at,
            ]);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionBlacklistadd(){
        if (Yii::$app->admin->can('blacklistManager')) {
            $type_id=Yii::$app->getRequest()->post('type_id');
            $email=Yii::$app->getRequest()->post('email');
            $start_at=Yii::$app->getRequest()->post('start_at');
            $end_at=Yii::$app->getRequest()->post('end_at');
            $reason=Yii::$app->getRequest()->post('reason');
            $d=new PgBlacklist();
            $d->type_id=$type_id;
            $d->email=$email;
            $d->start_at=$start_at;
            $d->end_at=$end_at;
            $d->created_reason=$reason;
            $d->save();
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0]));
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
}