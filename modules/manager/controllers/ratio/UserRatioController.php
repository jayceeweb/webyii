<?php
namespace app\modules\manager\controllers\ratio;

use app\models\CustomerEntity;
use app\models\PgLensType;
use app\models\CustomerInsurance;
use app\models\PgScoreRule;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class UserRatioController extends BaseController{
    public function actionIndex() {
        if (Yii::$app->admin->can('user_management')) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $biz_type=Yii::$app->getRequest()->get('biz_type','');
            $condition["biz_type"]=$biz_type;
            $biz_connect=Yii::$app->getRequest()->get('biz_connect','');
            $condition["biz_connect"]=$biz_connect;
            $cart_ratio_start=Yii::$app->getRequest()->get('cart_ratio_start','');
            $cart_ratio_end=Yii::$app->getRequest()->get('cart_ratio_end','');
            $condition["cart_ratio_start"]=$cart_ratio_start;
            $condition["cart_ratio_end"]=$cart_ratio_end;
            $freight_ratio_start=Yii::$app->getRequest()->get('freight_ratio_start','');
            $freight_ratio_end=Yii::$app->getRequest()->get('freight_ratio_end','');
            $condition["freight_ratio_start"]=$freight_ratio_start;
            $condition["freight_ratio_end"]=$freight_ratio_end;
            $compel=Yii::$app->getRequest()->get('compel','');
            $condition["compel"]=$compel;
            $rows=CustomerInsurance::getList($page,$perpage,$condition);
            $cnt=CustomerInsurance::getListCount($condition);
            $type=CustomerInsurance::getListType();
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'type'=>$type,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    public function actionAdd(){
        if (Yii::$app->admin->can('user_management')) {
            $id=Yii::$app->getRequest()->get('id','');
            $biz_type=Yii::$app->getRequest()->get('biz_type','');
            $biz_connect=Yii::$app->getRequest()->get('biz_connect','');
            $cart_ratio=Yii::$app->getRequest()->get('cart_ratio','');
            $freight_ratio=Yii::$app->getRequest()->get('freight_ratio','');
            $compel=Yii::$app->getRequest()->get('compel','');
            $d=[
                "id"=>$id,
                "biz_type"=>$biz_type,
                "biz_connect"=>$biz_connect,
                "cart_ratio"=>$cart_ratio,
                "freight_ratio"=>$freight_ratio,
                "compel"=>$compel,
            ];
            return $this->render('add',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    public function actionSave(){
        if (Yii::$app->admin->can('user_management')) {
            $id=Yii::$app->getRequest()->post('id','');
            $biz_type=Yii::$app->getRequest()->post('biz_type','');
            $biz_connect=Yii::$app->getRequest()->post('biz_connect','');
            $cart_ratio=Yii::$app->getRequest()->post('cart_ratio','');
            $freight_ratio=Yii::$app->getRequest()->post('freight_ratio','');
            $compel=Yii::$app->getRequest()->post('compel','');
            $d=[
                "id"=>$id,
                "biz_type"=>$biz_type,
                "biz_connect"=>$biz_connect,
                "cart_ratio"=>$cart_ratio,
                "freight_ratio"=>$freight_ratio,
                "compel"=>$compel,
            ];
            if($id){
                CustomerInsurance::updateRatio($d);
            }else{
                unset($d["id"]);
                CustomerInsurance::addRatio($d);
            }
            die(json_encode(["code"=>0,"ret"=>$d]));
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    public function actionDeleteRatio(){
        $id=Yii::$app->getRequest()->post('id','');
        CustomerInsurance::deleteRatio($id);
    }
}