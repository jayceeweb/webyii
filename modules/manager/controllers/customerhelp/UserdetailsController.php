<?php
namespace app\modules\manager\controllers\customerhelp;

use app\models\PgCouponCodeInfo;
use app\models\PgOrganization;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class UserdetailsController extends BaseController{
    public function actionIndex() {
        return $this->render('index');
    }
}