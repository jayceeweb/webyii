<?php
namespace app\modules\manager\controllers\customerhelp;

use app\models\PgCouponCodeInfo;
use app\models\PgOrganization;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class UpdatepasswordController extends BaseController{
    public function actionIndex() {
        if (Yii::$app->admin->can('password')) {
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition=[];
            $condition['email'] = Yii::$app->getRequest()->get('email','');
            $rows=$this->getList($page,$perpage,$condition);
            $cnt=$this->getListCount($condition);
            $data=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$data);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    //孙进超 21/1/26
    public function getListSQL() {
        $sql="select cop.entity_id,cop.email,cop.old_password_hash,cop.new_password_hash,cop.new_password,cop.created_at,cop.updated_at,cop.author_name,cop.update_name,`enable` from customer_old_password as cop";
        return $sql;
    }

    //获取用户密码记录数据并分页
    public function getList($page=0,$pagecount=10,$condition=[]) {
        $sql=$this->getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"])
            ->offset(($page -1) * $pagecount)->limit($pagecount);
        $query->andWhere(sprintf("enable = 1"));
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'email'){
                    $query->andWhere(sprintf("email = :%s",$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $data=$query->all();
        return $data;
    }
    //获取用户密码记录数据数量
    public function getListCount($condition=[]) {
        $sql=$this->getListSQL();
        $query = (new \yii\db\Query())->select('*')->from(['main' => "(".$sql.")"]);
        $query->andWhere(sprintf("enable = 1"));
        $params=[];
        foreach ($condition as $c=>$v) {
            if($v!==''){
                if ($c == 'email'){
                    $query->andWhere(sprintf("email = :%s",$c));
                    $params[":".$c]=$v;
                }
            }
        }
        $query->addParams($params);
        $data=$query->count();
        return $data;
    }
    public function actionUpdatePwd(){
        $email=Yii::$app->getRequest()->post('email','');
        $password=Yii::$app->getRequest()->post('password','');
        $transaction = Yii::$app->getDb()->beginTransaction();
        try{
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerRepository = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
            $customerRegistry = $objectManager->get('\Magento\Customer\Model\CustomerRegistry');
            $createPasswordHash = $objectManager->get('\Magento\Customer\Model\AccountManagement');
            $customer = $customerRepository->get($email);
            $customerSecure = $customerRegistry->retrieveSecureData($customer->getId());
            $old_password = $customerSecure->getPasswordHash();
            $new_password = $password;
            $customerSecure->setPasswordHash($createPasswordHash->getPasswordHashs($password));
            $password = $customerSecure->getPasswordHash();
            $sql = "UPDATE customer_entity SET password_hash = '$password' where email = '$email'";
            Yii::$app->db->createCommand($sql)->execute();
            $loginname = Yii::$app->admin->getIdentity()->getAttribute('username');
            $sql = "INSERT INTO customer_old_password (email,old_password_hash,new_password_hash,author_name,new_password) VALUES ('{$email}','{$old_password}','{$password}','{$loginname}','{$new_password}')";
            Yii::$app->db->createCommand($sql)->execute();
            $transaction->commit();
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array())));
        }catch (\Exception $e){
            $transaction->rollBack();
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    public function actionResetPwd(){
        $entity_id=Yii::$app->getRequest()->post('entity_id','');
        $transaction = Yii::$app->getDb()->beginTransaction();
        try{
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerRepository = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
            $customerRegistry = $objectManager->get('\Magento\Customer\Model\CustomerRegistry');
            $createPasswordHash = $objectManager->get('\Magento\Customer\Model\AccountManagement');
            $sql = "select old_password_hash,email from customer_old_password where entity_id = $entity_id";
            $customer_old_password_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $old_password_hash = $customer_old_password_data['old_password_hash'];
            $email = $customer_old_password_data['email'];
            $sql = "UPDATE customer_entity SET password_hash = '$old_password_hash' where email = '$email'";
            Yii::$app->db->createCommand($sql)->execute();
            $updatename = Yii::$app->admin->getIdentity()->getAttribute('username');
            $sql = "UPDATE customer_old_password SET update_name = '{$updatename}', `enable` = 0 where entity_id = $entity_id";
            Yii::$app->db->createCommand($sql)->execute();
            $transaction->commit();
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array())));
        }catch (\Exception $e){
            $transaction->rollBack();
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
}