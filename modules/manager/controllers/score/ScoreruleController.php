<?php
namespace app\modules\manager\controllers\score;

use app\models\PgScoreRule;
use app\models\PgScoreRuleDetail;
use Yii;
use app\modules\manager\controllers\BaseController;
use app\components\helper\AuthcodeHelper;
use yii\data\Pagination;
use app\models\CustomerGroup;
use app\models\EavAttributeSet;

class ScoreruleController extends BaseController
{
    public function actionIndex() {
        if (Yii::$app->admin->can('scoreManager')) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();

            $condition=[];
            $name=Yii::$app->getRequest()->get('name','');
            $sku=Yii::$app->getRequest()->get('sku','');
            $type_id=Yii::$app->getRequest()->get('type_id','');
            $attribute_set_id=Yii::$app->getRequest()->get('attribute_set_id','');
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',50);
            $condition["name"]=$name;
            $condition["sku"]=$sku;
            $condition["type_id"]=$type_id;
            $condition["attribute_set_id"]=$attribute_set_id;
            $cnt=PgScoreRule::getListCount($condition);
            $rows=PgScoreRule::getList($page,$perpage,$condition);
            foreach ($rows as $k=>$row) {
                $row["user_groups"]=PgScoreRuleDetail::getRuleDetailBySkuId($row["entity_id"]);
                $rows[$k]=$row;
            }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $ACCESSORIES_ATTRIBUTE_SET_IDS = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["ACCESSORIES_ATTRIBUTE_SET_IDS"];
            $GLASSES_ATTRIBUTE_SET_ID = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["GLASSES_ATTRIBUTE_SET_ID"];
            $GOGGLES_ATTRIBUTE_SET_ID = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["GOGGLES_ATTRIBUTE_SET_ID"];
            $EAV_ATTRIBUTE_SETS=EavAttributeSet::getAllAttributeSets();
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
                'ACCESSORIES_ATTRIBUTE_SET_IDS'=>$ACCESSORIES_ATTRIBUTE_SET_IDS,
                'GLASSES_ATTRIBUTE_SET_ID'=>$GLASSES_ATTRIBUTE_SET_ID,
                'GOGGLES_ATTRIBUTE_SET_ID'=>$GOGGLES_ATTRIBUTE_SET_ID,
                "base_url"=>Yii::$container->get("app\models\CoreConfigData")->getBaseUrl(),
                "static_url"=>Yii::$container->get("app\models\CoreConfigData")->getStaticUrl(),
                "EAV_ATTRIBUTE_SETS"=>$EAV_ATTRIBUTE_SETS,
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionChangeDetailScore() {
        if (Yii::$app->admin->can('scoreManager')) {
            $item_id=Yii::$app->getRequest()->post('item_id',0);
            $field_name=Yii::$app->getRequest()->post('field_name',0);
            $value=Yii::$app->getRequest()->post('value',0);
            if (!empty($item_id)) {
                $detail=PgScoreRuleDetail::findOne(['item_id'=>$item_id]);
                $detail->$field_name=$value;
                $detail->save();
                header("Content-type:application/json;charset=utf-8");
                die(json_encode(["code"=>0,"ret"=>$detail->getAttributes()]));
            } else {
                header("Content-type:application/json;charset=utf-8");
                die(json_encode(["code"=>0,"ret"=>""]));
            }
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
}