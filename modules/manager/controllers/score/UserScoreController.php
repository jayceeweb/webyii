<?php
namespace app\modules\manager\controllers\score;

use app\models\PgScore;
use app\models\PgScoreBill;
use app\models\PgScoreRule;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;

class UserScoreController extends BaseController
{
    public function actionIndex() {
        if (Yii::$app->admin->can('scoreManager')) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $condition=[];
            $email=Yii::$app->getRequest()->get('email','');
            $confirm_email_confirm=Yii::$app->getRequest()->get('confirm_email_confirm','');
            $recommended=Yii::$app->getRequest()->get('recommended','');
            $permissions=Yii::$app->getRequest()->get('permissions','');
            $enabled_score=Yii::$app->getRequest()->get('enabled_score','');
            $discount_amount=Yii::$app->getRequest()->get('discount_amount','');
            $recommendstart_time=Yii::$app->getRequest()->get('recommendstart_time','');
            $is_clone=Yii::$app->getRequest()->get('is_clone','');
            $recommendstart_timeto = '';
            if($recommendstart_time){
                $recommendstart_timeto = str_replace("T"," ","$recommendstart_time");
                $recommendstart_timeto .= ':00';
            }
            $recommendend_time=Yii::$app->getRequest()->get('recommendend_time','');
            $recommendend_timeto='';
            if($recommendend_time){
                $recommendend_timeto = str_replace("T"," ","$recommendend_time");
                $recommendend_timeto .= ':00';
            }
            $confirmstart_time=Yii::$app->getRequest()->get('confirmstart_time','');
            $condition["confirmstart_time"]=$confirmstart_time;
            $confirmstart_timeto='';
            if($confirmstart_time){
                $confirmstart_timeto = str_replace("T"," ","$confirmstart_time");
                $confirmstart_timeto .= ':00';
            }
            $confirmend_time=Yii::$app->getRequest()->get('confirmend_time','');
            $confirmend_timeto='';
            if($confirmend_time){
                $confirmend_timeto = str_replace("T"," ","$confirmend_time");
                $confirmend_timeto .= ':00';
            }
            $discountamountstart_time=Yii::$app->getRequest()->get('discountamountstart_time','');
            $discountamountstart_timeto='';
            if($discountamountstart_time){
                $discountamountstart_timeto = str_replace("T"," ","$discountamountstart_time");
                $discountamountstart_timeto .= ':00';
            }
            $discountamountend_time=Yii::$app->getRequest()->get('discountamountend_time','');
            $discountamountend_timeto='';
            if($discountamountend_time){
                $discountamountend_timeto = str_replace("T"," ","$discountamountend_time");
                $discountamountend_timeto .= ':00';
            }
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition["email"]=$email;
            $condition["confirm_email_confirm"]=$confirm_email_confirm;
            $condition["recommended"]=$recommended;
            $condition["permissions"]=$permissions;
            $condition["enabled_score"]=$enabled_score;
            $condition["discount_amount"]=$discount_amount;
            $condition["recommendstart_time"]=$recommendstart_timeto;
            $condition["recommendend_time"]=$recommendend_timeto;
            $condition["confirmstart_time"]=$confirmstart_timeto;
            $condition["confirmend_time"]=$confirmend_timeto;
            $condition["discountamountstart_time"]=$discountamountstart_timeto;
            $condition["discountamountend_time"]=$discountamountend_timeto;
            $condition["is_clone"]=$is_clone;
            $cnt=PgScore::getListCount($condition);
            $cnts=PgScore::Count();
            $rows=PgScore::getList($page,$perpage,$condition);
            $condition["recommendstart_time"]=$recommendstart_time;
            $condition["recommendend_time"]=$recommendend_time;
            $condition["confirmstart_time"]=$confirmstart_time;
            $condition["confirmend_time"]=$confirmend_time;
            $condition["discountamountstart_time"]=$discountamountstart_time;
            $condition["discountamountend_time"]=$discountamountend_time;
            $d=[
                'condition'=>$condition,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'rows_cnts'=>$cnts,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];

            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }


    public function actionAdd() {
        if (Yii::$app->admin->can('scoreManager')) {
            $email=Yii::$app->getRequest()->get('email','0');
            $src_type=Yii::$app->getRequest()->get('src_type','');
            $src_id=Yii::$app->getRequest()->get('src_id','');
            $operation=Yii::$app->getRequest()->get('operation','');
            $score_status=Yii::$app->getRequest()->get('score_status','');
            $score=Yii::$app->getRequest()->get('score','');
            $d=[
                "email"=>$email,
                "src_type"=>$src_type,
                "src_id"=>$src_id,
                "operation"=>$operation,
                "score_status"=>$score_status,
                "score"=>$score,
            ];
            return $this->render('add',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionSave() {
        if (Yii::$app->admin->can('scoreManager')) {
            $email=Yii::$app->getRequest()->post('email','0');
            $customer_id = '';
            if($email){
               $sql = "select * from customer_entity where email ='$email'";
               $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
               $customer_id = $data['entity_id'];
            }
            $src_type=Yii::$app->getRequest()->post('src_type','');
            $src_id=Yii::$app->getRequest()->post('src_id','');
            $operation=Yii::$app->getRequest()->post('operation','');
            $score_status=Yii::$app->getRequest()->post('score_status','');
            $score=Yii::$app->getRequest()->post('score','');
            $pgScoreBill=new PgScoreBill();
            $pgScoreBill->setAttribute("customer_id",$customer_id);
            $pgScoreBill->setAttribute("src_type",$src_type);
            if ($src_type=="customer_entity") {
                if ($src_id=="") {
                    $src_id=$customer_id;
                }
            }else if($src_type=="sales_order"){
                if($src_id){
                    $sql = "select * from sales_order where increment_id = '$src_id'";
                    $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                    $src_id = $data['entity_id'];
                }
            }
            if ($src_id=="") {
                $src_id=0;
            }
            $pgScoreBill->setAttribute("src_id",$src_id);
            $pgScoreBill->setAttribute("operation",$operation);
            $pgScoreBill->setAttribute("score_status",$score_status);
            if ($operation==0) {
                $pgScoreBill->score=abs($score);
            } else if ($operation==1) {
                $pgScoreBill->score=-abs($score);
            }
            $pgScoreBill->save();
            if (!empty($pgScoreBill->id) && $pgScoreBill->score_status==1) {
                //保存成功且状态为生效
                $ret=PgScoreBill::scoreActive($pgScoreBill->id);
            }
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0,"ret"=>$pgScoreBill->getAttributes()]));
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    /**
     * 用户积分重新初始化
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionPointInit() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $entity_id=Yii::$app->request->post("entity_id");

        if (!empty($entity_id)) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection();
            //清空bill表
            $sql=sprintf("delete from pg_score_bill where customer_id=%s",$entity_id);
            $connection->query($sql);
            //重新计算
            $sql=sprintf("SELECT entity_id,quote_id,customer_id,increment_id FROM sales_order 
WHERE (customer_id is not null) AND (is_clone IS NULL OR is_clone=0) 
AND (is_php IS NULL OR is_php=0) 
AND (customer_id=%s)
#and entity_id=2059
AND created_at > '2020-01-01'",$entity_id);
            $rows=$connection->fetchAll($sql);
            $i_index=0;
            foreach ($rows as $row) {
                $i_index=$i_index+1;
                $entity_id=$row["entity_id"];
                $quote_id=$row["quote_id"];
                $customer_id=$row["customer_id"];
                $increment_id=$row["increment_id"];
                $log=sprintf("共%s条记录，当前第%s条,entity_id=%s,quote_id=%s,customer_id=%s,increment_id=%s",count($rows),$i_index,$entity_id,$quote_id,$customer_id,$increment_id).PHP_EOL;
                $quote=null;
                $order=$objectManager->get("\Magento\Sales\Api\OrderRepositoryInterface")->get($entity_id);
                if (!empty($quote_id)) {
                    $quote=$objectManager->get("\Magento\Quote\Model\Quote")->load($quote_id);
                }
                $bill=$objectManager->get("Pg\CatalogExtend\Helper\PriceScoreHelper")->purcacheScore($quote,$order);
                $log=sprintf("entity_id=%s,total=%s",$entity_id,$bill->getAttribute("score")).PHP_EOL;
            }

            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0]));
        } else {
            die(json_encode(["code"=>-1]));
        }
    }

    /**
     * 启用用户积分
     */
    public function actionPointEnable() {
        $entity_id=Yii::$app->request->post("entity_id");
        $enabled=Yii::$app->request->post("enabled");
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $now=$localeDate->date()->format('Y-m-d H:i:s');
        if (!empty($entity_id)) {
            $enable_sql=sprintf("update customer_entity set enabled_score=%s,enabled_score_time='$now' where entity_id=%s",$enabled,$entity_id);
            Yii::$app->db->createCommand($enable_sql)->execute();
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0]));
        } else {
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>-1]));
        }
    }
    /**
     * 用户积分矫正
     */
    public function actionRevise(){
        $customer_id=Yii::$app->request->post("entity_id");
        if($customer_id){
            $cnts=PgScore::revise($customer_id);
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0]));
        } else {
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>-1]));
        }
    }
    public function actionRecommend(){
        $customer_id=Yii::$app->request->post("entity_id");
        if($customer_id){
            $cnts=PgScore::recommend($customer_id);
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0]));
        } else {
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>-1]));
        }
    }
}