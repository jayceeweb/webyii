<?php
namespace app\modules\manager\controllers\score;

use app\models\CustomerEntity;
use app\models\PgScore;
use app\models\PgScoreBill;
use app\models\PgScoreRule;
use app\modules\webyiiapi\controllers\SendController;
use app\modules\webyiiapi\Webyiiapi;
use Yii;
use app\modules\manager\controllers\BaseController;
use yii\data\Pagination;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ScoreController extends BaseController
{
    public function actionIndex() {
        if (Yii::$app->admin->can('scoreManager')) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $condition=[];
            $email=Yii::$app->getRequest()->get('email','');
            $increment_id=Yii::$app->getRequest()->get('increment_id','');
            $discount_amount=Yii::$app->getRequest()->get('discount_amount','');
            $is_remake=Yii::$app->getRequest()->get('is_remake','');
            $remake_order_number=Yii::$app->getRequest()->get('remake_order_number','');
            $review=Yii::$app->getRequest()->get('review','');
            $score_status=Yii::$app->getRequest()->get('qty_score_status','');
            $active_start_time=Yii::$app->getRequest()->get('active_start_time','');
            $active_end_time=Yii::$app->getRequest()->get('active_end_time','');
            $start_bill_time=Yii::$app->getRequest()->get('start_bill_time','');
            $end_bill_time=Yii::$app->getRequest()->get('end_bill_time','');
            $is_delievered=Yii::$app->getRequest()->get('is_delievered','');
            $start_delievered_time=Yii::$app->getRequest()->get('start_delievered_time','');
            $end_delievered_time=Yii::$app->getRequest()->get('end_delievered_time','');
            $is_recommend=Yii::$app->getRequest()->get('is_recommend','');
            $is_email_confirm=Yii::$app->getRequest()->get('is_email_confirm','');
            $is_enabled_score=Yii::$app->getRequest()->get('is_enabled_score','');
            $has_coupon_code=Yii::$app->getRequest()->get('has_coupon_code','');
            $coupon_code=Yii::$app->getRequest()->get('coupon_code','');
            $is_deleted=Yii::$app->getRequest()->get('is_deleted','');
            $has_warranty=Yii::$app->getRequest()->get('has_warranty','');
            $src_id = '';
            if($increment_id){
                $sql = "select * from sales_order where increment_id = '$increment_id'";
                $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                $src_id = $data['entity_id'];
            }
            $src_type=Yii::$app->getRequest()->get('src_type','');
            $page=Yii::$app->getRequest()->get('page',1);
            $perpage=Yii::$app->getRequest()->get('per-page',10);
            $condition["email"]=$email;
            $condition["discount_amount"]=$discount_amount;
            $condition["is_remake"]=$is_remake;
            $condition["remake_order_number"]=$remake_order_number;
            $condition["review"]=$review;
            $condition["src_id"]=$src_id;
            $condition["src_type"]=$src_type;
            $condition["score_status"]=$score_status;
            $condition["active_start_time"]=$active_start_time;
            $condition["active_end_time"]=$active_end_time;
            $condition["start_bill_time"]=$start_bill_time;
            $condition["end_bill_time"]=$end_bill_time;
            $condition["is_delievered"]=$is_delievered;
            $condition["start_delievered_time"]=$start_delievered_time;
            $condition["end_delievered_time"]=$end_delievered_time;
            $condition["is_recommend"]=$is_recommend;
            $condition["is_email_confirm"]=$is_email_confirm;
            $condition["is_enabled_score"]=$is_enabled_score;
            $condition["has_coupon_code"]=$has_coupon_code;
            $condition["coupon_code"]=$coupon_code;
            $condition["is_deleted"]=$is_deleted;
            $condition["has_warranty"]=$has_warranty;
            $cnt=PgScoreBill::getListCount($condition);
            $rows=PgScoreBill::getList($page,$perpage,$condition);
            foreach ($rows as $k=>$row) {
                $obj=PgScoreBill::getSrcObject(["src_type"=>$row["src_type"],"src_id"=>$row["src_id"]]);
                $rows[$k]["src_obj"]=$obj;
            }
            foreach ($rows as $k=>$row) {
                $obj=PgScoreBill::getReObject($row["src_id"]);
                $rows[$k]["re_obj"]=$obj;
            }
            $condition["increment_id"]=$increment_id;
            $type = PgScoreBill::getSrcType();
            $d=[
                'condition'=>$condition,
                'type'=>$type,
                'rows'=>$rows,
                'rows_cnt'=>$cnt,
                'page_count'=> ceil($cnt/$perpage),
                'page'=>$page,
                'pager'=>new Pagination([
                    'totalCount' => $cnt,
                    'pageSize' => $perpage,
                    'pageSizeParam'=>false,
                    'pageParam'=>'page',
                ]),
            ];
            return $this->render('index',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionItem() {
        if (Yii::$app->admin->can('scoreManager')) {
            $customer_id=Yii::$app->getRequest()->get('item','0');
            $id=Yii::$app->getRequest()->get('id','0');
            $re=Yii::$app->getRequest()->get('re','');
            $review=Yii::$app->getRequest()->get('review','');
            $re_data = array();
            if($re){
                $re = json_decode($re,true);
                foreach ($re as $key => $val){
                    $re_data[$val][] = $key;
                }
            }
            $sql = "select catalog_product_flat_1.special_from_date,catalog_product_flat_1.special_to_date,sales_order_item.product_type,sales_order_item.order_id,sales_order_item.discount_amount,sales_order_item.created_at,sales_order_item.qty_ordered,sales_order_item.item_id,sales_order_item.sku,sales_order_item.`name`,gaving_score from sales_order_item LEFT JOIN catalog_product_flat_1 on sales_order_item.sku =  catalog_product_flat_1.sku where sales_order_item.order_id = $customer_id and sales_order_item.product_type='configurable'";
            $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            foreach ($data as $k => $v){
                $sql = "select catalog_product_flat_1.special_from_date,catalog_product_flat_1.special_to_date,sales_order_item.product_type,sales_order_item.order_id,sales_order_item.discount_amount,sales_order_item.created_at,sales_order_item.qty_ordered,sales_order_item.item_id,sales_order_item.sku,sales_order_item.`name`,gaving_score from sales_order_item LEFT JOIN catalog_product_flat_1 on sales_order_item.sku =  catalog_product_flat_1.sku where sales_order_item.parent_item_id = ".$v['item_id'];
                $datas = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                $data[$k]['discount_amounts'] = 0;
                $data[$k]['re_make'] = 0;
                $data[$k]['remake_order'] = array();
                if($v['discount_amount'] &&$v['discount_amount']!='0.0000'){
                    $data[$k]['discount_amounts'] = 1;
                }
                foreach ($re_data as $ke => $val){
                    if($v['item_id'] == $ke){
                        $data[$k]['re_make'] = 1;
                        $data[$k]['remake_order'] = $val;
                    }
                }
                foreach ($datas as $key => $val){
                    $data[$k]['score_detail'][$key]['sku'] = $val['sku'];
                    $data[$k]['score_detail'][$key]['item_id'] = $val['item_id'];
                    $data[$k]['score_detail'][$key]['val'] = $val['gaving_score']/$val['qty_ordered'];
                    $data[$k]['score_detail'][$key]['special'] = 0;
                    if(!is_null($val['special_from_date']) && !is_null($val['special_to_date'])){
                        if($val['created_at'] >$val['special_from_date'] && $val['created_at']<$val['special_to_date']){
                            $data[$k]['score_detail'][$key]['special'] = 1;
                        }
                    }else if(!is_null($val['special_from_date'])){
                        if($val['created_at'] >$val['special_from_date']){
                            $data[$k]['score_detail'][$key]['special'] = 1;
                        }
                    }else if(!is_null($val['special_to_date'])){
                        if($val['created_at']<$val['special_to_date']){
                            $data[$k]['score_detail'][$key]['special'] = 1;
                        }
                    }
                }
            }
            $d = array("item"=>$data,"id"=>$id,'review'=>$review);
            return $this->render('item',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionAdd() {
        if (Yii::$app->admin->can('scoreManager')) {
            $email=Yii::$app->getRequest()->get('email','0');
            $customer_id=Yii::$app->getRequest()->get('customer_id','');
            $src_type=Yii::$app->getRequest()->get('src_type','');
            $increment_id=Yii::$app->getRequest()->get('increment_id','');
            $operation=Yii::$app->getRequest()->get('operation','');
            $score_status=Yii::$app->getRequest()->get('score_status','');
            $score=Yii::$app->getRequest()->get('score','');
            $d=[
                "email"=>$email,
                "customer_id"=>$customer_id,
                "src_type"=>$src_type,
                "increment_id"=>$increment_id,
                "operation"=>$operation,
                "score_status"=>$score_status,
                "score"=>$score,
            ];
            return $this->render('add',$d);
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    public function actionSave() {
        if (Yii::$app->admin->can('scoreManager')) {
            $customer_email=Yii::$app->getRequest()->post('customer_email','');
            $customer=CustomerEntity::findOne(["email"=>$customer_email]);
            if (!empty($customer)) {
                $customer_id=$customer->entity_id;
            }
            $src_type=Yii::$app->getRequest()->post('src_type','');
            $increment_id=Yii::$app->getRequest()->post('increment_id','');
            $operation=Yii::$app->getRequest()->post('operation','');
            $score_status=Yii::$app->getRequest()->post('score_status','');
            $score=Yii::$app->getRequest()->post('score','');
            $note=Yii::$app->getRequest()->post('note','');
            if (!empty($customer_id) && !empty($src_type) && !empty($score)) {
                $pgScoreBill=new PgScoreBill();
                $pgScoreBill->setAttribute("customer_id",$customer_id);
                $pgScoreBill->setAttribute("src_type",$src_type);
                $src_id = '';
                if ($src_type=="customer_entity") {
                    if (empty($increment_id)) {
                        $src_id=$customer_id;
                    }
                }else if($src_type=="sales_order"){
                    if($increment_id){
                        $sql = "select * from sales_order where increment_id = '$increment_id'";
                        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                        $src_id = $data['entity_id'];
                    }
                }
                if ($src_id=="") {
                    $src_id=0;
                }
                $pgScoreBill->setAttribute("src_id",$src_id);
                $pgScoreBill->setAttribute("operation",$operation);
                $pgScoreBill->setAttribute("score_status",$score_status);
                $pgScoreBill->setAttribute("note",$note);
                if ($operation==0) {
                    $pgScoreBill->score=abs($score);
                } else if ($operation==1) {
                    $pgScoreBill->score=-abs($score);
                }
                $pgScoreBill->save();
                if (!empty($pgScoreBill->id) && $pgScoreBill->score_status==1) {
                    //保存成功且状态为生效
                    $ret=PgScoreBill::scoreActive($pgScoreBill->id);
                }
                header("Content-type:application/json;charset=utf-8");
                if (!empty($pgScoreBill->getAttribute("id"))) {
                    die(json_encode(["code"=>0,"ret"=>$pgScoreBill->getAttributes()]));
                } else {
                    die(json_encode(["code"=>-2,"message"=>"param error"]));
                }
            } else {
                die(json_encode(["code"=>-2,"message"=>"param error"]));
            }

        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }

    /**
     * 积分生效
     */
    public function actionScoreActive() {
        if (Yii::$app->admin->can('scoreManager')) {
            $id=Yii::$app->getRequest()->post('id','');
            PgScoreBill::scoreActive($id);
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>0,"ret"=>""]));
        } else {
            return $this->redirect(["/manager/default/no-permission"]);
        }
    }
    //sunjinchao 测试
    public function actionAsd(){
        $increment_id=$_GET['increment_id'];
        $a = PgScoreBill::SendActiveEmail($increment_id);
        die(json_encode(["code"=>0,"ret"=>"$a"]));
    }

    /**
     * 积分修改
     */
    public function actionUpdateScoreview(){
        $src_id=Yii::$app->getRequest()->get('src_id','0');
        $score=Yii::$app->getRequest()->get('score','0');
        $increment_id=Yii::$app->getRequest()->get('increment_id','0');
        $d=[
            "src_id"=>$src_id,
            "increment_id"=>$increment_id,
            "score"=>$score,
        ];
        return $this->render('updateScoreview',$d);
    }
    /**
     * 积分修改生效
     */
    public function actionUpdateScore(){
        $src_id=Yii::$app->getRequest()->post('src_id','0');
        $old_score=Yii::$app->getRequest()->post('old_score','0');
        $new_score=Yii::$app->getRequest()->post('new_score','0');
        if($old_score != $new_score){
            $note = '原积分：'.$old_score.'修改后：'.$new_score;
            $sql = "update pg_score_bill set score = $new_score,note = '$note' where id = $src_id";
            Yii::$app->db->createCommand($sql)->execute();
        }
        header("Content-type:application/json;charset=utf-8");
        die(json_encode(["code"=>0,"ret"=>""]));
    }
    public function actionReview(){
        $src_id=Yii::$app->getRequest()->post('src_id','0');
        if($src_id != 0){
            $arr = array();
            $arr['review_user'] = Yii::$app->admin->getIdentity()->getAttribute('username');
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
            $now=$localeDate->date()->format('Y-m-d H:i:s');
            $arr['review_datetime'] = $now;
            $arr['review'] = 1;
            $obj = PgScoreBill::updateReview($src_id,$arr);
            die(json_encode(["code"=>0,"ret"=>""]));
        }else{
            die(json_encode(["code"=>-1,"ret"=>""]));
        }
    }
    //sunjinchao 20/4/17 删除积分记录
    public function actionDeleteBill(){
        $id=Yii::$app->getRequest()->post('id','');
        $deletecode=Yii::$app->getRequest()->post('deletecode','');
        if($deletecode == 1){
            $deletecode = 0;
        }else{
            $deletecode = 1;
        }
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager=\Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $now=$localeDate->date()->format('Y-m-d H:i:s');
        PgScoreBill::deleteBill($id,$deletecode,$now);
    }
	//sunjinchao 修改妥投时间
    public function actionUpdateDelievered(){
        $id=Yii::$app->getRequest()->get('id','');
        $delievered_time=Yii::$app->getRequest()->get('delievered_time','');
        $increment_id=Yii::$app->getRequest()->get('increment_id','');
        if($id){
            $d=[
                "id"=>$id,
                "increment_id"=>$increment_id,
                "delievered_time"=>$delievered_time,
            ];
            return $this->render('updateDelievered',$d);
        }
        var_dump($id,$delievered_time);
    }
    public function actionDelieveredSave(){
        $id=Yii::$app->getRequest()->post('ids','');
        $delievered_time=Yii::$app->getRequest()->post('delievered_time','');
        PgScoreBill::updateDelievered($id,$delievered_time);
    }
	// add sunjinchao excal导出当前页面的积分列表
    public function actionData(){
        $condition=Yii::$app->getRequest()->get('condition','');
        $rows=PgScoreBill::getExcelList($condition);
        foreach ($rows as $k=>$row) {
            $obj=PgScoreBill::getSrcObject(["src_type"=>$row["src_type"],"src_id"=>$row["src_id"]]);
            $rows[$k]["src_obj"]=$obj;
        }
        foreach ($rows as $k=>$row) {
            $obj=PgScoreBill::getReObject($row["src_id"]);
            $rows[$k]["re_obj"]=$obj;
        }
        $key_value = "Customer Info,Order Number,Status:Score,Discount,Time,Note,Review,Info";
        $data = array();
        foreach ($rows as $key => $row){
            $row['recommended'] = isset($row['recommended'])?"yes":"no";
            $row['email_confirm'] = isset($row['email_confirm'])?"yes":"no";
            $row['enabled_score'] = isset($row['enabled_score'])?"yes":"no";
            $row['coupon_code'] = isset($row['coupon_code'])?$row["coupon_code"]:"no";
            if($row["score_status"]==1){$status = "active";}else if ($row["score_status"]==0){$status = "pending";}
            if($row["src_type"]=="sales_order" && isset($row["src_obj"]["src_obj"]["discount_amount"])){
                if ($row["src_obj"]["src_obj"]["discount_amount"]==0){
                    $data[$key][3] = "no";
                }else{
                    $data[$key][3] = "yes";
                }
            }
            $data[$key][0] = 'email:'.$row["email"].',group:'.$row["group_id"].',recommended:'.$row["recommended"].',email_confirm:'.$row["email_confirm"].',enabled_score:'.$row["enabled_score"];
            if($row["src_type"]=="sales_order" || $row["src_type"]=="sales_point_for"){
                $data[$key][1] = 'increment_id:'.$row["src_obj"]["src_obj"]["increment_id"].',coupon_code:'.$row["coupon_code"];
            }
            $data[$key][2] = "status:".$status.",score:".$row['score'];
            $data[$key][4] = "Created At:".$row['created_at'].",Bill Time:".$row['bill_time'].",Delievered Time:".$row['delievered_time'];
            $data[$key][5] = "Note:".$row["note"];
            if ($row["review"]==0){
                $review = "no";
            }elseif($row["review"]>0){
                $review = "yes";
            }
            if ($row["is_deleted"]==0){
                $deleted = "no";
            }elseif($row["is_deleted"] == 1){
                $deleted = "yes";
            }
            $data[$key][6] = "Review:".$review.",Review User:".$row['review_user'];
            $data[$key][7] = "Is Delete:".$deleted.",Delete At:".$row['deleted_datetime'];
            $title = "积分列表";
        }
        $this->actionGetscoreExcal($key_value,$data,$title);
    }
//    sunjinchao 详细添加
//    public function actionGetscoreExcal($key_value,$data,$title){
//        ini_set("memory_limit", "2048M");
//        set_time_limit(0);
//        $keys_value = explode(",",$key_value);
//        $abc = range('A','Z');
//        $spreadsheet = new Spreadsheet();
//        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
//            ->setLastModifiedBy('Maarten Balliauw')
//            ->setTitle('Office 2007 XLSX Test Document')
//            ->setSubject('Office 2007 XLSX Test Document')
//            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
//            ->setKeywords('office 2007 openxml php')
//            ->setCategory('Test result file');
//        foreach ($keys_value as $key => $value){
//            $spreadsheet->setActiveSheetIndex(0)
//                ->setCellValue($abc[$key].'1', $value);
//        }
//        $n = 2;
//        foreach ($data as $key => $value){
//            foreach ($value as $k => $v){
//                $spreadsheet->setActiveSheetIndex(0)
//                    ->setCellValue($abc[$k].($n) ,$v);
//            }
//            $n = $n +1;
//        }
//        $spreadsheet->getActiveSheet()->setTitle($title);
//        ob_end_clean();
//        header('Content-Type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment;filename="'.$title.'(' . date('Ymd-His') . ').xls"');
//        header('Cache-Control: max-age=0');
//        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//        $writer->save('php://output');
//        unset($data);
//    }
//  快速添加
    public function actionGetscoreExcal($key_value,$data,$title){
        ini_set("memory_limit", "512M");
        set_time_limit(0);
        $keys_value = array();
        $keys_value[0] = explode(",",$key_value);
        $spreadsheet = new Spreadsheet();
        $arrData = array_merge($keys_value, $data);
        $spreadsheet->getActiveSheet()->fromArray($arrData);
        $spreadsheet->getActiveSheet()->setTitle($title);
        header('Content-Description: File Transfer');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$title.'.csv');
        header('Cache-Control: max-age=0');
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
        $writer->save('php://output');
        exit();
    }
    public function actionSalesOrderData(){
        $condition=Yii::$app->getRequest()->get('condition','');
        $has_warranty = 1;
        $start_bill_time = '';
        $end_bill_time = '';
        if($condition){
            $has_warranty = empty($condition["has_warranty"])?1:$condition["has_warranty"];
            $start_bill_time = empty($condition["start_bill_time"])?'':$condition["start_bill_time"];
            $end_bill_time = empty($condition["end_bill_time"])?'':$condition["end_bill_time"];
        }
        $sql = "select increment_id,created_at,IFNULL(warranty,0) as warranty from sales_order where has_warranty = $has_warranty";
        if($start_bill_time){
            $sql .= ' and created_at >= \''.$start_bill_time.'\'';
        }
        if($end_bill_time){
            $sql .= ' and created_at <= \''.$end_bill_time.'\'';
        }
        $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $excelData = array();
        foreach ($data as $key => $value){
            $excelData[$key][0] = $value['increment_id'];
            $excelData[$key][1] = $value['created_at'];
            $excelData[$key][2] = round($value['warranty'],2);
        }
        $title = "订单保险";
        $key_value = "Increment Id,Created At,Warranty";
        $this->actionGetscoreExcal($key_value,$excelData,$title);
    }
}