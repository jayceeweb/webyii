<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    $this->registerCsrfMetaTags();
    $this->head();
    ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="layui-fluid">
    <div class="layui-row">
        <form class="layui-form">
            <div class="layui-form-item">
                <label for="L_type_id" class="layui-form-label">
                    <span class="x-red">*</span>prescription_id</label>
                <div class="layui-input-inline">
                    <select name="prescription_id" id="prescription_id" lay-filter="test_xs" required="" lay-verify="required">
                        <option value="" selected ></option>
                        <?php foreach ($prescriptions as $key=>$prescription):?>
                            <option value="<?php echo $prescription['entity_id']; ?>"><?php echo $prescription['prescription_name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span></div>
            </div>
            <div class="layui-form-item">
                <label for="L_type_id" class="layui-form-label">
                    <span class="x-red">*</span>frame_sku</label>
                <div class="layui-input-inline">
                    <input class="layui-input"  autocomplete="off" placeholder="Frame SKU" name="frame_sku" id="frame_sku" style="text-transform:uppercase" required="" lay-verify="required">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>
                </div>
            </div>
            <div class="layui-form-item">
                <table class="layui-table layui-form">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>SH - Segment Height</th>
                    </thead>
                    <tbody>
                    <tr><td>OD (Right Eye)</td><td><input class="layui-input"  autocomplete="off" placeholder="" name="right_tonggao" id="right_tonggao" required="" lay-verify="required"></td></tr>
                    <tr><td>OS (Left Eye)</td></td><td><input class="layui-input"  autocomplete="off" placeholder="" name="left_tongkuan" id="left_tongkuan" required="" lay-verify="required"></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="layui-form-item">
                <label for="L_repass" class="layui-form-label"></label>
                <button class="layui-btn" lay-filter="add" lay-submit="" name="btnSave" id="btnSave">Save</button></div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'layer','jquery','laydate'],
        function() {
            $ = layui.jquery;
            var laydate = layui.laydate;
            var form = layui.form;
            var layer = layui.layer;

            //监听提交
            form.on('submit(add)',
                function(data) {
                    //发异步，把数据提交给php
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/shouji/tongju/save']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'prescription_id':$("#prescription_id").val(),
                            'frame_sku':$("#frame_sku").val(),
                            'left_tonggao':$("#left_tonggao").val(),
                            'left_tongkuan':$("#left_tongkuan").val(),
                            'right_tonggao':$("#right_tonggao").val(),
                            'right_tongkuan':$("#right_tongkuan").val()
                        },
                        success: function (result) {
                            layer.alert("Success", {
                                    icon: 6
                                },
                                function() {
                                    //关闭当前frame
                                    xadmin.close();

                                    // 可以对父窗口进行刷新
                                    xadmin.father_reload();
                                }
                            );
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                    return false;
                });

        });</script>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>