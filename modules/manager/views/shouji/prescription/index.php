<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="Refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="email"  placeholder="please input email" autocomplete="off" class="layui-input" value="<?php echo $condition["sku"] ?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-header">
                        <button class="layui-btn" onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/shouji/prescription/add']); ?>',1000,800)"><i class="layui-icon"></i>New</button>
                    </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>name</th>
                                <th>info</th>
                                <th>created_at</th>
                                <th>option</th>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td><?php echo $row["entity_id"] ?></td>
                                    <td><?php echo $row["prescription_name"] ?></td>
                                    <td>
                                        <table class="layui-table layui-form">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><center>SPH(Sphere)</center></th>
                                                <th><center>CYL(Cylinder)</center></th>
                                                <th><center>Axis(Axis)</center></th>
                                                <th><center>ADD(NV-ADD)</center></th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>OD (Right Eye)</td>
                                                <td><?php echo $row["rsph"] ?></td>
                                                <td><?php echo $row["rcyl"] ?></td>
                                                <td><?php echo $row["rax"] ?></td>
                                                <td><?php echo $row["radd"] ?></td>
                                            </tr>
                                            <tr>
                                                <td>OS (Left Eye)</td>
                                                <td><?php echo $row["lsph"] ?></td>
                                                <td><?php echo $row["lcyl"] ?></td>
                                                <td><?php echo $row["lax"] ?></td>
                                                <td><?php echo $row["ladd"] ?></td>
                                            </tr>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <table class="layui-table layui-form">
                                            <thead>
                                            <tr>
                                                <th>OD-Right</th>
                                                <th>OS-Left</th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><?php echo $row["rpd"] ?></td>
                                                <td><?php echo $row["lpd"] ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="layui-table layui-form">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><center>Prism-H</center></th>
                                                <th><center>Base-H</center></th>
                                                <th><center>Prism-V</center></th>
                                                <th><center>Base-V</center></th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>OD (Right Eye)</td>
                                                <td><?php echo $row["rpri"] ?></td>
                                                <td><?php echo $row["rbase"] ?></td>
                                                <td><?php echo $row["rpri_1"] ?></td>
                                                <td><?php echo $row["rbase_1"] ?></td>
                                            </tr>
                                            <tr>
                                                <td>OS (Left Eye)</td>
                                                <td><?php echo $row["lpri"] ?></td>
                                                <td><?php echo $row["lbase"] ?></td>
                                                <td><?php echo $row["lpri_1"] ?></td>
                                                <td><?php echo $row["lbase_1"] ?></td>
                                            </tr>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td><?php echo $row["created_at"] ?></td>
                                    <td class="td-manage">
                                        <a title="delete" onclick="row_del(this,'<?php echo $row["entity_id"] ?>')" href="javascript:;">
                                            <i class="layui-icon">&#xe640;</i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="layui-card-body ">
                        <div class="page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function row_del(obj, id) {
            layer.confirm('Are you confirm to delete？', function (index) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Url::toRoute(['/manager/shouji/prescription/remove']); ?>",
                    data: {'id':id,'_csrf':$('meta[name="csrf-token"]').attr("content")},
                    success: function (result) {
                        $(obj).parents("tr").remove();
                        layer.msg('Success!', {icon: 1, time: 1000});
                    },
                    error: function (e) {
                        layer.msg('Failed!', {icon: 1, time: 1000});
                    }
                });
            });
        }
    </script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>