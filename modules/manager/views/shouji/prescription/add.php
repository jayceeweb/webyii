<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    $this->registerCsrfMetaTags();
    $this->head();
    ?>
    <style>
        .layui-input-inline {
            width:100px;
        }
    </style>
</head>
<body>
<?php $this->beginBody() ?>
<div class="layui-fluid">
    <div class="layui-row">
        <form class="layui-form">
            <div class="layui-form-item">
                <label for="L_type_id" class="layui-form-label">
                    <span class="x-red">*</span>name</label>
                <div class="layui-input-inline">
                    <input class="layui-input"  autocomplete="off" placeholder="please input prescription name" name="prescription_name" id="prescription_name" required="" lay-verify="required">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>
                </div>
            </div>
            <div class="layui-form-item">
                <table class="layui-table layui-form">
                    <thead>
                    <tr>
                        <th></th>
                        <th><center>SPH(Sphere)</center></th>
                        <th><center>CYL(Cylinder)</center></th>
                        <th><center>Axis(Axis)</center></th>
                        <th><center>ADD(NV-ADD)</center></th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>OD (Right Eye)</td>
                        <td align="center">
                            <select name="rsph" id="rsph" lay-filter="test_xs" style="width:100px;">
                                <option value="-16.0">-16.0</option>
                                <option value="-15.75">-15.75</option>
                                <option value="-15.5">-15.5</option>
                                <option value="-15.25">-15.25</option>
                                <option value="-15.0">-15.0</option>
                                <option value="-14.75">-14.75</option>
                                <option value="-14.5">-14.5</option>
                                <option value="-14.25">-14.25</option>
                                <option value="-14.0">-14.0</option>
                                <option value="-13.75">-13.75</option>
                                <option value="-13.5">-13.5</option>
                                <option value="-13.25">-13.25</option>
                                <option value="-13.0">-13.0</option>
                                <option value="-12.75">-12.75</option>
                                <option value="-12.5">-12.5</option>
                                <option value="-12.25">-12.25</option>
                                <option value="-12.0">-12.0</option>
                                <option value="-11.75">-11.75</option>
                                <option value="-11.5">-11.5</option>
                                <option value="-11.25">-11.25</option>
                                <option value="-11.0">-11.0</option>
                                <option value="-10.75">-10.75</option>
                                <option value="-10.5">-10.5</option>
                                <option value="-10.25">-10.25</option>
                                <option value="-10.0">-10.0</option>
                                <option value="-9.75">-9.75</option>
                                <option value="-9.5">-9.5</option>
                                <option value="-9.25">-9.25</option>
                                <option value="-9.0">-9.0</option>
                                <option value="-8.75">-8.75</option>
                                <option value="-8.5">-8.5</option>
                                <option value="-8.25">-8.25</option>
                                <option value="-8.0">-8.0</option>
                                <option value="-7.75">-7.75</option>
                                <option value="-7.5">-7.5</option>
                                <option value="-7.25">-7.25</option>
                                <option value="-7.0">-7.0</option>
                                <option value="-6.75">-6.75</option>
                                <option value="-6.5">-6.5</option>
                                <option value="-6.25">-6.25</option>
                                <option value="-6.0">-6.0</option>
                                <option value="-5.75">-5.75</option>
                                <option value="-5.5">-5.5</option>
                                <option value="-5.25">-5.25</option>
                                <option value="-5.0">-5.0</option>
                                <option value="-4.75">-4.75</option>
                                <option value="-4.5">-4.5</option>
                                <option value="-4.25">-4.25</option>
                                <option value="-4.0">-4.0</option>
                                <option value="-3.75">-3.75</option>
                                <option value="-3.5">-3.5</option>
                                <option value="-3.25">-3.25</option>
                                <option value="-3.0">-3.0</option>
                                <option value="-2.75">-2.75</option>
                                <option value="-2.5">-2.5</option>
                                <option value="-2.25">-2.25</option>
                                <option value="-2.0">-2.0</option>
                                <option value="-1.75">-1.75</option>
                                <option value="-1.5">-1.5</option>
                                <option value="-1.25">-1.25</option>
                                <option value="-1.0">-1.0</option>
                                <option value="-0.75">-0.75</option>
                                <option value="-0.5">-0.5</option>
                                <option value="-0.25">-0.25</option>
                                <option value="0.0" selected="selected">0.0</option>
                                <option value="0.25">0.25</option>
                                <option value="0.5">0.5</option>
                                <option value="0.75">0.75</option>
                                <option value="1.0">1.0</option>
                                <option value="1.25">1.25</option>
                                <option value="1.5">1.5</option>
                                <option value="1.75">1.75</option>
                                <option value="2.0">2.0</option>
                                <option value="2.25">2.25</option>
                                <option value="2.5">2.5</option>
                                <option value="2.75">2.75</option>
                                <option value="3.0">3.0</option>
                                <option value="3.25">3.25</option>
                                <option value="3.5">3.5</option>
                                <option value="3.75">3.75</option>
                                <option value="4.0">4.0</option>
                                <option value="4.25">4.25</option>
                                <option value="4.5">4.5</option>
                                <option value="4.75">4.75</option>
                                <option value="5.0">5.0</option>
                                <option value="5.25">5.25</option>
                                <option value="5.5">5.5</option>
                                <option value="5.75">5.75</option>
                                <option value="6.0">6.0</option>
                                <option value="6.25">6.25</option>
                                <option value="6.5">6.5</option>
                                <option value="6.75">6.75</option>
                                <option value="7.0">7.0</option>
                                <option value="7.25">7.25</option>
                                <option value="7.5">7.5</option>
                                <option value="7.75">7.75</option>
                                <option value="8.0">8.0</option>
                                <option value="8.25">8.25</option>
                                <option value="8.5">8.5</option>
                                <option value="8.75">8.75</option>
                                <option value="9.0">9.0</option>
                                <option value="9.25">9.25</option>
                                <option value="9.5">9.5</option>
                                <option value="9.75">9.75</option>
                                <option value="10.0">10.0</option>
                                <option value="10.25">10.25</option>
                                <option value="10.5">10.5</option>
                                <option value="10.75">10.75</option>
                                <option value="11.0">11.0</option>
                                <option value="11.25">11.25</option>
                                <option value="11.5">11.5</option>
                                <option value="11.75">11.75</option>
                                <option value="12.0">12.0</option>
                                <option value="12.25">12.25</option>
                                <option value="12.5">12.5</option>
                                <option value="12.75">12.75</option>
                                <option value="13.0">13.0</option>
                                <option value="13.25">13.25</option>
                                <option value="13.5">13.5</option>
                                <option value="13.75">13.75</option>
                                <option value="14.0">14.0</option>
                                <option value="14.25">14.25</option>
                                <option value="14.5">14.5</option>
                                <option value="14.75">14.75</option>
                                <option value="15.0">15.0</option>
                                <option value="15.25">15.25</option>
                                <option value="15.5">15.5</option>
                                <option value="15.75">15.75</option>
                                <option value="16.0">16.0</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="rcyl" id="rcyl" lay-filter="test_xs" style="width:100px;">
                            <option value="-6.0">-6.0</option>
                            <option value="-5.75">-5.75</option>
                            <option value="-5.5">-5.5</option>
                            <option value="-5.25">-5.25</option>
                            <option value="-5.0">-5.0</option>
                            <option value="-4.75">-4.75</option>
                            <option value="-4.5">-4.5</option>
                            <option value="-4.25">-4.25</option>
                            <option value="-4.0">-4.0</option>
                            <option value="-3.75">-3.75</option>
                            <option value="-3.5">-3.5</option>
                            <option value="-3.25">-3.25</option>
                            <option value="-3.0">-3.0</option>
                            <option value="-2.75">-2.75</option>
                            <option value="-2.5">-2.5</option>
                            <option value="-2.25">-2.25</option>
                            <option value="-2.0">-2.0</option>
                            <option value="-1.75">-1.75</option>
                            <option value="-1.5">-1.5</option>
                            <option value="-1.25">-1.25</option>
                            <option value="-1.0">-1.0</option>
                            <option value="-0.75">-0.75</option>
                            <option value="-0.5">-0.5</option>
                            <option value="-0.25">-0.25</option>
                            <option value="0.0" selected="selected">0.0</option>
                            <option value="0.25">0.25</option>
                            <option value="0.5">0.5</option>
                            <option value="0.75">0.75</option>
                            <option value="1.0">1.0</option>
                            <option value="1.25">1.25</option>
                            <option value="1.5">1.5</option>
                            <option value="1.75">1.75</option>
                            <option value="2.0">2.0</option>
                            <option value="2.25">2.25</option>
                            <option value="2.5">2.5</option>
                            <option value="2.75">2.75</option>
                            <option value="3.0">3.0</option>
                            <option value="3.25">3.25</option>
                            <option value="3.5">3.5</option>
                            <option value="3.75">3.75</option>
                            <option value="4.0">4.0</option>
                            <option value="4.25">4.25</option>
                            <option value="4.5">4.5</option>
                            <option value="4.75">4.75</option>
                            <option value="5.0">5.0</option>
                            <option value="5.25">5.25</option>
                            <option value="5.5">5.5</option>
                            <option value="5.75">5.75</option>
                            <option value="6.0">6.0</option>
                        </select>
                        </td>
                        <td align="center">
                            <select name="rax" id="rax" lay-filter="test_xs" style="width:100px;">
                                <option value="0" selected="selected">000</option>
                                <option value="1"> 001 </option>
                                <option value="2"> 002 </option>
                                <option value="3"> 003 </option>
                                <option value="4"> 004 </option>
                                <option value="5"> 005 </option>
                                <option value="6"> 006 </option>
                                <option value="7"> 007 </option>
                                <option value="8"> 008 </option>
                                <option value="9"> 009 </option>
                                <option value="10"> 010 </option>
                                <option value="11"> 011 </option>
                                <option value="12"> 012 </option>
                                <option value="13"> 013 </option>
                                <option value="14"> 014 </option>
                                <option value="15"> 015 </option>
                                <option value="16"> 016 </option>
                                <option value="17"> 017 </option>
                                <option value="18"> 018 </option>
                                <option value="19"> 019 </option>
                                <option value="20"> 020 </option>
                                <option value="21"> 021 </option>
                                <option value="22"> 022 </option>
                                <option value="23"> 023 </option>
                                <option value="24"> 024 </option>
                                <option value="25"> 025 </option>
                                <option value="26"> 026 </option>
                                <option value="27"> 027 </option>
                                <option value="28"> 028 </option>
                                <option value="29"> 029 </option>
                                <option value="30"> 030 </option>
                                <option value="31"> 031 </option>
                                <option value="32"> 032 </option>
                                <option value="33"> 033 </option>
                                <option value="34"> 034 </option>
                                <option value="35"> 035 </option>
                                <option value="36"> 036 </option>
                                <option value="37"> 037 </option>
                                <option value="38"> 038 </option>
                                <option value="39"> 039 </option>
                                <option value="40"> 040 </option>
                                <option value="41"> 041 </option>
                                <option value="42"> 042 </option>
                                <option value="43"> 043 </option>
                                <option value="44"> 044 </option>
                                <option value="45"> 045 </option>
                                <option value="46"> 046 </option>
                                <option value="47"> 047 </option>
                                <option value="48"> 048 </option>
                                <option value="49"> 049 </option>
                                <option value="50"> 050 </option>
                                <option value="51"> 051 </option>
                                <option value="52"> 052 </option>
                                <option value="53"> 053 </option>
                                <option value="54"> 054 </option>
                                <option value="55"> 055 </option>
                                <option value="56"> 056 </option>
                                <option value="57"> 057 </option>
                                <option value="58"> 058 </option>
                                <option value="59"> 059 </option>
                                <option value="60"> 060 </option>
                                <option value="61"> 061 </option>
                                <option value="62"> 062 </option>
                                <option value="63"> 063 </option>
                                <option value="64"> 064 </option>
                                <option value="65"> 065 </option>
                                <option value="66"> 066 </option>
                                <option value="67"> 067 </option>
                                <option value="68"> 068 </option>
                                <option value="69"> 069 </option>
                                <option value="70"> 070 </option>
                                <option value="71"> 071 </option>
                                <option value="72"> 072 </option>
                                <option value="73"> 073 </option>
                                <option value="74"> 074 </option>
                                <option value="75"> 075 </option>
                                <option value="76"> 076 </option>
                                <option value="77"> 077 </option>
                                <option value="78"> 078 </option>
                                <option value="79"> 079 </option>
                                <option value="80"> 080 </option>
                                <option value="81"> 081 </option>
                                <option value="82"> 082 </option>
                                <option value="83"> 083 </option>
                                <option value="84"> 084 </option>
                                <option value="85"> 085 </option>
                                <option value="86"> 086 </option>
                                <option value="87"> 087 </option>
                                <option value="88"> 088 </option>
                                <option value="89"> 089 </option>
                                <option value="90"> 090 </option>
                                <option value="91"> 091 </option>
                                <option value="92"> 092 </option>
                                <option value="93"> 093 </option>
                                <option value="94"> 094 </option>
                                <option value="95"> 095 </option>
                                <option value="96"> 096 </option>
                                <option value="97"> 097 </option>
                                <option value="98"> 098 </option>
                                <option value="99"> 099 </option>
                                <option value="100"> 100 </option>
                                <option value="101"> 101 </option>
                                <option value="102"> 102 </option>
                                <option value="103"> 103 </option>
                                <option value="104"> 104 </option>
                                <option value="105"> 105 </option>
                                <option value="106"> 106 </option>
                                <option value="107"> 107 </option>
                                <option value="108"> 108 </option>
                                <option value="109"> 109 </option>
                                <option value="110"> 110 </option>
                                <option value="111"> 111 </option>
                                <option value="112"> 112 </option>
                                <option value="113"> 113 </option>
                                <option value="114"> 114 </option>
                                <option value="115"> 115 </option>
                                <option value="116"> 116 </option>
                                <option value="117"> 117 </option>
                                <option value="118"> 118 </option>
                                <option value="119"> 119 </option>
                                <option value="120"> 120 </option>
                                <option value="121"> 121 </option>
                                <option value="122"> 122 </option>
                                <option value="123"> 123 </option>
                                <option value="124"> 124 </option>
                                <option value="125"> 125 </option>
                                <option value="126"> 126 </option>
                                <option value="127"> 127 </option>
                                <option value="128"> 128 </option>
                                <option value="129"> 129 </option>
                                <option value="130"> 130 </option>
                                <option value="131"> 131 </option>
                                <option value="132"> 132 </option>
                                <option value="133"> 133 </option>
                                <option value="134"> 134 </option>
                                <option value="135"> 135 </option>
                                <option value="136"> 136 </option>
                                <option value="137"> 137 </option>
                                <option value="138"> 138 </option>
                                <option value="139"> 139 </option>
                                <option value="140"> 140 </option>
                                <option value="141"> 141 </option>
                                <option value="142"> 142 </option>
                                <option value="143"> 143 </option>
                                <option value="144"> 144 </option>
                                <option value="145"> 145 </option>
                                <option value="146"> 146 </option>
                                <option value="147"> 147 </option>
                                <option value="148"> 148 </option>
                                <option value="149"> 149 </option>
                                <option value="150"> 150 </option>
                                <option value="151"> 151 </option>
                                <option value="152"> 152 </option>
                                <option value="153"> 153 </option>
                                <option value="154"> 154 </option>
                                <option value="155"> 155 </option>
                                <option value="156"> 156 </option>
                                <option value="157"> 157 </option>
                                <option value="158"> 158 </option>
                                <option value="159"> 159 </option>
                                <option value="160"> 160 </option>
                                <option value="161"> 161 </option>
                                <option value="162"> 162 </option>
                                <option value="163"> 163 </option>
                                <option value="164"> 164 </option>
                                <option value="165"> 165 </option>
                                <option value="166"> 166 </option>
                                <option value="167"> 167 </option>
                                <option value="168"> 168 </option>
                                <option value="169"> 169 </option>
                                <option value="170"> 170 </option>
                                <option value="171"> 171 </option>
                                <option value="172"> 172 </option>
                                <option value="173"> 173 </option>
                                <option value="174"> 174 </option>
                                <option value="175"> 175 </option>
                                <option value="176"> 176 </option>
                                <option value="177"> 177 </option>
                                <option value="178"> 178 </option>
                                <option value="179"> 179 </option>
                                <option value="180"> 180 </option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="radd" id="radd" lay-filter="test_xs" style="width:100px;">
                                <option value="0.00" selected="selected">0.00</option>
                                <option value="0.50"> +0.50 </option>
                                <option value="0.75"> +0.75 </option>
                                <option value="1.00"> +1.00 </option>
                                <option value="1.25"> +1.25 </option>
                                <option value="1.50"> +1.50 </option>
                                <option value="1.75"> +1.75 </option>
                                <option value="2.00"> +2.00 </option>
                                <option value="2.25"> +2.25 </option>
                                <option value="2.50"> +2.50 </option>
                                <option value="2.75"> +2.75 </option>
                                <option value="3.00"> +3.00 </option>
                                <option value="3.25"> +3.25 </option>
                                <option value="3.50"> +3.50 </option>
                                <option value="3.75"> +3.75 </option>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>OS (Left Eye)</td>
                        <td align="center">
                            <select name="lsph" id="lsph" lay-filter="test_xs" style="width:100px;">
                                <option value="-16.0">-16.0</option>
                                <option value="-15.75">-15.75</option>
                                <option value="-15.5">-15.5</option>
                                <option value="-15.25">-15.25</option>
                                <option value="-15.0">-15.0</option>
                                <option value="-14.75">-14.75</option>
                                <option value="-14.5">-14.5</option>
                                <option value="-14.25">-14.25</option>
                                <option value="-14.0">-14.0</option>
                                <option value="-13.75">-13.75</option>
                                <option value="-13.5">-13.5</option>
                                <option value="-13.25">-13.25</option>
                                <option value="-13.0">-13.0</option>
                                <option value="-12.75">-12.75</option>
                                <option value="-12.5">-12.5</option>
                                <option value="-12.25">-12.25</option>
                                <option value="-12.0">-12.0</option>
                                <option value="-11.75">-11.75</option>
                                <option value="-11.5">-11.5</option>
                                <option value="-11.25">-11.25</option>
                                <option value="-11.0">-11.0</option>
                                <option value="-10.75">-10.75</option>
                                <option value="-10.5">-10.5</option>
                                <option value="-10.25">-10.25</option>
                                <option value="-10.0">-10.0</option>
                                <option value="-9.75">-9.75</option>
                                <option value="-9.5">-9.5</option>
                                <option value="-9.25">-9.25</option>
                                <option value="-9.0">-9.0</option>
                                <option value="-8.75">-8.75</option>
                                <option value="-8.5">-8.5</option>
                                <option value="-8.25">-8.25</option>
                                <option value="-8.0">-8.0</option>
                                <option value="-7.75">-7.75</option>
                                <option value="-7.5">-7.5</option>
                                <option value="-7.25">-7.25</option>
                                <option value="-7.0">-7.0</option>
                                <option value="-6.75">-6.75</option>
                                <option value="-6.5">-6.5</option>
                                <option value="-6.25">-6.25</option>
                                <option value="-6.0">-6.0</option>
                                <option value="-5.75">-5.75</option>
                                <option value="-5.5">-5.5</option>
                                <option value="-5.25">-5.25</option>
                                <option value="-5.0">-5.0</option>
                                <option value="-4.75">-4.75</option>
                                <option value="-4.5">-4.5</option>
                                <option value="-4.25">-4.25</option>
                                <option value="-4.0">-4.0</option>
                                <option value="-3.75">-3.75</option>
                                <option value="-3.5">-3.5</option>
                                <option value="-3.25">-3.25</option>
                                <option value="-3.0">-3.0</option>
                                <option value="-2.75">-2.75</option>
                                <option value="-2.5">-2.5</option>
                                <option value="-2.25">-2.25</option>
                                <option value="-2.0">-2.0</option>
                                <option value="-1.75">-1.75</option>
                                <option value="-1.5">-1.5</option>
                                <option value="-1.25">-1.25</option>
                                <option value="-1.0">-1.0</option>
                                <option value="-0.75">-0.75</option>
                                <option value="-0.5">-0.5</option>
                                <option value="-0.25">-0.25</option>
                                <option value="0.0" selected="selected">0.0</option>
                                <option value="0.25">0.25</option>
                                <option value="0.5">0.5</option>
                                <option value="0.75">0.75</option>
                                <option value="1.0">1.0</option>
                                <option value="1.25">1.25</option>
                                <option value="1.5">1.5</option>
                                <option value="1.75">1.75</option>
                                <option value="2.0">2.0</option>
                                <option value="2.25">2.25</option>
                                <option value="2.5">2.5</option>
                                <option value="2.75">2.75</option>
                                <option value="3.0">3.0</option>
                                <option value="3.25">3.25</option>
                                <option value="3.5">3.5</option>
                                <option value="3.75">3.75</option>
                                <option value="4.0">4.0</option>
                                <option value="4.25">4.25</option>
                                <option value="4.5">4.5</option>
                                <option value="4.75">4.75</option>
                                <option value="5.0">5.0</option>
                                <option value="5.25">5.25</option>
                                <option value="5.5">5.5</option>
                                <option value="5.75">5.75</option>
                                <option value="6.0">6.0</option>
                                <option value="6.25">6.25</option>
                                <option value="6.5">6.5</option>
                                <option value="6.75">6.75</option>
                                <option value="7.0">7.0</option>
                                <option value="7.25">7.25</option>
                                <option value="7.5">7.5</option>
                                <option value="7.75">7.75</option>
                                <option value="8.0">8.0</option>
                                <option value="8.25">8.25</option>
                                <option value="8.5">8.5</option>
                                <option value="8.75">8.75</option>
                                <option value="9.0">9.0</option>
                                <option value="9.25">9.25</option>
                                <option value="9.5">9.5</option>
                                <option value="9.75">9.75</option>
                                <option value="10.0">10.0</option>
                                <option value="10.25">10.25</option>
                                <option value="10.5">10.5</option>
                                <option value="10.75">10.75</option>
                                <option value="11.0">11.0</option>
                                <option value="11.25">11.25</option>
                                <option value="11.5">11.5</option>
                                <option value="11.75">11.75</option>
                                <option value="12.0">12.0</option>
                                <option value="12.25">12.25</option>
                                <option value="12.5">12.5</option>
                                <option value="12.75">12.75</option>
                                <option value="13.0">13.0</option>
                                <option value="13.25">13.25</option>
                                <option value="13.5">13.5</option>
                                <option value="13.75">13.75</option>
                                <option value="14.0">14.0</option>
                                <option value="14.25">14.25</option>
                                <option value="14.5">14.5</option>
                                <option value="14.75">14.75</option>
                                <option value="15.0">15.0</option>
                                <option value="15.25">15.25</option>
                                <option value="15.5">15.5</option>
                                <option value="15.75">15.75</option>
                                <option value="16.0">16.0</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="lcyl" id="lcyl" lay-filter="test_xs" style="width:100px;">
                                <option value="-6.0">-6.0</option>
                                <option value="-5.75">-5.75</option>
                                <option value="-5.5">-5.5</option>
                                <option value="-5.25">-5.25</option>
                                <option value="-5.0">-5.0</option>
                                <option value="-4.75">-4.75</option>
                                <option value="-4.5">-4.5</option>
                                <option value="-4.25">-4.25</option>
                                <option value="-4.0">-4.0</option>
                                <option value="-3.75">-3.75</option>
                                <option value="-3.5">-3.5</option>
                                <option value="-3.25">-3.25</option>
                                <option value="-3.0">-3.0</option>
                                <option value="-2.75">-2.75</option>
                                <option value="-2.5">-2.5</option>
                                <option value="-2.25">-2.25</option>
                                <option value="-2.0">-2.0</option>
                                <option value="-1.75">-1.75</option>
                                <option value="-1.5">-1.5</option>
                                <option value="-1.25">-1.25</option>
                                <option value="-1.0">-1.0</option>
                                <option value="-0.75">-0.75</option>
                                <option value="-0.5">-0.5</option>
                                <option value="-0.25">-0.25</option>
                                <option value="0.0" selected="selected">0.0</option>
                                <option value="0.25">0.25</option>
                                <option value="0.5">0.5</option>
                                <option value="0.75">0.75</option>
                                <option value="1.0">1.0</option>
                                <option value="1.25">1.25</option>
                                <option value="1.5">1.5</option>
                                <option value="1.75">1.75</option>
                                <option value="2.0">2.0</option>
                                <option value="2.25">2.25</option>
                                <option value="2.5">2.5</option>
                                <option value="2.75">2.75</option>
                                <option value="3.0">3.0</option>
                                <option value="3.25">3.25</option>
                                <option value="3.5">3.5</option>
                                <option value="3.75">3.75</option>
                                <option value="4.0">4.0</option>
                                <option value="4.25">4.25</option>
                                <option value="4.5">4.5</option>
                                <option value="4.75">4.75</option>
                                <option value="5.0">5.0</option>
                                <option value="5.25">5.25</option>
                                <option value="5.5">5.5</option>
                                <option value="5.75">5.75</option>
                                <option value="6.0">6.0</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="lax" id="lax" lay-filter="test_xs" style="width:100px;">
                                <option value="0" selected="selected">000</option>
                                <option value="1"> 001 </option>
                                <option value="2"> 002 </option>
                                <option value="3"> 003 </option>
                                <option value="4"> 004 </option>
                                <option value="5"> 005 </option>
                                <option value="6"> 006 </option>
                                <option value="7"> 007 </option>
                                <option value="8"> 008 </option>
                                <option value="9"> 009 </option>
                                <option value="10"> 010 </option>
                                <option value="11"> 011 </option>
                                <option value="12"> 012 </option>
                                <option value="13"> 013 </option>
                                <option value="14"> 014 </option>
                                <option value="15"> 015 </option>
                                <option value="16"> 016 </option>
                                <option value="17"> 017 </option>
                                <option value="18"> 018 </option>
                                <option value="19"> 019 </option>
                                <option value="20"> 020 </option>
                                <option value="21"> 021 </option>
                                <option value="22"> 022 </option>
                                <option value="23"> 023 </option>
                                <option value="24"> 024 </option>
                                <option value="25"> 025 </option>
                                <option value="26"> 026 </option>
                                <option value="27"> 027 </option>
                                <option value="28"> 028 </option>
                                <option value="29"> 029 </option>
                                <option value="30"> 030 </option>
                                <option value="31"> 031 </option>
                                <option value="32"> 032 </option>
                                <option value="33"> 033 </option>
                                <option value="34"> 034 </option>
                                <option value="35"> 035 </option>
                                <option value="36"> 036 </option>
                                <option value="37"> 037 </option>
                                <option value="38"> 038 </option>
                                <option value="39"> 039 </option>
                                <option value="40"> 040 </option>
                                <option value="41"> 041 </option>
                                <option value="42"> 042 </option>
                                <option value="43"> 043 </option>
                                <option value="44"> 044 </option>
                                <option value="45"> 045 </option>
                                <option value="46"> 046 </option>
                                <option value="47"> 047 </option>
                                <option value="48"> 048 </option>
                                <option value="49"> 049 </option>
                                <option value="50"> 050 </option>
                                <option value="51"> 051 </option>
                                <option value="52"> 052 </option>
                                <option value="53"> 053 </option>
                                <option value="54"> 054 </option>
                                <option value="55"> 055 </option>
                                <option value="56"> 056 </option>
                                <option value="57"> 057 </option>
                                <option value="58"> 058 </option>
                                <option value="59"> 059 </option>
                                <option value="60"> 060 </option>
                                <option value="61"> 061 </option>
                                <option value="62"> 062 </option>
                                <option value="63"> 063 </option>
                                <option value="64"> 064 </option>
                                <option value="65"> 065 </option>
                                <option value="66"> 066 </option>
                                <option value="67"> 067 </option>
                                <option value="68"> 068 </option>
                                <option value="69"> 069 </option>
                                <option value="70"> 070 </option>
                                <option value="71"> 071 </option>
                                <option value="72"> 072 </option>
                                <option value="73"> 073 </option>
                                <option value="74"> 074 </option>
                                <option value="75"> 075 </option>
                                <option value="76"> 076 </option>
                                <option value="77"> 077 </option>
                                <option value="78"> 078 </option>
                                <option value="79"> 079 </option>
                                <option value="80"> 080 </option>
                                <option value="81"> 081 </option>
                                <option value="82"> 082 </option>
                                <option value="83"> 083 </option>
                                <option value="84"> 084 </option>
                                <option value="85"> 085 </option>
                                <option value="86"> 086 </option>
                                <option value="87"> 087 </option>
                                <option value="88"> 088 </option>
                                <option value="89"> 089 </option>
                                <option value="90"> 090 </option>
                                <option value="91"> 091 </option>
                                <option value="92"> 092 </option>
                                <option value="93"> 093 </option>
                                <option value="94"> 094 </option>
                                <option value="95"> 095 </option>
                                <option value="96"> 096 </option>
                                <option value="97"> 097 </option>
                                <option value="98"> 098 </option>
                                <option value="99"> 099 </option>
                                <option value="100"> 100 </option>
                                <option value="101"> 101 </option>
                                <option value="102"> 102 </option>
                                <option value="103"> 103 </option>
                                <option value="104"> 104 </option>
                                <option value="105"> 105 </option>
                                <option value="106"> 106 </option>
                                <option value="107"> 107 </option>
                                <option value="108"> 108 </option>
                                <option value="109"> 109 </option>
                                <option value="110"> 110 </option>
                                <option value="111"> 111 </option>
                                <option value="112"> 112 </option>
                                <option value="113"> 113 </option>
                                <option value="114"> 114 </option>
                                <option value="115"> 115 </option>
                                <option value="116"> 116 </option>
                                <option value="117"> 117 </option>
                                <option value="118"> 118 </option>
                                <option value="119"> 119 </option>
                                <option value="120"> 120 </option>
                                <option value="121"> 121 </option>
                                <option value="122"> 122 </option>
                                <option value="123"> 123 </option>
                                <option value="124"> 124 </option>
                                <option value="125"> 125 </option>
                                <option value="126"> 126 </option>
                                <option value="127"> 127 </option>
                                <option value="128"> 128 </option>
                                <option value="129"> 129 </option>
                                <option value="130"> 130 </option>
                                <option value="131"> 131 </option>
                                <option value="132"> 132 </option>
                                <option value="133"> 133 </option>
                                <option value="134"> 134 </option>
                                <option value="135"> 135 </option>
                                <option value="136"> 136 </option>
                                <option value="137"> 137 </option>
                                <option value="138"> 138 </option>
                                <option value="139"> 139 </option>
                                <option value="140"> 140 </option>
                                <option value="141"> 141 </option>
                                <option value="142"> 142 </option>
                                <option value="143"> 143 </option>
                                <option value="144"> 144 </option>
                                <option value="145"> 145 </option>
                                <option value="146"> 146 </option>
                                <option value="147"> 147 </option>
                                <option value="148"> 148 </option>
                                <option value="149"> 149 </option>
                                <option value="150"> 150 </option>
                                <option value="151"> 151 </option>
                                <option value="152"> 152 </option>
                                <option value="153"> 153 </option>
                                <option value="154"> 154 </option>
                                <option value="155"> 155 </option>
                                <option value="156"> 156 </option>
                                <option value="157"> 157 </option>
                                <option value="158"> 158 </option>
                                <option value="159"> 159 </option>
                                <option value="160"> 160 </option>
                                <option value="161"> 161 </option>
                                <option value="162"> 162 </option>
                                <option value="163"> 163 </option>
                                <option value="164"> 164 </option>
                                <option value="165"> 165 </option>
                                <option value="166"> 166 </option>
                                <option value="167"> 167 </option>
                                <option value="168"> 168 </option>
                                <option value="169"> 169 </option>
                                <option value="170"> 170 </option>
                                <option value="171"> 171 </option>
                                <option value="172"> 172 </option>
                                <option value="173"> 173 </option>
                                <option value="174"> 174 </option>
                                <option value="175"> 175 </option>
                                <option value="176"> 176 </option>
                                <option value="177"> 177 </option>
                                <option value="178"> 178 </option>
                                <option value="179"> 179 </option>
                                <option value="180"> 180 </option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="ladd" id="ladd" lay-filter="test_xs" style="width:100px;">
                                <option value="0.00" selected="selected">0.00</option>
                                <option value="0.50"> +0.50 </option>
                                <option value="0.75"> +0.75 </option>
                                <option value="1.00"> +1.00 </option>
                                <option value="1.25"> +1.25 </option>
                                <option value="1.50"> +1.50 </option>
                                <option value="1.75"> +1.75 </option>
                                <option value="2.00"> +2.00 </option>
                                <option value="2.25"> +2.25 </option>
                                <option value="2.50"> +2.50 </option>
                                <option value="2.75"> +2.75 </option>
                                <option value="3.00"> +3.00 </option>
                                <option value="3.25"> +3.25 </option>
                                <option value="3.50"> +3.50 </option>
                                <option value="3.75"> +3.75 </option>

                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="layui-form-item">
                <table class="layui-table layui-form">
                    <thead>
                    <tr>
                        <th>OD-Right</th>
                        <th>OS-Left</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input class="layui-input"  autocomplete="off" placeholder="rpd" name="rpd" id="rpd" required="" lay-verify="required"></td>
                        <td><input class="layui-input"  autocomplete="off" placeholder="lpd" name="lpd" id="lpd" required="" lay-verify="required"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="layui-form-item">
                <table class="layui-table layui-form">
                    <thead>
                    <tr>
                        <th></th>
                        <th><center>Prism-H</center></th>
                        <th><center>Base-H</center></th>
                        <th><center>Prism-V</center></th>
                        <th><center>Base-V</center></th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>OD (Right Eye)</td>
                        <td align="center">
                            <select name="rpri" id="rpri" lay-filter="test_xs" style="width:100px;">
                                <option value="0.0" selected="selected">0.0</option>
                                <option value="0.25">0.25</option>
                                <option value="0.5">0.5</option>
                                <option value="0.75">0.75</option>
                                <option value="1.0">1.0</option>
                                <option value="1.25">1.25</option>
                                <option value="1.5">1.5</option>
                                <option value="1.75">1.75</option>
                                <option value="2.0">2.0</option>
                                <option value="2.25">2.25</option>
                                <option value="2.5">2.5</option>
                                <option value="2.75">2.75</option>
                                <option value="3.0">3.0</option>
                                <option value="3.25">3.25</option>
                                <option value="3.5">3.5</option>
                                <option value="3.75">3.75</option>
                                <option value="4.0">4.0</option>
                                <option value="4.25">4.25</option>
                                <option value="4.5">4.5</option>
                                <option value="4.75">4.75</option>
                                <option value="5.0">5.0</option>
                                <option value="5.25">5.25</option>
                                <option value="5.5">5.5</option>
                                <option value="5.75">5.75</option>
                                <option value="6.0">6.0</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="rbase" id="rbase" lay-filter="test_xs">
                                <option value="" selected ></option>
                                <option value="In">In</option>
                                <option value="Out">Out</option>
                                <option value="Up">Up</option>
                                <option value="Down">Down</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="rpri_1" id="rpri_1" lay-filter="test_xs" style="width:100px;">
                                <option value="0.0" selected="selected">0.0</option>
                                <option value="0.25">0.25</option>
                                <option value="0.5">0.5</option>
                                <option value="0.75">0.75</option>
                                <option value="1.0">1.0</option>
                                <option value="1.25">1.25</option>
                                <option value="1.5">1.5</option>
                                <option value="1.75">1.75</option>
                                <option value="2.0">2.0</option>
                                <option value="2.25">2.25</option>
                                <option value="2.5">2.5</option>
                                <option value="2.75">2.75</option>
                                <option value="3.0">3.0</option>
                                <option value="3.25">3.25</option>
                                <option value="3.5">3.5</option>
                                <option value="3.75">3.75</option>
                                <option value="4.0">4.0</option>
                                <option value="4.25">4.25</option>
                                <option value="4.5">4.5</option>
                                <option value="4.75">4.75</option>
                                <option value="5.0">5.0</option>
                                <option value="5.25">5.25</option>
                                <option value="5.5">5.5</option>
                                <option value="5.75">5.75</option>
                                <option value="6.0">6.0</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="rbase_1" id="rbase_1" lay-filter="test_xs">
                                <option value="" selected ></option>
                                <option value="In">In</option>
                                <option value="Out">Out</option>
                                <option value="Up">Up</option>
                                <option value="Down">Down</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>OS (Left Eye)</td>
                        <td align="center">
                            <select name="lpri" id="lpri" lay-filter="test_xs" style="width:100px;">
                                <option value="0.0" selected="selected">0.0</option>
                                <option value="0.25">0.25</option>
                                <option value="0.5">0.5</option>
                                <option value="0.75">0.75</option>
                                <option value="1.0">1.0</option>
                                <option value="1.25">1.25</option>
                                <option value="1.5">1.5</option>
                                <option value="1.75">1.75</option>
                                <option value="2.0">2.0</option>
                                <option value="2.25">2.25</option>
                                <option value="2.5">2.5</option>
                                <option value="2.75">2.75</option>
                                <option value="3.0">3.0</option>
                                <option value="3.25">3.25</option>
                                <option value="3.5">3.5</option>
                                <option value="3.75">3.75</option>
                                <option value="4.0">4.0</option>
                                <option value="4.25">4.25</option>
                                <option value="4.5">4.5</option>
                                <option value="4.75">4.75</option>
                                <option value="5.0">5.0</option>
                                <option value="5.25">5.25</option>
                                <option value="5.5">5.5</option>
                                <option value="5.75">5.75</option>
                                <option value="6.0">6.0</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="lbase" id="lbase" lay-filter="test_xs">
                                <option value="" selected ></option>
                                <option value="In">In</option>
                                <option value="Out">Out</option>
                                <option value="Up">Up</option>
                                <option value="Down">Down</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="lpri_1" id="lpri_1" lay-filter="test_xs" style="width:100px;">
                                <option value="0.0" selected="selected">0.0</option>
                                <option value="0.25">0.25</option>
                                <option value="0.5">0.5</option>
                                <option value="0.75">0.75</option>
                                <option value="1.0">1.0</option>
                                <option value="1.25">1.25</option>
                                <option value="1.5">1.5</option>
                                <option value="1.75">1.75</option>
                                <option value="2.0">2.0</option>
                                <option value="2.25">2.25</option>
                                <option value="2.5">2.5</option>
                                <option value="2.75">2.75</option>
                                <option value="3.0">3.0</option>
                                <option value="3.25">3.25</option>
                                <option value="3.5">3.5</option>
                                <option value="3.75">3.75</option>
                                <option value="4.0">4.0</option>
                                <option value="4.25">4.25</option>
                                <option value="4.5">4.5</option>
                                <option value="4.75">4.75</option>
                                <option value="5.0">5.0</option>
                                <option value="5.25">5.25</option>
                                <option value="5.5">5.5</option>
                                <option value="5.75">5.75</option>
                                <option value="6.0">6.0</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="lbase_1" id="lbase_1" lay-filter="test_xs">
                                <option value="" selected ></option>
                                <option value="In">In</option>
                                <option value="Out">Out</option>
                                <option value="Up">Up</option>
                                <option value="Down">Down</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="layui-form-item">
                <label for="L_repass" class="layui-form-label"></label>
                <button class="layui-btn" lay-filter="add" lay-submit="">Save</button></div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'layer','jquery','laydate'],
        function() {
            $ = layui.jquery;
            var laydate = layui.laydate;
            var form = layui.form;
            var layer = layui.layer;

            form.on('submit(add)',
                function(data) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/shouji/prescription/save']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'prescription_name':$("#prescription_name").val(),
                            'rsph':$("#rsph").val(),
                            'lsph':$("#lsph").val(),
                            'rcyl':$("#rcyl").val(),
                            'lcyl':$("#lcyl").val(),
                            'rax':$("#rax").val(),
                            'lax':$("#lax").val(),
                            'rpri':$("#rpri").val(),
                            'lpri':$("#lpri").val(),
                            'rbase':$("#rbase").val(),
                            'lbase':$("#lbase").val(),
                            'radd':$("#radd").val(),
                            'ladd':$("#ladd").val(),
                            'pd':$("#pd").val(),
                            'single_pd':$("#single_pd").val(),
                            'rpd':$("#rpd").val(),
                            'lpd':$("#lpd").val(),
                            'rpri_1':$("#rpri_1").val(),
                            'lpri_1':$("#lpri_1").val(),
                            'rbase_1':$("#rbase_1").val(),
                            'lbase_1':$("#lbase_1").val()
                        },
                        success: function (result) {
                            layer.alert("Success", {
                                    icon: 6
                                },
                                function() {
                                    //关闭当前frame
                                    xadmin.close();

                                    // 可以对父窗口进行刷新
                                    xadmin.father_reload();
                                }
                            );
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                    return false;
                });

        });</script>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>