<?php

$magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
$magento_autoload->loadMagentoFrameword();
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$static_url=$objectManager->get("Pg\Learning\Block\Page\Banner")->getStaticUrl();
$Version=$objectManager->get("\Magento\Framework\App\View\Deployment\Version")->getValue();
?>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets/x-admin/userDetails/css/index.css?v=<?php echo $Version;?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets/x-admin/lib/layui/css/layui.css?v=<?php echo $Version;?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets/x-admin/userDetails/kkpager/kkpager_orange.css?v=<?php echo $Version;?>">
</head>
<body>
<div class="parent_pad">
    <!--图片放大展示html-->
    <div id="outerdiv" style="position:fixed;top:0;left:0;background:rgba(0,0,0,0.6);z-index: 20000000;;width:100%;height:100%;display:none;">
        <div id="innerdiv" style="position:absolute;">
            <img id="bigimg" style="border:5px solid #fff;" src="" />
        </div>
    </div>
    <table class="layui-hide" id="demo" lay-filter="test"></table>

    <fieldset class="layui-elem-field Filter_class_parent">
        <legend class="Filter_class">Filter:</legend>
        <div class="layui-inline">
            <label class="layui-form-label">Email:</label>
            <div class="layui-input-inline">
                <input type="text" name="email" lay-verify="email" autocomplete="off" class="layui-input email_filter"
                       placeholder="Enter a filtered mailbox"
                >
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">Order_id:</label>
            <div class="layui-input-inline">
                <input type="text" name="orderId" lay-verify="email" autocomplete="off" class="layui-input email_orderId"
                       placeholder="Enter a filtered order id"
                >
            </div>
        </div>
        <button type="button" class="layui-btn layui-btn-normal layui-btn-radius" id="Search_filter">Search</button>
        <button type="button" class="layui-btn layui-btn-warm layui-btn-radius" id="Search_clearall">Clear All</button>
    </fieldset>

    <div class="parents_ys">
        <span>Current number: </span>
        <span class="ys_text Current_number"></span>
        <span>Pages Current: </span>
        <span class="ys_text Pages_Current"></span>
        <span>PageCount: </span>
        <span class="ys_text PageCount"></span>
    </div>



    <table class="layui-table"  id="layerDemo">
        <colgroup>
            <col width="150" />
            <col width="200" />
            <col />
        </colgroup>
        <thead>
        <tr>
            <th>
                Name
            </th>
            <th>
                Email
                </span>
            </th>
            <th>
                Created Date
            </span>
            </th>
            <th>
                Action
            </th>
        </tr>
        </thead>
        <tbody id='help_html'></tbody>
    </table>




    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs View_Cart" lay-event="detail">View Cart</a>
        <a class="layui-btn layui-btn-normal layui-btn-xs View_Order" lay-event="detail">View Order</a>
        <a class="layui-btn layui-btn-warm layui-btn-xs View_Profile" lay-event="detail">View Profile</a>
        <a class="layui-btn layui-btn-primary layui-btn-xs View_Prescription" lay-event="detail">View Prescription</a>
    </script>

    <!--底部分页展示 html-->
    <div style="width:800px;margin:0 auto;">
        <div id="kkpager"></div>
    </div>
</div>
</body>
<script>
    var staticUrl = "<?php echo $static_url;?>";
</script>
<script type="text/javascript" src='<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/js/jquery.js?v=<?php echo $Version;?>'></script>
<script type="text/javascript" src="<?php echo $static_url; ?>/assets/x-admin/lib/layui/layui.js?v=<?php echo $Version;?>"></script>
<script type="text/javascript" src="<?php echo $static_url; ?>/assets/x-admin/userDetails/kkpager/kkpager.js?v=<?php echo $Version;?>"></script>
<script type="text/javascript" src="<?php echo $static_url; ?>/assets/x-admin/userDetails/js/index.js?v=<?php echo $Version;?>"></script>
