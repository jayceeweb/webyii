<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
        <style>
            .mobel_juli button{
                margin-left: 0px !important;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body">
                        <table style="BORDER-COLLAPSE: collapse" bordercolor="#000000" cellspacing="0" width="300" align="center" bgcolor="#ffffff" border="1">
                            <tbody>
                            <tr>
                                <td colspan="3">
                                    <div align="center">
                                        密码修改
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="100">
                                    <div align="center">
                                        邮箱
                                    </div>
                                </td>
                                <td width="100">
                                    <div align="center">
                                        <input type="text" name="Email" id="Email" placeholder="Email" style="width: 200px"  autocomplete="off" class="layui-input">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center">
                                        密码
                                    </div>
                                </td>
                                <td>
                                    <div align="center">
                                        <input type="text" name="Password" id="Password"  placeholder="Password" style="width: 200px"  autocomplete="off" class="layui-input">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div align="center">
                                        <button class="layui-btn"  onclick="activeUpdatePwd()">Update</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            </tbody>
                        </table>
                        <br>
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <input type="text" name="email"  placeholder="email" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['email']?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block btn-seach1">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Email</th>
                                <th>New Password</th>
                                <th>Author Name</th>
                                <th>Created At</th>
                                <th>reset</th>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td>
                                        <?php echo $row['entity_id']; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row['email']; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row['new_password']; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row['author_name']; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row['created_at']; ?><br>
                                    </td>
                                    <td>
                                        <button class="layui-btn"  onclick="activeReset('<?php echo $row["entity_id"];?>')">
                                            <!-- active this score-->
                                            <a href="#" title="reset the password">
                                                <img src="/assets2/resource/img/disable.png" alt="reset the password" class="img_box">
                                            </a>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        /**
         * 修改密码
         */
        function activeUpdatePwd() {
            confirm_message="Are you sure you want to change the password for this account?";
            var email = $('#Email').val();
            var password = $('#Password').val();
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/customerhelp/updatepassword/update-pwd']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'email':email,
                            'password':password,
                        },
                        success: function (result) {
                            result = JSON.parse(result);
                            if(result.code < 0){
                                layer.msg('The user does not exist.', {icon: 1, time: 1000});
                            }else{
                                layer.msg('success!', {icon: 1, time: 1000});
                                xadmin.father_reload();
                            }
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }
        function activeReset(entity_id) {
            confirm_message="Are you sure to reset the password for the current user?";
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/customerhelp/updatepassword/reset-pwd']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'entity_id':entity_id,
                        },
                        success: function (result) {
                            result = JSON.parse(result);
                            if(result.code < 0){
                                layer.msg('The user does not exist.', {icon: 1, time: 1000});
                            }else{
                                layer.msg('success!', {icon: 1, time: 1000});
                                xadmin.father_reload();
                            }
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }

        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;
                lay(".from_date").each(function () {
                    laydate.render({
                        elem: this,
                        trigger:'click',
                        obj:$(this).attr("obj"),
                        done: function(value, date, endDate){
                            obj=JSON.parse($(this).attr("obj"));
                            saveChange(obj,value,"from_date");
                        }
                    });
                })
            }
        );
    </script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>