<?php

$magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
$magento_autoload->loadMagentoFrameword();
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$static_url=$objectManager->get("Pg\Learning\Block\Page\Banner")->getStaticUrl();
$Version=$objectManager->get("\Magento\Framework\App\View\Deployment\Version")->getValue();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/css/index.css?v=<?php echo $Version;?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets/x-admin/lib/layui/css/layui.css?v=<?php echo $Version;?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/kkpager/kkpager_orange.css?v=<?php echo $Version;?>">
    <link rel="stylesheet" href="<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/css/multiple-select.min.css">

</head>
<body>
<h1 class="Prescriptions_h1">Edit Prescriptions For Customers</h1>
<!--图片放大展示html-->
<div id="outerdiv" style="position:fixed;top:0;left:0;background:rgba(0,0,0,0.6);z-index: 20000000;;width:100%;height:100%;display:none;">
    <div id="innerdiv" style="position:absolute;">
        <img id="bigimg" style="border:5px solid #fff;" src="" />
    </div>
</div>

<div class="box-top" id="top">
    <!--筛选栏展示 html-->
    <div>
        <fieldset class='layui-elem-field Filter_class_parent'>
            <legend class='Filter_class'>Filter:</legend>
            <div class='Filter_parent_class'>
                <div class="Filter_parent_child">
                    <div style="margin-bottom: 10px">
                        <label class="">Email</label>
                        <div class="Filter_parent_child_div">
                            <input type="text" name="email" class="filter_email" placeholder="Enter a filtered mailbox">
                        </div>
                    </div>
                    <div>
                        <label class="">Know Your Pd</label>
                        <div class="Filter_parent_child_div">
                            <select name="" id="Pd_status">
                                <option selected="" value="0">All</option>
                                <option value="1">Yes</option>
                                <option value="2">No</option>
                                <option value="3">Never</option>
                            </select>
                        </div>
                    </div>
                    <div style="margin-top: 15px">
                        <label class="">Archive Search</label>
                        <div class="" id="multiple_gd">
                            <select name='Archive_Search' multiple="multiple" id="Archive_Search">
                                <option value='0' selected="">Archive</option>
                                <option value='1'>Lack of PD</option>
                                <option value='2'>Lack of prescription photo</option>
                                <option value='3'>Email sent</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="Filter_parent_child">
                    <div style="margin-bottom: 10px">
                        <label class="">Profile Name</label>
                        <div class="Filter_parent_child_div">
                            <input type="text" name="ProfileName" class="filter_profile_name" placeholder="Enter filtered profile name">
                        </div>
                    </div>
                    <div class="">
                        <input type="checkbox" id="myCheck" style="display: none">
                        <label for="myCheck"></label>
                        <label class="same_text">view the same user</label>
                    </div>
                </div>
                <div class="select-inline Filter_parent_child">
                    <label class="">Status</label>
                    <div class="" id="multiple_status">
                        <select name='filter_status' multiple="multiple" id="filter_status">
                            <option value="0" selected="">new</option>
                            <option value="1" selected="">rx added</option>
                            <option value="2" selected="">verified</option>
                            <option value="3" selected="">cust notified</option>
                        </select>
                    </div>
                </div>
                <div class="select-inline Filter_parent_child">
                    <label class="">Date</label>
                    <div class="Filter_parent_child_div">
                        <select name="" id="filter_date">
                            <option selected="" value="">All</option>
                            <option value="1">Last month</option>
                            <option value="2">Last three months</option>
                            <option value="3">Last half year</option>
                            <option value="4">Last year</option>
                        </select>
                    </div>
                </div>
                <div class="select-inline Filter_parent_child">
                    <button class='Confirm_filter'>Search</button>
                    <button class='Clear_All_filter'>Clear All</button>
                </div>
            </div>
        </fieldset>
    </div>

    <!--Add Prescriptions For Customers 按钮展示 html-->
    <button type="button" class="layui-btn layui-btn-warm add_Customers_click">
        <i class="layui-icon layui-icon-add-circle"></i> Add Prescriptions For Customers
    </button>


    <div class="parents_ys">
        <span>Current number: </span>
        <span class="ys_text Current_number"></span>
        <span>Pages Current: </span>
        <span class="ys_text Pages_Current"></span>
        <span>PageCount: </span>
        <span class="ys_text PageCount"></span>
    </div>

</div>



<!--求助用户列表【表头展示】html-->
<table class="layui-table"  id="layerDemo">
    <colgroup>
        <col width="150" />
        <col width="200" />
        <col />
    </colgroup>
    <thead>
    <tr>
        <th></th>
        <th>Profile Name</th>
        <th>Email</th>
        <th>Sku</th>
        <th class="sort_jl" state="asc" state_val="prescription_status">
            Know Your Pd
            <span class="layui-table-sort layui-inline sort_pd sort" lay-sort="asc">
                <i class="layui-edge layui-table-sort-asc"  state="asc" state_val="help_pd" title="Ascending order"></i>
                <i class="layui-edge layui-table-sort-desc" state="desc" state_val="help_pd" title="Descending order"></i>
            </span>
        </th>
        <th>
            Requested Date
            <span class="layui-table-sort layui-inline sort_requested sort" lay-sort="asc">
                <i class="layui-edge layui-table-sort-asc" state="asc" state_val="ahc_created_at" title="Ascending order"></i>
                <i class="layui-edge layui-table-sort-desc" state="desc" state_val="ahc_created_at" title="Descending order"></i>
            </span>
        </th>
        <th>
            Rx Date
            <span class="layui-table-sort layui-inline sort_completed sort" lay-sort="asc">
                <i class="layui-edge layui-table-sort-asc" state="asc" state_val="ape_created_at" title="Ascending order"></i>
                <i class="layui-edge layui-table-sort-desc" state="desc" state_val="ape_created_at" title="Descending order"></i>
            </span>
        </th>
        <th>
            Confirm Date
            <span class="layui-table-sort layui-inline sort_completed sort" lay-sort="asc">
                <i class="layui-edge layui-table-sort-asc" state="asc" state_val="acknowledging_time" title="Ascending order"></i>
                <i class="layui-edge layui-table-sort-desc" state="desc" state_val="acknowledging_time" title="Descending order"></i>
            </span>
        </th>
        <th>
            Send Date
            <span class="layui-table-sort layui-inline sort_completed sort" lay-sort="asc">
                <i class="layui-edge layui-table-sort-asc" state="asc" state_val="send_created_time" title="Ascending order"></i>
                <i class="layui-edge layui-table-sort-desc" state="desc" state_val="send_created_time" title="Descending order"></i>
            </span>
        </th>
        <th>
            Status
            <span class="layui-table-sort layui-inline sort_status sort" lay-sort="asc">
                <i class="layui-edge layui-table-sort-asc layui-table-sort-asc_active" state="asc" state_val="prescription_status" title="Ascending order"></i>
                <i class="layui-edge layui-table-sort-desc" state="desc" state_val="prescription_status" title="Descending order"></i>
            </span>
        </th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody id='help_html'></tbody>
</table>

<!--底部分页展示 html-->
<div style="width:800px;margin:0 auto;">
    <div id="kkpager"></div>
</div>


</body>
<script>
    var staticUrl = "<?php echo $static_url;?>";
</script>
<script type="text/javascript" src='<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/js/jquery.js?v=<?php echo $Version;?>'></script>
<script type="text/javascript" src="<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/js/multiple-select-es.js"></script>
<script type="text/javascript" src="<?php echo $static_url; ?>/assets/x-admin/lib/layui/layui.js?v=<?php echo $Version;?>"></script>
<script type="text/javascript" src="<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/kkpager/kkpager.js?v=<?php echo $Version;?>"></script>
<script type="text/javascript" src="<?php echo $static_url; ?>/assets/x-admin/PrescriptionHelp/js/index.js?v=<?php echo $Version;?>"></script>
</html>