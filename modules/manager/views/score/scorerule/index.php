<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">
  
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      <?php
      $this->registerCsrfMetaTags();
      $this->head();
      ?>
  </head>
  
  <body>
  <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
  <div class="layui-fluid">
      <div class="layui-row layui-col-space15">
          <div class="layui-col-md12">
              <div class="layui-card">
                  <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="name"  placeholder="please input product name" style="width: 200px" autocomplete="off" class="layui-input" value="<?php echo $condition["name"] ?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="sku"  placeholder="please input sku" autocomplete="off" class="layui-input" value="<?php echo $condition["sku"] ?>">
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="type_id" id="type_id">
                                    <option <?php if ($condition["type_id"]==""):?>selected<?php endif; ?>></option>
                                    <option <?php if ($condition["type_id"]=="simple"):?>selected<?php endif; ?>>simple</option>
                                    <option <?php if ($condition["type_id"]=="virtual"):?>selected<?php endif; ?>>virtual</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="attribute_set_id" id="attribute_set_id">
                                    <option value="" <?php if ($condition["attribute_set_id"]==""):?>selected<?php endif; ?>></option>
                                    <?php foreach ($EAV_ATTRIBUTE_SETS as $key=>$EAV_ATTRIBUTE_SET): ?>
                                    <option value="<?php echo $EAV_ATTRIBUTE_SET["attribute_set_id"] ?>" <?php if ($condition["attribute_set_id"]==$EAV_ATTRIBUTE_SET["attribute_set_id"]):?>selected<?php endif; ?>><?php echo $EAV_ATTRIBUTE_SET["attribute_set_name"] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                          <thead>
                            <tr>
                              <th>sku</th>
                              <th>name</th>
                              <th>type</th>
                              <th>info</th>
                              <th>price</th>
                              <th>thumb</th>
                              <th>action</th></tr>
                          </thead>
                          <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                            <tr>
                              <td><?php echo $row["sku"] ?></td>
                              <td><?php echo $row["name"] ?></td>
                              <td><?php echo $row["type_id"] ?></td>
                              <td>
                                  stock_sku=<?php echo $row["entity_id"] ?><br>
                                  attribute_set_id=<?php echo $row["attribute_set_id"] ?><br>
                                  <?php if ($row["type_id"]=="simple"): ?>
                                  color=<?php echo $row["color_value"] ?><br>
                                  <?php endif; ?>
                              </td>
                              <td>
                                  price=<?php echo number_format($row["price"],2) ?><br>
                                  special_price=<?php echo number_format($row["special_price"],2) ?><br>
                              </td>
                              <td>
                                  <?php if ($row["type_id"]=="configurable" || $row["type_id"]=="simple"): ?>
                                      <?php
                                      $href="";
                                      if ($row["type_id"]=="simple" &&
                                          ($row["attribute_set_id"]==$GLASSES_ATTRIBUTE_SET_ID
                                            || $row["attribute_set_id"]==$GOGGLES_ATTRIBUTE_SET_ID
                                          )
                                      ){
                                          $href=$base_url."/".Yii::$container->get("app\models\UrlRewrite")->getGlassesSimpleUrl($row["entity_id"])."?s=".$row["sku"];
                                      } elseif ($row["type_id"]=="simple" && in_array($row["attribute_set_id"],$ACCESSORIES_ATTRIBUTE_SET_IDS)) {
                                          $href=$base_url."/".Yii::$container->get("app\models\UrlRewrite")->getSimpleUrl($row["entity_id"]);
                                      } else {
                                          $href="";
                                      }
                                      ?>
                                  <a target="_blank" href="<?php echo $href; ?>">
                                  <img src="<?php echo $static_url."/media/catalog/product/".$row["thumbnail"] ?>" width="200" height="100" />
                                  </a>
                                  <?php endif; ?>
                              </td>
                              <td class="td-manage">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>group_code</th>
                                        <th>list_score</th>
                                        <th>special_score</th>
                                        <th>birthday_score</th>
                                        <th>from_date</th>
                                        <th>to_date</th>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($row["user_groups"] as $k=>$user_group): ?>
                                    <tr>
                                        <td><?php echo $user_group["customer_group_code"] ?></td>
                                        <td width="5px;" style="padding: 2px;"><input style="width: 80px;" type="text" name="list_score" class="layui-input" width="10" value="<?php echo $user_group['list_score']; ?>" obj='<?php echo json_encode($user_group); ?>'></td>
                                        <td width="5px;" style="padding: 2px;"><input style="width: 80px;" type="text" name="special_score" class="layui-input" width="10" value="<?php echo $user_group['special_score']; ?>" obj='<?php echo json_encode($user_group); ?>'></td>
                                        <td width="5px;" style="padding: 2px;"><input style="width: 80px;" type="text" name="birthday_score" class="layui-input" width="10" value="<?php echo $user_group['birthday_score']; ?>" obj='<?php echo json_encode($user_group); ?>'></td>
                                        <td width="5px;" style="padding: 2px;"><input style="width: 80px;" type="text" name="from_date" class="layui-input from_date" width="20" value="<?php echo $user_group['from_date']; ?>" obj='<?php echo json_encode($user_group); ?>'></td>
                                        <td width="5px;" style="padding: 2px;"><input style="width: 80px;" type="text" name="to_date" class="layui-input to_date" width="20" value="<?php echo $user_group['to_date']; ?>" obj='<?php echo json_encode($user_group); ?>'></td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                          </tbody>
                        </table>
                    </div>
                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script>
    layui.use(['form', 'layer','jquery','laydate'],
        function() {
            $ = layui.jquery;
            var laydate = layui.laydate;
            var form = layui.form;
            var layer = layui.layer;
            lay(".from_date").each(function () {
                laydate.render({
                    elem: this,
                    trigger:'click',
                    obj:$(this).attr("obj"),
                    done: function(value, date, endDate){
                        obj=JSON.parse($(this).attr("obj"));
                        saveChange(obj,value,"from_date");
                    }
                });
            })
            lay(".to_date").each(function () {
                laydate.render({
                    elem: this,
                    trigger:'click',
                    obj:$(this).attr("obj"),
                    done: function(value, date, endDate){
                        obj=JSON.parse($(this).attr("obj"));
                        saveChange(obj,value,"to_date");
                    }
                });
            })

            function saveChange(obj,v,field_name) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Url::toRoute(['/manager/score/scorerule/change-detail-score']); ?>",
                    data: {
                        '_csrf':$('meta[name="csrf-token"]').attr("content"),
                        'item_id':obj.item_id,
                        "field_name":field_name,
                        "value":v,
                    },
                    success: function (result) {
                        layer.msg('success!', {icon: 1, time: 1000});
                    },
                    error: function (e) {
                        layer.msg('Failed!', {icon: 1, time: 1000});
                    }
                });
            }

            $('input[name="list_score"]').blur(function(){
                obj=JSON.parse($(this).attr("obj"));
                saveChange(obj,$(this).val(),"list_score");
            });

            $('input[name="special_score"]').blur(function(){
                obj=JSON.parse($(this).attr("obj"));
                saveChange(obj,$(this).val(),"special_score");
            });

            $('input[name="birthday_score"]').blur(function(){
                obj=JSON.parse($(this).attr("obj"));
                saveChange(obj,$(this).val(),"birthday_score");
            });
        }
    );
    </script>
  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>