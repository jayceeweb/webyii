<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>


        <style>
            .Recommended{
                width: auto !important;
                height: 30px;
                padding: 0;
                text-align: left;
                line-height: 30px;
                display: inline-block;
                margin-right: 15px;
            }
            .Recommended_input{
                display: inline-block;
                height: 30px;
            }
            .Recommended_box{
                /*width: 600px;*/
                display: inline-block;
            }
            .box{
                /*display: flex;*/
                /*justify-content: space-between;*/
                margin-top: 15px;
            }
            .box_list{
                float: left;
                margin-bottom: 10px;
                margin-right: 10px;
            }
            .btn-seach1{
                margin-left: 20px;
                height: 30px;
                line-height: 30px;
            }
              .img_box{
                  width: 20px;
                  height: 20px;
              }
            .mobel_juli{
                height: auto !important;
            }
            .mobel_juli button{
                margin-left: 0px !important;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="email"  placeholder="Customer Email" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['email']?>">
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="recommended">
                                    <option <?php if ($condition["recommended"]==""):?>selected<?php endif; ?> value="">Recommended</option>
                                    <option <?php if ($condition["recommended"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["recommended"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="confirm_email_confirm">
                                    <option <?php if ($condition["confirm_email_confirm"]==""):?>selected<?php endif; ?> value="">Confirmed Agreement</option>
                                    <option <?php if ($condition["confirm_email_confirm"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["confirm_email_confirm"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block zIndex">
                                <select name="enabled_score" id="enabled_score">
                                    <option <?php if ($condition["enabled_score"]==""):?>selected<?php endif; ?> value="">Enable Points</option>
                                    <option <?php if ($condition["enabled_score"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["enabled_score"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="discount_amount" id="discount_amount">
                                    <option <?php if ($condition["discount_amount"]==""):?>selected<?php endif; ?> value="">Discount</option>
                                    <option <?php if ($condition["discount_amount"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["discount_amount"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_clone" id="is_clone">
                                    <option <?php if ($condition["is_clone"]==""):?>selected<?php endif; ?> value="">Reorder</option>
                                    <option <?php if ($condition["is_clone"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_clone"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <br>


                            <div class="box">

                                <div class="box_list">
                                    <label for="L_src_type" class="layui-form-label Recommended">Discount Amount</label>
                                    <input type="datetime-local" name="discountamountstart_time" class="Recommended_input"  style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['discountamountstart_time']?>">
                                    <input type="datetime-local" name="discountamountend_time" class="Recommended_input"  style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['discountamountend_time']?>">
                                </div>
                                <div class="box_list">
                                    <label for="L_src_type" class="layui-form-label Recommended">Recommended Date</label>
                                    <div class="layui-inline layui-show-xs-block Recommended_box" style="z-index: 555">
                                        <input type="datetime-local" name="recommendstart_time" class="Recommended_input"  style="width: 200px;"  autocomplete="off" class="layui-input"  value="<?php echo $condition['recommendstart_time']?>">
                                        <input type="datetime-local" name="recommendend_time" class="Recommended_input"  style="width: 200px;"  autocomplete="off" class="layui-input"  value="<?php echo $condition['recommendend_time']?>">
                                    </div>
                                </div>
                                <div class="box_list">
                                    <label for="L_src_type" class="layui-form-label">Confirm Date</label>
                                    <div class="layui-inline layui-show-xs-block Recommended_box" style="z-index: 555">
                                    <input type="datetime-local" name="confirmstart_time" class="Recommended_input"  style="width: 200px;"  autocomplete="off" class="layui-input"  value="<?php echo $condition['confirmstart_time']?>">
                                    <input type="datetime-local" name="confirmend_time" class="Recommended_input"  style="width: 200px;"  autocomplete="off" class="layui-input"  value="<?php echo $condition['confirmend_time']?>">
                                    </div>
                                </div>
                                <div class="layui-inline layui-show-xs-block btn-seach1">
                                    <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </div>
                        </form>
                    </div>


                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer Email</th>
                                <th>Available Points</th>
                                <th>Total Points</th>
                                <th>Recommended(<?php echo $rows_cnts['count_recommended']?>)</th>
                                <th>Recommended Date</th>
                                <th>Confirm Aggrement(<?php echo $rows_cnts['count_confirm_email_confirm']?>)</th>
                                <th>Confirm Date</th>
                                <th>Enable Points(<?php echo $rows_cnts['count_enabled_score']?>)</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td>
                                        <?php echo $row["entity_id"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["email"]; ?><br>
                                    </td>
                                    <td>
                                        <?php if($row["total_score"]){
                                            echo $row["total_score"];
                                        }else{
                                            echo 0;
                                        }?>
                                    </td>
                                    <td>
                                        <?php if($row["all_score"]){
                                            echo $row["all_score"];
                                        }else{
                                            echo 0;
                                        }?>
                                    </td>
                                    <td>
                                        <?php if($row["recommended"]){ ?>
                                            <input type="checkbox" name="<?php echo $row['entity_id'];?>" disabled checked="checked">
                                        <?php }else{ ?>
                                            <input type="checkbox" name="<?php echo $row['entity_id'];?>"  disabled>
                                        <?php }?>
                                    </td>
                                    <td>
                                    <?php if($row["recommend_date"] && $row["recommend_date"] != '0000-00-00 00:00:00'){ ?>
                                        <?php
                                        $row["recommend_date"] = date("Y-m-d",strtotime($row["recommend_date"]));
                                        echo $row["recommend_date"]; ?>
                                    <?php }?>
                                    </td>
                                    <td>
                                        <?php if($row["confirm_email_confirm"]){ ?>
                                            <input type="checkbox" disabled checked="checked">
                                        <?php }else{ ?>
                                            <input type="checkbox" disabled>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?php if($row["confirm_date"]&& $row["confirm_date"] != '0000-00-00 00:00:00'){ ?>
                                            <?php
                                                $row["confirm_date"] = date("Y-m-d",strtotime($row["confirm_date"]));
                                                echo $row["confirm_date"]; ?>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if($row["enabled_score"]){ ?>
                                            <input type="checkbox" disabled checked="checked">
                                        <?php }else{ ?>
                                            <input type="checkbox" disabled>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <div class="layui-card-header mobel_juli">
                                            <button class="layui-btn" onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/score/user-score/add','email'=>$row["email"], 'all_score'=> $row["all_score"]]); ?>',800,600)">
                                                <a href="#" title="Operation">
                                                    <img src="/assets2/resource/img/manage.png" alt="Operation" class="img_box">
                                                </a>
                                            </button>
                                            <button class="layui-btn" onclick="javascript:pointInit(<?php echo $row['entity_id']; ?>)">
                                                <a href="#" title="Point init">
                                                    <img src="/assets2/resource/img/pointer.png" alt="Point init" class="img_box">
                                                </a>
                                            </button>
                                            <?php if ($row["enabled_score"]==0): ?>
                                                <button class="layui-btn" onclick="javascript:EnablePoints(<?php echo $row['entity_id']; ?>,1)">
                                                    <a href="#" title="Enable Points">
                                                        <img src="/assets2/resource/img/Enable.png" alt="Enable Points" class="img_box">
                                                    </a>
                                                </button>
                                            <?php else: ?>
                                                <button class="layui-btn" onclick="javascript:EnablePoints(<?php echo $row['entity_id']; ?>,0)">
                                                    <a href="#" title="Disable Points">
                                                        <img src="/assets2/resource/img/disable.png" alt="Disable Points" class="img_box">
                                                    </a>
                                                </button>
                                            <?php endif; ?>
<!--                                            add sunjinchao 20/3/30 积分校验-->
                                            <button class="layui-btn" onclick="javascript:Revise(<?php echo $row['entity_id']; ?>)">
                                                <a href="#" title="Revise">
                                                    <img src="/assets2/resource/img/revise.png" alt="Revise" class="img_box">
                                                </a>
                                            </button>
<!--                                                add sunjinchao 20/6/9 用户邀请按钮-->
                                            <?php if($row["recommended"] !=1 ){ ?>
                                                <button class="layui-btn" onclick="javascript:recommend(<?php echo $row['entity_id']; ?>)">
                                                    <a href="#" title="Revise">
                                                        <img src="/assets2/resource/img/update_score.png" alt="recommend" class="img_box">
                                                    </a>
                                                </button>
                                            <?php }?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        /**
         * 用户积分初始化
         * @param entity_id
         */
        function pointInit(entity_id) {
            layer.confirm('Are you confirm to init this customer\'t points？',
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['//manager/score/user-score/point-init']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'entity_id':entity_id
                        },
                        success: function (result) {
                            if (result.code==0) {
                                layer.msg('success!', {icon: 1, time: 1000});
                                window.location.reload();
                            } else {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }
        function recommend(entity_id) {
            confirm_message="Are you sure you want to invite this member?";
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['//manager/score/user-score/recommend']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'entity_id':entity_id,
                        },
                        success: function (result) {
                            if (result.code==0) {
                                layer.msg('success!', {icon: 1, time: 1000});
                                window.location.reload();
                            } else {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }
        function Revise(entity_id){
            confirm_message="Are you sure you want to correct the user's credits?";
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['//manager/score/user-score/revise']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'entity_id':entity_id,
                        },
                        success: function (result) {
                            if (result.code==0) {
                                layer.msg('success!', {icon: 1, time: 1000});
                                window.location.reload();
                            } else {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }
        /**
         * 启用积分
         * @param entity_id
         * @constructor
         */
        function EnablePoints(entity_id,enabled) {
            if (enabled==1) {
                confirm_message="Are you confirm to enable this customer points?";
            } else {
                confirm_message="Are you confirm to disable this customer points?";
            }
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['//manager/score/user-score/point-enable']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'entity_id':entity_id,
                            'enabled':enabled
                        },
                        success: function (result) {
                            console.log(result.code);
                            if (result.code==0) {
                                window.location.reload();
                            } else {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }

        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;
                lay(".from_date").each(function () {
                    laydate.render({
                        elem: this,
                        trigger:'click',
                        obj:$(this).attr("obj"),
                        done: function(value, date, endDate){
                            obj=JSON.parse($(this).attr("obj"));
                            saveChange(obj,value,"from_date");
                        }
                    });
                })
                lay(".to_date").each(function () {
                    laydate.render({
                        elem: this,
                        trigger:'click',
                        obj:$(this).attr("obj"),
                        done: function(value, date, endDate){
                            obj=JSON.parse($(this).attr("obj"));
                            saveChange(obj,value,"to_date");
                        }
                    });
                })

                function saveChange(obj,v,field_name) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/score/scorerule/change-detail-score']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'item_id':obj.item_id,
                            "field_name":field_name,
                            "value":v,
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                }

                $('input[name="list_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"list_score");
                });

                $('input[name="special_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"special_score");
                });

                $('input[name="birthday_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"birthday_score");
                });
            }
        );
    </script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>