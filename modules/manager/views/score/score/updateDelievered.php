<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">
                        <span class="x-red">*</span>Increment Id</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_increment_id" name="increment_id" required="" autocomplete="off" class="layui-input" value="<?php echo $increment_id; ?>">
                        <input type="hidden" id="l_ids" required="" name = "ids" autocomplete="off" class="layui-input" value="<?php echo $id; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">
                        <span class="x-red">*</span>Delievered Time</label>
                    <div class="layui-input-inline">
                        <input type="datetime-local" id="l_delievered_time" name="delievered_time" required="" autocomplete="off" class="layui-input" value="<?php echo substr(str_replace(" ", "T", $delievered_time),0,strlen(str_replace(" ", "T", $delievered_time))-3); ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>

                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button class="layui-btn" lay-filter="add" lay-submit="">Save</button></div>
            </form>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;

                laydate.render({
                    elem: '#start_at' //指定元素
                });
                laydate.render({
                    elem: '#end_at' //指定元素
                });

                //监听提交
                form.on('submit(add)',
                    function(data) {
                        var id_val = $("#l_ids").val();
                        console.log(id_val)
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Url::toRoute(['/manager/score/score/delievered-save']); ?>",
                            data: {
                                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                                'ids':id_val,
                                'delievered_time':$("#l_delievered_time").val(),
                            },
                            success: function (result) {
                                layer.alert("Success", {
                                        icon: 6
                                    },
                                    function() {
                                        //关闭当前frame
                                        xadmin.close();

                                        // 可以对父窗口进行刷新
                                        xadmin.father_reload();
                                    }
                                );
                            },
                            error: function (e) {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        });
                        return false;
                    });
                form.on('select(test_xs)', function(data){
                    if(data.value=="customer_entity"){
                        $("#l_increment_id").hide()
                    }
                    else if(data.value=="sales_order"){
                        $("#l_increment_id input").attr("placeholder","Please Input Order Number");
                    } else{
                        $("#l_increment_id").show()
                        $("#l_increment_id input").attr("placeholder","Please checkout the type");
                    }
                    var val_s = $("#l_increment_id_s input").attr("val_s");
                    if(data.value=="sales_order"){
                        $("#l_increment_id_s input").attr("disabled","disabled");
                        $("#l_increment_id_s input").val(val_s);
                    }else{
                        $("#l_increment_id_s input").removeAttr("disabled");
                        $("#l_increment_id_s input").val("");
                    }
                });
            });</script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>