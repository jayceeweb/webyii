<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">
                        <span class="x-red">*</span>customer email</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_customer_email" <?php if($email){echo 'disabled';} ?> name="customer_email" required="" autocomplete="off" class="layui-input" value="<?php echo $email; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>

                    <div class="layui-form-item">
                        <label for="L_src_type" class="layui-form-label">
                            <span class="x-red">*</span>type</label>
                        <div class="layui-input-inline">
                            <?php if(!$increment_id && !$email){ ?>
                            <select name="src_type" id="l_src_type" lay-filter="test_xs">
                                <option value="" selected ></option>
                                <option value="sales_order">sales_order (销售订单)</option>
                                <option value="customer_entity">customer_entity (用户)</option>
                            </select>
                            <?php }elseif ($increment_id){ ?>
                                <select name="src_type" id="l_src_type" lay-filter="test_xs">
                                    <option value="" selected ></option>
                                    <option value="sales_order">sales_order (销售订单)</option>
                                    <option value="customer_entity" disabled="disabled">customer_entity (用户)</option>
                                </select>
                            <?php }elseif ($email){ ?>
                                <select name="src_type" id="l_src_type" disabled="disabled">
                                    <option value="customer_entity">customer_entity (用户)</option>
                                </select>
                            <?php } ?>
                        </div>
                    </div>



                <?php if($increment_id){  ?>
                    <div class="layui-form-item" id="l_increment_id_s">
                        <label for="L_increment_id" class="layui-form-label">order number</label>
                        <div class="layui-input-inline">
                            <input type="text"  name="increment_id"  required="" lay-verify="reason" autocomplete="off" class="layui-input items_val" value="" val_s="<?php echo $increment_id; ?>"></div>
                    </div>
                <?php }elseif($email){ ?>
                    <div class="layui-form-item" id="l_increment_id_s" >
                        <div class="layui-input-inline">
                            <input type="text" name="increment_id" style="display: none"  required="" lay-verify="reason" autocomplete="off" class="layui-input items_val" value="" val_s="<?php echo $increment_id; ?>"></div>
                    </div>
                <?php }else if(!$increment_id && !$email){ ?>
                    <div class="layui-form-item"  id="l_increment_id">
                        <label for="L_increment_id" class="layui-form-label">order number</label>
                        <div class="layui-input-inline">
                            <input type="text" name="increment_id" required="" lay-verify="reason" autocomplete="off" class="layui-input items_val" placeholder="Please checkout the type" value="<?php echo $increment_id; ?>"></div>
                    </div>
                <?php }?>
                <div class="layui-form-item">
                    <label for="L_src_type" class="layui-form-label">
                        <span class="x-red">*</span>operation</label>
                    <div class="layui-input-inline">
                        <select name="operation" id="l_operation">
                            <option value="0" <?php if ($operation=="0"):?>selected<?php endif; ?>>add ( + )</option>
                            <option value="1" <?php if ($operation=="1"):?>selected<?php endif; ?>>sub ( - )</option>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_src_type" class="layui-form-label">
                        <span class="x-red">*</span>score status</label>
                    <div class="layui-input-inline">
                        <select name="score_status" id="l_score_status">
                            <option value="0">pending</option>
                            <!--
                            防止失误，暂不允许直接生效
                            <option value="1">active</option>
                            -->
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_score" class="layui-form-label">score</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_score" name="score" required="" autocomplete="off" class="layui-input" value="<?php echo $score; ?>"></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_note" class="layui-form-label"><span class="x-red">*</span>note</label>
                    <div class="layui-input-inline">
                        <textarea type="text" id="l_note" style="width: 300px;height: 100px;" name="note" required="" autocomplete="off" class=""></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button class="layui-btn" lay-filter="add" lay-submit="">Save</button></div>
            </form>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;

                laydate.render({
                    elem: '#start_at' //指定元素
                });
                laydate.render({
                    elem: '#end_at' //指定元素
                });

                //监听提交
                form.on('submit(add)',
                    function(data) {
                        var note = $("#l_note").val();
                        if(note.length <= 0){
                            layer.msg('Cant be empty!', {icon: 7, time: 1000});
                            $("#l_note").css({
                                border:"1px solid red"
                            });
                            setTimeout(function(){
                                $("#l_note").css({
                                    border:"1px solid #e6e6e6"
                                });
                            },2000);
                            return;
                        }

                        //发异步，把数据提交给php
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Url::toRoute(['/manager/score/score/save']); ?>",
                            data: {
                                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                                'customer_email':$("#l_customer_email").val(),
                                'src_type':$("#l_src_type").val(),
                                'increment_id':$(".items_val").val(),
                                'operation':$("#l_operation").val(),
                                'score_status':$("#l_score_status").val(),
                                'score':$("#l_score").val(),
                                'note':$("#l_note").val(),
                            },
                            success: function (result) {
                                layer.alert("Success", {
                                        icon: 6
                                    },
                                    function() {
                                        //关闭当前frame
                                        xadmin.close();

                                        // 可以对父窗口进行刷新
                                        xadmin.father_reload();
                                    }
                                );
                            },
                            error: function (e) {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        });
                        return false;
                    });
                form.on('select(test_xs)', function(data){
                    if(data.value=="customer_entity"){
                        $("#l_increment_id").hide()
                    }
                    else if(data.value=="sales_order"){
                        $("#l_increment_id input").attr("placeholder","Please Input Order Number");
                    } else{
                        $("#l_increment_id").show()
                        $("#l_increment_id input").attr("placeholder","Please checkout the type");
                    }
                    var val_s = $("#l_increment_id_s input").attr("val_s");
                    if(data.value=="sales_order"){
                        $("#l_increment_id_s input").attr("disabled","disabled");
                        $("#l_increment_id_s input").val(val_s);
                    }else{
                        $("#l_increment_id_s input").removeAttr("disabled");
                        $("#l_increment_id_s input").val("");
                    }
                });
            });</script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>