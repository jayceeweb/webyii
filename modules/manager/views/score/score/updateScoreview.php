<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">
                        <span class="x-red">*</span>Order Number</label>
                    <div class="layui-input-inline">
                        <input type="hidden" id="l_src_id" disabled name="src_id" autocomplete="off" class="layui-input" value="<?php echo $src_id; ?>">
                        <input type="text" id="l_increment_id" disabled name="increment_id" autocomplete="off" class="layui-input" value="<?php echo $increment_id; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">
                        <span class="x-red">*</span>score</label>
                    <div class="layui-input-inline">
                        <input type="hidden" id="l_old_score" name="old_score" required="" autocomplete="off" class="layui-input" value="<?php echo $score; ?>">
                        <input type="text" id="l_new_score" name="new_score" required="" autocomplete="off" class="layui-input" value="<?php echo $score; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <div class="layui-btn layui-btn_s" >Save</div></div>
                </div>

            </form>
        </div>
    </div>


    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;
                var old_score = $("#l_old_score").val();
                $(".layui-btn_s").on("click",function () {
                    var arr = [];
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/score/score/update-score']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'src_id':$("#l_src_id").val(),
                            'old_score':$("#l_old_score").val(),
                            'new_score':$("#l_new_score").val(),
                        },
                        success: function (result) {
                            console.log(result);
                            if (result.code==0) {
                                layer.msg('success!', {icon: 1, time: 1000});
//                                window.location.reload();
                            } else {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        }
                    });
                    console.log(arr);
                });
            })
    </script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>