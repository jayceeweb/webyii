<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">
  
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      <?php
      $this->registerCsrfMetaTags();
      $this->head();
      ?>

      <style>
          .img_box{
              width: 20px;
              height: 20px;
          }
      </style>
  </head>
  
  <body>
  <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
  <div class="layui-fluid">
      <div class="layui-row layui-col-space15">
          <div class="layui-col-md12">
              <div class="layui-card">
                  <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5" method="get">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="email"  placeholder="Customer Email" style="width: 200px" autocomplete="off" class="layui-input" value="<?php echo $condition['email']; ?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="increment_id"  placeholder="Order Number" autocomplete="off" class="layui-input" value="<?php echo $condition['increment_id']; ?>">
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="discount_amount" id="discount_amount">
                                    <option <?php if ($condition["discount_amount"]==""):?>selected<?php endif; ?> value="">Has Discount</option>
                                    <option <?php if ($condition["discount_amount"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["discount_amount"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div><br>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="review" id="review">
                                    <option <?php if ($condition["review"]==""):?>selected<?php endif; ?> value="">Review</option>
                                    <option <?php if ($condition["review"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["review"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_remake" id="is_remake">
                                    <option <?php if ($condition["is_remake"]==""):?>selected<?php endif; ?> value="">Is Remake</option>
                                    <option <?php if ($condition["is_remake"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_remake"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <input type="text" placeholder="Remake order number" style="width: 200px" autocomplete="off" class="layui-input" name="remake_order_number" id="remake_order_number" value="<?php echo $condition["remake_order_number"];?>"></input>
                            </div><br>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="qty_score_status" id="qty_score_status">
                                    <option <?php if ($condition["score_status"]==""):?>selected<?php endif; ?> value="">Score Status</option>
                                    <option <?php if ($condition["score_status"]=="0"):?>selected<?php endif; ?> value="0">pending</option>
                                    <option <?php if ($condition["score_status"]=="1"):?>selected<?php endif; ?> value="1">active</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="active start time" name="active_start_time" id="active_start_time" value="<?php echo $condition["active_start_time"];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="active end time" name="active_end_time" id="active_end_time" value="<?php echo $condition["active_end_time"];?>">
                            </div><br>
                            <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="start bill time" name="start_bill_time" id="start_bill_time" value="<?php echo $condition["start_bill_time"];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="end bill time" name="end_bill_time" id="end_bill_time" value="<?php echo $condition["end_bill_time"];?>">
                            </div><br>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_delievered" id="is_delievered">
                                    <option <?php if ($condition["is_delievered"]==""):?>selected<?php endif; ?> value="">Is Delievered</option>
                                    <option <?php if ($condition["is_delievered"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_delievered"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="start delievered time" name="start_delievered_time" id="start_delievered_time" value="<?php echo $condition["start_delievered_time"];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input class="layui-input"  autocomplete="off" placeholder="end delievered time" name="end_delievered_time" id="end_delievered_time" value="<?php echo $condition["end_delievered_time"];?>">
                            </div><br>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_recommend" id="is_recommend">
                                    <option <?php if ($condition["is_recommend"]==""):?>selected<?php endif; ?> value="">Is Recommend</option>
                                    <option <?php if ($condition["is_recommend"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_recommend"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_email_confirm" id="is_email_confirm">
                                    <option <?php if ($condition["is_email_confirm"]==""):?>selected<?php endif; ?> value="">Is Email Confirm</option>
                                    <option <?php if ($condition["is_email_confirm"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_email_confirm"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_enabled_score" id="is_enabled_score">
                                    <option <?php if ($condition["is_enabled_score"]==""):?>selected<?php endif; ?> value="">Is Enabled Score</option>
                                    <option <?php if ($condition["is_enabled_score"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_enabled_score"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                </select>
                            </div><br>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="has_coupon_code" id="has_coupon_code">
                                    <option <?php if ($condition["has_coupon_code"]==""):?>selected<?php endif; ?> value="">Has Coupon Code</option>
                                    <option <?php if ($condition["has_coupon_code"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["has_coupon_code"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_deleted" id="is_deleted">
                                    <option <?php if ($condition["is_deleted"]==""):?>selected<?php endif; ?> value="">Delete Bill</option>
                                    <option <?php if ($condition["is_deleted"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_deleted"]=="2"):?>selected<?php endif; ?> value="2">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="coupon_code" id="coupon_code"  placeholder="Coupon Code" autocomplete="off" class="layui-input" value="<?php echo $condition['coupon_code']; ?>">
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="has_warranty" id="is_deleted">
                                    <option <?php if ($condition["has_warranty"]==""):?>selected<?php endif; ?> value="">Has Warranty</option>
                                    <option <?php if ($condition["has_warranty"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["has_warranty"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                </select>
                            </div><br>
                              <div class="layui-inline layui-input-inline layui-show-xs-block">
                                  <select name="has_warranty" id="is_deleted">
                                      <option <?php if ($condition["has_warranty"]==""):?>selected<?php endif; ?> value="">Has Warranty</option>
                                      <option <?php if ($condition["has_warranty"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                      <option <?php if ($condition["has_warranty"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                  </select>
                              </div>
                              <div class="layui-inline layui-input-inline layui-show-xs-block">
                                  <select name="src_type" id="src_type">
                                      <option <?php if ($condition["src_type"]==""):?>selected<?php endif; ?> value="">Src Type</option>
                                      <?php foreach ($type as $key => $val){ ?>
                                          <option <?php if ($condition["src_type"]==$val["src_type"]):?>selected<?php endif; ?> value="<?php echo $val["src_type"]?>"><?php echo $val["src_type"]?></option>
                                      <?php } ?>
                                  </select>
                              </div>
                            <div class="layui-inline layui-show-xs-block">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                            <a href="<?php echo Url::toRoute(['/manager/score/score/data','condition'=>$condition]); ?>">导出当前积分列表</a>
                            <a href="<?php echo Url::toRoute(['/manager/score/score/sales-order-data','condition'=>$condition]); ?>">导出订单保险</a>
                        </form>
                    </div>
                  <div class="layui-card-header">
                      <button class="layui-btn" onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/score/score/add']); ?>',800,600)"><i class="layui-icon"></i>New</button>
                  </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                          <thead>
                            <tr>
                              <th>Customer Info</th>
                              <th>Order Number</th>
                              <th>Status:Score</th>
                              <th>Discount</th>
                              <th>Time</th>
                              <th>Note</th>
                              <th>Review</th>
                              <th>Info</th>
                              <th>Action</th>
                          </thead>

                          <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                            <tr>
                                <td nowrap="">

                                    <button class="layui-btn"  style="margin-right: 10px"    onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/score/score/add','customer_id'=>$row['customer_id'],'email'=>$row['email']]); ?>',800,600)">
                                        <a href="#" title="manager this customer score">
                                            <img src="/assets2/resource/img/manage.png" alt="manager this customer score" class="img_box">
                                        </a>
                                    </button>
                                    <br>
                                    email: <?php echo $row["email"]; ?><br>
                                    group: <?php echo $row["group_id"]; ?><br>
                                    recommended: <?php if ($row["recommended"]) {echo "Yes";} else {echo "No";}; ?><br>
                                    email_confirm: <?php if ($row["confirm_email_confirm"]) {echo "Yes";} else {echo "No";}; ?><br>
                                    enabled_score: <?php if ($row["enabled_score"]) {echo "Yes";} else {echo "No";}; ?><br>
                                </td>
                                <td nowrap="">
                                    src_type:<?php echo $row["src_type"];?><br>
                                    <?php if ($row["src_type"]=="sales_order"): ?>
                                        increment_id: <?php echo $row["src_obj"]["src_obj"]["increment_id"]; ?><br>
                                        coupon code: <?php if(isset($row["coupon_code"])) {echo $row["coupon_code"];}; ?><br>
                                    <?php elseif($row["src_type"]=="sales_point_for"): ?>
                                        increment_id: <?php echo $row["src_obj"]["src_obj"]["increment_id"]; ?><br>
                                        coupon code: <?php if(isset($row["coupon_code"])) {echo $row["coupon_code"];}; ?><br>
                                    <?php endif; ?>
                                </td>
                              <td>
                                  <?php if($row["score_status"]==0): ?>
                                  pending
                                  <?php elseif($row["score_status"]==1): ?>
                                  active
                                  <?php endif; ?>
                                  <?php echo ':'.$row["score"]; ?>
                              </td>
                                <td>
                                    <?php if($row["src_type"]=="sales_order" && isset($row["src_obj"]["src_obj"]["discount_amount"])){  ?>
                                        <?php if ($row["src_obj"]["src_obj"]["discount_amount"]==0): ?>
                                            <input type="checkbox"  disabled>
                                        <?php elseif($row["src_obj"]["src_obj"]["discount_amount"]<0): ?>
                                            <input type="checkbox" disabled checked="checked">
                                        <?php endif; ?>
                                    <?php }?>
                                </td>
                              <td nowrap="">
                                  Created At: <?php echo $row["created_at"]; ?><br>
                                  Bill Time: <?php echo $row["bill_time"]; ?><br>
                                  Active Time: <?php echo $row["active_time"]; ?><br>
                                  Delievered Time: <?php echo $row["delievered_time"]; ?>
                                  <?php if($row["src_type"] == 'sales_order'){ ?>
                                  <button class="layui-btn"  onclick="xadmin.open('Delievered Time','<?php echo Url::toRoute(['/manager/score/score/update-delievered','id'=>$row['id'],'increment_id'=>$row["src_obj"]["src_obj"]["increment_id"],'delievered_time'=>$row['delievered_time']]); ?>',800,600)">
                                      <!--manager this order score-->
                                      <a href="#" title="update this order delievered_time">
                                          <img src="/assets2/resource/img/revise.png" alt="update this order delievered_time" class="img_box">
                                      </a>
                                  </button>
                                  <?php } ?>
                                  <br>
                              </td>
                              <td><?php echo $row["note"]; ?></td>
                                <td>
                                    Review:
                                    <?php if ($row["review"]==0): ?>
                                        <input type="checkbox"  disabled>
                                    <?php elseif($row["review"]>0): ?>
                                        <input type="checkbox" disabled checked="checked">
                                    <?php endif; ?>
                                    <br>
                                    Review User: <?php echo $row["review_user"]; ?><br>
                                </td>
                                <td>
                                    Is Delete:
                                    <?php if ($row["is_deleted"]==0): ?>
                                        <input type="checkbox"  disabled>
                                    <?php elseif($row["is_deleted"] == 1): ?>
                                        <input type="checkbox" disabled checked="checked">
                                    <?php endif; ?>
                                    <br>
                                    Delete At: <?php echo $row["deleted_datetime"]; ?><br>
                                </td>
                              <td class="td-manage">
                                  <?php $re_item = array(); ?>
                                  <?php if ($row["src_type"]=="sales_order"): ?>
                                      <?php foreach ($row["re_obj"] as $key => $val){
                                          if($val['remake_order'] && $val['item_id']){
                                              $re_item[$val['remake_order']] = $val['item_id'];
                                          }
                                      } ?>
                                  <?php endif; ?>
                                  <?php if ($row["src_type"]=="sales_order"): ?>
                                      <button class="layui-btn"  onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/score/score/add','customer_id'=>$row['customer_id'],'email'=>$row['email'],'src_type'=>'sales_order','increment_id'=>$row["src_obj"]["src_obj"]["increment_id"],'operation'=>'1','score'=>$row['score']]); ?>',800,600)">
                                            <!--manager this order score-->
                                          <a href="#" title="manager this order score">
                                              <img src="/assets2/resource/img/manage.png" alt="manager this order score" class="img_box">
                                          </a>
                                      </button>
                                  <?php endif; ?>
                                  <?php if ($row["score_status"]==0): ?>
                                      <button class="layui-btn"  onclick="activeScoreBill('<?php echo $row["id"];?>','<?php echo $row["src_type"];?>','<?php echo $row["src_id"];?>')">
                                                <!-- active this score-->
                                          <a href="#" title="active this score">
                                          <img src="/assets2/resource/img/active.png" alt="active this score" class="img_box">
                                              </a>
                                      </button>
                                  <?php endif; ?>
                                  <?php if($row['additional_data']){ ?>
                                      <button class="layui-btn"  onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/score/score/item','id'=>$row['id'],'review'=>$row['review'],'item'=>$row['src_id'],'re'=>json_encode($re_item)]); ?>',800,600)">
                                          <!--Detail-->
                                          <a href="#" title="Detail">
                                              <img src="/assets2/resource/img/detail.png" alt="Detail" class="img_box">
                                          </a>
                                      </button>
                                      <button class="layui-btn"  onclick="xadmin.open('Update Score','<?php echo Url::toRoute(['/manager/score/score/update-scoreview','src_id'=>$row['id'],'increment_id'=>$row["src_obj"]["src_obj"]["increment_id"],'score'=>$row["score"]]); ?>',800,600)">
                                          <!--Detail-->
                                          <a href="#" title="Updata Score">
                                              <img src="/assets2/resource/img/update_score.png" alt="Update Score" class="img_box">
                                          </a>
                                      </button>
                                  <?php } ?>
                                  <button class="layui-btn"  onclick="activeDeleteScoreBill('<?php echo $row["id"];?>','<?php echo $row["is_deleted"];?>')">
                                      <!-- active this score-->
                                      <?php if($row["is_deleted"] == 1){?>
                                          <a href="#" title="restore this record">
                                              <img src="/assets2/resource/img/Enable.png" alt="active this score" class="img_box">
                                          </a>
                                      <?php }else if($row["is_deleted"] == 0){ ?>
                                          <a href="#" title="delete this record">
                                              <img src="/assets2/resource/img/disable.png" alt="active this score" class="img_box">
                                          </a>
                                      <?php } ?>
                                  </button>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                          </tbody>
                        </table>
                    </div>
                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script>
    layui.use(['form', 'layer','jquery','laydate'],
        function() {
            $ = layui.jquery;
            var laydate = layui.laydate;
            var form = layui.form;
            var layer = layui.layer;
            lay(".from_date").each(function () {
                laydate.render({
                    elem: this,
                    trigger:'click',
                    obj:$(this).attr("obj"),
                    done: function(value, date, endDate){
                        obj=JSON.parse($(this).attr("obj"));
                        saveChange(obj,value,"from_date");
                    }
                });
            })
            lay(".to_date").each(function () {
                laydate.render({
                    elem: this,
                    trigger:'click',
                    obj:$(this).attr("obj"),
                    done: function(value, date, endDate){
                        obj=JSON.parse($(this).attr("obj"));
                        saveChange(obj,value,"to_date");
                    }
                });
            })

            function saveChange(obj,v,field_name) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Url::toRoute(['/manager/score/scorerule/change-detail-score']); ?>",
                    data: {
                        '_csrf':$('meta[name="csrf-token"]').attr("content"),
                        'item_id':obj.item_id,
                        "field_name":field_name,
                        "value":v,
                    },
                    success: function (result) {
                        layer.msg('success!', {icon: 1, time: 1000});
                    },
                    error: function (e) {
                        layer.msg('Failed!', {icon: 1, time: 1000});
                    }
                });
            }

            $('input[name="list_score"]').blur(function(){
                obj=JSON.parse($(this).attr("obj"));
                saveChange(obj,$(this).val(),"list_score");
            });

            $('input[name="special_score"]').blur(function(){
                obj=JSON.parse($(this).attr("obj"));
                saveChange(obj,$(this).val(),"special_score");
            });

            $('input[name="birthday_score"]').blur(function(){
                obj=JSON.parse($(this).attr("obj"));
                saveChange(obj,$(this).val(),"birthday_score");
            });

            var laydate = layui.laydate;

            laydate.render({
                elem: '#active_end_time' //指定元素
            });

            laydate.render({
                elem: '#active_start_time' //指定元素
            });

            laydate.render({
                elem: '#start_bill_time' //指定元素
            });

            laydate.render({
                elem: '#end_bill_time' //指定元素
            });

            laydate.render({
                elem: '#start_delievered_time' //指定元素
            });

            laydate.render({
                elem: '#end_delievered_time' //指定元素
            });
        }
    );
    function activeDeleteScoreBill(id,deletecode) {
        if(deletecode == 1){
            var msg = 'You are sure to restore this record？';
        }else{
            var msg = 'You are sure to delete this record？';
        }
        layer.confirm(msg,
        function(index) {
            $.ajax({
                type: "POST",
                url: "<?php echo Url::toRoute(['/manager/score/score/delete-bill']); ?>",
                data: {
                    '_csrf':$('meta[name="csrf-token"]').attr("content"),
                    'id':id,
                    'deletecode':deletecode
                },
                success: function (result) {
                    layer.msg('success!', {icon: 1, time: 1000});
                    window.location.reload();
                },
            });
        });
    }
    /**
     * 积分生效
     */
    function activeScoreBill(id) {
        layer.confirm('Are you confirm active this score？',
            function(index) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Url::toRoute(['/manager/score/score/score-active']); ?>",
                    data: {
                        '_csrf':$('meta[name="csrf-token"]').attr("content"),
                        'id':id
                    },
                    success: function (result) {
                        layer.msg('success!', {icon: 1, time: 1000});
                        window.location.reload();
                    },
                    error: function (e) {
                        layer.msg('Failed!', {icon: 1, time: 1000});
                    }
                });
        });
    }
    </script>
  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>