<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    $this->registerCsrfMetaTags();
    $this->head();
    ?>


    <style>

        .box{
            width: 98%;
            height: 100%;
            padding: 10px;
            background: #fff;
        }
        #my-orders-table{
            /*border:1px solid #ccc;*/
            width: 100%;
            margin-bottom: 29px;
            background-color: #f2f2f2;
        }
        #my-orders-table th,#my-orders-table td{
            text-align: center;
            line-height: 20px;
            border:1px solid #e6e6e6;
            color: #666;
            font-size: 15px;
            word-break: inherit !important;
            padding: 10px ;
            /*border-bottom:1px solid #000;*/
        }
        #my-orders-table td:last-child{
            border-right:none;
        }
        .tr_class{
            border-top: 1px solid #ccc;
        }
        .a_box{
            /*background: #009688;*/
            /*color: #ccc;*/
            margin-right: 5px;
            /* padding: 10px; */
            display: inline-block;
            margin-bottom: 5px;
            width: 190px;
            text-align: left;

        }
        .a_box>span:nth-of-type(1){
            color: #666;
            font-weight: bold;
        }
        .a_box>span:nth-of-type(2){
            color: red;
        }

        .a_box:hover{
            color: #fff !important;
        }
        .padd{
            padding: 10px;
        }
        .box_input{
            width: 50px;
            display: inline-block;
        }
        .Review{
            width: 60%;
            height: 40px;
            margin: 0 auto;
            background: #009688;
            text-align: center;
            line-height: 40px;
            color: #fff;
            font-weight: bold;
            style="cursor: pointer";
        }

    </style>
</head>
<body>
<?php $this->beginBody() ?>
            <div class="box">

<!--                <table class="data table table-order-items recent" id="my-orders-table">-->
                <?php foreach ($item as $key => $v){ ?>
                    <?php if($v['product_type']=='configurable'){ ?>
<!--                </table>-->
                <table class="data table table-order-items recent" id="my-orders-table">
                        <thead>
                        <tr>
                            <th scope="col" class="col id" colspan="7"> <?php echo $v['name']?></th>
                        </tr>
                        <tr>
                            <th scope="col" class="col date">Sku</th>
                            <th scope="col" class="col shipping">Detail</th>
                            <th scope="col" class="col status">Quantity</th>
                            <th scope="col" class="col status">Discount</th>
                            <th scope="col" class="col status">Reorder</th>
                            <th scope="col" class="col status">Score</th>
                            <th scope="col" class="col status">Total Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="tr_class">
                            <td data-th="Order #" class="col id"><?php echo $v['sku']?></td>
                            <td data-th="Order #" class="col id">
                                <ul class="padd">
<!--                                    --><?php //$i = 1; if(count($v['score_detail'])%2 != 0){$v['score_detail']['aaaa'] ='aaaa';} var_dump($v['score_detail']); ?>
                                    <li>
                                    <?php foreach ($v['score_detail'] as $k => $val){ ?>
                                            <a class="a_box">
                                                <span <?php if($val['special'] == 1){ echo "title='special'";} ?>><?php echo $val['sku']?>:</span>
                                                <span>
                                                    <?php echo $val['val']?> points
                                                </span>
                                            </a>
                                    <?php }?>
                                    </li>


                            </td>
                            <td data-th="Order #" class="col id"><?php echo (int)$v['qty_ordered']?></td>
                            <td data-th="Order #" class="col id">
                                <?php if($v['discount_amounts']){ ?>
                                    <input type="checkbox" checked="checked">
                                <?php }else{ ?>
                                    <input type="checkbox" disabled>
                                <?php }?>
                            </td>
                            <td data-th="Order #" class="col id">
                                <?php foreach ($v['remake_order'] as $keys => $values){
                                    echo $values.'<br>';
                                } ?>
                            </td>
                            <td data-th="Order #" class="col id"><?php echo $v['gaving_score']/$v['qty_ordered']?></td>
                            <td data-th="Order #" class="col id"><?php echo $v['gaving_score']?></td>
                            <td data-th="Order #" class="col id"></td>
                        </tr>
                        <tbody>

                    <?php }else{ ?>
                        <tbody>
                            <tr class="tr_class">
                                <td data-th="Order #" class="col id">accessories</td>
                                <td data-th="Order #" class="col id"><?php echo $v['item_id']?></td>
                                <td data-th="Order #" class="col id"><?php echo $v['sku']?></td>
                                <td data-th="Order #" class="col id"><?php echo $v['name']?></td>
                                <td data-th="Order #" class="col id"><?php echo (int)$v['qty_ordered']?></td>
                                <td data-th="Order #" class="col id"><?php echo $v['gaving_score']/$v['qty_ordered']?></td>
                                <td data-th="Order #" class="col id"><?php echo $v['gaving_score']?></td>
                                <td data-th="Order #" class="col id"></td>
                            </tr>
                        <tbody>
                <?php }} ?>
                </table>



                <?php if($review != 1){ ?>
                    <div class="Review">review</div>
                <?php } ?>
            </div>
<?php $this->endBody() ?>
</body>


<script>
    layui.use(['form', 'layer','jquery','laydate'],
        function() {
            $ = layui.jquery;
            var laydate = layui.laydate;
            var form = layui.form;
            var layer = layui.layer;
            var id = <?php echo $id;?>;

        $(".Review").on("click",function () {
            $.ajax({
                type: "POST",
                url: "<?php echo Url::toRoute(['/manager/score/score/review']); ?>",
                data: {
                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                 'src_id':id,
            },
            success: function (result) {
                    layer.msg('success!', {icon: 1, time: 1000});
                    window.location.reload();
                }
            });
        });





    })



</script>

</html>
<?php $this->endPage() ?>