<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    $this->registerCsrfMetaTags();
    $this->head();
    ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="layui-fluid">
    <div class="layui-row">
        <form class="layui-form">
            <div class="layui-form-item">
                <label for="L_type_id" class="layui-form-label">
                    <span class="x-red">*</span>type_id</label>
                <div class="layui-input-inline">
                    <select id="L_type_id" name="type_id" class="valid">
                        <option value="all_email">all_email</option>
                    </select>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span></div>
            </div>
            <div class="layui-form-item">
                <label for="L_email" class="layui-form-label">
                    <span class="x-red">*</span>Email</label>
                <div class="layui-input-inline">
                    <input type="text" id="L_email" name="email" required="" lay-verify="email" autocomplete="off" class="layui-input" value=""></div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span></div>
            </div>
            <div class="layui-form-item">
                <label for="L_username" class="layui-form-label">Start Date</label>
                <div class="layui-input-inline">
                    <input class="layui-input"  autocomplete="off" placeholder="Start Date" name="start_at" id="start_at" value="<?php echo $start_at; ?>">
                </div>
            </div>
            <div class="layui-form-item">
                <label for="L_username" class="layui-form-label">End Date</label>
                <div class="layui-input-inline">
                    <input class="layui-input"  autocomplete="off" placeholder="End Date" name="end_at" id="end_at" value="<?php echo $end_at; ?>">
                </div>
            </div>
            <div class="layui-form-item">
                <label for="L_username" class="layui-form-label">Reason</label>
                <div class="layui-input-inline">
                    <input type="text" id="L_reason" name="reason" required="" lay-verify="reason" autocomplete="off" class="layui-input" value=""></div>
            </div>
            <div class="layui-form-item">
                <label for="L_repass" class="layui-form-label"></label>
                <button class="layui-btn" lay-filter="add" lay-submit="">Save</button></div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'layer','jquery','laydate'],
        function() {
            $ = layui.jquery;
            var laydate = layui.laydate;
            var form = layui.form;
            var layer = layui.layer;

            laydate.render({
                elem: '#start_at' //指定元素
            });
            laydate.render({
                elem: '#end_at' //指定元素
            });

            //监听提交
            form.on('submit(add)',
                function(data) {
                    //发异步，把数据提交给php
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/magento/blacklist/blacklist/blacklistadd']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'type_id':$("#L_type_id").val(),
                            'email':$("#L_email").val(),
                            'start_at':$("#start_at").val(),
                            'end_at':$("#end_at").val(),
                            'reason':$("#L_reason").val()
                        },
                        success: function (result) {
                            layer.alert("Success", {
                                    icon: 6
                                },
                                function() {
                                    //关闭当前frame
                                    xadmin.close();

                                    // 可以对父窗口进行刷新
                                    xadmin.father_reload();
                                }
                            );
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                    return false;
                });

        });</script>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>