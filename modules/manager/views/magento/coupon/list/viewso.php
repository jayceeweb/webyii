<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    $this->registerCsrfMetaTags();
    $this->head();
    ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="layui-fluid">
    <div class="layui-row">
        <table class="layui-table layui-form">
            <thead>
            <tr>
                <th>name</th>
                <th>sku</th>
                <th>qty_ordered</th>
                <th>qty_refunded</th>
                <th>price</th>
                <th>discount_amount</th>
                <th>row_total</th>
                <th>warranty</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rows as $key=>$row): ?>
            <tr>
                <td><?php echo $row["name"]; ?></td>
                <td><?php echo $row["sku"]; ?></td>
                <td><?php echo (int)$row["qty_ordered"]; ?></td>
                <td><?php echo $row["qty_refunded"]; ?></td>
                <td><?php echo number_format($row["price"],2); ?></td>
                <td><?php echo number_format($row["discount_amount"],2); ?></td>
                <td><?php echo number_format($row["row_total"],2); ?></td>
                <td>
                    has_warranty=<?php if ($row["has_warranty"]==1) {echo "Yes";} else {echo "No";} ?><br>
                    warranty=<?php echo number_format($row["warranty"],2) ?><br>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>