<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use app\components\helper\AuthcodeHelper;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="x-admin-sm">
  
  <head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      <?php
      $this->registerCsrfMetaTags();
      $this->head();
      ?>
  </head>
  
  <body>
  <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="coupon"  placeholder="please input coupon code"  style="width: 200px" autocomplete="off" class="layui-input" value="<?php echo $condition["coupon"] ?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                开始时间：<input type="date" class="layui-input"  autocomplete="off" placeholder="start order time" name="start_order_time" id="start_order_time" value="<?php echo $condition["start_order_time"];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                停止时间：<input type="date" class="layui-input"  autocomplete="off" placeholder="end order time" name="end_order_time" id="end_order_time" value="<?php echo $condition["end_order_time"];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-body ">
                        <span>coupon code info</span>
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>rule name</th>
                                <th>from date</th>
                                <th>to date</th>
                                <th>is active</th>
                                <th>discount amount</th>
                                <th>discount qty</th>
                                <th>total use times</th>
                                <th>sales order use times</th>
                                <th>expiration time</th>
                                <th>action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($data["coupon_list"] as $key=>$row): ?>
                                <tr>
                                    <td><?php echo $row["name"] ?></td>
                                    <td><?php echo $row["from_date"] ?></td>
                                    <td><?php echo $row["to_date"] ?></td>
                                    <td><?php if ($row["is_active"]==1) {echo "Yes";} else {echo "No";} ?></td>
                                    <td><?php echo round($row["discount_amount"],2) ?></td>
                                    <td><?php echo (int)$row["discount_qty"] ?></td>
                                    <td><?php echo (int)$row["times_used"] ?></td>
                                    <td><?php echo (int)$row["order_times_used"] ?></td>
                                    <td><?php echo $row["expiration_date"] ?></td>
                                    <td>
                                        <?php if (($row["is_active"]==0 || $row["to_date"]<Yii::$container->get("app\components\helper\TimezoneHelper")->getDate()) && (int)$row["order_times_used"]==0): ?>
                                        <button class="layui-btn"  onclick="extend_life('<?php echo $condition["coupon"] ?>')">extend</button>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="layui-card-body ">
                        <span>quote</span>
                        <table class="layui-table layui-form">
                          <thead>
                            <tr>
                                <th>is_active</th>
                                <th>customer</th>
                                <th>items_count</th>
                                <th>items_qty</th>
                                <th>total</th>
                                <th>order_id</th>
                                <th>warranty</th>
                                <th>action</th>
                          </thead>
                          <tbody>
                            <?php foreach ($data["quote_list"] as $key=>$row): ?>
                            <tr>
                              <td><?php if ($row["is_active"]==1) {echo "Yes";} else {echo "No";} ?><br></td>
                              <td>
                                  email=<?php echo $row["customer_email"] ?><br>
                                  firstname=<?php echo $row["customer_firstname"] ?><br>
                                  lastname=<?php echo $row["customer_lastname"] ?><br>
                              </td>
                              <td><?php echo $row["items_count"] ?></td>
                                <td><?php echo (int)$row["items_qty"] ?></td>
                                <td>
                                    grand_total=<?php echo number_format($row["grand_total"],2) ?><br>
                                    subtotal=<?php echo number_format($row["subtotal"],2) ?><br>
                                </td>
                                <td><?php echo $row["reserved_order_id"] ?></td>
                                <td>
                                    has_warranty=<?php if ($row["has_warranty"]==1) {echo "Yes";} else {echo "No";} ?><br>
                                    warranty=<?php echo number_format($row["warranty"],2) ?><br>
                                </td>
                                <td><button class="layui-btn"  onclick="xadmin.open('view detail','<?php echo Url::toRoute(['/manager/magento/coupon/list/viewquote',"id"=>AuthcodeHelper::encode_ex($row["entity_id"])]); ?>',1200,600)">View Detail</button></td>
                            </tr>
                            <?php endforeach; ?>
                          </tbody>
                        </table>
                    </div>
                    <div class="layui-card-body ">
                        <span>sales order</span>
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>increment_id</th>
                                <th>state</th>
                                <th>customer</th>
                                <th>discount_amount</th>
                                <th>total</th>
                                <th>shipping</th>
                                <th>warranty</th>
                                <th>action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data["so_list"] as $key=>$row): ?>
                                <tr>
                                    <td><?php echo $row["increment_id"] ?></td>
                                    <td><?php echo $row["state"] ?></td>
                                    <td>
                                        email=<?php echo $row["customer_email"] ?><br>
                                        firstname=<?php echo $row["customer_firstname"] ?><br>
                                        lastname=<?php echo $row["customer_lastname"] ?><br>
                                    </td>
                                    <td><?php echo number_format($row["discount_amount"],2) ?></td>
                                    <td>
                                        grand_total=<?php echo number_format($row["grand_total"],2) ?><br>
                                        subtotal=<?php echo number_format($row["subtotal"],2) ?><br>
                                        total_qty_ordered=<?php echo (int)$row["total_qty_ordered"] ?><br>
                                        subtotal_refunded=<?php echo number_format($row["subtotal_refunded"],2) ?><br>
                                        total_paid=<?php echo number_format($row["total_paid"],2) ?><br>
                                        total_canceled=<?php echo number_format($row["total_canceled"],2) ?><br>
                                        total_refunded=<?php echo number_format($row["total_refunded"],2) ?><br>
                                    </td>
                                    <td>
                                        shipping_amount=<?php echo number_format($row["shipping_amount"],2) ?><br>
                                        shipping_method=<?php echo $row["shipping_method"] ?><br>
                                    </td>
                                    <td>
                                        has_warranty=<?php if ($row["has_warranty"]==1) {echo "Yes";} else {echo "No";} ?><br>
                                        warranty=<?php echo number_format($row["warranty"],2) ?><br>
                                    </td>
                                    <td><button class="layui-btn"  onclick="xadmin.open('view detail','<?php echo Url::toRoute(['/manager/magento/coupon/list/viewso',"id"=>AuthcodeHelper::encode_ex($row["entity_id"])]); ?>',1200,600)">View Detail</button></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php if($condition["coupon"]){ ?>
                        <?php $url = sprintf("/webyiiapi/organization/excaldata.html?coupon=%s&start_order_time=%s&end_order_time=%s",$condition["coupon"],$condition["start_order_time"],$condition["end_order_time"]) ?>
                        <button class="layui-btn">
                            <a  style="color: white" href="<?php echo $url; ?>">Export</a>
                        </button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div> 
  <?php $this->endBody() ?>
  </body>
    <script>
        function extend_life(coupon) {
            if (confirm('do you confirm extend '+coupon+" code?")) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Url::toRoute(['/manager/magento/coupon/list/couponextend']); ?>",
                    data: {
                        '_csrf': $('meta[name="csrf-token"]').attr("content"),
                        'coupon': coupon
                    },
                    success: function (result) {
                        layer.alert("Success", {
                                icon: 6
                            },
                            function () {
                                // 可以对父窗口进行刷新
                                location.reload();
                            }
                        );
                    },
                    error: function (e) {
                        layer.msg('Failed!', {icon: 1, time: 1000});
                    }
                });
            } else {
                return false;
            }
        };
    </script>

</html>
<?php $this->endPage() ?>