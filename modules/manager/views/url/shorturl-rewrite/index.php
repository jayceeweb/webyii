<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>

        <style>
            .img_box{
                width: 20px;
                height: 20px;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5" method="get">
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="is_active" id="is_active">
                                    <option <?php if ($condition["is_active"]==""):?>selected<?php endif; ?> value="">Is Active</option>
                                    <option <?php if ($condition["is_active"]=="1"):?>selected<?php endif; ?> value="1">Yes</option>
                                    <option <?php if ($condition["is_active"]=="0"):?>selected<?php endif; ?> value="0">No</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="rewrite_type" id="rewrite_type">
                                    <option <?php if ($condition["rewrite_type"]==""):?>selected<?php endif; ?> value="">Rewrite Type</option>
                                    <option <?php if ($condition["rewrite_type"]=="1"):?>selected<?php endif; ?> value="1">正则跳转</option>
                                    <option <?php if ($condition["rewrite_type"]=="0"):?>selected<?php endif; ?> value="0">直接跳转</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="rewrite_code" id="rewrite_code">
                                    <option <?php if ($condition["rewrite_code"]==""):?>selected<?php endif; ?> value="">Rewrite Code</option>
                                    <option <?php if ($condition["rewrite_code"]=="301"):?>selected<?php endif; ?> value="301">永久性转移</option>
                                    <option <?php if ($condition["rewrite_code"]=="302"):?>selected<?php endif; ?> value="302">暂时性转移</option>
                                </select>
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <input type="text" name="ori_uri" id="ori_uri"  placeholder="Ori Uri" autocomplete="off" class="layui-input" value="<?php echo $condition['ori_uri']; ?>">
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <input type="text" name="rewrite_url" id="rewrite_url"  placeholder="Rewrite Url" autocomplete="off" class="layui-input" value="<?php echo $condition['rewrite_url']; ?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>

                        <button class="layui-btn"  onclick="xadmin.open('Add Coupon','<?php echo Url::toRoute(['/manager/url/shorturl-rewrite/update-shouturl']); ?>',800,600)">
                            <i class="layui-icon"></i>New
                        </button>
                    </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>Rewrite Type</th>
                                <th>Ori Uri</th>
                                <th>Rewrite Url</th>
                                <th>Rewrite Code</th>
                                <th>Log Format</th>
                                <th>Time</th>
                                <th>User</th>
                                <th>Is Active</th>
                                <th>Action</th>
                            </thead>

                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td>
                                        <?php if ($row["rewrite_type"]==0): ?>
                                            直接跳转
                                        <?php elseif($row["rewrite_type"]>0): ?>
                                            正则跳转
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php echo $row['ori_uri']?>
                                    </td>
                                    <td>
                                        <?php echo $row['rewrite_url']?>
                                    </td>
                                    <td>
                                        <?php if ($row["rewrite_code"]==301): ?>
                                            永久性转移
                                        <?php elseif($row["rewrite_code"]==302): ?>
                                            暂时性转移
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php echo $row['log_format']?>
                                    </td>
                                    <td>
                                        Create_at:<?php echo $row['create_at'];?><br><br>
                                        Last_update_at:<?php echo $row['update_at'];?>
                                    </td>
                                    <td>
                                        Create user:<?php echo $row['create_admin_id'];?><br><br>
                                        Last update user:<?php echo $row['update_admin_id'];?>
                                    </td>
                                    <td>
                                        <?php if ($row["is_active"]==0): ?>
                                            <input type="checkbox"  disabled>
                                        <?php elseif($row["is_active"]>0): ?>
                                            <input type="checkbox" disabled checked="checked">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <button class="layui-btn"  onclick="xadmin.open('Update Shouturl','<?php echo Url::toRoute(['/manager/url/shorturl-rewrite/update-shouturl','id'=>$row['id']]); ?>',800,600)">
                                            <!--Detail-->
                                            <a href="#" title="Update Shouturl" style="color: white">
                                                Edit
                                            </a>
                                        </button>

                                        <button class="layui-btn"  onclick="activeShouturl('<?php echo $row["id"];?>','<?php echo $row["is_active"];?>')">
                                            <!-- active this score-->
                                            <?php if($row["is_active"] == 0){?>
                                                <a href="#" title="restore this Shouturl" style="color: white">
                                                    Active
                                                </a>
                                            <?php }else if($row["is_active"] == 1){ ?>
                                                <a href="#" title="delete this Shouturl"  style="color: white">
                                                    Disable
                                                </a>
                                            <?php } ?>
                                        </button>

                                        <button class="layui-btn"  onclick="deleteShouturl('<?php echo $row["id"];?>')">
                                            <!-- active this score-->
                                                <a href="#" title="delete this Shouturl" style="color: white">
                                                    Delete
                                                </a>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;
                lay(".from_date").each(function () {
                    laydate.render({
                        elem: this,
                        trigger:'click',
                        obj:$(this).attr("obj"),
                        done: function(value, date, endDate){
                            obj=JSON.parse($(this).attr("obj"));
                            saveChange(obj,value,"from_date");
                        }
                    });
                })
                lay(".to_date").each(function () {
                    laydate.render({
                        elem: this,
                        trigger:'click',
                        obj:$(this).attr("obj"),
                        done: function(value, date, endDate){
                            obj=JSON.parse($(this).attr("obj"));
                            saveChange(obj,value,"to_date");
                        }
                    });
                })

                function saveChange(obj,v,field_name) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/score/scorerule/change-detail-score']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'item_id':obj.item_id,
                            "field_name":field_name,
                            "value":v,
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                }

                $('input[name="list_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"list_score");
                });

                $('input[name="special_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"special_score");
                });

                $('input[name="birthday_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"birthday_score");
                });

                var laydate = layui.laydate;

                laydate.render({
                    elem: '#active_end_time' //指定元素
                });

                laydate.render({
                    elem: '#active_start_time' //指定元素
                });

                laydate.render({
                    elem: '#start_bill_time' //指定元素
                });

                laydate.render({
                    elem: '#end_bill_time' //指定元素
                });

                laydate.render({
                    elem: '#start_delievered_time' //指定元素
                });

                laydate.render({
                    elem: '#end_delievered_time' //指定元素
                });
            }
        );

        function activeShouturl(id,is_active) {
            if(is_active == 0){
                var msg = 'You are sure to start this Shouturl？';
            }else{
                var msg = 'You are sure to close this Shouturl？';
            }
            layer.confirm(msg,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/url/shorturl-rewrite/is-active']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'id':id,
                            'is_active':is_active
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                            window.location.reload();
                        },
                    });
                });
        }
        function deleteShouturl(id) {
            $.ajax({
                type: "POST",
                url: "<?php echo Url::toRoute(['/manager/url/shorturl-rewrite/to-delete']); ?>",
                data: {
                    '_csrf':$('meta[name="csrf-token"]').attr("content"),
                    'id':id,
                },
                success: function (result) {
                    layer.msg('success!', {icon: 1, time: 1000});
                    window.location.reload();
                },
            });
        }
    </script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>