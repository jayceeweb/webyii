<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
        <style type="text/css">
            .layui-form-item label{
                width: 160px !important;
            }
            .layui-form-item .layui-input-inline{
                width: 70%;
            }
        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>Rewrite Type</label>
                    <div class="layui-input-inline">
                        <input type="hidden" id="l_id" name="id" required="" autocomplete="off" class="layui-input" value="<?php echo $id; ?>">
                        <select name="l_rewrite_type" id="rewrite_type">
                            <option <?php if($is_active == 0){echo 'selected';} ?> value="0">直接跳转</option>
                            <option <?php if($is_active == 1){echo 'selected';} ?> value="1">正则跳转</option>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>Ori Uri</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_ori_uri" name="ori_uri" required="" autocomplete="off" class="layui-input" value="<?php echo $ori_uri; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>Rewrite Url</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_rewrite_url" name="rewrite_url" required="" autocomplete="off" class="layui-input" value="<?php echo $rewrite_url; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>Rewrite Code</label>
                    <div class="layui-input-inline">
                        <select name="rewrite_code" id="l_rewrite_code">
                            <option <?php if ($rewrite_code==""):?>selected<?php endif; ?> value="">Rewrite Code</option>
                            <option <?php if ($rewrite_code=="301"):?>selected<?php endif; ?> value="301">永久性转移</option>
                            <option <?php if ($rewrite_code=="302"):?>selected<?php endif; ?> value="302">暂时性转移</option>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>Log Format</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_log_format" name="log_format" required="" autocomplete="off" class="layui-input" value="<?php echo $log_format; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>

                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>Active</label>
                    <div class="layui-input-inline">
                        <select name="is_active" id="l_is_active">
                            <option <?php if($is_active == 0){echo 'selected';} ?> value="0">disabled</option>
                            <option <?php if($is_active == 1){echo 'selected';} ?> value="1">active</option>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button class="layui-btn" lay-filter="add" lay-submit="">Save</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;
                laydate.render({
                    elem: '#start_at' //指定元素
                });
                laydate.render({
                    elem: '#end_at' //指定元素
                });

                //监听提交
                form.on('submit(add)',
                    function(data) {

                        if($("#l_ori_uri").val().length <= 0){
                            layer.msg('Cant be empty!', {icon: 7, time: 1000});
                            return;
                        }
                        if($("#l_rewrite_url").val().length <= 0){
                            layer.msg('Cant be empty!', {icon: 7, time: 1000});
                            return;
                        }
                        //发异步，把数据提交给php
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Url::toRoute(['/manager/url/shorturl-rewrite/update-save']); ?>",
                            data: {
                                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                                'id':$("#l_id").val(),
                                'rewrite_type':$("#l_rewrite_type").val(),
                                'ori_uri':$("#l_ori_uri").val(),
                                'rewrite_url':$("#l_rewrite_url").val(),
                                'is_active':$("#l_is_active").val(),
                                'rewrite_code':$("#l_rewrite_code").val(),
                                'log_format':$("#l_log_format").val(),
                            },
                            success: function (result) {
                                layer.alert("Success", {
                                        icon: 6
                                    },
                                    function() {
                                        //关闭当前frame
                                        xadmin.close();

                                        // 可以对父窗口进行刷新
                                        xadmin.father_reload();
                                    }
                                );
                            },
                            error: function (e) {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        });
                        return false;
                    });

                form.on('select(test)', function(data){
                    if(data.value=="customer_entity"){
                        $("#src_id").hide()
                    }else{
                        $("#src_id").show()
                    }
                });

            });




    </script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>