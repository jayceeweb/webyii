<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
        <style type="text/css">
            .layui-form-item label{
                width: 160px !important;
            }
            .layui-form-item .layui-input-inline{
                width: 70%;
            }
        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>Product Name</label>
                    <div class="layui-input-inline">
                        <input type="hidden" id="l_url_rewrite_id" name="url_rewrite_id" required="" autocomplete="off" class="layui-input" value="<?php echo $url_rewrite_id; ?>">
                        <input type="text" id="l_dest_name" name="dest_name" required="" autocomplete="off" class="layui-input" value="<?php echo $dest_name; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_meta_title" class="layui-form-label">
                        <span class="x-red">*</span>Meta Title</label>
                    <div class="layui-input-inline">
                        <textarea name="meta_title" style="width: 99%;height:72px;font-size: 13px;resize:none;" id="l_meta_title" value="<?php echo $meta_title; ?>"><?php echo $meta_title; ?></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_meta_keywords" class="layui-form-label">
                        <span class="x-red">*</span>Meta Keywords</label>
                    <div class="layui-input-inline">
                        <textarea name="meta_keywords" style="width: 99%;height:72px;font-size: 13px;resize:none;"  id="l_meta_keywords" cols="30" rows="10" value="<?php echo $meta_keywords; ?>"><?php echo $meta_keywords; ?></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_page_description" class="layui-form-label">
                        <span class="x-red">*</span>Page Description</label>
                    <div class="layui-input-inline">
                        <textarea name="page_description" id="l_page_description" style="width: 99%;height:72px;font-size: 13px;resize:none;" cols="30" rows="10" value="<?php echo $page_description; ?>"><?php echo $page_description; ?></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_page_description" class="layui-form-label">
                        First Description(part1)</label>
                    <div class="layui-input-inline">
                        <textarea name="first_description" id="l_first_description" style="width: 99%;height:72px;font-size: 13px;resize:none;" cols="30" rows="10" value="<?php echo $first_description; ?>"><?php echo $first_description; ?></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_page_description" class="layui-form-label">
                        Second Description(part2)</label>
                    <div class="layui-input-inline">
                        <textarea name="second_description" id="l_second_description" style="width: 99%;height:72px;font-size: 13px;resize:none;" cols="30" rows="10" value="<?php echo $second_description; ?>"><?php echo $second_description; ?></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_meta_description" class="layui-form-label">
                        <span class="x-red">*</span>Meta Description(part3)</label>
                    <div class="layui-input-inline">
                        <textarea name="meta_description" id="l_meta_description" style="width: 99%;height:72px;font-size: 13px;resize:none;" cols="30" rows="10" value="<?php echo $meta_description; ?>"><?php echo $meta_description; ?></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button class="layui-btn" lay-filter="add" lay-submit="">Save</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;

                laydate.render({
                    elem: '#start_at' //指定元素
                });
                laydate.render({
                    elem: '#end_at' //指定元素
                });

                //监听提交
                form.on('submit(add)',
                    function(data) {
                        //发异步，把数据提交给php
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Url::toRoute(['/manager/url/url-write/update-save']); ?>",
                            data: {
                                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                                'url_rewrite_id':$("#l_url_rewrite_id").val(),
                                'dest_name':$("#l_dest_name").val(),
                                'meta_title':$("#l_meta_title").val(),
                                'meta_keywords':$("#l_meta_keywords").val(),
                                'meta_description':$("#l_meta_description").val(),
                                'page_description':$("#l_page_description").val(),
                                'first_description':$("#l_first_description").val(),
                                'second_description':$("#l_second_description").val(),
                            },
                            success: function (result) {
                                layer.alert("Success", {
                                        icon: 6
                                    },
                                    function() {
                                        //关闭当前frame
                                        xadmin.close();

                                        // 可以对父窗口进行刷新
                                        xadmin.father_reload();
                                    }
                                );
                            },
                            error: function (e) {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        });
                        return false;
                    });

                form.on('select(test)', function(data){
                    if(data.value=="customer_entity"){
                        $("#src_id").hide()
                    }else{
                        $("#src_id").show()
                    }
                });

            });




    </script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>