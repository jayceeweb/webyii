<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>


        <style>
            .Recommended{
                width: auto !important;
                height: 30px;
                padding: 0;
                text-align: left;
                line-height: 30px;
                display: inline-block;
                margin-right: 15px;
            }
            .Recommended_input{
                display: inline-block;
                height: 30px;
            }
            .Recommended_box{
                /*width: 600px;*/
                display: inline-block;
            }
            .box{
                /*display: flex;*/
                /*justify-content: space-between;*/
                margin-top: 15px;
            }
            .box_list{
                float: left;
                margin-bottom: 10px;
                margin-right: 10px;
            }
            .btn-seach1{
                margin-left: 20px;
                height: 30px;
                line-height: 30px;
            }
            .img_box{
                width: 20px;
                height: 20px;
            }
            .mobel_juli{
                height: auto !important;
            }
            .mobel_juli button{
                margin-left: 0px !important;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="dest_name"  placeholder="Dest Name" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['dest_name']?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="dest_sku"  placeholder="Dest Sku" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['dest_sku']?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="sku"  placeholder="Product Sku" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['sku']?>">
                            </div><br>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="name"  placeholder="Product Name" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['name']?>">
                            </div>
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <select name="entity_type" style="display: block;width:205px;height:28px;border:1px solid #e6e6e6;color: #666;">
                                    <option <?php if ($condition["entity_type"]==""):?>selected<?php endif; ?> value="">Entity Type</option>
                                    <?php foreach ($type as $key => $val){ ?>
                                        <option <?php if($val["entity_type"] != 'pg_frame'){ echo 'disabled';} ?> <?php if ($condition["entity_type"]==$val["entity_type"]):?>selected<?php endif; ?> value="<?php echo $val["entity_type"]?>"><?php echo $val["entity_type"]?></option>
                                    <?php } ?>
                                </select>
                            </div><br>
                            <div class="layui-inline layui-show-xs-block btn-seach1">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>Product Info</th>
                                <th>Request Path</th>
                                <th>Target Info</th>
                                <th>Meta Info</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td>
                                        <table>
                                            <tr><td>sku</td><td><?php echo $row["sku"]; ?></td></tr>
                                            <tr><td>name</td><td><?php echo $row["name"]; ?></td></tr>
                                            <tr><td>entity_type</td><td><?php echo $row["entity_type"]; ?></td></tr>
                                            <tr><td>lens_type</td><td><?php echo $row["lens_type"]; ?></td></tr>
                                        </table>
                                    </td>
                                    <td>
                                        <?php echo $row["request_path"]; ?><br>
                                    </td>
                                    <td>
                                        <table>
                                            <tr><td>target name</td><td><?php echo $row["dest_name"]; ?></td></tr>
                                            <tr><td>target sku</td><td><?php echo $row["dest_sku"]; ?></td></tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table>
                                            <tr><td>meta_title</td><td><?php echo $row["meta_title"]; ?></td></tr>
                                            <tr><td>meta_keywords</td><td><?php echo $row["meta_keywords"]; ?></td></tr>
                                            <tr><td>page_description</td><td><?php echo $row["page_description"]; ?></td></tr>
                                            <tr><td>first_description(part1)</td><td><?php echo $row["first_description"]; ?></td></tr>
                                            <tr><td>second_description(part2)</td><td><?php echo $row["second_description"]; ?></td></tr>
                                            <tr><td>meta_description(part3)</td><td><?php echo $row["meta_description"]; ?></td></tr>
                                        </table>
                                    </td>
                                    <td>
                                        <div class="layui-card-header mobel_juli">
                                            <?php if ($row["entity_type"]=="pg_frame"){ ?>
                                                <button class="layui-btn" onclick="xadmin.open('update','<?php echo Url::toRoute(['/manager/url/url-write/update','url_rewrite_id'=>$row["url_rewrite_id"],'dest_name'=>$row["dest_name"],'meta_title'=>$row["meta_title"],'meta_keywords'=>$row["meta_keywords"],'meta_description'=>$row["meta_description"],'page_description'=>$row["page_description"],'page_description'=>$row["page_description"],'first_description'=>$row["first_description"],'second_description'=>$row["second_description"]]); ?>',800,600)">
                                                    <a href="#" title="Url Rewrite Id">
                                                        <img src="/assets2/resource/img/manage.png" alt="Operation" class="img_box">
                                                    </a>
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>