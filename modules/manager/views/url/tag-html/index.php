<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>


        <style>
            .Recommended{
                width: auto !important;
                height: 30px;
                padding: 0;
                text-align: left;
                line-height: 30px;
                display: inline-block;
                margin-right: 15px;
            }
            .Recommended_input{
                display: inline-block;
                height: 30px;
            }
            .Recommended_box{
                /*width: 600px;*/
                display: inline-block;
            }
            .box{
                /*display: flex;*/
                /*justify-content: space-between;*/
                margin-top: 15px;
            }
            .box_list{
                float: left;
                margin-bottom: 10px;
                margin-right: 10px;
            }
            .btn-seach1{
                margin-left: 20px;
                height: 30px;
                line-height: 30px;
            }
            .img_box{
                width: 20px;
                height: 20px;
            }
            .mobel_juli{
                height: auto !important;
            }
            .mobel_juli button{
                margin-left: 0px !important;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="name"  placeholder="Tag Name" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['name'];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block btn-seach1">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-header">
                        <button class="layui-btn" onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/url/tag-html/add']); ?>',800,600)"><i class="layui-icon"></i>New</button>
                    </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Category Id</th>
                                <th>Banner</th>
                                <th>Breadcrumbs Title</th>
                                <th>List Name</th>
                                <th>Category Name</th>
                                <th>Meta Title</th>
                                <th>Meta Description</th>
                                <th>Meta Robots</th>
                                <th>Meta Keywords</th>
                                <th>Base Url Canonical</th>
                                <th>Page Num</th>
                                <th>Genders</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td>
                                        <?php echo $row["name"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["category_id"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["banner"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["breadcrumbs_title"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["list_name"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["category_name"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["meta_title"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["meta_description"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["meta_robots"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["meta_keywords"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["base_url_canonical"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["page_num"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["genders"]; ?><br>
                                    </td>
                                    <td>
                                        <button class="layui-btn"  onclick="xadmin.open('Update Tag Html','<?php echo Url::toRoute(['/manager/url/tag-html/add','id'=>$row['id']]); ?>',800,600)">
                                            <!--Detail-->
                                            <a href="#" title="Updata Score">
                                                <img src="/assets2/resource/img/update_score.png" alt="Update The Pg Tag" class="img_box">
                                            </a>
                                        </button>
                                        <button class="layui-btn"  onclick="xadmin.open('Url Rewrite','<?php echo Url::toRoute(['/manager/url/tag-html/url-add','base_url_canonical'=>$row['base_url_canonical']]); ?>',800,600)">
                                            <!--Detail-->
                                            <a href="#" title="Updata Score">
                                                <img src="/assets2/resource/img/manage.png" alt="Update The Url Rewrite" class="img_box">
                                            </a>
                                        </button>
                                        <button class="layui-btn"  onclick="activeDelete('<?php echo $row["id"];?>','<?php echo $row["base_url_canonical"]?>')">
                                            <!-- active this score-->
                                            <a href="#" title="delete this lens_type">
                                                <img src="/assets2/resource/img/disable.png" alt="delete this lens_type" class="img_box">
                                            </a>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function activeDelete(id,base_url_canonical) {
            confirm_message="Are you sure to delete this pg_tag_html?";
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['//manager/url/tag-html/delete']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'id':id,
                            'base_url_canonical':base_url_canonical,
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                            xadmin.father_reload();
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }
    </script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>