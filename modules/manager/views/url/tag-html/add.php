<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Tag Name</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_name" name="name" required="" autocomplete="off" class="layui-input" value="<?php echo $name; ?>">
                        <input type="hidden" id="l_id" name="id" required="" autocomplete="off" class="layui-input" value="<?php echo $id; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Category Id</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_category_id" name="category_id" required="" autocomplete="off" class="layui-input" value="<?php echo $category_id; ?>">
                    </div>
                    <span style="color: red;">推荐：0</span>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Banner</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_banner" name="banner" required="" autocomplete="off" class="layui-input" value="<?php echo $banner; ?>">
                    </div>
                    <span style="color: red;">推荐：magento配置的Identifier</span>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Breadcrumbs Title</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_breadcrumbs_title" name="breadcrumbs_title" required="" autocomplete="off" class="layui-input" value="<?php echo $breadcrumbs_title; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">List Name</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_list_name" name="list_name" required="" autocomplete="off" class="layui-input" value="<?php echo $list_name; ?>">
                    </div>
                    <span style="color: red;">推荐：eyeglasses plp +name</span>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Category Name</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_category_name" name="category_name" required="" autocomplete="off" class="layui-input" value="<?php echo $category_name; ?>">
                    </div>
                    <span style="color: red;">推荐：eyeglasses +name</span>
                    <div class="layui-form-mid layui-word-aux">
                </div>
                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Meta Title</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_meta_title" name="meta_title" required="" autocomplete="off" class="layui-input" value="<?php echo $meta_title; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Meta Description</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_meta_description" name="meta_description" required="" autocomplete="off" class="layui-input" value="<?php echo $meta_description; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Meta Robots</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_meta_robots" name="meta_robots" required="" autocomplete="off" class="layui-input" value="<?php echo $meta_robots; ?>">
                    </div>
                    <span style="color: red;">推荐：Index, nofollow</span>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Meta Keywords</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_meta_keywords" name="meta_keywords" required="" autocomplete="off" class="layui-input" value="<?php echo $meta_keywords; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Base Url Canonical</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_base_url_canonical" name="base_url_canonical" required="" autocomplete="off" class="layui-input" value="<?php echo $base_url_canonical; ?>">
                    </div>
                    <span style="color: red;">推荐：/eyeglasses/+name+.html</span>
                    <div class="layui-form-mid layui-word-aux">
                </div>


                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Page Num</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_page_num" name="page_num" required="" autocomplete="off" class="layui-input" value="<?php echo $page_num; ?>">
                    </div>
                    <span style="color: red;">推荐：12</span>
                    <div class="layui-form-mid layui-word-aux">
                </div>

                <div class="layui-form-item">
                    <label for="L_customer_email" class="layui-form-label">Genders</label>
                    <div class="layui-input-inline">
                        <input type="checkbox" name="genders" value="6" <?php if(in_array(6,$genders)){ echo "checked=\"checked\"";}?>  /> women
                        <input type="checkbox" name="genders" value="8" <?php if(in_array(8,$genders)){ echo "checked=\"checked\"";}?>/> men
                        <input type="checkbox" name="genders" value="10" <?php if(in_array(10,$genders)){ echo "checked=\"checked\"";}?>/> child
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button class="layui-btn" lay-filter="add" lay-submit="">Save</button></div>
            </form>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;

                laydate.render({
                    elem: '#start_at' //指定元素
                });
                laydate.render({
                    elem: '#end_at' //指定元素
                });

                //监听提交
                form.on('submit(add)',

                    function(data) {
                        var genders = $('input:checkbox[name="genders"]:checked').map(function () {
                            return $(this).val();
                        }).get().join(",");
                        //发异步，把数据提交给php
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Url::toRoute(['/manager/url/tag-html/save']); ?>",
                            data: {
                                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                                'id':$("#l_id").val(),
                                'name':$("#l_name").val(),
                                'category_id':$("#l_category_id").val(),
                                'banner':$("#l_banner").val(),
                                'breadcrumbs_title':$("#l_breadcrumbs_title").val(),
                                'list_name':$("#l_list_name").val(),
                                'category_name':$("#l_category_name").val(),
                                'meta_title':$("#l_meta_title").val(),
                                'meta_description':$("#l_meta_description").val(),
                                'meta_robots':$("#l_meta_robots").val(),
                                'meta_keywords':$("#l_meta_keywords").val(),
                                'base_url_canonical':$("#l_base_url_canonical").val(),
                                'page_num':$("#l_page_num").val(),
                                'genders':genders,
                            },
                            success: function (result) {
                                layer.alert("Success", {
                                        icon: 6
                                    },
                                    function() {
                                        //关闭当前frame
                                        xadmin.close();

                                        // 可以对父窗口进行刷新
                                        xadmin.father_reload();
                                    }
                                );
                            },
                            error: function (e) {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        });
                        return false;
                    });

                form.on('select(test)', function(data){
                    if(data.value=="customer_entity"){
                        $("#src_id").hide()
                    }else{
                        $("#src_id").show()
                    }
                });

            });




    </script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>