<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>


        <style>
            .Recommended{
                width: auto !important;
                height: 30px;
                padding: 0;
                text-align: left;
                line-height: 30px;
                display: inline-block;
                margin-right: 15px;
            }
            .Recommended_input{
                display: inline-block;
                height: 30px;
            }
            .Recommended_box{
                /*width: 600px;*/
                display: inline-block;
            }
            .box{
                /*display: flex;*/
                /*justify-content: space-between;*/
                margin-top: 15px;
            }
            .box_list{
                float: left;
                margin-bottom: 10px;
                margin-right: 10px;
            }
            .btn-seach1{
                margin-left: 20px;
                height: 30px;
                line-height: 30px;
            }
            .img_box{
                width: 20px;
                height: 20px;
            }
            .mobel_juli{
                height: auto !important;
            }
            .mobel_juli button{
                margin-left: 0px !important;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-input-inline layui-show-xs-block">
                                <input type="text" name="id"  placeholder="Id" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['id']?>">
                            </div>
                            <?php foreach ($type as $key => $value){ ?>
                                <div class="layui-inline layui-input-inline layui-show-xs-block">
                                    <select name="<?php echo $key; ?>">
                                        <option <?php if ($condition[$key]==""):?>selected<?php endif; ?> value=""><?php echo $key ?></option>
                                        <?php foreach ($value as $ke => $val){ ?>
                                            <option  <?php if ($condition[$key]==$val[$key]):?>selected<?php endif; ?> value="<?php echo $val[$key]?>"><?php echo $val[$key]?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                            <div class="layui-inline layui-show-xs-block btn-seach1">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                        <div class="layui-card-header">
                            <button class="layui-btn" onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/url/lens-type/add']); ?>',800,600)"><i class="layui-icon"></i>New</button>
                        </div>
                    </div>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Lens Type</th>
                                <th>Lens Type Code</th>
                                <th>Frame Group</th>
                                <th>Name</th>
                                <th>Mark</th>
                                <th>Lens Type Route</th>
                                <th>Lens Type Dest Name</th>
                                <th>Price Included</th>
                                <th>Clipon Price Included</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td>
                                        <?php echo $row["id"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["lens_type"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["lens_type_code"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["frame_group"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["name"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["mark"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["lens_type_route"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["lens_type_dest_name"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["price_included"]; ?><br>
                                    </td>
                                    <td>
                                        <?php echo $row["clipon_price_included"]; ?><br>
                                    </td>
                                    <td>
                                        <button class="layui-btn"  onclick="xadmin.open('update','<?php echo Url::toRoute(['/manager/url/lens-type/add','id'=>$row['id'],'lens_type'=>$row['lens_type'],'lens_type_code'=>$row['lens_type_code'],'frame_group'=>$row['frame_group'],'name'=>$row['name'],'mark'=>$row['mark'],'lens_type_route'=>$row['lens_type_route'],'lens_type_dest_name'=>$row['lens_type_dest_name'],'lens_type_dest_sku'=>$row['lens_type_dest_sku'],'price_included'=>$row['price_included'],'clipon_price_included'=>$row['clipon_price_included']]); ?>',800,600)">
                                            <!--manager this order score-->
                                            <a href="#" title="update this lens_type">
                                                <img src="/assets2/resource/img/manage.png" alt="manager this order score" class="img_box">
                                            </a>
                                        </button>

                                        <?php if (Yii::$app->admin->can('scoreManagers')) { ?>
                                        <button class="layui-btn"  onclick="activeDelete('<?php echo $row["id"];?>')">
                                            <!-- active this score-->
                                            <a href="#" title="delete this lens_type">
                                                <img src="/assets2/resource/img/disable.png" alt="delete this lens_type" class="img_box">
                                            </a>
                                        </button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        /**
         * lens_type数据删除
         * @param id
         * @constructor
         */
        function activeDelete(id) {
            confirm_message="Are you sure to delete this lens_type?";
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['//manager/url/lens-type/delete']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'id':id,
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                            xadmin.father_reload();
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
        }

        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;
                lay(".from_date").each(function () {
                    laydate.render({
                        elem: this,
                        trigger:'click',
                        obj:$(this).attr("obj"),
                        done: function(value, date, endDate){
                            obj=JSON.parse($(this).attr("obj"));
                            saveChange(obj,value,"from_date");
                        }
                    });
                })
                lay(".to_date").each(function () {
                    laydate.render({
                        elem: this,
                        trigger:'click',
                        obj:$(this).attr("obj"),
                        done: function(value, date, endDate){
                            obj=JSON.parse($(this).attr("obj"));
                            saveChange(obj,value,"to_date");
                        }
                    });
                })

                function saveChange(obj,v,field_name) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/score/scorerule/change-detail-score']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'item_id':obj.item_id,
                            "field_name":field_name,
                            "value":v,
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                }

                $('input[name="list_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"list_score");
                });

                $('input[name="special_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"special_score");
                });

                $('input[name="birthday_score"]').blur(function(){
                    obj=JSON.parse($(this).attr("obj"));
                    saveChange(obj,$(this).val(),"birthday_score");
                });
            }
        );
    </script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>