<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
        <style type="text/css">
            .layui-form-item label{
                width: 160px !important;
            }
            .layui-form-item .layui-input-inline{
                width: 70%;
            }
        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <?php foreach ($type as $key => $value){ ?>
                    <div class="layui-form-item">
                        <label for="L_<?php echo $key;?>" class="layui-form-label">
                            <span class="x-red">*</span>
                            <?php
                                echo ltrim(ucwords(str_replace("_"," ", strtolower($key))),"_");
                            ?>
                        </label>
                        <div class="layui-input-inline">
                            <?php if($key == 'price_included' || $key == 'clipon_price_included'){ ?>
                                <textarea style="width: 92%;height:30px;font-size: 13px;resize:none;padding: 20px" name="<?php echo $key; ?>" id="l_<?php echo $key;?>" cols="30" rows="10" value="<?php echo $value; ?>"><?php echo $value; ?></textarea>
                            <?php }else{ ?>
                                <input type="text" <?php if($key == 'id'){echo 'disabled';} ?> id="l_<?php echo $key;?>"  name="<?php echo $key; ?>" required="" autocomplete="off" class="layui-input" value="<?php echo $value; ?>">
                            <?php } ?>
                            </div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span></div>
                    </div>
                <?php } ?>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <div class="layui-btn layui-btn_s" >Save</div></div>
                </div>
            </form>
        </div>
    </div>


    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;
                var old_score = $("#l_old_score").val();
                $(".layui-btn_s").on("click",function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/url/lens-type/save']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            <?php foreach ($type as $key => $value){ ?>
                            '<?php echo $key?>':$("#l_<?php echo $key?>").val(),
                            <?php } ?>
                        },
                        success: function (result) {
                            layer.alert("Success", {
                                    icon: 6
                                },
                                function() {
                                    //关闭当前frame
                                    xadmin.close();

                                    // 可以对父窗口进行刷新
                                    xadmin.father_reload();
                                }
                            );
                        },
                        error: function (e) {
                            layer.msg('Failed!', {icon: 1, time: 1000});
                        }
                    });
                });
            })
    </script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>