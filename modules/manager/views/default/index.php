<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!doctype html>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>Pg Admin</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="/assets/x-admin/css/font.css">
    <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
    <link rel="shortcut icon" type="image/ico" href="/media/favicon/stores/1/favIcon.ico">
    <!-- <link rel="stylesheet" href="./css/theme5.css"> -->
    <script src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        // 是否开启刷新记忆tab功能
        // var is_remember = false;
    </script>
    <?php
    $this->registerCsrfMetaTags();
    $this->head();
    ?>
</head>
<body class="index">
<!-- 顶部开始 -->
<?php $this->beginBody() ?>
<div class="container">
    <div class="logo">
        <a href="./index.html">Pg Admin</a></div>
    <div class="left_open">
        <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
    </div>
    <ul class="layui-nav right" lay-filter="">
        <li class="layui-nav-item">
            <a href="javascript:;"><?php echo Yii::$app->admin->getIdentity()->getAttribute('username'); ?></a>
            <dl class="layui-nav-child">
                <!-- 二级菜单 -->
<!--                <dd><a onclick="xadmin.open('个人信息','http://www.baidu.com')">个人信息</a></dd>-->
                <dd><a href="<?php echo Url::toRoute(['/manager/default/logout']); ?>">Logout</a></dd>
            </dl>
        </li>
<!--        <li class="layui-nav-item to-index"><a href="/" target="_blank">前台首页</a></li>-->
    </ul>
</div>
<!-- 顶部结束 -->
<!-- 中部开始 -->
<!-- 左侧菜单开始 -->
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
<!--            <li>-->
<!--                <a href="javascript:;">-->
<!--                    <i class="iconfont left-nav-li" lay-tips="会员管理">&#xe6b8;</i>-->
<!--                    <cite>会员管理</cite>-->
<!--                    <i class="iconfont nav_right">&#xe697;</i></a>-->
<!--                <ul class="sub-menu">-->
<!--                    <li>-->
<!--                        <a onclick="xadmin.add_tab('会员删除','member-del.html')">-->
<!--                            <i class="iconfont">&#xe6a7;</i>-->
<!--                            <cite>会员删除</cite></a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
            <li>
                <a href="javascript:;">
                    <i class="iconfont left-nav-li" lay-tips="Website Admin">&#xe6b8;</i>
                    <cite>Website Admin</cite>
                    <i class="iconfont nav_right">&#xe697;</i></a>
                    <ul class="sub-menu">
                        <?php if ($has_blacklistManager): ?>
                        <li>
                            <a href="javascript:;">
                                <i class="iconfont">&#xe70b;</i>
                                <cite>Blacklist</cite>
                                <i class="iconfont nav_right">&#xe697;</i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Blacklist Admin','<?php echo Url::toRoute(['/manager/magento/blacklist/blacklist/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Blacklist Admin</cite>
                                        </a>
                                    </li>
                                </ul>
                        </li>
                        <?php endif; ?>
                        <li>
                            <a href="javascript:;">
                                <i class="iconfont">&#xe70b;</i>
                                <cite>coupon</cite>
                                <i class="iconfont nav_right">&#xe697;</i></a>
                            <ul class="sub-menu">
                                <li>
                                    <a onclick="xadmin.add_tab('coupon usage check','<?php echo Url::toRoute(['/manager/magento/coupon/list/index']); ?>')">
                                        <i class="iconfont">&#xe6a7;</i>
                                        <cite>coupon usage check</cite>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php if ($has_scoreManager): ?>
                            <li>
                                <a href="javascript:;">
                                    <i class="iconfont">&#xe70b;</i>
                                    <cite>Score Admin</cite>
                                    <i class="iconfont nav_right">&#xe697;</i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Score Rule Admin','<?php echo Url::toRoute(['/manager/score/scorerule/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Score Rule Admin</cite>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="xadmin.add_tab('Score Bill Admin','<?php echo Url::toRoute(['/manager/score/score/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Score Bill Admin</cite>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="xadmin.add_tab('User Score Admin','<?php echo Url::toRoute(['/manager/score/user-score/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>User Score Admin</cite>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                        <?php if ($has_urlManager): ?>
                            <li>
                                <a href="javascript:;">
                                    <i class="iconfont">&#xe70b;</i>
                                    <cite>Url Rewrite Admin</cite>
                                    <i class="iconfont nav_right">&#xe697;</i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Url Rewrite','<?php echo Url::toRoute(['/manager/url/url-write/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Url Rewrite</cite>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Lens Type','<?php echo Url::toRoute(['/manager/url/lens-type/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Lens Type</cite>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Pg Tag Html','<?php echo Url::toRoute(['/manager/url/tag-html/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Pg Tag Html</cite>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Pg Shorturl Rewrite','<?php echo Url::toRoute(['/manager/url/shorturl-rewrite/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Pg Shorturl Rewrite</cite>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <?php if ($has_user_management): ?>
                            <li>
                                <a href="javascript:;">
                                    <i class="iconfont">&#xe70b;</i>
                                    <cite>Ratio</cite>
                                    <i class="iconfont nav_right">&#xe697;</i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('User ratio','<?php echo Url::toRoute(['/manager/ratio/user-ratio/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>User ratio</cite>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <?php if ($has_refreshProduct): ?>
                            <li>
                                <a href="javascript:;">
                                    <i class="iconfont">&#xe70b;</i>
                                    <cite>Tool</cite>
                                    <i class="iconfont nav_right">&#xe697;</i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Refresh Product','<?php echo Url::toRoute(['/manager/tool/refresh-product/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Products to refresh</cite>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <li>
                            <a href="javascript:;">
                                <i class="iconfont">&#xe70b;</i>
                                <cite>Collect</cite>
                                <i class="iconfont nav_right">&#xe697;</i></a>
                            <ul class="sub-menu">
                                <li>
                                    <a onclick="xadmin.add_tab('Prescription Collect','<?php echo Url::toRoute(['/manager/shouji/prescription/index']); ?>')">
                                        <i class="iconfont">&#xe6a7;</i>
                                        <cite>Prescription Collect</cite>
                                    </a>
                                </li>
                            </ul>
                            <ul class="sub-menu">
                                <li>
                                    <a onclick="xadmin.add_tab('PD Collect','<?php echo Url::toRoute(['/manager/shouji/tongju/index']); ?>')">
                                        <i class="iconfont">&#xe6a7;</i>
                                        <cite>PD Collect</cite>
                                    </a>
                                </li>
                            </ul>
                        </li>



<?php if ($has_organization){ ?>
                        <li>
                            <a href="javascript:;">
                                <i class="iconfont">&#xe70b;</i>
                                <cite>Organization Administration</cite>
                                <i class="iconfont nav_right">&#xe697;</i></a>
                            <ul class="sub-menu">
                                <li>
                                    <a onclick="xadmin.add_tab('organization','<?php echo Url::toRoute(['/manager/organization/organization/index']); ?>')">
                                        <i class="iconfont">&#xe6a7;</i>
                                        <cite>Organization</cite>
                                    </a>
                                </li>
                            </ul>
                            <ul class="sub-menu">
                                <li>
                                    <a onclick="xadmin.add_tab('Coupon Code','<?php echo Url::toRoute(['/manager/organization/coupon-code/index']); ?>')">
                                        <i class="iconfont">&#xe6a7;</i>
                                        <cite>Coupon Code</cite>
                                    </a>
                                </li>
                            </ul>
                        </li>
<?php } ?>


                        <!--帮人做单-->
                            <li>
                                <a href="javascript:;">
                                    <i class="iconfont">&#xe70b;</i>
                                    <cite>Customers</cite>
                                    <i class="iconfont nav_right">&#xe697;</i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('Prescription','<?php echo Url::toRoute(['/manager/customerhelp/prescriptionhelp/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>Prescription</cite>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('All customers','<?php echo Url::toRoute(['/manager/customerhelp/userdetails/index']); ?>')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>All customers</cite>
                                        </a>
                                    </li>
                                </ul>

                                <!--                                修改密码-->
                                <?php if ($password){ ?>
                                    <ul class="sub-menu">
                                        <li>
                                            <a onclick="xadmin.add_tab('Update Password','<?php echo Url::toRoute(['/manager/customerhelp/updatepassword/index']); ?>')">
                                                <i class="iconfont">&#xe6a7;</i>
                                                <cite>Update Password</cite>
                                            </a>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </li>
                    </ul>
            </li>
        </ul>
    </div>
</div>
<!-- <div class="x-slide_left"></div> -->
<!-- 左侧菜单结束 -->
<!-- 右侧主体开始 -->
<div class="page-content">
    <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
        <ul class="layui-tab-title">
            <li class="home">
                <i class="layui-icon">&#xe68e;</i>My Desktop
            </li>
        </ul>
        <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
            <dl>
                <dd data-type="this">Close</dd>
                <dd data-type="other">Close Other</dd>
                <dd data-type="all">Close All</dd>
            </dl>
        </div>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='<?php echo Url::toRoute(['/manager/welcome/index']); ?>' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
        </div>
        <div id="tab_show"></div>
    </div>
</div>
<div class="page-content-bg"></div>
<style id="theme_style"></style>
<!-- 右侧主体结束 -->
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>