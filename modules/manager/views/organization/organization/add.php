<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
        <style type="text/css">
            .layui-form-item label{
                width: 160px !important;
            }
            .layui-form-item .layui-input-inline{
                width: 70%;
            }
        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <input type="hidden" id="l_entity_id" name="entity_id" required="" autocomplete="off" class="layui-input" value="<?php echo $entity_id; ?>">
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>org_type</label>
                    <div class="layui-input-inline">
                        <select name="org_type" id="l_org_type">
                            <option <?php if ($org_type==""):?>selected<?php endif; ?> value="">Org Type</option>
                            <option <?php if ($org_type=="1"):?>selected<?php endif; ?> value="1">School</option>
                            <option <?php if ($org_type=="2"):?>selected<?php endif; ?> value="2">Company</option>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>org_name</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_org_name" name="org_name" required="" autocomplete="off" class="layui-input" value="<?php echo $org_name; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>org_code</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_org_code" name="org_code" required="" autocomplete="off" class="layui-input" value="<?php echo $org_code; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>org_description</label>
                    <div class="layui-input-inline">
                        <textarea name="org_description" id="l_org_description" cols="30" rows="10" ><?php echo $org_description; ?></textarea>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>street</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_street" name="street" required="" autocomplete="off" class="layui-input" value="<?php echo $street; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>city</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_city" name="city" required="" autocomplete="off" class="layui-input" value="<?php echo $city; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>region</label>
                    <div class="layui-input-inline">
                        <select name="region_id" id="l_region_id">
                            <option <?php if ($region==""):?>selected<?php endif; ?> value=""><?php echo 'region' ?></option>
                            <?php foreach ($region_name as $key => $val){ ?>
                                <option  <?php if ($region==$val['default_name']):?>selected<?php endif; ?> value="<?php echo $val['region_id']?>"><?php echo $val['default_name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>postcode</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_postcode" name="postcode" required="" autocomplete="off" class="layui-input" value="<?php echo $postcode; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>telephone</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_telephone" name="telephone" required="" autocomplete="off" class="layui-input" value="<?php echo $telephone; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>fax</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_fax" name="fax" required="" autocomplete="off" class="layui-input" value="<?php echo $fax; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_dest_name" class="layui-form-label">
                        <span class="x-red">*</span>is_active</label>
                    <div class="layui-input-inline">
                        <select name="is_active" id="l_is_active">
                            <option <?php if($is_active == 0){echo 'selected';} ?> value="0">disabled</option>
                            <option <?php if($is_active == 1){echo 'selected';} ?> value="1">active</option>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button class="layui-btn" lay-filter="add" lay-submit="">Save</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;

                laydate.render({
                    elem: '#start_at' //指定元素
                });
                laydate.render({
                    elem: '#end_at' //指定元素
                });

                //监听提交
                form.on('submit(add)',
                    function(data) {
                        //发异步，把数据提交给php
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Url::toRoute(['/manager/organization/organization/update-save']); ?>",
                            data: {
                                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                                'entity_id':$("#l_entity_id").val(),
                                'org_type':$("#l_org_type").val(),
                                'org_name':$("#l_org_name").val(),
                                'org_code':$("#l_org_code").val(),
                                'org_description':$("#l_org_description").val(),
                                'street':$("#l_street").val(),
                                'city':$("#l_city").val(),
                                'region_id':$("#l_region_id").val(),
                                'postcode':$("#l_postcode").val(),
                                'telephone':$("#l_telephone").val(),
                                'fax':$("#l_fax").val(),
                                'is_active':$("#l_is_active").val(),
                            },
                            success: function (result) {
                                if(!result){
                                    layer.msg('Failed!', {icon: 1, time: 1000});
                                }else{
                                    layer.alert("Success", {
                                            icon: 6
                                        },
                                        function() {
                                            //关闭当前frame
                                            xadmin.close();

                                            // 可以对父窗口进行刷新
                                            xadmin.father_reload();
                                        }
                                    );
                                }
                            },
                            error: function (e) {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        });
                        return false;
                    });

                form.on('select(test)', function(data){
                    if(data.value=="customer_entity"){
                        $("#src_id").hide()
                    }else{
                        $("#src_id").show()
                    }
                });

            });




    </script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>