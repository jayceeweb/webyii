<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layui-fluid">
        <div class="layui-row">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="L_biz_type" class="layui-form-label">Biz Type</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_biz_type" <?php if($id){echo 'disabled';} ?> name="biz_type" required="" autocomplete="off" class="layui-input" value="<?php echo $biz_type; ?>">
                        <input type="hidden" id="l_id" name="id" required="" autocomplete="off" class="layui-input" value="<?php echo $id; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_biz_connect" class="layui-form-label">Biz Connect</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_biz_connect" <?php if($id && $biz_type =="email"){echo 'disabled';} ?> name="biz_connect" required="" autocomplete="off" class="layui-input" value="<?php echo $biz_connect; ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_cart_ratio" class="layui-form-label">Cart Ratio</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_cart_ratio" name="cart_ratio" required="" autocomplete="off" class="layui-input" value="<?php echo $cart_ratio; ?>">
                        <font style="color:red">单位%</font>
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_freight_ratio" class="layui-form-label">Freight Ratio</label>
                    <div class="layui-input-inline">
                        <input type="text" id="l_freight_ratio" name="freight_ratio" required="" autocomplete="off" class="layui-input" value="<?php echo $freight_ratio; ?>">
                        <font style="color:red">单位%</font>
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_compel" class="layui-form-label">Compel</label>
                    <div class="layui-input-inline">
                        <select name="compel" id="l_compel">
                            <option value="0" <?php if ($compel=="0"):?>selected<?php endif; ?>>非强制</option>
                            <option value="1" <?php if ($compel=="1"):?>selected<?php endif; ?>>强制</option>
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button class="layui-btn" lay-filter="add" lay-submit="">Save</button></div>
            </form>
        </div>
    </div>
    <script>
        layui.use(['form', 'layer','jquery','laydate'],
            function() {
                $ = layui.jquery;
                var laydate = layui.laydate;
                var form = layui.form;
                var layer = layui.layer;

                laydate.render({
                    elem: '#start_at' //指定元素
                });
                laydate.render({
                    elem: '#end_at' //指定元素
                });

                //监听提交
                form.on('submit(add)',
                    function(data) {
                        //发异步，把数据提交给php
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Url::toRoute(['/manager/ratio/user-ratio/save']); ?>",
                            data: {
                                '_csrf':$('meta[name="csrf-token"]').attr("content"),
                                'id':$("#l_id").val(),
                                'biz_type':$("#l_biz_type").val(),
                                'biz_connect':$("#l_biz_connect").val(),
                                'cart_ratio':$("#l_cart_ratio").val(),
                                'freight_ratio':$("#l_freight_ratio").val(),
                                'compel':$("#l_compel").val(),
                            },
                            success: function (result) {
                                layer.alert("Success", {
                                        icon: 6
                                    },
                                    function() {
                                        //关闭当前frame
                                        xadmin.close();

                                        // 可以对父窗口进行刷新
                                        xadmin.father_reload();
                                    }
                                );
                            },
                            error: function (e) {
                                layer.msg('Failed!', {icon: 1, time: 1000});
                            }
                        });
                        return false;
                    });
            });</script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>