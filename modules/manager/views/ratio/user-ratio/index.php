<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>


        <style>
            .Recommended{
                width: auto !important;
                height: 30px;
                padding: 0;
                text-align: left;
                line-height: 30px;
                display: inline-block;
                margin-right: 15px;
            }
            .Recommended_input{
                display: inline-block;
                height: 30px;
            }
            .Recommended_box{
                /*width: 600px;*/
                display: inline-block;
            }
            .box{
                /*display: flex;*/
                /*justify-content: space-between;*/
                margin-top: 15px;
            }
            .box_list{
                float: left;
                margin-bottom: 10px;
                margin-right: 10px;
            }
            .btn-seach1{
                margin-left: 20px;
                height: 30px;
                line-height: 30px;
            }
            .img_box{
                width: 20px;
                height: 20px;
            }
            .mobel_juli{
                height: auto !important;
            }
            .mobel_juli button{
                margin-left: 0px !important;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5">
                            <div class="layui-inline layui-show-xs-block">
                                <select name="biz_type" style="display: block;width:205px;height:28px;border:1px solid #e6e6e6;color: #666;">
                                    <option <?php if ($condition['biz_type']==""):?>selected<?php endif; ?> value="">Biz Type</option>
                                    <?php foreach ($type as $key => $val){ ?>
                                        <option <?php if ($condition["biz_type"]==$val["biz_type"]):?>selected<?php endif; ?> value="<?php echo $val["biz_type"]?>"><?php echo $val["biz_type"]?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="biz_connect"  placeholder="Biz connect" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['biz_connect'];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <select name="compel" style="display: block;width:205px;height:28px;border:1px solid #e6e6e6;color: #666;">
                                    <option <?php if ($condition['compel']==""):?>selected<?php endif; ?> value="">Compel</option>
                                    <?php for ($i=1 ;$i<3 ;$i++){ ?>
                                        <option <?php if ($condition["compel"]==$i):?>selected<?php endif; ?> value="<?php echo $i?>"><?php if($i == 1){echo "强制";}else if($i==2){echo "非强制";}?></option>
                                    <?php } ?>
                                </select>
                            </div><br>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="cart_ratio_start"  placeholder="Cart ratio start" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['cart_ratio_start'];?>">
                                <input type="text" name="cart_ratio_end"  placeholder="Cart ratio end" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['cart_ratio_end'];?>">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="freight_ratio_start"  placeholder="Freight ratio start" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['freight_ratio_start'];?>">
                                <input type="text" name="freight_ratio_end"  placeholder="Freight ratio end" style="width: 200px"  autocomplete="off" class="layui-input"  value="<?php echo $condition['freight_ratio_end'];?>">
                            </div><br>
                            <div class="layui-inline layui-show-xs-block btn-seach1">
                                <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </form>
                    </div>
                    <button class="layui-btn"  style="margin-right: 10px"    onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/ratio/user-ratio/add']); ?>',800,600)">
                        <a href="#" title="manager this customer score">
                            <img src="/assets2/resource/img/manage.png" alt="manager this customer score" class="img_box">
                        </a>
                    </button>
                    <div class="layui-card-body ">
                        <table class="layui-table layui-form">
                            <thead>
                            <tr>
                                <th>Biz type</th>
                                <th>Biz connect</th>
                                <th>Cart ratio</th>
                                <th>Freight ratio</th>
                                <th>Compel</th>
                                <th>Operation</th>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key=>$row): ?>
                                <tr>
                                    <td>
                                        <?php echo $row['biz_type'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['biz_connect'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['cart_ratio'];?>%
                                    </td>
                                    <td>
                                        <?php echo $row['freight_ratio'];?>%
                                    </td>
                                    <td>
                                        <?php echo $row['compel'];?>
                                    </td>
                                    <td>
                                        <button class="layui-btn"  style="margin-right: 10px"    onclick="xadmin.open('New','<?php echo Url::toRoute(['/manager/ratio/user-ratio/add',"id"=>$row['id'],"biz_type"=>$row['biz_type'],"biz_connect"=>$row['biz_connect'],"cart_ratio"=>$row['cart_ratio'],"freight_ratio"=>$row['freight_ratio'],"compel"=>$row['compel']]); ?>',800,600)">
                                            <a href="#" title="manager this customer score">
                                                <img src="/assets2/resource/img/manage.png" alt="manager this customer score" class="img_box">
                                            </a>
                                        </button>
                                        <button class="layui-btn" onclick="javascript:DeleteRatio(<?php echo $row['id']; ?>)">
                                            <a href="#" title="Revise">
                                                <img src="/assets2/resource/img/disable.png" alt="DeleteRatio" class="img_box">
                                            </a>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="layui-card-body ">
                        <div class="page current_page">
                            <div>
                                <?php echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pager,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function DeleteRatio($id) {
            confirm_message="Are you sure you want to delete this ratio data?";
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/ratio/user-ratio/delete-ratio']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                            'id':$id
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                            xadmin.father_reload();
                        },
                    });
                });
        }
    </script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>