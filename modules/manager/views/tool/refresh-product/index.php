<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>欢迎页面-X-admin2.2</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/assets/x-admin/css/font.css">
        <link rel="stylesheet" href="/assets/x-admin/css/xadmin.css">
        <script type="text/javascript" src="/assets/x-admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/assets/x-admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $this->registerCsrfMetaTags();
        $this->head();
        ?>


        <style>
            .Recommended{
                width: auto !important;
                height: 30px;
                padding: 0;
                text-align: left;
                line-height: 30px;
                display: inline-block;
                margin-right: 15px;
            }
            .Recommended_input{
                display: inline-block;
                height: 30px;
            }
            .Recommended_box{
                /*width: 600px;*/
                display: inline-block;
            }
            .box{
                /*display: flex;*/
                /*justify-content: space-between;*/
                margin-top: 15px;
            }
            .box_list{
                float: left;
                margin-bottom: 10px;
                margin-right: 10px;
            }
            .btn-seach1{
                margin-left: 20px;
                height: 30px;
                line-height: 30px;
            }
            .img_box{
                width: 20px;
                height: 20px;
            }
            .mobel_juli{
                height: auto !important;
            }
            .mobel_juli button{
                margin-left: 0px !important;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="refresh">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
    </div>
    <div class="layui-fluid">
            <h1 style="font-size: 50px;">Tools</h1>
            <br>
            <button class="layui-btn"  onclick="refreshProduct()">
                <!-- active this score-->
                <a href="#" title="Products to refresh" style="color: white;">
                    Products to refresh
                </a>
            </button>
    </div>
    <script>
        function refreshProduct() {
            confirm_message="Are you sure you want a product refresh?";
            layer.confirm(confirm_message,
                function(index) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Url::toRoute(['/manager/tool/refresh-product/refresh']); ?>",
                        data: {
                            '_csrf':$('meta[name="csrf-token"]').attr("content"),
                        },
                        success: function (result) {
                            layer.msg('success!', {icon: 1, time: 1000});
                            xadmin.father_reload();
                        },
                    });
                });
        }
    </script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>