<?php
namespace app\modules\manager\rbac;

use yii\rbac\Rule;

class TestRule extends Rule
{
    public $name = 'isTest';

    /**
     * @param string|integer $user 用户 ID.
     * @param Item $item 该规则相关的角色或者权限
     * @param array $params 传给 ManagerInterface::checkAccess() 的参数
     * @return boolean 代表该规则相关的角色或者权限是否被允许
     */
    public function execute($user, $item, $params)
    {
        echo "测试使用<br>";
        print_r($user);
        echo "<br>";
        print_r($item);
        echo "<br>";
        print_r($params);
        return false;

        $b=new \mdm\admin\components\AccessControl();
    }
}