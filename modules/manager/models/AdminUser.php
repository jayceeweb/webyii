<?php
namespace app\modules\manager\models;

use Yii;
use yii\base\Model;


/**
 * This is the model class for table "admin_user".
 *
 * @property int $user_id User ID
 * @property string|null $firstname User First Name
 * @property string|null $lastname User Last Name
 * @property string|null $email User Email
 * @property string|null $username User Login
 * @property string $password User Password
 * @property string $created User Created Time
 * @property string $modified User Modified Time
 * @property string|null $logdate User Last Login Time
 * @property int $lognum User Login Number
 * @property int $reload_acl_flag Reload ACL
 * @property int $is_active User Is Active
 * @property string|null $extra User Extra Data
 * @property string|null $rp_token Reset Password Link Token
 * @property string|null $rp_token_created_at Reset Password Link Token Creation Date
 * @property string $interface_locale Backend interface locale
 * @property int|null $failures_num Failure Number
 * @property string|null $first_failure First Failure
 * @property string|null $lock_expires Expiration Lock Dates
 *
 * @property AdminPasswords[] $adminPasswords
 * @property AdminUserSession[] $adminUserSessions
 * @property OauthToken[] $oauthTokens
 * @property UiBookmark[] $uiBookmarks
 */
class AdminUser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $user_id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            [['created', 'modified', 'logdate', 'rp_token_created_at', 'first_failure', 'lock_expires'], 'safe'],
            [['lognum', 'reload_acl_flag', 'is_active', 'failures_num'], 'integer'],
            [['extra', 'rp_token'], 'string'],
            [['firstname', 'lastname'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 128],
            [['username'], 'string', 'max' => 40],
            [['password'], 'string', 'max' => 255],
            [['interface_locale'], 'string', 'max' => 16],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'created' => 'Created',
            'modified' => 'Modified',
            'logdate' => 'Logdate',
            'lognum' => 'Lognum',
            'reload_acl_flag' => 'Reload Acl Flag',
            'is_active' => 'Is Active',
            'extra' => 'Extra',
            'rp_token' => 'Rp Token',
            'rp_token_created_at' => 'Rp Token Created At',
            'interface_locale' => 'Interface Locale',
            'failures_num' => 'Failures Num',
            'first_failure' => 'First Failure',
            'lock_expires' => 'Lock Expires',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminPasswords()
    {
        return $this->hasMany(AdminPasswords::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminUserSessions()
    {
        return $this->hasMany(AdminUserSession::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOauthTokens()
    {
        return $this->hasMany(OauthToken::className(), ['admin_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUiBookmarks()
    {
        return $this->hasMany(UiBookmark::className(), ['user_id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user=AdminUser::find()->where(["user_id"=>$id])->one();
        if ($user) {
            return $user;
        } else {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user=AdminUser::find()->where(["user_id"=>$token])->one();
        if ($user) {
            return $user;
        } else {
            return null;
        }
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user=AdminUser::find()->where(["username"=>$username])->one();
        if ($user) {
            return $user;
        } else {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getAttribute("user_id");
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->getAttribute("authKey");
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}