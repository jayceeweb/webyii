<?php
namespace app\modules\webyiiapi;

/**
 * product module definition class
 */
class Webyiiapi extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\webyiiapi\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
