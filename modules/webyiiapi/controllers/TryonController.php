<?php
namespace app\modules\webyiiapi\controllers;

use Yii;
use app\models\PgS3File;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use \GuzzleHttp\Psr7;

class TryonController extends BaseController {
    function msectime() {
        list($msec, $sec) = explode(' ', microtime());
        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }

    public function actionImageToS3(){
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $data=isset($content['data'])? $content['data']:[];
        $content_type=isset($data['content_type'])? $data['content_type']:".png";
        $obj_type=isset($data['obj_type'])? $data['obj_type']:"";
        $obj_id=isset($data['obj_id'])? $data['obj_id']:"0";
        $customer_id=isset($data['customer_id'])? $data['customer_id']:"0";
        if ($customer_id==0) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $session = $objectManager->get('\Magento\Customer\Model\Session');
            if(!empty($session)) {
                $customer = $session->getCustomer();
                if (!empty($customer->getEntityID())) {
                    $customer_id=$customer->getEntityID();
                }
            }
        }
        if (empty($customer_id)) {
            $customer_id=0;
        }
        $profile_id=isset($data['profile_id'])? $data['profile_id']:"0";
        if ($profile_id==0) {
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $session = $objectManager->get('\Magento\Customer\Model\Session');
            if(!empty($session)) {
                $profile_id=$session->getProfileId();
            }
        }
        if (empty($profile_id)) {
            $profile_id=0;
        }
        $profile_name=isset($data['profile_name'])? $data['profile_name']:"";
        $sku=isset($data['sku'])? $data['sku']:"";
        $additional_data=isset($data['additional_data'])? $data['additional_data']:"";
        $s3=isset($content['s3'])? $content['s3']:[];
        $region=isset($s3['region'])? $s3['region']:"us-east-1";
        $bucket=isset($s3['bucket'])? $s3['bucket']:"pg-product-images";
        $s=isset($content['s'])? $content['s']:"";
        $ss=explode(",",$s);
        if (count($ss)>=2) {
            $s=$ss[1];
            $d=base64_decode($s);
            $s3Client = new S3Client([
                'profile'=>"default",
                'region' => $region,
                'version' => 'latest'
            ]);
            $minitype=Psr7\mimetype_from_extension(strtolower($content_type));
            $path=sprintf("tryon_image/%s/%s.%s",date("Y/m/d"),md5($this->msectime()),$content_type);
            $result = $s3Client->putObject([
                'Bucket' => $bucket,
                'Key' => $path,
                'Body' => $d,
                'ContentType'=>$minitype
            ]);
            $objFile=new PgS3File();
            $objFile->content_type=$content_type;
            $objFile->obj_type=$obj_type;
            $objFile->obj_id=$obj_id;
            $objFile->customer_id=$customer_id;
            $objFile->profile_id=$profile_id;
            $objFile->profile_name=$profile_name;
            $objFile->sku=$sku;
            $objFile->additional_data=$additional_data;
            $s3_result=$result->toArray();
            $u=parse_url($s3_result["ObjectURL"]);
            $objFile->server_domain=$u["scheme"]."://".$u["host"];
            $objFile->file_path=$u["path"];
            $ret=$objFile->save();
            header("Content-type:application/json;charset=utf-8");
            $ret=[
                "code"=>0,
                "data"=>$objFile->getAttributes()
            ];
            die(json_encode($ret));
        } else {
            header("Content-type:application/json;charset=utf-8");
            $ret=[
                "code"=>-1,
                "data"=>[]
            ];
            die(json_encode($ret));
        }
    }

}
