<?php
namespace app\modules\webyiiapi\controllers;

use Yii;
use app\modules\webyiiapi\controllers\observer\ScaperCollectionObserver;
use app\modules\webyiiapi\controllers\observer\DataCollectionAction;

class DataCollectionController extends BaseController {
    public function actionPush(){
        $data=file_get_contents('php://input');
        $action=Yii::$container->get("app\modules\webyiiapi\controllers\observer\DataCollectionAction");
        $scaperAction=new ScaperCollectionObserver();
        $scaperAction->setData($data);
        $action->addObserver(get_class($scaperAction),$scaperAction);
        $action->notify();

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
        } else {
            header("Access-Control-Allow-Origin:"."*");
        }
        header("Access-Control-Allow-Methods:*");
        header("Access-Control-Allow-Credentials:true");
        header("Access-Control-Allow-Headers:x-requested-with,content-type");
        header("Expires:-1");
        header("Last-Modified:".gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control:no-store, no-cache, must-revalidate,post-check=0, pre-check=0");
        header("Pragma:no-cache");
//        header("Content-type:application/json;charset=utf-8");
        $ret=[
            "code"=>0
        ];
        die(json_encode($ret));
    }

    //20201123数据埋点
    public function actionPushNew() {
        //获取数据
        $data=file_get_contents('php://input');
        //观察者模式注册观察者，监测事件
        $action=Yii::$container->get("app\modules\webyiiapi\controllers\observer\DataCollectionAction");
        $scaperAction=new ScaperCollectionObserver();
        $scaperAction->setData($data);
        $action->addObserver(get_class($scaperAction),$scaperAction);
        $action->notify();

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
        } else {
            header("Access-Control-Allow-Origin:"."*");
        }
        header("Access-Control-Allow-Methods:*");
        header("Access-Control-Allow-Credentials:true");
        header("Access-Control-Allow-Headers:x-requested-with,content-type");
        header("Expires:-1");
        header("Last-Modified:".gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control:no-store, no-cache, must-revalidate,post-check=0, pre-check=0");
        header("Pragma:no-cache");
//        header("Content-type:application/json;charset=utf-8");
        $ret=[
            "code"=>0,
            "msg"=>'success',
        ];
        die(json_encode($ret));
    }
}
