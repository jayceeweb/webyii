<?php
namespace app\modules\webyiiapi\controllers;

use app\components\helper\AuthcodeHelper;
use app\modules\manager\models\AdminUser;
use Yii;

class RelatedController extends BaseController {
    public function actionRelated(){
        //pdp页面关联产品
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $product_id = $content['product_id'];
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["cache"];
        $options = $cfg["frontend"]["default"]["backend_options"];
        $redis = new \Cm_Cache_Backend_Redis($options);
        $cacheHashKey = "related_products_cache_data";
        $cacheData = $redis->getRedis()->hGet($cacheHashKey, $product_id);
        //为防止缓存策略失效，获取缓存数据失败情况下直接读取数据库并重新设置缓存
        if(empty($cacheData)) {
            $jsonData = [];
            $product=$objectManager->get("\Magento\Catalog\Model\ProductFactory")->create()->load($product_id);
            $relatedProducts = $product->getRelatedProducts();
            $default_related_id = array();
            if (!empty($relatedProducts)) {
                foreach ($relatedProducts as $relatedProduct) {
                    $default_related_id[] = $relatedProduct->getId();
                }
                $arr = $this->actionGetParents($default_related_id);
                $linked_product_id = '';
                foreach ($arr as $key => $value){
                    $linked_product_id .= $value['product_id'].',';
                }
                $linked_product_id = rtrim($linked_product_id,',');
                if($linked_product_id){
                    $sql = "SELECT cpf1.sku, cpf1.color_value as color_name, cpf1.image, eaos.`value` AS color_value, cpf1.color, cpf1.price, cpf1.`name`, cpsl.product_id, cpsl.parent_id, cpf1.`entity_id`, ur.request_path AS url FROM catalog_product_flat_1 AS cpf1 INNER JOIN eav_attribute_option_swatch AS eaos ON eaos.option_id = cpf1.color INNER JOIN cataloginventory_stock_item AS csi ON cpf1.entity_id = csi.product_id INNER JOIN catalog_product_super_link AS cpsl ON cpsl.product_id = cpf1.entity_id INNER JOIN url_rewrite AS ur ON ur.entity_id = cpf1.entity_id AND cpf1.retired != 1 WHERE cpf1.entity_id IN ($linked_product_id) GROUP BY cpf1.sku ORDER BY FIELD( cpf1.entity_id ,$linked_product_id)";
//                    $data = \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->queryAll();
                    $data = $connection->fetchAll($sql);
                    $con_data = array();
                    foreach ($data as $key => $value){
                        $value['default'] = false;
                        if(in_array($value['product_id'],$default_related_id)){
                            $value['default'] = true;
                        }
                        $product_info=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo($value['sku']);
                        $value['special_price'] = $product_info['special_price'];
                        $con_data[$value['parent_id']][] = $value;
                    }
                    $jsonData = json_encode(["code"=>0,"msg"=>"Success","data"=>$con_data]);
//                    header("Content-type:application/json;charset=utf-8");
//                    die(json_encode(["code"=>0,"msg"=>"Success","data"=>$con_data]));
                } else {
                    $jsonData = json_encode(["code"=>-2,"msg"=>"The currently recommended product is empty","data"=>array()]);
                }
//                header("Content-type:application/json;charset=utf-8");
//                die(json_encode(["code"=>-2,"msg"=>"The currently recommended product is empty","data"=>array()]));
            } else {
                $jsonData = json_encode(["code"=>-1,"msg"=>"The recommended product is empty","data"=>array()]);
//                header("Content-type:application/json;charset=utf-8");
//                die(json_encode(["code"=>-1,"msg"=>"The recommended product is empty","data"=>array()]));
            }
            $redis->getRedis()->hSet($cacheHashKey, $product_id, $jsonData);
            header("Content-type:application/json;charset=utf-8");
            die($jsonData);
        } else {
            header("Content-type:application/json;charset=utf-8");
            die($cacheData);
        }
    }
    public function actionGetParents($default_related_id){
        $simple_id = '';
        foreach ($default_related_id as $key => $value){
            $simple_id .= $value.',';
        }
        $simple_id = rtrim($simple_id,',');
        $sql = "select product_id from catalog_product_super_link where parent_id in (select parent_id from catalog_product_super_link where product_id in ({$simple_id})) ORDER BY FIELD(parent_id,{$simple_id})";
        $data=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->queryAll();
        return $data;
    }
}
