<?php
namespace app\modules\webyiiapi\controllers;

use Yii;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TagexcalController extends BaseController {
    // add sunjinchao 20/4/29 TagExcalUpdate
    public function actionExcaldata(){
        $file = $_FILES['excel_file'] ['tmp_name'];
        $data = $this->getExcaldata($file);
        $same = array();
        $diff = array();
        $tag_item = array();
        foreach ($data as $key => $value){
            $sku = $value["A"];
            $sql = "select entity_id from catalog_product_flat_1 where sku = '$sku'";
            $product_id = Yii::$app->db->createCommand($sql)->queryOne();
            if($product_id){
                $product_id = $product_id['entity_id'];
            }else{
                continue;
            }
            $sql = "select tag,product_id from pg_product_tag where product_id = $product_id";
            $tagdata = Yii::$app->db->createCommand($sql)->queryAll();
            if(!$tagdata){
                $diff[$product_id] = $value;
                continue;
            }
            $tagdatas = array();
            foreach ($tagdata as $k => $v){
                $tagdatas[] = $v["tag"];
                $product_id = $v["product_id"];
            }
            if($value["B"]){
                if(in_array($value["B"],$tagdatas)){
                    $same[$product_id] = $value;
                }else{
                    $diff[$product_id] = $value;
                }
            }
        }
        if($same){
            $same = $this->getSamedata($same);
        }
        if($diff){
            $diff = $this->getDiffdata($diff);
        }
        foreach ($data as $key => $value){
            $sku = $value["A"];
            $sql = "select tag,product_id,id from pg_product_tag where product_id = (select entity_id from catalog_product_flat_1 where sku = '$sku')";
            $tagdata = Yii::$app->db->createCommand($sql)->queryAll();
            $tagdatas = array();
            foreach ($tagdata as $k => $v){
                $tagdatas[''] = $v["tag"];
                $product_id = $v["product_id"];
            }
            if(!$product_id){
                continue;
            }
            $Tag = $value["B"];
            $simple_sql = "select id from pg_product_tag where product_id = $product_id and tag = '$Tag'";
            $simple_tagdata = Yii::$app->db->createCommand($simple_sql)->queryOne();
            $simple_tagdata = $simple_tagdata['id'];
            $product_sku = array();
            $i = 0;
            foreach(range('D','Z') as $k => $v){
                if(isset($value[$v])){
                    if(!$value[$v]){
                        break;
                    }
                    $product_sku[$value[$v]] = $i;
                    $i++;
                }
            }
            if($product_sku){
                foreach ($product_sku as $k => $v){
                    if($k){
                        $product_sql = "select entity_id from catalog_product_flat_1 where sku = '$k'";
//                    var_dump("$product_sql");
                        $product_id = Yii::$app->db->createCommand($product_sql)->queryOne();
                        if($product_id){
                            $product_id = $product_id['entity_id'];
                            $tag_item[$product_id][$simple_tagdata] = $v;
                        }
                    }
                }
            }
        }
        if($tag_item){
            $tag_item = $this->updateTagitem($tag_item);
        }
        var_dump("成功");
        die();
        // sunjinchao excal设置item tag带固定数据
        //17703	sunjinchao	9	17703K07	1
        //17703	sunjinchao	9	17703P07	2
        //17703	sunjinchao	9	17703U07	3
//        foreach ($data as $key => $value){
//            $sku = $value["A"];
//            $sql = "select tag,product_id from pg_product_tag where product_id = (select entity_id from catalog_product_flat_1 where sku = '$sku')";
//            $tagdata = Yii::$app->db->createCommand($sql)->queryAll();
//            $tagdatas = array();
//            foreach ($tagdata as $k => $v){
//                $tagdatas[] = $v["tag"];
//                $product_id = $v["product_id"];
//            }
//            $Tag = $value["B"];
//            $simple_sql = "select id from pg_product_tag where product_id = $product_id and tag = '$Tag'";
//            $simple_tagdata = Yii::$app->db->createCommand($simple_sql)->queryOne();
//            $simple_tagdata = $simple_tagdata['id'];
//            $product_sku = $value["D"];
//            $product_sql = "select entity_id from catalog_product_flat_1 where sku = '$product_sku'";
//            $product_id = Yii::$app->db->createCommand($product_sql)->queryOne();
//            $product_id = $product_id['entity_id'];
//            $tag_item[$product_id][$simple_tagdata] = $value["E"];
//        }
//        $tag_item = $this->updateTagitem($tag_item);
//        var_dump("成功");
//        die();
    }
    public function getSamedata($data){
        $datas= array();
        $sql = "update pg_product_tag set tag_position = CASE id";
        foreach ($data as $key => $value){
            $tag = $value["B"];
            $sql = "select id from pg_product_tag where product_id = $key and tag = '$tag'";
            $pg_product_tag_data = Yii::$app->db->createCommand($sql)->queryOne();
            $datas[$pg_product_tag_data["id"]] = $value["C"];
        }
        $keys = "(";
        $sql = "update pg_product_tag set tag_position = CASE id";
        foreach ($datas as $key => $value){
            $keys .= $key.",";
            $sql .= " WHEN $key THEN $value";
        }
        $keys = rtrim($keys,',');
        $keys .= ')';
        $sql .= " END where id in $keys";
        $pg_product_tag_data = Yii::$app->db->createCommand($sql)->execute();
    }
    public function getDiffdata($data){
        $datas = array();
        foreach ($data as $key => $value){
            $datas[$key][$value["B"]]['tag_position'] = $value["C"];
        }
        $sql = "insert into pg_product_tag (`product_id`,`tag`,`tag_position`) values ";
        foreach ($datas as $key => $v){
            foreach ($v as $k => $v){
                $sql .= '(\''.$key.'\',\''.$k.'\',\''.$v["tag_position"].'\'),';
            }
        }
        $sql = rtrim($sql,',');
        $pg_product_tag_data = Yii::$app->db->createCommand($sql)->execute();
    }
    public function updateTagitem($data){
        $items_id = "(";
        $updsql = "";
        $addsql = "";
        foreach ($data as $key => $value){
            foreach ($value as $k => $val){
                if($k){
                    $sql = "select item_id FROM pg_product_tag_item where tag_id = $k and product_id =$key";
                    $pg_product_tag_item = Yii::$app->db->createCommand($sql)->queryOne();
                    if($pg_product_tag_item["item_id"]){
                        $item_id = $pg_product_tag_item["item_id"];
                        $items_id .= $item_id.",";
                        $updsql .= " WHEN $item_id THEN $val";
                    }else{
                        $addsql .='('. $key.',';
                        $addsql .= $k.',';
                        $addsql .= $val.'),';
                    }
                }
            }
        }
        if($addsql){
            $addsql = "insert into pg_product_tag_item (product_id,tag_id,tag_position) values ".$addsql;
            $addsql = rtrim($addsql,',');
            Yii::$app->db->createCommand($addsql)->execute();
        }
        if($updsql){
            $updsql = "update pg_product_tag_item set tag_position = CASE item_id".$updsql;
            $items_id = rtrim($items_id,',');
            $items_id .= ')';
            $updsql .= " END where item_id in $items_id";
            Yii::$app->db->createCommand($updsql)->execute();
        }
    }
    public function getExcaldata($file){
        $sheet = 0;
        $columnCnt = 0;
        $options = [];
        try {
            /* 转码 */
            $file = iconv("utf-8", "gb2312", $file);

            if (empty($file) OR !file_exists($file)) {
                throw new \Exception('文件不存在!');
            }

            /** @var Xlsx $objRead */
            $objRead = IOFactory::createReader('Xlsx');

            if (!$objRead->canRead($file)) {
                /** @var Xls $objRead */
                $objRead = IOFactory::createReader('Xls');

                if (!$objRead->canRead($file)) {
                    throw new \Exception('只支持导入Excel文件！');
                }
            }

            /* 如果不需要获取特殊操作，则只读内容，可以大幅度提升读取Excel效率 */
            empty($options) && $objRead->setReadDataOnly(true);
            /* 建立excel对象 */
            $obj = $objRead->load($file);
            /* 获取指定的sheet表 */
            $currSheet = $obj->getSheet($sheet);

            if (isset($options['mergeCells'])) {
                /* 读取合并行列 */
                $options['mergeCells'] = $currSheet->getMergeCells();
            }

            if (0 == $columnCnt) {
                /* 取得最大的列号 */
                $columnH = $currSheet->getHighestColumn();
                /* 兼容原逻辑，循环时使用的是小于等于 */
                $columnCnt = Coordinate::columnIndexFromString($columnH);
            }

            /* 获取总行数 */
            $rowCnt = $currSheet->getHighestRow();
            $data   = [];

            /* 读取内容 */
            for ($_row = 2; $_row <= $rowCnt; $_row++) {
                $isNull = true;

                for ($_column =1; $_column <= $columnCnt; $_column++) {
                    $cellName = Coordinate::stringFromColumnIndex($_column);
                    $cellId   = $cellName . $_row;
                    $cell     = $currSheet->getCell($cellId);

                    if (isset($options['format'])) {
                        /* 获取格式 */
                        $format = $cell->getStyle()->getNumberFormat()->getFormatCode();
                        /* 记录格式 */
                        $options['format'][$_row][$cellName] = $format;
                    }

                    if (isset($options['formula'])) {
                        /* 获取公式，公式均为=号开头数据 */
                        $formula = $currSheet->getCell($cellId)->getValue();

                        if (0 === strpos($formula, '=')) {
                            $options['formula'][$cellName . $_row] = $formula;
                        }
                    }

                    if (isset($format) && 'm/d/yyyy' == $format) {
                        /* 日期格式翻转处理 */
                        $cell->getStyle()->getNumberFormat()->setFormatCode('yyyy/mm/dd');
                    }

                    $data[$_row][$cellName] = trim($currSheet->getCell($cellId)->getFormattedValue());

                    if (!empty($data[$_row][$cellName])) {
                        $isNull = false;
                    }
                }

                /* 判断是否整行数据为空，是的话删除该行数据 */
                if ($isNull) {
                    unset($data[$_row]);
                }
            }

            return ($data);
        } catch (\Exception $e) {
            throw $e;
        }
    }
    public function actionGettagexcal(){
        $condition=Yii::$app->getRequest()->get('condition','');
        $sql = "select sku,entity_id from catalog_product_flat_1 where type_id = 'configurable'";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $excaldata = array();
        $count = 0;
        foreach ($data as $key => $value){
            $product_id = $value["entity_id"];
            $sql = "select tag,id,tag_position from pg_product_tag where product_id = $product_id";
            if($condition){
                $sql .= " and tag = '$condition'";
            }
            $tagdata = Yii::$app->db->createCommand($sql)->queryAll();
            if($tagdata){
                foreach ($tagdata as $k => $v){
                    $tag_id = $v["id"];
                    $sql = "select sku,tag_position from pg_product_tag_item INNER JOIN catalog_product_flat_1 ON pg_product_tag_item.product_id = catalog_product_flat_1.entity_id where tag_id = $tag_id";
                    $itemdata = Yii::$app->db->createCommand($sql)->queryAll();
                    if(count($itemdata) > $count){
                        $count = count($itemdata);
                    }
                    if($itemdata){
                        $excaldata[$key][$k][0] = $value["sku"];
                        $excaldata[$key][$k][1] = $v["tag"];
                        $excaldata[$key][$k][2] = $v["tag_position"];
                        foreach ($itemdata as $ke => $va){
                            $excaldata[$key][$k][3][$ke] = $va["sku"];

                        }
                    }else{
                        $excaldata[$key][$k][] = $value["sku"];
                        $excaldata[$key][$k][] = $v["tag"];
                        $excaldata[$key][$k][] = $v["tag_position"];
                    }
                }
            }
        }
        $title = "tag列表";
        $key_value = "Configurable SKU,Tag Name,Tag Position,SIMPLE SKU";
        for($i = 1;$i<=$count;$i++){
            $key_value .= "SIMPLE SKU $i,";
        }
        $key_value = rtrim($key_value,",");
        $this->actionGetscoreExcal($key_value,$excaldata,$title);
    }
    public function actionGetscoreExcal($key_value,$data,$title){
        ini_set("memory_limit", "2048M");
        set_time_limit(0);
        $keys_value = explode(",",$key_value);
        $abc = range('A','Z');
        \PhpOffice\PhpSpreadsheet\Settings::setCache(new \app\components\excel\FileCache(sys_get_temp_dir().'/excel_cache'));
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');
        foreach ($keys_value as $key => $value){
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue($abc[$key].'1',$value);
        }
        $n = 2;
        $r=1;
        foreach ($data as $key => $value){
            foreach ($value as $ke => $val){
                foreach ($val as $k => $v){
                    if(!is_array($v)){
                        $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue($abc[$k].($n) ,$v);
                    }else{
                        foreach ($v as $keys => $values){
                            $num = $k+$keys;
                            $spreadsheet->setActiveSheetIndex(0)->setCellValue($abc[$num].($n) ,$values."\t");
                        }
                    }
                }
                $n++;
            }
            $r++;
        }
        $spreadsheet->getActiveSheet()->setTitle($title);
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'(' . date('Ymd-His') . ').xls"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//        $writer->save('php://output');
        $xls_file=sys_get_temp_dir()."/".date('Ymd-His').".xlsx";
        $writer->save($xls_file);
        echo file_get_contents($xls_file);
        unset($data);
    }
}
