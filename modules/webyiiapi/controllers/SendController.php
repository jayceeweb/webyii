<?php
namespace app\modules\webyiiapi\controllers;

use app\modules\manager\models\AdminUser;
use Yii;
use GuzzleHttp\Client as GuzzleClient;
use app\components\helper\AuthcodeHelper;
use app\models\PgExchangeData;
use app\models\CoreConfigData;

class SendController extends BaseController {
    /**
     * 功能暂时没启用
     * 购物车发送优惠码邮件
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCart(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        //获取url前缀
        $MediaUrl=CoreConfigData::getMedia();
        //图片
        $BaseUrl=CoreConfigData::getBaseUrl();
        //网站
        //邮件配置
        $ACCOUNT_ID=2160;
        $AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
        $REWARD_MEMBERS_LIST_ID=3;
        //连接magento，邮件发送和日志

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dr=$objectManager->get("Magento\Framework\Filesystem\DirectoryList");
        //邮件
        $monitor_log=$objectManager->create("Pg\Monitor\Log");
        $monitor_log->log_start("cart");
        //日志

//        1.获取2020年1月1日以后注册并且没有订单的用户；
//        2.获取这些用户有效的购物车id
//        3.连接购物车详情，获取商品的图片，链接，名称，价格
//        4.筛选出最先加入购物车的商品
        //请求多次数据库  start

        $sql = "SELECT entity_id FROM customer_entity WHERE customer_entity.created_at > \"2020-01-11\" AND NOT EXISTS ( SELECT * FROM sales_order WHERE sales_order.customer_id = customer_entity.entity_id )";
//        1.获取2020年1月1日以后注册并且没有订单的用户；
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $customer_data = array();
        foreach ($data as $key => $val){
            $customer_data[] = $val['entity_id'];
        }
        $customer_data = array_flip($customer_data);
        $sql = "select customer_firstname as firstname,customer_lastname as lastname,customer_id,customer_email as email,entity_id from quote where is_active = 1 and subtotal>50";
//        2.获取这些用户有效的购物车id
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $quote_data = array();
        foreach ($data as $key => $value){
            if($value['customer_id']){
                $quote_data[$value['customer_id']] = $value;
            }
        }
        $customers_data = array_intersect_key($quote_data,$customer_data);
//        筛选出条件1,2都相同的用户
        $quote_item_data = array();
        $quote_id = '';
        foreach ($customers_data as $key => $val){
            $quote_item_data[$val['entity_id']] = $val;
            $quote_id .= $val['entity_id'].',';
        }
        $quote_id = rtrim($quote_id,',');
        $sql = "SELECT * FROM ( SELECT product_id, sku, `name`, ROUND((row_total / qty), 2) AS price, quote_id, created_at FROM quote_item WHERE quote_id IN ($quote_id) AND product_type = 'configurable' ) AS datas WHERE ( SELECT COUNT(0) FROM ( SELECT created_at, quote_id FROM quote_item WHERE quote_id IN ($quote_id) AND product_type = 'configurable' ) AS dataa WHERE dataa.quote_id = datas.quote_id AND dataa.created_at > datas.created_at ) < 2 ORDER BY datas.created_at DESC";
        $quote_items_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
//        连接购物车详情，获取商品的名称，价格
        $sku = '';
        foreach ($quote_items_data as $key => $value){
            $sku .= '\''.$value['sku'].'\',';
        }
        $sku = rtrim($sku,',');
        $sql = "select entity_id,image,sku from catalog_product_flat_1 where sku in ($sku)";
//        连接商品详情，获取商品的图片
        $catalog_product_flat_1_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $catalog_product_flat_1_datas = array();
        $catalog_product_flat_1_str = '';
        foreach ($catalog_product_flat_1_data as $key =>$value){
            $catalog_product_flat_1_datas[$value['entity_id']]['image'] = $value['image'];
            $catalog_product_flat_1_datas[$value['entity_id']]['sku'] = $value['sku'];
            $catalog_product_flat_1_str .= $value['entity_id'].',';
        }
        $catalog_product_flat_1_str = rtrim($catalog_product_flat_1_str,',');
        $sql = "SELECT entity_id, request_path as url FROM url_rewrite WHERE entity_id in ($catalog_product_flat_1_str) group by entity_id";
//        连接商品url表，获取商品的链接
        $url_rewrite_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $url_rewrite_datas = array();
//        数据处理，数据合并 start
        foreach ($url_rewrite_data as $key =>$value){
            $url_rewrite_datas[$value['entity_id']]['url'] = $value['url'];
        }
        $url_data = array();
        foreach ($catalog_product_flat_1_datas as $key => $value){
            foreach ($url_rewrite_datas as $k => $val){
                if($key == $k){
                    $url_data[$k] = array_merge($value,$val);
                }
            }
        }
        foreach ($quote_items_data as $key => $value){
            foreach ($url_data as $k => $v){
                if($value['sku'] == $v['sku']){
                    $quote_items_data[$key] = array_merge($value,$v);
                }
            }
        }
        $cart_data = array();
        foreach ($quote_items_data as $key => $value){
            foreach ($quote_item_data as $k => $v){
                if($value['quote_id'] == $k){
                    $cart_data[$v['email']][] = array_merge($value,$v);
                }
            }
        }
        //邮件发送
        $email_data = array();
        $log = array();
        $error_log = array();
        foreach ($cart_data as $key => $value){
            for($i=0;$i<count($value);$i++){
                $email_data[$key]['email'] = $key;
                $email_data[$key]['firstname'] = $value[$i]['firstname'];
                $email_data[$key]['lastname'] = $value[$i]['lastname'];
                $num = $i+1;
                $email_data[$key]['data']['quote_product_image'.$num] = $MediaUrl.'catalog/product/cache/600x300'.$value[$i]['image'];
                $email_data[$key]['data']['quote_product_name'.$num] = $value[$i]['name'];
                $email_data[$key]['data']['quote_product_price'.$num] = $value[$i]['price'];
                $email_data[$key]['data']['quote_product_url'.$num] = $BaseUrl.'/'.$value[$i]['url'];
            }
        }
        $document_root=$dr->getRoot();
        //delete lee 2020.12.4 替换为klaviyo
//        include_once $document_root."/lib/internal/Pg/Maropost/tests/autoloader.php";
//        $contacts=new Contacts($ACCOUNT_ID, $AUTHTOKEN);
//        $contacts->createOrUpdateForList($REWARD_MEMBERS_LIST_ID,$email_data['1053957645@qq.com']['email'],$email_data['1053957645@qq.com']['firstname'],$email_data['1053957645@qq.com']['lastname'],null,null,null,$email_data['1053957645@qq.com']['data']);
//        $client = new GuzzleClient();
//        $url=sprintf("https://api.maropost.com/accounts/2160/journeys/18/trigger/8181040588408.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$email_data['1053957645@qq.com']['email']);
//        $res = $client->request('POST', $url);
//        $err_no=$res->getBody()->getContents();
//        if (strtolower($err_no)=="success!") {
//            array_push($log,$data);
//        } else {
//            array_push($error_log,$data);
//        }
        //end
        if($log){
            $monitor_log->log(json_encode($log));
        }
        if($error_log){
            $monitor_log->log_error(["code"=>-1,"data"=>json_encode($error_log)]);
        }
    }

    public function actionList(){
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $sql = "SELECT psb.src_type, so.increment_id, psb.score, ce.firstname, ce.entity_id, lastname, email, ps.all_score, total_score, (6000 - all_score) AS diff_score FROM sales_order AS so INNER JOIN pg_score_bill AS psb ON psb.src_id = so.entity_id INNER JOIN customer_entity AS ce ON so.customer_id = ce.entity_id INNER JOIN pg_score AS ps ON ps.entity_id = so.customer_id AND psb.src_type = 'sales_order' AND so.increment_id = $content";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $customer_id = $data[0]["entity_id"];
        $recommend_list=Yii::$container->get("app\components\helper\RecommendHelper")->get_customer_recommend_products($customer_id);
        var_dump($recommend_list);
        die();
    }
    //购物车发送优惠码邮件
    public function actionSendActiveEmail($data){
//        100071804
        $content=$_GET['increment_id'];
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        //邮件配置
        $ACCOUNT_ID=2160;
        $AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
        $REWARD_MEMBERS_LIST_ID=13;
        $increment_id = $content;
        $sql = "SELECT psb.src_type, so.increment_id, psb.score, ce.firstname, ce.entity_id, lastname, email, ps.all_score, total_score, (6000 - all_score) AS diff_score FROM sales_order AS so INNER JOIN pg_score_bill AS psb ON psb.src_id = so.entity_id INNER JOIN customer_entity AS ce ON so.customer_id = ce.entity_id INNER JOIN pg_score AS ps ON ps.entity_id = so.customer_id AND psb.src_type = 'sales_order' AND so.increment_id = $increment_id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        //模拟

        //发送邮件
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dr=$objectManager->get("Magento\Framework\Filesystem\DirectoryList");
        $document_root=$dr->getRoot();
        include_once $document_root."/lib/internal/Pg/Maropost/tests/autoloader.php";
        $email = 'zhuqiuying@zhijingoptical.com';
        $porder_score = $data["score"];
        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $customer_id = $data["entity_id"];
        $Available_now = $data["total_score"];
        $Total_points = $data["all_score"];
        $diff_score = $data["diff_score"];
        $increment_id = $data["increment_id"];
        $recommend_list=Yii::$container->get("app\components\helper\RecommendHelper")->get_customer_recommend_products($customer_id);
        $data=[
            "avail_total_points"=>$Available_now,
            "total_points"=>$Total_points,
            "tmp_so_increment_id"=>$increment_id,
            "tmp_so_gaving_points"=>$porder_score,
        ];
        $recommend_count = 0;
        if(count($recommend_list)>=4){
            $recommend_count = 4;
        }else if(count($recommend_list)<4 && count($recommend_list)>=2){
            $recommend_count = 2;
        }else{
            $recommend_count = 0;
        }
        for ($key = 0;$key<$recommend_count;$key++){
            $data["recommend_product_image".($key+1)]=$recommend_list[$key]["image"];
            $data["recommend_product_name".($key+1)]=$recommend_list[$key]["name"];
            $data["recommend_product_price".($key+1)]=$recommend_list[$key]["product_price"];
            $data["recommend_product_url".($key+1)]=$recommend_list[$key]["url"];
        }
        if($diff_score > 0){
            $data["the_integral_of_the_difference"] = 6000-$Available_now;
        }
        //add lee 2020.12.4 增加klaviyo
        $customer_properties=array_merge(['$email'=>$email,'$first_name'=>$firstname,'$last_name'=>$lastname],$data);
        $properties=$data;
        if($Available_now >= 6000 ){
            $err_no=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("rewards active more than 6000",$customer_properties,$properties);
        } else {
            $err_no=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("rewards active less than 6000",$customer_properties,$properties);
        }
        //end

        //delete lee 2020.12.4
//        $contacts=new Contacts($ACCOUNT_ID, $AUTHTOKEN);
//        $contacts->createOrUpdateForList($REWARD_MEMBERS_LIST_ID,$email,$firstname,$lastname,null,null,null,$data);
//        $client = new GuzzleClient();
//        if($Available_now >= 6000 ){
//            $url=sprintf("https://api.maropost.com/accounts/2160/journeys/22/trigger/27579871241115.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$email);
//        }else if($Available_now < 6000 ){
//            $url=sprintf("https://api.maropost.com/accounts/2160/journeys/23/trigger/27579871241115.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$email);
//        }
//        $res = $client->request('POST', $url);
//        $err_no=$res->getBody()->getContents();
//        var_dump($err_no);
    }
}
