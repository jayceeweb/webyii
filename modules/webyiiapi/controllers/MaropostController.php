<?php
namespace app\modules\webyiiapi\controllers;

use Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use Yii;
use Maropost\Api\Journeys;
use GuzzleHttp\Client as GuzzleClient;
use app\components\helper\AuthcodeHelper;
use app\models\PgExchangeData;

class MaropostController extends BaseController {
    const LIST_ID=3;
    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
    const JOURNESID=4;
    private $journeys_config=[
        "OUT_FOR_DELIVERY"=>[
            "JOURNESID"=>4,
            "TRIGGER"=>"99949064890551",
        ],
        "CROSS_COUNTRY_TRANSFER"=>[
            "JOURNESID"=>10,
            "TRIGGER"=>"99949064890551",
        ],
        "REMAKE"=>[
            "JOURNESID"=>7,
            "TRIGGER"=>"99949064890551",
        ],
        "REMAKE1"=>[
            "JOURNESID"=>15,
            "TRIGGER"=>"99949064890551",
        ],
        "TEST"=>[
            "JOURNESID"=>11,
            "TRIGGER"=>"78958334700213",
        ],
    ];

    private function get_so($order_id) {
        $sql="SELECT so.entity_id,so.increment_id,so.state,so.coupon_code,so.shipping_description,so.`customer_id`,ce.`email`,so.subtotal,so.grand_total
,so.shipping_amount,so.total_qty_ordered,so.shipping_address_id,so.customer_email,ce.firstname as customer_firstname,ce.lastname as customer_lastname,so.shipping_method,so.created_at
,soa.`address_type`,soa.`city` as `web_city`,soa.`company` as web_company,soa.`postcode` as web_postcode,soa.`region` as web_region,soa.`street` as web_street,soa.`telephone` as web_telephone
FROM sales_order so
INNER JOIN customer_entity ce ON ce.`entity_id`=so.`customer_id`
INNER JOIN sales_order_address soa ON soa.`entity_id`=so.`shipping_address_id`
WHERE so.`entity_id`=%s";
        $sql=sprintf($sql,$order_id);
        $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $data;
    }

    private function get_so_items($item_ids) {
        $sql="SELECT flat.`entity_id`,flat.`sku`,flat.`name`,soi.`discount_amount`,soi.`price`,flat.`image`,flat.`attribute_set_id` ,pe.ladd,pe.radd,pe.`ladd` + pe.`radd` AS padd
FROM sales_order_item soi
LEFT JOIN prescription_entity pe ON pe.entity_id=soi.profile_prescription_id
INNER JOIN `catalog_product_flat_all_1` flat ON flat.`entity_id`=soi.`product_id`
WHERE soi.`item_id` IN (%s)
ORDER BY padd DESC";
        $sql=sprintf($sql,implode(",",$item_ids));
        $data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $data;
    }

    public function authcode_encode($data) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $secret=$cfg["secret_key"];

        return base64_encode(AuthcodeHelper::encode($data,$secret));
    }

    public function getJourneysUrl($event_type,$email){
        $url=sprintf("https://api.maropost.com/accounts/%s/journeys/%s/trigger/%s.json?auth_token=%s&email=%s",
            static::ACCOUNT_ID,
            $this->journeys_config[$event_type]["JOURNESID"],
            $this->journeys_config[$event_type]["TRIGGER"],
            static::AUTHTOKEN,
            $email);
        return $url;
    }

    public function sendMail($event_type,$email,$tags) {
        //$email=$tags["email"];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $monitor_log=new \Pg\Monitor\Log();
        $monitor_log->log_start("maropost-sendMail");
        $monitor_log->log(["tags"=>$tags]);
        try {
            $tmp_glasses_other_qty=(int)$tags["glasses_qty"] - 1;
            $d=[
                "email_auth_code"=>$this->authcode_encode($email),
                "tmp_customer_firstname"=>$tags["customer_firstname"],
                "tmp_customer_lastname"=>$tags["customer_lastname"],
                "tmp_shipping_city"=>$tags["web_city"],
                "tmp_shipping_company"=>$tags["web_company"],
                "tmp_shipping_delivery_count"=>$tags["delivery_count"],
                "tmp_shipping_postcode"=>$tags["postcode"],
                "tmp_shipping_region"=>$tags["region"],
                "tmp_shipping_street"=>$tags["street"],
                "tmp_shipping_telephone"=>$tags["telephone"],
                "tmp_shipping_tracking_code"=>$tags["tracking_code"],
                "tmp_shipping_tracking_code_auth_code"=>$this->authcode_encode($tags["tracking_code"]),
                "tmp_so_increment_id"=>$tags["increment_id"],
                "tmp_first_product_sku"=>$tags["first_product_sku"],
                "tmp_first_product_name"=>$tags["first_product_name"],
                "tmp_first_product_img_url"=>$tags["first_product_img_url"],
                "tmp_glasses_qty"=>$tags["glasses_qty"],
                "tmp_glasses_other_qty"=> $tmp_glasses_other_qty,
                "tmp_first_product_url"=>$tags["first_product_url"],
                "tmp_progressive_count"=>$tags["progressive_count"],
                "tmp_exchange_id_auth_code"=>$this->authcode_encode($tags["tmp_exchange_id_auth_code"]),
                "tmp_ep_label_url"=>$tags["raw_content"]["ep_label_url"],
                "tmp_ep_public_url"=>$tags["raw_content"]["ep_public_url"],
            ];
            //delete lee 2020.12.7 改为klaviyo
            //$url=$this->getJourneysUrl($event_type,$email);
            //end
            $mail_ms_data=[
//                delete lee 2020.12.7 改为klaviyo
//                "account_id"=>static::ACCOUNT_ID,
//                "list_id"=>static::LIST_ID,
//                "authtoken"=>static::AUTHTOKEN,
//                "url"=>$url,
//                  end
                "email"=>$email,
                "customer_firstname"=>$tags["customer_firstname"],
                "customer_lastname"=>$tags["customer_lastname"],
                "customer_data"=>$d,
                "event_type"=>$event_type
            ];
            if ($event_type=="OUT_FOR_DELIVERY") {
                $customer_id=$tags["customer_id"];
                if (empty($customer_id)) {
                    $customer_id=Yii::$container->get("app\components\helper\RecommendHelper")->get_customer_id_by_email($tags["email"]);
                }
                $recommend_list=Yii::$container->get("app\components\helper\RecommendHelper")->get_customer_recommend_products($customer_id);
                foreach ($recommend_list as $key=>$recommend) {
                    $mail_ms_data["customer_data"]["recommend_product_image".($key+1)]=$recommend["image"];
                    $mail_ms_data["customer_data"]["recommend_product_name".($key+1)]=$recommend["name"];
                    $mail_ms_data["customer_data"]["recommend_product_price".($key+1)]=$recommend["product_price"];
                    $mail_ms_data["customer_data"]["recommend_product_url".($key+1)]=$recommend["url"];
                }
            }
            if ($event_type=="REMAKE") {
                $mail_ms_data["customer_data"]["tmp_event_type"]="remake";
            }
            if ($event_type=="REMAKE1") {
                $mail_ms_data["customer_data"]["tmp_event_type"]="remake";
            }
            $ret=Yii::$app->redis_cache->lpush("CACHE_MAROPOST_DELIVERY_LIST",json_encode($mail_ms_data));
            $monitor_log->log_stop(["send_ret"=>$ret,"mail_ms_data"=>$mail_ms_data]);
            return 0;
        } catch (\Exception $e) {
            $monitor_log->log(["exception"=>$e->getMessage()]);
        }
        return -1;
    }

    public function actionDelivery(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();
        $session=Yii::$app->session;
        $session->open();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $monitor_log=new \Pg\Monitor\Log();
        $monitor_log->log_start("maropost-delivery");
        $ret=[];
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
//        $content=json_decode(file_get_contents("/data/webyii/www/commands/remake_send_email.json"),true);
        $parameters=$content["parameters"];
        $monitor_log->log(["parameters"=>$parameters]);
        if (!empty($parameters)) {
            $ret_message=[];
            $event_type=$parameters["event_type"];
            if (isset($parameters["shipments"])
                && in_array($event_type,["OUT_FOR_DELIVERY","CROSS_COUNTRY_TRANSFER","REMAKE","REMAKE1","TEST"])) {
                $shipments=$parameters["shipments"];
                $err_message=[];
                for ($k=0;$k<count($shipments);$k++) {
                    $shipment=$shipments[$k];
                    $shipment=$shipment["shipment"];
                    //保存每个shipment
                    $pgExchangeModel=new PgExchangeData();
                    $pgExchangeModel->customer_id=0;
                    $pgExchangeModel->exchange_code="lab_delivery";
                    $exchange_data=[
                        "data"=>$shipment,
                        "event_type"=>$event_type
                    ];
                    $pgExchangeModel->data_content=json_encode($exchange_data);
                    $pgExchangeModel->save();
                    //处理shipment
                    $order_number=$shipment["web_order_number"];
                    $order_entity=$shipment["web_order_entity_id"];
                    $created_at=$shipment["created_at"];
                    $tracking_code=$shipment["ep_tracking_code"];
                    $glasses_qty=$shipment["glasses_qty"];
                    $items=$shipment["items"];
                    $item_ids=[];
                    foreach ($items as $j=>$item) {
                        $product_id=$item["web_product_id"];
                        $item_id=$item["web_item_id"];
                        $item_ids[]=$item_id;
                    }
                    $progressive_count=0;
                    $so=$this->get_so($order_entity);
                    $so_items=$this->get_so_items($item_ids);
                    $tags=[];
                    $tags["tmp_exchange_id_auth_code"]=$pgExchangeModel->getAttribute("exchange_id");
                    $tags["first_product_url"]="";
                    $tags["first_product_sku"]="";
                    $tags["first_product_name"]="";
                    $tags["first_product_img_url"]="";
                    $tags["glasses_qty"]=1;
                    $tags["progressive_count"]=0;
                    $tags=array_merge($tags,$so);
                    $t2=["my_string2"=>"李海峰","items"=>"i1,i2","delivery_count"=>count($item_ids),"tracking_code"=>$tracking_code];
                    $tags=array_merge($tags,$t2);
                    $items_strs="";
                    $so_items_index=0;
                    foreach ($so_items as $i=>$so_item) {
                        if ($so_item["padd"] > 0) {
                            $progressive_count=$progressive_count+1;
                        }
                    }
                    $tags["progressive_count"]=$progressive_count;
                    foreach ($so_items as $i=>$so_item) {
                        $so_items_index=$so_items_index+1;
                        $items_str="";
                        foreach ($so_item as $key=>$v) {
                            if ($items_str=="") {
                                $items_str=sprintf("%s",$v);
                            } else {
                                $items_str=$items_str."，".sprintf("%s",$v);
                            }
                        }
                        //增加图片
                        if ($so_items_index==1) {
                            $first_product_sku=$so_item["sku"];
                            $first_product_name=$so_item["name"];
                            $first_product_img_url=Yii::$container->get("app\models\CoreConfigData")->getStaticUrl()."/media/catalog/product/cache/600x300".$so_item["image"];
                            $ACCESSORIES_ATTRIBUTE_SET_IDS = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["ACCESSORIES_ATTRIBUTE_SET_IDS"];
                            if (in_array($so_item["attribute_set_id"],$ACCESSORIES_ATTRIBUTE_SET_IDS)) {
                                $sql=sprintf("select entity_id from catalog_product_entity where sku='%s'",$so_item["sku"]);
                                $simple_id=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()["entity_id"];
                                $first_product_url=Yii::$container->get("app\models\CoreConfigData")->getBaseUrl()."/".Yii::$container->get("app\models\UrlRewrite")->getSimpleUrl($simple_id);
                            } else {
                                $first_product_url=Yii::$container->get("app\models\CoreConfigData")->getBaseUrl()."/".Yii::$container->get("app\models\UrlRewrite")->getSimpleUrl($so_item["entity_id"])."?s=".$so_item["sku"];
                            }
                            $tags["first_product_url"]=$first_product_url;
                            $tags["first_product_sku"]=$first_product_sku;
                            $tags["first_product_name"]=$first_product_name;
                            $tags["first_product_img_url"]=$first_product_img_url;
                            $tags["glasses_qty"]=$glasses_qty;
                        }
                        if ($items_strs=="") {
                            $items_strs.=$items_str;
                        } else {
                            $items_strs.=$items_strs."|".$items_str;
                        }
                    }
                    $tags["so_items"]=$items_strs;
                    $to_address=$shipment["to_address"];
                    $tags["city"]=$to_address["city"];
                    $tags["company"]=$to_address["company"];
                    $tags["postcode"]=$to_address["zip"];
                    $tags["region"]=$to_address["state"];
                    $tags["street"]=$to_address["street1"];
                    if (!empty($to_address["street2"])) {
                        $tags["street"]=$tags["street"]." ".$to_address["street2"];
                    }
                    $tags["telephone"]=$to_address["phone"];
                    foreach ($tags as $key=>$tag) {
                        if (empty($tag) && $tag！=0) {
                            $tags[$key]="";
                        }
                    }
                    $tags["raw_content"]=$shipment;
                    $ret_message[]=$order_number;
                    $monitor_log->log(["send_tags"=>$tags]);
                    if ($event_type=="OUT_FOR_DELIVERY") {
                        $email="email-marketing@payneglasses.com";
                        $errno=$this->sendMail($event_type,$email,$tags);
                    } else if ($event_type=="CROSS_COUNTRY_TRANSFER"){
//                        $email="zhangyaqin_0@163.com";
//                        $errno=$this->sendMail($event_type,$email,$tags);
//                        $email="email-marketing@payneglasses.com";
//                        $errno=$this->sendMail($event_type,$email,$tags);
                        $email=$tags["email"];
                        $errno=$this->sendMail($event_type,$email,$tags);
                    } else if ($event_type=="REMAKE"){
//                        $email="zhangyaqin_0@163.com";
//                        $errno=$this->sendMail($event_type,$email,$tags);
//                        $email="email-marketing@payneglasses.com";
//                        $errno=$this->sendMail($event_type,$email,$tags);
//                        $email=$tags["email"];
//                        $errno=$this->sendMail($event_type,$email,$tags);
                    } else if ($event_type=="REMAKE1"){
//                        $email="zhangyaqin_0@163.com";
//                        $errno=$this->sendMail($event_type,$email,$tags);
//                        $email=$tags["email"];
//                        $errno=$this->sendMail($event_type,$email,$tags);
                    } else if ($event_type=="TEST"){
                        $email="zhangyaqin_0@163.com";
                        $errno=$this->sendMail($event_type,$email,$tags);
                    }
                    $err_message=$errno;
                }
            }
            $ret=["code"=>0,"message"=>$ret_message];
        } else {
            $ret=["code"=>-1,"message"=>"param error"];
        }
        $monitor_log->log_stop($ret);

        $session->close();
        return $ret;
    }
}
