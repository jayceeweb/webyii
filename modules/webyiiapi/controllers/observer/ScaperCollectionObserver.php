<?php
namespace app\modules\webyiiapi\controllers\observer;

use app\models\EventsUsersMap;
use Braintree\Exception;
use Yii;
use yii\log\Logger;

class ScaperCollectionObserver extends DataCollectionInterface {
//    public function execute() {
//        $d=json_decode($this->getData(),true);
//        $collection=$d["collection"];
//        foreach ($collection as $key=>$value) {
//            if (strtolower($key)==strtolower("productview")) {
//                $products=$value;
//                foreach ($products as $product) {
//                    $p=$product["product"];
//                    $c=$product["customer"];
//                    $p_data=[
//                        "action"=>"productview",
//                        "data"=>$product
//                    ];
//                    $ret=Yii::$app->redis_cache->lpush("LIST_DATA_COLLECTION",json_encode($p_data));
//                }
//            }
//        }
//        return 0;
//    }

    public function execute() {
        try{
            //获取观察监测到的数据并解析json为php数组
            $jsonData=json_decode($this->getData(),true);
            //校验事件数据字段
            if(!empty($jsonData)) {
                $jsonD = json_decode($jsonData['data'],true);
                //获取采集开关
                $isOpen = isset($jsonD['is_open']) ? $jsonD['is_open'] : 1;
                if($isOpen) {
                    //判断是否登录，将前端的deviceId与用户中心id进行匹配
                    $customerInfo = $this->_getCollectionCustomerData();
                    if(!empty($customerInfo['customer_id'])) {
                        $jsonD['customerId'] = $customerInfo['customer_id'];
                    } else {
                        $jsonD['customerId'] = 0;
                    }
                    if(!empty($customerInfo['profile_id'])) {
                        $jsonD['profileId'] = $customerInfo['profile_id'];
                    } else {
                        $jsonD['profileId'] = 0;
                    }
                    $opData = json_encode(array("data"=>json_encode($jsonD)));
                    //将事件数据存储进入redis队列,服务端脚本进行细节处理
                    $redis = $this->redisObj();
                    $redis->lpush("COLLECTION_LISTS",$opData);
//                    Yii::$app->redis_cache->lpush("COLLECTION_LISTS",$opData);
                } else {
                    //采集关闭，不做任何操作
                    return true;
                }
            }
        } catch(\Exception $e) {
            Yii::getLogger()->log($e->getMessage(), Logger::LEVEL_TRACE);
            return false;
        }

        return 0;
    }

    //验证用户是否登录，获取用户中心id
    public function _getCollectionCustomerData() {
        $data = [];
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
//        $session=Yii::$app->session;
//        $session->open();
//        $s=Yii::$container->get("app\components\helper\CustomerHelper");
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer_id = $objectManager->get("Magento\Customer\Model\Session")->getCustomerId();
        $profile_id = $objectManager->get("Magento\Customer\Model\Session")->getProfileId();

        $data = ['customer_id' => $customer_id, "profile_id" => $profile_id];
        return $data;
    }

    //切换到magento的redis服务
    public function redisObj() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["cache"];
        $server=$cfg["frontend"]["default"]["backend_options"]["server"];
        $port=$cfg["frontend"]["default"]["backend_options"]["port"];
        $database=$cfg["frontend"]["default"]["backend_options"]["database"];
        $redis=new \Credis_Client($server, $port, 30,false, $database, null);
        return $redis;
    }
}