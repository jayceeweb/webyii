<?php
namespace app\modules\webyiiapi\controllers\observer;

abstract class DataCollectionInterface
{
    private $data;

    public function setData($d) {
        $this->data=$d;
    }

    public function getData() {
        return $this->data;
    }

    abstract public function execute();
}