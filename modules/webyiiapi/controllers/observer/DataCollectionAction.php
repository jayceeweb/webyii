<?php
namespace app\modules\webyiiapi\controllers\observer;

class DataCollectionAction{
    /**
     * @var array 保存所有已注册的观察者
     */
    public $_observer = [];

    /**
     * @purpose: 添加观察者
     * @param string $key 给所添加的观察者的一个唯一 key，方便从注册树中移除观察者
     * @param Observer $observer 观察者对象
     * @return mixed
     */
    public function addObserver($key, DataCollectionInterface $observer)
    {
        $this->_observer[$key] = $observer;
    }

    /**
     * @purpose: 从注册树中移除观察者
     * @param string $key 给所添加的观察者的一个唯一 key，方便从注册树中移除观察者
     * @return mixed
     */
    public function removeObserver($key)
    {
        unset($this->_observer[$key]);
    }

    /**
     * @purpose: 广播通知以注册的观察者，对注册树进行遍历，让每个对象实现其接口提供的操作
     * @return mixed
     */
    public function notify()
    {
        foreach ($this->_observer as $observer) {
            $observer->execute();
        }
    }
}