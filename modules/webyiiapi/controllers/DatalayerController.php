<?php
namespace app\modules\webyiiapi\controllers;

use app\components\helper\AuthcodeHelper;
use app\modules\manager\models\AdminUser;
use Yii;

class DatalayerController extends BaseController {
    public function actionSave(){
        //订单成功页新增dataLayer记录
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        //日志开始
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $monitor_log=$objectManager->create("Pg\Monitor\Log");
        $log = array();
        $log['content'] = $content;
        //end
        if(!isset($content['increment_id']) ||!isset($content['datalayer'])){
            //日志开始
            $monitor_log->log_error(["code"=>-1,"data"=>$log,"model"=>'success_order_add_dataLayer']);
            //end
            die(json_encode(["code"=>-2,"msg"=>"Error","data"=>"The fetch data is undefined"]));
        }
        $increment_id = $content['increment_id'];
        $datalayer = $content['datalayer'];
        if(!$content['code']){
            $monitor_log->log_error(["code"=>-5,"data"=>$log,"model"=>'success_order_add_dataLayer']);
        }

        if(empty($increment_id)||empty($datalayer)){
            //日志开始
            $monitor_log->log_error(["code"=>-2,"msg"=>$log,"model"=>'success_order_add_dataLayer']);
            //end
            die(json_encode(["code"=>-3,"msg"=>"Error","data"=>"The fetch data is null"]));
        }

        $sql = sprintf("select * from sales_order where increment_id = '%s'",$increment_id);
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        if(empty($data)){
            //日志开始
            $monitor_log->log_error(["code"=>-3,"msg"=>$log,"model"=>'success_order_add_dataLayer']);
            //end
            die(json_encode(["code"=>-4,"msg"=>"Error","data"=>"The increment_id is error"]));
        }


        $datalayer = json_encode($datalayer);
        $time = date("Y-m-d H:i:s", time());
        $internet['userAgent'] = $content['userAgent'];
        $internet['platform'] = $content['platform'];
        $internet = json_encode($internet);
        try{
            $sql = "insert into datalayer_content (`increment_id`,`datalayer`,`create_at`,`internet`) values ('$increment_id','$datalayer','$time','$internet')";
            $data = Yii::$app->db->createCommand($sql)->execute();
        } catch (\Exception $e) {
            //日志开始
            $monitor_log->log_error(["code"=>-4,"msg"=>$log,"model"=>'success_order_add_dataLayer',"error"=>$e]);
            //end
            die(json_encode(["code"=>-1,"msg"=>"Error","data"=>"Data addition failure"]));
        }
        die(json_encode(["code"=>0,"msg"=>"Success","data"=>$data]));
    }
}
