<?php
namespace app\modules\webyiiapi\controllers;

use app\components\helper\AuthcodeHelper;
use app\modules\manager\models\AdminUser;
use Yii;

class RelatedController extends BaseController {
    public function actionGetPrescriptionId(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $sql = "SELECT
	qi.additional_data as quote_additional_data,
	soi.additional_data as order_additional_data,
	soi.profile_prescription_id as order_profile_prescription_id,
	soi.glasses_prescription_id as order_glasses_prescription_id,
	qi.profile_prescription_id as quote_profile_prescription_id,
	qi.glasses_prescription_id as quote_glasses_prescription_id
FROM
	sales_order AS so
INNER JOIN sales_order_item AS soi ON so.entity_id = soi.order_id
AND (
	soi.profile_id != 0
	AND soi.product_id IS NOT NULL
	AND soi.product_id != ''
)
INNER JOIN quote_item as qi on qi.item_id = soi.quote_item_id
WHERE
	so.increment_id = '{$content['increment_id']}'";
        return $sql;
    }
}
