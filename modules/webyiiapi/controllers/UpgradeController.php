<?php
namespace app\modules\webyiiapi\controllers;

use app\components\helper\AuthcodeHelper;
use app\modules\manager\models\AdminUser;
use Yii;

class UpgradeController extends BaseController {
    public function actionUpdates(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //开启错误日志
        $monitor_log=$objectManager->create("Pg\Monitor\Log");

        $session = $objectManager->get('\Magento\Customer\Model\Session');
        $original_data =Yii::$app->getRequest()->getRawBody();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $log = array("msg"=>'Upgrade quote items');
        try{
            if(!$content){
                //日志开始
                $log['getRawBody'] = Yii::$app->getRequest()->getRawBody();
                $monitor_log->log_error(["code"=>-1,"data"=>$log,"model"=>'The current fetch data is empty']);
                //end
                die(json_encode(["code"=>-2,"msg"=>"Error","data"=>"The current fetch data is empty"]));
            }
            $quote_item_id = $content['quote_item_id'];
            $lens_sku = $content['lens_sku'];
            $addcart_data = '';
            if(isset($content['addcart_data'])){
                $addcart_data = $content['addcart_data'];
            }else{
                $sql = "select additional_data from quote_item where item_id = $quote_item_id";
                $additional_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                $additional_data = unserialize($additional_data['additional_data']);
                $addcart_data = $additional_data['params'];
            }
            $glass_type = $content['glass_type'];

            $old_sku = $addcart_data['orderglass']['Lenss']['sku'];
            $old_item_id = $quote_item_id;
            $old_additional_data = json_encode($addcart_data);
            $upgrade_type = $glass_type;

            switch ($glass_type){
                case 'blue_sku': $addcart_data = $this->blue_sku($addcart_data,$lens_sku);break;
                case 'distance_sku': $addcart_data = $this->distance_sku($addcart_data,$lens_sku);break;
                case 'reading_sku': $addcart_data = $this->reading_sku($addcart_data,$lens_sku);break;
                case 'tint_sku': $addcart_data = $this->tint_sku($addcart_data,$lens_sku);break;
                case 'polarize_sku': $addcart_data = $this->polarize_sku($addcart_data,$lens_sku);break;
            }

            if(!$addcart_data){
                //日志开始
                $log['data'] = $old_additional_data;
                $monitor_log->log_error(["code"=>-1,"data"=>$log,"model"=>'addcart_data is empty']);
                //end
                die(json_encode(["code"=>-2,"msg"=>"Error","data"=>"addcart_data is empty"]));
            }
            if($session->isLoggedIn()){
                $sql = "select profile_prescription_id from quote_item where item_id = $quote_item_id";
                $profile_prescription_id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                if($profile_prescription_id){
                    $profile_prescription_id = $profile_prescription_id[0]['profile_prescription_id'];
                    $addcart_data['prescription']['id'] = $profile_prescription_id;
                }
            }
            //新增购物车
            $sql = "select qty from quote_item where item_id = $quote_item_id";
            $qty_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $addcart_data['qty'] = $qty_data['qty'];
            $code = $objectManager->get('\Pg\CheckoutExtend\Controller\Cart\Add')->execute($addcart_data);
            if($code['code'] == 0){
                $new_sku = $addcart_data['orderglass']['Lenss']['sku'];
                $new_quote_id = $code['message'];
                $new_item_id = $code['new_item_id'];
                $new_additional_data = json_encode($addcart_data);
                $old_additional_data = str_replace('\'', '\'\'', $old_additional_data);
                $new_additional_data = str_replace('\'', '\'\'', $new_additional_data);
                $sql = "INSERT INTO quote_upgrade (old_sku,new_sku,old_additional_data,new_additional_data,old_item_id,new_quote_id,upgrade_type,new_item_id,original_data) VALUES ('{$old_sku}','{$new_sku}','{$old_additional_data}','{$new_additional_data}','{$old_item_id}','{$new_quote_id}','{$upgrade_type}','{$new_item_id}','$original_data')";
                Yii::$app->db->createCommand($sql)->execute();
                if($glass_type == 'blue_sku'){
                    die(json_encode(array("code"=>0,"msg"=>"Add successfully, remove old cart items")));
                }else{
                    die(json_encode(array("code"=>-1,"msg"=>"Add successfully, refresh current page")));
                }
            }else{
                $log['data'] = $addcart_data;
                $log['code'] = $code;
                $monitor_log->log_error(["code"=>-1,"data"=>$log,"model"=>'Add failure']);
                die(json_encode(array("code"=>-2,"msg"=>"Add failure")));
            }
        }catch (\Exception $e){
            die(json_encode(array("code"=>-4,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    public function blue_sku($addcart_data,$lens_sku){
        $addcart_data['orderglass']['Lenss'] = $this->get_data($lens_sku);
        $addcart_data['orderglass']['Coating'] = $this->get_data('VCAR1');
        return $addcart_data;
    }
    public function distance_sku($addcart_data,$lens_sku){
        $prescription_type = 'S';
        $progressive_type = 'DISTANCE ONLY';
        $use_for = 'DISTANCE';
        $prescription_data = $addcart_data ['profile_prescription'];
        $prescription_data['prescription_type'] = $prescription_type;
        $prescription_data['progressive_type'] = $progressive_type;
        $prescription_data['use_for'] = $use_for;
        $prescription = $this->PyCurl($prescription_data);
        if($prescription){
            $addcart_data['prescription'] = $prescription['prescription'];
        }
        $addcart_data['orderglass']['Lenss'] = $this->get_data($lens_sku);
        unset($addcart_data['orderglass']['PAL_Design']);
        return $addcart_data;
    }
    public function reading_sku($addcart_data,$lens_sku){
        $prescription_type = 'S';
        $progressive_type = 'READING ONLY';
        $use_for = 'READING';
        $prescription_data = $addcart_data ['profile_prescription'];
        $prescription_data['prescription_type'] = $prescription_type;
        $prescription_data['progressive_type'] = $progressive_type;
        $prescription_data['use_for'] = $use_for;
        $prescription = $this->PyCurl($prescription_data);
        if($prescription){
            $addcart_data['prescription'] = $prescription['prescription'];
        }
        $addcart_data['orderglass']['Lenss'] = $this->get_data($lens_sku);
        unset($addcart_data['orderglass']['PAL_Design']);
        return $addcart_data;
    }
    public function tint_sku($addcart_data,$lens_sku){
        if($addcart_data['orderglass']['Lenss']['sku'] == 'LSC56'){
            $addcart_data['orderglass']['Coating'] = $this->get_data('VCAR1');
        }
        foreach ($lens_sku as $key => $value){
            switch ($key){
                case 'lens_sku':$addcart_data['orderglass']['Lenss'] = $this->get_data($value);break;
                case 'color_sku':$addcart_data['orderglass']['Tint'] = $this->get_data($value);break;
            }
        }
        return $addcart_data;
    }
    public function polarize_sku($addcart_data,$lens_sku){
        $addcart_data['orderglass']['Lenss'] = $this->get_data($lens_sku);
        return $addcart_data;
    }
    public function actionRemove(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $quote_item_id = $content['quote_item_id'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $delete_code = $objectManager->get("Pg\CheckoutExtend\Controller\Cart\Delete")->deleteByid($quote_item_id);
        return true;
    }
    public function get_data($lens_sku){
        $sql = "SELECT sku, `name`, entity_id AS productId, price AS `price`, price AS `price-original`, ('lenss') AS `type`, IFNULL(color, '') AS color, ('false') AS colorname, price AS `special_price`,(1)as `qty` 
FROM catalog_product_flat_1 
WHERE sku = '{$lens_sku}'";
        $data=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->queryAll();
        if(!$data){
            return null;
        }
        $data = $data[0];
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$product=$objectManager->get("\Magento\Catalog\Api\ProductRepositoryInterface")->get($data['sku']);
        $sql="select sku, `name`,price,special_price 
FROM catalog_product_flat_1 
WHERE sku = '{$data['sku']}'
";
        $products=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->queryAll();
        if (!empty($products)) {
            $product=$products[0];
            $special_price = $product["special_price"];
            if($special_price){
                $data['price'] = $product["price"];
                $data['special_price'] = $special_price;
            }
        }
        return $data;
    }
    public function PyCurl($parameters){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product=$objectManager->get("\Magento\Framework\App\Config\ScopeConfigInterface");
        $convert_rx_api = $product->getValue('orderglass/general/convert_rx_api');
        $service_url=$convert_rx_api;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//规避SSL验证
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($parameters));
        $curl_response = curl_exec($curl);
        $data = json_decode($curl_response,true);
        if($data['code'] == 0){
            return array("prescription"=>$data['glasses_prescription'],"profile_prescription"=>$data['profile_prescription']);
        }
        return null;
    }
}
