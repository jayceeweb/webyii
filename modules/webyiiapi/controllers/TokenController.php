<?php
namespace app\modules\webyiiapi\controllers;

use app\components\helper\AuthcodeHelper;
use app\modules\manager\models\AdminUser;
use Yii;

class TokenController extends ApiController {
    public function actionGetToken(){
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $username=$content['username'];
        $password=$content['password'];
        $model = new AdminUser();
        $model->username=$username;
        $model->password=$password;
        if (!empty($username) && !empty($password)) {
            $adminuser=AdminUser::find()->where(["username"=>$username])->one();
            if ($adminuser) {
                $p=Yii::$container->get("app\components\Encryptor");
                $checkpassword=$p->isValidHash($password,$adminuser->getAttribute("password"));
                if ($checkpassword) {
                    $string = $username."-".$password."-".Yii::$app->getSecurity()->generateRandomString();
                    $string = AuthcodeHelper::encode_ex($string);
                    return ["code"=>0,"Authorization "=>$string];
                }
            }
        } else {
            return ["code"=>-1,"message"=>"param error"];
        }
    }
}
