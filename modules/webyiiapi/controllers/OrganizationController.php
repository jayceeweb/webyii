<?php
namespace app\modules\webyiiapi\controllers;

use Yii;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use app\models\SalesOrder;

class OrganizationController extends BaseController {
    // add sunjinchao 20/4/29 TagExcalUpdate

    public function actionExcaldatas(){
        $coupon=Yii::$app->getRequest()->get('coupon','');
        $start_order_time=Yii::$app->getRequest()->get('start_order_time','');
        $end_order_time=Yii::$app->getRequest()->get('end_order_time','');
        $excal_data = array();
        $sales_data = SalesOrder::getSalesOrderByCoupon($coupon,$start_order_time,$end_order_time);
        $title = $coupon."列表";
        $key_value = 'entity_id,state,coupon_code,shipping_description,customer_id,discount_amount,grand_total,shipping_amount,subtotal,total_qty_ordered,subtotal_refunded,total_paid,total_canceled,total_refunded,quote_id,increment_id,customer_email,customer_firstname,customer_lastname,customer_gender,shipping_method,total_item_count,has_warranty,warranty,so';
        foreach ($sales_data[0] as $key => $value){
            $key_value .= $key.',';
        }
        $key = explode(',',$key_value);
        foreach ($key as $k => $v){
            $excal_data[0][$v] = '';
        }
        $data = $excal_data;
        $abc = range('A','Z');
        $count = count($data[0]);

//        $this->actionGetscoreExcal($key_value,$excal_data,$title);
    }
    public function actionGetscoreExcals($key_value,$data,$title){
        ini_set("memory_limit", "2048M");
        set_time_limit(0);
        $keys_value = explode(",",$key_value);
        $abc = range('A','Z');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');
        foreach ($keys_value as $key => $value){
            if($key <= 26){
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue($abc[$key].'1',$value);
            }else{}
        }
        $n = 2;
        foreach ($data as $key => $value){
            foreach ($value as $ke => $val){
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue($abc[$ke].($n) ,$val);
            }
            $n++;
        }
        $spreadsheet->getActiveSheet()->setTitle($title);
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'(' . date('Ymd-His') . ').xls"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        unset($data);
    }






    public function actionExcaldata(){
        $coupon=Yii::$app->getRequest()->get('coupon','');
        $start_order_time=Yii::$app->getRequest()->get('start_order_time','');
        $end_order_time=Yii::$app->getRequest()->get('end_order_time','');
        $excal_data = array();
        $sales_data = SalesOrder::getSalesOrderByCoupon($coupon,$start_order_time,$end_order_time);
        $title = $coupon."列表";
        $key_value = "increment_id,state,customer,discount_amount,total,shipping,warranty";
        $key = explode(',',$key_value);
        foreach ($key as $k => $v){
            $excal_data[0][$v] = '';
        }
        if($sales_data){
            foreach ($sales_data as $key => $value){
                foreach ($value as $k => $v){
                    switch ($k)
                    {
                        case 'increment_id':
                            if(!isset($excal_data[$key]['increment_id'])){
                                $excal_data[$key]['increment_id'] = '';
                            }
                            $excal_data[$key]['increment_id'] = $v."\r\n";
                            break;
                        case 'state':
                            if(!isset($excal_data[$key]['state'])){
                                $excal_data[$key]['state'] = '';
                            }
                            $excal_data[$key]['state'] = $v."\r\n";
                            break;
                        case 'customer_email':
                            if(!isset($excal_data[$key]['customer'])){
                                $excal_data[$key]['customer'] = '';
                            }
                            $excal_data[$key]['customer'] .= 'email='.$v."\r\n";
                            break;
                        case 'customer_firstname':
                            if(!isset($excal_data[$key]['customer'])){
                                $excal_data[$key]['customer'] = '';
                            }
                            $excal_data[$key]['customer'] .= 'firstname='.$v."\r\n";
                            break;
                        case 'customer_lastname':
                            if(!isset($excal_data[$key]['customer'])){
                                $excal_data[$key]['customer'] = '';
                            }
                            $excal_data[$key]['customer'] .= 'lastname='.$v."\r\n";
                            break;
                        case 'discount_amount':
                            $excal_data[$key]['discount_amount'] = number_format($v,2)."\r\n";
                            break;
                        case 'grand_total':
                            if(!isset($excal_data[$key]['total'])){
                                $excal_data[$key]['total'] = '';
                            }
                            $excal_data[$key]['total'] .= 'grand_total='.$v."\r\n";
                            break;
                        case 'subtotal':
                            if(!isset($excal_data[$key]['total'])){
                                $excal_data[$key]['total'] = '';
                            }
                            $excal_data[$key]['total'] .= 'subtotal='.$v."\r\n";
                            break;
                        case 'total_qty_ordered':
                            if(!isset($excal_data[$key]['total'])){
                                $excal_data[$key]['total'] = '';
                            }
                            $excal_data[$key]['total'] .= 'total_qty_ordered='.$v."\r\n";
                            break;
                        case 'subtotal_refunded':
                            if(!isset($excal_data[$key]['total'])){
                                $excal_data[$key]['total'] = '';
                            }
                            $excal_data[$key]['total'] .= 'subtotal_refunded='.$v."\r\n";
                            break;
                        case 'total_paid':
                            if(!isset($excal_data[$key]['total'])){
                                $excal_data[$key]['total'] = '';
                            }
                            $excal_data[$key]['total'] .= 'total_paid='.$v."\r\n";
                            break;
                        case 'total_canceled':
                            if(!isset($excal_data[$key]['total'])){
                                $excal_data[$key]['total'] = '';
                            }
                            $excal_data[$key]['total'] .= 'total_canceled='.$v."\r\n";
                            break;
                        case 'total_refunded':
                            if(!isset($excal_data[$key]['total'])){
                                $excal_data[$key]['total'] = '';
                            }
                            $excal_data[$key]['total'] .= 'total_refunded='.$v."\r\n";
                            break;
                        case 'shipping_amount':
                            if(!isset($excal_data[$key]['shipping'])){
                                $excal_data[$key]['shipping'] = '';
                            }
                            $excal_data[$key]['shipping'] .= 'shipping_amount='.$v."\r\n";
                            break;
                        case 'shipping_method':
                            if(!isset($excal_data[$key]['shipping'])){
                                $excal_data[$key]['shipping'] = '';
                            }
                            $excal_data[$key]['shipping'] .= 'shipping_method='.$v."\r\n";
                            break;
                        case 'has_warranty':
                            if(!isset($excal_data[$key]['warranty'])){
                                $excal_data[$key]['warranty'] = '';
                            }
                            $excal_data[$key]['warranty'] .= 'has_warranty='.$v."\r\n";
                            break;
                        case 'warranty':
                            if(!isset($excal_data[$key]['warranty'])){
                                $excal_data[$key]['warranty'] = '';
                            }
                            $excal_data[$key]['warranty'] .= 'warranty='.$v."\r\n";
                            break;
                    }
                }
            }
        }
        $new_excal_data = array();
        foreach ($excal_data as $key => $value){
            foreach ($value as $k => $v){
                $new_excal_data[$key][] = $v;
            }
        }
        $this->actionGetscoreExcals($key_value,$new_excal_data,$title);
    }

    public function actionGetscoreExcal($key_value,$data,$title){
        ini_set("memory_limit", "2048M");
        set_time_limit(0);
        $keys_value = explode(",",$key_value);
        $abc = range('A','Z');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');
        foreach ($keys_value as $key => $value){
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue($abc[$key].'1',$value);
        }
        $n = 2;
        foreach ($data as $key => $value){
            foreach ($value as $ke => $val){
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue($abc[$ke].($n) ,$val);
            }
            $n++;
        }
        $spreadsheet->getActiveSheet()->setTitle($title);
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'(' . date('Ymd-His') . ').xls"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        unset($data);
    }
}
