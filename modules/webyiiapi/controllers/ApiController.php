<?php
namespace app\modules\webyiiapi\controllers;

use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\CompositeAuth;
use yii\filters\Cors;

class ApiController extends ActiveController {
    public $modelClass = 'app\models\AdminUser';
    public $exclude_route= ["webyiiapi/token/get-token"];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
//        $behaviors['authenticator'] = [
//            'class' => CompositeAuth::className(),
//            'authMethods' => [
//                QueryParamAuth::className(),
//            ],
//            // 写在optional里的方法不需要token验证
//            'optional' => [
//                'login'
//            ],
//        ];
        // 这个是跨域配置
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                // restrict access to
                'Access-Control-Request-Method' => ["*"],
                // Allow only POST and PUT methods
                'Access-Control-Request-Headers' => ['*'],
                // Allow only headers 'X-Wsse'
                'Access-Control-Allow-Credentials' => false,
                // Allow OPTIONS caching
                'Access-Control-Max-Age' => 3600,
                // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                'Access-Control-Expose-Headers' => ['Content-Type, Content-Length, Authorization, Accept, X-Requested-With'],
            ],
        ];
        # 定义返回格式是：JSON
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function beforeAction($action)
    {
        if (!in_array(Yii::$app->controller->route,$this->exclude_route)) {
            $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
            $token=$content["token"];
            if (empty($token)) {
                header("Content-type:application/json;charset=utf-8");
                die(json_encode(["code"=>-1,"message"=>"param error"]));
            }
        }
        return parent::beforeAction($action);
    }

}
