<?php
namespace app\modules\webyiiapi\controllers;

use app\components\helper\AuthcodeHelper;
use app\modules\manager\models\AdminUser;
use Yii;
use Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use Maropost\Api\Journeys;
use GuzzleHttp\Client as GuzzleClient;
use app\models\PgExchangeData;
use app\models\CoreConfigData;


class AgentController extends BaseController {
    //帮人加入购物车
    public function actionAddByQuoteId(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $code = $objectManager->get('\Pg\CheckoutExtend\Controller\Cart\Add')->help_other($content,'service');
        if($code['code']==0){
            die(json_encode(["code"=>1,"message"=>"add to cart failed.","data"=>array()]));
        }else{
            die(json_encode(["code"=>1,"message"=>$code['msg'],"data"=>array()]));
        }
    }
    //帮人新建验光单
    public function actionAddByPerscriptionId(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $sql = '';
            if(isset($_COOKIE['help_status'],$_COOKIE['need_help_customer_id'],$_COOKIE['need_help_profile_id']) && $_COOKIE['help_status'] == 1 && ($_COOKIE['need_help_customer_id'] != $_customerSession->getCustomerId())){
                $sql = "insert into audit_prescription (prescription_id,new_profile_id,new_customer_id,old_customer_id,service_type)
 VALUES ('{$content['prescription_id']}','{$_COOKIE['need_help_profile_id']}','{$_COOKIE['need_help_customer_id']}','{$_customerSession->getCustomerId()}','{$content['service_type']}')";
            }
            if($sql){
                Yii::$app->db->createCommand($sql)->execute();
                die(json_encode(array("code"=>0,"msg"=>"success","data"=>array())));
            }else{
                die(json_encode(array("code"=>-1,"msg"=>"error","data"=>array())));
            }
        } catch (\Exception $e) {
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //根据
    public function actionGetCustomerName(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $redis = $this->actionForRedis();
        return $redis;
        try{
            $email = $content['email'];
            $sql = "select firstname,lastname,entity_id from customer_entity where email = '{$email}'";
            $customer_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $sql = "SELECT p.profile_id, nickname FROM customer_entity AS ce INNER JOIN `profile` AS p ON ce.entity_id = p.customer_id WHERE email = '{$email}'";
            $profile_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            if($customer_data){
                die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array("customer"=>$customer_data,"profile"=>$profile_data))));
            }
            die(json_encode(array("code"=>-2,"msg"=>"No customer record found with this email address.","data"=>array())));
        } catch (\Exception $e) {
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    public function actionSend($from_customer,$to_customer,$url){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $from_sql = "select firstname,lastname,email from customer_entity where entity_id = $from_customer";
        $from_customer_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($from_sql)->queryOne();
        $to_sql = "select firstname,lastname,email from customer_entity where entity_id = $to_customer";
        $to_customer_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($to_sql)->queryOne();
        $to_customer_data['email'] = 'sunjinchao41baba@qq.com';
        $send_code = $objectManager->get("Magento\Customer\Model\EmailNotification")->sendHelp($from_customer_data,$to_customer_data,$url);
        return $send_code;
    }
    //个人中心页面新增帮助列表
    public function actionKfAgent(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $product_block = $objectManager->get("Pg\CatalogExtend\Block\Product\View\Interceptor");
            $product_sku = $content['product_sku'];
            $content['service_type']='service';
            if($product_sku){
                $url_base = $objectManager->get('\Pg\Learning\Block\Page\Banner')->getBaseUrl();
                $product = $objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->get($product_sku);
                $attribute_set_id = $product->getData('attribute_set_id');
                //获取太阳镜的category_id
                $sunglasses_category_ids = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["sunglasses_category_ids"];
                $entity_id = $product->getData('entity_id');
                $sql = "select parent_id from catalog_product_super_link where product_id = $entity_id";
                $parent_id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['parent_id'];
                $parent = $objectManager->get("\Magento\Catalog\Api\ProductRepositoryInterface")->getById($parent_id);
                $configure_product_sku = $parent->getData('sku');
                $simple_product_color = $product->getData('color');
                if (in_array($attribute_set_id,$sunglasses_category_ids)) {
                    $url = $url_base.'/buy/index/index/product/' . $product_block->getIdBySku($product_block->escapeHtml($product_sku)) . '?product_main=' . $product->getId().'&sku='.$configure_product_sku.'&_super_attribute[93]='.$simple_product_color.'&category='.$attribute_set_id."&is_sunglasses=1&nonrx=1";
                }else{
                    $url = $url_base.'/buy/index/index/product/' . $product_block->getIdBySku($product_block->escapeHtml($product_sku)) . '?product_main=' . $product->getId().'&sku='.$configure_product_sku.'&_super_attribute[93]='.$simple_product_color.'&category='.$attribute_set_id;
                }
                $content['help_sku'] = $url;
                $this->toinsertKf($content);
            }else{
                $this->toinsertKf($content);
            }
            die(json_encode(array("code"=>0,"msg"=>"You've submit the info successfully","data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //    数据添加
    public function toinsertKf($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $val = str_replace('\'','&#x27;',$val);
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into audit_help_cart($keys) values ($vals)";
        $data = Yii::$app->db->createCommand($sql)->execute();
    }
    //    数据修改
    public function toupdateKf($data=[],$id){
        $vals = '';
        foreach ($data as $key => $val){
            $val = str_replace('\'','&#x27;',$val);
            $vals .= '`'.$key.'`=\''.$val.'\',';
        }
        $vals = rtrim($vals,',');
        $sql = "update audit_help_cart set $vals where id = $id";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $sql;
    }
    //个人中心页面新增帮助列表数据修改
    public function actionUpdateKfAgent(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $product_block = $objectManager->get("Pg\CatalogExtend\Block\Product\View\Interceptor");
            $product_sku = $content['product_sku'];
            $id = $content['help_cart_id'];
            unset($content['help_cart_id']);
            if($product_sku){
                $url_base = $objectManager->get('\Pg\Learning\Block\Page\Banner')->getBaseUrl();
                $product = $objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->get($product_sku);
                $url = $url_base.'/buy/index/index/product/' . $product_block->getIdBySku($product_block->escapeHtml($product_sku)) . '?product_main=' . $product->getId() . '&sku=' . $product->getData('sku');
                $content['help_sku'] = $url;
                $this->toupdateKf($content,$id);
            }else{
                $this->toupdateKf($content,$id);
            }
            die(json_encode(array("code"=>0,"msg"=>"You've submit the info successfully","data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //购物车页面新增帮助列表
    public function actionAgent(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer_id = $objectManager->get("Magento\Customer\Model\Session")->getCustomerId();
        $profile_id = $objectManager->get("Magento\Customer\Model\Session")->getProfileId();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $code = '';
        if(!$customer_id || !$profile_id){
            try{
                $service_type = $content['service_type'];
                if($service_type == 'service'){
                    $help_email = $content['help_email'];
                    $prescription_img = $content['prescription_img'];
                    $service_status = $content['service_status'];
                    $prescription_name = $content['prescription_name'];
                    $prescription_name = str_replace('\'','&#x27;',$prescription_name);
                    $help_pd = $content['help_pd'];
                    $help_sku = $content['help_sku'];
                    $product_sku = $content['product_sku'];
                    $description = $content['description']?$content['description']:'';
                    $description = str_replace('\'','&#x27;',$description);
                    $sql = "INSERT INTO audit_help_cart (service_type,help_sku,email,prescription_name,description,prescription_img,is_login,service_status,product_sku,help_pd) VALUES ('service','$help_sku','$help_email','$prescription_name','$description','$prescription_img',0,'$service_status','$product_sku','$help_pd')";
                    Yii::$app->db->createCommand($sql)->execute();
                    if($service_status == 1){
//                    工单
                        $product_name = '';
                        if($product_sku){
                            $sql = "select `name` from catalog_product_flat_1 where sku = '{$product_sku}'";
                            $product_name = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['name'];
                        }
                        $code = $objectManager->get("Magento\Customer\Model\EmailNotification")->sendKfHelpPrescription($help_email,$prescription_name,$prescription_img,$description,$help_sku,$product_sku,0,$product_name);
                        $codeToMe = $objectManager->get("Magento\Customer\Model\EmailNotification")->sendKfHelpPrescriptionToMe($help_email,$prescription_name,$prescription_img,$description,$help_sku,$product_sku,0,$product_name);
                    }
                    die(json_encode(array("code"=>0,"msg"=>"You've submit the info successfully","data"=>array($code,$codeToMe))));
                }
            }catch (\Exception $e){
                die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
            }
        }
        try{
            $service_type = $content['service_type'];
            $help_email = $content['help_email'];
            $prescription_name = $content['prescription_name'];
            $service_status = $content['service_status'];
            $prescription_name = str_replace('\'','&#x27;',$prescription_name);
            $profile_id = isset($content['profile_id'])?$content['profile_id']:$profile_id;
            $description = $content['description']?$content['description']:'';
            $description = str_replace('\'','&#x27;',$description);
            if(!in_array($service_type,array("customer","service"))){
                die(json_encode(array("code"=>-1,"msg"=>"The current service identity is not defined","data"=>array())));
            }
            if($service_type != 'service'){
                $email = $content['email'];
                $sql = "select entity_id from customer_entity where email = '{$email}'";
                $help_customer_id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                if($help_customer_id){
                    $help_customer_id = $help_customer_id['entity_id'];
                    if($help_customer_id != $customer_id){
                        $sql = "INSERT INTO audit_help_cart (new_customer_id,new_customer_profile,service_type,description,old_customer_id,help_sku,email,prescription_name,product_sku) VALUES ('$customer_id','$profile_id','customer','{$description}','$help_customer_id','{$content['help_sku']}','$help_email','{$prescription_name}','{$content['product_sku']}')";
                        Yii::$app->db->createCommand($sql)->execute();
                        $send_code = $this->actionSend($customer_id,$help_customer_id,$content['help_sku'].'&is_other='.$profile_id);
                        if($send_code == 'success'){
                            die(json_encode(array("code"=>0,"msg"=>"You've submit the info successfully","data"=>array())));
                        }
                        die(json_encode(array("code"=>-1,"msg"=>"waiting for upstream","data"=>array())));
                    }
                    die(json_encode(array("code"=>-1,"msg"=>"The requested account is the same as the current account","data"=>array())));
                }
                die(json_encode(array("code"=>-1,"msg"=>"The input mailbox does not exist","data"=>array())));
            }else{
                $sql = "INSERT INTO audit_help_cart (new_customer_id,new_customer_profile,service_type,description,help_sku,prescription_img,email,prescription_name,service_status,product_sku,help_pd) VALUES ('$customer_id','$profile_id','service','{$description}','{$content['help_sku']}','{$content['prescription_img']}','$help_email','{$prescription_name}','{$service_status}','{$content['product_sku']}','{$content['help_pd']}')";
                Yii::$app->db->createCommand($sql)->execute();
                if($service_status == 1){
//                    工单
                    $product_name = '';
                    if($content['product_sku']){
                        $sql = "select `name` from catalog_product_flat_1 where sku = '{$content['product_sku']}'";
                        $product_name = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['name'];
                    }
                    $code = $objectManager->get("Magento\Customer\Model\EmailNotification")->sendKfHelpPrescription($help_email,$prescription_name,$content['prescription_img'],$description,$content['help_sku'],$content['product_sku'],0,$product_name);
                    $codeToMe = $objectManager->get("Magento\Customer\Model\EmailNotification")->sendKfHelpPrescriptionToMe($help_email,$prescription_name,$content['prescription_img'],$description,$content['help_sku'],$content['product_sku'],0,$product_name);
                }
                die(json_encode(array("code"=>0,"msg"=>"You've submit the info successfully","data"=>array($code,$codeToMe))));
            }
        } catch (\Exception $e) {
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    public function actionFile(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dr=$objectManager->get("\Magento\Framework\Filesystem\DirectoryList");
        $media_path=$dr->getPath("media");
        if($_FILES){
            // 允许上传的图片后缀
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            $temp = explode(".", $_FILES["file"]["name"]);
            $extension = end($temp);        // 获取文件后缀名
            if ((($_FILES["file"]["type"] == "image/gif")
                    || ($_FILES["file"]["type"] == "image/jpeg")
                    || ($_FILES["file"]["type"] == "image/jpg")
                    || ($_FILES["file"]["type"] == "image/pjpeg")
                    || ($_FILES["file"]["type"] == "image/x-png")
                    || ($_FILES["file"]["type"] == "image/png"))
                && in_array(strtolower($extension), $allowedExts))
            {
                if($_FILES["file"]["size"] > 20971520){
                    die(json_encode(array("code"=>-2,"msg"=>"The selected file size is too large to be uploaded.","data"=>array())));
                }    // 小于 200 kb)
                if ($_FILES["file"]["error"] > 0)
                {
                    die(json_encode(array("code"=>-1,"msg"=>"错误：: " . $_FILES["file"]["error"],"data"=>array())));
                }
                else
                {
                    $lujingdetail = 'customer/prescriptions';
                    $file_name = '/agent/prescription/'.date("y",time()).'/'.date("m",time()).'/'.date("d",time());
                    $lujing=$media_path.'/'.$lujingdetail.$file_name;
                    $dir = iconv("UTF-8", "GBK", $lujing);
                    if (!file_exists($dir)){
                        mkdir ($dir,0777,true);
                    }
                    $file_detail = rand(1,100000).'.'.$extension;
                    move_uploaded_file($_FILES["file"]["tmp_name"], $lujing .'/' . $file_detail);
                    $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
                    $secureURL = $_scopeConfig->getValue('web/secure/base_media_url',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
                    die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array("prescription_img"=>$file_name.'/'.$file_detail))));
                }
            }else{
                die(json_encode(array("code"=>-1,"msg"=>'Illegal file format',"data"=>array())));
            }
        }else{
            die(json_encode(array("code"=>-1,"msg"=>'Illegal file format',"data"=>array())));
        }
    }
    public function actionGetHelpCustomerByProfile(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $profile_id = $content['profile_id'];
            $sql = "select * from audit_help_cart where new_customer_profile = $profile_id";
            $audit_help_cart_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>$audit_help_cart_data)));
        }catch (\Exception $e) {
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    public function actionGetHelpCustomer(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer_id = $objectManager->get("Magento\Customer\Model\Session")->getCustomerId();
        try{
            $sql = "select * from audit_help_cart where new_customer_id = $customer_id";
            $audit_help_cart_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>$audit_help_cart_data)));
        }catch (\Exception $e) {
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //maropost 发送邮件
    public function actionSendEmail($email=null,$url=null){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        //邮件配置
        $ACCOUNT_ID=2160;
        $AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
        $REWARD_MEMBERS_LIST_ID=3;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dr=$objectManager->get("Magento\Framework\Filesystem\DirectoryList");
        $document_root=$dr->getRoot();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            if(isset($content['email']) && isset($content['url'])){
                $email = $content['email'];
                $url = $content['url'];
            }
            if(!$email || !$url){
                die(json_encode(array("code"=>-1,"msg"=>'Data is empty',"data"=>array())));
            }
            $sql = "select firstname,lastname from customer_entity where email = '15210066137@163.com'";
            $customer_entity_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $firstname = $customer_entity_data['firstname'];
            $lastname = $customer_entity_data['lastname'];
            $data = array("purchase_assistance_link"=>$url);
            include_once $document_root."/lib/internal/Pg/Maropost/tests/autoloader.php";
            $contacts=new Contacts($ACCOUNT_ID, $AUTHTOKEN);
            $contacts->createOrUpdateForList($REWARD_MEMBERS_LIST_ID,$email,$firstname,$lastname,null,null,null,$data);
            $client = new GuzzleClient();
            $url=sprintf("https://api.maropost.com/accounts/2160/journeys/18/trigger/8181040588408.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$email);
            $res = $client->request('POST', $url);
            $err_no=$res->getBody()->getContents();
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>$err_no)));
        }catch (\Exception $e) {
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //redis防止恶意刷新
    public function actionForRedis(){
        $ip = $this->getIP();
        $limit = 5; //访问限制次数
        $limit_time = 30;   //访问限制时间
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["cache"];
        $server=$cfg["frontend"]["default"]["backend_options"]["server"];
        $port=$cfg["frontend"]["default"]["backend_options"]["port"];
        $database=$cfg["frontend"]["default"]["backend_options"]["database"];
        $redis=new \Credis_Client($server, $port, 30,false, $database, null);
        $list = $redis->lrange($ip, 0, -1);
        $count = count($list);
        if(!$count){
            $redis->rpush($ip, time());
            $redis ->expire($ip,10);
        }else{
            if($count<$limit){
                $redis->rpush($ip, time());
            }else{
                $list = $redis->lrange($ip, 0, 0);
                $time = array_values($list)[0];
                if($time+$limit_time > time()){
                    die(json_encode(array("code"=>-1,"msg"=>'error',"data"=>array())));
                }else{
                    $redis->lpop($ip);
                    $redis->rpush($ip, time());
                }
            }
        }
        $list = $redis->lrange($ip, 0, 0);
        die(json_encode(array("code"=>0,"msg"=>$count,"data"=>array())));
    }
    //获取用户的ip地址
    public function getIP()
    {
        global $ip;
        if (getenv("HTTP_CLIENT_IP"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if(getenv("HTTP_X_FORWARDED_FOR"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if(getenv("REMOTE_ADDR"))
            $ip = getenv("REMOTE_ADDR");
        else
            $ip = "Unknow";
        return $ip;
    }
    //用户修改请求帮助他人的状态
    public function actionUpdateHelpStatus(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $old_customer_id = $content['old_customer_id'];
            $new_customer_id = $content['new_customer_id'];
            $new_customer_profile = $content['new_customer_profile'];
            $status = $content['status'];
            if($status == 1){
                $status = 0;
            }else{
                $status = 1;
            }
            $sql = "update audit_help_cart SET `status` = $status where old_customer_id = $old_customer_id and new_customer_id = $new_customer_id and new_customer_profile = $new_customer_profile";
            Yii::$app->db->createCommand($sql)->execute();
            die(json_encode(array("code"=>1,"msg"=>'success',"data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>0,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //高并发设置，5秒内不能同时访问
    public function actionPreventingHighConcurrency($key){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["cache"];
        $server=$cfg["frontend"]["default"]["backend_options"]["server"];
        $port=$cfg["frontend"]["default"]["backend_options"]["port"];
        $database=$cfg["frontend"]["default"]["backend_options"]["database"];
        $redis=new \Credis_Client($server, $port, 30,false, $database, null);
        if($redis->get($key)){
            return false;
        }else{
            $redis->set($key,1);
            $redis->expire($key,5);
            return true;
        }
    }
    //客服帮助创建验光单
    public function actionAuditPrescription(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $prescription = $content;
            $help_cart_id = '';
            foreach ($prescription as $key => $value){
                switch ($key){
                    case 'exam_date': $value = $value?$value:date("y-m-d h:i:s",time()); $prescription[$key] = date('Y-m-d H:i:s',strtotime($value));break;
                    case 'rpd': $prescription[$key] = $value?$value:0;break;
                    case 'lpd': $prescription[$key] = $value?$value:0;break;
                    case 'help_cart_id': $help_cart_id = $value;
                }
            }
            //验证当前请求是否合法
            if($help_cart_id){
                $code = $this->actionPreventingHighConcurrency('make_prescription'.$help_cart_id);
                if($code){
                    $sql = "select count(0) as count from audit_prescription where help_cart_id = $help_cart_id";
                    $audit_prescription_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                    if($audit_prescription_data['count']>0){
                        die(json_encode(array("code"=>-1,"msg"=>"To help this user create an optometry list","data"=>array())));
                    }
                }else{
                    die(json_encode(array("code"=>-1,"msg"=>"Too many simultaneous visits","data"=>array())));
                }
            }
            if(isset($prescription['rpd'],$prescription['lpd'])){
                if($prescription['rpd'] && $prescription['lpd']){
                    unset($prescription['pd']);
                }
            }
            if(isset($prescription['expire_date']) && !(empty($prescription['expire_date']))){
                $prescription['expire_date'] = date("Y-m-d h:i:s",strtotime($prescription['expire_date']));
            }else{
                $prescription['expire_date'] = date("Y-m-d h:i:s",strtotime('+24 month',strtotime($prescription['exam_date'])));
            }
            unset($prescription['prism_more_check']);
            unset($prescription['prism']);
            unset($prescription['help_cart_id']);
            $sql = "select new_customer_id,new_customer_profile,prescription_img,service_status from audit_help_cart where id = $help_cart_id";
            $customer_entity_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            if(isset($prescription['prescription_img'])){
                $sql = "update audit_help_cart set prescription_img = '{$prescription['prescription_img']}' where id = $help_cart_id";
                Yii::$app->db->createCommand($sql)->execute();
            }else{
                $prescription['prescription_img'] = $customer_entity_data['prescription_img'];
            }
            $prescription['prescription_name'] = str_replace('\'','&#x27;',$prescription['prescription_name']);
            $id = $this->toinsert($prescription);
            $data = array();
            $data['new_customer_id'] = $customer_entity_data['new_customer_id'];
            $data['new_profile_id'] = $customer_entity_data['new_customer_profile'];
            $data['help_cart_id'] = $help_cart_id;
            $data['prescription_id'] = $id['id'];
            $data['service_type'] = 'service';
            $data['status'] = 1;
            $data = $this->toinsertAuditprescription($data);
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    public function actionUpdateAuditPrescription(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $prescription = $content;
            $entity_id = '';
            foreach ($prescription as $key => $value){
                switch ($key){
                    case 'exam_date': $value = $value?$value:date("y-m-d h:i:s",time()); $prescription[$key] = date('Y-m-d H:i:s',strtotime($value));break;
                    case 'rpd': $prescription[$key] = $value?$value:0;break;
                    case 'lpd': $prescription[$key] = $value?$value:0;break;
                    case 'entity_id': $entity_id = $value;
                }
            }
            if(isset($prescription['rpd'],$prescription['lpd'])){
                if($prescription['rpd'] && $prescription['lpd']){
                    unset($prescription['pd']);
                }
            }
            if(isset($prescription['expire_date']) && !(empty($prescription['expire_date']))){
                $prescription['expire_date'] = date("Y-m-d h:i:s",strtotime($prescription['expire_date']));
            }else{
                $prescription['expire_date'] = date("Y-m-d h:i:s",strtotime('+24 month',strtotime($prescription['exam_date'])));
            }
            unset($prescription['prism_more_check']);
            unset($prescription['prism']);
            unset($prescription['entity_id']);
            $sql = "select help_cart_id from audit_prescription where prescription_id = $entity_id";
            $help_cart_id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $sql = "select new_customer_id,new_customer_profile,prescription_img,service_status from audit_help_cart where id ='{$help_cart_id['help_cart_id']}'";
            $customer_entity_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            if(isset($prescription['prescription_img'])){
                $sql = "update audit_help_cart set prescription_img = '{$prescription['prescription_img']}' where id = '{$help_cart_id['help_cart_id']}'";
                Yii::$app->db->createCommand($sql)->execute();
            }else{
                $prescription['prescription_img'] = $customer_entity_data['prescription_img'];
            }
            $prescription['prescription_name'] = str_replace('\'','&#x27;',$prescription['prescription_name']);
            $this->toupdate($prescription,$entity_id);
            $sql = "update audit_prescription SET `status` = 1 where prescription_id = $entity_id";
            $data = Yii::$app->db->createCommand($sql)->execute();
            $sql = "UPDATE audit_additional_data set is_active = 0 where help_cart_id = {$help_cart_id['help_cart_id']}";
            Yii::$app->db->createCommand($sql)->execute();
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //    数据修改
    public function toupdate($data=[],$entity_id){
        $vals = '';
        foreach ($data as $key => $val){
            $val = str_replace('\'','&#x27;',$val);
            $vals .= '`'.$key.'`=\''.$val.'\',';
        }
        $vals = rtrim($vals,',');
        $sql = "update audit_prescription_entity set $vals where entity_id = $entity_id";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $sql;
    }
    //    数据添加
    public function toinsert($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $val = str_replace('\'','&#x27;',$val);
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into audit_prescription_entity($keys) values ($vals)";
        $data = Yii::$app->db->createCommand($sql)->execute();
        $sql = "select last_insert_id() as id from audit_prescription_entity";
        $id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $id;
    }
    //    数据添加
    public function toinsertAuditprescription($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $val = str_replace('\'','&#x27;',$val);
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into audit_prescription($keys) values ($vals)";
        $data = Yii::$app->db->createCommand($sql)->execute();
        return $data;
    }
    //客服获取所需帮助名单
    public function actionHelp(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $sql = "select * from audit_help_cart where service_type = 'service'";
        $row=$connection->fetchAll($sql);
        return $row;
    }
    //获取验光单数据
    public function actionGetPrescription(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        try{
            $help_cart_id = json_decode(Yii::$app->getRequest()->getRawBody(),true)['id'];
            $sql = "select * from audit_prescription where help_cart_id = $help_cart_id";
            $row=$connection->fetchAll($sql);
            if($row){
                $prescription = $row[0]['prescription_id'];
                $sql = "select * from audit_prescription_entity where entity_id = $prescription";
                $row=$connection->fetchAll($sql);
                die(json_encode(array("code"=>0,"msg"=>'success',"data"=>$row)));
            }else{
                die(json_encode(array("code"=>-1,"msg"=>'Please go and create an optometry list',"data"=>array())));
            }
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //验光单确认按钮
    public function actionAgreePrescription(){
        try{
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
            $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
            $id = $content['id'];
            $sql = "update audit_prescription set status = 2,acknowledging_time='".date("Y-m-d H:i:s",time())."' where help_cart_id = $id";
            $data = Yii::$app->db->createCommand($sql)->execute();
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //确认发送邮件接口
    public function actionAgreeSendEmailDetail(){
        try{
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
            $help_cart_id = $content['help_cart_id'];
            $sql = "select created_at from audit_send_email where help_cart_id = '$help_cart_id' ORDER BY created_at desc";
            $send_email_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $sql = "select prescription_name,product_sku,help_sku from audit_help_cart where id = '$help_cart_id'";
            $email_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            if($email_data){
                $email_data = $email_data[0];
                if($email_data['product_sku']){
                    $sql = "select `name` from catalog_product_flat_1 where sku = '{$email_data['product_sku']}'";
                    $email_data['product_sku_name'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['name'];
                }
                if($email_data['help_sku']){
                    $email_data['help_sku'] = str_replace("#",'',$email_data['help_sku']);
                    $email_data['help_sku'] = $email_data['help_sku']."&help_cart_id=".urlencode($this->authcode($help_cart_id,'ENCODE'));
                }else{
                    $url_base = $objectManager->get('\Pg\Learning\Block\Page\Banner')->getBaseUrl();
                    $email_data['help_sku'] = $url_base."/customer/account/auditPrescription?id=".urlencode($this->authcode($help_cart_id,'ENCODE'));
                }
                die(json_encode(array("code"=>0,"msg"=>'success',"data"=>array("send_email_detail"=>$send_email_data,"email_detail"=>$email_data))));
            }
            die(json_encode(array("code"=>-2,"msg"=>'The current data is empty',"data"=>array("send_email_detail"=>$send_email_data,""))));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //确认发送邮件接口
    public function actionAgreeSendEmail(){
        try{
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
            $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
            $help_cart_id = $content['help_cart_id'];
            $title = $content['title'];
            $content = $content['content'];
            $sql = "select prescription_name,email,help_sku,product_sku from audit_help_cart where id = $help_cart_id";
            $help_cart_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $prescription_name = $help_cart_data['prescription_name'];
            $product_sku = $help_cart_data['product_sku'];
            $sql = "select `name` from catalog_product_flat_1 where sku = '{$product_sku}'";
            $name = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['name'];
            $email = $help_cart_data['email'];
            if($help_cart_data['help_sku']){
                $help_cart_data['help_sku'] = str_replace("#",'',$help_cart_data['help_sku']);
                $url = $help_cart_data['help_sku']."&help_cart_id=".urlencode($this->authcode($help_cart_id,'ENCODE'));
            }else{
                $url_base = $objectManager->get('\Pg\Learning\Block\Page\Banner')->getBaseUrl();
                $url = $url_base."/customer/account/auditPrescription?id=".urlencode($this->authcode($help_cart_id,'ENCODE'));
            }
            $send_code = $objectManager->get("Magento\Customer\Model\EmailNotification")->sendHelpPrescription($email,$title,$content,$url,$name);
            if($send_code == 'success'){
                $sql = "update audit_prescription set status = 3 where help_cart_id = $help_cart_id";
                Yii::$app->db->createCommand($sql)->execute();
                $sql = "INSERT INTO audit_send_email (email,help_cart_id) VALUES ('$email',$help_cart_id)";
                Yii::$app->db->createCommand($sql)->execute();
                die(json_encode(array("code"=>0,"msg"=>"Email has been sent to {$email} successfully!","data"=>array())));
            }
            die(json_encode(array("code"=>-1,"msg"=>'Email failed to send',"data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //加密接口
    function authcode($string, $operation = 'DECODE', $expiry = 0) {
        $key = 'www.payneglasses.com';
        $ckey_length = 4;
        $key = md5($key ? $key : '');
        $keya = md5(substr($key, 0, 16));
        $keyb = md5(substr($key, 16, 16));
        $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length):
            substr(md5(microtime()), -$ckey_length)) : '';
        $cryptkey = $keya.md5($keya.$keyc);
        $key_length = strlen($cryptkey);
        $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) :
            sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
        $string_length = strlen($string);
        $result = '';
        $box = range(0, 255);
        $rndkey = array();
        for($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }
        for($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        if($operation == 'DECODE') {
            if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) &&
                substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            return $keyc.str_replace('=', '', base64_encode($result));
        }
    }
    //用户从邮件跳转解密id,登录新建验光单，未登录不处理，返回数据
    public function actionGetProfilePrescriptionData(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $help_card_id = $this->authcode(urldecode($content['help_cart_id']));
            $data = array();
            $sql = "SELECT * FROM audit_help_cart WHERE id = $help_card_id";
            $audit_help_cart_data=$connection->fetchAll($sql)[0];
            $sql = "select * from audit_prescription where help_cart_id = $help_card_id";
            $audit_prescription_data=$connection->fetchAll($sql)[0];
            $prescription_id = $audit_prescription_data['prescription_id'];
            $sql = "select * from audit_prescription_entity where entity_id = $prescription_id";
            $audit_prescription_entity_data=$connection->fetchAll($sql)[0];
            $data['audit_help_cart_data'] = $audit_help_cart_data;
            $data['audit_prescription_data'] = $audit_prescription_data;
            $data['audit_prescription_entity_data'] = $audit_prescription_entity_data;

            $session = $objectManager->get('\Magento\Customer\Model\Session');
            $login = $session->isLoggedIn();
            $customer_id = $session->getCustomerId();
            $profile_id = $session->getProfileId();
            if(!empty($session) && $session->isLoggedIn() && !empty($customer_id)){
                $add_data = $this->addPrescriptionLogin($audit_prescription_entity_data);
            }
            return array("code"=>0,"msg"=>'',"data"=>$data);
        }catch (\Exception $e){
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    //用户登录创建用户验光单
    public function addPrescriptionLogin($prescription){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $connection->beginTransaction();
        try{
            $session = $objectManager->get('\Magento\Customer\Model\Session');
            $profile_id = $session->getProfileId();
            $sql = "update prescription_entity SET is_default = 0 where parent_id = $profile_id";
            $connection->query($sql);
            $prescription['is_default'] = 1;
            $prescription['parent_id'] = $profile_id;
            $prescription['expire_date'] = $prescription['exam_date'];
            unset($prescription['entity_id']);
            $prescription['expire_date'] = $prescription['exam_date'];
            $sql = $this->toinsertLogin($prescription);
            $connection->query($sql);
            $connection->commit();
            return array("code"=>0,"msg"=>'success',"data"=>array());
        }catch (\Exception $e){
            $connection->rollBack();
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    //    数据添加
    public function toinsertLogin($data=[]){
        $keys = '';
        $vals = '';
        foreach ($data as $key => $val){
            $val = str_replace('\'','&#x27;',$val);
            $keys .= '`'.$key.'`,';
            $vals .= '\''.$val.'\',';
        }
        $keys = rtrim($keys,',');
        $vals = rtrim($vals,',');
        $sql = "insert into prescription_entity($keys) values ($vals)";
        return $sql;
    }
    //登录状态验证用户验光单是否是客服添加的验光单并进行过修改
    public function actionVerified(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $new_prescription = $content['new_prescription'];
            $entity_id = $new_prescription['entity_id'];
            $sql = "select verified from prescription_entity where entity_id = $entity_id";
            $verified = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['verified'];
            if($verified){
                $old_prescription = $content['old_prescription'];
                $new_prescription_detail = array();
                $old_prescription_detail = array();
                $need_key = array("rsph","rcyl","rax","radd","lsph","lcyl","lax","ladd","rpri","rbase","lpri","lbase","rpri_1","rbase_1","lpri_1","lbase_1","pd","rpd","lpd","single_pd");
                foreach ($new_prescription as $key => $value){
                    if(in_array($key,$need_key)){
                        $new_prescription_detail[$key] = sprintf('%.2f',$value);
                    }
                }
                foreach ($old_prescription as $key => $value){
                    if(in_array($key,$need_key)){
                        $old_prescription_detail[$key] = sprintf('%.2f',$value);
                    }
                }
                $result=array_diff_assoc($old_prescription_detail,$new_prescription_detail);
                $diff = 0;
                if($result){
                    $diff = 1;
                }else{
                    if($new_prescription['prescription_name'] != $old_prescription['prescription_name']){
                        $diff = 1;
                    }
                }
            }else{
                $diff = 1;
            }
            return array("code"=>0,"msg"=>"success","data"=>array("diff"=>$diff));
        }catch (\Exception $e){
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    //未登录状态验证用户验光单是否是客服添加的验光单并进行过修改
    public function actionVerifiedNoLogin(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $new_prescription = $content['new_prescription'];
            $old_prescription = $content['old_prescription'];
            $new_prescription_detail = array();
            $old_prescription_detail = array();
            $need_key = array("rsph","rcyl","rax","radd","lsph","lcyl","lax","ladd","rpri","rbase","lpri","lbase","rpri_1","rbase_1","lpri_1","lbase_1","pd","rpd","lpd","single_pd");
            foreach ($new_prescription as $key => $value){
                if(in_array($key,$need_key)){
                    $new_prescription_detail[$key] = sprintf('%.2f',$value);
                }
            }
            foreach ($old_prescription as $key => $value){
                if(in_array($key,$need_key)){
                    $old_prescription_detail[$key] = sprintf('%.2f',$value);
                }
            }
            $result=array_diff_assoc($old_prescription_detail,$new_prescription_detail);
            $diff = 0;
            if($result){
                $diff = 1;
            }else{
                if($new_prescription['prescription_name'] != $old_prescription['prescription_name']){
                    $diff = 1;
                }
            }
            return array("code"=>0,"msg"=>"success","data"=>array("diff"=>$diff));
        }catch (\Exception $e){
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    //yii个人中心页面数据获取 start
    public function actionGethelpdata($page = 1){
        $num = 25;
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $page = $content['page'];
            $row=$this->getAuditData($page);
            if($row['code'] === -1){
                return array("code"=>-1,"msg"=>$row['msg'],"data"=>array());
            }
            $row = $row['data'];
            $count = $this->getAuditCount();
            if($count['code'] === -1){
                return array("code"=>-1,"msg"=>$row['msg'],"data"=>array());
            }
            $count = $count['data'];
            $count_page = ceil($count/$num);
            if($row){
                $id = '';
                foreach ($row as $key => $value){
                    $id .= '\''.$value['id'].'\',';
                }
                $id = rtrim($id,',');
                $sql = "select help_cart_id,count(0) as send_count from audit_send_email where help_cart_id in ($id) GROUP BY help_cart_id";
                $send_email_data=$connection->fetchAll($sql);
                foreach ($row as $key => $value){
                    $row[$key]['send_count'] = 0;
                    foreach ($send_email_data as $k => $v){
                        if($value['id'] == $v['help_cart_id']){
                            $row[$key]['send_count'] = $v['send_count'];
                        }
                    }
                }
            }
            return array("code"=>0,"msg"=>"success","data"=>$row,"count_page"=>$count_page);
        }catch (\Exception $e){
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    public function getAuditData($page){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $num = 25;
            $page = ($page-1) * $num;
            $sql = $this->getAuditSql();
            $help_email = isset($content['help_email'])?$content['help_email']:'';
            $help_status = isset($content['help_status'])?$content['help_status']:'';
            //排序
            $orderby = isset($content['orderby'])?$content['orderby']:'';
            $sort = isset($content['sort'])?$content['sort']:'';
            $help_pd = isset($content['help_pd'])?$content['help_pd']:'';
            $pigeonhole = isset($content['pigeonhole'])?$content['pigeonhole']:'';
            $help_group = isset($content['help_group'])?$content['help_group']:'';
            $help_name = isset($content['help_name'])?$content['help_name']:'';
            $help_name = str_replace('\'','&#x27;',$help_name);
            $help_start_time = isset($content['help_start_time'])?$content['help_start_time']:'';
            $help_start_time_to = '';
            if($help_start_time){
                switch ($help_start_time){
                    case 1:$help_start_time_to = date("Y-m-d",strtotime('-1 month',time()));break;
                    case 2:$help_start_time_to = date("Y-m-d",strtotime('-3 month',time()));break;
                    case 3:$help_start_time_to = date("Y-m-d",strtotime('-6 month',time()));break;
                    case 4:$help_start_time_to = date("Y-m-d",strtotime('-12 month',time()));break;
                }
            }
            $ahc_createdstart_at = isset($content['ahc_createdstart_at'])?$content['ahc_createdstart_at']:'';
            if($ahc_createdstart_at){
                $ahc_createdstart_at = str_replace("T"," ","$ahc_createdstart_at");
                $ahc_createdstart_at .= ':00';
            }
            $ahc_createdend_at = isset($content['ahc_createdend_at'])?$content['ahc_createdend_at']:'';
            if($ahc_createdend_at){
                $ahc_createdend_at = str_replace("T"," ","$ahc_createdend_at");
                $ahc_createdend_at .= ':00';
            }
            if(!empty($help_email)){
                $sql .= " and ahc.email like '%{$help_email}%'";
            }
            if(!empty($help_status)){
                if(substr_count($help_status, '0')){
                    $sql .= " and (ap.`status` in ($help_status) or ap.`status` is NULL)";
                }else{
                    $sql .= " and ap.`status` in ({$help_status})";
                }
            }
            if(!empty($help_name)){
                $sql .= " and ahc.prescription_name like '%{$help_name}%'";
            }
            if(!empty($help_start_time_to)){
                $sql .= " and ahc.created_at >= '{$help_start_time_to}'";
            }
            if(!empty($ahc_createdstart_at)){
                $sql .= " and ahc.created_at >= '{$ahc_createdstart_at}'";
            }
            if(!empty($ahc_createdend_at)){
                $sql .= " and ahc.created_at <= '{$ahc_createdend_at}'";
            }
            if($help_status === '0'){
                $sql .= " and ap.`status` is NULL";
            }
            if(!empty($help_pd)){
                $sql .= " and ahc.help_pd = '{$help_pd}'";
            }
            if(!empty($pigeonhole)){
                $sql .= " and ahc.pigeonhole in ({$pigeonhole})";
            }
            if($pigeonhole === '0'){
                $sql .= " and ahc.pigeonhole = 0";
            }
            $order_sql = "";
            if(!empty($orderby) && !empty($sort)){
                switch ($orderby){
                    case "prescription_status":$order_sql .= "IFNULL(ap.`status`,0) $sort ";break;
                    case "help_pd":$order_sql .= "IFNULL(ahc.`help_pd`,0) $sort";break;
                    case "ahc_created_at":$order_sql .= "IFNULL(ahc.created_at,0) $sort";break;
                    case "ape_created_at":$order_sql .= "IFNULL(ape.created_at,0) $sort";break;
                    case "acknowledging_time":$order_sql .= "IFNULL(ap.acknowledging_time,0) $sort";break;
                    case "send_created_time":$order_sql .= "IFNULL(asec.created_at,0) $sort";break;
                }
                $order_sql .= ',ahc.id DESC';
            }else{
                $order_sql = ' ahc.id desc';
            }
            $sql .= " ORDER BY $order_sql";
            //分组
            if(!empty($help_group) && $help_group){
                //分组
                $sql = "SELECT count(0) AS data_count, detail.* FROM ( $sql ) AS `detail` GROUP BY detail.email";
            }

            $sql .= " limit $page,$num";
            $row=$connection->fetchAll($sql);
            return array("code"=>0,"msg"=>'success',"data"=>$row);
        }catch (\Exception $e){
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    public function getAuditCount(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $sql = $this->getAuditSql();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $help_email = isset($content['help_email'])?$content['help_email']:'';
            $help_status = isset($content['help_status'])?$content['help_status']:'';
            $help_pd = isset($content['help_pd'])?$content['help_pd']:'';
            $pigeonhole = isset($content['pigeonhole'])?$content['pigeonhole']:'';
            $help_group = isset($content['help_group'])?$content['help_group']:'';
            $help_name = isset($content['help_name'])?$content['help_name']:'';
            $help_name = str_replace('\'','&#x27;',$help_name);
            $help_start_time = isset($content['help_start_time'])?$content['help_start_time']:'';
            $help_start_time_to = '';
            if($help_start_time){
                switch ($help_start_time){
                    case 1:$help_start_time_to = date("Y-m-d",strtotime('-1 month',time()));break;
                    case 2:$help_start_time_to = date("Y-m-d",strtotime('-3 month',time()));break;
                    case 3:$help_start_time_to = date("Y-m-d",strtotime('-6 month',time()));break;
                    case 4:$help_start_time_to = date("Y-m-d",strtotime('-12 month',time()));break;
                }
            }
            $ahc_createdstart_at = isset($content['ahc_createdstart_at'])?$content['ahc_createdstart_at']:'';
            if($ahc_createdstart_at){
                $ahc_createdstart_at = str_replace("T"," ","$ahc_createdstart_at");
                $ahc_createdstart_at .= ':00';
            }
            $ahc_createdend_at = isset($content['ahc_createdend_at'])?$content['ahc_createdend_at']:'';
            if($ahc_createdend_at){
                $ahc_createdend_at = str_replace("T"," ","$ahc_createdend_at");
                $ahc_createdend_at .= ':00';
            }
            if(!empty($help_email)){
                $sql .= " and ahc.email like '%{$help_email}%'";
            }
            if(!empty($help_status)){
                if(substr_count($help_status, '0')){
                    $sql .= " and (ap.`status` in ($help_status) or ap.`status` is NULL)";
                }else{
                    $sql .= " and ap.`status` in ({$help_status})";
                }
            }
            if(!empty($help_name)){
                $sql .= " and ahc.prescription_name like '%{$help_name}%'";
            }
            if($help_status === '0'){
                $sql .= " and ap.`status` is NULL";
            }
            if(!empty($help_start_time_to)){
                $sql .= " and ahc.created_at >= '{$help_start_time_to}'";
            }
            if(!empty($ahc_createdstart_at)){
                $sql .= " and ahc.created_at >= '{$ahc_createdstart_at}'";
            }
            if(!empty($ahc_createdend_at)){
                $sql .= " and ahc.created_at <= '{$ahc_createdend_at}'";
            }
            if(!empty($help_pd)){
                $sql .= " and ahc.help_pd = '{$help_pd}'";
            }
            if(!empty($pigeonhole)){
                $sql .= " and ahc.pigeonhole in ({$pigeonhole})";
            }
            if($pigeonhole === '0'){
                $sql .= " and ahc.pigeonhole = 0";
            }
            if(!empty($help_group) && $help_group){
                //分组
                $sql .= " GROUP BY ahc.email ORDER BY ahc.id desc";
            }
            $row=$connection->fetchAll($sql);
            return array("code"=>0,"msg"=>'success',"data"=>count($row));
        }catch (\Exception $e){
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    public function getAuditSql(){
        $sql = "SELECT ahc.id, ahc.pigeonhole, ahc.email, ahc.prescription_name AS help_name, ahc.new_customer_id, ahc.new_customer_profile, ahc.service_type, ahc.old_customer_id, ahc.`status`, ahc.created_at AS ahc_created_at, ahc.prescription_img AS ahc_prescription_img, ahc.description, ahc.help_sku, ahc.is_login, ahc.service_status, ahc.product_sku, ahc.help_pd, ap.prescription_id ,ap.acknowledging_time, ap.`status` AS prescription_status, ape.created_at AS ape_created_at, ape.*,asec.created_at as send_created_time FROM audit_help_cart AS ahc LEFT JOIN audit_prescription AS ap ON ahc.id = ap.help_cart_id LEFT JOIN audit_prescription_entity AS ape ON ap.prescription_id = ape.entity_id LEFT JOIN ( SELECT * FROM ( SELECT * FROM audit_send_email ORDER BY created_at DESC ) AS ase GROUP BY ase.help_cart_id ) as asec on ahc.id = asec.help_cart_id where ahc.service_type = 'service' and ahc.`status` = 1";
        return $sql;
    }
    public function actionGetHelpDetail(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $help_email = $content['email'];
            $help_cart_id = $content['help_cart_id'];
            $sql = $this->getAuditSql();
            $sql .= " and ahc.email = '{$help_email}' and ahc.id not in ('{$help_cart_id}') ORDER BY ahc.id desc";
            $row=$connection->fetchAll($sql);
            return array("code"=>0,"msg"=>'success',"data"=>$row);
        }catch (\Exception $e){
            return array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array());
        }
    }
    //获取购物车详细数据
    public function actionKfCart(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $sql = "select * from audit_additional_data where help_cart_id = {$content['help_cart_id']} and is_active = 1";
            //购买多付修改直接注释下面这行 start
            $sql .= ' order by id desc limit 1';
            //购买多付修改直接注释下面这行 end
            $audit_additional_data_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $additional_data = array();
            foreach ($audit_additional_data_data as $key => $value){
                $data = json_decode($value['additional_data'],true);
                $additional_data[$value['id']]['orderglass'] = $data['orderglass'];
                $additional_data[$value['id']]['product'] = $this->GetProductDetail($data['stock_sku']);
            }
            die(json_encode(array("code"=>0,"msg"=>"success","data"=>$additional_data)));
        }catch(\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //获取产品信息sql查询
    public function GetProductDetail($sku){
        $sql = "SELECT cpf1.sku, cpf1.color_value as color_name, cpf1.image, eaos.`value` AS color_value, cpf1.color, cpf1.price, cpf1.`name`, cpsl.product_id, cpsl.parent_id, cpf1.`entity_id`, ur.request_path AS url FROM catalog_product_flat_1 AS cpf1 INNER JOIN eav_attribute_option_swatch AS eaos ON eaos.option_id = cpf1.color INNER JOIN cataloginventory_stock_item AS csi ON cpf1.entity_id = csi.product_id INNER JOIN catalog_product_super_link AS cpsl ON cpsl.product_id = cpf1.entity_id INNER JOIN url_rewrite AS ur ON ur.entity_id = cpf1.entity_id AND cpf1.retired != 1 WHERE cpf1.sku = '$sku' GROUP BY cpf1.sku";
        $product_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $product_data;
    }

    public function actionUpdateKfCart(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $sql = "UPDATE audit_additional_data set is_active = 0 where id = {$content['additional_data_id']}";
            Yii::$app->db->createCommand($sql)->execute();
            die(json_encode(array("code"=>0,"msg"=>"success","data"=>array())));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }

    }

    //获取产品详细数据
    public function actionGetProductDetail(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $product_data = $this->GetProductDetail($content['sku']);
            die(json_encode(array("code"=>0,"msg"=>"success","data"=>$product_data)));
        }catch(\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }

    public function actionPigeonhole(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $help_cart_id = $content['help_cart_id'];
            $pigeonhole = $content['pigeonhole'];
            $sql = "UPDATE audit_help_cart SET pigeonhole = $pigeonhole where id = $help_cart_id";
            Yii::$app->db->createCommand($sql)->execute();
            die(json_encode(array("code"=>0,"msg"=>"success","data"=>array($help_cart_id,$pigeonhole))));
        }catch(\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //yii个人中心页面数据获取 end
    //暂未调用
    //客服添加购物车数据处理
    public function actionParms(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $data = array();
            $product_sku = $content['product_sku'];
            $product = $objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->get($product_sku);
            $product = $product->getData();
            $sql = "select parent_id from catalog_product_super_link where product_id = {$product['entity_id']}";
            $data['product'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['parent_id'];
            $sql = "select request_path,lens_type,frame_group from url_rewrite where dest_sku = '{$product_sku}'";
            $url_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $url_base = $objectManager->get('\Pg\Learning\Block\Page\Banner')->getBaseUrl();
            $data['sku_url'] = $url_base.$url_data['request_path'];
            $data['stock_sku'] = $product_sku;
            $data['lens_type'] = $url_data['lens_type'];
            $data['frame_group'] = $url_data['frame_group'];
            $data['qty'] = 1;
            $data['super_attribute'] = array("93"=>$product['color']);
            $data['selected_configurable_option'] = '';
            $data['related_product'] = '';
            $data['support_php'] = '';
            $sql = "select * from audit_prescription_entity where entity_id = {$content['prescription_id']}";
            $profile_prescription = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            unset($profile_prescription['entity_id']);
            unset($profile_prescription['created_at']);
            unset($profile_prescription['updated_at']);
            unset($profile_prescription['verified']);
            $data['profile_prescription'] = $profile_prescription;
            $data['orderglass'] = $content['orderglass'];
            $data['additional_data'] = $content['additional_data'];
            $data['prescription'] = $content['prescription'];
            $data = json_encode($data);
            //默认为单个 start
            $sql = "UPDATE audit_additional_data set is_active = 0 where help_cart_id = {$content['help_cart_id']}";
            Yii::$app->db->createCommand($sql)->execute();
            //默认为单个 end
            $sql = "INSERT INTO audit_additional_data (additional_data,help_cart_id,is_active) VALUES ('{$data}',{$content['help_cart_id']},1)";
            Yii::$app->db->createCommand($sql)->execute();
            $help_cart_id = urlencode($this->authcode($content['help_cart_id'],1));
            die(json_encode(array("code"=>0,"msg"=>'success',"data"=>$help_cart_id)));
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }

    public function actionHelpNoLogin(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $code = $objectManager->get('\Pg\CheckoutExtend\Controller\Cart\Add')->help_other($content['parms'],"customer");
        return $code;
    }
    public function actionGetPassword(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerRepository = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
        $customerRegistry = $objectManager->get('\Magento\Customer\Model\CustomerRegistry');
        $createPasswordHash = $objectManager->get('\Magento\Customer\Model\AccountManagement');
        $customer = $customerRepository->get('sjc@qq.com');
        $customerSecure = $customerRegistry->retrieveSecureData($customer->getId());
        $customerSecure->setPasswordHash($createPasswordHash->getPasswordHashs('124qwer'));
        return $customerSecure->getPasswordHash();
    }
    public function actionGetProfile(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try{
            $help_cart_id = $this->authcode(urldecode($content['help_cart_id']));
            $sql = "select * from audit_additional_data where help_cart_id = {$help_cart_id} and is_active = 1";
            //购买多付修改直接注释下面这行 start
            $sql .= ' order by id desc limit 1';
            //购买多付修改直接注释下面这行 end
            $audit_additional_data_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $sql = "select prescription_name from audit_help_cart where id = {$help_cart_id}";
            $audit_help_cart_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            if($_customerSession->isLoggedIn()){
                $profile_Repository = $objectManager->get('\Pg\Profile\Api\ProfileRepositoryInterface');
                $profile_data = $profile_Repository->getById($_customerSession->getProfileId());
                $profile_data = $profile_data->getData();
                $profile = array("id"=>$profile_data['profile_id'],"nickname"=>$profile_data['nickname'],"age_group"=>$profile_data['age_group']);

                //登录后新增验光单
                $sql = "select * from audit_prescription where help_cart_id = $help_cart_id";
                $audit_prescription_data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                $prescription_id = $audit_prescription_data['prescription_id'];
                $sql = "select * from audit_prescription_entity where entity_id = $prescription_id";
                $audit_prescription_entity_data=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                $this->addPrescriptionLogin($audit_prescription_entity_data);

                $sql = "select * from prescription_entity where parent_id = {$profile_data['profile_id']} and is_default = 1";
                $prescription_entity_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                $additional_data = array();
                foreach ($audit_additional_data_data as $key => $value){
                    $additional_data[$value['id']] = json_decode($value['additional_data'],true);
                    $additional_data[$value['id']]['profile'] = $profile;
                    $additional_data[$value['id']]['profile_prescription']['id'] = $prescription_entity_data['entity_id'];
                    $additional_data[$value['id']]['profile_prescription']['parent_id'] = $prescription_entity_data['parent_id'];
                    $additional_data[$value['id']]['profile_prescription']['rpri1'] = $additional_data[$value['id']]['profile_prescription']['rpri_1'];
                    $additional_data[$value['id']]['profile_prescription']['lpri1'] = $additional_data[$value['id']]['profile_prescription']['lpri_1'];
                    $additional_data[$value['id']]['profile_prescription']['rbase1'] = $additional_data[$value['id']]['profile_prescription']['rbase_1'];
                    $additional_data[$value['id']]['profile_prescription']['lbase1'] = $additional_data[$value['id']]['profile_prescription']['lbase_1'];
                    $additional_data[$value['id']]['prescription']['id'] = $prescription_entity_data['entity_id'];
                    $additional_data[$value['id']]['prescription']['parent_id'] = $profile_data['profile_id'];
                    unset($additional_data[$value['id']]['profile_prescription']['rpri_1']);
                    unset($additional_data[$value['id']]['profile_prescription']['lpri_1']);
                    unset($additional_data[$value['id']]['profile_prescription']['rbase_1']);
                    unset($additional_data[$value['id']]['profile_prescription']['lbase_1']);
                    unset($additional_data[$value['id']]['profile_prescription']['prescription_img']);
                    unset($additional_data[$value['id']]['prescription']['prescription_img']);
                }
                $success_data = array();
                foreach ($additional_data as $key => $value){
                    $code = $objectManager->get('\Pg\CheckoutExtend\Controller\Cart\Add')->execute($value);
                    if($code['code']==0){
                        $code = $code['data'];
                    }
                    $success_data[$key] = $code;
                }
                die(json_encode(array("code"=>0,"msg"=>"success","data"=>$success_data)));
            }else{
                $profile = array("nickname"=>$audit_help_cart_data['prescription_name'],"age_group"=>'25',"birthyear"=>'2019',"gender"=>'1',"guest_id"=>'0');
                $profile = $this->actionProfileGuestAdd($profile);
                $additional_data = array();
                foreach ($audit_additional_data_data as $key => $value){
                    $additional_data[$value['id']] = json_decode($value['additional_data'],true);
                    $additional_data[$value['id']]['profile'] = $profile;
                    $additional_data[$value['id']]['profile_prescription']['is_default'] = 1;
                    $additional_data[$value['id']]['profile_prescription']['guest_profile_id'] = $profile['guest_profile_id'];
                    $additional_data[$value['id']]['profile_prescription']['guest_id'] = $profile['guest_id'];
                    $additional_data[$value['id']]['profile_prescription']['reading_power_type'] = 1;
                    $additional_data[$value['id']]['prescription']['is_default'] = 1;
                    $additional_data[$value['id']]['prescription']['guest_profile_id'] = $profile['guest_profile_id'];
                    $additional_data[$value['id']]['prescription']['guest_id'] = $profile['guest_id'];
                    $additional_data[$value['id']]['prescription']['reading_power_type'] = 1;
                    $guest_prescription_id = $this->actionPrescriptionGuestAdd($additional_data[$value['id']]['profile_prescription']);
                    $additional_data[$value['id']]['profile_prescription']['guest_prescription_id'] = $guest_prescription_id;
                    $additional_data[$value['id']]['prescription']['guest_prescription_id'] = $guest_prescription_id;
                    $additional_data[$value['id']]['profile_prescription']['exam_date'] = date("m/d/Y",strtotime($additional_data[$value['id']]['profile_prescription']['exam_date']));
                    unset($additional_data[$value['id']]['profile_prescription']['expire_date']);
                    unset($additional_data[$value['id']]['prescription']['expire_date']);
                    unset($additional_data[$value['id']]['prescription']['from_rule_id']);
                }
                $success_data = array();
                foreach ($additional_data as $key => $value){
                    $code = $objectManager->get('\Pg\CheckoutExtend\Controller\Cart\Add')->execute($value);
                    $success_data[$key] = $code;
                }
                die(json_encode(array("code"=>0,"msg"=>"success","data"=>array($additional_data,$success_data))));
            }
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //匿名模拟/prescription/guest/add接口
    public function actionPrescriptionGuestAdd($prescription){
        unset($prescription['expire_date']);
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $headers = array('Content-Type: application/x-www-form-urlencoded');
        $url_base = $objectManager->get('\Pg\Learning\Block\Page\Banner')->getBaseUrl();
        $service_url=$url_base."/prescription/guest/add";
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $service_url); // 要访问的地址
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
            curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
            curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
            curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($prescription)); // Post提交的数据包
            curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
            curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($curl); // 执行操作
            if (curl_errno($curl)) {
                $error = 'Errno'.curl_error($curl);
                curl_close($curl); // 关闭CURL会话
                return array("code"=>-1,"msg"=>$error,"data"=>array());//捕抓异常
            }
            $result = json_decode($result,true);
            return 1307;
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //匿名模拟/profile/guest/add接口
    public function actionProfileGuestAdd($profile){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $headers = array('Content-Type: application/x-www-form-urlencoded');
        $url_base = $objectManager->get('\Pg\Learning\Block\Page\Banner')->getBaseUrl();
        $service_url=$url_base."/profile/guest/add";
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $service_url); // 要访问的地址
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
            curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
            curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
            curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($profile)); // Post提交的数据包
            curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
            curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($curl); // 执行操作
            if (curl_errno($curl)) {
                $error = 'Errno'.curl_error($curl);
                curl_close($curl); // 关闭CURL会话
                return array("code"=>-1,"msg"=>$error,"data"=>array());//捕抓异常
            }
            $result = json_decode($result,true);
            $profile['guest_profile_id'] = 1563;
            return $profile;
        }catch (\Exception $e){
            die(json_encode(array("code"=>-1,"msg"=>$e->getMessage(),"data"=>array())));
        }
    }
    //邮件发送模拟
    public function actionSendsss(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection=$objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $send_code = $objectManager->get("Magento\Customer\Model\EmailNotification")->sendHelpPrescription('15210066137@qq.com','123','www.baidu.com');
        var_dump($send_code);
        die();
    }
    //根据眼镜镜片的sku获取添加验光单的眼镜镜片数据
    public function get_data($lens_sku){
        $sql = "SELECT sku, `name`, entity_id AS productId, price AS `price`, price AS `price-original`, ('lenss') AS `type`, IFNULL(color, '') AS color, ('false') AS colorname, price AS `special_price`,(1)as `qty` 
FROM catalog_product_flat_1 
WHERE sku = '{$lens_sku}'";
        $data=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->queryAll();
        if(!$data){
            return null;
        }
        $data = $data[0];
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$product=$objectManager->get("\Magento\Catalog\Api\ProductRepositoryInterface")->get($data['sku']);
        $sql="select sku, `name`,price,special_price 
FROM catalog_product_flat_1 
WHERE sku = '{$data['sku']}'
";
        $products=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->queryAll();
        if (!empty($products)) {
            $product=$products[0];
            $special_price = $product["special_price"];
            if($special_price){
                $data['price'] = $product["price"];
                $data['special_price'] = $special_price;
            }
        }
        return $data;
    }
}
