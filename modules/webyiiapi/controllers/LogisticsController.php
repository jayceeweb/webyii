<?php
namespace app\modules\webyiiapi\controllers;

use app\modules\manager\models\AdminUser;
use Yii;
use Maropost\Api\Journeys;
use GuzzleHttp\Client as GuzzleClient;
use app\components\helper\AuthcodeHelper;
use app\models\PgExchangeData;

class LogisticsController extends BaseController {

//    add sunjinchao 20/2/25 物流状态
    public function actionLogistics(){
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $monitor_log=$objectManager->create("Pg\Monitor\Log");
        $monitor_log->log_start("logistics");
        $log = array();
        array_push($log,$content);
        $monitor_log->log($log);
        //开启日志
        //验证header
//        $token = Yii::$app->request->post('Authorization');
//        $token = 'NmVhNklnSHg1b3c4UE9oZTVSSG0rOXBxdkloUFVIdllhSXBxLzdDZGZ4eithdWpaelcveVpPV2RhdVdGLzljbDExbjlpeEhlclVjSjRWWGZTOS9DVGk5d2VZQXJRY0pKL2dlMTlYVlVJbDZSRjBMUTErR05yZz09';
//
//
//        $string = AuthcodeHelper::decode_ex($token);
//        if(!$string){
//            die(json_encode(["code"=>-2,"msg"=>"Interface secret key error"]));
//        }
//        $data = explode('-',$string);
//        $username = $data[0];
//        $password = $data[1];
//        $adminuser=AdminUser::find()->where(["username"=>$username])->one();
//        if (!$adminuser) {
//            die(json_encode(["code"=>-2,"msg"=>"The user does not exist"]));
//        }
//        array_push($log,$username);
//        $monitor_log->log($log);
//        $p=Yii::$container->get("app\components\Encryptor");
//        $checkpassword=$p->isValidHash($password,$adminuser->getAttribute("password"));
//        if (!$checkpassword) {
//            die(json_encode(["code"=>-2,"msg"=>"Password mistake"]));
//        }
        //验证header end
        //json数据处理

        $transaction= Yii::$app->db->beginTransaction();
        try {
        $data=$content['parameters'];
//        $data = Yii::$app->request->post('data');
        $event_type = $data['event_type'];
        if($event_type =='OUT_FOR_DELIVERY'){
            $logistics_code = 2;
        }else if($event_type =='CROSS_COUNTRY_TRANSFER'){
            $logistics_code = 1;
        }else{
            $monitor_log->log_error(["code"=>-1,"msg"=>"Wrong logistics status"]);
            die(json_encode(["code"=>-1,"msg"=>"Wrong logistics status"]));
        }
        array_push($log,$logistics_code);
        $monitor_log->log($log);
        $shipments = $data['shipments'];
        $logisticdata = "";
        $updatesql = "";
        $item_ids = array();
        $web_order_id = array();
        foreach ($shipments as $key => $value){
            foreach ($value['shipment']['items'] as $k => $v){
                $logisticdata .= '(\''.$data['event_type'].'\',';
                $logisticdata .= $value['shipment']['user_id'].',';
                $logisticdata .= '\''.$value['shipment']['user_name'].'\',';
                $logisticdata .= '\''.$value['shipment']['type'].'\',';
                $logisticdata .= '\''.$value['shipment']['shipping_id'].'\',';
                $logisticdata .= $value['shipment']['web_order_entity_id'].',';
                $web_order_id[] = $value['shipment']['web_order_entity_id'];
                $logisticdata .= $value['shipment']['web_order_number'].',';   //order_id 订单编号
                $logisticdata .= $value['shipment']['glasses_qty'].',';    //五六个数
                $logisticdata .= $value['shipment']['ep_tracking_code'].',';
                $logisticdata .= $logistics_code.',';
                $logisticdata .= $v['web_item_id'].'),';
                $updatesql .='('.$v['web_item_id'].',';
                $updatesql .= $value['shipment']['web_order_entity_id'].',';
                $updatesql .= $logistics_code.'),';
                $item_ids[] = $v['web_item_id'];
            }
        }
        $logisticdata = rtrim($logisticdata,',');
        $updatesql = rtrim($updatesql,',');
        //json数据处理结束
        //修改物流状态
            $insert_key = 'event_type,user_id,user_name,logistic_type,shipping_id,order_number,web_order_number,glasses_qty,ep_tracking_code,logistics_code,item_id'; //数组变为字符串
            $sql = "INSERT INTO sales_order_item_logistics_details ($insert_key) VALUES $logisticdata";
            $data = Yii::$app->db->createCommand($sql)->execute();
            if(!$data){
                $monitor_log->log_error(["code"=>-1,"msg"=>"New logistics details failed"]);
                die(json_encode(["code"=>-1,"msg"=>"New logistics details failed"]));
            }
            $sql = "INSERT into sales_order_item_logistics(item_id, order_id, logistics_code) values $updatesql ON DUPLICATE KEY UPDATE logistics_code=VALUES(logistics_code) , order_id=VALUES(order_id)";
            $data = Yii::$app->db->createCommand($sql)->execute();
            if (!$data){
                $monitor_log->log_error(["code"=>-1,"msg"=>"Added logistics status failure"]);
                die(json_encode(["code"=>-1,"msg"=>"Added logistics status failure"]));
            }
            //修改物流状态成功
            $orderitem_id = array();
            $order_increment_id = '(';
            $logisticsitem_id = array();
            $logistics_logistics_code = array();
            $substatus = "";
            $web_order_id_string = "(";
            foreach ($web_order_id as $key => $value) {
                $web_order_id_string .= $value.',';
            }
            $web_order_id_string = rtrim($web_order_id_string,',');
            $web_order_id_string .= ")";
            $sql = "select * from sales_order_item where `order_id` in $web_order_id_string and product_type='configurable'";
            $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            foreach ($data as $key => $val){
                $orderitem_id[$val['order_id']][] = $val['item_id'];
            }
            $sql = "select item_id,logistics_code,order_id from sales_order_item_logistics where `order_id` in $web_order_id_string";
            $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            foreach ($data as $key => $val){
                $logisticsitem_id[$val['order_id']][] = $val['item_id'];
                $logistics_logistics_code[] = $val['logistics_code'];
            }
            foreach ($logisticsitem_id as $key => $val){
                foreach ($orderitem_id as $k => $v){
                    if($key == $k){
                        if (count($val) == count($v)) {
                            if (in_array(1, $logistics_logistics_code) && in_array(2, $logistics_logistics_code)) {
                                //1,2
                                $substatus .= "WHEN $key THEN ".'\'Partially Shipped/Delivered\' ';
                                $order_increment_id .=  $key . ',';
                            } else if (in_array(1, $logistics_logistics_code)) {
                                //1
                                $substatus .= "WHEN $key THEN ".'\'Shipped\' ';
                                $order_increment_id .=  $key . ',';
                            } else if (in_array(2, $logistics_logistics_code)) {
                                //2
                                $substatus .= "WHEN $key THEN ".'\'Delivered\' ';
                                $order_increment_id .=  $key . ',';
                            }
                        } else {
                            if (in_array(1, $logistics_logistics_code) && in_array(2, $logistics_logistics_code)) {
                                //1,2,0
                                $order_increment_id .=  $key . ',';
                                $substatus .= "WHEN $key THEN ".'\'Partially Shipped/Delivered/In Lab\' ';
                            } else if (in_array(1, $logistics_logistics_code)) {
                                //1,0
                                $order_increment_id .=  $key . ',';
                                $substatus .= "WHEN $key THEN ".'\'Partially Shipped\' ';
                            } else if (in_array(2, $logistics_logistics_code)) {
                                //2,0
                                $order_increment_id .=  $key . ',';
                                $substatus .= "WHEN $key THEN ".'\'Partially Delivered\' ';
                            }
                        }
                    }
                }
            }
            $order_increment_id = rtrim($order_increment_id,',');
            $order_increment_id .= ')';
            $sql = "UPDATE sales_order SET substate = CASE entity_id $substatus END WHERE entity_id IN $order_increment_id";
            array_push($log,$sql);
            $monitor_log->log($log);
            $data = Yii::$app->db->createCommand($sql)->execute();
            $transaction->commit();
            $monitor_log->log_stop('Success');
            die(json_encode(["code"=>0,"msg"=>"Success"]));
        } catch(\Exception $e) {
            $transaction->rollBack();
            $monitor_log->log_error(["code"=>-1,"msg"=>"error","ret"=>$e]);
            die(json_encode(["code"=>-1,"msg"=>"error"]));
        }
    }
}
