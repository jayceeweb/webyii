<?php
namespace app\modules\webyiiapi\controllers;

use Yii;
use app\models\CatalogProductFlat1;
use app\models\CoreConfigData;
use app\models\PgProductTag;
use app\models\PgProductTagItem;

class TagController extends ApiController {
    public function actionListTagProduct(){
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $token=$content["token"];
        $page=$content["page"];
        $pagecount=$content["pagecount"];
        $condition=$content["condition"];
        if (empty($condition)) {
            $condition=[];
        }

        if (!isset($condition["tag"])) {
            return ["code"=>-1,"message"=>"param error"];
        }

        $ret=$this->actionListProduct();
        //"action"=>"list_products",
        $ret["action"]="list_tag_products";
        return $ret;
    }

    public function actionListProduct(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

//        $condition=[
//            "name"=>"Oval",
//            "short_description"=>"Oval"
//        ];
//        $content=[
//            "token"=>"123456",
//            "page"=>"0",
//            "pagecount"=>"10",
//            "condition"=>$condition,
//        ];

        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $token=$content["token"];
        $page=$content["page"];
        $pagecount=$content["pagecount"];
        $condition=$content["condition"];
        if (empty($condition)) {
            $condition=[];
        }

        $products=CatalogProductFlat1::getAllInStockConfigurableProducts($page,$pagecount,$condition);
        $cnt=CatalogProductFlat1::getAllInStockConfigurableProductsCount($condition);
        $static_url=CoreConfigData::getStaticUrl();
        foreach ($products as $i => $product) {
            if (isset($condition["tag"]) && !empty($condition["tag"])) {
                $simple_products=CatalogProductFlat1::getAllSimplesByTag([$product["entity_id"]],$condition["tag"]);
            } else {
                $simple_products=CatalogProductFlat1::getAllSimplesBySimpleData([$product["entity_id"]]);
            }
            foreach ($simple_products as $j => $simple_product) {
                $image=$simple_product["image"];
                $simple_product["image"]=$static_url."/media/catalog/product".$image;
                $simple_product["thumbnail"]=$static_url."/media/catalog/product/cache/600x300".$image;
                $simple_product["sku_id"]=$simple_product["entity_id"];
                unset($simple_product["entity_id"]);
                $simple_products[$j]=$simple_product;
            }
            $tags=PgProductTag::get_tags_by_product_id($product["entity_id"]);
            $product["product_id"]=$product["entity_id"];
            unset($product["entity_id"]);
            $product["tags"]=$tags;
            $product["skus"]=$simple_products;
            $product["image"]=$static_url."/media/catalog/product".$product["image"];
            $product["small_image"]=$static_url."/media/catalog/product".$product["small_image"];
            $product["thumbnail"]=$static_url."/media/catalog/product/cache/600x300".$product["thumbnail"];
            $products[$i]=$product;
        }
        $result=[
            "action"=>"list_products",
            "page"=>$page,
            "pagecount"=>$pagecount,
            "cnt"=>$cnt,
            "totalpage"=> ceil($cnt / $pagecount),
            "condition"=>$condition,
            "products"=>$products,
        ];
        return $result;
    }

    public function actionPostTagList() {
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $token=$content["token"];
        $products=$content["products"];
        $ret=[];
        if (!empty($token) && !empty($products)) {
            foreach ($products as $k=>$product) {
                $product_id=$product["product_id"];
                $tag=$product["tag"];
                $tag_position=$product["tag_position"];
                $skus=$product["skus"];
                $pg_tag=PgProductTag::findOne(["product_id"=>$product_id,"tag"=>$tag]);
                if (empty($pg_tag)) {
                    $pg_tag=new PgProductTag();
                }
                $pg_tag->product_id=$product_id;
                $pg_tag->tag=$tag;
                $pg_tag->tag_position=$tag_position;
                $pg_tag->save();
                $ret_pg_tag=$pg_tag->getAttributes();
                //保存明细
                foreach ($skus as $key=>$v) {
                    $pg_tag_item=PgProductTagItem::findOne(["tag_id"=>$pg_tag->getAttribute("id"),"product_id"=>$v["sku_id"]]);
                    if (empty($pg_tag_item)) {
                        $pg_tag_item=new PgProductTagItem();
                    }
                    $pg_tag_item->setAttribute("tag_id",$pg_tag->getAttribute("id"));
                    $pg_tag_item->setAttribute("product_id",$v["sku_id"]);
                    $pg_tag_item->setAttribute("tag_position",$v["tag_position"]);
                    $pg_tag_item->save();
                    $pg_tag_item_data=$pg_tag_item->getAttributes();
                    $pg_tag_item_data["sku_id"]=$pg_tag_item_data["product_id"];
                    unset($pg_tag_item_data["product_id"]);
                    $ret_pg_tag["skus"][]=$pg_tag_item_data;
                }
                $ret[]=$ret_pg_tag;
            }
            return ["code"=>0,"message"=>$ret];
        } else {
            return ["code"=>-1,"message"=>"param error"];
        }
    }

    public function actionRemoveTags(){
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $token=$content["token"];
        $products=$content["products"];
        if (!empty($token) && !empty($products)) {
            //参数校验
            foreach ($products as $k=>$product) {
                $product_id=$product["product_id"];
                $tag=$product["tag"];
                if (empty($product_id) || empty($tag)) {
                    return ["code"=>-1,"message"=>"param error"];
                }
            }
            //真正删除
            foreach ($products as $k=>$product) {
                $product_id = $product["product_id"];
                $tag = $product["tag"];
                $pg_tag=PgProductTag::findOne(["product_id" => $product_id, "tag" => $tag]);
                if (!empty($pg_tag)) {
                    PgProductTagItem::deleteAll(["tag_id"=>$pg_tag->getAttribute("id")]);
                }
                $ret = PgProductTag::deleteAll(["product_id" => $product_id, "tag" => $tag]);
            }
            return ["code"=>0,"message"=>"OK"];
        } else {
            return ["code"=>-1,"message"=>"param error"];
        }
    }
}
