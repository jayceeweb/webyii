<?php
namespace app\modules\webyiiapi\controllers;

use Yii;

class HoldController extends BaseController {
    public function actionHold(){
        $content=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $monitor_log=$objectManager->create("Pg\Monitor\Log");
        $monitor_log->log_start("hold");
        $order_id = '';
        $order_item_id = '';
        $item_id = '(';
        $orders_id = '(';
        $prescription_id = '(';
        $content_data = array();
        $i = 0;
        $log = array();
        array_push($log,$content);
        if(!$content){
            $monitor_log->log($log);
            die(json_encode(["code"=>0,"msg"=>"Success","data"=>$content]));
        }
        foreach ($content as $key => $value){
            foreach ($value as $k => $val){
                $content_data[$i]['order_id'] = $key;
                $content_data[$i]['item_id'] = ($val[0]=='')?'null':$val[0];
                $content_data[$i]['prescription_id'] = ($val[1]=='')?'null':$val[1];
                $content_data[$i]['profile_id'] = ($val[2]=='')?'null':$val[2];
                $i++;
                $order_id .= "(".$key.',';
                $order_id .= ($val[2]=='')?'null':$val[2];
                $order_id .= '),';

                $order_item_id .= "(".$key.',';
                $order_item_id .= ($val[0]=='')?'0':$val[0];
                $order_item_id .= '),';

                $orders_id .= $key.',';

                $item_id .= ($val[0]=='')?'null':$val[0];
                $item_id .= ',';

                $prescription_id .= ($val[1]=='')?'null':$val[1];
                $prescription_id .= ',';
            }
        }
        if(!$content_data){
            $monitor_log->log_error(["code"=>-1,"msg"=>"$content_data is error"]);
        }
        $item_id = rtrim($item_id,',');
        $item_id .= ')';
        $order_id = rtrim($order_id,',');
        $order_item_id = rtrim($order_item_id,',');
        $orders_id = rtrim($orders_id,',');
        $orders_id .= ')';
        $prescription_id = rtrim($prescription_id,',');
        $prescription_id .= ')';
        //测试图片
        $sql = "select entity_id,creation_time,order_id,profile_id,image_path,is_type,ifnull(item_id,0) as item_id from order_image where item_id in $item_id or ((order_id,profile_id) in ($order_id) and ifnull(item_id,0) = 0)";
        $order_image_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        //位置
//        $sql = "select order_id,profile_id,pupils_position,pupils_position_name,created_at from sales_order_survey where (order_id,profile_id) in ($order_id)";
//        $sales_order_survey_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        //历史验光单id
        $sql = "select prescription_id,created_at from pg_prescription_images_history where prescription_id in $prescription_id";
        $pg_prescription_images_history_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        //订单详情
        $sql = "select order_entity,order_item_entity,created_at,instruction from (select * from sales_order_additional where order_entity in $orders_id ORDER BY created_at DESC) `a` GROUP BY order_item_entity";
        $sales_order_additional_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        //暂停原因
        $sql = "select order_id,created_at,reason from sales_order_hold where order_id in $orders_id";
        $sales_order_hold_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        //验光单内是否有附加图片
        $sql = "select pe.prescription_img,soi.order_id,soi.profile_prescription_id from prescription_entity as pe INNER JOIN sales_order_item as soi on pe.entity_id = soi.profile_prescription_id where soi.order_id in $orders_id GROUP BY soi.profile_prescription_id";
        $prescription_entity_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($content_data as $key => $value){
            $content_data[$key]['code'] = 'false';
            $content_data[$key]['connects'] = '';
        }
        foreach ($content_data as $key => $value){
            foreach ($order_image_data as $k => $v){
                if($value['item_id'] == 'null'){
                    $value['item_id'] =0;
                }
                //item_id相同，但不等于0
                if ($value['item_id'] == $v['item_id'] && $value['item_id']!=0){
                    $content_data[$key]['code'] = 'true';
                    $content_data[$key]['connects'] = 'hold：图片hold，HoldTime：'.$v['creation_time'];
                }
                //order_id , profile_id ,item_id 相同，item_id 等于0
                if($value['profile_id'] == $v['profile_id']){
                    $content_data[$key]['code'] = 'true';
                    $content_data[$key]['connects'] = 'hold：图片hold，HoldTime：'.$v['creation_time'];
                }
            }
        }
//        foreach ($content_data as $key => $value){
//            foreach ($sales_order_survey_data as $k => $v){
//                //order_id , profile_id相同
//                if($value['order_id'] == $v['order_id'] && $value['profile_id'] == $v['profile_id']){
//                    $content_data[$key]['code'] = 'true';
//                    $content_data[$key]['connects'] = 'hold：位置hold，HoldTime：'.$v['created_at'];
//                }
//            }
//        }
        foreach ($content_data as $key => $value){
            foreach ($pg_prescription_images_history_data as $k => $v){
                if($value['prescription_id'] == $v['prescription_id']){
                    $content_data[$key]['code'] = 'true';
                    $content_data[$key]['connects'] = 'hold：历史验光单hold，HoldTime：'.$v['created_at'];
                }
            }
        }
        foreach ($content_data as $key => $value){
            foreach ($sales_order_additional_data as $k => $v){
                //order_id 和item_id相同
                if($value['order_id'] == $v['order_entity']){
                    $content_data[$key]['code'] = 'true';
                    $content_data[$key]['connects'] = 'hold：订单'.$v['instruction'].'，HoldTime：'.$v['created_at'];
                }
            }
        }
        foreach ($content_data as $key => $value){
            foreach ($sales_order_hold_data as $k => $v){
                if($value['order_id'] == $v['order_id']){
                    $content_data[$key]['code'] = 'true';
                    $content_data[$key]['connects'] = 'hold：原因'.$v['reason'].'，HoldTime：'.$v['created_at'];
                }
            }
        }
        foreach ($content_data as $key => $value){
            foreach ($prescription_entity_data as $k => $v){
                //order_id相同，并且
                if($value['order_id'] == $v['order_id'] && !empty($v['prescription_img'])){
                    $content_data[$key]['code'] = 'true';
                    $content_data[$key]['connects'] = 'hold：验光单hold，Prescription Img：'.$v['prescription_img'];
                }
            }
        }
        array_push($log,$content_data);
        $monitor_log->log($log);
        die(json_encode(["code"=>0,"msg"=>"Success","data"=>$content_data]));
    }
}
