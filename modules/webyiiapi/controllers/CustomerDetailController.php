<?php
namespace app\modules\webyiiapi\controllers;

use \Aws\Sns\SnsClient;
use \Aws\Credentials\Credentials;
use Aws\Exception\AwsException;
use Yii;

class CustomerDetailController extends BaseController {
    //customer系列start
    //获取customer的默认sql
    public function actionCustomerSql(){
        //计算用户有效购物车详细数量
//        $sql = "SELECT ce.entity_id, ce.email, CONCAT(ce.firstname, ce.lastname) AS `name`, ce.created_at, ( SELECT COUNT(0) FROM `profile` WHERE customer_id = ce.entity_id ) AS profile_num, IFNULL(( SELECT ROUND(sum(items_qty), 0) FROM quote WHERE customer_id = ce.entity_id AND is_active = 1 AND items_count > 0 ), 0 ) AS cart_num, ( SELECT COUNT(0) FROM sales_order WHERE customer_id = ce.entity_id ) AS order_num, ( SELECT COUNT(0) FROM `prescription_entity` AS pe INNER JOIN `profile` AS p ON pe.parent_id = p.profile_id WHERE p.customer_id = ce.entity_id ) AS Prescription_num FROM customer_entity AS ce";
        //计算用户购物车数量
        $sql = "SELECT ce.entity_id, ce.email, CONCAT(ce.firstname, ce.lastname) AS `name`, ce.created_at, ( SELECT COUNT(0) FROM `profile` WHERE customer_id = ce.entity_id ) AS profile_num, (SELECT IFNULL(cast(SUM(items_qty) AS decimal(15,0)),0) FROM quote WHERE customer_id = ce.entity_id and is_active = 1) AS cart_num, ( SELECT COUNT(0) FROM sales_order WHERE customer_id = ce.entity_id ) AS order_num, ( SELECT COUNT(0) FROM `prescription_entity` AS pe INNER JOIN `profile` AS p ON pe.parent_id = p.profile_id WHERE p.customer_id = ce.entity_id ) AS Prescription_num FROM customer_entity AS ce";
        return $sql;
    }
    //获取customer页面显示数据
    public function actionCustomer(){
        $num = 15;
        try{
            $customer_detail = $this->actionCustomerDetail($num);
            $customer_details = array();
            if($customer_detail['code']){
                $customer_details = $customer_detail['data'];
            }
            $customer_count = $this->actionCustomerCount();
            $customer_counts = array();
            if($customer_count['code']){
                $customer_counts = $customer_count['data'];
            }
            return ["code"=>1,"message"=>"ok","data"=>array("rows"=>$customer_details,"count"=>ceil($customer_counts/$num))];
        }catch (\Exception $e){
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //获取customer的详细信息
    public function actionCustomerDetail($num)
    {
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $page = $content['page']?$content['page']:1;
            $page = ($page-1) * $num;
            $email = $content['email']?$content['email']:'';
            $increment_id = $content['increment_id']?$content['increment_id']:'';
            $customer_sql = $this->actionCustomerSql();
            if($increment_id){
                $customer_sql .= " LEFT JOIN sales_order as so on ce.entity_id = so.customer_id WHERE so.increment_id = $increment_id";
                if($email){
                    $customer_sql .= " and ce.email = '{$email}'";
                }
                $customer_sql .= ' GROUP BY ce.email';
            }else{
                if($email){
                    $customer_sql .= " where ce.email = '{$email}'";
                }
            }
            $customer_sql .= " ORDER BY ce.entity_id desc limit $page,$num";
            $customer_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($customer_sql)->queryAll();
            return ["code"=>1,"message"=>"success","data"=>$customer_detail];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //获取customer的数量
    public function actionCustomerCount()
    {
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $email = $content['email']?$content['email']:'';
            $customer_sql = $this->actionCustomerSql();
            $increment_id = $content['increment_id']?$content['increment_id']:'';
            if($increment_id){
                $customer_sql .= " LEFT JOIN sales_order as so on ce.entity_id = so.customer_id WHERE so.increment_id = $increment_id";
                if($email){
                    $customer_sql .= " and ce.email = '{$email}'";
                }
                $customer_sql .= ' GROUP BY ce.email';
            }else{
                if($email){
                    $customer_sql .= " where ce.email = '{$email}'";
                }
            }
            $customer_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($customer_sql)->queryAll();
            $customer_count = count($customer_detail);
            return ["code"=>1,"message"=>"success","data"=>$customer_count];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据customer获取当前用户的验光单和此验光单下的购物车数量和订单数量
    public function actionCustomerPrescription(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $customer_id = $content['customer_id']?$content['customer_id']:'';
            $prescription_sql = "select * from prescription_entity as pe INNER JOIN `profile` as p on p.profile_id = pe.parent_id where customer_id = $customer_id";
            $prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($prescription_sql)->queryAll();
            $prescription_data = array();
            foreach ($prescription_detail as $key => $value){
                $prescription_data[$value['nickname']][$key] = $value;
                $prescription_data[$value['nickname']][$key]['cart_num'] = 0;
                $prescription_data[$value['nickname']][$key]['order_num'] = 0;
                $sql = "select count(0) as count from quote as q INNER JOIN quote_item as qi on q.entity_id = qi.quote_id where qi.profile_prescription_id = {$value['entity_id']} and q.is_active = 1";
                $prescription_data[$value['nickname']][$key]['cart_num'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['count'];
                $sql = "select count(0) as count from (select count(0) from sales_order_item where profile_prescription_id = {$value['entity_id']} GROUP BY order_id) as count";
                $prescription_data[$value['nickname']][$key]['order_num'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['count'];
            }
            return ["code"=>1,"message"=>"success","data"=>$prescription_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据customer和验光单iD获取购物车详情
    public function actionCustomerPrescriptionCart(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $customer_id = $content['customer_id']?$content['customer_id']:'';
            $prescription_id = $content['prescription_id']?$content['prescription_id']:'';
            $sql = "select qi.* from quote as q INNER JOIN quote_item as qi on q.entity_id = qi.quote_id where q.customer_id = $customer_id and profile_prescription_id = $prescription_id and is_active = 1";
            $cart_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $cart_group = array();
            foreach ($cart_detail as $key => $value){
                if(!$value['parent_item_id']){
                    $sql = "select * from quote_item where parent_item_id = {$value['parent_item_id']}";
                    $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                    array_unshift($data,$value);
                    $cart_group[$value['item_id']]=$data;
                }
            }
            $cart_data = array();
            foreach ($cart_group as $key => $value){
                $cart_data[$key]['cart_detail'] = $value;
                foreach ($value as $k => $val){
                    if($key == $val['item_id']){
                        if($val['profile_id']){
                            if($val['profile_prescription_id'] && $val['glasses_prescription_id']){
                                $profile_prescription_sql = "select * from prescription_entity where entity_id = {$val['profile_prescription_id']}";
                                $profile_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_prescription_sql)->queryOne();
                                $glass_prescription_sql = "select * from glasses_prescription where entity_id = {$val['glasses_prescription_id']}";
                                $glass_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($glass_prescription_sql)->queryOne();
                                $cart_data[$key]['profile_prescription'] = $profile_prescription_detail;
                                $cart_data[$key]['glass_prescription'] = $glass_prescription_detail;
                            }else{
                                $cart_data[$key]['profile_prescription'] = NULL;
                                $cart_data[$key]['glass_prescription'] = NULL;
                            }
                            $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                        }else{
                            $cart_data[$key]['profile_prescription'] = 'ACCESSORIES';
                            $cart_data[$key]['glass_prescription'] = 'ACCESSORIES';
                        }
                    }
                }
            }
            return ["code"=>1,"message"=>"success","data"=>$cart_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据customer获取当前用户的有效购物车详情
    public function actionCustomerCart(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $customer_id = $content['customer_id']?$content['customer_id']:'';
            $sql = "select qi.* from quote as q INNER JOIN quote_item as qi on q.entity_id = qi.quote_id where q.customer_id = $customer_id and is_active = 1";
            $cart_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $cart_group = array();
            foreach ($cart_detail as $key => $value){
                if(!$value['parent_item_id']){
                    $cart_group[$value['item_id']][]=$value;
                }else{
                    $cart_group[$value['parent_item_id']][]=$value;
                }
            }
            $cart_data = array();
            foreach ($cart_group as $key => $value){
                $cart_data[$key]['cart_detail'] = $value;
                foreach ($value as $k => $val){
                    if($key == $val['item_id']){
                        if($val['profile_id']){
                            if($val['profile_prescription_id'] && $val['glasses_prescription_id']){
                                $profile_prescription_sql = "select * from prescription_entity where entity_id = {$val['profile_prescription_id']}";
                                $profile_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_prescription_sql)->queryOne();
                                $glass_prescription_sql = "select * from glasses_prescription where entity_id = {$val['glasses_prescription_id']}";
                                $glass_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($glass_prescription_sql)->queryOne();
                                $cart_data[$key]['profile_prescription'] = $profile_prescription_detail;
                                $cart_data[$key]['glass_prescription'] = $glass_prescription_detail;
                            }else{
                                $cart_data[$key]['profile_prescription'] = NULL;
                                $cart_data[$key]['glass_prescription'] = NULL;
                            }
                            $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                        }else{
                            $cart_data[$key]['profile_prescription'] = 'ACCESSORIES';
                            $cart_data[$key]['glass_prescription'] = 'ACCESSORIES';
                        }
                    }
                }
            }
            return ["code"=>1,"message"=>"success","data"=>$cart_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据customer获取当前用户的全部购物车详情
    public function actionCustomerAllCart(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $customer_id = $content['customer_id']?$content['customer_id']:'';
            $sql = "select * from quote where customer_id = $customer_id and is_active = 1 ORDER BY created_at DESC";
            $cart_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            return ["code"=>1,"message"=>"success","data"=>$cart_detail];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据customer获取当前用户的全部订单详情
    public function actionCustomerAllOrder(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $customer_id = $content['customer_id']?$content['customer_id']:'';
            $sql = "select * from sales_order where customer_id = {$customer_id} ORDER BY created_at DESC";
            $order_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            return ["code"=>1,"message"=>"success","data"=>$order_detail];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //customer系列end
    //profile系列stert
    //获取profile的默认sql
    public function actionProfileSql(){
        $sql = "SELECT p.profile_id, p.nickname, p.gender, p.created_at, p.is_active, p.birthyear, ( SELECT COUNT(0) FROM `prescription_entity` AS pe WHERE pe.parent_id = p.profile_id ) AS Prescription_num, ( SELECT count(0) FROM customer_entity AS ce WHERE ce.entity_id = p.customer_id AND ce.default_profile = p.profile_id ) AS default_profile FROM `profile` AS p WHERE 1=1";
        return $sql;
    }
    //获取profile页面显示数据
    public function actionProfile(){
        try{
            $profile_detail = $this->actionProfileDetail();
            $profile_details = array();
            if($profile_detail['code']){
                $profile_details = $profile_detail['data'];
            }
            $profile_count = $this->actionProfileCount();
            $profile_counts = array();
            if($profile_count['code']){
                $profile_counts = $profile_count['data'];
            }
            return ["code"=>1,"message"=>"ok","data"=>array("rows"=>$profile_details,"count"=>$profile_counts)];
        }catch (\Exception $e){
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //获取profile的详细信息
    public function actionProfileDetail()
    {
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $customer_id = $content['customer_id']?$content['customer_id']:'';
            $profile_sql = $this->actionProfileSql();
            $profile_sql .= " and p.customer_id = $customer_id";
            $profile_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_sql)->queryAll();
            return ["code"=>1,"message"=>"success","data"=>$profile_detail];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //获取profile的数量
    public function actionProfileCount()
    {
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $customer_id = $content['customer_id']?$content['customer_id']:'';
            $profile_sql = $this->actionProfileSql();
            $profile_sql .= " and p.customer_id = $customer_id";
            $profile_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_sql)->queryAll();
            $profile_count = count($profile_detail);
            return ["code"=>1,"message"=>"success","data"=>$profile_count];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据profile获取当前用户的验光单和此验光单下的购物车数量和订单数量
    public function actionProfilePrescription(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $profile_id = $content['profile_id']?$content['profile_id']:'';
            $prescription_sql = "select * from prescription_entity where `parent_id` = $profile_id";
            $prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($prescription_sql)->queryAll();
            foreach ($prescription_detail as $key => $value){
                $prescription_detail[$key]['cart_num'] = 0;
                $prescription_detail[$key]['order_num'] = 0;
                $sql = "select count(0) as count from quote as q INNER JOIN quote_item as qi on q.entity_id = qi.quote_id where qi.profile_prescription_id = {$value['entity_id']} and q.is_active = 1";
                $prescription_detail[$key]['cart_num'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['count'];
                $sql = "select count(0) as count from (select count(0) from sales_order_item where profile_prescription_id = {$value['entity_id']} GROUP BY order_id) as count";
                $prescription_detail[$key]['order_num'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne()['count'];
            }
            return ["code"=>1,"message"=>"success","data"=>$prescription_detail];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据profile获取当前profile的购物车详情
    public function actionProfileCart(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $profile_id = $content['profile_id']?$content['profile_id']:'';
            $sql = "SELECT p.nickname, cpf1.color, cpf1.color_value, cpf1.image, eaos.`value` AS color_detail, qi.* FROM quote AS q INNER JOIN quote_item AS qi ON q.entity_id = qi.quote_id INNER JOIN catalog_product_flat_1 AS cpf1 ON qi.sku = cpf1.sku LEFT JOIN eav_attribute_option_swatch AS eaos ON eaos.option_id = cpf1.color LEFT JOIN `profile` AS p ON p.profile_id = qi.profile_id WHERE qi.profile_id = $profile_id and is_active = 1 ORDER BY item_id ASC";
            $cart_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $cart_group = array();
            foreach ($cart_detail as $key => $value){
                if(!$value['parent_item_id']){
                    $cart_group[$value['item_id']][]=$value;
                }else{
                    $cart_group[$value['parent_item_id']][]=$value;
                }
            }
            $cart_data = array();
            foreach ($cart_group as $key => $value){
                $cart_data[$key]['cart_detail'] = $value;
                foreach ($value as $k => $val){
                    if($key == $val['item_id']){
                        if($val['profile_id']){
                            if($val['profile_prescription_id'] && $val['glasses_prescription_id']){
                                $profile_prescription_sql = "select * from prescription_entity where entity_id = {$val['profile_prescription_id']}";
                                $profile_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_prescription_sql)->queryOne();
                                $glass_prescription_sql = "select * from glasses_prescription where entity_id = {$val['glasses_prescription_id']}";
                                $glass_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($glass_prescription_sql)->queryOne();
                                $cart_data[$key]['profile_prescription'] = $profile_prescription_detail;
                                $cart_data[$key]['glass_prescription'] = $glass_prescription_detail;
                            }else{
                                $cart_data[$key]['profile_prescription'] = NULL;
                                $cart_data[$key]['glass_prescription'] = NULL;
                            }
                            $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                        }else{
                            $cart_data[$key]['profile_prescription'] = 'ACCESSORIES';
                            $cart_data[$key]['glass_prescription'] = 'ACCESSORIES';
                        }
                    }else{
                        $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                    }
                }
            }
            return ["code"=>1,"message"=>"success","data"=>$cart_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //profile系列end
    //cart系列stert
    //根据购物车id获取购物车详情
    public function actionCartidCart(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $cart_id = $content['cart_id']?$content['cart_id']:'';
            $sql = "SELECT p.nickname, cpf1.color, cpf1.color_value, cpf1.image, eaos.`value` AS color_detail, qi.* FROM quote AS q INNER JOIN quote_item AS qi ON q.entity_id = qi.quote_id INNER JOIN catalog_product_flat_1 AS cpf1 ON qi.sku = cpf1.sku LEFT JOIN eav_attribute_option_swatch AS eaos ON eaos.option_id = cpf1.color LEFT JOIN `profile` AS p ON p.profile_id = qi.profile_id WHERE q.entity_id = $cart_id ORDER BY item_id ASC";
            $cart_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $cart_group = array();
            foreach ($cart_detail as $key => $value){
                if(!$value['parent_item_id']){
                    $cart_group[$value['item_id']][]=$value;
                }else{
                    $cart_group[$value['parent_item_id']][]=$value;
                }
            }
            $cart_data = array();
            foreach ($cart_group as $key => $value){
                $cart_data[$key]['cart_detail'] = $value;
                foreach ($value as $k => $val){
                    if($key == $val['item_id']){
                        if($val['profile_id']){
                            if($val['profile_prescription_id'] && $val['glasses_prescription_id']){
                                $profile_prescription_sql = "select * from prescription_entity where entity_id = {$val['profile_prescription_id']}";
                                $profile_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_prescription_sql)->queryOne();
                                $glass_prescription_sql = "select * from glasses_prescription where entity_id = {$val['glasses_prescription_id']}";
                                $glass_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($glass_prescription_sql)->queryOne();
                                $cart_data[$key]['profile_prescription'] = $profile_prescription_detail;
                                $cart_data[$key]['glass_prescription'] = $glass_prescription_detail;
                            }else{
                                $cart_data[$key]['profile_prescription'] = NULL;
                                $cart_data[$key]['glass_prescription'] = NULL;
                            }
                            $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                        }else{
                            $cart_data[$key]['profile_prescription'] = 'ACCESSORIES';
                            $cart_data[$key]['glass_prescription'] = 'ACCESSORIES';
                        }
                    }else{
                        $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                    }
                }
            }
            //准备写跳转路径
//            foreach ($cart_data as $key => $value){
//
//            }
//            $cart_data['glass_prescription']['use_for']
//            $cart_data['glass_prescription']['use_for']
            return ["code"=>1,"message"=>"success","data"=>$cart_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //cart系列end
    //order系列stert
    //根据订单id获取订单详情
    public function actionOrderidOrder(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $order_id = $content['order_id']?$content['order_id']:'';
            $sql = "SELECT p.nickname, cpf1.color, cpf1.color_value, cpf1.image, eaos.`value` AS color_detail, soi.* FROM sales_order_item AS soi  INNER JOIN catalog_product_flat_1 AS cpf1 ON soi.sku = cpf1.sku LEFT JOIN eav_attribute_option_swatch AS eaos ON eaos.option_id = cpf1.color LEFT JOIN `profile` AS p ON p.profile_id = soi.profile_id WHERE soi.order_id = $order_id ORDER BY soi.item_id ASC";
            $order_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $order_address_sql = "SELECT soa.*, so.*, vpt.details FROM sales_order AS so LEFT JOIN sales_order_address AS soa ON so.shipping_address_id = soa.entity_id LEFT JOIN sales_payment_transaction AS spt ON spt.order_id = so.entity_id LEFT JOIN vault_payment_token_order_payment_link AS vptopl ON spt.payment_id = vptopl.order_payment_id LEFT JOIN vault_payment_token AS vpt ON vpt.entity_id = vptopl.payment_token_id WHERE so.entity_id = $order_id";
            $order_address_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($order_address_sql)->queryOne();
            $order_address_detail = $this->orderaddressdetail($order_address_detail);
            $order_group = array();
            foreach ($order_detail as $key => $value){
                if(!$value['parent_item_id']){
                    $order_group[$value['item_id']][]=$value;
                }else{
                    $order_group[$value['parent_item_id']][]=$value;
                }
            }
            $order_data = array();
            foreach ($order_group as $key => $value){
                $order_data[$key]['order_detail'] = $value;
                $order_data[$key]['order_address_data'] = $order_address_detail;
                foreach ($value as $k => $val){
                    if($key == $val['item_id']){
                        if($val['profile_id']){
                            if($val['profile_prescription_id'] && $val['glasses_prescription_id']){
                                $profile_prescription_sql = "select * from prescription_entity where entity_id = {$val['profile_prescription_id']}";
                                $profile_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_prescription_sql)->queryOne();
                                $glass_prescription_sql = "select * from glasses_prescription where entity_id = {$val['glasses_prescription_id']}";
                                $glass_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($glass_prescription_sql)->queryOne();
                                $order_data[$key]['profile_prescription'] = $profile_prescription_detail;
                                $order_data[$key]['glass_prescription'] = $glass_prescription_detail;
                            }else{
                                $order_data[$key]['profile_prescription'] = NULL;
                                $order_data[$key]['glass_prescription'] = NULL;
                            }
                            $order_data[$key]['order_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                        }else{
                            $order_data[$key]['profile_prescription'] = 'ACCESSORIES';
                            $order_data[$key]['glass_prescription'] = 'ACCESSORIES';
                        }
                    }else{
                        $order_data[$key]['order_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                    }
                }
            }
            return ["code"=>1,"message"=>"success","data"=>$order_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //order系列end
    //prescription系列stert
    //根据验光单id获取购物车详情
    public function actionPrescriptionCart(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $prescription_id = $content['prescription_id']?$content['prescription_id']:'';
            $sql = "SELECT p.nickname, cpf1.color, cpf1.color_value, cpf1.image, eaos.`value` AS color_detail, qi.* FROM quote AS q INNER JOIN quote_item AS qi ON q.entity_id = qi.quote_id INNER JOIN catalog_product_flat_1 AS cpf1 ON qi.sku = cpf1.sku LEFT JOIN eav_attribute_option_swatch AS eaos ON eaos.option_id = cpf1.color LEFT JOIN `profile` AS p ON p.profile_id = qi.profile_id where qi.profile_prescription_id = $prescription_id and q.is_active = 1";
            $cart_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $cart_group = array();
            foreach ($cart_detail as $key => $value){
                if(!$value['parent_item_id']){
                    $sql = "select * from quote_item where parent_item_id = {$value['item_id']}";
                    $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                    array_unshift($data,$value);
                    $cart_group[$value['item_id']]=$data;
                }
            }
            $cart_data = array();
            foreach ($cart_group as $key => $value){
                $cart_data[$key]['cart_detail'] = $value;
                foreach ($value as $k => $val){
                    if($key == $val['item_id']){
                        if($val['profile_id']){
                            $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                            if($val['profile_prescription_id'] && $val['glasses_prescription_id']){
                                $profile_prescription_sql = "select * from prescription_entity where entity_id = {$val['profile_prescription_id']}";
                                $profile_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_prescription_sql)->queryOne();
                                $glass_prescription_sql = "select * from glasses_prescription where entity_id = {$val['glasses_prescription_id']}";
                                $glass_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($glass_prescription_sql)->queryOne();
                                $cart_data[$key]['profile_prescription'] = $profile_prescription_detail;
                                $cart_data[$key]['glass_prescription'] = $glass_prescription_detail;
                            }else{
                                $cart_data[$key]['profile_prescription'] = NULL;
                                $cart_data[$key]['glass_prescription'] = NULL;
                            }
                            $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                        }else{
                            $cart_data[$key]['profile_prescription'] = 'ACCESSORIES';
                            $cart_data[$key]['glass_prescription'] = 'ACCESSORIES';
                        }
                    }else{
                        $cart_data[$key]['cart_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                    }
                }
            }
            return ["code"=>1,"message"=>"success","data"=>$cart_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //根据验光单id获取订单详情
    public function actionPrescriptionOrder(){
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        try {
            $prescription_id = $content['prescription_id']?$content['prescription_id']:'';
            $sql = "SELECT p.nickname, cpf1.color, cpf1.color_value, cpf1.image, eaos.`value` AS color_detail, soi.* FROM sales_order_item AS soi INNER JOIN catalog_product_flat_1 AS cpf1 ON soi.sku = cpf1.sku LEFT JOIN eav_attribute_option_swatch AS eaos ON eaos.option_id = cpf1.color LEFT JOIN `profile` AS p ON p.profile_id = soi.profile_id WHERE soi.profile_prescription_id = $prescription_id";
            $order_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $order_group = array();
            foreach ($order_detail as $key => $value){
                if(!$value['parent_item_id']){
                    $sql = "select * from sales_order_item where parent_item_id = {$value['item_id']}";
                    $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                    array_unshift($data,$value);
                    $order_group[$value['item_id']]=$data;
                }
            }
            $order_data = array();
            foreach ($order_group as $key => $value){
                $order_data[$key]['order_detail'] = $value;
                foreach ($value as $k => $val){
                    if($key == $val['item_id']){
                        if($val['profile_id']){
                            $order_address_sql = "SELECT soa.*, so.*, vpt.details FROM sales_order AS so LEFT JOIN sales_order_address AS soa ON so.shipping_address_id = soa.entity_id LEFT JOIN sales_payment_transaction AS spt ON spt.order_id = so.entity_id LEFT JOIN vault_payment_token_order_payment_link AS vptopl ON spt.payment_id = vptopl.order_payment_id LEFT JOIN vault_payment_token AS vpt ON vpt.entity_id = vptopl.payment_token_id WHERE so.entity_id = {$val['order_id']}";
                            $order_address_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($order_address_sql)->queryOne();
                            $order_address_detail = $this->orderaddressdetail($order_address_detail);
                            if($val['profile_prescription_id'] && $val['glasses_prescription_id']){
                                $profile_prescription_sql = "select * from prescription_entity where entity_id = {$val['profile_prescription_id']}";
                                $profile_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($profile_prescription_sql)->queryOne();
                                $glass_prescription_sql = "select * from glasses_prescription where entity_id = {$val['glasses_prescription_id']}";
                                $glass_prescription_detail = \app\components\helper\DbHelper::getSlaveDb()->createCommand($glass_prescription_sql)->queryOne();
                                $order_data[$key]['profile_prescription'] = $profile_prescription_detail;
                                $order_data[$key]['glass_prescription'] = $glass_prescription_detail;
                            }else{
                                $order_data[$key]['profile_prescription'] = NULL;
                                $order_data[$key]['glass_prescription'] = NULL;
                            }
                            $order_data[$key]['order_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                            $order_data[$key]['order_address_detail'] = $order_address_detail;
                        }else{
                            $order_data[$key]['profile_prescription'] = 'ACCESSORIES';
                            $order_data[$key]['glass_prescription'] = 'ACCESSORIES';
                        }
                    }else{
                        $order_data[$key]['order_detail'][$k]['special_price'] = $this->specialPrice($val['sku']);
                    }
                }
            }
            return ["code"=>1,"message"=>"success","data"=>$order_data];
        } catch (\Exception $e) {
            return ["code"=>-1,"message"=>$e->getMessage(),"data"=>array()];
        }
    }
    //prescription系列end
    //补充接口start

    //获取产品打折价
    public function specialPrice($sku){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product_price_info=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo($sku);
        return $product_price_info['special_price'];
    }
    public function orderaddressdetail($order_address_detail){
        $details = $order_address_detail['details'];
        $order_address_detail['maskedCC'] = '';
        $order_address_detail['card_type'] = '';
        $order_address_detail['country_name'] = '';
        $details = json_decode($details, true);
        $details['maskedCC'] = isset($details['maskedCC'])?$details['maskedCC']:'';
        $order_address_detail['maskedCC'] = 'xxxx-' . $details['maskedCC'];
        $card_type = [
            'AE' => 'American Express',
            'VI' => 'Visa',
            'MC' => 'MasterCard',
            'DI' => 'Discover',
            'JBC' => 'JBC',
            'CUP' => 'China Union Pay',
            'MI' => 'Maestro',
        ];
        $countries = [
            'AD' => 'Andorra',
            'AE' => 'United Arab Emirates',
            'AF' => 'Afghanistan',
            'AG' => 'Antigua and Barbuda',
            'AI' => 'Anguilla',
            'AL' => 'Albania',
            'AM' => 'Armenia',
            'AN' => 'Netherlands Antilles',
            'AO' => 'Angola',
            'AR' => 'Argentina',
            'AT' => 'Austria',
            'AU' => 'Australia',
            'AW' => 'Aruba',
            'AX' => 'Aland Island (Finland)',
            'AZ' => 'Azerbaijan',
            'BA' => 'Bosnia-Herzegovina',
            'BB' => 'Barbados',
            'BD' => 'Bangladesh',
            'BE' => 'Belgium',
            'BF' => 'Burkina Faso',
            'BG' => 'Bulgaria',
            'BH' => 'Bahrain',
            'BI' => 'Burundi',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BN' => 'Brunei Darussalam',
            'BO' => 'Bolivia',
            'BR' => 'Brazil',
            'BS' => 'Bahamas',
            'BT' => 'Bhutan',
            'BW' => 'Botswana',
            'BY' => 'Belarus',
            'BZ' => 'Belize',
            'CA' => 'Canada',
            'CC' => 'Cocos Island (Australia)',
            'CD' => 'Congo, Democratic Republic of the',
            'CF' => 'Central African Republic',
            'CG' => 'Congo, Republic of the',
            'CH' => 'Switzerland',
            'CI' => 'Ivory Coast (Cote d Ivoire)',
            'CK' => 'Cook Islands (New Zealand)',
            'CL' => 'Chile',
            'CM' => 'Cameroon',
            'CN' => 'China',
            'CO' => 'Colombia',
            'CR' => 'Costa Rica',
            'CU' => 'Cuba',
            'CV' => 'Cape Verde',
            'CX' => 'Christmas Island (Australia)',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DE' => 'Germany',
            'DJ' => 'Djibouti',
            'DK' => 'Denmark',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'DZ' => 'Algeria',
            'EC' => 'Ecuador',
            'EE' => 'Estonia',
            'EG' => 'Egypt',
            'ER' => 'Eritrea',
            'ES' => 'Spain',
            'ET' => 'Ethiopia',
            'FI' => 'Finland',
            'FJ' => 'Fiji',
            'FK' => 'Falkland Islands',
            'FM' => 'Micronesia, Federated States of',
            'FO' => 'Faroe Islands',
            'FR' => 'France',
            'GA' => 'Gabon',
            'GB' => 'Great Britain and Northern Ireland',
            'GD' => 'Grenada',
            'GE' => 'Georgia, Republic of',
            'GF' => 'French Guiana',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GL' => 'Greenland',
            'GM' => 'Gambia',
            'GN' => 'Guinea',
            'GP' => 'Guadeloupe',
            'GQ' => 'Equatorial Guinea',
            'GR' => 'Greece',
            'GS' => 'South Georgia (Falkland Islands)',
            'GT' => 'Guatemala',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HK' => 'Hong Kong',
            'HN' => 'Honduras',
            'HR' => 'Croatia',
            'HT' => 'Haiti',
            'HU' => 'Hungary',
            'ID' => 'Indonesia',
            'IE' => 'Ireland',
            'IL' => 'Israel',
            'IN' => 'India',
            'IQ' => 'Iraq',
            'IR' => 'Iran',
            'IS' => 'Iceland',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JO' => 'Jordan',
            'JP' => 'Japan',
            'KE' => 'Kenya',
            'KG' => 'Kyrgyzstan',
            'KH' => 'Cambodia',
            'KI' => 'Kiribati',
            'KM' => 'Comoros',
            'KN' => 'Saint Kitts (Saint Christopher and Nevis)',
            'KP' => 'North Korea (Korea, Democratic People\'s Republic of)',
            'KR' => 'South Korea (Korea, Republic of)',
            'KW' => 'Kuwait',
            'KY' => 'Cayman Islands',
            'KZ' => 'Kazakhstan',
            'LA' => 'Laos',
            'LB' => 'Lebanon',
            'LC' => 'Saint Lucia',
            'LI' => 'Liechtenstein',
            'LK' => 'Sri Lanka',
            'LR' => 'Liberia',
            'LS' => 'Lesotho',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'LV' => 'Latvia',
            'LY' => 'Libya',
            'MA' => 'Morocco',
            'MC' => 'Monaco (France)',
            'MD' => 'Moldova',
            'MG' => 'Madagascar',
            'MK' => 'Macedonia, Republic of',
            'ML' => 'Mali',
            'MM' => 'Burma',
            'MN' => 'Mongolia',
            'MO' => 'Macao',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MS' => 'Montserrat',
            'MT' => 'Malta',
            'MU' => 'Mauritius',
            'MV' => 'Maldives',
            'MW' => 'Malawi',
            'MX' => 'Mexico',
            'MY' => 'Malaysia',
            'MZ' => 'Mozambique',
            'NA' => 'Namibia',
            'NC' => 'New Caledonia',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NI' => 'Nicaragua',
            'NL' => 'Netherlands',
            'NO' => 'Norway',
            'NP' => 'Nepal',
            'NR' => 'Nauru',
            'NZ' => 'New Zealand',
            'OM' => 'Oman',
            'PA' => 'Panama',
            'PE' => 'Peru',
            'PF' => 'French Polynesia',
            'PG' => 'Papua New Guinea',
            'PH' => 'Philippines',
            'PK' => 'Pakistan',
            'PL' => 'Poland',
            'PM' => 'Saint Pierre and Miquelon',
            'PN' => 'Pitcairn Island',
            'PT' => 'Portugal',
            'PY' => 'Paraguay',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RS' => 'Serbia',
            'RU' => 'Russia',
            'RW' => 'Rwanda',
            'SA' => 'Saudi Arabia',
            'SB' => 'Solomon Islands',
            'SC' => 'Seychelles',
            'SD' => 'Sudan',
            'SE' => 'Sweden',
            'SG' => 'Singapore',
            'SH' => 'Saint Helena',
            'SI' => 'Slovenia',
            'SK' => 'Slovak Republic',
            'SL' => 'Sierra Leone',
            'SM' => 'San Marino',
            'SN' => 'Senegal',
            'SO' => 'Somalia',
            'SR' => 'Suriname',
            'ST' => 'Sao Tome and Principe',
            'SV' => 'El Salvador',
            'SY' => 'Syrian Arab Republic',
            'SZ' => 'Swaziland',
            'TC' => 'Turks and Caicos Islands',
            'TD' => 'Chad',
            'TG' => 'Togo',
            'TH' => 'Thailand',
            'TJ' => 'Tajikistan',
            'TK' => 'Tokelau (Union Group) (Western Samoa)',
            'TL' => 'East Timor (Timor-Leste, Democratic Republic of)',
            'TM' => 'Turkmenistan',
            'TN' => 'Tunisia',
            'TO' => 'Tonga',
            'TR' => 'Turkey',
            'TT' => 'Trinidad and Tobago',
            'TV' => 'Tuvalu',
            'TW' => 'Taiwan',
            'TZ' => 'Tanzania',
            'UA' => 'Ukraine',
            'UG' => 'Uganda',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VA' => 'Vatican City',
            'VC' => 'Saint Vincent and the Grenadines',
            'VE' => 'Venezuela',
            'VG' => 'British Virgin Islands',
            'VN' => 'Vietnam',
            'VU' => 'Vanuatu',
            'WF' => 'Wallis and Futuna Islands',
            'WS' => 'Western Samoa',
            'YE' => 'Yemen',
            'YT' => 'Mayotte (France)',
            'ZA' => 'South Africa',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
            'US' => 'United States',
        ];
        $details['type'] = isset($details['type'])?$details['type']:'';
        $order_address_detail['card_type'] = isset($card_type[$details['type']]) ? $card_type[$details['type']] : $details['type'];
        $order_address_detail['country_name'] = isset($countries[$order_address_detail['country_id']])?$countries[$order_address_detail['country_id']]:'';
        return $order_address_detail;
    }
}
