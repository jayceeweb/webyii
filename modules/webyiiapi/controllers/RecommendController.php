<?php
namespace app\modules\webyiiapi\controllers;

use Yii;
use app\components\helper\AuthcodeHelper;

class RecommendController extends BaseController {
    //推荐配件第一版 start
    public function actionRecommend(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $recommend_array = array();
        $sku = '';
        $count = 3;
        $blue_code = false;

        //获取传来的购物车id
        $quote_id=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        if(!isset($quote_id['quote_id'])){
            die(json_encode(["code"=>-1,"msg"=>"Error","data"=>array('error'=>-1,'mag'=>'The shopping cart ID could not be found')]));
        }
        $quote_id = $quote_id['quote_id'];
        $accessories_str = $this->getaccessories_str();
        $accessories_array = explode(',',$accessories_str);
        $accessories_sql = '';
        foreach ($accessories_array as $key => $value){
            $accessories_sql .= " cpf1.attribute_set_id = $value or";
        }
        $accessories_sql = rtrim($accessories_sql,'or');
        $accessories_sql = '('.$accessories_sql.')';
        if($objectManager->get("\Magento\Customer\Model\Session")->isLoggedIn()){
            $sql = "SELECT cpf1.sku FROM ( SELECT customer_email FROM quote WHERE entity_id = $quote_id ) AS `q` INNER JOIN quote AS `q1` ON q.customer_email = q1.customer_email INNER JOIN quote_item AS qi ON q1.entity_id = qi.quote_id INNER JOIN catalog_product_flat_1 AS cpf1 ON cpf1.sku = qi.sku AND $accessories_sql WHERE q1.inactive = 1 OR q1.entity_id = $quote_id GROUP BY qi.sku";
        }else{
            $sql = "select qi.sku from catalog_product_flat_1 as cpf1 INNER JOIN quote_item as qi on cpf1.sku = qi.sku and $accessories_sql where quote_id = $quote_id GROUP BY qi.sku";
        }
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($data as $key => $value){
            $sku .= '\''.$value['sku'].'\',';
        }
        $sku = rtrim($sku,',');
        //获取默认推荐的sku
        $array = $this->getSku($recommend_array,$count,$sku,'default');
        $recommend_array = $array['recommend_array'];
        $count = $array['count'];
        $sku = $array['sku'];
        $sku = $sku?$sku:'\'\'';
        //判断默认推荐的sku个数
        if($count){
            $sql = "select cpf1.clipon as sku from catalog_product_flat_1 as cpf1 INNER JOIN quote_item as qi on cpf1.sku = qi.sku and qi.quote_id = $quote_id and cpf1.is_clip_on = 1 and cpf1.clipon not in (select sku from catalog_product_flat_1 where entity_id in (select cpsl.parent_id from catalog_product_super_link as cpsl INNER JOIN catalog_product_flat_1 as cpf1 on cpsl.product_id = cpf1.entity_id and cpf1.sku in ($sku))) GROUP BY cpf1.sku";
            $clipon = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            if($clipon){
                $clipon_sku = '';
                foreach ($clipon as $key => $value){
                    $clipon_sku .= '\''.$value['sku'].'\',';
                }
                $clipon_sku = rtrim($clipon_sku,',');
                $sql = "select cpsl.product_id as entity_id from catalog_product_flat_1 as cpf1 INNER JOIN catalog_product_super_link as cpsl on cpsl.parent_id = cpf1.entity_id where sku in ($clipon_sku) GROUP BY cpf1.sku";
                $clipon_simple_entity_id_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                $clipon_simple_sku_id = '';
                foreach ($clipon_simple_entity_id_data as $key => $value){
                    $clipon_simple_sku_id .= '\''.$value['entity_id'].'\',';
                }
                $clipon_simple_sku_id = rtrim($clipon_simple_sku_id,',');
                $sql = "SELECT cpf1.sku, cpf1.image, cpf1.color, cpf1.price, cpf1.`name`, cpsl.product_id, cpsl.parent_id, cpf1.`entity_id` FROM catalog_product_flat_1 AS cpf1 INNER JOIN catalog_product_super_link AS cpsl ON cpsl.product_id = cpf1.entity_id INNER JOIN cataloginventory_stock_item AS csi ON csi.product_id = cpf1.entity_id WHERE cpf1.entity_id IN ($clipon_simple_sku_id) AND type_id = 'simple' AND cpf1.retired != 1 AND csi.is_in_stock = 1 GROUP BY cpf1.sku limit $count";
                $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                foreach ($data as $key => $value){
                    if(count($recommend_array) >=3){
                        break;
                    }else{
                        $entity_id = $value['entity_id'];
                        $url_sql = "select request_path from url_rewrite where entity_id = $entity_id";
                        $value['url'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($url_sql)->queryAll()[0]['request_path'];
                        $recommend_array[] = $value;
                        $sku .= '\''.$value['sku'].'\',';
                    }
                }
                $sku = rtrim($sku,',');
                $count = 3-count($recommend_array);
            }
        }
        //判断默认推荐的sku个数和夹片推荐的个数
        if($count){
            $sql = "select qi.`name` from catalog_product_flat_1 as cpf1 INNER JOIN quote_item as qi on cpf1.sku = qi.sku and qi.quote_id = $quote_id and cpf1.attribute_set_id = 14 and qi.`name` like '%blue%'";
            $name = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            //判断是否还有蓝光眼镜
            if($name){
                $blue_code = true;
            }
            if(!$blue_code){
                $sql = sprintf("select customer_id from quote where entity_id = %u",$quote_id);
                $name = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                //判断购物车是否含有customer_id
                if(isset($name['customer_id']) && $name['customer_id'] != ''){
                    $customer_id = $name['customer_id'];
                    $sql = "SELECT soi.`name` FROM sales_order AS so INNER JOIN sales_order_item AS soi ON so.entity_id = soi.order_id AND so.customer_id = $customer_id AND soi.`name` LIKE '%blue%'";
                    $name = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                    //判断当前购物车中的用户是否曾经下过蓝光眼镜
                    if($name){
                        $blue_code = true;
                    }
                }
            }
            //如果含有蓝光眼镜，新增蓝光推荐
            if($blue_code){
                $array = $this->getSku($recommend_array,$count,$sku,'blue');
                $recommend_array = $array['recommend_array'];
                $count = $array['count'];
                $sku = $array['sku'];
            }
            //判断数量是否大于3
            if($count){
                //获取配件的accessories_id;
                $sku = $sku?$sku:'\'\'';
                $sql = sprintf("SELECT soi.sku,cpf1.image,cpf1.color,cpf1.price,cpf1.`name`,cpsl.product_id,cpsl.parent_id,cpf1.`entity_id` FROM sales_order_item AS soi INNER JOIN catalog_product_flat_1 AS cpf1 ON soi.sku = cpf1.sku and cpf1.sku not in (%s) INNER JOIN cataloginventory_stock_item AS csi ON cpf1.entity_id = csi.product_id AND $accessories_sql AND cpf1.type_id = 'simple' AND soi.product_type = 'simple' AND cpf1.retired != 1 AND csi.is_in_stock = 1  inner JOIN catalog_product_super_link AS cpsl on cpsl.product_id = cpf1.entity_id GROUP BY soi.sku ORDER BY sum(soi.qty_ordered) DESC LIMIT %u",$sku,$array['count']);
                $name = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                $sku = $sku!='\'\''?$sku.',':'';
                //获取销量最好的产品
                foreach ($name as $key => $value){
                    if(count($recommend_array) >=3){
                        break;
                    }else{
                        $entity_id = $value['entity_id'];
                        $url_sql = "select request_path from url_rewrite where entity_id = $entity_id";
                        $value['url'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($url_sql)->queryAll()[0]['request_path'];
                        $recommend_array[] = $value;
                        $sku .= '\''.$value['sku'].'\',';
                    }
                }
                $sku = rtrim($sku,',');
                $count = 3-count($recommend_array);
            }
            if($count){
                $sku = $sku?$sku:'\'\'';
                $sql = "SELECT cpf1.sku, cpf1.image, cpf1.color, cpf1.price, cpf1.`name`, cpsl.product_id, cpsl.parent_id, cpf1.`entity_id` FROM catalog_product_flat_1 AS cpf1 INNER JOIN catalog_product_super_link AS cpsl ON cpsl.product_id = cpf1.entity_id INNER JOIN cataloginventory_stock_item AS csi ON csi.product_id = cpf1.entity_id WHERE $accessories_sql and cpf1.sku not in ($sku) AND type_id = 'simple' AND cpf1.retired != 1 AND csi.is_in_stock = 1 GROUP BY cpf1.sku limit $count";
                $product_sku = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
                $sku = $sku!='\'\''?$sku.',':'';
                //如果个数不够，获取其他配件补齐
                foreach ($product_sku as $key => $value){
                    if(count($recommend_array) >=3){
                        break;
                    }else{
                        $entity_id = $value['entity_id'];
                        $url_sql = "select request_path from url_rewrite where entity_id = $entity_id";
                        $value['url'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($url_sql)->queryAll()[0]['request_path'];
                        $recommend_array[] = $value;
                    }
                }
            }
        }
        foreach ($recommend_array as $key => $value){
            $recommend_array[$key]['special_price'] = $this->getPrice($value['sku']);
        }
        die(json_encode(["code"=>0,"msg"=>"Success","data"=>$recommend_array]));
    }
    public function getPrice($sku){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $p = $objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->get($sku);
        $dp_info=$objectManager->get("Pg\CatalogExtend\Helper\Product")->calcProductPrice($p);
        $special_price = isset($dp_info['special_price'])?$dp_info['special_price']:0;
        return number_format($special_price,2);
    }
    public function getSku($recommend_array,$count,$sku,$type){
        $product_sku = array();
        $sku = $sku?$sku:'\'\'';
        if($count){
            $sql = sprintf("SELECT accessories_sku as sku,cpf1.image,cpf1.color,cpf1.price,cpf1.`name`,cpsl.product_id,cpsl.parent_id,cpf1.entity_id FROM accessories_recommended AS ar INNER JOIN catalog_product_flat_1 AS cpf1 ON ar.accessories_sku = cpf1.sku AND cpf1.retired != 1  AND cpf1.sku not in (%s) INNER JOIN cataloginventory_stock_item AS csi ON cpf1.entity_id = csi.product_id AND csi.is_in_stock = 1 AND ar.recommended_type = '%s' inner JOIN catalog_product_super_link AS cpsl on cpsl.product_id = cpf1.entity_id group by accessories_sku limit %u",$sku,$type,$count);
            $product_sku = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        }
        $sku = $sku!='\'\''?$sku.',':'';
        if($product_sku){
            foreach ($product_sku as $key => $value){
                $entity_id = $value['entity_id'];
                $url_sql = "select request_path from url_rewrite where entity_id = $entity_id";
                $value['url'] = \app\components\helper\DbHelper::getSlaveDb()->createCommand($url_sql)->queryAll()[0]['request_path'];
                if(count($recommend_array) >=3){
                    break;
                }else{
                    $recommend_array[] = $value;
                    $sku .= '\''.$value['sku'].'\',';
                }
            }
            if(count($recommend_array) >=3){
                $count = 0;
            }else{
                $count = 3-count($recommend_array);
            }
        }
        $sku = rtrim($sku,',');
        return array('count'=>$count,"recommend_array"=>$recommend_array,"sku"=>$sku);
    }
    public function getaccessories_str(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $accessories_data = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["ACCESSORIES_ATTRIBUTE_SET_IDS"];
        $PROTECTIVE_ATTRIBUTE_SET_ID = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["PROTECTIVE_ATTRIBUTE_SET_IDS"];
        $accessories_data = array_merge($accessories_data,$PROTECTIVE_ATTRIBUTE_SET_ID);
        $accessories_str = '';
        foreach ($accessories_data as $key => $value){
            $accessories_str .= $value.',';
        }
        $accessories_str = rtrim($accessories_str,',');
        return $accessories_str;
    }
    //推荐配件第一版 end
    // 购物车页面显示当前购物车距离优惠金额所差金额接口
    public function actionGetAmountPrice(){
        $quote_id=json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $quote_id = $quote_id['quote_id'];
        $price = 0;//购物车总金额
        $Non_Rx_price = '89';//赠送Non-Rx Blue Light Glasses最低价格
        $Readers_price = '69';//赠送Single Vision Readers最低价格
        $difference_amount = 0;//距离最低眼镜价格所差金额
        $sql = "select subtotal,coupon_code from quote where entity_id = $quote_id";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        if($data){
            $price = $data['subtotal'];
            $coupon_code = $data['coupon_code'];
        }

        $is_login=false;
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $session=Yii::$app->session;
        $session->open();
        $s=Yii::$container->get("app\components\helper\CustomerHelper");
        $customer_id=$s->getSessionCustomerId();
        if (!empty($customer_id)) {
            $is_login=true;
        }

        if($price>0){
            if($Non_Rx_price>$price){
                $difference_amount = $Non_Rx_price-$price;
                $difference_amount = sprintf('%.2f',$difference_amount);
                die(json_encode(["code"=>201,"msg"=>"Want free 5 Pcs FFP2 face masks? Add <span>$$difference_amount</span> more. discount applied automatically Continue Shopping","data"=>array("price"=>$difference_amount)]));
            }
//            if($Readers_price>$price){
//                $difference_amount = $Readers_price-$price;
//                die(json_encode(["code"=>201,"msg"=>"Want free $5.95 Single Vision Readers? Add <span>$$difference_amount</span> more. Lens: 1.56 Mid-Index Lenses must be included, discount will be applied at checkout. ","data"=>array("price"=>$difference_amount)]));
//            }else{
//                if($Non_Rx_price>$price){
//                    $difference_amount = $Non_Rx_price-$price;
//                    die(json_encode(["code"=>202,"msg"=>"Want free $13.99 Non-Rx Blue Light Glasses? Add <span>$$difference_amount</span> more. Lens: 1.59 Polycarbonate Blue Blocking Lenses (Impact Resistant) must be included, discount will be applied at checkout. ","data"=>array("price"=>$difference_amount)]));
//                }
//            }
        }
        die(json_encode(["code"=>200,"msg"=>"","data"=>array("price"=>$difference_amount)]));
    }
}
