<?php
namespace app\modules\webyiiapi\controllers;

use \Aws\Sns\SnsClient;
use \Aws\Credentials\Credentials;
use Aws\Exception\AwsException;
use Yii;

class AwsController extends BaseController {
	//aws短信接口暂未调用
    public function actionAws()
    {
        $content = json_decode(Yii::$app->getRequest()->getRawBody(),true);
        $s3=isset($content['s3'])? $content['s3']:[];
        $region=isset($s3['region'])? $s3['region']:"us-east-1";
        try {
            $PhoneNumber = $content['PhoneNumber'];
            $message = $content['message'];
            $client = new SnsClient([
                'profile'=>"default",
                'region' => $region,
                'version' => 'latest'
            ]);
            $client->SetSMSAttributes([
                'attributes' => [
                    'DefaultSMSType' => 'Transactional',
                ]
            ]);
            $result = $client->publish([
                'Message' => $message,
                'PhoneNumber' => $PhoneNumber,
            ]);
            $data = array("send_detail"=>(array)$result,"phone"=>$PhoneNumber,"message"=>$message);
            header("Content-type:application/json;charset=utf-8");
            die(json_encode(["code"=>1,"message"=>"success","data"=>array($data)]));
        }catch (AwsException $e) {
            echo $e->getMessage();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
