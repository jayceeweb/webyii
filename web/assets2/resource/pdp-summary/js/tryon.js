
    var isIE = /msie|trident/g.test(window.navigator.userAgent.toLowerCase());
//判断当ie浏览器的轮播的左右点击箭头不兼容添加
    if (isIE) {
        $("#next_ie").html("&gt;");
        $("#prev_ie").html("&lt;");
    }
//试戴左上角弹出菜单展示
    $(".menu_class").on("click", function () {
        $(".select_pd").hide();
        if ($(".menu_list").attr("class").indexOf("hide") != -1) {
            $(".menu_list").removeClass("hide");
            $(".menu_class").addClass("menu_class_add");
        } else {
            $(".menu_list").addClass("hide");
            $(".menu_class").removeClass("menu_class_add");
        }
    });
    function Expandrecovery(ele, that) {
        if ($(ele).attr("class").indexOf("hide") != -1) {
            $(ele).removeClass("hide");
            $(that).find("span").css({
                "transform": "rotate(180deg)"
            })
        } else {
            $(ele).addClass("hide");
            $(that).find("span").css({
                "transform": "rotate(0deg)"
            })
        }
    }

//Adjustments 点击展开和收回
    $(".pd_slide_show,.pd_slide_Anti,.pd_slide_Mirror,.pd_slide_Dioptry,.pd_slide_Photochromic,.pd_slide_Thickness").on("click", function () {
        $(this).parents().siblings().find(".pd_slide1,.pd_slide2,.pd_slide_Dioptry_show,.pd_slide3,.pd_slide,.pd_slide_Thickness_show").addClass("hide");
        if ($(this).attr("class").indexOf("Anti") != -1) {
            Expandrecovery(".pd_slide1", this);
        } else if ($(this).attr("class").indexOf("Mirror") != -1) {
            Expandrecovery(".pd_slide2", this);
        } else if ($(this).attr("class").indexOf("Dioptry") != -1) {
            Expandrecovery(".pd_slide_Dioptry_show", this);
        } else if ($(this).attr("class").indexOf("Photochromic") != -1) {
            Expandrecovery(".pd_slide3", this);
        } else if ($(this).attr("class").indexOf("Thickness") != -1) {
            Expandrecovery(".pd_slide_Thickness_show", this);
        } else {
            Expandrecovery(".pd_slide", this);
        }
    });


    //  luanjue   tryOn 点击采集数据  start
    var smartCollectOn = $("#smartCollectOn").val();
    function ljTryOnCollect() {
        smart.init('collectionPdp',{
            loaded: function(sdk) {
                // luanjue_3  清除super_properties
                sdk.clear_event_super_properties();
            }
        });
        var ljProductColor = $(".btn-color-click.active").attr("simple_product_color");
        var ljColorValue = $(".btn-color-click.active").attr("color_value");
        var ljDestSku = $("label.active").attr("dest_sku");
        var ljPrice = $("label.active").attr("price");
        var ljSalesPrice = $("label.active").attr("sales_price");
        var ljLensType = $("label.active").attr("lens-type");
        var ljFrameGroup = $("label.active").attr("frame_group");
        ljFrameGroup = ljRemoveEnterOrSpace(ljFrameGroup) ;
        var ljSimpleUrl = $("label.active").attr("simple_url");
        var ljStockSku = $("#orderRx").attr("stock_sku");
        var reviewSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
        var ljEntityID = $("#orderRx").attr("simple_product_id");
        var ljConfigurableID = $("#orderRx").attr("configure_product_id");
        var ljConfigureProductSku = $("#orderRx").attr("configure_product_sku");
        var ljCategoryID = $("#orderRx").attr("category_id");
        var ljCatetoryType = $("#orderRx").attr("catetory_type");
        var ljStock = $("body").find("#orderRx").attr("ljstock");
        var ljFrameGroupHref = $("#orderRx").attr("frame_group_href");   // 镜框类别 rx_non_progressive_semi_rimless、goggles、full_rim

        var ljDataObj = {
            "productColor": ljProductColor,
            "productColorValue": ljColorValue,
            "destSku": ljDestSku,
            "price": ljPrice,
            "salesPrice": ljSalesPrice,
            "lensType": ljLensType,
            "frameGroup": ljFrameGroup,
            "frameGroupHref": ljFrameGroupHref,
            "simpleUrl": ljSimpleUrl,
            "stockSku": ljStockSku,
            "reviewSku":reviewSku,
            "entityID": ljEntityID,
            "configurableID": ljConfigurableID,
            "configureProductSku": ljConfigureProductSku,
            "categoryID": ljCategoryID,
            "catetoryType": ljCatetoryType,
            "stock": ljStock
        };
        smart.track_event("pdp_try_on_confirm", ljDataObj);
    }
    //  luanjue   tryOn 点击采集数据  end

//点击Try on 按钮触发7
    $("#btn-Try").on("click", function () {
        var catetory_type = $(this).attr("catetory_type");
        var sku = $(this).attr("sku");
        var currnt_sku = $(this).attr("currnt_sku");
        var flag_tryon = $(this).attr("flag_tryon");
        if (flag_tryon == "true") {
            if (isIE) {
                var tips_tryon = "<p class='tryon_class_ts'>Sorry！The browser you're using isn't compatible with the Try On. Try again with other browsers.</p>";
                layer.open({
                    shade: 0.5,
                    type: 1,
                    title: "Error",
                    // area: ['60%', 'auto'],
                    shadeClose: true, //点击遮罩关auto闭
                    content: tips_tryon,
                    btn: ["Confirm"],
                    resize: false,
                    move: false,
                    btn1: function (index, lay) {
                        // luanjue  tryOn数据采集  start
                        if(smartCollectOn == 1){
                            ljTryOnCollect();
                        }
                        // luanjue  tryOn数据采集  end

                        layer.close(index);
                    }
                });
                return;
            } else {
                //判断当前试戴是否支持网络摄像头
                if (!TryOnAPI.isAPI_Webcams_supported()) {
                    //判断当前是否是ios操作系统
                    var u = navigator.userAgent, app = navigator.appVersion;
                    var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //g
                    var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
                    if (isIOS) {
                        //这个是ios操作系统
                        tips_tryon = "<p class='tryon_class_ts'>Please use the Safari browser on the latest iOS for try-on function!</p>";
                    }else{
                        tips_tryon = "<p class='tryon_class_ts'>Current not supported Webcams supported</p>";
                    }
                    layer.open({
                        shade: 0.5,
                        type: 1,
                        title: "Error",
                        // area: ['60%', 'auto'],
                        shadeClose: true, //点击遮罩关auto闭
                        content: tips_tryon,
                        btn: ["Confirm"],
                        resize: false,
                        move: false,
                        btn1: function (index, lay) {
                            // luanjue  tryOn数据采集  start
                            if(smartCollectOn == 1) {
                                ljTryOnCollect();
                            }
                            // luanjue  tryOn数据采集  end

                            layer.close(index);
                        }
                    });
                    return;
                }
            }
            $("#try_without_pd,#next_tryon").attr("catetory_type", catetory_type);
            $("#try_without_pd,#next_tryon").attr("currnt_sku", currnt_sku);
            $("#try_without_pd,#next_tryon").attr("sku", sku);
            $(".tryon_class").show();
        } else {
            var tryon_html = "<p class='tryon_class_ts'>This frame is not available for the viritual try on yet.  Try again soon! </p>";
            layer.open({
                shade: 0.5,
                type: 1,
                title: "Notice",
                // area: ['60%', 'auto'],
                shadeClose: true, //点击遮罩关auto闭
                content: tryon_html,
                btn: ["Confirm"],
                resize: false,
                move: false,
                btn1: function (index, lay) {
                    // luanjue  tryOn数据采集  start
                    if(smartCollectOn == 1) {
                        ljTryOnCollect();
                    }
                    // luanjue  tryOn数据采集  end

                    layer.close(index);
                },
            })
        }
    });
//试戴页面pd值默认插入
    var pd_option = "<option selected='selected' value=''>Select</option>";
    var tryon_option = "";
    for (var i = 40; i <= 79; i += 0.5) {
        var str = i + "";
        if (str.indexOf(".") == -1) {
            str = str + ".0"
        }
        pd_option += "<option value=" + str + ">" + str + "</option>";
        tryon_option += "<option value=" + str + ">" + str + "</option>";
    }
    $("#pd_select").html(pd_option);
    $("#tryon_selected").html(tryon_option);
//当前pd下拉框选择(单pd)【Enter your pupillary distance (PD) for an even better fit界面】
    $("#pd_select").on("change", function () {
        var pd_select_val = $(this).val();
        if (pd_select_val) {
            $("#next_tryon").attr("pd_text", pd_select_val);
            pd_select_val = parseFloat(pd_select_val);
            pd_select_val = pd_select_val / 10;
            $("#next_tryon").attr("pd_num", pd_select_val);
            $("#next_tryon").removeClass("next_jx");
            $("#try_without_pd").addClass("next_jx");
        } else {
            $("#next_tryon").addClass("next_jx");
            $("#try_without_pd").removeClass("next_jx");
        }
    });
//点击set_pd  弹出pd切换框【试戴中】
    $("#pd_xz_class").on("click", function () {
        $(".menu_list").addClass("hide");
        $(".menu_class").removeClass("menu_class_add");
        if ($(window).width() >= 769) {
            if ($("#pd_xz_class").text().indexOf("SET PD") != -1) {
                $("#pd_xz_class").text("CLOSE PD");
                $(".select_pd").show();
            } else {
                $("#pd_xz_class").text("SET PD");
                $(".select_pd").hide();
            }
        } else {
            if ($(this).find("img").attr("src").indexOf("checked") == -1) {
                $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/setPd_checked.png");
                $(".select_pd").show();
            } else {
                $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/setPd.png");
                $(".select_pd").hide();
            }
        }
    });


//带有pd点击下一步【next按钮】
    $("#next_tryon").on("click", function () {
        var catetory_type = $(this).attr("catetory_type");
        var currnt_sku = $(this).attr("currnt_sku");
        var sku = $(this).attr("sku");
        var pd_num = $(this).attr("pd_num");
        var pd_text = $(this).attr("pd_text");
        var s_left = $(this).attr("s_left");
        var s_right = $(this).attr("s_right");
        var color = "";
        if (catetory_type == "sunglasses") {
            color = sku.substr(sku.length - 1, 1);
        }
        var pd_obj = {};
        if (pd_num) {
            pd_num = parseFloat(pd_num);
            pd_obj.pd = pd_num;
            pd_obj.falg_ds = false;
            pd_obj.pd_text = pd_text
        }
        var current_hei = document.documentElement.clientHeight;
        var current_width = document.documentElement.clientWidth;
        $(".smak,.tryon_box_fex,#pd_xz_class").show();
        $(".tryon_box_fex_bottom").css({
            opacity: 1
        });
        $(".select_tryon,.close_pd_select").hide();
        tryon_fn(currnt_sku, catetory_type, color, pd_obj, current_hei, current_width, sku, true);
    });
//没有pd点击下一步
    $("#try_without_pd").on("click", function () {
        var catetory_type = $(this).attr("catetory_type");
        var currnt_sku = $(this).attr("currnt_sku");
        var sku = $(this).attr("sku");
        var color = "";
        if (catetory_type == "sunglasses") {
            color = sku.substr(sku.length - 1, 1);
        }
        var current_hei = document.documentElement.clientHeight;
        var current_width = document.documentElement.clientWidth;
        $(".smak,.tryon_box_fex").show();
        $(".tryon_box_fex_bottom").css({
            opacity: 1
        });
        $(".select_tryon,.close_pd_select").hide();
        tryon_fn(currnt_sku, catetory_type, color, {}, current_hei, current_width, sku, true);
    });


//关闭试戴事件
    $(".close_tryon").on("click", function () {
        $("#close_glasses,.close_pd_select").show();
        $("#fp_tryon_one img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_one.png");
        if ($(window).width() <= 768) {
            $("#fp_tryon img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_two_mobel_checked.png");
        } else {
            $("#fp_tryon img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_two_checked.png");
        }
        $("#close_glasses img").attr("src", "/assets2/resource/pdp-summary/images/glasses.png");
        $("#close_glasses").attr("title", "Remove the glasses");
        $("#photograph_glasses img").attr("src", "/assets2/resource/pdp-summary/images/photograph.png");
        $("#photograph_glasses").attr("title", "Take a picture for try-on");
        $(".tryon_box_fex_bottom").css({
            opacity: 1
        });
        $(".menu_class").removeClass("hide");
        $("#tryon,.select_pd,.pd_cd,.tryon_class,.tryon_box_fex").hide();
        $(".tryon_box_fex_bottom").css({
            opacity: 0
        });
        tryon_fn("close");
        $(".select_tryon").show();
        $(".tryon-list").addClass("tryon-list-jx");
        document.querySelector(".tryon_class .tryon_box").innerHTML = "";
    });
// Enter your pupillary distance (PD) for an even better fit 界面关闭事件
    $(".close_pd_select").on("click", function () {
        $(".tryon_class").hide();
    });
//pc端拖动(Adjustments) Start
    function Adjustments_move(ele, ele1, ele2, fn) {
        var scroll = document.getElementById(ele);
        var bar = document.getElementById(ele1);
        var mask = document.getElementById(ele2);
        var barleft = 0;
        bar.onmousedown = function (event) {
            var event = event || window.event;
            var leftVal = event.clientX - this.offsetLeft;
            var that = this;
            // 拖动一定写到 down 里面才可以
            document.onmousemove = function (event) {
                fn(event, barleft, leftVal, scroll, bar, mask, that);
            }
        };
        document.onmouseup = function () {
            document.onmousemove = null; //弹起鼠标不做任何操作
        };
    }

//pc端拖动(Adjustments) End
//移动端拖动(Adjustments) Start
    function Adjustments_move_mobel(ele, ele1, fn) {
        var scale = function (btn, bar, title) {
            this.btn = document.getElementById(btn);
            this.bar = document.getElementById(bar);
            this.step = this.bar.getElementsByTagName("div")[0];
            this.init();
        };
        scale.prototype = {
            init: function () {
                var f = this, g = document, b = window, m = Math;
                f.btn.ontouchstart = function (e) {
                    var x = (e || b.event).touches[0].clientX;
                    var l = this.offsetLeft;
                    var max = f.bar.offsetWidth - this.offsetWidth;
                    g.ontouchmove = function (e) {
                        var thisX = (e || b.event).touches[0].clientX;
                        var to = m.min(max, m.max(-2, l + (thisX - x)));
                        f.btn.style.left = to + 'px';
                        f.ondrag(m.round(m.max(0, to / max) * 100), to);
                        b.getSelection ? b.getSelection().removeAllRanges() : g.selection.empty();
                    };
                    g.ontouchend = new Function('this.οnmοusemοve=null');
                };
            },
            ondrag: function (pos, x) {
                fn(pos, x);
            }
        }
        new scale(ele, ele1);
    }

//移动端拖动(Adjustments) ENd
//试戴眼镜上下拖动查看事件函数
    function glassesMove(renderer, domElement, renderer1) {
        var glassesSelected = false;
        const mouseTest = {
            glasses: true
        };
        const offset = {
            position: 0
            , rotation: 0
        };

        //Position(PC) : 拖动
        Adjustments_move("scroll", "bar", "mask", function (event, barleft, leftVal, scroll, bar, mask, that) {
            var event = event || window.event;
            barleft = event.clientX - leftVal;
            if (barleft < 0)
                barleft = 0;
            else if (barleft > scroll.offsetWidth - bar.offsetWidth)
                barleft = scroll.offsetWidth - bar.offsetWidth;
            mask.style.width = barleft + 'px';
            that.style.left = barleft + "px";
            //防止选择内容--当拖动鼠标过快时候，弹起鼠标，bar也会移动，修复bug
            window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
            var num = barleft / (scroll.offsetWidth - bar.offsetWidth);
            offset.position = num;
            offset.rotation = 0;

            if (renderer1) {
                renderer.setGlassesOffset(offset);
                renderer1.setGlassesOffset(offset);
            } else {
                renderer.setGlassesOffset(offset);
            }

        });

        //Rotation(PC) : 拖动
        Adjustments_move("scroll1", "bar1", "mask1", function (event, barleft, leftVal, scroll, bar, mask, that) {
            var event = event || window.event;
            barleft = event.clientX - leftVal;
            if (barleft < 0)
                barleft = 0;
            else if (barleft > scroll.offsetWidth - bar.offsetWidth)
                barleft = scroll.offsetWidth - bar.offsetWidth;
            mask.style.width = barleft + 'px';
            that.style.left = barleft + "px";
            //防止选择内容--当拖动鼠标过快时候，弹起鼠标，bar也会移动，修复bug
            window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
            var num = barleft / (scroll.offsetWidth - bar.offsetWidth);
            offset.position = 0;
            offset.rotation = num * 100;
            if (renderer1) {
                renderer.setGlassesOffset(offset);
                renderer1.setGlassesOffset(offset);
            } else {
                renderer.setGlassesOffset(offset);
            }
        });


        //Position(mobel) : 拖动
        Adjustments_move_mobel('btn', 'touch_bar', function (pos, x) {
            offset.position = pos / 100;
            offset.rotation = 0;
            if (renderer1) {
                renderer.setGlassesOffset(offset);
                renderer1.setGlassesOffset(offset);
            } else {
                renderer.setGlassesOffset(offset);
            }
        });

        //Rotation(mobel) : 拖动
        Adjustments_move_mobel('btn1', 'touch_bar1', function (pos, x) {
            offset.position = 0;
            offset.rotation = pos;
            if (renderer1) {
                renderer.setGlassesOffset(offset);
                renderer1.setGlassesOffset(offset);
            } else {
                renderer.setGlassesOffset(offset);
            }
        });


        //Adding interactions
        domElement.addEventListener('mousedown', function (e) {
            //testing if the user's mouse is over the glasses
            glassesSelected = renderer.raycast(e.offsetX, e.offsetY, mouseTest);
            document.body.style.cursor = glassesSelected ? 'grabbing' : 'default';
        });
        domElement.addEventListener('mousemove', function (e) {
            if (glassesSelected) {
                document.body.style.cursor = 'grabbing';
                //interaction: modifying the glasses offset
                if (e.buttons == 1) {
                    offset.position = Math.max(0.0, Math.min(offset.position + e.movementY * 0.005, 1.0));
                    if (renderer1) {
                        renderer.setGlassesOffset(offset);
                        renderer1.setGlassesOffset(offset);
                    } else {
                        renderer.setGlassesOffset(offset);
                    }
                } else if (e.buttons == 2) {
                    //Rotation offset change (on x axis between -25 to +35 deg)
                    //Same thing here about clamping the value.
                    offset.rotation = Math.max(-25.0, Math.min(offset.rotation + e.movementY * 0.1, 35.0));
                    if (renderer1) {
                        renderer.setGlassesOffset(offset);
                        renderer1.setGlassesOffset(offset);
                    } else {
                        renderer.setGlassesOffset(offset);
                    }
                }
            } else {
                //suggesting the interaction availability
                const isOverGlasses = renderer.raycast(e.offsetX, e.offsetY, mouseTest);
                document.body.style.cursor = isOverGlasses ? 'grab' : 'default';
            }
        });
        domElement.addEventListener('mouseup', function (e) {
            glassesSelected = false;
            const stillOverGlasses = renderer.raycast(e.offsetX, e.offsetY, mouseTest);
            document.body.style.cursor = stillOverGlasses ? 'grab' : 'default';
        });
        domElement.addEventListener('contextmenu', function (e) {
            e.preventDefault();
        });
    }

//太阳镜参数判断颜色值
    function sunglasse_color(color_val) {
        var color_obj = {};
        if (color_val == "G") { //灰色
            color_obj = {
                top_left: {r: 0.1725, g: 0.1882, b: 0.1843}
                , top_right: {r: 0.1725, g: 0.1882, b: 0.1843}
                , bottom_right: {r: 0.1725, g: 0.1882, b: 0.1843}
                , bottom_left: {r: 0.1725, g: 0.1882, b: 0.1843}
            }
        }
        else if (color_val == "E") {  //绿色
            color_obj = {
                top_left: {r: 0.2823, g: 0.3216, b: 0.2745}
                , top_right: {r: 0.2823, g: 0.3216, b: 0.2745}
                , bottom_right: {r: 0.2823, g: 0.3216, b: 0.2745}
                , bottom_left: {r: 0.2823, g: 0.3216, b: 0.2745}
            }
        } else if (color_val == "B") {  //棕色
            color_obj = {
                top_left: {r: 0.4353, g: 0.3216, b: 0.2627}
                , top_right: {r: 0.4353, g: 0.3216, b: 0.2627}
                , bottom_right: {r: 0.4353, g: 0.3216, b: 0.2627}
                , bottom_left: {r: 0.4353, g: 0.3216, b: 0.2627}
            }
        }
        return color_obj;
    }


//试戴页面【针对是否为太阳镜函数】
    function sunglasseSwitch(catetory_type, color, renderer, mirror_flag, parames_flag) {
        if (catetory_type == "sunglasses" || mirror_flag) {
            var color_obj = sunglasse_color(color);
            var customization = {
                frame: {
                    // color:{ r:0.75, g:1.0, b:0.75 }
                }
                , lenses: {
                    mirror: false
                    , gradient: color_obj,
                    //眼镜厚度
                    thicknessCorrection: 0,
                    //眼镜透明度
                    opacity: 0.9,
                    // mirror:false,
                    dioptry: 0,
                    antiReflectiveCoating: true
                }
            };
            if (parames_flag) {
                renderer.setGlassesCustomization(null);
            } else {
                renderer.setGlassesCustomization(customization);
            }
        }
    }

    // console.log("Webcams supported: " + TryOnAPI.isAPI_Webcams_supported());
    // console.log("Rendering supported: " + TryOnAPI.isAPI_WebGL_supported());
    // console.log("WebAssembly tracking supported: " + TryOnAPI.isAPI_WebAssembly_supported());
    //试戴启动函数
    function tryon_fn(currnt_sku, catetory_type, color, pd_obj, current_hei, current_width, sku, fp_tryon) {
        fp_tryon = true;
        $("#fp_tryon").attr("currnt_sku", currnt_sku);
        $("#fp_tryon").attr("color", color);
        $("#fp_tryon").attr("pd_obj", pd_obj ? JSON.stringify(pd_obj) : pd_obj);
        $("#fp_tryon").attr("sku", sku);
        document.querySelector(".tryon_box").innerHTML = "";
        if (catetory_type == "sunglasses") {
            $(".anti_mirror_dioptry_Pho_thickness").hide();
        } else {
            $(".anti_mirror_dioptry_Pho_thickness").show();
        }
        if ($("#first_pd").length >= 1) {
            $("#first_pd").remove();
        }
        //试戴页面中切换产品的轮播展示方式函数
        function Tryon_Resolution(num, father_num) {
            if ($(window).width() >= 769) {
                $(".tryon_box_fex_bottom .swiper-container").css({
                    width: num
                });
                $("#tryon-father").css({
                    width: father_num
                });
            }
        }

        var currnt_sku_two = currnt_sku;
        var color_two = color;
        //判断当前试戴页面中切换产品的轮播展示方式
        $("#swiper-container_tryon .swiper-slide").each(function (idx, ele) {
            var current_list_sku = $(ele).attr("sku");
            if (current_list_sku == sku) {
                var num_tryon = 0;
                var zc_tryon_len = $("#swiper-container_tryon .swiper-slide").length;
                if (zc_tryon_len >= 3) {
                    Tryon_Resolution("700px", "850px");
                    num_tryon = 3;
                } else if (zc_tryon_len == 2) {
                    $(".swiper-button-prev1,.swiper-button-next1").remove();
                    Tryon_Resolution("450px", "500px");
                    num_tryon = 2;
                } else {
                    $("#fp_tryon_one,#fp_tryon").hide();
                    $(".swiper-button-prev1,.swiper-button-next1").remove();
                    if ($(window).width() >= 769) {
                        $(".tryon_box_fex_bottom .swiper-container").css({
                            width: "200px"
                        });
                        $("#tryon-father").css({
                            width: "300px"
                        });
                    } else if ($(window).width() <= 768) {
                        $(".tryon_box_fex_bottom .swiper-container").css({
                            width: "200px"
                        });
                        $("#tryon-father").css({
                            width: "300px"
                        });
                    } else if ($(window).width() <= 700) {
                        $(".tryon_box_fex_bottom .swiper-container").css({
                            width: "150px"
                        });
                        $("#tryon-father").css({
                            width: "300px"
                        });
                    }
                    num_tryon = 1;
                }
                $(ele).addClass("tryon_checked");
                $("#swiper-container_tryon .swiper-wrapper").prepend($(ele));
                var img_ele_src = $(ele).next().find("img").attr("src");
                var next_stock_sku = $(ele).next().attr("stock_sku");
                var next_sku = $(ele).next().attr("sku");
                if (fp_tryon) {
                    if (catetory_type == "sunglasses") {
                        color_two = next_sku.substr(next_sku.length - 1, 1);
                    }
                    currnt_sku_two = next_stock_sku;
                }
                var galleryThumbs = new Swiper('.tryon_box_fex_bottom .swiper-container', {
                    initialSlide: 0,
                    spaceBetween: 20,
                    slidesPerView: num_tryon,
                    navigation: {
                        nextEl: '.swiper-button-prev1',
                        prevEl: '.swiper-button-next1',
                    },
                    observer: true,//修改swiper自己或子元素时，自动初始化swiper
                    observeParents: false,//修改swiper的父元素时，自动初始化swiper
                });
            } else {
                $(ele).removeClass("tryon_checked");
            }
        });
        const camConstraints = {
            width: {min: 640, ideal: 1280, max: 1920},
            height: {min: 480, ideal: 720, max: 1080},
            facingMode: {ideal: 'user'}
        };
        // const rendererSize = {width: 50, height: 100};
        const rendererSize1 = {width: current_width, height: current_hei};
        const apiConfig = new TryOnAPI.Config({
            serverURL: 'https://tryon.cosium.com/setup/api/try-on',
            assetsFolder: '/assets2/resource/pdp-summary/js/assets/',
            licences: [
                '381-458-099-397-684-299-868-932-034-557-686.vlc', //TryOn License for domain 'localhost'
                '381-458-069-367-684-299-868-932-470-119-053.vlc'
            ]
        });
        var vidio = document.createElement('video');
        vidio.playsInline = true;
        vidio.style = 'position: absolute; width:1px; height:1px; top:0; left:0';
        const videoElement = document.querySelector(".tryon_box").appendChild(vidio);
        // document.querySelector(".tryon_box_two").appendChild(vidio);
        const imageSource = new TryOnAPI.ImageSource(vidio);
        const tracker = new TryOnAPI.Tracker(apiConfig, imageSource);
        const renderer = new TryOnAPI.Renderer(apiConfig, imageSource);
        renderer.setFrameAutoResizing(true);
        imageSource.autoUpdate(true);
        var renderer1 = new TryOnAPI.Renderer(apiConfig, imageSource);
        const analysis =new TryOnAPI.UserAnalysis();
        // [测量pd临时注释]
        // if(fp_tryon){
        //     $("#Measuring_PD").addClass("hide");
        // }else{
        //     $("#Measuring_PD").removeClass("hide");
        // }
        document.querySelector(".tryon_box").appendChild(renderer.getDomElement());
        document.querySelector(".tryon_box").appendChild(renderer1.getDomElement());
        // renderer.setStyleSize(rendererSize["width"]+"%", rendererSize["height"]+"%");
        renderer.setFrameSize(rendererSize1.width, rendererSize1.height);
        renderer1.setFrameSize(rendererSize1.width / 2, rendererSize1.height);
        $(".tryon_box canvas:nth-of-type(2)").addClass("canvas_two hide");
        const domElement = renderer.getDomElement();
        var requestedIPD = 0;
        var ipdCanBeSet = false;
        var ipdCanBeSetReg = null;
        //试戴中设置pd函数
        function updateIPD(value) {
            if (ipdCanBeSet) {
                tracker.setIPD(value);
            } else {
                if (ipdCanBeSetReg == null) {
                    ipdCanBeSetReg = tracker.onDetectedUserNbChanged.reg(function (nbTracked) {
                        if (nbTracked > 0) {
                            ipdCanBeSet = true;
                            tracker.setIPD(requestedIPD);
                            ipdCanBeSetReg.dispose();
                        }
                    });
                }
                requestedIPD = value;
            }
        }


        //Starting the whole initialization phase
        Promise.all([
            TryOnAPI.Webcams.initAndStart().then(function (w) {
                const webcamAttach = w.attach(videoElement);
                if (currnt_sku == "close") {
                    w.stop();
                    return false;
                } else {
                    w.attach(vidio)
                }
            }),
            Promise.all([
                tracker.init().then(function (t) {
                    t.start()
                }),
                renderer.init().then(function (r) {
                    r.start();
                    imageSource.setMirrored(true);
                }),
                fp_tryon ? renderer1.init().then(function (r) {
                    r.stop();
                    imageSource.setMirrored(true);
                }) : "",
            ]).then(function (){
                return renderer.setTracker(tracker);
            }).then(function () {
                //试戴测量pd只针对单屏幕生效[未启动]
                // if(!fp_tryon){
                //     //Calibration process
                //     const calibration = new TryOnAPI.Calibration();
                //     //Overlay drawing
                //     const ctx =  document.querySelector(".tryon_box").appendChild(document.createElement('canvas')).getContext('2d');
                //     ctx.canvas.id="Measuring_PD_canvas";
                //     ctx.canvas.style.position = 'absolute';
                //     ctx.canvas.style.top = renderer.getDomElement().offsetTop +'px';
                //     ctx.canvas.style.left = renderer.getDomElement().offsetLeft +'px';
                //     ctx.canvas.width = renderer.getFrameWidth();
                //     ctx.canvas.height = renderer.getFrameHeight();
                //     ctx.canvas.style.display="none";
                //     //Hints
                //     const faceZone = { x:0 , y:0 , width:0 , height:0 };
                //     const cardZone = { x:0 , y:0 , width:0 , height:0 };
                //     const cardROI = { x:0 , y:0 , width:0 , height:0 };
                //     //Compute the hints according
                //     renderer.getCalibrationHints( faceZone, cardZone, cardROI );
                //     //Create according shapes
                //     const cardRect = new Path2D();
                //     cardRect.rect(cardZone.x,cardZone.y,cardZone.width,cardZone.height);
                //     const faceOval = new Path2D();
                //     faceZone.width *= 0.5;
                //     faceZone.height *= 0.5;
                //     faceOval.ellipse(faceZone.x + faceZone.width, faceZone.y + faceZone.height, faceZone.width, faceZone.height, 0, 0, 2*Math.PI);
                //
                //     //Hints display and update
                //     function drawHints( calib ){
                //         ctx.clearRect(0,0,ctx.canvas.width,ctx.canvas.height);
                //         //face oval
                //         ctx.strokeStyle = calib.isStarted()
                //             ?calib.isUserDetected()
                //             ?calib.isUserFacing()
                //             ?'green'
                //             :'orange'
                //             :'red'
                //             :'grey';
                //         ctx.stroke(faceOval);
                //         //card rect
                //         ctx.strokeStyle = calib.isStarted()
                //             ?calib.isCardDetected()
                //             ?'green'
                //             :'red'
                //             :'grey';
                //         ctx.stroke(cardRect);
                //     };
                //     drawHints( calibration );//first draw
                //     calibration.onDetectionChanged.reg( drawHints );//draw update
                // }
                // $("#start_PD").on("click",function(){
                //     calibration.start( tracker, 7000, cardROI ).then(function(ipdCM){
                //         console.log('IPD: '+ipdCM+' cm');
                //     });
                //
                //
                //     calibration.start( tracker, 7000, cardROI ).then(function( ipdCM ){
                //         console.log('Done.');
                //         console.log('IPD: '+ipdCM+' cm');
                //         //the tracker is automaticaly adjusted -> tracker.getIPD() == ipdCM
                //     }).catch(function(e){
                //             console.log( 'Calibration process rejected.' );
                //         console.error(e);
                //     });
                //     console.log('Calibration process started.');
                // });
                // $("#stop_PD").on("click",function(){
                //     calibration.stop( 'This process has to be stopped.' );
                // });


                $(".Maximum_users div").on("click", function () {
                    var current_user = $(this).index() + 1;
                    $(this).addClass("Maximum_users_frist").siblings().removeClass("Maximum_users_frist");
                    tracker.start(current_user)
                });
                if (fp_tryon) {
                    glassesMove(renderer, domElement, renderer1);
                } else {
                    glassesMove(renderer, domElement);
                }


                //点击切换镜片
                $("#swiper-container_tryon .swiper-slide").unbind("click");
                $("#swiper-container_tryon .swiper-slide").on("click", function () {
                    var current_name_class = $(this).attr("class") ? $(this).attr("class") : "swiper-slide";
                    $("#close_glasses img").attr("src", "/assets2/resource/pdp-summary/images/glasses.png");
                    var catetory_type = $("#btn-Try").attr("catetory_type");
                    var currnt_sku = $(this).attr("stock_sku");
                    var sku = $(this).attr("sku");
                    $(this).addClass("tryon_checked").siblings().removeClass("tryon_checked");
                    var color = "";
                    if (catetory_type == "sunglasses") {
                        color = sku.substr(sku.length - 1, 1);
                    }


                    // var next_stock_sku = $(this).next().attr("stock_sku");
                    // var next_sku = $(this).next().attr("sku");
                    // if (catetory_type == "sunglasses") {
                    //     color_two = next_sku.substr(next_sku.length - 1, 1);
                    // }
                    // currnt_sku_two = next_stock_sku;

                    $(".pd_slide3").attr("color", color);
                    $("#close_glasses").attr("color", color);
                    $("#close_glasses").attr("currnt_sku", currnt_sku);
                    $("#fp_tryon").attr("currnt_sku", currnt_sku);
                    $("#fp_tryon").attr("color", color);
                    $("#fp_tryon").attr("sku", sku);
                    renderer.setGlasses(currnt_sku),
                        sunglasseSwitch(catetory_type, color, renderer);

                   var falg_onDetectedUserNbChanged_click = true;
                    //Analysis events registration
                    analysis.onNewUser.reg(function(userAnalysisData) {
                        if(falg_onDetectedUserNbChanged_click){
                            falg_onDetectedUserNbChanged_click = false;
                            //屏幕实时截图
                            renderer.getCapture()
                                .then(function(imgElement){
                                    function isJSON(str) {
                                        if (typeof str == 'string') {
                                            try {
                                                var obj = JSON.parse(str);
                                                if (typeof obj == 'object' && obj) {
                                                    return true;
                                                } else {
                                                    return false;
                                                }
                                            } catch (e) {
                                                console.log('error：' + str + '!!!' + e);
                                                return false;
                                            }
                                        }
                                    }
                                    // 获取到当前用户的实时抓取图片
                                    if(imgElement){
                                        imgElement = imgElement.getAttribute("src");
                                        var datas = {
                                            "data":{
                                                "sku":currnt_sku,
                                                "content_type":"png",
                                                "obj_type":"tryon",
                                                "obj_id":$("#orderRx").attr("simple_product_id"),
                                                "customer_id":0,
                                                "profile_id":0,
                                                "profile_name":"",
                                                "additional_data":JSON.stringify(userAnalysisData)
                                            },
                                            "s3":{
                                                "region":"us-east-1",
                                                "bucket":"pg-product-images"
                                            },
                                            "s":imgElement
                                        };
                                        var logincustomer = $("body").attr("logincustomer");
                                        if(isJSON(logincustomer)){
                                            logincustomer = JSON.parse(logincustomer);
                                            if(logincustomer["customer"]["islogged"]){
                                                datas["data"]["profile_id"]=logincustomer["customer"]["current_profile_id"];
                                                datas["data"]["profile_name"]=logincustomer["customer"]["current_profile_name"];
                                            }else{
                                                if (window.localStorage && window.localStorage.length) {
                                                    var defaultLoca = {
                                                        global_closed_message: "[]"
                                                    };
                                                    defaultLoca["guest-profile"] = "{\"nickname\":\"NEW NAME\",\"gender\":\"2\",\"age_group\":\"\"}";
                                                    defaultLoca["guest-profile"] = defaultLoca["guest-profile"];
                                                    var myLocal = window.localStorage['mage-cache-storage-pg'] && window.localStorage['mage-cache-storage-pg'] != 'undefined' && window.localStorage['mage-cache-storage-pg'].length ? $.parseJSON(window.localStorage['mage-cache-storage-pg']) : {};
                                                    if (myLocal['guest-profile'] == undefined) {
                                                        myLocal = defaultLoca;
                                                    }
                                                    if(myLocal){
                                                        if(isJSON(myLocal["guest-profile"])){
                                                            var json_guest=myLocal["guest-profile"];
                                                            json_guest = JSON.parse(json_guest);
                                                            datas["data"]["profile_name"]=json_guest["nickname"];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        $.ajax({
                                            url:"/webyiiapi/tryon/image-to-s3.html",
                                            type: "POST",
                                            contentType: "application/json",
                                            dataType: "json",
                                            data:JSON.stringify(datas),
                                            success:function(res){}
                                        });
                                    }
                                });
                        }
                    });
                    analysis.start(tracker, -1);
                });



                // facebook 分享
                var facebook_app_id = $(".share_f").attr("facebookAppId");
                var faceFlag = true;
                // 定义facebook对象
                window.fbAsyncInit = function() {
                    FB.init({
                        appId            : facebook_app_id,
                        autoLogAppEvents : true,
                        xfbml            : true,
                        version          : 'v3.2'
                    });
                };
                console.log(facebook_app_id,'facebook_app_id')
                $(".share_f").click(function(){
                    $(".loading-jz").show()
                    $(".share_f").addClass('share_f_noclick')
                    if(faceFlag){
                        faceFlag = false
                        renderer.getCapture().then(function(imgElement){
                            if(imgElement){
                                imgElement = imgElement.getAttribute("src");
                                var datas = {
                                    "data":{
                                        "sku":$("#btn-Try").attr("currnt_sku"),
                                        "content_type":"png",
                                        "obj_type":"tryon",
                                        "obj_id":$("#orderRx").attr("simple_product_id"),
                                        "customer_id":0,
                                        "profile_id":0,
                                        "profile_name":"",
                                        "additional_data":""
                                    },
                                    "s3":{
                                        "region":"us-east-1",
                                        "bucket":"pg-product-images"
                                    },
                                    "s":imgElement
                                }
                                $.ajax({
                                    url:"/webyiiapi/tryon/image-to-s3.html",
                                    type: "POST",
                                    contentType: "application/json",
                                    dataType: "json",
                                    data:JSON.stringify(datas),
                                    success:function(res){
                                        faceFlag = true
                                        $(".loading-jz").hide()
                                        $(".share_f").removeClass('share_f_noclick')
                                        if(res.code==0){
                                            var data = res.data
                                            var imgSrc = data.server_domain + data.file_path;
                                            $(".faceImg").attr('src',imgSrc)
                                            $(".faceImgBox").show()
                                            $(".share_f_i").click(function(){
                                                if (FB) {
                                                    FB.ui({
                                                        method: 'share',
                                                        mobile_iframe: true,
                                                        href: imgSrc
                                                    }, function (response) {
                                                        if (response && !response.error_message) {}
                                                    });
                                                }
                                            })
                                            console.log(res,imgSrc,'res---------')
                                        }
                                    }
                                });
                            }
                        })
                    }
                });
                $(".closeFace").click(function(){
                    $(".faceImgBox").hide()
                })

                //试戴双屏展示点击事件
                $("#fp_tryon").on("click", function () {
                    var current_hei = document.documentElement.clientHeight;
                    var current_width = document.documentElement.clientWidth;
                    if ($(this).find("img").attr("src").indexOf("checked") != -1) {
                        if ($(window).width() <= 768) {
                            $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_two_mobel.png");
                        } else {
                            $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_two.png");
                        }
                        $("#fp_tryon_one img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_one_checked.png");
                        $(".canvas_two").removeClass("hide");
                        if ($(window).width() >= 769) {
                            $(".tryon_box").addClass("tryon_box_fp");
                            $(".tryon_box canvas").attr("width", current_width / 2);
                        } else {
                            $(".tryon_box").removeClass("tryon_box_fp");
                            $(".tryon_box canvas").attr("height", current_hei / 2);
                        }

                    }
                    renderer1.start();
                });


                //试戴单屏展示点击事件
                $("#fp_tryon_one").on("click", function () {
                    var current_hei = document.documentElement.clientHeight;
                    var current_width = document.documentElement.clientWidth;
                    if ($(this).find("img").attr("src").indexOf("checked") != -1) {
                        $(".tryon_box").removeClass("tryon_box_fp");
                        $(".canvas_two").addClass("hide");
                        $(".tryon_box canvas").attr("width", current_width);
                        $(".tryon_box canvas").attr("height", current_hei);
                        $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_one.png");
                        if ($(window).width() <= 768) {
                            $("#fp_tryon img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_two_mobel_checked.png");
                        } else {
                            $("#fp_tryon img").attr("src", "/assets2/resource/pdp-summary/images/fp_tryon_two_checked.png");
                        }
                    }
                    renderer1.stop();
                });


                //开启测试pd [测量pd临时注释]
                // $("#Measuring_PD").on("click",function(){
                //     var currnt_sku = $("#fp_tryon").attr("currnt_sku");
                //     var color = $("#fp_tryon").attr("color");
                //
                //
                //     function Measuring_PD_fn_bus_xz(renderer,currnt_sku,catetory_type,color){
                //         $("#Measuring_PD_canvas").css({
                //             display:"none"
                //         });
                //         $(".menu_class,.icon_glasses,#pd_xz_class,#tryon-father").removeClass("hide");
                //         $(".box_class_pd").addClass("hide");
                //         renderer.setGlasses(currnt_sku),
                //             sunglasseSwitch(catetory_type,color,renderer)
                //     }
                //     function Measuring_PD_fn_bus_wxz(renderer){
                //         $("#Measuring_PD_canvas").css({
                //             display:"block"
                //         });
                //         $(".menu_class,.icon_glasses,#pd_xz_class,#tryon-father").addClass("hide");
                //         $(".box_class_pd").removeClass("hide");
                //         renderer.setGlasses(null);
                //         var tips_tryon = "<p class='tryon_class_ts'>The user is invited to stand still, to look at the camera and to place a card with a magnetic stripe on his forehead. The card should fit within a specified region ( here, the rectangle, aka the `cardROI` variable ). When ready, the user have to click on the start button. The process is set with a maximum computation time of 7 seconds, and will try to estimate the user's InterPupillary Distance. To make a measurement, the user and the magnetic stripe of the card have to be detected at the same time and the magnetic band have to take at least 60% of the provided region. The user must be wary of the orientation of the card (magnetic stripe on the bottom) as well as the possible glare on the magentic stripe since it is a black horizontal rectangle</p>";
                //         layer.open({
                //             shade: 0.5,
                //             type: 1,
                //             title: "help",
                //             area: ['80%', 'auto'],
                //             shadeClose: true, //点击遮罩关auto闭
                //             content:tips_tryon,
                //             btn: ["Confirm"],
                //             resize: false,
                //             move: false,
                //             btn1: function (index, lay) {
                //                 layer.close(index);
                //             }
                //         });
                //     }
                //
                //     if($(window).width()>=769){
                //         if($("#Measuring_PD").text().indexOf("CLOSE")!=-1){
                //             $("#Measuring_PD").text("MEAAURING PD");
                //             Measuring_PD_fn_bus_xz(renderer,currnt_sku,catetory_type,color);
                //         }else{
                //             $("#Measuring_PD").text("CLOSE MEAAURING PD");
                //             Measuring_PD_fn_bus_wxz(renderer)
                //         }
                //     }else{
                //         if($(this).find("img").attr("src").indexOf("checked")!=-1){
                //             $(this).find("img").attr("src","/assets2/resource/pdp-summary/images/meaauring_Pd_checked.png");
                //             Measuring_PD_fn_bus_xz(renderer,currnt_sku,catetory_type,color);
                //         }else{
                //             $(this).find("img").attr("src","/assets2/resource/pdp-summary/images/meaauring_Pd.png");
                //             Measuring_PD_fn_bus_wxz(renderer);
                //         }
                //     }
                // });


                $("#tryon_selected").unbind("change");
                //pd动态选择
                $("#tryon_selected").on("change", function () {
                    var pd_xz = $(this).val();
                    $("#first_pd").remove();
                    if (pd_xz) {
                        $(".pd_xz_bottom").text("Currently selected PD: " + pd_xz);
                        pd_obj.pd_text = pd_xz;
                        pd_xz = parseFloat(pd_xz);
                        pd_xz = pd_xz / 10;
                        pd_xz = (pd_xz + 0.2).toFixed(2);
                        pd_xz = parseFloat(pd_xz);
                        updateIPD(pd_xz);
                        pd_obj.pd = pd_xz;
                        $("#fp_tryon").attr("pd_obj", pd_obj ? JSON.stringify(pd_obj) : pd_obj);
                    } else {
                        $(".pd_xz_bottom").text("Currently not selected Pd");
                    }
                    if ($(window).width() >= 769) {
                        $("#pd_xz_class").text("SET PD");
                    } else {
                        $(".set_pd_mobel").attr("src", "/assets2/resource/pdp-summary/images/setPd.png");
                    }

                    $(".select_pd").hide();
                });

                $("#close_glasses").attr("currnt_sku", currnt_sku);
                function mr_options(ele, select_val) {
                    var opt = document.querySelectorAll(ele);
                    for (var i = 0; i < opt.length; i++) {
                        if (opt[i].value == select_val) {
                            opt[i].setAttribute("selected", "selected");
                        }
                    }
                };


                // Anti reflective coating  选择
                $("#defaultCheck1").on("change", function () {
                    var falg = $(this).prop('checked');
                    $(".pd_slide2>li>div:last-child").addClass("Mirror_class").siblings().removeClass("Mirror_class");
                    if (falg) {
                        renderer.setGlassesCustomization({
                            lenses: {
                                antiReflectiveCoating: false
                            }
                        });
                        if (fp_tryon) {
                            renderer1.setGlassesCustomization({
                                lenses: {
                                    antiReflectiveCoating: false
                                }
                            })
                        }
                    } else {
                        renderer.setGlassesCustomization({
                            lenses: {
                                antiReflectiveCoating: true
                            }
                        });
                        if (fp_tryon) {
                            renderer1.setGlassesCustomization({
                                lenses: {
                                    antiReflectiveCoating: true
                                }
                            })
                        }
                    }
                });


                // Mirror Customize
                $(".pd_slide2>li>div").on("click", function () {
                    $(this).addClass("Mirror_class").siblings().removeClass("Mirror_class");
                    var current_rgb = $(this).attr("color");
                    $(".pd_slide3").attr("color", current_rgb);
                    var parames_flag = false;
                    if (!current_rgb) {
                        parames_flag = true;
                    }
                    sunglasseSwitch(catetory_type, current_rgb, renderer, true, parames_flag);
                    if (fp_tryon) {
                        sunglasseSwitch(catetory_type, current_rgb, renderer1, true, parames_flag);
                    }
                });


                //Dioptry(PC) : 拖动
                Adjustments_move("scroll2", "bar2", "mask2", function (event, barleft, leftVal, scroll, bar, mask, that) {
                    var event = event || window.event;
                    barleft = event.clientX - leftVal;
                    if (barleft < 0)
                        barleft = 0;
                    else if (barleft > scroll.offsetWidth - bar.offsetWidth)
                        barleft = scroll.offsetWidth - bar.offsetWidth;
                    mask.style.width = barleft + 'px';
                    that.style.left = barleft + "px";
                    //防止选择内容--当拖动鼠标过快时候，弹起鼠标，bar也会移动，修复bug
                    window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
                    var num = barleft / (scroll.offsetWidth - bar.offsetWidth) * 10;

                    $(".pd_slide2>li>div:last-child").addClass("Mirror_class").siblings().removeClass("Mirror_class");
                    renderer.setGlassesCustomization({
                        lenses: {
                            dioptry: num
                        }
                    });
                    if (fp_tryon) {
                        renderer1.setGlassesCustomization({
                            lenses: {
                                dioptry: num
                            }
                        });
                    }

                });

                //Dioptry(mobel) : 拖动
                Adjustments_move_mobel('btn2', 'touch_bar2', function (pos, x) {
                    renderer.setGlassesCustomization({
                        lenses: {
                            dioptry: pos / 10
                        }
                    });
                    if (fp_tryon) {
                        renderer1.setGlassesCustomization({
                            lenses: {
                                dioptry: pos / 10
                            }
                        });
                    }
                });

                var img_Arr = [
                    {
                        a: "/assets2/resource/pdp-summary/images/GoodMorning_checked.png",
                        b: "/assets2/resource/pdp-summary/images/GoodMorning.png"
                    },
                    {
                        a: "/assets2/resource/pdp-summary/images/in_checked.png",
                        b: "/assets2/resource/pdp-summary/images/in.png"
                    },
                    {
                        a: "/assets2/resource/pdp-summary/images/night_checked.png",
                        b: "/assets2/resource/pdp-summary/images/night.png"
                    }
                ];
                // Photochromic lenses
                $(".pd_slide3 div").on("click", function () {
                    var opctiy_galsses = $(this).attr("opctiy_galsses");
                    opctiy_galsses = parseFloat(opctiy_galsses);
                    var curren_idx = $(this).index();
                    $(".pd_slide3 div img").each(function (idx, ele) {
                        $(ele).attr("src", img_Arr[idx].b);
                    });
                    $(this).find("img").attr("src", img_Arr[curren_idx].a);
                    renderer.setGlassesCustomization({
                        lenses: {
                            opacity: opctiy_galsses
                        }
                    });
                    if (fp_tryon) {
                        renderer1.setGlassesCustomization({
                            lenses: {
                                opacity: opctiy_galsses
                            }
                        });
                    }
                });

                //Thickness Correction(PC) : 拖动
                Adjustments_move("scroll3", "bar3", "mask3", function (event, barleft, leftVal, scroll, bar, mask, that) {
                    var event = event || window.event;
                    barleft = event.clientX - leftVal;
                    if (barleft < 0)
                        barleft = 0;
                    else if (barleft > scroll.offsetWidth - bar.offsetWidth)
                        barleft = scroll.offsetWidth - bar.offsetWidth;
                    mask.style.width = barleft + 'px';
                    that.style.left = barleft + "px";
                    //防止选择内容--当拖动鼠标过快时候，弹起鼠标，bar也会移动，修复bug
                    window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
                    var num = barleft / (scroll.offsetWidth - bar.offsetWidth);
                    num = num.toFixed(2);
                    renderer.setGlassesCustomization({
                        lenses: {
                            thicknessCorrection: num / 10
                        }
                    });

                    if (fp_tryon) {
                        renderer1.setGlassesCustomization({
                            lenses: {
                                thicknessCorrection: num / 10
                            }
                        });
                    }
                });

                //Thickness Correction(mobel) : 拖动
                Adjustments_move_mobel('btn3', 'touch_bar3', function (pos, x) {
                    renderer.setGlassesCustomization({
                        lenses: {
                            thicknessCorrection: pos / 10
                        }
                    });

                    if (fp_tryon) {
                        renderer1.setGlassesCustomization({
                            lenses: {
                                thicknessCorrection: pos / 10
                            }
                        });
                    }
                });

                var setGlassesOffset_obj= {
                    position:0.3,
                    rotation: 0
                };

                $(".pd_slide3").attr("color", color);
                if (pd_obj.pd) {
                    mr_options("#tryon_selected option", pd_obj.pd_text);
                    $(".pd_xz_bottom").text("Currently selected PD: " + pd_obj.pd_text);
                    return Promise.all([
                        renderer.setTracker(tracker),
                        renderer.setGlasses(currnt_sku),
                        fp_tryon ? renderer1.setTracker(tracker) : "",
                        fp_tryon ? renderer1.setGlasses(currnt_sku_two) : "",
                        renderer.setGlassesOffset(setGlassesOffset_obj),
                        renderer1.setGlassesOffset(setGlassesOffset_obj),
                        sunglasseSwitch(catetory_type, color, renderer),
                        fp_tryon ? sunglasseSwitch(catetory_type, color_two, renderer1) : "",
                        updateIPD(pd_obj.pd + 0.2),
                    ]);
                } else {
                    var get_pd = tracker.getIPD();
                    var option = "<option value='select' id='first_pd'>select</option>";
                    $("#tryon_selected").prepend(option);
                    mr_options("#first_pd", "select");
                    $(".pd_xz_bottom").text("Currently not selected Pd");
                    return Promise.all([
                        renderer.setTracker(tracker),
                        renderer.setGlasses(currnt_sku),
                        fp_tryon ? renderer1.setTracker(tracker) : "",
                        fp_tryon ? renderer1.setGlasses(currnt_sku_two) : "",
                        renderer.setGlassesOffset(setGlassesOffset_obj),
                        renderer1.setGlassesOffset(setGlassesOffset_obj),
                        sunglasseSwitch(catetory_type, color, renderer),
                        fp_tryon ? sunglasseSwitch(catetory_type, color_two, renderer1) : "",
                        updateIPD(get_pd + 0.2),

                    ]);
                }
            })
        ]).then(function () {
            //When everything is ready, let's start
            const roi = {x: 0, y: 0, width: 0, height: 0};
            imageSource.computeROIToFitRatio(roi, rendererSize1.width / rendererSize1.height);
            imageSource.setROI(roi);
            if (currnt_sku != "close") {
                $(".smak").hide();
                $(".tryon-sm,.pd_cd").show();
                $(".tryon-list").removeClass("tryon-list-jx");
            }
            var falg_onDetectedUserNbChanged = true;
            //Analysis events registration
            analysis.onNewUser.reg(function(userAnalysisData) {
                if(falg_onDetectedUserNbChanged){
                    falg_onDetectedUserNbChanged = false;
                    //屏幕实时截图
                    renderer.getCapture()
                        .then(function(imgElement){
                            function isJSON(str) {
                                if (typeof str == 'string') {
                                    try {
                                        var obj = JSON.parse(str);
                                        if (typeof obj == 'object' && obj) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    } catch (e) {
                                        console.log('error：' + str + '!!!' + e);
                                        return false;
                                    }
                                }
                            }
                            // 获取到当前用户的实时抓取图片
                            if(imgElement){
                                imgElement = imgElement.getAttribute("src");
                                var datas = {
                                    "data":{
                                        "sku":currnt_sku,
                                        "content_type":"png",
                                        "obj_type":"tryon",
                                        "obj_id":$("#orderRx").attr("simple_product_id"),
                                        "customer_id":0,
                                        "profile_id":0,
                                        "profile_name":"",
                                        "additional_data":JSON.stringify(userAnalysisData)
                                    },
                                    "s3":{
                                        "region":"us-east-1",
                                        "bucket":"pg-product-images"
                                    },
                                    "s":imgElement
                                };
                                var logincustomer = $("body").attr("logincustomer");
                                if(isJSON(logincustomer)){
                                    logincustomer = JSON.parse(logincustomer);
                                    if(logincustomer["customer"]["islogged"]){
                                        datas["data"]["profile_id"]=logincustomer["customer"]["current_profile_id"];
                                        datas["data"]["profile_name"]=logincustomer["customer"]["current_profile_name"];
                                    }else{
                                        if (window.localStorage && window.localStorage.length) {
                                            var defaultLoca = {
                                                global_closed_message: "[]"
                                            };
                                            defaultLoca["guest-profile"] = "{\"nickname\":\"NEW NAME\",\"gender\":\"2\",\"age_group\":\"\"}";
                                            defaultLoca["guest-profile"] = defaultLoca["guest-profile"];
                                            var myLocal = window.localStorage['mage-cache-storage-pg'] && window.localStorage['mage-cache-storage-pg'] != 'undefined' && window.localStorage['mage-cache-storage-pg'].length ? $.parseJSON(window.localStorage['mage-cache-storage-pg']) : {};
                                            if (myLocal['guest-profile'] == undefined) {
                                                myLocal = defaultLoca;
                                            }
                                            if(myLocal){
                                                if(isJSON(myLocal["guest-profile"])){
                                                    var json_guest=myLocal["guest-profile"];
                                                    json_guest = JSON.parse(json_guest);
                                                    datas["data"]["profile_name"]=json_guest["nickname"];
                                                }
                                            }
                                        }
                                    }
                                }
                                $.ajax({
                                    url:"/webyiiapi/tryon/image-to-s3.html",
                                    type: "POST",
                                    contentType: "application/json",
                                    dataType: "json",
                                    data:JSON.stringify(datas),
                                    success:function(res){}
                                });
                            }
                        });
                }
            });
            analysis.start(tracker, -1);
        }).catch(function (e) {
            console.warn("a problem occurred : " + e);
            if (currnt_sku != "close") {
                var tips_tryon = "<p class='tryon_class_ts'>Failed to try on, please refresh and try again！</p>";
                layer.open({
                    shade: 0.5,
                    type: 1,
                    title: "Error",
                    // area: ['60%', 'auto'],
                    shadeClose: true, //点击遮罩关auto闭
                    content: tips_tryon,
                    btn: ["Confirm"],
                    resize: false,
                    move: false,
                    btn1: function (index, lay) {
                        layer.close(index);
                        $(".smak,.tryon_class").hide();
                        window.location.reload();
                    }
                });
                return;
            }
        });


        $("#close_glasses,#photograph_glasses").unbind("click");
        $("#photograph_glasses").on("click", function () {
            if ($(this).find("img").attr("src").indexOf("checked") == -1) {
                $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/photograph_checked.png");
                $(".photograph").removeClass("hide");
                $(".tryon_box_fex").hide();
            } else {
                $(".tryon_box video").removeClass("hide");
                $(".photograph").addClass("hide");
                $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/photograph.png")
            }
        });


        $(".photograph_readius").on("click", function () {
            $(".djs_readius").removeClass("hide");
            $(".photograph_readius,.clickOn_the").addClass("hide");
            $(".close_photograph").hide();
            $(".djs_readius").text(5);
            var a = 5;
            var item = setInterval(function () {
                a--;
                if (a <= 0) {
                    clearInterval(item);
                    $(".tryon_box video,.photograph,.djs_readius").addClass("hide");
                    $(".photograph_readius,.clickOn_the").removeClass("hide");
                    $(".tryon_box_fex").show();
                    $(".close_photograph").show();
                }
                $(".djs_readius").text(a);
            }, 1000);
        });


        $(".close_photograph").on("click", function () {
            $(".tryon_box_fex").show();
            $(".photograph_readius,.clickOn_the").removeClass("hide");
            $(".photograph,.djs_readius").addClass("hide");
            $("#photograph_glasses").find("img").attr("src", "/assets2/resource/pdp-summary/images/photograph.png")
        });


        //当前试戴眼镜是否不展示眼镜切换事件
        $("#close_glasses").on("click", function () {
            var color = $("#close_glasses").attr("color");
            var currnt_sku = $("#close_glasses").attr("currnt_sku");
            if ($(this).find("img").attr("src").indexOf("checked") == -1) {
                $(this).attr("title", "Put on the glasses");
                renderer.setGlasses(null);
                $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/glasses_checked.png");
                $(".set_Ipd").hide();
            } else {
                if (pd_obj) {
                    $(".set_Ipd").show();
                }
                $(this).attr("title", "Remove the glasses");
                $("#fp_tryon").attr("currnt_sku", currnt_sku);
                $("#fp_tryon").attr("color", color);
                renderer.setGlasses(currnt_sku);
                sunglasseSwitch(catetory_type, color, renderer);
                $(this).find("img").attr("src", "/assets2/resource/pdp-summary/images/glasses.png");
            }
        });
    }




