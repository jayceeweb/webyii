
//Initialize Swiper
$(document).ready(function () {

    //获取浏览器高度
    // var body_height = $(window).height();

    // var num_height = 50;
    // if($(window).width()<=546){
    //     num_height  = 110;
    // }
    //
    // $(".LensGuide_box").css({
    //     height:(body_height-num_height)+"px"
    // });
    //LensGuide_box 关闭
    // $(".LensGuide_box_close").on("click",function(){
    //     $("body").css({
    //         "position": "static",
    //         "overflow":"auto"
    //     });
    //     $(".LensGuide").addClass("hide");
    // });
    // $("#right_class_btn").on("click",function(){
    //     $("body").css({
    //         "position": "fixed",
    //         "overflow":"hidden"
    //     });
    //     $(".LensGuide").removeClass("hide");
    // });
    //
    // $(".LensGuide").on("click",function(ev){
    //     if(ev.target!=this && !ev.target.getAttribute("href")){
    //         return false;
    //     }
    //     $("body").css({
    //         "position": "static",
    //         "overflow":"auto"
    //     });
    //     $(".LensGuide").addClass("hide");
    // });




    //获取是否登录状态
     var getis_login = {};
    //获取当前已登录的profile对象
    var current_profile = {};
    //获取当前登录信息 Start
    setTimeout(function(){
        var loginCustomer = $("#webYi_customerInfo").attr("logincustomer");
        if(loginCustomer){
            getis_login = JSON.parse(loginCustomer);
            $("#Email_S").val(getis_login.customer.email);
            current_profile.id=getis_login.customer.current_profile_id;
            current_profile.name=getis_login.customer.current_profile_name
        }else{
            getis_login = {
                customer:{
                    islogged:0
                }
            };
        }
    },1000);
    //获取当前登录信息 End
    var configureProductObj = $("body").attr("configableData");
    if(configureProductObj){
        configureProductObj = JSON.parse(configureProductObj);
    }

    // 数据采集 Start
    function acquisition_data_fn(type,cid,sku,stock_sku,name,price,sales_price,try_on,color_value){
        var data = {};
        data["product"] =  {
            "type":type,
            "cid":cid,
            "attribute_set_id":31,
            "uid":sku,
            "stock_sku":stock_sku,
            "name":name,
            "category_id":$("#orderRx").attr("category_id"),
            "category":$("#category_ele").attr("category"),
            "parent_category":$(".orderRx").attr("catetory_type"),
            "price":price,
            "sales_price":sales_price,
            "brand":"Payne",
            "lens_width_value":tgtrim(configureProductObj["lens_width_value"]),
            "bridge_value":tgtrim(configureProductObj["bridge_value"]),
            "temple_length_value":tgtrim(configureProductObj["temple_length_value"]),
            "frame_size_value":tgtrim(configureProductObj["frame_size_value"]),
            "frame_width_value":tgtrim(configureProductObj["frame_width_value"]),
            "lens_height_value":tgtrim(configureProductObj["lens_height_value"]),
            "frame_weight_value":tgtrim(configureProductObj["frame_weight_value"]),
            "frame_shape_value":tgtrim(configureProductObj["frame_shape_value"]),
            "material_value":tgtrim(configureProductObj["material_value"]),
            "min_pd":tgtrim(configureProductObj["min_pd"]),
            "max_pd":tgtrim(configureProductObj["max_pd"]),
            "rim_type_value":tgtrim(configureProductObj["rim_type_value"]),
            "support_progressive":support_progressive_value,
            "nose_bridge_value":nose_bridge_value,
            "created_at":created_at,
            "has_spring_hinges":tgtrim(configureProductObj["has_spring_hinges_value"]),
            "nose_pad":$("#nose_pad_value").attr("nose_pad_value"),
            "support_rx":is_support_rx,
            "color_changing":is_color_changing,
            "low_bridge_fit":is_low_bridge_fit,
            "is_goggles":isGoogleGTMEnabled,
            "try_on":try_on,
            "color_value":color_value,
        };
        return data;
    }
    var price  = "";
    var sales_price  = "";
    if(current_simple_sales_price){
        if(current_simple_price!=current_simple_sales_price){
            sales_price = current_simple_sales_price;
        }else{
            price = current_simple_price;
        }
    }else{
        price = current_simple_price;
    }
    var entity_id = $(".choose-wrap").attr("entity_id");
    var sku =  $(".sku_text_attr").attr("dest_sku");
    var stock_sku = $(".choose-wrap").attr("stock_sku");
    var  dest_name = $(".choose-wrap").attr("dest_name");
    var color_value = $("#pdp-color_list").attr("color_value");
    var is_tryon = $("#btn-Try").attr("flag_tryon");
    if(is_tryon=="true"){
        is_tryon = true;
    }else{
        is_tryon = false;
    }
    try {
        acquisition_fn("productview",acquisition_data_fn("default",entity_id,sku,stock_sku,dest_name,price,sales_price,is_tryon,color_value));  //默认触发
    } catch(e){
        console.log(e);
    }
    // 数据采集 End


    //电商增强函数
    function tgtrim(str){
        if (str==null || str+""=="tgtrim") {
            return "";
        } else {
            return str;
        }
    }
    var support_progressive_recommend="recommend-PAL";
    var support_progressive_support="support-PAL";
    var support_progressive_no="no-PAL";
    var clip_on_yes="clip-on";
    var support_ditto_yes="support-ditto";
    var support_php_yes="support-php";
    var nose_pad_yes="nose-pad";
    var nose_bridge_no="nose-bridge-no";
    var nose_bridge_standard="nose-bridge-standard";
    var nose_bridge_low="nose-bridge-low-bridge-fit";
    var has_spring_hinges_yes="has-spring-hinges";
    var support_rx_yes="support_rx";
    function GoogleGTMEnabled(configureProductObj,swiper_img_id,simpleProductObj,event,lens_type,add_to_cart){
        var flag_simple_id = true;
        if(lens_type){
            flag_simple_id = true;
        }else{
            if(current_simple_id!=swiper_img_id){
                flag_simple_id = true;
            }else{
                flag_simple_id = false;
            }
        }
        //判断如果为checkout方式的话，就使用前者判断
        if (event=='checkout'? (isGoogleGTMEnabled==1) : (isGoogleGTMEnabled==1 && flag_simple_id)) {
            progressive="";
            var checkoutdata = [];
            if (tgtrim(configureProductObj["support_progressive_value"])=="Yes" && tgtrim(configureProductObj["recommend_value"])=="Yes") {
                progressive=support_progressive_recommend;
            } else if (tgtrim(configureProductObj["support_progressive_value"])=="Yes" && tgtrim(configureProductObj["recommend_value"])=="No") {
                progressive=support_progressive_support;
            } else {
                progressive=support_progressive_no;
            }
            feature="";
            tgtrim(configureProductObj["support_progressive_value"])
            if (tgtrim(configureProductObj["is_clip_on"])=="1") {
                feature=clip_on_yes;
            }
            if (tgtrim(simpleProductObj["support_ditto"])==1) {
                feature=feature+"/"+support_ditto_yes;
            }
            if (tgtrim(configureProductObj["nose_pad"])=="1") {
                feature=feature+"/"+nose_pad_yes;
            }
            feature=feature+"/"+progressive;
            if (tgtrim(configureProductObj["nose_bridge_value"])=="Standard") {
                feature=feature+"/"+nose_bridge_standard;
            } else if (tgtrim(configureProductObj["nose_bridge_value"])=="Low bridge fit") {
                feature=feature+"/"+nose_bridge_low;
            }else{
                feature=feature+"/"+nose_bridge_no;
            }
            if (tgtrim(configureProductObj["has_spring_hinges_value"])=="Yes") {
                feature=feature+"/"+has_spring_hinges_yes;
            }
            if (tgtrim(configureProductObj["support_rx"])=="1") {
                feature=feature+"/"+support_rx_yes;
            }
            var frame_price=tgtrim(simpleProductObj["simple_price"]);
            function getCookie(c_name){
                if (document.cookie.length>0){
                    var c_start = document.cookie.indexOf(c_name + "=");
                    if (c_start!=-1){
                        c_start=c_start + c_name.length+1;
                        c_end=document.cookie.indexOf(";",c_start);
                        if (c_end==-1) c_end=document.cookie.length;
                        return unescape(document.cookie.substring(c_start,c_end))
                    }
                }
                return "";
            }

            var dimension10 = getCookie("dimension10");
            var dimension14 = getCookie("dimension14")?getCookie("dimension14"):"ANON";
            var dimension15 = getCookie("dimension15")?getCookie("dimension15"):"OTH";
            var dimension18 = getCookie("dimension18");
            checkoutdata.push({
                "id": tgtrim(simpleProductObj["simple_sku"]),
                "name": tgtrim(simpleProductObj["simple_name"]),
                "variant": tgtrim(simpleProductObj["simple_colorString"]),
                "price": parseFloat(frame_price),
                'brand': tgtrim("Payne"),
                'category': tgtrim($("#orderRx").attr("catetory_type")),
                "dimension1":tgtrim(simpleProductObj["simple_stock_sku"]),
                "dimension2":tgtrim(configureProductObj["lens_width_value"]),
                "dimension3":tgtrim(configureProductObj["bridge_value"]),
                "dimension4":tgtrim(configureProductObj["temple_length_value"]),
                "dimension5":tgtrim(configureProductObj["frame_size_value"]),
                "dimension6":tgtrim(configureProductObj["frame_width_value"]),
                "dimension7":tgtrim(configureProductObj["lens_height_value"]),
                "dimension8":tgtrim(configureProductObj["frame_weight_value"]),
                "dimension9":tgtrim(configureProductObj["frame_shape_value"]),
                // "dimension10":tgtrim(configureProductObj["rim_type_value"]),
                "dimension11":tgtrim(configureProductObj["material_value"]),
                "dimension12":tgtrim(configureProductObj["min_pd"]),
                "dimension13":tgtrim(configureProductObj["max_pd"]),
                // "dimension14":tgtrim(configureProductObj["min_sph_value"]),
                // "dimension15":tgtrim(configureProductObj["max_sph_value"]),
                "dimension16":tgtrim(configureProductObj["other_colors_value"]),
                "dimension17":tgtrim(""),
                // "dimension18":tgtrim(progressive),
                "dimension19":tgtrim(configureProductObj["age_range_value"]),
                "dimension20":tgtrim(feature),
                "metric1":tgtrim(frame_price),
            });
            if(event=='productDetail'){
                dataLayer.push({
                    'event': event,
                    'ecommerce': {
                        'detail': {
                            'actionField': {'list': ''},    // 'detail' actions have an optional list property.
                            'products': checkoutdata
                        }
                    }
                });
            }
            if(event=='checkout'){
                if(add_to_cart=='addToCart'){
                    dataLayer.push({
                        'event': 'addToCart',
                        'ecommerce': {
                            'currencyCode': 'USD',
                            'add': {
                                'products': checkoutdata
                            }
                        }
                    });
                    dataLayer.push({
                        'event': event,
                        'ecommerce': {
                            'checkout': {
                                'actionField': {'step': 3},
                                'products': checkoutdata
                            }
                        }
                    });
                }else{
                    dataLayer.push({
                        'event': event,
                        'ecommerce': {
                            'checkout': {
                                'actionField': {'step': 1},
                                'products': checkoutdata
                            }
                        }
                    });
                }
            }

        }
        current_simple_id=swiper_img_id;
    }
    var order_btn = $('#box_fix');
    var t = order_btn.offset().top;
    var fh = order_btn.height();
    var wh = window.innerHeight;
    if (!((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i).test(navigator.userAgent)) && $(window).width()>=500) {
        /*点击返回顶部*/
        $(window).scroll(function () {
            var botopH = $(window).scrollTop();//滚动的高度
            if (botopH > 50) {
                $('#botop').stop().fadeIn();
            } else {
                setTimeout(function () {
                    $('#botop').stop().fadeOut();
                }, 2000)
            }
        });
        $('#botop').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 600);
        });

            //移动端滑动save ordergalsses固定顶部
            $(window).scroll(function () {
                var s = $(document).scrollTop();
                if (s > t + fh) {
                    // order_btn.addClass("box_fix_class_pc");
                } else if (s < t - wh + fh + 10) {
                    order_btn.addClass("box_fix_class_pc");
                } else {
                    order_btn.removeClass("box_fix_class_pc");
                }
            });
    }else{
        //移动端滑动save ordergalsses固定顶部
        $(window).scroll(function () {
            var s = $(document).scrollTop();
            if (s > t + fh) {
                order_btn.addClass("box_fix_class");
            } else if (s < t - wh + fh + 10) {
                order_btn.addClass("box_fix_class");
            } else {
                order_btn.removeClass("box_fix_class");
            }
        });
    }
    function initSwiper(num) {
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            initialSlide :num,
            spaceBetween: 10,
            slidesPerView:20,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            observer: true,//修改swiper自己或子元素时，自动初始化swiper
            observeParents: false,//修改swiper的父元素时，自动初始化swiper
        });
        var galleryTop = new Swiper('.gallery-top', {
            //   spaceBetween: 10,
            navigation: {
                    nextEl: '.swiper-button-next-simple',
                    prevEl: '.swiper-button-prev-simple',
            },
            initialSlide :num,
            thumbs: {
                swiper: galleryThumbs
            },
            observer: true,//修改swiper自己或子元素时，自动初始化swiper
            observeParents: false,//修改swiper的父元素时，自动初始化swiper
        });
    }
    initSwiper(0);

    //当前swiper样式通过lens Type个数判断
    var customRadioLen = document.querySelectorAll(".custom-radio");
    if($(window).width()>=1200){
        if(customRadioLen.length>=4){
            $(".padd-top").css({
                // "padding-top": "135px"
            })
        }else{
            $(".padd-top").css({
                // "padding-top": "30px"
            })
        }
    }
    //判断当前浏览器不为safari的时候添加一样式
    if(!window.safari){
        $(".row").addClass("row_class");
    };
    // 替换当前url参数的函数
    function changeURLArg(url,arg,arg_val){
        var pattern=arg+'=([^&]*)';
        var replaceText=arg+'='+arg_val;
        if(url.match(pattern)){
            var tmp='/('+ arg+'=)([^&]*)/gi';
            tmp=url.replace(eval(tmp),replaceText);
            return tmp;
        }else{
            if(url.match('[\?]')){
                return url+'&'+replaceText;
            }else{
                return url+'?'+replaceText;
            }
        }
        return url+'\n'+arg+'\n'+arg_val;
    }
// End
    //获取指定url中的参数值
    function getvar(url,par) {
        var urlsearch = url.split('?');
        pstr = urlsearch[1].split('&');
        for (var i = pstr.length - 1; i >= 0; i--) {
            var tep = pstr[i].split("=");
            if (tep[0] == par) {
                return tep[1];
            }
        }
        return (false);
    }
//end
    // 检测产品的状态
    function Testingroducts(url,configurable_id,fn){
        $.ajax({
            url:url,
            type:"POST",
            // async:false,   // 同步
            data:{
                cpid:configurable_id
            },
            success:fn
        });
    }
    var configurable_id = $(".stock_text").attr("configurable_id");
    var current_entity_id = $(".stock_text").attr("current_entity_id");
    /*** luanjue_3   isColorChange变量只是为了判断是否是点击color切换事件  yes=是，no=不是 ***/
    function stock_fn(configurable_id,entity_id,frame_group, isColorChange, price, salesPrice){
        var price = price || 0;
        var salesPrice = salesPrice || 0;
        //modify lee 2020.6.9
        Testingroducts("/e/product/ProductInfo/cproductInfo",configurable_id,function(res){
            if(res.simples){
                /**  luanjue_3
                 *  446数据返回有问题 可以去重处理(让后台去做去重处理！！) 生产没问题即可！
                 *  // console.log(res.simples);
                    // console.log(JSON.stringify(res.simples));
                    // console.log(price);
                    // console.log(salesPrice);
                 * **/

                res.simples.forEach(function(val){
                    if(val.id==entity_id){
                        $("#orderRx").attr("stock",val.stock_txt);
                        $(".stock_text").text(val.stock_txt);

                        // luanjue_3  stock采集数据赋值   start
                        if(smartOn == 1) {
                            $("#orderRx").attr("ljstock", val.stock_txt);
                            if (isColorChange == 'yes') {
                                var ljData5 = ljCollectFun(val.stock_txt);
                                smart.track_event("pdp_frame_color", ljData5);
                            }
                        }
                        // luanjue_3  stock采集数据赋值  end

                        // for google Recommend  detail-page-view  start  颜色切换也算是浏览详情
                        // console.log(window.loginCustomer.customer.current_profile_id, "------------window.loginCustomer.customer.current_profile_id");
                        // console.log(val, '----------------val');
                        // dataLayer = dataLayer || [];
                        var googleUserInfo = {
                            'visitorId': getLocalDeviceID
                        };
                        var googleUserID = window.loginCustomer.customer.current_profile_id;
                        if(googleUserID) {
                            googleUserInfo.userId = googleUserID;
                        }
                        dataLayer.push({
                            'event':'detail-page-view',
                            'automl': {
                                'eventType': 'detail-page-view',
                                'userInfo': googleUserInfo,
                                'productEventDetail': {
                                    'productDetails': [{
                                        "id": val['sku'],
                                    }]
                                }
                            }
                        });
                        // for google Recommend  detail-page-view  end


                        if(val.stock_txt=="RETIRED"){
                            $("#orderRx").text(val.stock_txt);
                            $("#orderRx").addClass("RETIRED");
                            $("#ts_ele").show();
                            $("#container_box").css({
                                marginTop:"23px"
                            });
                            $(".dest_name").css({
                                "padding-top":"0px"
                            });
                            $("#ts_ele").attr("class","ts_ele");
                            $("#ts_ele").text('So sorry! The selected frame is retired. Try another frame!');
                        }else if(val.stock_txt=="OUT OF STOCK"){
                            $("#ts_ele").show();
                            $("#container_box").css({
                                marginTop:"23px"
                            });
                            $(".dest_name").css({
                                "padding-top":"0px"
                            });
                            $("#ts_ele").attr("class","ts_ele");
                            $("#ts_ele").text('The frame of this color is out of stock. Click "Sign up" to be notified when it is back in stock.');
                            $("#orderRx").text("Sign up");
                        }else{
                            $("#ts_ele").hide();
                            $(".dest_name").css({
                                "padding-top":"20px"
                            });
                            $("#container_box").css({
                                marginTop:"auto"
                            });
                            var orderRx_text =  $("#orderRx").attr("text");
                            $("#orderRx").removeClass("RETIRED");
                            $("#orderRx").text("Order Glasses");
                            if(frame_group.indexOf("only_blue_light_blocking")!=-1){
                                $("#orderRx").text("Order Glasses");
                            }else{
                                $("#orderRx").text(orderRx_text);
                            }
                        }
                    }
                })
            }
        });
    }
    stock_fn(configurable_id,current_entity_id,"111", "no", price, sales_price);

    // PDP页面数据埋点   luanjue_3  start
    var smartOn = $("#smartCollectOn").val();
    if(smartOn == 1) {
        smart.init('collectionPdp', {
            loaded: function (sdk) {
                // luanjue_3  清除super_properties
                sdk.clear_event_super_properties();
            }
        });
        smart.clear_event_super_properties();

        //  2、添加监听点击切换LensType事件
        smart.time_event('pdp_lens_type');
        //  5、切换颜色事件采集
        smart.time_event('pdp_frame_color');
        //   6、 blueLightBlocking -- OrderGlasses -- Pres 事件 填写验光单   pdp-summary\js\pdp-revision\detail.js文件layer触发地方
        //   blueLightBlocking -- OrderGlasses 1：Prescription Glasses事件 0:Non-Prescription Glasses事件
        smart.time_event('pdp_blue_light_order_glasses');
        // 7、 pdp页面Out Of Stock时，signUp 提交confirm按钮事件
        smart.time_event('pdp_outofstock_signup');
    }

    function ljCollectFun(stockObj){
        /* configurableID： 归属ID  parent_id  */
        var lj5ConfigurableID = $("#orderRx").attr("configure_product_id");
        var lj5LensType = $(".choose-wrap").find(".active").attr("lens-type");
        var lj5DestSku = $(".choose-wrap").find(".active").attr("dest_sku");
        var ljSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");

        var ljData = {
            "configurableID": lj5ConfigurableID,
            "destSku": lj5DestSku,
            "lensType": lj5LensType,
            "sku": ljSku
        };
        return ljData;
    }
    // PDP页面数据埋点   luanjue_3  end


    // 推荐 Start
    var entity_id = $(".choose-wrap").attr("entity_id");
    function recommend_fn(entity_id){
        var product_id_obj = {product_id:entity_id};
        $.ajax({
            url:"/webyiiapi/related/related.html",
            type:"POST",
            data:JSON.stringify(product_id_obj),
            success:function(res){
                // res = JSON.parse(res);
                if(res.code!=0){
                    $(".recommend").hide();
                    $(".default_show").addClass("hide");
                }else{
                    $(".default_show").removeClass("hide");
                    $(".recommend").show();
                    var data_obj = res.data;
                    var attributeCount = function(obj) { //获取对象属性的个数
                        var count = 0;
                        for(var i in obj) {
                            if(obj.hasOwnProperty(i)) {
                                count++;
                            }
                        }
                        return count;
                    };
                    var slidesPerView = 3;
                    var slidesPerView_2 = 2;

                    var data_obj_length = attributeCount(data_obj);
                    if(data_obj_length>=3){
                        if(data_obj_length==3){
                            if($(window).width()<=1200){
                                $(".swiper-button-hide").show();
                            }else{
                                $(".swiper-button-hide").hide();
                            }
                        }else{
                            $(".swiper-button-hide").show();
                        }
                        $(".recommend").attr("id","recommend_3");
                        slidesPerView = 3;
                        slidesPerView_2 = 2;
                    }else if(data_obj_length==2){
                        if($(window).width()<=600){
                            $(".swiper-button-hide").show();
                        }else{
                            $(".swiper-button-hide").hide();
                        }
                        $(".recommend").attr("id","recommend_2");
                        slidesPerView = 2;
                        slidesPerView_2 = 2;
                    }else{
                        $(".swiper-button-hide").hide();
                        $(".recommend").attr("id","recommend_1");
                        slidesPerView = 1;
                        slidesPerView_2 = 1;
                    }

                    //指定默认排序到最前函数
                    function setArrFirst(arr, property, value) {
                        var chooseIndex;    //选中元素在数组中的索引
                        var areBothObj = arr.every(function(item, index, array) {    //判断数组中的元素是否都为对象，都是对象则返回true
                            return item instanceof Object;
                        });
                        if (areBothObj) {
                            arr.forEach(function(item, index, array) {    //遍历数组，找到符合条件的元素索引
                                if (item[property] == value) {
                                    chooseIndex =  index;
                                }
                            });
                        } else {
                            return false;
                        }
                        var choosed = arr.splice(chooseIndex, 1);    //将选中元素从原数组中剔除
                        var newArr = choosed.concat(arr);    //合并选中元素和剔除了选中元素的原数组
                        return newArr;
                    }

                    var html_recommend = "";
                    for(var i in data_obj) {
                        var span_html = "";
                        var new_data_obj = data_obj[i];
                        new_data_obj = setArrFirst(new_data_obj, 'default', true);
                        new_data_obj.forEach(function(val,idx){
                            var color_value = val["color_value"];
                            var color_img = "/media/catalog/product/"+val["image"];
                            var color_url = "/"+val["url"];
                            var price = val["price"];
                            var special_price =val["special_price"];


                            if(color_value.length>7){ //使用图片
                                span_html+="<span class='color_value_click' color_img="+color_img+" color_url="+color_url+" price="+price+" special_price="+special_price+" sku="+val['sku']+" parent_id="+val['parent_id']+"><span style=background-image:url(/media/attribute/swatch/swatch_image/30x20"+color_value+")></span></span>";
                            }else{ //使用背景色
                                span_html+="<span class='color_value_click' color_img="+color_img+" color_url="+color_url+" price="+price+" special_price="+special_price+" sku="+val['sku']+" parent_id="+val['parent_id']+"><span style=background:"+color_value+"></span></span>";
                            }
                        });
                        var url = "/"+new_data_obj[0].url;
                        var img = "/media/catalog/product/"+new_data_obj[0].image;
                        var price = new_data_obj[0].price;
                        var special_price = new_data_obj[0].special_price;
                        var name = new_data_obj[0].name;
                        price = parseFloat(price);
                        price = price.toFixed(2);
                        special_price = parseFloat(special_price);
                        special_price = special_price.toFixed(2);

                        var price_html = "";
                        if(price!=special_price && special_price){ //有打折价
                            price_html = "<del>$"+price+"</del><span>$"+special_price+"</span>";
                        }else{ // 没有打折价
                            price_html = "<span>$"+price+"</span>";
                        }
                        html_recommend += "<div class='swiper-slide recommend_swiperSlide'>"+
                            // "<a href="+url+">"+
                            "<a href="+url+" price='"+price+"' name='"+name+"' special_price='"+special_price+"'>"+
                            "<img src="+img+">"+
                            "</a><div class='xyuandian'>"+span_html+
                            "</div>"+
                            "<p>"+name+"</p>"+
                            "<li>"+price_html+"</li></div>";
                    }
                    $("#append_recommend").html(html_recommend);
                    new Swiper('.recommend', {
                        slidesPerView : slidesPerView,
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        },
                        breakpoints:{
                            1200: { //640屏幕下设定
                                slidesPerView: slidesPerView_2, //一行2个
                                spaceBetween: 10 //这是swiper的每个元素间隔的设置
                            },
                            600:{ //320屏幕下设定
                                slidesPerView: 1, //一行2个
                                spaceBetween: 0 //这是swiper的每个元素间隔的设置
                            }
                        }
                    });
                }
            },
            error: function (e) {
                $(".default_show").addClass("hide");
                $(".recommend").hide();
                console.log(e);
            }
        });
    }

    try {
        recommend_fn(entity_id);
        $(document).on("click",".color_value_click",function(){ //推荐颜色切换
            var color_img = $(this).attr("color_img");
            var color_url = $(this).attr("color_url");
            var price = $(this).attr("price");
            var special_price = $(this).attr("special_price");
            var ljSkuSwiper = $(this).attr("sku");
            var ljParentIDSwiper = $(this).attr("parent_id");
            $(this).parents(".recommend_swiperSlide").find("img").attr("src",color_img);
            $(this).parents(".recommend_swiperSlide").find("img").attr("parent_id",ljParentIDSwiper);
            $(this).parents(".recommend_swiperSlide").find("img").attr("sku",ljSkuSwiper);
            $(this).parents(".recommend_swiperSlide").find("a").attr("href",color_url);
            price = parseFloat(price);
            price = price.toFixed(2);
            special_price = parseFloat(special_price);
            special_price = special_price.toFixed(2);
            var price_html = "";
            if(price!=special_price && special_price){ //有打折价
                price_html = "<del>$"+price+"</del><span>$"+special_price+"</span>";
            }else{ // 没有打折价
                price_html = "<span>$"+price+"</span>";
            }
            $(this).parents(".recommend_swiperSlide").find("li").html(price_html);
            $(this).css({
                borderColor:"#000"
            }).siblings(".color_value_click").css({
                borderColor:"transparent"
            })
        });
    } catch(e){
        console.log(e);
    }


    // 推荐 End


    //产品颜色切换
    $(".btn-color-click").on("click",function(){
        var current_url_list = $(this).attr("current_url_list");
        var entity_id=$(this).attr("entity_id");
        var review_sku=$(this).attr("review_sku");
        var review_color=$(this).attr("review_color");
        var lens_type =  $("#pdp-color_list").attr("lens_type");
        var base_url = $(this).attr("base_url");
        var data_img = $(this).attr("data_img");
        var current_sku = $(this).attr("current_sku");
        var page_description = $(this).attr("page_description");
        var review_img = $(this).find("img").attr("imgs");
        $(".imgs").attr("src","/media/catalog/product"+review_img);
        var relate_product = $(this).attr("relate_product");
        var price_list = $(this).attr("price_list");
        var sales_price_list = $(this).attr("sales_price_list");
        var color_details = $(this).attr("color_details");
        var  flag_tryon = $(this).parent(".select_li").attr("flag");
        var  stock_sku = $(this).parent(".select_li").attr("stock_sku");
        var catetory_type = $("#orderRx").attr("catetory_type");
        var frame_group = $(this).attr("frame_group");
        var blue_price = $(this).attr("blue_price");
        var blue_sales_price = $(this).attr("blue_sales_price");
        var progressive = $(".Progressive_val_img").attr("progressive");
        var simple_product_id=$(this).attr("simple_product_id");
        var simple_product_color=$(this).attr("simple_product_color");
        var color_value=$(this).attr("color_value");
        $("#pdp-color_list").attr("color_value",color_value);

        // luanjue price_sku current_entity_id值 不用动
        // var ljEntityID = $(this).attr("simple_product_id");
        // $(".stock_text.right_class").attr("current_entity_id",ljEntityID);

        // 推荐 Start
        try {
            recommend_fn(entity_id);
        } catch(e){
            console.log(e);
        }
        // 推荐 End

        //获取当前评论参数
        $(".imgs").attr("data-simple");
        if(review_sku=="14209G07" || review_sku=="24209G07"){
            $("#Progressive_val").html("Progressive/Bifocal: <strong>No</strong>")
        }else{
            $("#Progressive_val").html("Progressive/Bifocal: <strong>"+progressive+"</strong>")
        }
        $(".choose-wrap").each(function(idx,ele){
            if($(ele).attr("entity_id")==entity_id){
                $(ele).removeClass("hide").siblings(".choose-wrap").addClass("hide");
            }
        });
        $("#orderRx").attr("frame_group_s",frame_group);
        // var str_details = $(".color_details_append").html();
        // if(str_details){
        //     str_details = str_details.replace(/\s*/g,"");
        // }
        if(color_details){
            var color_details_html = "<p style='margin-bottom: 10px;margin-top:10px;font-size: 15px;' class='box_Color'><span class='Color_Details_bt'></span> <span id='Color_Details'>"+color_details+"</span></p>";
            $(".color_details_append").html(color_details_html);
            $(".btn-color-parent-p-left").html("");
        }else{
            var color_name = "<p class='btn-color-parent-p btn-color-parent-p-left'>"+color_value+"</p>";
            $(".color_details_append").html("");
            $(".color_details_appends").html(color_name);
        }
        if(catetory_type!="sunglasses"){
            if(color_details){
                $(".box_Color").html("<span class='Color_Details_bt'></span> <span id='Color_Details'>"+color_details+"</span>");
                $(".btn-color-parent-p-left").html("");
            }else{
                $(".box_Color").html("");
                var color_name = "<p class='btn-color-parent-p btn-color-parent-p-left'>"+color_value+"</p>"
                $(".color_details_appends").html(color_name);
            }
        }
        //电商增强参数
        var color_string = $(this).next(".btn-color-parent-p").text();
        var simple_price = $(this).attr("price");
        if(color_string){
            color_string = color_string.replace(/\s/g,"");
        }
        var dist_sku = $(".sku_text_attr").attr("dest_sku");
        var simple_obj = {
            simple_sku:dist_sku,
            simple_colorString:color_string,
            simple_price:simple_price,
            simple_stock_sku:stock_sku,
            support_ditto:$("#btn-Try").attr("flag_tryon")?1:0,
        };
        $(".choose-wrap").attr("color_string",color_string);
        $(".choose-wrap").attr("stock_sku",stock_sku);
        // tryon
        $("#btn-Try").attr("flag_tryon",flag_tryon);
        $("#btn-Try").attr("currnt_sku",stock_sku);
        $("#btn-Try").attr("sku",review_sku);
        if(flag_tryon){
            $("#btn-Try").css({
                borderColor:"#f90"
            })
        }else{
            $("#btn-Try").css({
                borderColor:"#ccc"
            })
        }
        $(".tryon-parent").each(function(idx,ele){
            var current_sku = $(ele).attr("stock_sku");
            if(stock_sku==current_sku){
                $(ele).attr("stock_sku",current_sku);
                $(ele).find("a").addClass("active").parents(".tryon-parent").siblings(".tryon-parent").find("a").removeClass("active");
                $(ele).find("p").addClass("select_Color").parents(".tryon-parent").siblings(".tryon-parent").find("p").removeClass("select_Color");
            }
        });
        // tryon
        $(".reveiws-container").attr("entry_id",entity_id);
        $(".reveiws-container").attr("sku",review_sku);
        $(".reveiws-container").attr("color",review_color);
        $("#save_x").attr("currentid",entity_id);
        if(current_sku){
            current_sku = JSON.parse(current_sku);
            var arr_current_sku = [];
            for(var key in current_sku){
                if(key==lens_type){
                    simple_obj.simple_name = current_sku[key][0];
                    if(frame_group.indexOf("only_blue_light_blocking")==-1){
                        $(".simple_name").text(current_sku[key][0]);
                        $(".choose-wrap").attr("dest_name",current_sku[key][0]);
                    }
                    $(".sku_text").text(current_sku[key][1]);
                    $(".sku_text_attr").attr("dest_sku",current_sku[key][1]);
                    $("#sildWarp li").each(function(idx,ele){
                        $(ele).find("img").attr("alt",current_sku[key][0]);
                    });
                    $("#min_sildWarp li").each(function(idx,ele){
                        $(ele).find("img").attr("alt",current_sku[key][0]);
                    });
                }
                arr_current_sku.push(current_sku[key]);
            }
            for(var i=0; i<arr_current_sku.length; i++){
                $(".choose-wrap label").eq(i).attr("dest_name",arr_current_sku[i][0]);
                $(".choose-wrap label").eq(i).attr("dest_sku",arr_current_sku[i][1]);
            }
        }
        if(frame_group.indexOf("only_blue_light_blocking")!=-1){
            $("#orderRx").text("Order Glasses");
            $("#orderRx").attr("lens-type","Blue_Non_Rx");
            var current_url_listA = JSON.parse(current_url_list);
            history.replaceState(null, null,base_url+"/"+current_url_listA["Blue_Non_Rx"]);
            $(this).attr("ull",base_url+"/"+current_url_listA["Blue_Non_Rx"]);

            var str_price = "";
            if(blue_sales_price && blue_sales_price!=blue_price){
                blue_sales_price = parseFloat(blue_sales_price).toFixed(2);
                blue_price = parseFloat(blue_price).toFixed(2);
                str_price+="<del class='del_price price_class'>$"+blue_price+"</del><div class='price h4 price_class' style='margin-right: 5px;'>$"+blue_sales_price+"</div>";
                $("#price_text").attr("price",blue_price);
                $("#price_text").attr("space_price",blue_sales_price);
                collect_price=blue_price;
                collect_sales_price=blue_sales_price
            }else{
                var from_str = "<a class='from_class_a'>From: </a>";
                blue_price = parseFloat(blue_price).toFixed(2);
                str_price+="<div class='price h4 price_class'>$"+price+"</div>";
                $("#price_text").attr("price",price);
                collect_price=price
            }
            $(".price_text").html(str_price);
            $(".choose-wrap label").each(function(idx,ele){
                if($(ele).attr("lens-type")!="Blue_Non_Rx"){
                    $(ele).addClass("hide");
                }else{
                    var dest_name = $(ele).attr("dest_name");
                    $(".simple_name").text(dest_name);
                    $(".choose-wrap").attr("dest_name",dest_name);
                    $(ele).removeClass("hide");
                    var included = $(ele).attr("included");
                    if(included){
                        included= JSON.parse(included);
                        var str_li="";
                        included.forEach(function(val){
                            str_li+="<li>"+val+"</li>"
                        });
                        $(".list-check").html(str_li);
                    }
                }
            });
        }else{
            $(".choose-wrap label").each(function(idx,ele){
                $(ele).removeClass("hide");
                $(ele).find("input").attr("name","lenses");
            })
        }
        if(current_url_list && frame_group.indexOf("only_blue_light_blocking")==-1){
            current_url_list = JSON.parse(current_url_list);
            var new_simple_url = [];
            for(var key in current_url_list){
                new_simple_url.push(current_url_list[key]);
                if(key == lens_type){
                    $(".reveiws-container").attr("data-simple",base_url+"/"+current_url_list[key]);
                    // // 当前替换好的参数，改变url地址
                    history.replaceState(null, null,base_url+"/"+current_url_list[key]);
                    $(this).attr("ull",base_url+"/"+current_url_list[key]);
                }
            }
            $(".custom-radio").each(function(idx,ele){
                $(ele).attr("simple_url",new_simple_url[idx])
            });
        }
        if(page_description){
            page_description = JSON.parse(page_description);
            for(var key in page_description){
                if(key==lens_type){
                    $("#page_description").text(page_description[key]);
                }
            }
        }
        if(data_img){
            data_img = JSON.parse(data_img);
            if(data_img.length>4){
                var lis =  "<li class='swiper-slide swiper-slide-visible swiper-slide-active' ><img src=''></li>";
                var min_lis =  "<li class='swiper-slide swiper-slide-visible swiper-slide-active swiper-slide-thumb-active' ><img src=''></li>";
                if($("#sildWarp li").length<=4 || $("#min_sildWarp li").length<=4){
                    $("#sildWarp").append($(lis));
                    $("#min_sildWarp").append($(min_lis));
                }
            }else{
                if($("#sildWarp li").length>=5 || $("#min_sildWarp li").length>=5){
                    $("#sildWarp li:nth-of-type(1)").remove();
                    $("#min_sildWarp li:nth-of-type(1)").remove();
                }
            }
            $("#sildWarp li").each(function(idx,ele){
                try {
                    $(ele).find("img").attr("src",static_url+"/media/catalog/product"+data_img[idx]);
               } catch(e){
                    $(ele).find("img").attr("src","/media/catalog/product"+data_img[idx]);
                }
            });
            $("#min_sildWarp li").each(function(idx,ele){
                try {
                    $(ele).find("img").attr("src",static_url+"/media/catalog/product/cache/230x115"+data_img[idx]);
                } catch(e){
                    $(ele).find("img").attr("src","/media/catalog/product/cache/230x115"+data_img[idx]);
                }
            });
            // $(".pc_product li").each(function(idx,ele){
            //     $(ele).find("img").attr("src","/media/catalog/product/"+data_img[idx]);
            // });
        }
        var entity_id = $(this).attr("entity_id");
        $(this).addClass("active").parents(".select_li_frame").siblings(".select_li_frame").find(".btn-color-click").removeClass("active");
        $(this).next().addClass("select_Color").parents(".select_li_frame").siblings(".select_li_frame").find(".btn-color-parent-p").removeClass("select_Color");
        //眼镜互通处理
        if(relate_product && relate_product!="[]"){
            relate_product = JSON.parse(relate_product);
            var new_relate_product = [];
            for(var key in relate_product){
                new_relate_product.push(relate_product[key]);
                if(key == lens_type){
                    $(".relate_class").removeClass("hide");
                    $(".relate_class a").attr("href","/"+relate_product[key].url);
                }
            }
        }else{
            $(".relate_class").addClass("hide")
        }

        $(".choose-wrap").attr("entity_id",entity_id);
        //add 电商增强
        try {
            GoogleGTMEnabled(configureProductObj,entity_id,simple_obj,'productDetail');
        } catch (e) {
            console.log(e);
        }
        // end add
        initSwiper(0);
        $("#orderRx").attr("simple_product_id",simple_product_id);
        $("#orderRx").attr("stock_sku",stock_sku);
        $("#orderRx").attr("simple_product_color",simple_product_color);
        var simple_price=$(this).attr("price");
        var simple_special_price=$(this).attr("special_price");
        $(".choose-wrap").attr("special_price",simple_special_price);
        //指定lens_type切换实时读取simple的当前价格
        if(lens_type=="Single" || lens_type=="Non_Rx" || lens_type=="Reading"){
            var from_str = "<p class='from_class'>From: </p>";
            var str_price = "";
            if(simple_special_price && simple_special_price!="0.00"){
                if(simple_special_price!=simple_price){
                    str_price+=from_str+"<del class='del_price price_class'>$"+simple_price+"</del><div class='price h4 price_class' style='margin-right: 5px;'>$"+simple_special_price+"</div>";
                    $("#price_text").attr("price",simple_price);
                    $("#price_text").attr("space_price",simple_special_price);
                }else{
                    str_price+=from_str+"<div class='price h4 price_class'>$"+simple_price+"</div>";
                    $("#price_text").attr("price",simple_price);
                    $("#price_text").attr("space_price",simple_price);
                }
            }else{
                str_price+=from_str+"<div class='price h4 price_class'>$"+simple_price+"</div>";
                $("#price_text").attr("price",simple_price);
                $("#price_text").attr("space_price",simple_price);
            }
            $(".price_text").html(str_price);
        }else{
            var price_obj = {};
            if(sales_price_list && sales_price_list!="[]"){
                sales_price_list = JSON.parse(sales_price_list);
                for(var key in sales_price_list){
                    if(key == lens_type){
                        price_obj = {
                            name:key,
                            sales_price:sales_price_list[key]
                        };
                    }
                }
            }


            if(price_list && price_list!="[]"){
                price_list = JSON.parse(price_list);
                for(var key in price_list){
                    if(key == lens_type){
                        price_obj.price = price_list[key]
                    }
                }
            }

            $(".js-check").each(function(idx,ele){
                if($(ele).attr("lens-type")==lens_type){
                    $(ele).attr("sales_price",price_obj["sales_price"]);
                    $(ele).attr("price",price_obj["price"]);
                }
            });
            var from_str = "<p class='from_class'>From: </p>";
            var str_price = "";
            if(price_obj["name"]){
                if(price_obj["sales_price"] && price_obj["sales_price"]!="0.00"){
                    if(price_obj["sales_price"]!=price_obj["price"]){
                        str_price+=from_str+"<del class='del_price price_class'>$"+price_obj["price"]+"</del><div class='price h4 price_class' style='margin-right: 5px;'>$"+price_obj["sales_price"]+"</div>";
                        $("#price_text").attr("price",price_obj["price"]);
                        $("#price_text").attr("space_price",price_obj["sales_price"]);
                    }else{
                        str_price+=from_str+"<div class='price h4 price_class'>$"+price_obj["price"]+"</div>";
                        $("#price_text").attr("price",price_obj["price"]);
                        $("#price_text").attr("space_price",price_obj["price"]);
                    }
                }else{
                    str_price+=from_str+"<div class='price h4 price_class'>$"+price_obj["price"]+"</div>";
                    $("#price_text").attr("price",price_obj["price"]);
                    $("#price_text").attr("space_price",price_obj["price"]);
                }
                $(".price_text").html(str_price);
            }
        }
        var collect_price=  $("#price_text").attr("price");
        var collect_sales_price= $("#price_text").attr("space_price");

        // console.log(collect_price);
        // console.log(collect_sales_price);
        //获取当前产品状态
        //in stick  获取当前状态
        stock_fn(configurable_id,entity_id,frame_group, "yes", collect_price, collect_sales_price);

        //数据采集 【颜色切换】
        try {
            var price = $("#price_text").attr("price");
            var space_price = $("#price_text").attr("space_price");
            var dest_name = $(".choose-wrap").attr("dest_name");
            var is_tryon = $("#btn-Try").attr("flag_tryon");
            var current_sku = $(".sku_text_attr").attr("dest_sku");
            if(is_tryon=="true"){
                is_tryon = true;
            }else{
                is_tryon = false;
            }
            acquisition_fn("productview",acquisition_data_fn("frame_color",entity_id,current_sku,stock_sku,dest_name,price,space_price,is_tryon,color_value));
        } catch(e){
            console.log(e);
        }
        //数据采集 【颜色切换】


    });
    //产品颜色切换 End
    //移动端的时候把dittis插入到下面,pc端插入到上面
    if($(window).width()<=1200){
        var pc_dittis_html = $(".pc_dittis").html();
        $("#mobel_ditles").html(pc_dittis_html);
        $(".pc_dittis").html("");
    }

    //点击Sign in  或者Start here.  给url地址添加referer
    var currentHref = window.location.href;
    var b = new Base64();
    var new_refer = b.encode(currentHref);
    function Base64() {
        // private property
        _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        // public method for encoding
        this.encode = function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = _utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                    _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                    _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
            }
            return output;
        }
        // public method for decoding
        this.decode = function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = _keyStr.indexOf(input.charAt(i++));
                enc2 = _keyStr.indexOf(input.charAt(i++));
                enc3 = _keyStr.indexOf(input.charAt(i++));
                enc4 = _keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = _utf8_decode(output);
            return output;
        }
        // private method for UTF-8 encoding
        _utf8_encode = function (string) {
            string = string.replace(/\r\n/g,"\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }
            return utftext;
        }
        // private method for UTF-8 decoding
        _utf8_decode = function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while ( i < utftext.length ) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i+1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i+1);
                    c3 = utftext.charCodeAt(i+2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    }


    // 未登录修改profile函数
    function profileS(profileLength,current_profile,profileList,loginUrl,urlType,fn){
        profileLength  = profileLength.islogged;
        current_profile.name = profileLength.current_profile_name;
        current_profile.id = profileLength.current_profile_id;
        /**
         * 参数：
         * profileLength:当前用户的profle列表的长度
         * current_profile:当前profile对象「id,name」
         * profileList:用户所有的profile列表项
         * loginUrl:弹出框中的login路径「如果为未登录,传入该登录路径」
         * fn:执行回调函数
         *
         **/
        //当input框不为空的时候执行
        $(".profile_class .nicknames").keyup(function () {
            $(".profile_class .nicknames").css({
                borderColor: "#ccc"
            });
            $(".profile_class #boxSS").fadeOut();
        });
        var text_s = "saved prescription";
        if(urlType=="reading"){
            text_s = "saved reading power";
        }

        var href_login = "/customer/account/login/referer/"+new_refer;
        var str ="<h4 class='profile_class'>" +
            "<div style='display:inline-block' class='dispTitle'>Please enter wearer's name:</div>"+
            "<input type='text' maxlength='50' autofocus='autofocus' class='nicknames' placeholder='NEW NAME'>"+
            "<div class='profleLogin'>or"+
            "<span><a class='loginProfile' href="+href_login+">login</a></span>"+
            "to use your <b id='saved'>"+text_s+"</b></div><span id='#boxSS'></span></h4>";
        function confirem_fn(){
            var profileName = $(".profile_class  input").val();
            if(profileName==""){
                profileName =  $(".profile_class  input").attr("placeholder");
            }
            //获取用户输入的是否都为空格
            var valuestr = profileName.replace(/^\s*|(\s*$)/g,"");
            if (profileName != "" && valuestr.length > 0) {
                //未登录状态
                if (profileLength == 0) {
                    //更新location中的匿名profile名字到数据库
                    var data = {
                        nickname: profileName,
                        age_group: localPorfile.age_group,
                        birthyear: '2019',
                        gender: localPorfile.gender,
                        guest_id: "0"
                    };
                    if (localPorfile.guest_profile_id != undefined) {
                        data.guest_profile_id = localPorfile.guest_profile_id;
                    }
                    $.ajax({
                        type: "post",
                        showLoader: true,
                        url: "/profile/guest/add",
                        data: data,
                        success: function (http) {
                            /**将输入的profile名字赋值给当前缓存项上默认的profile名字上*/
                            localPorfile.nickname = profileName;
                            //将返回出来guest_profile_id写入到localStorage
                            localPorfile.guest_profile_id = http.guest_profile_id;
                            var new_localPorfile = JSON.stringify(localPorfile);

                            if(myLocal){
                                myLocal["guest-profile"] = new_localPorfile;
                            }else{
                                myLocal = {};
                                myLocal["global_closed_message"]=[];
                                myLocal["guest-profile"] = new_localPorfile
                            }
                            /**进行localStorage设置到缓存中*/
                            window.localStorage.setItem('mage-cache-storage-pg', JSON.stringify(myLocal));
                            fn();
                        }
                    })
                }else{
                    var repeatProfile= profileList.filter(function(val){
                        return val.nickname == profileName && profileName != current_profile.name;
                    });
                    //当前输入的不重复
                    $(".profile_class  #boxSS").fadeOut();
                    //如果已登录profile为NEW NAME、yourself,点击confirm继续执行
                    if(current_profile.name && (current_profile.name.indexOf("NEW NAME")!=-1 || current_profile.name.indexOf("yourself") !=-1)){
                        $.ajax({
                            url: "/profile/manage/update/?nickname=" + profileName + "&profile_id=" + current_profile.id,
                            type: "GET",
                            showLoader: true,
                            success: function (data) {
                                fn();
                            },
                            error:function(err){
                                fn();
                            }
                        })
                    }
                }
            } else {
                /**如果input没有输入值*/
                $(".profile_class .loginProfile").attr('href',loginUrl);
                $(".profile_class input").css({
                    borderColor: "red"
                });
                $(".profile_class  #boxSS").text("This is a required field.");
                $(".profile_class  #boxSS").fadeIn();
            }
        };
        //如果未登陆状态(profile长度为0）
        if (profileLength == 0) {
            //给弹出框a标签绑定登录的路径
            $(".profile_class .loginProfile").attr('href',loginUrl);
            $(".profile_class b").text("Prescription");
            //profile缓存进行获取未登陆的名字
            if (window.localStorage && window.localStorage.length) {
                var defaultLoca = {
                    global_closed_message: "[]"
                };
                defaultLoca["guest-profile"] = {nickname: "yourself", gender: "2", age_group: ""};
                defaultLoca["guest-profile"] = defaultLoca["guest-profile"];
                var myLocal = window.localStorage['mage-cache-storage-pg'] && window.localStorage['mage-cache-storage-pg'] != 'undefined' && window.localStorage['mage-cache-storage-pg'].length ? $.parseJSON(window.localStorage['mage-cache-storage-pg']) : {};
                if (myLocal['guest-profile'] == undefined) {
                    myLocal = defaultLoca;
                }
            }
            //获取profile的名字项
            var localPorfile = "";
            if(myLocal){
                localPorfile = myLocal['guest-profile'];
            }else{
                localPorfile = {nickname: "yourself", gender: "2", age_group: ""}
            }
            /**判断存储为json对象的时候*/
            if(localPorfile) {
                var local_name = "";
                //判断如果未登录当前的profile为NEW NAME和yourself只弹框
                if(typeof(localPorfile)=="string"){
                    var nack_name =  JSON.parse(localPorfile);
                    local_name = nack_name.nickname;
                }
                else if(typeof(localPorfile)=="object"){
                    local_name  = localPorfile.nickname;
                }
                if(local_name=="yourself"){
                    layer.open({
                        shade: 0.5,
                        type: 1,
                        title:"<p class='size_title_pdp'>Who will wear the glasses?</p>",
                        area: ['90%', 'auto'],
                        shadeClose: true, //点击遮罩关auto闭
                        content:str,
                        btn:["Confirm","Cancel"],
                        resize:false,
                        move:false,
                        yes:function(){
                            confirem_fn();
                        },
                        btn2:function(index,lay){
                            layer.close(index);
                        },
                        success:function(layero, index){
                            this.enterConfirm = function(event){
                                if(event.keyCode === 13){
                                    confirem_fn();
                                }
                            };
                            $(document).on('keydown', this.enterConfirm); //监听键盘事件
                        },
                    });
                }else{
                    fn();
                }
                //end
                if (typeof localPorfile != "object") {
                    localPorfile = JSON.parse(localPorfile);
                }
                if (localPorfile.nickname == "yourself") {
                    $(".profile_class input").attr("placeholder","NEW NAME");
                } else {
                    $(".profile_class input").attr("placeholder",localPorfile.nickname);
                }

            }
        }else{
            fn();
        }
    }
    $("#btn-Try").hover(function () {
       var is_try = $(this).attr("flag_tryon");
        if(is_try=="true"){
            $(this).css({
                backgroundColor:"#f90",
                borderColor:"#f90",
                color:"#fff"
            })
        }else{
            $(this).css({
                backgroundColor:"#fff",
                borderColor:"#eaeaea",
                color:"#212529"
            })
        }
    }, function () {
        var is_try = $(this).attr("flag_tryon");
        if(is_try=="true"){
            $(this).css({
                backgroundColor:"#fff",
                borderColor:"#f90",
                color:"#212529"
            })
        }else{
            $(this).css({
                backgroundColor:"#fff",
                borderColor:"#eaeaea",
                color:"#212529"
            })
        }
    });
    //抗蓝光参数整合
    var dataKlg={
        "profile" : {
            "id":"",
            "nickname":""
        },
        "is_nonPrescription":true,
        "orderglass":{
            "Lenss":{
                "sku":blocker_sku,
                "name":blocker_name,
                "productId":blocker_entity_id,
                "price":blocker_price,
                "price-original":blocker_original_price,
                "type":"Lenss",
                "qty":1,
                "color":"",
                "colorname":"false",
            }
        },
        "support_php":""
    };

    //Frame_Only参数整合
    var frameOnly={
        "profile" : {
            "id":"",
            "nickname":""
        },
        "is_nonPrescription":false,
        "orderglass":{},
        "support_php":""
    };

    //点击Blue Light Blocking按钮发送的ajax
    function ajaxKlg(data,fn){
        var configure_product_id = $("#orderRx").attr("configure_product_id");
        var simple_product_id = $("#orderRx").attr("simple_product_id");
        var simple_stock_sku = $("#orderRx").attr("stock_sku");
        var simple_product_color = $("#orderRx").attr("simple_product_color");
        $.ajax({
            type: "POST",
            url: "/checkout/cart/add/product/"+configure_product_id+"/?product="+simple_product_id+"&stock_sku="+simple_stock_sku+"&qty=1&super_attribute%5B93%5D="+simple_product_color,
            data: data,
            success:fn,
            beforeSend:function(){
                $(".loading-jz").show();
            },
            error:function(){
                $(".loading-jz").hide();
            }
        });
    }
    // end
    // 点击老化处理逻辑
    function readingFn(reading_type){
        var sum = 0;
        var newrx = {};
        var hc  = window.localStorage.getItem("mage-cache-storage-pg");
        function isJSON(str) {
            if (typeof str == 'string') {
                try {
                    JSON.parse(str);
                    return true;
                } catch(e) {
                    return false;
                }
            }
        }
        var prescription = {};
        if(isJSON(hc)){
            hc = JSON.parse(hc);
            if(hc["guest-prescription"]){
                prescription = JSON.parse(hc["guest-prescription"]);
            }
        }
        function prescriptionFn(obj) {
            for (var i in obj) {
                return true;
            }
            return false;
        };
        if(prescriptionFn(prescription)){
            newrx = {
                lax:prescription['lax'],
                labse:prescription['lbase'],
                lbase_1:prescription['lbase_1'],
                lcyl:prescription['lcyl'],
                lpri:prescription['lpri'],
                lsph:prescription['lsph'],
                prescruption_img:prescription['prescription_img'],
                rax:prescription['rax'],
                rbase:prescription['rbase'],
                rbase_1:prescription['rbase_1'],
                rcyl:prescription['rcyl'],
                rpri:prescription['rpri'],
                rpri_1:prescription['rpri_1'],
                rsph:prescription['rsph']
            };
        }
        if(newrx instanceof Object){
            for(var key in newrx){
                if(typeof(newrx[key])=='string'){
                    newrx[key] = Math.abs(Number(newrx[key]));
                    sum += newrx[key];
                }
            }
        }
        if(reading_type=="sum"){
            return sum
        }else{
            return newrx
        }
    }

    $("#price_text").attr("price",current_simple_price);
    $("#price_text").attr("space_price",current_simple_sales_price);

    // 点击老化处理逻辑 end
    // 默认页面进来触发电商增强函数 start
    function simple_obj(){
        var simple_obj = {
            support_ditto:$("#btn-Try").attr("flag_tryon")?1:0,
        };
        simple_obj["simple_stock_sku"] =  $("#orderRx").attr("stock_sku");
        simple_obj["simple_sku"] = current_simple_distSku;
        simple_obj["simple_colorString"] = current_simple_color;
        if(current_simple_sales_price){
            if(current_simple_price!=current_simple_sales_price){
                simple_obj["simple_price"] = current_simple_sales_price;
            }else{
                simple_obj["simple_price"] = current_simple_price;
            }
        }else{
            simple_obj["simple_price"] = current_simple_price;
        }
        simple_obj["simple_name"]=current_simple_sales_name;
        return simple_obj;
    }
    var simple_obj_entity_id = current_simple_entity_id;
    try {
        GoogleGTMEnabled(configureProductObj,simple_obj_entity_id,simple_obj(),'productDetail',true);
    } catch (e) {
        console.log(e);
    }
    // 默认页面进来触发电商增强函数 end
    //  点击orderrx处理
    $(".orderRx").on("click",function(e,tag){
        var loginCustomer = $("#webYi_customerInfo").attr("logincustomer");
        if(loginCustomer){
            getis_login = JSON.parse(loginCustomer);
            $("#Email_S").val(getis_login.customer.email);
            current_profile.id=getis_login.customer.current_profile_id;
            current_profile.name=getis_login.customer.current_profile_name
        }else{
            getis_login = {
                customer:{
                    islogged:0
                }
            };
        }
        var order_lensType = $(this).attr("lens-type");
        var order_frame_group = $(this).attr("frame_group");
        var frame_group_href = $(this).attr("frame_group_href");
        var order_frame_group_s = $(this).attr("frame_group_s");
        var profileLength  = getis_login.customer;
        var configure_product_id = $(this).attr("configure_product_id");
        var simple_product_id = $(this).attr("simple_product_id");
        var configure_product_sku = $(this).attr("configure_product_sku");
        var simple_product_color = $(this).attr("simple_product_color");
        var category_id = $(this).attr("category_id");
        var catetory_type = $(this).attr("catetory_type");
        var type_click = $(this).attr("type_click");
        var order_url = "";
        var  btnColor = document.querySelectorAll(".btn-color");
        var data = {};

        var collection_stock_sku = $("#orderRx").attr("stock_sku");
        var collection_current_sku = $(".sku_text_attr").attr("dest_sku");
        var collection_color_value = $("#pdp-color_list").attr("color_value");
        var collection_is_tryon = $("#btn-Try").attr("flag_tryon");
        var collection_entity_id=  $(".choose-wrap").attr("entity_id");
        var collection_dest_name =  $(".choose-wrap").attr("dest_name");
        var collection_price = $("#price_text").attr("price");
        var collection_sales_price = $("#price_text").attr("sales_price");
        if(collection_is_tryon=="true"){
            collection_is_tryon = true;
        }else{
            collection_is_tryon = false;
        }

        function isJSON(str) {
            if (typeof str == 'string') {
                try {
                    var obj = JSON.parse(str);
                    if (typeof obj == 'object' && obj) {
                        return true;
                    } else {
                        return false;
                    }
                } catch (e) {
                    console.log('error：' + str + '!!!' + e);
                    return false;
                }
            }
        }
        //电商增强参数 Start
        var simple_obj_order = {
            simple_sku:$(".sku_text_attr").attr("dest_sku"),
            simple_colorString:$(".reveiws-container").attr("color"),
            simple_stock_sku:$(".choose-wrap").attr("stock_sku"),
            support_ditto:$("#btn-Try").attr("flag_tryon")?1:0,
            simple_name:$(".choose-wrap").attr("dest_name")
        };
        var current_simple_price = $("#price_text").attr("price");
        var current_simple_sales_price = $("#price_text").attr("space_price");
        var entry_id__order = $(".reveiws-container").attr("entry_id");
        if(current_simple_sales_price){
            if(current_simple_price!=current_simple_sales_price){
                simple_obj_order["simple_price"] = current_simple_sales_price;
            }else{
                simple_obj_order["simple_price"] = current_simple_price;
            }
        }else{
            simple_obj_order["simple_price"] = current_simple_price;
        }
        //电商增强参数 End

        for(var i=0; i<btnColor.length; i++){
            if(btnColor[i].getAttribute("class")!=-1){
                var sku =  btnColor[i].getAttribute("review_sku");
                data.sku=sku;
            }
        }
        var customerEmail = getis_login.customer.email?getis_login.customer.email:"";
        if($(this).text().indexOf("Sign up")!=-1){
            var signUp = "<input class='email product-out-of-stock-sales-notify-button-input upload-wearing-glasses-photo-popup-n-hold-textarea' type='email'    placeholder='Please input your email address.' id='php_val' style='border: none;' value="+customerEmail+">";
            var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            layer.open({
                shade: 0.5,
                type: 1,
                title: "In Stock Notification",
                area: ['90%', 'auto'],
                shadeClose: true, //点击遮罩关auto闭
                content: signUp,
                btn: ["Confirm", "Cancel"],
                resize: false,
                move: false,
                btn1: function (index, lay) {
                    // luanjue   pdp页面Out Of Stock时，signUp ->confrim提交事件  start
                    if(smartOn == 1) {
                        var ljData7 = ljCollectFun();
                        ljData7['signupType'] = 1;
                        smart.track_event("pdp_outofstock_signup", ljData7);
                    }
                    // luanjue   pdp页面Out Of Stock时，signUp ->confrim提交事件 end

                    data.email = $("#php_val").val();
                    if(!email.test(data.email)){
                        $("#ts_ele").attr("class","error");
                        $("#ts_ele").text("Please enter a valid email address (Ex: johndoe@domain.com).");
                        layer.close(index);
                        $('html, body').animate({scrollTop: 0}, 600);
                        return false;
                    }else{
                        $.ajax({
                            url:"/catalog/product/saleNotify",
                            data:data,
                            success:function(res){
                                $("#ts_ele").attr("class","success");
                                $("#ts_ele").text("You have successfully been added for a notification");
                                layer.close(index);
                                $('html, body').animate({scrollTop: 0}, 600);
                            }
                        });
                    }
                },
                btn2: function (index, lay) {
                    // luanjue   pdp页面Out Of Stock时，signUp ->Cancel提交事件  start
                    if(smartOn == 1) {
                        var ljData7 = ljCollectFun();
                        ljData7['signupType'] = 0;
                        smart.track_event("pdp_outofstock_signup", ljData7);
                    }
                    // luanjue   pdp页面Out Of Stock时，signUp ->Cancel提交事件 end

                    $('html, body').animate({scrollTop: 0}, 600);
                },
            });
            return false;
        }
        if(Object.prototype.toString.call(getis_login)=== '[object Object]'){
            islogin = getis_login.customer.islogged;
        }else{
            islogin = 0;
        }
        //太阳镜分类
        if(catetory_type=="sunglasses"){
            if(order_lensType=="Reading"){
                order_url = '/buy/index/index/product/' + simple_product_id + '?product_main=' + configure_product_id + '&sku=' + configure_product_sku + '&_super_attribute[93]=' + simple_product_color + '&category=' + category_id + "&is_sunglasses=1&nonrx=0&type=reading";
            }else if(order_lensType=="Non_Rx"){
                order_url = '/buy/index/index/product/' + simple_product_id + '?product_main=' + configure_product_id + '&sku=' + configure_product_sku + '&_super_attribute[93]=' + simple_product_color + '&category=' + category_id + "&is_sunglasses=1&nonrx=1";
            }else{
                order_url = '/buy/index/index/product/' + simple_product_id + '?product_main=' + configure_product_id + '&sku=' + configure_product_sku + '&_super_attribute[93]=' + simple_product_color + '&category=' + category_id + "&is_sunglasses=1&nonrx=0";
            }
        }else{
            order_url = '/buy/index/index/product/' + simple_product_id + '?product_main=' + configure_product_id + '&sku=' + configure_product_sku + '&_super_attribute[93]=' + simple_product_color + '&category=' + category_id;
        }
        if(order_lensType=="Blue_Non_Rx" && !tag){
            function BlueNonRxFn(islogin,dataKlg,ajaxKlg,order_lensType,frame_group_href){
                //点击抗蓝按钮事件触发
                //未登录状态
                if (islogin!=1) {
                    var orgId = window.localStorage.getItem("org_id");
                    var id_orgCustomerId = window.localStorage.getItem("org_customer_id");
                    if(orgId && id_orgCustomerId){
                        dataKlg.profile.org_id=orgId;
                        dataKlg.profile.org_customer_id=id_orgCustomerId;
                    }
                    //profile缓存进行获取未登陆的名字
                    if (window.localStorage && window.localStorage.length) {
                        var defaultLoca = {
                            global_closed_message: "[]"
                        };
                        defaultLoca["guest-profile"] = "{\"nickname\":\"NEW NAME\",\"gender\":\"2\",\"age_group\":\"\"}";
                        defaultLoca["guest-profile"] = defaultLoca["guest-profile"];
                        var myLocal = window.localStorage['mage-cache-storage-pg'] && window.localStorage['mage-cache-storage-pg'] != 'undefined' && window.localStorage['mage-cache-storage-pg'].length ? $.parseJSON(window.localStorage['mage-cache-storage-pg']) : {};
                        if (myLocal['guest-profile'] == undefined) {
                            myLocal = defaultLoca;
                        }
                    }
                    dataKlg.profile.nickname="NEW NAME";
                    if(myLocal){
                        if(isJSON(myLocal["guest-profile"])){
                            var json_guest=myLocal["guest-profile"];
                            json_guest = JSON.parse(json_guest);
                            var guestProfileName = json_guest["nickname"];
                            dataKlg.profile.nickname=guestProfileName;
                        }
                    }
                    window.localStorage.setItem('mage-cache-storage-pg', JSON.stringify(myLocal));
                }
                //已登录状态
                else{
                    dataKlg.profile.nickname=current_profile.name;
                    dataKlg.profile.id=current_profile.id;
                }
                var Coating_UV = {
                    sku:"VCUV",
                    name:"100% UV Protection",
                    productId:6216,
                    price:0.00,
                    type:"Coating_UV",
                    qty:1,
                    color:"",
                    colorname:false,
                    special_price:0.00,
                };
                Coating_UV["price-original"]=0.00;
                var Coating_Scratch = {
                    sku:"VCAS",
                    name:"Anti-Scratch Coating",
                    productId:6136,
                    price:0.00,
                    type:"Coating_Scratch",
                    qty:1,
                    color:"",
                    colorname:false,
                    special_price:0.00,
                };
                Coating_Scratch["price-original"]=0.00;
                var Coating = {
                    sku:"VCAR1",
                    name:"Anti-Reflective Coating",
                    productId:6134,
                    price:0.00,
                    type:"Coating",
                    qty:1,
                    color:"",
                    colorname:false,
                    special_price:0.00,
                };
                Coating_Scratch["price-original"]=3.95;
                dataKlg.orderglass.Coating_UV=Coating_UV;
                dataKlg.orderglass.Coating_Scratch=Coating_Scratch;
                dataKlg.orderglass.Coating=Coating;
                dataKlg["lens_type"]=order_lensType;
                dataKlg["frame_group"]=frame_group_href;
                ajaxKlg(dataKlg,function(data){
                    if (data["code"]==0) {
                        setTimeout(function(){
                            $(".loading-jz").hide();
                        },4000);
                        //add 电商增强
                        try {
                            GoogleGTMEnabled(configureProductObj,entry_id__order,simple_obj_order,'checkout',true,"addToCart");
                        } catch (e) {
                            console.log(e);
                        }
                        // end

                        //数据采集 【addToCart】
                        try {
                            acquisition_fn("productview",acquisition_data_fn("addToCart",collection_entity_id,collection_current_sku,collection_stock_sku,collection_dest_name,collection_price,collection_sales_price,collection_is_tryon,collection_color_value));
                        } catch(e){
                            console.log(e);
                        }
                        //数据采集 【addToCart】


                        window.location="/checkout/cart";
                    } else {
                        $('html,body').animate({scrollTop:0},1000);//回到顶端
                        $(".klgErr").text(data.message);
                        $(".klgErr").show();
                        setTimeout(function(){
                            $(".klgErr").hide();
                            $(".loading-jz").hide();
                        },4000);
                    }
                });
            }
            if(order_frame_group_s.indexOf("only_blue_light_blocking")!=-1){
                var str ="<h4 class='profile_class'>Please confirm you are purchasing a pair of Blue Light Blocking Glasses WITHOUT prescription.</h4>"
                layer.open({
                    shade: 0.5,
                    type: 1,
                    title:"Attention",
                    area: ['60%', 'auto'],
                    shadeClose: true, //点击遮罩关auto闭
                    content:str,
                    btn:["Confirm","Cancle"],
                    resize:false,
                    move:false,
                    btn1:function(index,lay){
                        // luanjue   Prescription Glasses按钮点击采集事件  start
                        if(smartOn == 1) {
                            var ljData6 = ljCollectFun();
                            ljData6['blueType'] = 1;
                            smart.track_event("pdp_blue_light_order_glasses", ljData6);
                        }
                        // luanjue   Prescription Glasses按钮点击采集事件  end

                        layer.close(index);
                        BlueNonRxFn(islogin,dataKlg,ajaxKlg,order_lensType,frame_group_href);
                    },
                    btn2:function(index,lay){
                        // luanjue   Prescription Glasses按钮点击采集事件  start
                        if(smartOn == 1) {
                            var ljData6 = ljCollectFun();
                            ljData6['blueType'] = 0;
                            smart.track_event("pdp_blue_light_order_glasses", ljData6);
                        }
                        // luanjue   Prescription Glasses按钮点击采集事件  end

                        layer.close(index);
                    }
                });
            }else{
                var str ="<h4 class='profile_class'>Please confirm if you are purchasing a pair of Blue Light Blocking Glasses WITH or WITHOUT prescription.</h4>";
                layer.open({
                    shade: 0.5,
                    type: 1,
                    title:"CONFIRMATION",
                    area: ['60%', 'auto'],
                    shadeClose: true, //点击遮罩关auto闭
                    content:str,
                    btn:["Prescription Glasses","Non-Prescription Glasses","Close"],
                    resize:false,
                    move:false,
                    btn1:function(index,lay){
                        // luanjue   Prescription Glasses按钮点击采集事件  start
                        if(smartOn == 1) {
                            var ljData6 = ljCollectFun();
                            ljData6['blueType'] = 1;
                            smart.track_event("pdp_blue_light_order_glasses", ljData6);
                        }
                        // luanjue   Prescription Glasses按钮点击采集事件  end

                        //  luanjue trigger() 方法触发被选元素的指定事件类型。click
                        //  传递到事件处理程序的额外参数。 额外的参数对自定义事件特别有用。blue
                        $("#orderRx").trigger('click',"blue");   // luanjue  orderGlasses跳转。
                        layer.close(index);
                    },
                    btn2:function(index,lay){
                        // luanjue   Prescription Glasses按钮点击采集事件  start
                        if(smartOn == 1) {
                            var ljData6 = ljCollectFun();
                            ljData6['blueType'] = 0;
                            smart.track_event("pdp_blue_light_order_glasses", ljData6);
                        }
                        // luanjue   Prescription Glasses按钮点击采集事件  end

                        layer.close(index);
                        BlueNonRxFn(islogin,dataKlg,ajaxKlg,order_lensType,frame_group_href);
                    },
                    btn3:function(index,lay){
                        layer.close(index);
                    }
                });
            }
        }else if(order_lensType=="Frame_Only"){
            function Frame_OnlyFn(islogin,frameOnly,ajaxKlg,order_lensType,frame_group_href){
                //点击抗蓝按钮事件触发
                //未登录状态
                if (islogin!=1) {
                    //profile缓存进行获取未登陆的名字
                    if (window.localStorage && window.localStorage.length) {
                        var defaultLoca = {
                            global_closed_message: "[]"
                        };
                        defaultLoca["guest-profile"] = "{\"nickname\":\"NEW NAME\",\"gender\":\"2\",\"age_group\":\"\"}";
                        defaultLoca["guest-profile"] = defaultLoca["guest-profile"];
                        var myLocal = window.localStorage['mage-cache-storage-pg'] && window.localStorage['mage-cache-storage-pg'] != 'undefined' && window.localStorage['mage-cache-storage-pg'].length ? $.parseJSON(window.localStorage['mage-cache-storage-pg']) : {};
                        if (myLocal['guest-profile'] == undefined) {
                            myLocal = defaultLoca;
                        }
                    }
                    window.localStorage.setItem('mage-cache-storage-pg', JSON.stringify(myLocal));
                    frameOnly.profile.nickname="NEW NAME";
                }
                //已登录状态
                else{
                    frameOnly.profile.nickname=current_profile.name;
                    frameOnly.profile.id=current_profile.id;
                }
                var additional_data = {
                    so_type:"frame_only"
                };
                frameOnly["additional_data"]=additional_data;
                frameOnly["frame_group"]=frame_group_href;
                ajaxKlg(frameOnly,function(data){
                    if (data["code"]==0) {
                        setTimeout(function(){
                            $(".loading-jz").hide();
                        },4000);
                        //add 电商增强
                        try {
                            GoogleGTMEnabled(configureProductObj,entry_id__order,simple_obj_order,'checkout',true,"addToCart");
                        } catch (e) {
                            console.log(e);
                        }
                        // end add


                        //数据采集 【addToCart】
                        try {
                            acquisition_fn("productview",acquisition_data_fn("addToCart",collection_entity_id,collection_current_sku,collection_stock_sku,collection_dest_name,collection_price,collection_sales_price,collection_is_tryon,collection_color_value));
                        } catch(e){
                            console.log(e);
                        }
                        //数据采集 【addToCart】



                        window.location="/checkout/cart";
                    } else {
                        $('html,body').animate({scrollTop:0},1000);//回到顶端
                        $(".klgErr").text(data.message);
                        $(".klgErr").show();
                        setTimeout(function(){
                            $(".klgErr").hide();
                            $(".loading-jz").hide();
                        },4000);
                    }
                });
            }
            Frame_OnlyFn(islogin,frameOnly,ajaxKlg,order_lensType,frame_group_href);
        }else{
            //add 电商增强
            try {
                GoogleGTMEnabled(configureProductObj,entry_id__order,simple_obj_order,'checkout',true);
            } catch (e) {
                console.log(e);
            }
            // end add

            //数据采集 【order_glasses】
            try {
                acquisition_fn("productview",acquisition_data_fn("order_glasses",collection_entity_id,collection_current_sku,collection_stock_sku,collection_dest_name,collection_price,collection_sales_price,collection_is_tryon,collection_color_value));
            } catch(e){
                console.log(e);
            }
            //数据采集 【order_glasses】


            if(order_lensType=="Non_Rx"){
                window.location.href = order_url+"&type=norx&lens_type="+order_lensType+"&frame_group="+frame_group_href;
            }else if(type_click=="ApplyFor"){
                order_url+="&php=true";
                window.location.href = order_url;
            }else{
                profileS(profileLength,current_profile,JSON.parse(profileList),loginUrl,order_lensType,function(){
                    if(readingFn("sum")>0 && order_lensType=="Reading" && JSON.stringify(readingFn("newrx")) != "{}" && islogin!=1){
                        var href_login = "/customer/account/login/referer/"+new_refer;
                        window.location.href = href_login;
                    }else{
                        if(catetory_type!="sunglasses"){
                            if(order_lensType=="Reading"){
                                order_url+="&type=reading";
                            }
                        }
                        window.location.href = order_url+"&lens_type="+order_lensType+"&frame_group="+frame_group_href;
                    }
                });
            }
        }

    });
    //write review
    var userAgentInfo = navigator.userAgent;//获取游览器请求的用户代理头的值
    var Agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];//定义移动设备数组
    for (var v = 0; v < Agents.length; v++) {
        //判断是否是移动设备
        if (userAgentInfo.indexOf(Agents[v]) > 0) {
            isPc= false;
            break;
        }
    }
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
    var isIE = /msie|trident/g.test(window.navigator.userAgent.toLowerCase());
    //类型选择触发
    $('.js-check :radio').change(function (e,tag) {
        var that = this;
        if(tag){
            that = tag;
        }
        item = $(that).closest('.js-check');
        if(!isIE){
            if ($(that).is(':checked')) {
                item.siblings().removeClass('active');
                item.addClass('active');
            } else {
                item.removeClass('active');
            }
            $(this).siblings(".box_gl").removeClass("hide").parent().siblings().find(".box_gl").addClass("hide");
        }

        var base_url = $(that).parents(".choose-wrap").attr("base_url");
        var simple_url = $(that).parents().attr("simple_url");
        var price = $(that).parents().attr("price");
        var sales_price = $(that).parents().attr("sales_price");
        var str_price = "";
        var children_lens_type = $(that).parents().attr("lens-type");
        var children_frame_group = $(that).parents().attr("frame_group");
        var catetory_type = $(".orderRx").attr("catetory_type");
        var is_php = $(".orderRx").attr("is_php");
        var dest_sku = $(that).parents().attr("dest_sku");
        var dest_name = $(that).parents().attr("dest_name");
        var included = $(that).parents().attr("included");
        var page_description = $(that).parents().attr("page_description");
        var relate_product = $(that).parents().attr("relate_product");
        $(".simple_name").text(dest_name);
        $(".choose-wrap").attr("dest_name",dest_name);
        var Color_Details = $(".choose-wrap").attr("Color_Details");
        var simple_name = $(".choose-wrap").attr("simple_name");
        $("#sildWarp li").each(function(idx,ele){
            $(ele).find("img").attr("alt",dest_name);
        });
        $("#min_sildWarp li").each(function(idx,ele){
            $(ele).find("img").attr("alt",dest_name);
        });
        $(".sku_text_attr").attr("dest_sku",dest_sku);
        $(".sku_text").text(dest_sku);

        $("#page_description").text(page_description);
        if(included){
            included= JSON.parse(included);
            var str_li="";
            included.forEach(function(val){
                str_li+="<li>"+val+"</li>"
            });
            $(".list-check").html(str_li);
        }
        if($("#orderRx").attr("stock")=="IN STOCK"){
            if(children_lens_type=="Reading"){
                $("#orderRx").text("Select Reading Power");
            }
            else if(children_lens_type=="Non_Rx"){
                $("#orderRx").text("Order Non-RX Glasses");
            }else if(children_lens_type=="Frame_Only"){
                $("#orderRx").text("Add to Cart");
            }else{
                $("#orderRx").text("Order Glasses");
            }
        }
        if(children_lens_type=="Reading"){
            $("#orderRx").attr("text","Select Reading Power");
        }
        else if(children_lens_type=="Non_Rx"){
            $("#orderRx").attr("text","Order Non-RX Glasses");
        }else if(children_lens_type=="Frame_Only"){
            $("#orderRx").attr("text","Add to Cart");
        } else{
            $("#orderRx").attr("text","Order Glasses");
        }
        $("#orderRx").attr("lens-type",children_lens_type);
        $("#orderRx").attr("frame_group",children_frame_group);
        $("#pdp-color_list").attr("lens_type",children_lens_type);
        if(base_url && simple_url) {
            var new_url = base_url+"/"+simple_url;
            // 当前替换好的参数，改变url地址
            $(".btn-color-click").each(function(idx,ele){
                if($(ele).attr("class").indexOf("active")!=-1){
                    $(ele).attr("ull",new_url);
                }
            });
            history.replaceState(null, null,new_url);
        }

        var price_list = {};
        var sales_price_list = {};
        $(".btn-color-click").each(function(idx,ele){
            if($(ele).attr("class")){
                if($(ele).attr("class").indexOf("active")!=-1){
                    price_list = $(ele).attr("price_list");
                    if(price_list){
                        price_list = JSON.parse(price_list);
                    }
                    sales_price_list = $(ele).attr("sales_price_list");
                    if(sales_price_list){
                        sales_price_list = JSON.parse(sales_price_list);
                    }
                }
            }
        });

        var new_price_list = price_list[children_lens_type];
        var new_sales_price_list = sales_price_list[children_lens_type];
        if(new_price_list){
            price =  new_price_list;
        }
        if(new_sales_price_list){
            sales_price =  new_sales_price_list;
        }


        var simple_obj = {
            simple_sku:dest_sku,
            simple_colorString:$(".choose-wrap").attr("color_string"),
            simple_stock_sku:$(".choose-wrap").attr("stock_sku"),
            support_ditto:$("#btn-Try").attr("flag_tryon")?1:0,
            simple_name:dest_name
        };

        if(sales_price && sales_price!=price){
            sales_price = parseFloat(sales_price).toFixed(2);
            price = parseFloat(price).toFixed(2);
            var from_str = "<p class='from_class'>From: </p>";
            if(children_lens_type=="Blue_Non_Rx"){
                from_str="";
            }
            str_price+=from_str+"<del class='del_price price_class'>$"+price+"</del><div class='price h4 price_class' style='margin-right: 5px;'>$"+sales_price+"</div>";
            simple_obj["simple_price"] = sales_price;
        }else{
            var from_str = "<a class='from_class_a'>From: </a>";
            if(children_lens_type=="Blue_Non_Rx"){
                from_str="";
            }
            price = parseFloat(price).toFixed(2);
            str_price+=from_str+"<div class='price h4 price_class'>$"+price+"</div>";
            simple_obj["simple_price"] = price;
        }


        $(".price_text").html(str_price);


        if(relate_product && relate_product!="[]" && relate_product!="false"){
            relate_product = JSON.parse(relate_product);
            if(!isIE){
                $(".relate_class").removeClass("hide");
            }
            $(".relate_class a").attr("href","/"+relate_product.url);
        }else{
            if(!isIE){
                $(".relate_class").addClass("hide")
            }
        }
        var entity_id = $(".choose-wrap").attr("entity_id");
        //add 电商增强

        try {
            GoogleGTMEnabled(configureProductObj,entity_id,simple_obj,'productDetail',"lens_type");
        } catch (e) {
            console.log(e);
        }


        //数据采集 【lens_type切换】
        try {
            var stock_sku = $("#orderRx").attr("stock_sku");
            var color_value = $("#pdp-color_list").attr("color_value");
            var is_tryon = $("#btn-Try").attr("flag_tryon");
            if(is_tryon=="true"){
                is_tryon = true;
            }else{
                is_tryon = false;
            }
            acquisition_fn("productview",acquisition_data_fn("lens_type",entity_id,dest_sku,stock_sku,dest_name,price,sales_price,is_tryon,color_value));
        } catch(e){
            console.log(e);
        }
        //数据采集 【lens_type切换】


        /****  luanjue  lensType切换采集数据   pdp_lens_type   start    ***/
        if(smartOn == 1) {
            var ljSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
            var ljConfigurableID = $("#orderRx").attr("configure_product_id");

            var ljDataObj = {
                "configurableID": ljConfigurableID,
                "destSku": dest_sku,
                "lensType": children_lens_type,
                "sku":ljSku,
            };

            smart.track_event("pdp_lens_type", ljDataObj);
        }
        /****  luanjue  lensType切换采集数据   pdp_lens_type   end    ***/

    });
    //End
    //夹片
    $(".clipon_close").on("click",function(){
        $(".clipon_class").hide();
    });
    //试戴
    $(".tryon_close").on("click",function(){
        $(".tryon_class").hide();
    });
    var str_options = "";
    for(var i=1; i<=9; i++){
        str_options+="<option value="+i+">"+i+"</option>"
    }
    $(".modal-clip-select").html(str_options);
    $(".clip_on_btn1").on("click",function(){
        $(".loading-jz").show();
        var configid = $(this).attr("configid");
        var simpleid = $(this).attr("simpleid");
        var color = $(this).attr("color");
        var _qty = $(this).parent().prev(".modal-clip-num").find(".modal-clip-number option:selected").val();
        var data = {
            qty:_qty,
            product:simpleid,
        };
        data["super_attribute[93]"]=color;
        var that = this;
        $.ajax({
            type:'post',
            url: "/checkout/cart/add/product/"+configid+"/?product="+simpleid+"&qty="+_qty+"&super_attribute%5B93%5D="+color,
            data:data,
            success:function (res) {
                //加入购物车成功
                $(that).parent().next(".modal_hint").show();
                $(".loading-jz").hide();
                setTimeout(function(){
                    $(that).parent().next(".modal_hint").hide();
                },3000)
            }
        });
    });
    $("#clip_ons_click").on("click",function(){
        $(".clipon_class").show();
    });

    $("#btn-cart").on("click",function(){
        $(".loading-jz").show();
        window.location.href="/checkout/cart";
    });
    //夹片 End
    var flag_included = true;
    //移动端点击展开included
    $("#What_included").on("click",function(){
        if(flag_included){
            $(".mobel_includes").show();
            flag_included = false;
        }else{
            $(".mobel_includes").hide();
            flag_included = true;
        }
    })
});


// 将获得的数据去除回车、空格
function ljRemoveEnterOrSpace(strObj) {
    // console.log(strObj);
    var res = strObj.replace(/[\r\n]/g,"");
    res = res.replace(/[ ]/g,"");
    res = res.replace(/\ +/g,"");
    return res;
}
