
window.console = window.console || function(t) {};
//reveiws
$(function(){
    //页面滚动到顶部函数
    function scrollFn(num){
        $('html,body').animate({scrollTop:num},'slow',function(){});
    }

    //获取当前登录信息 Start
    setTimeout(function(){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
    },1000);
    //获取当前登录信息 End

    //填写表单判断函数
    function reviewsFrom(one,two,three,form,type){
        if(reviewsData.reviewHeader!="" && reviewsData.nickname!="" && reviewsData.reviewsContent!="" && reviewsData.score!=0 && reviewsData.recommend!=0 && reviewsData.email!=""){
            $("#reveiws-sub").css({
                pointerEvents:"auto",
                borderColor:"#f90",
                background:"#f90"
            });
        }else{
            $("#reveiws-sub").css({
                pointerEvents:"none",
                borderColor:"#ccc",
                background:"#ccc"
            });
        }
        if(one.data==""){
            $(one.ele).css({
                borderColor:"red"
            })
        }else{
            $(one.ele).css({
                borderColor:"#ccc"
            })
        }
        if(two.data==""){
            $(two.ele).css({
                borderColor:"red"
            })
        }else{
            $(two.ele).css({
                borderColor:"#ccc"
            })
        }
        if(three.data==""){
            $(three.ele).css({
                borderColor:"red"
            })
        }else{
            $(three.ele).css({
                borderColor:"#ccc"
            })
        }
        if(form.data==""){
            $(form.ele).css({
                borderColor:"red"
            })
        }else{
            $(form.ele).css({
                borderColor:"#ccc"
            })
        }
        if(type=="keyUp"){
            if(reviewsData.score==0){
                $(".reveiws-container .reveiws-wjx ul li .fa").css({
                    color:"red"
                })
            }else{
                $(".reveiws-container .reveiws-wjx ul li .fa").css({
                    color:"#ffb454"
                })
            }
            if(reviewsData.recommend==0){
                $(".reveiws-container .reveiws-yN .fa").css({
                    color:"red"
                })
            }else{
                $(".reveiws-container .reveiws-yN .fa").css({
                    color:"#ffb454"
                })
            }
        }
    }
    //当前提交评论的时间
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "/";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9){
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9){
            strDate = "0" + strDate;
        }
        var currentdate =   month + seperator1 + strDate + seperator1 + year;
        return currentdate;
    }
    var reviewsData = {};

    //打开评论填写区
    $(".review-pop-show").click(function(){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
        function review_data(){
            var reveiws_sku = $(".reveiws-container").attr("sku");
            var reveiws_entryId = $(".reveiws-container").attr("entry_id");
            var reveiws_price = $(".reveiws-container").attr("price");
            var reveiws_color= $(".reveiws-container").attr("color");
            var reveiws_name= $(".reveiws-container").attr("name");
            var reveiws_simpleUrl= $(".reveiws-container").attr("simple_url");
            var dataSimple = {
                sku:reveiws_sku,
                entry_id:reveiws_entryId,
                price:reveiws_price,
                color:reveiws_color,
                name:reveiws_name,
                simple_url:reveiws_simpleUrl
            };
            $(".imgs").attr("data-simple",JSON.stringify(dataSimple));
        }
        review_data();


        scrollFn(0);
        $("#fix-fluid").fadeIn();
        $("#errTips").text("This will only be used for notification purposes");
        $("#errTips").css({
            color:"#bababa"
        });
        if(loginCustomer){
            if(loginCustomer.customer.islogged=="1"){
                $("#Email_S").val(loginCustomer.customer.email);
                reviewsData.email=loginCustomer.customer.email;
            }else{
                $("#Email_S").val("");
                reviewsData.email="";
            }
        }
        var fas = document.querySelectorAll(".fas_review");
        for(var i=0; i<fas.length; i++){
            fas[i].className="fa fa-star-o fas fas_review";
        }

        $(".acs_fieldset_click span:nth-of-type(1)>span").attr("class","fa fa-thumbs-o-up");
        $(".acs_fieldset_click span:nth-of-type(2)>span").attr("class","fa fa-thumbs-o-down")
        reviewsData.score=0;
        reviewsData.recommend=0;
        reviewsData.nickname="";
        reviewsData.reviewsContent="";
        reviewsData.reviewHeader="";
        //再次评论将上次的评论内容清空
        $("#YourReview,#describe,#Nickname").val("");
    });

    reviewsData.score=0;
    reviewsData.recommend=0;
    reviewsData.nickname="";
    reviewsData.reviewsContent="";
    reviewsData.reviewHeader="";
    //评论填写页面点击Show me an example查看更多
    var flagDo = true;
    $("#reveiws-btn").click(function(){
        $(".acs-headline-examples").toggle();
        if(flagDo){
            $("#upDown").attr("class","fa fa-chevron-up");
            flagDo=false;
        }else{
            $("#upDown").attr("class","fa fa-chevron-down");
            flagDo=true;
        }
    });
    //end
    var fas = document.querySelectorAll(".fas_review");
    //评论填写页面点击五角星进行评分
    $(".reveiws-wjx ul li").click(function(){
        for(var i=0; i<fas.length; i++){
            fas[i].className="fa fa-star-o fas fas_review";
        }
        for(var i=0; i<$(this).index()+1; i++){
            fas[i].className="fa fa-star-o fas selFa fas_review";
        }
        $(".reveiws-container .reveiws-wjx ul li .fa").css({
            color:"#ffb454"
        });
        //评分赋值
        reviewsData.score = $(this).index()+1;
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            "click"
        );
    });
    //end

    $(".acs_fieldset_click span").click(function(){
        $(".reveiws-container .reveiws-yN .fa").css({
            color:"#ffb454"
        });
        if($(this).attr("val")=='yes'){
            reviewsData.recommend="yes";
            $(this).find("#reveiws-box").attr("class","fa fa-thumbs-up");
            $(".acs_fieldset span:nth-of-type(2)>span").attr("class","fa fa-thumbs-o-down");
        }else{
            reviewsData.recommend="no";
            $(this).find("#reveiws-box").attr("class","fa fa-thumbs-down");
            $(".acs_fieldset span:nth-of-type(1)>span").attr("class","fa fa-thumbs-o-up");
        }
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            "click"
        );
    });
    //填写评论内容时间
    var sum = parseInt($("#DescribeNum").text());
    $("#describe").keyup(function(){
        var newValue = $(this).val();
        var newSum = (sum - $(this).val().length);
        if(newSum<=0){
            newValue = $(this).val().slice(0,sum);
            $("#DescribeNum").text(0);
        }else{
            $("#DescribeNum").text(newSum);
        }
        reviewsData.reviewsContent = newValue;
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            {
                data:$(this).val(),
                ele:this
            },
            "keyUp"
        );

    });
    //end
    $("#YourReview").keyup(function(){
        reviewsData.reviewHeader = $(this).val();
        reviewsFrom(
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            {
                data:$(this).val(),
                ele:this
            },
            "keyUp"
        );
    });
    $("#Email_S").keyup(function(){
        reviewsData.email = $(this).val();
        $("#errTips").text("This will only be used for notification purposes");
        $("#errTips").css({
            color:"#bababa"
        });
        $("#Email_S").css({
            color:"#444"
        });
        reviewsFrom(
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:$(this).val(),
                ele:this
            },
            "keyUp"
        );
    });
    $("#Nickname").keyup(function(){
        reviewsData.nickname = $(this).val();
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            {
                data:$(this).val(),
                ele:this
            },
            "keyUp"
        );
    });
    var hei = document.querySelector(".pdp-parameters").offsetTop;
    var newHei = 0;
    //删除指定参数函数
    function delParam(paramKey) {
        var url = window.location.href;    //页面url
        var urlParam = window.location.search.substr(1);   //页面参数
        var beforeUrl = url.substr(0, url.indexOf("?"));   //页面主地址（参数之前地址）
        var nextUrl = "";

        var arr = new Array();
        if (urlParam != "") {
            var urlParamArr = urlParam.split("&"); //将参数按照&符分成数组
            for (var i = 0; i < urlParamArr.length; i++) {
                var paramArr = urlParamArr[i].split("="); //将参数键，值拆开
                //如果键雨要删除的不一致，则加入到参数中
                if (paramArr[0] != paramKey) {
                    arr.push(urlParamArr[i]);
                }
            }
        }
        if (arr.length > 0) {
            nextUrl = "?" + arr.join("&");
        }
        url = beforeUrl + nextUrl;
        return url;
    }
    var closeHref = delParam("comment");
    //end

    $(".share_click").click(function(){
        $(".reveiews-item").eq(1).show().siblings().hide();
        $(".h_g_one").css({
            width:"0%"
        });
        $(".h_g_two").css({
            width:"100%"
        });
        $("#tab-label-additional_reviews").find(".bus-downB").addClass("bus-down-active").parent().siblings(".bus-active").find(".bus-downB").removeClass("bus-down-active");
    });

    //输入错误函数
    function styleIpt(ele){
        $(ele).css({
            borderColor:"red"
        })
    }
    $("#reveiws-sub,#reveiws-cancel,.close-fluid").click(function(){
        //当关闭，或者提交后，将参数url地址回复为默认值
        history.replaceState(null, null, closeHref);
        if($(this).text()=="SUBMIT"){
            //评论标题填写val值
            var YourReview = $("#YourReview").val();
            //评论填写当前名字
            var Nickname = $("#Nickname").val();
            //评论内容获取
            var PreiwsContent = $("#describe").val();
            //邮箱内容
            var Email = $("#Email_S").val();
            var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            reviewsData.reviewHeader = YourReview;
            reviewsData.nickname = Nickname;
            reviewsData.email = Email;
            if(YourReview==""){
                styleIpt("#YourReview");
            }
            if(Nickname==""){
                styleIpt("#Nickname");
            }
            if(PreiwsContent==""){
                styleIpt("#describe");
            }

            if(Email=="" || !reg.test(Email)){
                styleIpt("#Email_S");
            }
            if(!reg.test(Email)){
                $("#errTips").text("Incorrect mailbox format");
                $("#errTips").css({
                    color:"red"
                });
            }
            if(reviewsData.score==0){
                $(".reveiws-container .reveiws-wjx ul li .fa").css({
                    color:"red"
                })
            }

            if(reviewsData.recommend==0){
                $(".reveiws-container .reveiws-yN .fa").css({
                    color:"red"
                })
            }

            if(YourReview!="" && Nickname!="" && PreiwsContent!="" && reviewsData.score!=0 && reviewsData.recommend!=0 && Email!="" && reg.test(Email)){
                //当前产品的simple;
                reviewsData.simples = JSON.parse($(".imgs").attr("data-simple"));
                //当前提交的时间
                reviewsData.reviewsDate = getNowFormatDate();
                $(".reveiews-item").eq(1).show().siblings().hide();
                $("#tab-label-additional_reviews").find(".bus-downB").addClass("bus-down-active").parent().siblings(".bus-active").find(".bus-downB").removeClass("bus-down-active");
                $(".h_g_one").css({
                    width:"0%"
                });
                $(".h_g_two").css({
                    width:"100%"
                });
                $("#fix-fluid").fadeOut();
                // $(".container_conter").show();
                // $(".reveiws-container").hide();
                newHei=(hei-200);
                if(reviewsData.recommend=="yes"){
                    reviewsData.recommend=1;
                }else{
                    reviewsData.recommend=0;
                }
                var data = {
                    simple_id:reviewsData.simples.entry_id,
                    subject:reviewsData.reviewHeader,
                    post_content:reviewsData.reviewsContent,
                    review_rating:reviewsData.score,
                    nickname:reviewsData.nickname,
                    will_recommend:reviewsData.recommend,
                    email:reviewsData.email
                };
                $.ajax({
                    url:"/reviews/product/save",
                    type: "POST",
                    data:data,
                    success:function(res){
                        $(".succes_review").removeClass("hide");
                        $(".succes_review").text("Thanks for your review ! Your review will be live in a few days.");
                        setTimeout(function(){
                            $(".succes_review").addClass("hide");
                        },3000);
                        scrollFn(newHei);
                    },error:function(err){

                    }
                });
            }else{
                $(this).css({
                    pointerEvents:"none",
                    borderColor:"#ccc",
                    background:"#ccc"
                });
                newHei=200;
            }
        }else{
            newHei=0;
            $("#fix-fluid").fadeOut();
            scrollFn(newHei);
        }

    });

    function currentPage(datas,fn,fn1){
        $.ajax({
            url:"/reviews/product/readreviews",
            type:"POST",
            data:datas,
            success:fn,
            beforeSend:fn1

        });
    }



    setTimeout(function() {
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
        if (Object.prototype.toString.call(loginCustomer) === '[object Object]') {
            var is_login = loginCustomer.customer.islogged;
            if (is_login == 1) {
                class_like = 'position:relative;padding-bottom:50px';
                var is_like_str = "";
                var acs_fieldsetS = "false";
                var dz_html = "";
                $(".data_list_reviews").each(function (idx, ele) {
                    var is_like = $(ele).attr("is_like");
                    var likes_num = $(ele).attr("likes_num");
                    var reviews_id = $(ele).attr("reviews_id");
                    if (is_like == "1") {
                        is_like_str = "fa-thumbs-up";
                        acs_fieldsetS = "false";
                    } else {
                        is_like_str = "fa-thumbs-o-up";
                        acs_fieldsetS = "true";
                    }
                    dz_html = "<div class='helpful'>Was this review helpful?" +
                        "<fieldset class='acs_fieldset'  fieldset_attr=" + acs_fieldsetS + " reviews_id=" + reviews_id + "><span val='yes'>" +
                        "<span class='fa " + is_like_str + " zc-box'>" +
                        "</span><p for='rec0' class='rec0 rec'>" +
                        "<span style='margin-left: 5px' class='reveiewNum'>(" + likes_num + ")</span></p></span></fieldset>" +
                        "</div>";
                    $(ele).append(dz_html);
                });
            } else {
                dz_html = "";
            }
        } else {
            dz_html = "";
        }
    },1100);


    function pageFn(nums){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
        //获取当前是否有评论数据
        if(nums>=1){
            currentPage(
                {
                    reviewspage:nums,
                    id:$(".stock_text").attr("configurable_id")
                },function(res){
                    $(".smak_review").addClass("hide");
                    var review_str="";
                    var review_list = res.data;
                    review_list.forEach(function(val){
                        var dz_html="";
                        var class_like = 'position:relative; padding-bottom';
                        if(Object.prototype.toString.call(loginCustomer) === '[object Object]'){
                            var is_login  = loginCustomer.customer.islogged;
                            if(is_login==1){
                                class_like='position:relative;padding-bottom:50px';
                                var is_like_str = "";
                                var acs_fieldsetS = "false";
                                if(val.is_like=="1"){
                                    is_like_str="fa-thumbs-up";
                                    acs_fieldsetS = "false";
                                }else{
                                    is_like_str="fa-thumbs-o-up";
                                    acs_fieldsetS = "true";
                                }
                                dz_html+="<div class='helpful'>Was this review helpful?"+
                                    "<fieldset class='acs_fieldset'  fieldset_attr="+acs_fieldsetS+" reviews_id="+val.reviews_id+"><span val='yes'>" +
                                    "<span class='fa "+is_like_str+" zc-box'>"+
                                    "</span><p for='rec0' class='rec0 rec'>"+
                                    "<span style='margin-left: 5px' class='reveiewNum'>("+val.likes_num+")</span></p></span></fieldset>"+
                                    "</div>";
                            }else{
                                dz_html="";
                            }
                        }else{
                            dz_html="";
                        }
                        var stars_num = val.review_rating;
                        stars_num =  stars_num*18;
                        review_str+="<div class='' style='margin-top: 10px;width: 100%'><article class='item-comment box' style="+class_like+">"+
                        "<div class='icontext align-items-start'>"+
                        "<img src='https://static.payneglasses.com/assets2/resource/pdp-summary/images/avatar.png' class='icon avatar-md'>"+
                        "<div class='text'>"+
                        "<h6 class='mb-1'>"+val.subject+"<small class='text-muted'> By "+val.nickname+" "+val.post_date_html+"  </small></h6>"+
                            "<span class='zx-sort'>"+ val.review_rating+"</span>"+
                        "<ul class='rating-stars'>" +
                            "<li style='width:"+stars_num+"%' class='stars-active xx_class'>"+
                            "<i class='fa fa-star'></i> <i class='fa fa-star'></i>"+
                            "<i class='fa fa-star'></i> <i class='fa fa-star'></i>"+
                            "<i class='fa fa-star'></i></li>"+
                            "<li class='xx_class'>"+
                            "<i class='fa fa-star'></i> <i class='fa fa-star'></i>"+
                            "<i class='fa fa-star'></i> <i class='fa fa-star'></i>"+
                            "<i class='fa fa-star'></i></li>"+
                            "<span>color: "+val.color+"</span>" +
                            "</ul></div></div><div class='mt-3'>"+
                        "<p class='content_worp'>"+val.post_content+"</p></div>" + dz_html +
                            "</article></div>"
                    });
                    $("#reviews_list").append(review_str);
                },function(){
                    $(".smak_review").removeClass("hide");
                }
            );
        }
    }

    //当前点赞
    $(document).on("click",".acs_fieldset",function(){
        var review_id=  $(this).attr("reviews_id");
        var that = this;
        $(".loading-jz").show();
        if($(this).attr("fieldset_attr")=="true"){
            $.ajax({
                url:"/reviews/like/add",
                type:"POST",
                data:{
                    reviews_id:review_id
                },
                success:function (res) {
                    if(res.status){
                        setTimeout(function(){
                            $(".loading-jz").hide();
                        },2000);
                        $(that).find(".fa").attr("class","fa fa-thumbs-up zc-box");
                        $(that).attr("fieldset_attr","false");
                        $(that).find(".reveiewNum").text("("+res.data.likes_num+")");
                    }else{
                        $(".loading-jz").hide();
                    }
                }
            });
        }else{
            setTimeout(function(){
                $(".loading-jz").hide();
            },3000)
        }
    });


    var num = 1;
    $(".review_show_more").on("click",function(){
        var sum_page = $(this).attr("reviews_page");
        num++;
        if(parseInt(sum_page)<=num){
            $(this).hide();
        }
        pageFn(num);
    });


    $('.circle').each(function(index, el) {
        var num = $(this).find('span').text() * 3.6;
        if (num<=180) {
            $(this).find('.right_circle').css('transform', "rotate(" + num + "deg)");
        } else {
            $(this).find('.right_circle').css('transform', "rotate(180deg)");
            $(this).find('.left_circle').css('transform', "rotate(" + (num - 180) + "deg)");
        };
    });
    if (document.location.search.match(/type=embed/gi)) {
        window.parent.postMessage("resize", "*");
    }
});
