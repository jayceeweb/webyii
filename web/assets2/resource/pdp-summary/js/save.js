$(function(){
    function save_Ajax(url,type,data,fn){
        $.ajax({
            url:url,
            type:type,
            data:data,
            success:fn
        })
    }

    //获取当前登录信息 Start
    setTimeout(function(){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
    },1000);
    //获取当前登录信息 End
    save_Ajax("/e/customer/Wishlist/search","POST",{simple_ids:simpleids_string},function(res){
        // save_Ajax("/wishlist/index/search","POST",{simple_ids:simpleids_string},function(res){
        var currentId = $("#save_x").attr("currentId");
        if(res.status){
            var objData = res.data;
            for(var keys in objData){
                if(objData[currentId]){
                    $('#save_x i').addClass("save_after_one");
                    $('#save_x i').removeClass("save_after_two");
                    break;
                }else{
                    $('#save_x i').addClass("save_after_two");
                    $('#save_x i').removeClass("save_after_one");
                }
            }
            $('#save_x').attr("arr_simple",JSON.stringify(objData));
        }else{
            // 匿名收藏 Start
            nm_save_fn(currentId);
            // 匿名收藏 end
        }
    });

    // 匿名收藏 start
    //未登录时 加载favorite
    function nm_save_fn(current_Id){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
        var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
        var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
        $.each(favorite_product_unknownCustemers,function(n,v){
            if(current_Id === v.id){
                // 添加收藏
                $('#save_x i').addClass("save_after_one");
                $('#save_x i').removeClass("save_after_two");
                return false;
            }else{
                // 取消收藏
                $('#save_x i').addClass("save_after_two");
                $('#save_x i').removeClass("save_after_one");
            }
        });
        // 匿名收藏 end
        var save_length = favorite_product_unknownCustemers.length;
        setTimeout(function(){
            var is_login  = loginCustomer.customer.islogged;
            if (is_login==0) {
                if(save_length>=1){
                    $(".SavedFrames").text("Saved Frames("+save_length+")");
                }else{
                    $(".SavedFrames").text("Saved Frames");
                }
                $(".SavedFrames").attr("href","/anonymouswishlist");
            }
        },1100);
    }


    $(".btn-color-click").on("click",function(){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
        var curent_id = $(this).attr("entity_id");
        $("#save_x").attr("currentid",curent_id);
        var arr_simple  = $('#save_x').attr("arr_simple");
        var is_login  = loginCustomer.customer.islogged;
        if(!is_login){
            // 匿名收藏 Start
            nm_save_fn(curent_id);
            // 匿名收藏 end
        }else{
            if(typeof(arr_simple)=="string"){
                arr_simple  = JSON.parse(arr_simple);
            }
            for(var key in arr_simple){
                if(arr_simple[curent_id]){
                    $('#save_x i').addClass("save_after_one");
                    $('#save_x i').removeClass("save_after_two");
                }else{
                    $('#save_x i').addClass("save_after_two");
                    $('#save_x i').removeClass("save_after_one");
                }
            }
        }
    });


    /*匿名收藏 Start
     *
     *  在未登陆时，将收藏的产品保存到本地缓存中
     *
     *
     * cookie name is pg-unlogin-wishlist
     * */
    // setUnknownCustomerFavorite:function(favoriteId,_this){
    function setUnknownCustomerFavorite(favoriteInfo,favoriteId){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
        var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
        var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
        var index_existFavoriteId = -1;
        for(var i =0;i<favorite_product_unknownCustemers.length;i++){
            if(favorite_product_unknownCustemers[i].id===favoriteId){
                index_existFavoriteId = i;
            }
        }
        if( index_existFavoriteId>=0){
            //取消收藏
            favorite_product_unknownCustemers.splice(index_existFavoriteId,1);
            $('#save_x i').addClass("save_after_two");
            $('#save_x i').removeClass("save_after_one");
            var Noislogin_profile = $("#yourselfName2").text();
            $("#tip_save").text("Item has been removed for "+Noislogin_profile);
            $("#tip_save").show();
            setTimeout(function(){
                $("#tip_save").hide();
            },2000);
        }
        else{
            favorite_product_unknownCustemers.push(favoriteInfo);
            $('#save_x i').addClass("save_after_one");
            $('#save_x i').removeClass("save_after_two");
        }
        var productids =[];
        $.each(favorite_product_unknownCustemers,function(n,v){
            productids.push(v.id);
        });
        var productidsString =JSON.stringify(productids);
        // $.cookie('pg-unlogin-wishlist',productidsString,{expires:3650,path:'/'});
        document.cookie="pg-unlogin-wishlist="+productidsString+"; expires=3650; path=/";
        favorite_product_unknownCustemersJsonString = JSON.stringify(favorite_product_unknownCustemers);
        window.localStorage.setItem('favorite_product_unknownCustemers',favorite_product_unknownCustemersJsonString);
        var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
        var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
        var save_length = favorite_product_unknownCustemers.length;
        var is_login  = loginCustomer.customer.islogged;
        if (is_login==0) {
            if(save_length>=1){
                $(".SavedFrames").text("Saved Frames("+save_length+")");
            }else{
                $(".SavedFrames").text("Saved Frames");
            }
        }

    };
    // 匿名收藏 End

    $("#save_x").on("click",function(){
        var loginCustomer = $("#webYi_customerInfo").attr("loginCustomer");
        if(loginCustomer){
            loginCustomer = JSON.parse(loginCustomer);
        }else{
            loginCustomer = {
                customer:{
                    islogged:0
                }
            };
        }
        var is_login  = loginCustomer.customer.islogged;
        $(".loading-jz").show();
        //已登录状态
        if(is_login!=0){
            var currentid = $(this).attr("currentid");
            var arr_simple  = $('#save_x').attr("arr_simple");
            if(typeof(arr_simple)=="string"){
                arr_simple  = JSON.parse(arr_simple);
            }
            var current_item = arr_simple[currentid];
            if(current_item){
                var obj = {
                    item:current_item['wishlist_item_id'],
                    uenc:current_item['sharing_code']
                };
                save_Ajax("/wishlist/index/remove/?ajax=true","POST",obj,function(res){
                    if(res.success){
                        var save_count = res.profile.count;
                        if(save_count==0){
                            $(".SavedFrames").text("Saved Frames");
                        }else{
                            $(".SavedFrames").text("Saved Frames("+save_count+")");
                        }
                        if(res.profile instanceof Object){
                            var currentProfile = res.profile.nickname;
                            if(currentProfile){
                                $("#tip_save").text("Item has been removed for "+currentProfile);
                            }
                        }
                        $("#tip_save").show();
                        setTimeout(function(){
                            $("#tip_save").hide();
                        },2000);
                        arr_simple[currentid] = null;
                        $('#save_x').attr("arr_simple",JSON.stringify(arr_simple));
                        $(".loading-jz").hide();
                    }
                });
                $('#save_x i').addClass("save_after_two");
                $('#save_x i').removeClass("save_after_one");
            }else{
                save_Ajax("/wishlist/index/add/?ajax=true","POST",{product:currentid},function(res){
                    if(res){
                        res = JSON.parse(res);
                        var save_count = res.data.count;
                        if(save_count==0){
                            $(".SavedFrames").text("Saved Frames");
                        }else{
                            $(".SavedFrames").text("Saved Frames("+save_count+")");
                        }
                        $(".loading-jz").hide();
                    }
                    var obj = {
                        product_id:currentid,
                        wishlist_item_id:res.data.item,
                        sharing_code:res.data.uenc
                    };
                    arr_simple[currentid] = obj;
                    $('#save_x').attr("arr_simple",JSON.stringify(arr_simple));
                });
                $('#save_x i').addClass("save_after_one");
                $('#save_x i').removeClass("save_after_two");
            }
        }
        //未登录状态
        else{
            //匿名收藏 Start
            var productInfoObject  = {};
            $(".btn-color-click").each(function(n,ele){
                if($(ele).attr("class").indexOf("active")!=-1){
                    productInfoObject  = {
                        name:$(ele).attr("name"),
                        img:$(ele).attr("thumbnail"),
                        price:$(ele).attr("price"),
                        link:$(ele).attr("ull"),
                        id:$(ele).attr("entity_id")
                    };
                }

            });
            setTimeout(function(){
                $(".loading-jz").hide();
            },1000);
            setUnknownCustomerFavorite(productInfoObject,productInfoObject.id);
            //匿名收藏 End
            // window.location.href = "/customer/account/login";
        }
    });
});
