$(document).ready(function() {
    var configureProductObj = {
        entity_id:$("#share_data").attr("configure_product_id"),
        name:$("#share_data").attr("name"),
    };

    var simple_url = window.location.href;
    var _is_shared = $("#share_data").attr("_is_shared");
    var _share_price = $("#share_data").attr("_share_price");
    var _final_price = $("#share_data").attr("_final_price");
    var thumbnail_url = $("#share_data").attr("thumbnail_url");
    var support_share_and_save = $("#share_data").attr("support_share_and_save");

    function sharePrice() {
        // 没有分享过 && 有分享价格 && share price's type is number && share price less than final price
        if (_is_shared == 0 && _share_price != undefined &&
            !isNaN(parseFloat(_share_price)) && !isNaN(parseFloat(_final_price)) &&
            parseFloat(_final_price) > parseFloat(_share_price)) {
            //状态改为已分享
                _is_shared=1;
                $("#price").html(_share_price);
            }
    }

    // 将获得的数据去除回车、空格
    function ljRemoveEnterOrSpace(strObj) {
        // console.log(strObj);
        var res = strObj.replace(/[\r\n]/g,"");
        res = res.replace(/[ ]/g,"");
        res = res.replace(/\ +/g,"");
        return res;
    }

    configure_product_id=configureProductObj["entity_id"];
    var _window_height = $(window).height();
    var _window_width = $(window).width();

    window.fbAsyncInit = function() {
        FB.init({
            // appId            : facebook_app_id,
            appId            : "267011757149662",
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.2'
        });
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    /*** luanjue  数据采集初始数据   start   ***/
    var smartOn = $("#smartCollectOn").val();
    if(smartOn == 1) {
        smart.init('collectionPdp',{
            loaded: function(sdk) {
                // luanjue_3  清除super_properties
                sdk.clear_event_super_properties();
            }
        });

        //  15、pdp页面点击分享  luanjue
        smart.time_event('pdp_share');
    }

    function getShareDataLJ() {
        var ljProductColor = $(".btn-color-click.active").attr("simple_product_color");
        var ljColorValue = $(".btn-color-click.active").attr("color_value");
        var ljDestSku = $("label.active").attr("dest_sku");
        var ljPrice = $("label.active").attr("price");
        var ljSalesPrice = $("label.active").attr("sales_price");
        var ljLensType = $("label.active").attr("lens-type");
        var ljFrameGroup = $("label.active").attr("frame_group");
        ljFrameGroup = ljRemoveEnterOrSpace(ljFrameGroup) ;
        var ljSimpleUrl = $("label.active").attr("simple_url");
        var ljStockSku = $("#orderRx").attr("stock_sku");
        var reviewSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
        var ljEntityID = $("#orderRx").attr("simple_product_id");
        var ljConfigurableID = $("#orderRx").attr("configure_product_id");
        var ljConfigureProductSku = $("#orderRx").attr("configure_product_sku");
        var ljCategoryID = $("#orderRx").attr("category_id");
        var ljCatetoryType = $("#orderRx").attr("catetory_type");
        var ljStock = $("#orderRx").attr("stock");
        var ljFrameGroupHref = $("#orderRx").attr("frame_group_href");

        var ljDataObj = {
            "productColor": ljProductColor,
            "productColorValue": ljColorValue,
            "destSku": ljDestSku,
            "price": ljPrice,
            "salesPrice": ljSalesPrice,
            "lensType": ljLensType,
            "frameGroup": ljFrameGroup,
            "frameGroupHref": ljFrameGroupHref,
            "simpleUrl": ljSimpleUrl,
            "stockSku": ljStockSku,
            "reviewSku":reviewSku,
            "entityID": ljEntityID,
            "configurableID": ljConfigurableID,
            "configureProductSku": ljConfigureProductSku,
            "categoryID": ljCategoryID,
            "catetoryType": ljCatetoryType,
            "stock": ljStock
        };

        return ljDataObj;
    }
    /*** luanjue  数据采集初始数据   end   ***/


    $(".fb-share").click(function () {
        if (FB) {
            FB.ui({
                method: 'share',
                mobile_iframe: true,
                href: simple_url
            }, function (response) {
                // luanjue 数据采集   start
                if(smartOn == 1) {
                    var ljDataObj = getShareDataLJ();
                    ljDataObj["shareType"] = 1;
                    smart.track_event("pdp_share", ljDataObj);
                }
                // luanjue 数据采集    end

                if (response && !response.error_message && support_share_and_save) {
                    $.ajax({
                        url: '/discount/share/success',
                        data: {'product_id': configure_product_id, 'share': true},
                        success: function (data) {
                            if (data.success) {
                                sharePrice();
                            }
                        }
                    });
                }
            });
        }
    });

    $(".tw-share").click(function () {
        // luanjue 数据采集   start
        if(smartOn == 1) {
            var ljDataObj = getShareDataLJ();
            ljDataObj["shareType"] = 2;
            smart.track_event("pdp_share", ljDataObj);
        }
        // luanjue 数据采集    end

        var _tw_window = window.open(
            'http://twitter.com/intent/tweet?text=' + simple_url,
            'newwindow',
            'height=' + _window_height * 0.8 + ',width=' + _window_width * 0.8 + ',top=111,left=111,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no'
        );
        if (support_share_and_save) {
            $.ajax({
                url: '/discount/share/success',
                data: {'product_id': configure_product_id, 'share': true},
                success: function (data) {
                    if (data.success) {
                        sharePrice();
                    }
                }
            });
        }
    })

    $(".pint-share").click(function () {
        // luanjue 数据采集   start
        if(smartOn == 1) {
            var ljDataObj = getShareDataLJ();
            ljDataObj["shareType"] = 3;
            smart.track_event("pdp_share", ljDataObj);
        }
        // luanjue 数据采集    end

        window.open(
            'http://pinterest.com/pin/create/button/?url=' + simple_url + '&description=' + configureProductObj["name"] + '&media=' + thumbnail_url,
            'newwindow',
            'height=' + _window_height * 0.8 + ',width=' + _window_width * 0.8 + ',top=111,left=111,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no'
        );
        if (support_share_and_save) {
            $.ajax({
                url: '/discount/share/success',
                data: {'product_id': configure_product_id, 'share': true},
                success: function (data) {
                    if (data.success) {
                        sharePrice();
                    }
                }
            });
        }
    })
});
