// define([
//     'jquery',
//     'Magento_Customer/js/model/authentication-popup'
// ], function ($, authenticationPopup) {
//     var isCurrentSkuAvailableShare = $('.share-price-sentence:visible').length,
//
//     // isNeededLogin = 1; You need to login when you share this product.
//     // isNeededLogin = 0; There is no need to login when you share this product.
//         isNeededLogin = 0,
//
//         _product_id = $('[data-product-id]').attr('data-product-id');
//
//     function sharePrice() {
//         // The price after current SKU is shared.
//         var _share_price = $('[name=cur_share_price]').val(),
//
//             //0 refers to this SKU hasn't been shared.
//             //1 refers to this SKU has been shared.
//             _is_shared = $('[name=is_shared]').val(),
//             _final_price  = $('[data-price-type="finalPrice"] .price').text().replace('$','');
//
//         // 没有分享过 && 有分享价格 && share price's type is number && share price less than final price
//         if (_is_shared == 0 && _share_price != undefined &&
//             !isNaN(parseFloat(_share_price)) && !isNaN(parseFloat(_final_price)) &&
//             parseFloat(_final_price) > parseFloat(_share_price)) {
//             //状态改为已分享
//             $('[name=is_shared]').val(1);
//             _is_shared = 1;
//
//             if($('[data-role=priceBox]').find('.special-price').length == 0){
//                 $('.price-container.price-final_price').wrap('<span class="special-price"></span>');
//             }
//
//             $('[itemprop="price"] .price').text('$' + _share_price);
//
//             if(_is_shared){
//                 $('.share-price-sentence .after-share').removeClass('hide').siblings().addClass('hide');
//             }else{
//                 $('.share-price-sentence .before-share').removeClass('hide').siblings().addClass('hide');
//             }
//
//             if ($('.old-price.sly-old-price').length) {
//                 $('.old-price.sly-old-price').removeClass('hide').find('[data-price-type=oldPrice] .price').text(_final_price);
//             } else {
//                 $('<span class="old-price sly-old-price no-display"> ' +
//                     '<span class="price-container price-final_price tax weee">' +
//                     '<span class="price-label">was</span> ' +
//                     '<span data-price-type="oldPrice" class="price-wrapper ">' +
//                     '<span class="price">' +
//                     _final_price +
//                     '</span></span> </span> </span>').appendTo(".price-box");
//             }
//         }
//     }
//
//     function getShareUrl() {
//         var _short_url = $('.PDP-share-social-list').attr('data-short-url'),
//             _url = _short_url ? _short_url :window.location.href;
//
//         return _url;
//     }
//
//     return function (config, node) {
//         // config.isShareDiscount = 0; No discount after sharing this product.
//         //                          1;
//         // console.log(config);
//         $('.fb-share').on('click', function () {
//             if (isNeededLogin) {
//                 $.cookie('login_redirect', location.href);
//                 authenticationPopup.showModal();
//                 return false;
//             } else {
//                 if (FB) {
//                     FB.ui({
//                         method: 'share',
//                         mobile_iframe: true,
//                         href: getShareUrl()
//                     }, function (response) {
//                         isCurrentSkuAvailableShare = $('.share-price-sentence:visible').length;
//                         if (response && !response.error_message && config.isShareDiscount && isCurrentSkuAvailableShare > 0) {
//                             $.ajax({
//                                 url: '/discount/share/success',
//                                 data: {'product_id': _product_id, 'share': true},
//                                 success: function (data) {
//                                     if (data.success) {
//                                         sharePrice();
//                                     }
//                                 }
//                             });
//                         }
//                     });
//                 }
//             }
//         });
//
//         $('.tw-share').on('click', function () {
//             if (!isNeededLogin) {
//                 var _window_height = $(window).height(),
//                     _window_width = $(window).width();
//
//                 var _tw_window = window.open(
//                     'http://twitter.com/intent/tweet?text=' + getShareUrl(),
//                     'newwindow',
//                     'height=' + _window_height * 0.8 + ',width=' + _window_width * 0.8 + ',top=111,left=111,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no'
//                 );
//                 isCurrentSkuAvailableShare = $('.share-price-sentence:visible').length;
//                 if (config.isShareDiscount && isCurrentSkuAvailableShare > 0) {
//                     $.ajax({
//                         url: '/discount/share/success',
//                         data: {'product_id': _product_id, 'share': true},
//                         success: function (data) {
//                             if (data.success) {
//                                 sharePrice();
//                             }
//                         }
//                     });
//                 }
//             } else {
//                 $.cookie('login_redirect', location.href);
//                 authenticationPopup.showModal();
//                 return false;
//             }
//         });
//
//         $('.pint-share').on('click', function () {
//             if (!isNeededLogin) {
//                 var _window_height = $(window).height(),
//                     _window_width = $(window).width();
//
//                 window.open(
//                     'http://pinterest.com/pin/create/button/?url=' + getShareUrl() + '&description=' + $('[itemprop="name"]').text() + '&media=' + $('#productItems img:first').attr('src'),
//                     'newwindow',
//                     'height=' + _window_height * 0.8 + ',width=' + _window_width * 0.8 + ',top=111,left=111,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no'
//                 );
//                 isCurrentSkuAvailableShare = $('.share-price-sentence:visible').length;
//                 if (config.isShareDiscount && isCurrentSkuAvailableShare > 0) {
//                     $.ajax({
//                         url: '/discount/share/success',
//                         data: {'product_id': _product_id, 'share': true},
//                         success: function (data) {
//                             if (data.success) {
//                                 sharePrice();
//                             }
//                         }
//                     });
//                 }
//             } else {
//                 $.cookie('login_redirect', location.href);
//                 authenticationPopup.showModal();
//
//                 return false;
//             }
//         });
//     };
// });