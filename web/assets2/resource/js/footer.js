//如果不为pc端的时候，将a标签的href去除(2019,9,4)
function browserRedirect() {
    var sUserAgent = navigator.userAgent.toLowerCase();
    var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
    var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
    var bIsMidp = sUserAgent.match(/midp/i) == "midp";
    var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
    var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
    var bIsAndroid = sUserAgent.match(/android/i) == "android";
    var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
    var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
    if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
        return "phone"
    } else {
        return "pc"
    }
}
var a = document.querySelectorAll('.isHref');
// function hover(ele,over,out){
//     ele.addEventListener('mouseover',over,false);
//     ele.addEventListener('mouseout',out,false);
// }
// if (browserRedirect() == "pc") {
//     for(var i=0; i<a.length; i++){
//         a[i].style.cursor = 'default';
//         a[i].setAttribute('href', 'javascript:;');
//     }
//     hover(a[0],function(){
//         a[0].style.textDecoration = 'none';
//     },function(){
//         a[0].style.textDecoration = 'none';
//     })
//     hover(a[1],function(){
//         a[1].style.textDecoration = 'none';
//     },function(){
//         a[1].style.textDecoration = 'none';
//     })
//
// }else{
//     for(var i=0; i<a.length; i++){
//         a[i].style.cursor = 'pointer';
//     }
//     hover(a[0],function(){
//         a[0].style.textDecoration = 'underline';
//     },function(){
//         a[0].style.textDecoration = 'underline';
//     })
//     hover(a[1],function(){
//         a[1].style.textDecoration = 'underline';
//     },function(){
//         a[1].style.textDecoration = 'underline';
//     })
// }
//end(sdd,2019,9,4)



//移动端下拉效果
(function(){
    var cooloseHeader = $('#list-collapse .pc-none');
    var cooloseCon = $('#list-collapse .list-con');
    cooloseHeader.on('click',function(){
        var preCooloseCon = $(this).parent().find('.list-con').eq('0');//当前内容
        if(preCooloseCon.css('display') == 'block'){
            $(this).removeClass('collapsed');
            preCooloseCon.slideUp();
            return false
        }
        $(this).addClass('collapsed');
        preCooloseCon.slideDown();
        cooloseCon.not(preCooloseCon).each(function(index,ele){
            $(ele).slideUp()
        })
    });
})();
//移动端下拉效果 End


/**
 * Subscribe Js
 *
 * **/
$("#footer_newsletter").on("keyup",function(){
    $("#span_show").html("");
});


$(".Email_footer .primary").click(function () {
    $(".assigned").html("");
    var input_value = $("#footer_newsletter").val();
    var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if(input_value==""){
        if($("#span_show").length>=1){
            $("#span_show").html("This is a required field.");
        }else{
            $("#newsletter-validate-details").parent().append("<span style='color:red' id='span_show'>This is a required field.</span>");
        }
        // $(".assigned").css({
        //     marginTop:"0px"
        // });
        return false;
    }else if(!email.test(input_value)){
        if($("#span_show").length>=1){
            $("#span_show").html("Please enter a valid email address (Ex: johndoe@domain.com).")
        }else {
            $("#newsletter-validate-details").parent().append("<span style='color:red' id='span_show'>Please enter a valid email address (Ex: johndoe@domain.com).</span>");
        }
        // $(".assigned").css({
        //     marginTop:"0px"
        // });
        return false;
    }else{
        $.ajax({
            url: "https://www.payneglasses.com/customer/Exchange/NewAction/",
            type: 'post',
            crossDomain: true,
            data: {
                "email": input_value
            },
            success: function (data, textStatus) {
                if (data) {
                    var html = "";
                    if (data.status == 1) {
                        html += '<div class="message1  message-success success">';
                        html += '<div id="pslogin-messages-message-success">' + data.msg + '</div>';
                        html += '</div>';
                    } else {
                        html += '<div class="message1  message-error error">';
                        html += '<div id="pslogin-messages-message-error">' + data.msg + '</div>';
                        html += '</div>';
                    }
                    if (data.msg && html) {
                        // $(".assigned").css({
                        //     marginTop:"67px"
                        // });
                        $(".assigned").html(html);
                        $('#footer_newsletter-error').text('');
                        $('html, body').animate({scrollTop: 0}, 600);
                    }
                }else{
                    // $(".assigned").css({
                    //     marginTop:"0px"
                    // });
                }
            }
        });
    }
});
/**
 * Subscribe Js End
 *
 * **/