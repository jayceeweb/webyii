
window.console = window.console || function(t) {};
//reveiws
$(function(){
    var dataSimple = {};
    if(current_simple){
        dataSimple = {
            sku:current_simple['sku'],
            entry_id:current_simple['entity_id'],
            price:current_simple['price'],
            color:current_simple['color'],
            name:current_simple['name'],
            simple_url:simple_url
        }
    };
    $(".imgs").attr("data-simple",JSON.stringify(dataSimple));

    //获取指定url中的参数值
    //     function getvar(url,par) {
    //         var urlsearch = url.split('?');
    //         pstr = urlsearch[1].split('&');
    //         for (var i = pstr.length - 1; i >= 0; i--) {
    //             var tep = pstr[i].split("=");
    //             if (tep[0] == par) {
    //                 return tep[1];
    //             }
    //         }
    //         return (false);
    //     }
    //end
    // var currentHref = window.location.href;
    // var flagHref = getvar(currentHref,"comment");
    // if(flagHref){
    //     $("#fix-fluid").show();
    // }else{
    //     $("#fix-fluid").hide();
    // }


    //页面滚动到顶部函数
    function scrollFn(num){
        $('html,body').animate({scrollTop:num},'slow',function(){});
    }

    //填写表单判断函数
    function reviewsFrom(one,two,three,form,type){
        if(reviewsData.reviewHeader!="" && reviewsData.nickname!="" && reviewsData.reviewsContent!="" && reviewsData.score!=0 && reviewsData.recommend!=0 && reviewsData.email!=""){
            $("#reveiws-sub").css({
                pointerEvents:"auto",
                borderColor:"#f90",
                background:"#f90"
            });
        }else{
            $("#reveiws-sub").css({
                pointerEvents:"none",
                borderColor:"#ccc",
                background:"#ccc"
            });
        }
        if(one.data==""){
            $(one.ele).css({
                borderColor:"red"
            })
        }else{
            $(one.ele).css({
                borderColor:"#ccc"
            })
        }
        if(two.data==""){
            $(two.ele).css({
                borderColor:"red"
            })
        }else{
            $(two.ele).css({
                borderColor:"#ccc"
            })
        }
        if(three.data==""){
            $(three.ele).css({
                borderColor:"red"
            })
        }else{
            $(three.ele).css({
                borderColor:"#ccc"
            })
        }
        if(form.data==""){
            $(form.ele).css({
                borderColor:"red"
            })
        }else{
            $(form.ele).css({
                borderColor:"#ccc"
            })
        }
        if(type=="keyUp"){
            if(reviewsData.score==0){
                $(".reveiws-container .reveiws-wjx ul li .fa").css({
                    color:"red"
                })
            }else{
                $(".reveiws-container .reveiws-wjx ul li .fa").css({
                    color:"#ffb454"
                })
            }
            if(reviewsData.recommend==0){
                $(".reveiws-container .reveiws-yN .fa").css({
                    color:"red"
                })
            }else{
                $(".reveiws-container .reveiws-yN .fa").css({
                    color:"#ffb454"
                })
            }
        }
    }
    //当前提交评论的时间
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "/";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9){
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9){
            strDate = "0" + strDate;
        }
        var currentdate =   month + seperator1 + strDate + seperator1 + year;
        return currentdate;
    }
    var reviewsData = {};

    $(".reviews_a,.btn-secondary").click(function(){
        scrollFn(0);
        $("#fix-fluid").fadeIn();
        $("#errTips").text("This will only be used for notification purposes");
        $("#errTips").css({
            color:"#bababa"
        });
        if(loginCustomer){
            if(loginCustomer.customer.islogged=="1"){
                $("#Email_S").val(loginCustomer.customer.email);
                reviewsData.email=loginCustomer.customer.email;
            }else{
                $("#Email_S").val("");
                reviewsData.email="";
            }
        }
        var fas = document.querySelectorAll(".fas");
        for(var i=0; i<fas.length; i++){
            fas[i].className="fa fa-star-o fas";
        }

        $(".acs_fieldset_click span:nth-of-type(1)>span").attr("class","fa fa-thumbs-o-up");
        $(".acs_fieldset_click span:nth-of-type(2)>span").attr("class","fa fa-thumbs-o-down")
        reviewsData.score=0;
        reviewsData.recommend=0;
        reviewsData.nickname="";
        reviewsData.reviewsContent="";
        reviewsData.reviewHeader="";
        //再次评论将上次的评论内容清空
        $("#YourReview,#describe,#Nickname").val("");
    });

    reviewsData.score=0;
    reviewsData.recommend=0;
    reviewsData.nickname="";
    reviewsData.reviewsContent="";
    reviewsData.reviewHeader="";
    //评论填写页面点击Show me an example查看更多
        var flagDo = true;
        $("#reveiws-btn").click(function(){
            $(".acs-headline-examples").toggle();
            if(flagDo){
                $("#upDown").attr("class","fa fa-chevron-up");
                flagDo=false;
            }else{
                $("#upDown").attr("class","fa fa-chevron-down");
                flagDo=true;
            }
        });
    //end
    var fas = document.querySelectorAll(".fas");
    //评论填写页面点击五角星进行评分
    $(".reveiws-wjx ul li").click(function(){
        for(var i=0; i<fas.length; i++){
            fas[i].className="fa fa-star-o fas";
        }
        for(var i=0; i<$(this).index()+1; i++){
            fas[i].className="fa fa-star-o fas selFa";
        }
        $(".reveiws-container .reveiws-wjx ul li .fa").css({
            color:"#ffb454"
        });
        //评分赋值
        reviewsData.score = $(this).index()+1;
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            "click"
        );
     });
    //end

    $(".acs_fieldset_click span").click(function(){
        $(".reveiws-container .reveiws-yN .fa").css({
            color:"#ffb454"
        });
        if($(this).attr("val")=='yes'){
            reviewsData.recommend="yes";
            $(this).find("#reveiws-box").attr("class","fa fa-thumbs-up");
            $(".acs_fieldset span:nth-of-type(2)>span").attr("class","fa fa-thumbs-o-down");
        }else{
            reviewsData.recommend="no";
            $(this).find("#reveiws-box").attr("class","fa fa-thumbs-down");
            $(".acs_fieldset span:nth-of-type(1)>span").attr("class","fa fa-thumbs-o-up");
        }
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            "click"
        );
    });
    //填写评论内容时间
    var sum = parseInt($("#DescribeNum").text());
    $("#describe").keyup(function(){
        var newValue = $(this).val();
        var newSum = (sum - $(this).val().length);
        if(newSum<=0){
            newValue = $(this).val().slice(0,sum);
            $("#DescribeNum").text(0);
        }else{
            $("#DescribeNum").text(newSum);
        }
        reviewsData.reviewsContent = newValue;
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            {
                data:$(this).val(),
                ele:this
            },
            "keyUp"
        );

    });
    //end
    $("#YourReview").keyup(function(){
        reviewsData.reviewHeader = $(this).val();
        reviewsFrom(
            {
                data:reviewsData.nickname,
                ele:"#Nickname"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            {
                data:$(this).val(),
                ele:this
            },
            "keyUp"
        );
    });
    $("#Email_S").keyup(function(){
        reviewsData.email = $(this).val();
        $("#errTips").text("This will only be used for notification purposes");
        $("#errTips").css({
            color:"#bababa"
        });
        $("#Email_S").css({
            color:"#444"
        });
            reviewsFrom(
                {
                    data:reviewsData.nickname,
                    ele:"#Nickname"
                },
                {
                    data:reviewsData.reviewsContent,
                    ele:"#describe"
                },
                {
                    data:reviewsData.reviewHeader,
                    ele:"#YourReview"
                },
                {
                    data:$(this).val(),
                    ele:this
                },
                "keyUp"
            );
    });
    $("#Nickname").keyup(function(){
        reviewsData.nickname = $(this).val();
        reviewsFrom(
            {
                data:reviewsData.reviewHeader,
                ele:"#YourReview"
            },
            {
                data:reviewsData.reviewsContent,
                ele:"#describe"
            },
            {
                data:reviewsData.email,
                ele:"#Email_S"
            },
            {
                data:$(this).val(),
                ele:this
            },
            "keyUp"
        );
    });
    var hei = document.querySelector("#Detail").offsetTop;
    var newHei = 0;
    //删除指定参数函数
    function delParam(paramKey) {
        var url = window.location.href;    //页面url
        var urlParam = window.location.search.substr(1);   //页面参数
        var beforeUrl = url.substr(0, url.indexOf("?"));   //页面主地址（参数之前地址）
        var nextUrl = "";

        var arr = new Array();
        if (urlParam != "") {
            var urlParamArr = urlParam.split("&"); //将参数按照&符分成数组
            for (var i = 0; i < urlParamArr.length; i++) {
                var paramArr = urlParamArr[i].split("="); //将参数键，值拆开
                //如果键雨要删除的不一致，则加入到参数中
                if (paramArr[0] != paramKey) {
                    arr.push(urlParamArr[i]);
                }
            }
        }
        if (arr.length > 0) {
            nextUrl = "?" + arr.join("&");
        }
        url = beforeUrl + nextUrl;
        return url;
    }
    var closeHref = delParam("comment");
    //end
    
    $(".share_click").click(function(){
        $(".reveiews-item").eq(1).show().siblings().hide();
        $(".h_g_one").css({
            width:"0%"
        });
        $(".h_g_two").css({
            width:"100%"
        });
        $("#tab-label-additional_reviews").find(".bus-downB").addClass("bus-down-active").parent().siblings(".bus-active").find(".bus-downB").removeClass("bus-down-active");
    });

    //输入错误函数
    function styleIpt(ele){
        $(ele).css({
            borderColor:"red"
        })
    }
    $("#reveiws-sub,#reveiws-cancel,.close-fluid").click(function(){
        //当关闭，或者提交后，将参数url地址回复为默认值
        history.replaceState(null, null, closeHref);
        if($(this).text()=="SUBMIT"){
            //评论标题填写val值
            var YourReview = $("#YourReview").val();
            //评论填写当前名字
            var Nickname = $("#Nickname").val();
            //评论内容获取
            var PreiwsContent = $("#describe").val();
            //邮箱内容
            var Email = $("#Email_S").val();
            var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
            reviewsData.reviewHeader = YourReview;
            reviewsData.nickname = Nickname;
            reviewsData.email = Email;
            if(YourReview==""){
                styleIpt("#YourReview");
            }
            if(Nickname==""){
                styleIpt("#Nickname");
            }
            if(PreiwsContent==""){
                styleIpt("#describe");
            }

            if(Email=="" || !reg.test(Email)){
                styleIpt("#Email_S");
            }
            if(!reg.test(Email)){
                $("#errTips").text("Incorrect mailbox format");
                $("#errTips").css({
                    color:"red"
                });
            }
            if(reviewsData.score==0){
                $(".reveiws-container .reveiws-wjx ul li .fa").css({
                    color:"red"
                })
            }

            if(reviewsData.recommend==0){
                $(".reveiws-container .reveiws-yN .fa").css({
                    color:"red"
                })
            }

            if(YourReview!="" && Nickname!="" && PreiwsContent!="" && reviewsData.score!=0 && reviewsData.recommend!=0 && Email!="" && reg.test(Email)){
                //当前产品的simple;
                reviewsData.simples = JSON.parse($(".imgs").attr("data-simple"));
                //当前提交的时间
                reviewsData.reviewsDate = getNowFormatDate();
                $(".reveiews-item").eq(1).show().siblings().hide();
                $("#tab-label-additional_reviews").find(".bus-downB").addClass("bus-down-active").parent().siblings(".bus-active").find(".bus-downB").removeClass("bus-down-active");
                $(".h_g_one").css({
                    width:"0%"
                });
                $(".h_g_two").css({
                    width:"100%"
                });
                $("#fix-fluid").fadeOut();
                // $(".container_conter").show();
                // $(".reveiws-container").hide();
                newHei=(hei-200);
                if(reviewsData.recommend=="yes"){
                    reviewsData.recommend=1;
                }else{
                    reviewsData.recommend=0;
                }
                var data = {
                    simple_id:reviewsData.simples.entry_id,
                    subject:reviewsData.reviewHeader,
                    post_content:reviewsData.reviewsContent,
                    review_rating:reviewsData.score,
                    nickname:reviewsData.nickname,
                    will_recommend:reviewsData.recommend,
                    email:reviewsData.email
                };
                $.ajax({
                    url:"/reviews/product/save",
                    type: "POST",
                    data:data,
                    success:function(res){
                        $(".ok_reviews").show();
                        setTimeout(function(){
                            $(".ok_reviews").hide();
                        },5000);
                    },error:function(err){

                    }
                });
            }else{
                $(this).css({
                    pointerEvents:"none",
                    borderColor:"#ccc",
                    background:"#ccc"
                });
                newHei=200;
            }
        }else{
            newHei=0;
            $(".reveiews-item").eq(0).show().siblings().hide();
            $("#tab-label-additional").find(".bus-downB").addClass("bus-down-active").parent().siblings(".bus-active").find(".bus-downB").removeClass("bus-down-active");
            $(".h_g_one").css({
                width:"100%"
            });
            $(".h_g_two").css({
                width:"0%"
            });
            $("#fix-fluid").fadeOut();
            // $(".container_conter").show();
            // $(".reveiws-container").hide();
        }
        scrollFn(newHei);
    });

    function currentPage(datas,fn){
        $.ajax({
            url:"/reviews/product/readreviews",
            type:"POST",
            data:datas,
            success:fn
        });
    }






    function pageFn(nums){
        //获取当前是否有评论数据
        if(nums>=1){
            currentPage(
                {
                    reviewspage:nums,
                    id:configurable.entity_id
                },function(res){
                    //获取当前评论条数
                    var num = parseInt(res.cnt);
                    var data = res.data;
                    var htmlPage = "";
                    if(nums>=2){
                        $(".arrow-left___1BoUt,.rsr-pagination-btn").css({
                            color:"#f90"
                        })
                    }
                    if(nums>=res.pagecount){
                        $(".rsr-pagination-btn").css({
                            color:"#ccc"
                        });
                        $(".arrow-left___1BoUt").css({
                            color:"#f90"
                        })
                    }

                    if(nums<=1){
                        $(".arrow-left___1BoUt").css({
                            color:"#ccc"
                        });
                        $(".rsr-pagination-btn").css({
                            color:"#f90"
                        })
                    }
                    //当前有评论
                    if(num>0 && (nums<=res.pagecount && nums>=1)){
                        $("#yes_revies").addClass("reveiews-item");
                        $("#No_revies").removeClass("reveiews-item");
                        $("#count_Sum").text(res.pagecount);
                        $("#pages").text(nums);
                        for(var i=0; i<data.length; i++){
                            var num = parseInt(data[i].review_rating);
                            var spans="";
                            var span="";
                            for(var k=0; k<num; k++){
                                spans += "<span class='fa fa-star fa-star___2gJYz' id='check'></span>";
                            }
                            var numS = 5-num;
                            for(var k=0; k<numS; k++){
                                span += "<span class='fa fa-star fa-star___2gJYz'></span>";
                            }
                            var zxWas = "";
                            var is_login  = loginCustomer.customer.islogged;


                            var str = "";
                            if(data[i].is_like=="0"){
                                str = "<span class='fa fa-thumbs-o-up zc-box'  reviewFlag="+data[i].is_like+"   reveiewId="+data[i].reviews_id+"></span>";
                            }else{
                                str = "<span class='fa fa-thumbs-up zc-box'  reviewFlag="+data[i].is_like+"   reveiewId="+data[i].reviews_id+"></span>";
                            }
                            if(is_login){
                                zxWas = "<span class='zx-p zx-was'> Was this review helpful?</span>"+
                                    "<fieldset class='acs_fieldset acs_fieldsetS'>"+
                                    "<span val='yes'>"+
                                    str+
                                    "<p for='rec0' class='rec0 rec'><span style='margin-left: 5px' class='reveiewNum'>("+data[i].likes_num+")</span></p>"+
                                    "</fieldset>";
                            }else{
                                zxWas="";
                            }
                            htmlPage += "<ul>"+
                                "<li>"+
                                "<span class='zx-sort'>"+data[i].review_rating+"</span>"+
                                "<span class='star-ratings__top___3IeBI zx-rating'>"+
                                spans+
                                span+
                                "<span class='zx-color'>"+
                                "<a>Color:</a>"+
                                "<a>"+data[i].color+"</a>"+
                                "</span>"+
                                "</li>"+
                                "<li>"+
                                "<h5 class='zc-h4'>"+data[i].subject+"</h5>"+
                                "<span class='zc-by'>by"+data[i].nickname+","+data[i].post_date_html+"</span>"+
                                "</li>"+
                                "<li>"+
                                "<p class='zx-p'>"+data[i].post_content+"</p>"+
                                zxWas+
                                "</li>"+
                                "</ul>"
                        };
                        $(".zx-div").html(htmlPage);
                    }else{
                        $("#No_revies").addClass("reveiews-item");
                        $("#yes_revies").removeClass("reveiews-item");
                    }
                    $(".bus-active").on("click",function(){
                        $(this).find(".bus-downB").addClass("bus-down-active").parent().siblings(".bus-active").find(".bus-downB").removeClass("bus-down-active");
                        $(".reveiews-item").eq($(this).index()).show().siblings().hide();
                        $.ajax({
                            url: "/reviews/like/get",
                            data: {
                                reviews_id: simpleids_string
                            },
                            type: "POST",
                            success: function (res) {
                                console.log(res);
                            }
                        });
                    });

                    $("body").on("click",".zc-box",function(){
                        var reveiewid = $(this).attr("reveiewid");
                        //获取当前的点赞元素
                        var currentEle = $(this).parent().find(".reveiewNum");
                       if($(this).attr("class")!="fa fa-thumbs-up zc-box"){
                           var that = this;
                           $.ajax({
                               url:"/reviews/like/add",
                               type:"POST",
                               data:{
                                   reviews_id:reveiewid
                               },
                               success:function (res) {
                                   if(res.status){
                                       $(that).attr("class","fa fa-thumbs-up zc-box");
                                       currentEle.text("("+res.data.likes_num+")");
                                   }
                               }
                           });
                       }
                    })
                }
            );
        }
    }
    pageFn(1);

    //右翻页
    $("#right_bou").on("click",function(){
          var num =  parseInt($("#pages").text());
            num++;
        pageFn(num);
    });


    //左翻页
    $("#left_bou").on("click",function(){
        var num =  parseInt($("#pages").text());
        num--;
        pageFn(num);
    });

    $('.circle').each(function(index, el) {
        var num = $(this).find('span').text() * 3.6;
        if (num<=180) {
            $(this).find('.right_circle').css('transform', "rotate(" + num + "deg)");
        } else {
            $(this).find('.right_circle').css('transform', "rotate(180deg)");
            $(this).find('.left_circle').css('transform', "rotate(" + (num - 180) + "deg)");
        };
    });
    if (document.location.search.match(/type=embed/gi)) {
        window.parent.postMessage("resize", "*");
    }
});
