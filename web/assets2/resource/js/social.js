$(document).ready(function() {
    var configureProductObj = configurable;

    function sharePrice() {
        // 没有分享过 && 有分享价格 && share price's type is number && share price less than final price
        if (_is_shared == 0 && _share_price != undefined &&
                !isNaN(parseFloat(_share_price)) && !isNaN(parseFloat(_final_price)) &&
                parseFloat(_final_price) > parseFloat(_share_price)) {
            //状态改为已分享
            _is_shared = 1;
            $("#price").html(_share_price);
        }
    }

    configure_product_id = configureProductObj["entity_id"];
    var _window_height = $(window).height();
    var _window_width = $(window).width();

    window.fbAsyncInit = function () {
        FB.init({
            // appId            : facebook_app_id,
            appId: "267011757149662",
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v3.2'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    /*** luanjue  数据采集初始数据   start   ***/
    var smartOn = $("#smartCollectOn").val();
    if(smartOn == 1) {
        smart.init('collectionPdp', {
            loaded: function (sdk) {
                // luanjue_3  清除super_properties
                sdk.clear_event_super_properties();
            }
        });
        //  15、pdp页面点击facebook分享  luanjue
        smart.time_event('pdp_access_share');
    }

    function getShareDataLJ() {
        var accessEntryID = $(".FrameColor_select").parent().attr("entryid");
        var accessProductID = $(".FrameColor_select").parent().attr("product");
        var accessColorID = $(".FrameColor_select").parent().attr("color_id");
        var accessName = $(".FrameColor_select").parent().attr("name");
        var accessPrice = $(".FrameColor_select").parent().attr("price");
        var accessSku = $(".FrameColor_select").parent().attr("sku");
        var accessUrl = $(".FrameColor_select").parent().attr("url");
        var accessStock = $(".FrameColor_select").parent().attr("stock");
        var reviewSku = accessSku;

        var dataSave = {
            "entityID": accessEntryID,
            "productID": accessProductID,
            "productColorID": accessColorID,
            "productName": accessName,
            "price": accessPrice,
            "sku": accessSku,
            "simpleUrl": accessUrl,
            "stock": accessStock,
            "accessShareType": 1,
            "reviewSku":reviewSku
        };
        return dataSave;
    }
    /*** luanjue  数据采集初始数据   end   ***/

    $(".fb-share").click(function () {
        // luanjue 数据采集   start
        if(smartOn == 1) {
            var dataSave = getShareDataLJ();
            dataSave["shareType"] = 1;
            smart.track_event("pdp_access_share", dataSave);
        }
        // luanjue 数据采集    end

        if (FB) {
            FB.ui({
                method: 'share',
                mobile_iframe: true,
                href: simple_url
            }, function (response) {
                if (response && !response.error_message && support_share_and_save) {
                    $.ajax({
                        url: '/discount/share/success',
                        data: {'product_id': configure_product_id, 'share': true},
                        success: function (data) {
                            if (data.success) {
                                sharePrice();
                            }
                        }
                    });
                }
            });
        }
    });

    //  14、pdp页面点击twiter分享 luanjue
    $(".tw-share").click(function () {
        // luanjue 数据采集   start
        if(smartOn == 1) {
            var dataSave = getShareDataLJ();
            dataSave["shareType"] = 2;
            smart.track_event("pdp_access_share", dataSave);
        }
        // luanjue 数据采集    end

        // simple_url=$("#b_sku_txt").attr("simple_url");
        // thumbnail_url=$("#b_sku_txt").attr("thumbnail");
        var _tw_window = window.open(
            'http://twitter.com/intent/tweet?text=' + simple_url,
            'newwindow',
            'height=' + _window_height * 0.8 + ',width=' + _window_width * 0.8 + ',top=111,left=111,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no'
        );
        if (support_share_and_save) {
            $.ajax({
                url: '/discount/share/success',
                data: {'product_id': configure_product_id, 'share': true},
                success: function (data) {
                    if (data.success) {
                        sharePrice();
                    }
                }
            });
        }
    })

    //  13、pdp页面点击google分享   luanjue
    $(".pint-share").click(function () {
        // luanjue 数据采集   start
        if(smartOn == 1) {
            var dataSave = getShareDataLJ();
            dataSave["shareType"] = 3;
            smart.track_event("pdp_access_share", dataSave);
        }
        // luanjue 数据采集    end

        // simple_url=$("#b_sku_txt").attr("simple_url");
        // thumbnail_url=$("#b_sku_txt").attr("thumbnail");
        window.open(
            'http://pinterest.com/pin/create/button/?url=' + simple_url + '&description=' + configureProductObj["name"] + '&media=' + thumbnail_url,
            'newwindow',
            'height=' + _window_height * 0.8 + ',width=' + _window_width * 0.8 + ',top=111,left=111,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no'
        );
        if (support_share_and_save) {
            $.ajax({
                url: '/discount/share/success',
                data: {'product_id': configure_product_id, 'share': true},
                success: function (data) {
                    if (data.success) {
                        sharePrice();
                    }
                }
            });
        }
    })
});
