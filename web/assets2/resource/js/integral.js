$(function(){

    var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    $(".tck_payne_ch,#Cancel").on("click",function(){
        $(".tck_payne").addClass("hide");
        $(".We_have").removeClass("hide");
        var val = $("#username").val();
        if(email.test(val)){
            $("#user_email").text($("#username").val());
        }else{
            $("#user_email").text(current_user);
        }
    });
    $(".btn_join").on("click",function(){
        var user = $("#username").val();
       if(invitation=="1"){
            var val = $("#username").val();
           $.ajax({
               //请求方式
               type: "POST",
               //请求的媒体类型
               //请求地址
               url: "/ucenter/payne-rewards/join.html",
               //数据，json字符
               data: {
                   '_csrf': $('meta[name="csrf-token"]').attr("content"),
                   'confirm_email': val,
                   'email': current_user,
                   'invitation':invitation
               },
               success: function (result) {
                   if(result){
                       result = JSON.parse(result);
                       if(result.code>=1){
                           //成功
                           window.location.href = '/ucenter/score/index.html';
                       }else{
                           $(".massges").removeClass("hide");
                           $(".massges").addClass("error");
                           $(".massges").text(result.ret);
                           setTimeout(function(){
                               $(".massges").addClass("hide");
                           },5000);
                           $('html, body').animate({scrollTop: 0}, 600);
                       }
                   }
               },
               beforeSend:function(){
                   $("#loading-loade").removeClass("hide");
                   setTimeout(function(){
                       $("#loading-loade").addClass("hide");
                   },5000)
               },
               //请求失败，包含具体的错误信息
               error : function(e){
                   $("#loading-loade").removeClass("hide");
               }
           })
       }else{
           $(".tck_payne").removeClass("hide");
       }
        if(user){
            $("#username").val(user);
        }

    });

    $(".We_have_ch,.We_have_close").on("click",function(){
        $(".We_have").addClass("hide");
    });

    $("#Confirm").on("click",function(e,tag){
        var val = "";
        if(tag){
            val = $(".user_ipt").text();
        }else{
            val = $("#username").val();
        }



        if(email.test(val)){
            // $("#user_email").text($("#username").val());
            $.ajax({
                //请求方式
                type : "POST",
                //请求的媒体类型
                //请求地址
                url : "/ucenter/payne-rewards/join.html",
                //数据，json字符
                data : {
                    '_csrf':$('meta[name="csrf-token"]').attr("content"),
                    'confirm_email':val,
                    'email':current_user
                },
                //请求成功
                success : function(result) {
                    $("#loading-loade").addClass("hide");
                    if(result){
                        result = JSON.parse(result);
                        if(result.code>=1){
                            //成功
                            $(".btn_join").hide();
                            $(".tck_payne").addClass("hide");
                            $(".We_have").removeClass("hide");
                            $(".massges").removeClass("hide");
                            $(".massges").addClass("success");
                            $(".massges").text(result.ret);
                            $('html, body').animate({scrollTop: 0}, 600);
                            setTimeout(function(){
                                $(".massges").addClass("hide");
                                window.location.reload();
                            },4000);
                        }else{
                            $(".massges").removeClass("hide");
                            $(".massges").addClass("error");
                            $(".massges").text(result.ret);
                            setTimeout(function(){
                                $(".massges").addClass("hide");
                            },5000);
                            $('html, body').animate({scrollTop: 0}, 600);
                        }
                    }
                },
                beforeSend:function(){
                    $("#loading-loade").removeClass("hide");
                    setTimeout(function(){
                        $("#loading-loade").addClass("hide");
                    },5000)
                },
                //请求失败，包含具体的错误信息
                error : function(e){
                    $("#loading-loade").removeClass("hide");
                }
            });
        }else{
            var err = "Please enter a valid email address.";
            if(tag){
                $(".user_ipt").css({
                    borderColor:"red"
                })
            }else{
                $("#err_cw").text(err);

            }
            $("#user_email").text(current_user);
        }
    });


    $(".user_ipt").on("keyup",function(){
        $(".user_ipt").css({
            borderColor:"#ccc"
        })
    });

    $("#username").on("keyup",function(){
        $("#err_cw").text("");
    });







    //点击推送邮件
    $("#jx_ts").on("click",function(){
        $('#Confirm').trigger('click',"jx_ts");
    });

    window.onload=function(){
        document.getElementById('username').focus();
    }


});