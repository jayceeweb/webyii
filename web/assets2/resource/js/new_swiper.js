var obj_img =  current_simple.media_gallery.images;
var current_img=[];
for(var key in obj_img){
    current_img.push('/media/catalog/product'+obj_img[key]['file']);
}
var mySwiper = new Swiper('.swiper-container-one', {
    pagination: {
        el: '.swiper-pagination',
        clickable:true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
var span_List = document.querySelectorAll(".swiper-pagination-bullets span");
for(var i=0; i<span_List.length; i++){
    var img = document.createElement("img");
    img.src=current_img[i];
    span_List[i].appendChild(img);
}
var simple_slide = document.querySelectorAll(".simple_slide");


// 推荐 Start
function recommend_fn(entity_id){
    var product_id_obj = {product_id:entity_id};
    $.ajax({
        url:"/webyiiapi/related/related.html",
        type:"POST",
        data:JSON.stringify(product_id_obj),
        success:function(res){
            // res = JSON.parse(res);
            if(res.code!=0){
                $(".recommend").hide();
            }else{
                $(".recommend").show();
                var data_obj = res.data;
                var attributeCount = function(obj) { //获取对象属性的个数
                    var count = 0;
                    for(var i in obj) {
                        if(obj.hasOwnProperty(i)) {
                            count++;
                        }
                    }
                    return count;
                };
                var slidesPerView = 3;
                var slidesPerView_2 = 2;
                var data_obj_length = attributeCount(data_obj);
                if(data_obj_length>=3){
                    if(data_obj_length==3){
                        if($(window).width()<=1200){
                            $(".swiper-button-hide").show();
                        }else{
                            $(".swiper-button-hide").hide();
                        }
                    }else{
                        $(".swiper-button-hide").show();
                    }
                    $(".recommend").attr("id","recommend_3");
                    slidesPerView = 3;
                    slidesPerView_2 = 2;
                }else if(data_obj_length==2){
                    if($(window).width()<=600){
                        $(".swiper-button-hide").show();
                    }else{
                        $(".swiper-button-hide").hide();
                    }
                    $(".recommend").attr("id","recommend_2");
                    slidesPerView = 2;
                    slidesPerView_2 = 2;
                }else{
                    $(".swiper-button-hide").hide();
                    $(".recommend").attr("id","recommend_1");
                    slidesPerView = 1;
                    slidesPerView_2 = 1;
                }
                var html_recommend = "";


                //指定默认排序到最前函数
                function setArrFirst(arr, property, value) {
                    var chooseIndex;    //选中元素在数组中的索引
                    var areBothObj = arr.every(function(item, index, array) {    //判断数组中的元素是否都为对象，都是对象则返回true
                        return item instanceof Object;
                    });
                    if (areBothObj) {
                        arr.forEach(function(item, index, array) {    //遍历数组，找到符合条件的元素索引
                            if (item[property] == value) {
                                chooseIndex =  index;
                            }
                        });
                    } else {
                        return false;
                    }
                    var choosed = arr.splice(chooseIndex, 1);    //将选中元素从原数组中剔除
                    var newArr = choosed.concat(arr);    //合并选中元素和剔除了选中元素的原数组
                    return newArr;
                }
                for(var i in data_obj) {
                    var span_html = "";
                    var new_data_obj = data_obj[i];
                    new_data_obj = setArrFirst(new_data_obj, 'default', true);
                    new_data_obj.forEach(function(val,idx){
                        var color_value = val["color_value"];
                        var color_img = "/media/catalog/product/"+val["image"];
                        var color_url = "/"+val["url"];
                        var price = val["price"];
                        var special_price =val["special_price"];
                        if(color_value.length>7){ //使用图片
                            span_html+="<span class='color_value_click' color_img="+color_img+" color_url="+color_url+" price="+price+" special_price="+special_price+" sku="+val['sku']+" parent_id="+val['parent_id']+"><span style=background-image:url(/media/attribute/swatch/swatch_image/30x20"+color_value+")></span></span>";
                        }else{ //使用背景色
                            span_html+="<span class='color_value_click' color_img="+color_img+" color_url="+color_url+" price="+price+" special_price="+special_price+" sku="+val['sku']+" parent_id="+val['parent_id']+"><span style=background:"+color_value+"></span></span>";
                        }
                    });
                    var url = "/"+new_data_obj[0].url;
                    var img = "/media/catalog/product/"+new_data_obj[0].image;
                    var price = new_data_obj[0].price;
                    var special_price = new_data_obj[0].special_price;
                    var name = new_data_obj[0].name;
                    // 采集数据可能会用到的数据 luanjue   start
                    var ljEntityId = new_data_obj[0].entity_id;
                    var ljColor = new_data_obj[0].color;
                    var ljSku = new_data_obj[0].sku;
                    var ljProductId = new_data_obj[0].product_id;
                    var ljParentId = new_data_obj[0].parent_id;
                    // console.log("new_swiper.js:  "+JSON.stringify(new_data_obj[0]) );
                    // 采集数据可能会用到的数据 luanjue   end
                    price = parseFloat(price);
                    price = price.toFixed(2);
                    special_price = parseFloat(special_price);
                    special_price = special_price.toFixed(2);

                    var price_html = "";
                    if(price!=special_price && special_price){ //有打折价
                        price_html = "<del>$"+price+"</del><span>$"+special_price+"</span>";
                    }else{ // 没有打折价
                        price_html = "<span>$"+price+"</span>";
                    }
                    html_recommend += "<div class='swiper-slide recommend_swiperSlide'>"+
                        // "<a href="+url+">"+
                        "<a href="+url+" sku='"+ljSku+"' color='"+ljColor+"' entity_id='"+ljEntityId+"' product_id='"+ljProductId+"' parent_id='"+ljParentId+"' price='"+price+"' name='"+name+"' special_price='"+special_price+"'>"+
                        "<img src="+img+">"+
                        "</a><div class='xyuandian'>"+span_html+
                        "</div>"+
                        "<p>"+name+"</p>"+
                        "<li>"+price_html+"</li></div>";
                }
                $("#append_recommend").html(html_recommend);
                new Swiper('.recommend', {
                    slidesPerView : slidesPerView,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    breakpoints:{
                        1200: { //640屏幕下设定
                            slidesPerView: slidesPerView_2, //一行2个
                            spaceBetween: 10 //这是swiper的每个元素间隔的设置
                        },
                        600:{ //320屏幕下设定
                            slidesPerView: 1, //一行2个
                            spaceBetween: 0 //这是swiper的每个元素间隔的设置
                        }
                    }
                });
            }
        },
        error: function (e) {
            $(".recommend").hide();
            console.log(e);
        }
    });
}

try {
    var entity_id = $(".FrameColor").attr("entryId");
    recommend_fn(entity_id);
    $(document).on("click",".color_value_click",function(){ //推荐颜色切换
        var color_img = $(this).attr("color_img");
        var color_url = $(this).attr("color_url");
        var price = $(this).attr("price");
        var special_price = $(this).attr("special_price");
        var ljSkuSwiper = $(this).attr("sku");
        var ljParentIDSwiper = $(this).attr("parent_id");
        $(this).parents(".recommend_swiperSlide").find("img").attr("src",color_img);
        $(this).parents(".recommend_swiperSlide").find("img").attr("sku",ljSkuSwiper);
        $(this).parents(".recommend_swiperSlide").find("img").attr("parent_id",ljParentIDSwiper);
        $(this).parents(".recommend_swiperSlide").find("a").attr("href",color_url);
        price = parseFloat(price);
        price = price.toFixed(2);
        special_price = parseFloat(special_price);
        special_price = special_price.toFixed(2);
        var price_html = "";
        if(price!=special_price && special_price){ //有打折价
            price_html = "<del>$"+price+"</del><span>$"+special_price+"</span>";
        }else{ // 没有打折价
            price_html = "<span>$"+price+"</span>";
        }
        $(this).parents(".recommend_swiperSlide").find("li").html(price_html);
        $(this).css({
            borderColor:"#000"
        }).siblings(".color_value_click").css({
            borderColor:"transparent"
        })
    });
} catch(e){
    console.log(e);
}
// 推荐 End

//点击切换产品颜色
/***
 *  luanjue   配件页面颜色切换数据采集   start
 * ***/
var smartOn = $("#smartCollectOn").val();
if(smartOn == 1) {
    smart.init('collectionPdp', {
        loaded: function (sdk) {
            // luanjue_3  清除super_properties
            sdk.clear_event_super_properties();
        }
    });
    smart.clear_event_super_properties();
    smart.time_event('pdp_access_color');
}

$(".fram_list").on("click",function(){
    /***
     *  luanjue   配件页面颜色切换数据采集   start
     * ***/
    if(smartOn == 1) {
        var accessProductID = $(this).attr("product");
        var accessSku = $(this).attr("sku");
        var ljSku = accessSku;
        var dataSave = {
            "configurableID": accessProductID,
            "destSku": accessSku,
            "sku": ljSku
        };
        smart.track_event("pdp_access_color", dataSave);
    }
    /***
     *  luanjue   配件页面颜色切换数据采集   end
     * ***/

    // for google Recommend  detail-page-view   配件页 start    颜色切换也算是浏览详情
    var googleUserInfo = {
                'visitorId': getLocalDeviceID
            };
    var googleUserID = window.loginCustomer.customer.current_profile_id;
    if(googleUserID) {
        googleUserInfo.userId = googleUserID;
    }
    var accessSku = $(this).attr("sku");
    // dataLayer = dataLayer || [];
    dataLayer.push({
        'event':'detail-page-view',
        'automl': {
            'eventType': 'detail-page-view',
            'userInfo': googleUserInfo,
            'productEventDetail': {
                'productDetails': [{
                    'id': accessSku,
                }]
            }
        }
    });
    // for google Recommend  detail-page-view   配件页  end


    var curent_id = $(this).attr("entryId");
    var special_price = $(this).attr("special_price");
    var price = $(this).attr("price");
    var str_price = "";

    // 推荐 Start
    try {
        recommend_fn(curent_id);
    } catch(e){
        console.log(e);
    }
    // 推荐 End

    if(special_price && special_price!="0.00"){
        if(special_price!=price){
            str_price = "<s>$"+price+"</s><span>$"+special_price+"</span>"
        }else{
            str_price = "<span>$"+price+"</span>"
        }
    }else{
        str_price = "<span>$"+price+"</span>"
    }
    $(".stock_price").html(str_price);



    function currentSimple(curent_id) {
        var  _name = 'var currents=' + "simple_" + curent_id;
        eval(_name);
        return currents;
    }
    var url = $(this).attr("url");
    if(url){
        history.replaceState(null, null, url);
    }
    //reviews  使用绑定当前simple
    var reviews_obj = {
        sku:$(this).attr("sku"),
        entry_id:$(this).attr("entryId"),
        price:$(this).attr("price"),
        color:$(this).attr("color_id"),
        name:$(this).attr("name"),
        simple_url:simple_url
    };
    $(".imgs").attr("data-simple",JSON.stringify(reviews_obj));
    if($(this).attr("price")){
        $(".imgs").attr("src",$(this).attr("thumbnail"));
        $(".zx-sku").text($(this).attr("sku"));
        $(".zx-name").text($(this).attr("name"));
    }
    var img_list = currentSimple(curent_id)['media_gallery']['images'];
    var current_stock = $(this).attr("stock");
    var current_img=[];
    for(var key in img_list){
        current_img.push('/media/catalog/product'+img_list[key]['file']);
    }
    $(this).find("div").addClass("FrameColor_select").parents().siblings().find("div").removeClass("FrameColor_select");
    $(this).find("p").addClass("selector_color").parents().siblings().find("p").removeClass("selector_color");
    for(var l=0; l<simple_slide.length; l++){
        $(".cur_sku").text(currentSimple(curent_id)['sku']);
        span_List[l].children[0].setAttribute("src",current_img[l]);
        simple_slide[l].children[0].setAttribute("src",current_img[l]);
    }
    if(current_stock){
        $(".stock").text(current_stock);
    }
    if(current_stock == "IN STOCK"){
        $("#orderRx").css({
            background:"#ffb454",
            cursor:"pointer"
        });
        $("#orderRx").attr("class","add_to_cart").text("Add To Cart");
    }else{
        $("#orderRx").css({
            background:"#dddddd",
            cursor:"not-allowed"
        });
        $("#orderRx").attr("class","out_of_stock").text("OUT OF STOCK");
    }
});
// 吸顶
$(window).load(function () {
    if (!((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i).test(navigator.userAgent))) {
    /*点击返回顶部*/
        $(window).scroll(function () {
            var botopH = $(window).scrollTop();//滚动的高度
            if (botopH > 50) {
                $('#botop').stop().fadeIn();
            } else {
                setTimeout(function () {
                    $('#botop').stop().fadeOut();
                }, 2000)
            }
        });
        $('#botop').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 600);
        })
    } else {
        var order_btn = $('.save_addTo');

        var t = order_btn.offset().top;
        var fh = order_btn.height();
        var wh = window.innerHeight;
        $(window).scroll(function () {

            var s = $(document).scrollTop();
            if (s > t + fh) {
                $("#tip_save").addClass("new_tip_save");
                order_btn.addClass("save_addTo_fiex");
            } else if (s < t - wh + fh + 10) {
                order_btn.addClass("save_addTo_fiex");
                $("#tip_save").addClass("new_tip_save");
            } else {
                order_btn.removeClass("save_addTo_fiex");
                $("#tip_save").removeClass("new_tip_save");
            }
        })
        /*点击返回顶部*/
        $(window).scroll(function () {
            var totopH = $(window).scrollTop();//滚动的高度
            if (totopH > 300) {
                $('#totop').stop().fadeIn();
            } else {
                setTimeout(function () {
                    $('#totop').stop().fadeOut();
                }, 2000)
            }
        });
        $('#totop').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 600);
        })
    }


    if(configurable){
        //in stick  获取当前状态
        $.ajax({
            //modify lee 2020.6.9
            // url:"/catalogextend/product/cproductInfo",
            url:"/e/product/ProductInfo/cproductInfo",
            //end
            type:"POST",
            data:{
                cpid:configurable['entity_id']
            },
            success:function(res){
                var simple_id = "";
                if(current_simple){
                    simple_id = current_simple.entity_id;
                }
                var arr_simple = res.simples;
                for(var i=0; i<arr_simple.length; i++){
                    if(simple_id == arr_simple[i]['id']){
                        $(".stock").text(arr_simple[i]['stock_txt']);
                        if(arr_simple[i]['stock_txt'] == "IN STOCK"){
                            $("#orderRx").css({
                                background:"#ffb454",
                                cursor:"pointer"
                            });
                            $("#orderRx").attr("class","add_to_cart").text("Add To Cart");
                        }else{
                            $("#orderRx").css({
                                background:"#dddddd",
                                cursor:"not-allowed"
                            });
                            $("#orderRx").attr("class","out_of_stock").text("OUT OF STOCK");
                        }
                    }
                    $(".fram_list").eq(i).attr("stock",arr_simple[i]['stock_txt']);
                }
            }
        })
    }

});
