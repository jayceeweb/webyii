$(function(){
    // add toCart
    var fram_list = document.querySelectorAll(".fram_list");
    var entryId = "";
    var product="";
    var color_id="";
    //请求失败函数
    function errFn(msg){
        var html="";
        html += '<div class="message1  message-error error">';
        html += '<div id="pslogin-messages-message-success">' + msg + '</div>';
        html += '</div>';
        $(".assigned").show();
        $(".assigned").html(html);
        $(".assigned").removeClass("hide");
        $('html, body').animate({scrollTop: 0}, 600);
    }

    //判断是否为json字符串
    function exhibits(str){
        if (typeof   str == 'string') {
            try {
                var obj=JSON.parse(str);
                return true;
            } catch(e) {
                return false;
            }
        }
    }

    $("#orderRx").on("click",function(){
        var protective = $(this).attr("protective");
        for(var i=0; i<fram_list.length; i++){
            if(fram_list[i].children[0].className){
                entryId = fram_list[i].getAttribute("entryid");
                product = fram_list[i].getAttribute("product");
                color_id = fram_list[i].getAttribute("color_id");
            }
        }

        //护目镜参数整合
        var goggles_data={
            "profile" : {
                "id":"",
                "nickname":""
            },
            "is_nonPrescription":false,
            "orderglass":{},
            "support_php":""
        };
        //pdp页面为护目镜的时候
        if(protective=="true"){
            if (window.localStorage && window.localStorage.length) {
                var defaultLoca = {
                    global_closed_message: "[]"
                };
                defaultLoca["guest-profile"] = {nickname: "yourself", gender: "2", age_group: ""};
                defaultLoca["guest-profile"] = defaultLoca["guest-profile"];
                var myLocal = window.localStorage['mage-cache-storage-pg'] && window.localStorage['mage-cache-storage-pg'] != 'undefined' && window.localStorage['mage-cache-storage-pg'].length ? $.parseJSON(window.localStorage['mage-cache-storage-pg']) : {};
                if (myLocal['guest-profile'] == undefined) {
                    myLocal = defaultLoca;
                }
            }


            //获取profile的名字项
            var localPorfile = "";
            if(myLocal){
                localPorfile = myLocal['guest-profile'];
                if(exhibits(localPorfile)){
                    localPorfile = JSON.parse(localPorfile);
                }else{
                    localPorfile = {nickname:"YOURSELF"};
                }
            }
            var loginCustomer = $("#webYi_customerInfo").attr("logincustomer");
            if(exhibits(loginCustomer)){
                loginCustomer = JSON.parse(loginCustomer);
            }else{
                loginCustomer = {
                    customer:{
                        current_profile_id:"",
                        current_profile_name:"",
                        islogged:0
                    }

                };
            }
            var profile_name = localPorfile['nickname'];
            var is_login = loginCustomer['customer']['islogged'];
            var current_profile_id = loginCustomer['customer']['current_profile_id'];
            var current_profile_name = loginCustomer['customer']['current_profile_name'];
            if(is_login){
                //已登录
                goggles_data['profile']['id'] = current_profile_id;
                goggles_data['profile']['nickname'] = current_profile_name;
            }else{
                //未登录
                goggles_data['profile']['nickname'] = profile_name;
            }
        }
        if(protective!="true"){
            goggles_data = {};
        }

        u="/checkout/cart/add/product/"+product+"/?product="+entryId+"&qty=1&super_attribute[93]="+color_id;
        if($(this).attr("class")=="add_to_cart"){
            $.ajax({
                type: "POST",
                url:u,
                data:goggles_data,
                success: function(data){
                    window.location.href="/checkout/cart/";
                    $("#loading-loade").addClass("hide");
                },
                beforeSend:function(res){
                    $("#loading-loade").removeClass("hide");
                },
                error:function(err){
                    $("#loading-loade").addClass("hide");
                    errFn("readyState : "+err.readyState+";status : "+err.status+"Please refresh and try again !!!");
                }
            });

        }

    })
});