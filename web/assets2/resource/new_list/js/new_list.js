


/* 轮播效果以及pjax实现 start */
/*  进入页面就执行的swiper轮播实现 start  */
function init_swiper() {
    var swiperContainerLength = $(".swiper-container").length;
    for(var p=1;p<swiperContainerLength+1;p++) {
        var lj_swiper_num = '.lj_swiper'+p;
        var ljSelectLi_num = '.ljSelectLi'+p;
        var swiper = new Swiper(lj_swiper_num, {
            pagination: {
                el: ljSelectLi_num,
                clickable: true,
                renderBullet: function (index, className) {

                    var temp = $(lj_swiper_num).find(".ljSelectColorList").eq(index).html();
                    return '<span class="' + className + '">' + temp + '</span>';
                },
            },
        });
    }

    /* 页面title变化处理 start */
    var ljNowPage = $("#ljNowPage").attr("data");
    var ljNowKeywords = $("#ljNowKeywords").attr("data");
    $("title").html("Search Results for "+ljNowKeywords+" (Page "+ljNowPage+") | Payne Glasses");
    /* 页面title变化处理 end */
}
init_swiper();
/*  进入页面就执行的swiper轮播实现 end  */

/*  pjax分页完成 需要重新执行swiper start  */
$(document).pjax('.m-pagination li a', '#ljListPageContentPjax');
/**  页面加载中提示  start  **/
$(document).on('pjax:send', function() {
    $(".shadow").css("display","block");
    // console.log("pajax:send");
})
/**  页面加载中提示  end  **/
$(document).on('pjax:complete', function() {
    $(".shadow").css("display","none");
    init_swiper();
    start();
})
/*  pjax分页完成 需要重新执行swiper end  */

/* 判断浏览器前进后退进来的吗？ 都要执行swiper  start */
if (window.history && window.history.pushState) {
    //监听浏览器前进后退事件
    $(window).on('popstate', function () {
        init_swiper();
        start();
    });
}
/* 判断浏览器前进后退进来的吗？ 都要执行swiper  end */
/* 轮播效果以及pjax实现 end */



/****  匿名收藏以及登录收藏处理 start   *****/
/* 是否已登錄 調取ajax查看一下   */
var is_login = 0;
save_Ajax("/e/customer/Exchange/CustomerInfo","GET",{},function(res){
    // console.log(res);
    if(res.customer.islogged){
        is_login = res.customer.islogged;
        /*  已登录用户  SavedFrames跳转地址 */
        $("#SavedFrames").attr("href","/wishlist/index/index/");
        $("#SavedFrames_mobel").attr("href","/wishlist/index/index/");
    }else{
        /*   匿名收藏  SavedFrames跳转地址 */
        $("#SavedFrames").attr("href","/anonymouswishlist");
        $("#SavedFrames_mobel").attr("href","/anonymouswishlist");
    }
});


/*  .ljGlassesSave 遍历得到simple_id 和storage中的数据对比 相等就是收藏的  start */
var ljGlassesSaveObj = $(".ljGlassesSave");
for(var ljk in ljGlassesSaveObj) {
    var current_Id = $(".ljGlassesSave").eq(ljk).attr('data');
    nm_save_fn(current_Id,$(".ljGlassesSave").eq(ljk));
}
/*  .ljGlassesSave 遍历得到simple_id 和storage中的数据对比 相等就是收藏的  end */

/*未登录时 加载favorite  start  */
function nm_save_fn(current_Id,nmObj){
    var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
    // console.log(favorite_product_unknownCustemersJsonString);
    /* 匿名收藏 start   */
    var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
    $.each(favorite_product_unknownCustemers,function(n,v){
        if(current_Id === v.id){
            // 添加收藏
            nmObj.find(".plp-heart-empty").addClass("saveAdded");
            return false;
        }else{
            // 取消收藏
            nmObj.find(".plp-heart-empty").removeClass("saveAdded");
        }
    });
    /* 匿名收藏 end   */

    // yourself中saved Frames链接要到什么地方处理
    var save_length = favorite_product_unknownCustemers.length;
    // var is_login  = loginCustomer.customer.islogged;
    if (is_login==0) {
        if(save_length>=1){
            $(".SavedFrames").text("Saved Frames("+save_length+")");
        }else{
            $(".SavedFrames").text("Saved Frames");
        }
        $(".SavedFrames").attr("href","/anonymouswishlist");
    }
}
/*未登录时 加载favorite  end  */


/*匿名收藏 Start
 *
 *  在未登陆时，将收藏的产品保存到本地缓存中
 *
 *
 * cookie name is pg-unlogin-wishlist
 * */
// setUnknownCustomerFavorite:function(favoriteId,_this){
function setUnknownCustomerFavorite(favoriteInfo,favoriteId,savedObj){
    var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
    var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
    var index_existFavoriteId = -1;
    for(var i =0;i<favorite_product_unknownCustemers.length;i++){
        if(favorite_product_unknownCustemers[i].id===favoriteId){
            index_existFavoriteId = i;
        }
    }

    if( index_existFavoriteId>=0){
        //取消收藏
        favorite_product_unknownCustemers.splice(index_existFavoriteId,1);
        savedObj.find(".plp-heart-empty").removeClass("saveAdded");
    }
    else{
        favorite_product_unknownCustemers.push(favoriteInfo);
        savedObj.find(".plp-heart-empty").addClass("saveAdded");
    }

    var productids =[];
    $.each(favorite_product_unknownCustemers,function(n,v){
        productids.push(v.id);
    });
    var productidsString =JSON.stringify(productids);
    $.cookie('pg-unlogin-wishlist',productidsString,{expires:3650,path:'/'});
    favorite_product_unknownCustemersJsonString = JSON.stringify(favorite_product_unknownCustemers);
    window.localStorage.setItem('favorite_product_unknownCustemers',favorite_product_unknownCustemersJsonString);
    var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
    var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
    var save_length = favorite_product_unknownCustemers.length;
    // var is_login  = loginCustomer.customer.islogged;
    if (is_login==0) {
        if(save_length>=1){
            $(".SavedFrames").text("Saved Frames("+save_length+")");
        }else{
            $(".SavedFrames").text("Saved Frames");
        }
    }

};
/* 匿名收藏 End   */


function save_Ajax(url,type,data,fn){
    $.ajax({
        url:url,
        type:type,
        data:data,
        success:fn
    })
}

/* 登录状态先查找wishlist数据 加载favorite  start  */
save_Ajax("/catalogextend/wishlist/loadwishlist","POST",{},function(res){
    // console.log(res);   // {...},{...},{...}
    // console.log(Object.keys(res[0]));   // ['"510100C01B"']

    // 判断是否有数据
    if(res.length > 0) {
        /*  得到本页面所有save按钮对象 遍历比对sku的值来判断是否是收藏数据 skudata  */
        var ljGlassesSaveObjLoadwishlist = $(".ljGlassesSave");
        for(var ljk3 in ljGlassesSaveObjLoadwishlist) {
            var ljSku = $(".ljGlassesSave").eq(ljk3).attr('skudata');
            for(var wishListKey in res) {
                /*  存在>0 收藏显示红色，将arr_simple数据写入 不存在不做处理。 */
                if(jQuery.inArray(ljSku,Object.keys(res[wishListKey])) > -1) {
                    $(".ljGlassesSave").eq(ljk3).find(".plp-heart-empty").addClass("saveAdded");
                    $(".ljGlassesSave").eq(ljk3).attr("arr_simple",JSON.stringify(res[wishListKey]));
                }
            }
        }
    }
    /* 没有数据 显示匿名收藏的产品   */
    else{
        var ljGlassesSaveObj = $(".ljGlassesSave");
        for(var ljk2 in ljGlassesSaveObj) {
            var current_Id = $(".ljGlassesSave").eq(ljk2).attr('data');
            nm_save_fn(current_Id,$(".ljGlassesSave").eq(ljk2));
        }
    }
});
/* 登录状态先查找wishlist数据 加载favorite   end   */


$("body").on("click",".ljGlassesSave",function() {
    // var is_login = loginCustomer.customer.islogged;
    //已登录状态
    var $ljThis = $(this);
    if(is_login!=0){
        var currentid = $(this).attr("data");
        var ljSku2 = $(this).attr("skudata");
        var arr_simple  = $(this).attr("arr_simple");

        /* 如果有值说明要删除该收藏产品  */
        if(arr_simple !== ""){
            if(typeof(arr_simple)=="string"){
                arr_simple  = JSON.parse(arr_simple);
            }
            // console.log(arr_simple);

            var current_item = arr_simple;
            var obj = {
                item:current_item['wishlist_item_id'],
                uenc:current_item['sharing_code']
            };
            save_Ajax("/wishlist/index/remove/?ajax=true","POST",obj,function(res){
                // console.log("ajax返回值："+res.success);
                if(res.success){
                    var save_count = res.profile.count;
                    if(save_count==0){
                        $(".SavedFrames").text("Saved Frames");
                    }else{
                        $(".SavedFrames").text("Saved Frames("+save_count+")");
                    }

                    arr_simple = null;
                    $ljThis.attr("arr_simple","");
                    $ljThis.find(".plp-heart-empty").removeClass("saveAdded");
                }
            });

        }
        else{
            save_Ajax("/wishlist/index/add/?ajax=true","POST",{product:currentid},function(res){
                // console.log(JSON.parse(res));
                if(res){
                    res = JSON.parse(res);
                    var save_count = res.data.count;
                    if(save_count==0){
                        $(".SavedFrames").text("Saved Frames");
                    }else{
                        $(".SavedFrames").text("Saved Frames("+save_count+")");
                    }
                }

                var obj = {
                    product_id:currentid,
                    wishlist_item_id:res.data.item,
                    sharing_code:res.data.uenc
                };

                $ljThis.attr("arr_simple",JSON.stringify(obj));
            });

            $ljThis.find(".plp-heart-empty").addClass("saveAdded");
        }


    }
    //未登录状态
    else{
        //匿名收藏 Start
        var productInfoObject  = {};
        productInfoObject  = {
            name:$(this).parent().find(".ljOnlyForSavedData").attr("name"),
            img:$(this).parent().find(".ljOnlyForSavedData").attr("imgUrl"),
            price:$(this).parent().find(".ljOnlyForSavedData").attr("price"),
            link:$(this).parent().find(".ljOnlyForSavedData").attr("linkUrl"),
            id:$(this).attr("data")
        };

        setUnknownCustomerFavorite(productInfoObject,productInfoObject.id,$ljThis);
        //匿名收藏 End
    }
});
/****  匿名收藏以及登录收藏处理 end   *****/


/*****  PC端红心hover事件  *****/
$("body").on("mouseenter",".ljGlassesSave",function () {
    $(this).find('.ljSave').css("color","#FFF");
});
$("body").on("mouseleave",".ljGlassesSave",function () {
    $(this).find('.ljSave').css("color","#f90");
});
/*****  PC端红心hover事件  *****/



/*********  图片实现懒加载  start *********/
// 利用闭包实现节流
function throttle(func, time) {
    let timer;
    return function () {
        if (timer) {
            return false;
        }
        timer = setTimeout(function () {
            func();
            clearTimeout(timer);
            timer = '';
        }, time)
    }
}

function start() {
    // 可视区域高度
    var ljh = window.innerHeight;
    // 滚动区域高度
    var ljs = document.documentElement.scrollTop || document.body.scrollTop;

    var ljListNewObj = $(".ljListNew");
    for(var ljItem=0;ljItem<ljListNewObj.length;ljItem++) {
        var listNewOffsetTop = ljListNewObj.eq(ljItem).offset().top;
        // 对象距离顶部的距离大于可视区域和滚动区域之和时懒加载
        var ljimgObj = ljListNewObj.eq(ljItem).find(".ljLazyImage");
        if ((ljh+ljs)>listNewOffsetTop) {

            for(var ljItemi=0;ljItemi<ljimgObj.length;ljItemi++) {
                var src = ljimgObj.eq(ljItemi).attr('data-src');
                ljimgObj.eq(ljItemi).attr("src",src);
                ljimgObj.eq(ljItemi).attr('isLoading', 'true');
            }
        }
    }
}

start();
window.onscroll = throttle(start, 200);
/*********  图片实现懒加载  end *********/


/*****  过滤条件吸顶效果  start *****/
        // 元素距离顶部高度
var ljFilterNavTop = $("#ljFilterNav").offset().top;
// console.log(ljFilterNavTop+'元素距离顶部高度');

var ljScrollTop = document.documentElement.scrollTop || document.body.scrollTop;
// console.log(ljScrollTop+'已滚动区域高度');
/***  滚动区域高度 如果滚动区域高度不是0 导航条应该直接吸顶  ***/
if(ljScrollTop>198) {
    $("#ljFilterNav").addClass("ljFilterNavFixed");
}

$(window).scroll(function() {
    // 滚动区域高度
    var ljScrollTop2 = document.documentElement.scrollTop || document.body.scrollTop;
    // console.log(ljScrollTop2+"滚动区域高");

    /**** 如果距离顶部的高度<滚动区域高度 固定导航栏  ****/
    if(ljScrollTop2>=ljFilterNavTop) {
        $("#ljFilterNav").addClass("ljFilterNavFixed");
    }else{
        $("#ljFilterNav").removeClass("ljFilterNavFixed");
    }

    /*** sortBy 滚动让ljSortByContent消失 start ***/
    if($(".ljSortByContent").hasClass("ljSortActive")) {
        $(".ljSortByContent").removeClass("ljSortActive");
    }
    /*** sortBy 滚动让ljSortByContent消失 end ***/

    /*** sortBy Mobile 滚动让ljSortByContent2消失 start ***/
    if($(".ljsortByTitle2").hasClass("ljactive")){
        $(".ljsortByTitle2").removeClass("ljactive");
        $(".ljSortByContent2").css("display","none");
    }
    /*** sortBy Mobile 滚动让ljSortByContent2消失 end ***/
});
/*****  过滤条件吸顶效果  end *****/

/****** filter PC过滤 start ******/
/*****  鼠标滑过显示过滤条件  start *****/
$("#ljFilterNav .ljFilterTitle").hover(
        function () {
            $(this).find(".ljDropdownTitle").addClass("ljDropdownTitleActive");
            $(this).find(".ljTitleDropdownContent").css("display","block");
        },
        function () {
            $(this).find(".ljDropdownTitle").removeClass("ljDropdownTitleActive");
            $(this).find(".ljTitleDropdownContent").css("display","none");
        }
);
/*****  鼠标滑过显示过滤条件  end *****/

/*****  点击选择过滤条件  start *****/
$("body").on("click","#ljFilterNav .ljTitleDropdownContent>ul>li",function() {
    ljFilter_fun($(this));
});
/*****  点击选择过滤条件  end *****/

/*****  鼠标滑过Color颜色显示图片颜色标题  start *****/
$(".ljCheckColorBoxDiv").hover(
        function () {
            $(this).find(".ljcolorTextPosition").css("display","block");
        },
        function () {
            $(this).find(".ljcolorTextPosition").css("display","none");
        }
);
/*****  鼠标滑过Color颜色显示图片颜色标题  end *****/
/****** filter PC过滤 end ******/


/****** filter 手机端过滤 start ******/
$("body").on("click","#ljFilter_m_content .ljDropdownTitle",function (){
    var $this = $(this);
    $(this).parent().find(".ljTitleDropdownContent").toggle(function() {
        if($(this).css('display') == "block") {
            $this.addClass("ljDropdownTitle2");
        }else{
            $this.removeClass("ljDropdownTitle2");
        }
    });
});
$("body").on("click","#ljFilter_m_content .ljTitleDropdownContent>ul>li",function (){
    ljFilter_fun($(this));
});


/* 点击标题显示filter过滤条件  start */
$("body").on("click","#ljFilter_m",function() {
    $(".shadow2").animate({
        width: "100%"
    },500);
    $('#ljFilter_m_content').animate({
        width: "80%"
    },500);

    $('#ljFilerTitle_m').animate({
        left: "0%"
    },500);
    // $(".shadow2").css("display","block");
    // $('#ljFilter_m_content').css("display","block");
});
/* 点击标题显示filter过滤条件  end */

/*** 手机端 点击关闭filter过滤条件  start  ***/
$("#ljCloseFilter,#filterDone,.shadow2").click(function() {
    $(".shadow2").animate({
        width: "0px"
    },500);
    $('#ljFilter_m_content').animate({
        width: "0px"
    },500);
    $('#ljFilerTitle_m').animate({
        left: "-80%"
    },500);
    // $(".shadow2").css("display","none");
    // $('#ljFilter_m_content').css("display","none");
});
/*** 手机端 点击关闭filter过滤条件  end  ***/
/****** filter 手机端过滤 end ******/


/*** 替换指定传入参数的值,url为地址,paramName为参数,replaceWith为新值 start ***/
/*
function replaceParamVal(pjaxFilterUrl, paramName, replaceVal, num) {
    // 如果val为空 说明不是第一次拼接url，直接将url连着？或者&清除
    console.log(window.location.search);

    if(replaceVal=="" || replaceVal==" ") {
        // var strUrl  = window.location.search;
        // var searchArr = strUrl.split("&");
        // if(searchArr.length > 1){
        //
        //
        //     var param = "?"+paramName;
        //     console.log(param,"------param");
        //     if(searchArr[0] == param) {
        //         re = eval('/(' + paramName + '=)([^&]*)/gi');
        //     }
        // }
        // var oUrl = pjaxFilterUrl.toString();
        //

        var oUrl = pjaxFilterUrl.toString();
        var re = eval('/([?|&]' + paramName + '=)([^&]*)/gi');
        var nUrl = oUrl.replace(re, "");
        return nUrl;
    }

    // console.log(replaceVal, "replaceVal有值");
    // 判断是否已经有参数 没有参数拼接用？ 已有参数拼接用&
    if(num == '0' || !num) {
        console.log(num,"-----------num");
        var pjaxFilterUrlArr = pjaxFilterUrl.split("?");
        if(pjaxFilterUrlArr.length>1) {
            pjaxFilterUrl = pjaxFilterUrl + "&" + paramName + "=" + replaceVal;
        }else{
            pjaxFilterUrl = pjaxFilterUrl + "?" + paramName + "=" + replaceVal;
        }
    }

    console.log(num,"-----------num2222222222");

    var oUrl = pjaxFilterUrl.toString();
    var re = eval('/(' + paramName + '=)([^&]*)/gi');
    var nUrl = oUrl.replace(re, paramName + '=' + replaceVal);
    return nUrl;
}
 */
function getUrl(eshref){
    var searchArr = [];
    var newStr = eshref;
    var hrefTop = window.location.href.indexOf('?')==-1?window.location.href:window.location.href.slice(0,window.location.href.indexOf('?'));
    if(!eshref){
        searchArr = window.location.search?window.location.search.split("&"):[];
    }else{
        if(eshref.indexOf('?')!=-1){
            searchArr = eshref.slice(eshref.indexOf('?')).split("&");
        }
    }
    if(searchArr.length){
        newStr = hrefTop;
        searchArr.forEach(function(item,index){
            if(!item.slice(item.indexOf("=")+1)){
                if(!index&&searchArr.length>1){
                    newStr+='?';
                }
            }else{
                newStr+=(item+'&')
            }
        })
        newStr = newStr==hrefTop?newStr:newStr.slice(0,-1);
        eshref?'':window.history.replaceState(null,null,newStr);
    }
    return newStr;
}
getUrl();
function replaceParamVal(url,paramName,replaceVal) {
    var oUrl = url.toString();
    var re = eval('/(' + paramName + '=)([^&]*)/gi');
    // var re = new RegExp('(' + paramName + '=)([^&]*)','img')
    var nUrl = oUrl.replace(re, paramName + '=' + replaceVal);
    if(nUrl.indexOf(paramName)==-1){
        var nUrls = nUrl.indexOf('?')==-1?nUrl+'?'+paramName + '=' + replaceVal:nUrl+'&'+paramName + '=' + replaceVal;
        nUrl = !replaceVal? getUrl(nUrl) : nUrls;
    }else{
        !replaceVal? nUrl = getUrl(nUrl) : '';
    }
    return nUrl;
}


/*** 替换指定传入参数的值,url为地址,paramName为参数,replaceWith为新值 start ***/


/* 过滤条件封装函数  ljObj为".ljTitleDropdownContent>ul>li"对象 start */
// var category_urlNum = $("input[name='Category']").val();
// if(category_urlNum!='0' && category_urlNum) {
//     var ljFilterCategoryArr = category_urlNum.split(",");
// }else{
//     var ljFilterCategoryArr = new Array();
// }

var stock_urlNum = $("input[name='Stock']").val();
if(stock_urlNum!='0' && stock_urlNum) {
    var ljFilterStockArr = stock_urlNum.split(",");
}else{
    var ljFilterStockArr = new Array();
}

var retired_urlNum = $("input[name='is_retired']").val();
if(retired_urlNum!='0' && retired_urlNum) {
    var ljFilterRetiredArr = retired_urlNum.split(",");
}else{
    var ljFilterRetiredArr = new Array();
}

// var shape_urlNum = $("input[name='FrameShape']").val();
// if(shape_urlNum!='0' && shape_urlNum) {
//     var ljFilterFrameShapeArr = shape_urlNum.split(",");
// }else{
//     var ljFilterFrameShapeArr = new Array();
// }
//
// var color_urlNum = $("input[name='Color']").val();
// if(color_urlNum!='0' && color_urlNum) {
//     var ljFilterColorArr = color_urlNum.split(",");
// }else{
//     var ljFilterColorArr = new Array();
// }
//
// var ClipOn_urlNum = $("input[name='ClipOn']").val();
// if(ClipOn_urlNum!='0' && ClipOn_urlNum) {
//     var ljFilterClipOnArr = ClipOn_urlNum.split(",");
// }else{
//     var ljFilterClipOnArr = new Array();
// }
//
// var NosePads_urlNum = $("input[name='NosePads']").val();
// if(NosePads_urlNum!='0' && NosePads_urlNum) {
//     var ljFilterNosePadsArr = NosePads_urlNum.split(",");
// }else{
//     var ljFilterNosePadsArr = new Array();
// }
//
// var SpringHinges_urlNum = $("input[name='SpringHinges']").val();
// if(SpringHinges_urlNum!='0' && SpringHinges_urlNum) {
//     var ljFilterSpringHingesArr = SpringHinges_urlNum.split(",");
// }else{
//     var ljFilterSpringHingesArr = new Array();
// }
//
// var ColorChanging_urlNum = $("input[name='ColorChanging']").val();
// if(ColorChanging_urlNum!='0' && ColorChanging_urlNum) {
//     var ljFilterColorChangingArr = ColorChanging_urlNum.split(",");
// }else{
//     var ljFilterColorChangingArr = new Array();
// }
//
// var LowBridgeFit_urlNum = $("input[name='LowBridgeFit']").val();
// if(LowBridgeFit_urlNum!='0' && LowBridgeFit_urlNum) {
//     var ljFilterLowBridgeFitArr = LowBridgeFit_urlNum.split(",");
// }else{
//     var ljFilterLowBridgeFitArr = new Array();
// }
//
// var SportsGlasses_urlNum = $("input[name='SportsGlasses']").val();
// if(SportsGlasses_urlNum!='0' && SportsGlasses_urlNum) {
//     var ljFilterSportsGlassesArr = SportsGlasses_urlNum.split(",");
// }else{
//     var ljFilterSportsGlassesArr = new Array();
// }
//
// var Available_urlNum = $("input[name='Try-OnAvailable']").val();
// if(Available_urlNum!='0' && Available_urlNum) {
//     var ljFilterAvailableArr = Available_urlNum.split(",");
// }else{
//     var ljFilterAvailableArr = new Array();
// }
//
// var RimType_urlNum = $("input[name='RimType']").val();
// if(RimType_urlNum!='0' && RimType_urlNum) {
//     var ljFilterRimTypeArr = RimType_urlNum.split(",");
// }else{
//     var ljFilterRimTypeArr = new Array();
// }
//
// var Material_urlNum = $("input[name='Material']").val();
// if(Material_urlNum!='0' && Material_urlNum) {
//     var ljFilterMaterialArr = Material_urlNum.split(",");
// }else{
//     var ljFilterMaterialArr = new Array();
// }

var Eyeglasses_urlNum = $("input[name='Eyeglasses']").val();
if(Eyeglasses_urlNum!='0' && Eyeglasses_urlNum!='') {
    var ljFilterEyeglassesArr = Eyeglasses_urlNum.split(",");
}else{
    var ljFilterEyeglassesArr = new Array();
}

var Sunglasses_urlNum = $("input[name='Sunglasses']").val();
if(Sunglasses_urlNum!='0' && Sunglasses_urlNum!='') {
    var ljFilterSunglassesArr = Sunglasses_urlNum.split(",");
}else{
    var ljFilterSunglassesArr = new Array();
}

/****
 * type类型不同处理url
 * ljObj 对象 .ljTitleDropdownContent>ul>li
 * arr   数组
 * num   判断是否URL有选中的参数及值 作为判断是否需要替换还是组装url的依据
 * filter_type   类型 判断是哪个类型的依据
 *
 *
 * ****/
function ljFilter_fun_son(ljObj,arr,num,filter_type,pjaxFilterUrl,pjaxFilterValue,pjaxFilterType){
    if(ljObj.find(".ljCheckBox").hasClass("ljCheckBoxActive")) {
        ljObj.find(".ljCheckBox").removeClass("ljCheckBoxActive");
        ljObj.find("a").css("color","#333");
        removeFromArray(arr,pjaxFilterValue);
    }else{
        ljObj.find(".ljCheckBox").addClass("ljCheckBoxActive");
        ljObj.find("a").css("color","#f90");
        arr.push(pjaxFilterValue);
        // 如果是已有过滤条件 title添加颜色
        ljObj.parents(".ljTitleDropdownContent").prev(".ljDropdownTitle").addClass("ljSelectedColor");
    }

    // var urlType = arr.toString();
    // var newUrlType = replaceParamVal(pjaxFilterUrl, pjaxFilterType, urlType, num);
    var urlType = arr.toString();
    if(num == '0') {
        // 判断是否已经有参数 没有参数拼接用？ 已有参数拼接用&
        var pjaxFilterUrlArr = pjaxFilterUrl.split("?");
        if(pjaxFilterUrlArr.length>1) {
            pjaxFilterUrl = pjaxFilterUrl + "&" + pjaxFilterType + "=" + urlType;
        }else{
            pjaxFilterUrl = pjaxFilterUrl + "?" + pjaxFilterType + "=" + urlType;
        }

        // pjaxFilterUrl = pjaxFilterUrl+"&"+pjaxFilterType+"="+urlType;
    }
    var newUrlType = replaceParamVal(pjaxFilterUrl,pjaxFilterType,urlType);
    // console.log(newUrlCategory);

    ljObj.find("a").attr("href",newUrlType);
    if(filter_type == "Category") {
        category_urlNum = urlType;
    }else if(filter_type == "Stock") {
        stock_urlNum = urlType;
    }else if(filter_type == "is_retired") {
        retired_urlNum = urlType;
    }else if(filter_type == "FrameShape") {
        shape_urlNum = urlType;
    }else if(filter_type == "ClipOn") {
        ClipOn_urlNum = urlType;
    }else if(filter_type == "NosePads") {
        NosePads_urlNum = urlType;
    }else if(filter_type == "SpringHinges") {
        SpringHinges_urlNum = urlType;
    }else if(filter_type == "ColorChanging") {
        ColorChanging_urlNum = urlType;
    }else if(filter_type == "LowBridgeFit") {
        LowBridgeFit_urlNum = urlType;
    }else if(filter_type == "SportsGlasses") {
        SportsGlasses_urlNum = urlType;
    }else if(filter_type == "Try-OnAvailable") {
        Available_urlNum = urlType;
    }else if(filter_type == "RimType") {
        RimType_urlNum = urlType;
    }else if(filter_type == "Material") {
        Material_urlNum = urlType;
    }else if(filter_type == "Sunglasses") {
        Sunglasses_urlNum = urlType;
    }else if(filter_type == "Eyeglasses") {
        Eyeglasses_urlNum = urlType;
    }
}

/*****  从数组中移除指定值  ******/
function removeFromArray(arr, val) {
    arr.splice(jQuery.inArray(val,arr),1);
}

function ljFilter_fun(ljObj) {
    /**** 选择条件之后组装url *****/
    var pjaxFilterUrl = window.location.href;
    var pjaxFilterValue = ljObj.find("a").attr("data");
    var pjaxFilterType = ljObj.find("a").attr("ljtype");

    if(pjaxFilterType == "Category") {
        ljFilter_fun_son(ljObj,ljFilterCategoryArr,category_urlNum,"Category",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "Stock") {
        ljFilter_fun_son(ljObj,ljFilterStockArr,stock_urlNum,"Stock",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "is_retired") {
        ljFilter_fun_son(ljObj,ljFilterRetiredArr,retired_urlNum,"is_retired",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "FrameShape") {
        ljFilter_fun_son(ljObj,ljFilterFrameShapeArr,shape_urlNum,"FrameShape",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "ClipOn") {
        ljFilter_fun_son(ljObj,ljFilterClipOnArr,ClipOn_urlNum,"ClipOn",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "NosePads") {
        ljFilter_fun_son(ljObj,ljFilterNosePadsArr,NosePads_urlNum,"NosePads",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "SpringHinges") {
        ljFilter_fun_son(ljObj,ljFilterSpringHingesArr,SpringHinges_urlNum,"SpringHinges",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "ColorChanging") {
        ljFilter_fun_son(ljObj,ljFilterColorChangingArr,ColorChanging_urlNum,"ColorChanging",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "LowBridgeFit") {
        ljFilter_fun_son(ljObj,ljFilterLowBridgeFitArr,LowBridgeFit_urlNum,"LowBridgeFit",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "SportsGlasses") {
        ljFilter_fun_son(ljObj,ljFilterSportsGlassesArr,SportsGlasses_urlNum,"SportsGlasses",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "Try-OnAvailable") {
        ljFilter_fun_son(ljObj,ljFilterAvailableArr,Available_urlNum,"Try-OnAvailable",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "RimType") {
        ljFilter_fun_son(ljObj,ljFilterRimTypeArr,RimType_urlNum,"RimType",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "Material") {
        ljFilter_fun_son(ljObj,ljFilterMaterialArr,Material_urlNum,"Material",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "Eyeglasses") {
        ljFilter_fun_son(ljObj,ljFilterEyeglassesArr,Eyeglasses_urlNum,"Eyeglasses",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "Sunglasses") {
        ljFilter_fun_son(ljObj,ljFilterSunglassesArr,Sunglasses_urlNum,"Sunglasses",pjaxFilterUrl,pjaxFilterValue,pjaxFilterType);
    }else if(pjaxFilterType == "Color") {
        if(ljObj.find(".ljCheckColorBoxDiv").hasClass("ljCheckColorBoxActive")) {
            ljObj.find(".ljCheckColorBoxDiv").removeClass("ljCheckColorBoxActive");
            ljObj.find("a").css("color","#333");
            removeFromArray(ljFilterColorArr,pjaxFilterValue);
        }else{
            ljObj.find(".ljCheckColorBoxDiv").addClass("ljCheckColorBoxActive");
            ljObj.find("a").css("color","#f90");
            ljFilterColorArr.push(pjaxFilterValue);
            // 如果是已有过滤条件 title添加颜色
            ljObj.parents(".ljTitleDropdownContent").prev(".ljDropdownTitle").addClass("ljSelectedColor");
        }

        var urlColor = ljFilterColorArr.toString();
        if(color_urlNum == '0') {
            var pjaxFilterUrlArr = pjaxFilterUrl.split("?");
            if(pjaxFilterUrlArr.length>1) {
                pjaxFilterUrl = pjaxFilterUrl + "&" + pjaxFilterType + "=" + urlColor;
            }else{
                pjaxFilterUrl = pjaxFilterUrl + "?" + pjaxFilterType + "=" + urlColor;
            }
        }
        var newUrlColor = replaceParamVal(pjaxFilterUrl,pjaxFilterType,urlColor);
        ljObj.find("a").attr("href",newUrlColor);
        color_urlNum = urlColor;
    }


    /** 判断其他同辈元素是否还有ljCheckColorBoxActive或ljCheckBoxActive 没有其他选择条件将title颜色变为黑色 start **/
    var colorALength = ljObj.parents(".ljTitleDropdownContent").find(".ljCheckColorBoxActive").length;
    var boxALength = ljObj.parent().find(".ljCheckBoxActive").length;
    if(colorALength > 0){
        ljObj.parents(".forSelectedActive").find(".ljDropdownTitle").addClass("ljSelectedColor");
    }else{
        if(boxALength > 0){
            ljObj.parents(".forSelectedActive").find(".ljDropdownTitle").addClass("ljSelectedColor");
        }else{
            ljObj.parents(".forSelectedActive").find(".ljDropdownTitle").removeClass("ljSelectedColor");
        }
    }
    /** 判断其他同辈元素是否还有ljCheckBoxActive 没有其他选择条件将title颜色变为黑色 end **/
}
/* 过滤条件封装函数  ljObj为".ljTitleDropdownContent>ul>li"对象 end */


/*****   排序点击处理 PC  start   *****/
$("body").on("click",".ljIcon",function() {
    // if($(this).next(".ljSortByContent").hasClass("ljSortActive")) {
    //     $(this).next(".ljSortByContent").removeClass("ljSortActive");
    // }else{
    //     $(this).next(".ljSortByContent").addClass("ljSortActive");
    //     $(this).find(".sort-open").addClass("sort-close");
    // }
    handleMenu()
});
$("body").on("click",".sort-open",function(event) {
    event.stopPropagation()
    handleMenu()
});
function handleMenu() {
    $(".ljSortByContent").toggleClass("ljSortActive")
    $(".sort-open").toggleClass("sort-close")
}
// 初始化sortBY的默认值
var searchStr = window.location.search.slice(1);
if(searchStr.indexOf('sort')!=-1){
    var searchArr = searchStr.split("&");
    if(searchArr.length==1){
        sortText(searchArr[0].slice(searchArr[0].indexOf("=")+1))
    }else{
        searchArr.forEach(function(item,index){
            if(item.indexOf("sort")!=-1){
                sortText(item.slice(item.indexOf("=")+1))
            }
        })
    }
}else{
    $(".sort-result").html("Newest")
}
function sortText(text) {
    var sortInner = 'Newest'
    switch (text) {
        case 'price_desc':
            sortInner = " Price High to Low "
            break;
        case 'price_asc':
            sortInner = " Price Low to High "
            break;
    }
    $(".sort-result").html(sortInner)
}
var sort_urlNum = $("input[name='sort']").val();
$("body").on("click",".ljSortByContent li",function() {
    /**** sortBY 拼接url地址  start ****/
    var pjaxSortbyUrl = window.location.href;
    var pjaxSortbyValue = $(this).find("a").attr("data");
    var pjaxSortbyType = $(this).find("a").attr("ljtype");

    if(sort_urlNum == '0') {
        var pjaxSortbyArr = pjaxSortbyUrl.split("?");
        if(pjaxSortbyArr.length>1) {
            pjaxSortbyUrl = pjaxSortbyUrl + "&" + pjaxSortbyType + "=" + pjaxSortbyValue;
        }else{
            pjaxSortbyUrl = pjaxSortbyUrl + "?" + pjaxSortbyType + "=" + pjaxSortbyValue;
        }
    }

    var newUrlSortby = replaceParamVal(pjaxSortbyUrl,pjaxSortbyType,pjaxSortbyValue);
    $(this).find("a").attr("href",newUrlSortby);
    sort_urlNum = 2 ;
    /**** sortBY 拼接url地址  end ****/

    $(this).parents('#ljsortResult').find(".sort-result").html($(this).find("a").html());
    $(".ljSortByContent").removeClass("ljSortActive");
    $(".sort-open").removeClass("sort-close");
});
$("body").click(function(e) {
    // console.log(e.target);
    if(e.target.className=="ljIcon" || e.target.className=="sort-result") {
        return false;
    }else{
        if($(".ljSortByContent").hasClass("ljSortActive")) {
            $(".ljSortByContent").removeClass("ljSortActive");
        }

        if($(".ljsortByTitle2").hasClass("ljactive")) {
            $(".ljsortByTitle2").removeClass("ljactive");
            $(".ljSortByContent2").css("display","none");
        }
        $(".sort-open").removeClass("sort-close");
    }
});

$(document).pjax('.ljSortByContent a', '#ljListPageContentPjax');
/*****   排序点击处理 PC  end   *****/


/*****   排序点击处理 MOBILE  start   *****/
$("body").on("click",".ljsortByTitle2",function() {
    // $(".shadow2").css("display","block");
    if($(this).hasClass("ljactive")) {
        $(this).removeClass("ljactive");
        $(".ljSortByContent2").css("display","none");
    }else{
        $(this).addClass("ljactive");
        $(".ljSortByContent2").css("display","block");
    }

});

var sort_urlNum2 = $("input[name='sort2']").val();
$("body").on("click",".ljSortByContent2 li",function() {
    /**** sortBY 拼接url地址  start ****/
    var pjaxSortbyUrl2 = window.location.href;
    var pjaxSortbyValue2 = $(this).find("a").attr("data");
    var pjaxSortbyType2 = $(this).find("a").attr("ljtype");

    if(sort_urlNum2 == '0') {
        var pjaxSortbyArr2 = pjaxSortbyUrl2.split("?");
        if(pjaxSortbyArr2.length>1) {
            pjaxSortbyUrl2 = pjaxSortbyUrl2 + "&" + pjaxSortbyType2 + "=" + pjaxSortbyValue2;
        }else{
            pjaxSortbyUrl2 = pjaxSortbyUrl2 + "?" + pjaxSortbyType2 + "=" + pjaxSortbyValue2;
        }
    }

    var newUrlSortby2 = replaceParamVal(pjaxSortbyUrl2,pjaxSortbyType2,pjaxSortbyValue2);
    $(this).find("a").attr("href",newUrlSortby2);
    sort_urlNum2 = 2 ;
    /**** sortBY 拼接url地址  end ****/


    $("#sortBy2 .ljsortByTitle2 .ljIcon").html($(this).find("a").html());
    // $(".shadow2").css("display","none");
    $(".ljsortByTitle2").removeClass("ljactive");
    $(".ljSortByContent2").css("display","none");
});
$(document).pjax('.ljSortByContent2 a', '#ljListPageContentPjax');
/*****   排序点击处理 MOBILE  end   *****/


/*****   选择过滤条件pjax处理数据  start   *****/
$(document).pjax('.ljTitleDropdownContent a', '#ljListPageContentPjax');
$(document).on('pjax:complete', function() {
    // $(".shadow").css("display","none");
    // console.log("pajax:complete====#ljFilterNav .ljTitleDropdownContent a");
    init_swiper();
    start();
})
/*****   选择过滤条件pjax处理数据  end   *****/

/****  阻止a链接自身的url跳转事件 start  ****/
var fn = {
    skip_1: function() {
    },
    skip_2: function() {
        //阻止冒泡
        var event = event || window.event;
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    }
}
// $("body").on("click","a",function() {
//     fn.skip_2();
// });
/****  阻止a链接自身的url跳转事件 end  ****/

