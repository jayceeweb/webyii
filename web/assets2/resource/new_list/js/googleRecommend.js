
/**
 *  param参数必包含 eventType(detail-page-view)、deviceID、sku
 *  可包含参数
 *      originalPrice(原价)、displayPrice(卖价)、currencyCode(YUAN\$\USD)
 *
 * **/
function getNewDeviceID(){
    var T = function T() {
        var d = 1 * new Date(),
                i = 0;
        while (d == 1 * new Date()) {
            i++;
        }
        return d.toString(16) + i.toString(16);
    };
    var R = function R() {
        return Math.random().toString(16).replace('.', '');
    };
    var UA = function UA(n) {
        var ua = navigator.userAgent,
                i,
                ch,
                buffer = [],
                ret = 0;

        function xor(result, byte_array) {
            var j,
                    tmp = 0;
            for (j = 0; j < byte_array.length; j++) {
                tmp |= buffer[j] << j * 8;
            }
            return result ^ tmp;
        }

        for (i = 0; i < ua.length; i++) {
            ch = ua.charCodeAt(i);
            buffer.unshift(ch & 0xFF);
            if (buffer.length >= 4) {
                ret = xor(ret, buffer);
                buffer = [];
            }
        }

        if (buffer.length > 0) {
            ret = xor(ret, buffer);
        }

        return ret.toString(16);
    };

    var ret = function () {
        // 有些浏览器取个屏幕宽度都异常...
        var se = String(screen.height * screen.width);
        if (se && /\d{5,}/.test(se)) {
            se = se.toString(16);
        } else {
            se = String(Math.random() * 31242).replace('.', '').slice(0, 8);
        }
        var val = T() + '-' + R() + '-' + UA() + '-' + se + '-' + T();
        if (val) {
            return val;
        } else {
            return (String(Math.random()) + String(Math.random()) + String(Math.random())).slice(2, 15);
        }
    };

    return ret();
};

var getLocalDeviceID = "";
var getLocalDeviceIDTemp = localStorage.getItem("ljUUID");
if(!getLocalDeviceIDTemp) {
    var ljUUID = getNewDeviceID();
    localStorage.setItem("ljUUID",ljUUID);
    getLocalDeviceID = ljUUID;
}
getLocalDeviceID = localStorage.getItem("ljUUID");


// 镜架详情页
function getDataFun() {
    // 进入页面就要带SKU，知道是哪个产品
    var ljConfigurableID = $("#orderRx").attr("configure_product_id");
    var ljDestSku = $("label.active").attr("dest_sku");
    var ljSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
    var ljLensType = $("label.active").attr("lens-type");
    // 给googleRecommendAPI用
    var ljPrice = $("label.active").attr("price");
    var ljSalesPrice = $("label.active").attr("sales_price");

    var ljpdp_Data = {
        "configurableID": ljConfigurableID,
        "destSku": ljDestSku,
        "lensType": ljLensType,
        "sku":ljSku,
        "originalPrice": parseFloat(ljPrice),
        "displayPrice": parseFloat(ljSalesPrice)
    };
    return ljpdp_Data;
}
// var ljpdp_Data_session = getDataFun();

// 配件页数据
function getAccessData() {
    var accessProductID = $(".FrameColor_select").parent().attr("product");
    var accessSku = $(".FrameColor_select").parent().attr("sku");
    var ljSku = accessSku;
    // 给googleRecommendAPI用
    var ljPrice = $(".FrameColor_select").parent().attr("price");
    var ljSalesPrice = $(".FrameColor_select").parent().attr("special_price");
    if(ljSalesPrice == "0.00") {
        ljSalesPrice = ljPrice;
    }

    var ljDataObj = {
        "configurableID": accessProductID,
        "destSku": accessSku,
        "sku": ljSku,
        "originalPrice": parseFloat(ljPrice),
        "displayPrice": parseFloat(ljSalesPrice)
    };
    return ljDataObj;
}
// var accessData_session = getAccessData();

