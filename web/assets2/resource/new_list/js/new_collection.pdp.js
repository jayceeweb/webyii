/*   分享js代码所在：   D:\zhijing\webyii_collection_v1\web\assets2\resource\pdp-summary\js\social.js  */


// 将获得的数据去除回车、空格
function ljRemoveEnterOrSpace(strObj) {
    // console.log(strObj);
    var res = strObj.replace(/[\r\n]/g,"");
    res = res.replace(/[ ]/g,"");
    res = res.replace(/\ +/g,"");
    return res;
}

function getDataFun() {
    // 进入页面就要带SKU，知道是哪个产品
    var ljProductColor = $(".btn-color-click.active").attr("simple_product_color");
    var ljColorValue = $(".btn-color-click.active").attr("color_value");
    var ljDestSku = $("label.active").attr("dest_sku");
    var ljPrice = $("label.active").attr("price");
    var ljSalesPrice = $("label.active").attr("sales_price");
    var ljLensType = $("label.active").attr("lens-type");
    var ljFrameGroup = $("label.active").attr("frame_group");
    ljFrameGroup = ljRemoveEnterOrSpace(ljFrameGroup) ;
    var ljSimpleUrl = $("label.active").attr("simple_url");
    var ljStockSku = $("#orderRx").attr("stock_sku");
    var reviewSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
    var ljEntityID = $("#orderRx").attr("simple_product_id");
    var ljConfigurableID = $("#orderRx").attr("configure_product_id");
    var ljConfigureProductSku = $("#orderRx").attr("configure_product_sku");
    var ljCategoryID = $("#orderRx").attr("category_id");
    var ljCatetoryType = $("#orderRx").attr("catetory_type");
    var ljStock = $("#orderRx").attr("ljstock");
    var ljFrameGroupHref = $("#orderRx").attr("frame_group_href");   // 镜框类别 rx_non_progressive_semi_rimless、goggles、full_rim

    var ljpdp_Data = {
        "productColor": ljProductColor,
        "productColorValue": ljColorValue,
        "destSku": ljDestSku,
        "price": ljPrice,
        "salesPrice": ljSalesPrice,
        "lensType": ljLensType,
        "frameGroup": ljFrameGroup,
        "frameGroupHref": ljFrameGroupHref,
        "simpleUrl": ljSimpleUrl,
        "stockSku": ljStockSku,
        "reviewSku":reviewSku,
        "entityID": ljEntityID,
        "configurableID": ljConfigurableID,
        "configureProductSku": ljConfigureProductSku,
        "categoryID": ljCategoryID,
        "catetoryType": ljCatetoryType,
        "stock": ljStock
    };

    return ljpdp_Data;
}

var ljpdp_Data_session = getDataFun();
smart.init('collectionPdp', {
    local_storage: {
        type: 'localStorage'
    },
    SPA: {
        is: true,
        mode: 'hash'
    },
    // pageview: false,   // 禁止自动触发PV事件
    debug: true,
    loaded: function(sdk) {
        // luanjue_3  清除super_properties
        sdk.clear_event_super_properties();
        sdk.register_event_super_properties(ljpdp_Data_session);
    }
});
//  smart.track_pv();   //  手动触发PV事件


//  用户点击事件获取的当前页面数据 ，除了color和已进入页面之外，因为这里的数据都是页面加载完成之后显示的数据才可加载到
function getPageData() {
    var ljProductColor = $(".btn-color-click.active").attr("simple_product_color");
    var ljColorValue = $(".btn-color-click.active").attr("color_value");
    var ljDestSku = $("label.active").attr("dest_sku");
    var ljPrice = $("label.active").attr("price");
    var ljSalesPrice = $("label.active").attr("sales_price");
    var ljLensType = $("label.active").attr("lens-type");
    var ljFrameGroup = $("label.active").attr("frame_group");
    ljFrameGroup = ljRemoveEnterOrSpace(ljFrameGroup) ;
    var ljSimpleUrl = $("label.active").attr("simple_url");
    var ljStockSku = $("#orderRx").attr("stock_sku");
    var reviewSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
    var ljEntityID = $("#orderRx").attr("simple_product_id");
    var ljConfigurableID = $("#orderRx").attr("configure_product_id");
    var ljConfigureProductSku = $("#orderRx").attr("configure_product_sku");
    var ljCategoryID = $("#orderRx").attr("category_id");
    var ljCatetoryType = $("#orderRx").attr("catetory_type");
    var ljStock = $("body").find("#orderRx").attr("ljstock");
    var ljFrameGroupHref = $("#orderRx").attr("frame_group_href");   // 镜框类别 rx_non_progressive_semi_rimless、goggles、full_rim

    var ljDataObj = {
        "productColor": ljProductColor,
        "productColorValue": ljColorValue,
        "destSku": ljDestSku,
        "price": ljPrice,
        "salesPrice": ljSalesPrice,
        "lensType": ljLensType,
        "frameGroup": ljFrameGroup,
        "frameGroupHref": ljFrameGroupHref,
        "simpleUrl": ljSimpleUrl,
        "stockSku": ljStockSku,
        "reviewSku":reviewSku,
        "entityID": ljEntityID,
        "configurableID": ljConfigurableID,
        "configureProductSku": ljConfigureProductSku,
        "categoryID": ljCategoryID,
        "catetoryType": ljCatetoryType,
        "stock": ljStock
    };

    return  ljDataObj;
}

//  1、添加监听点击OrderGlasses按钮事件 \ out of stock是，orderGlasses变成Sign Up
smart.time_event('pdp_order_glasses');
$("body").on("click","#orderRx",function(e,param1) {
    var param1 = param1 || "";
    var ljDataObj = getPageData();
    //  如果是blue抗蓝,不需要再次采集order_glasses事件
    if(param1 == "blue") {
        return;
    }
    smart.track_event("pdp_order_glasses", ljDataObj);
});

//   6、 blueLightBlocking -- OrderGlasses -- Pres 事件 填写验光单   pdp-summary\js\pdp-revision\detail.js文件layer触发地方
//   7、 blueLightBlocking -- OrderGlasses -- Non-pres 事件 不填写验光单
//  9、pdp页面Out Of Stock时，signUp 提交confirm按钮事件
//  10、pdp页面Out Of Stock时，signUp 提交cancel按钮事件

//  11、pdp页面tryOn按钮事件
smart.time_event('pdp_try_on');
$("body").on("click",".btn-Try-class",function() {
    var ljDataObj = getPageData();
    smart.track_event("pdp_try_on", ljDataObj);
});


//  2、添加监听点击切换LensType事件    lens_type切换html数据不对，从js中取，代码在detail.js下
// smart.time_event('pdp_lens_type');
// $("body").on("click",".custom-control-label",function() {
//     var ljDataObj = getPageData();
//     ljDataObj['lensType'] = $(this).parent().attr("lens-type");
//     ljDataObj['price'] = $(this).parent().attr("price");
//     ljDataObj['salesPrice'] = $(this).parent().attr("sales_price");
//     ljDataObj['destSku'] = $(this).parent().attr("dest_sku");
//
//     smart.track_event("pdp_lens_type", ljDataObj);
// });

//   3、添加监听点击Write A Review评论事件
smart.time_event('pdp_review');
$("body").on("click",".review-pop-show",function() {
    var ljDataObj = getPageData();
    smart.track_event("pdp_review", ljDataObj);
});

//   4、添加监听点击Write A Review写的评论提交
smart.time_event('pdp_review_submit');
$("body").on("click","#reveiws-sub",function() {
    var ljDataObj = getPageData();

    // 填写评论相关内容数据
    ljDataObj["star"] = $(".selFa").length;
    ljDataObj["recommend"] = $(".acs_fieldset.acs_fieldset_click").find(".fa").eq(0).hasClass("fa-thumbs-up") ? "yes":"no";

    /***  验证评论正确性， 正确发送采集数据   ***/
    var reviewsData = {};
    //评论标题填写val值
    var YourReview = $("#YourReview").val();
    //评论填写当前名字
    var Nickname = $("#Nickname").val();
    //评论内容获取
    var PreiwsContent = $("#describe").val();
    //邮箱内容
    var Email = $("#Email_S").val();
    var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    reviewsData.reviewHeader = YourReview;
    reviewsData.nickname = Nickname;
    reviewsData.email = Email;

    if(YourReview!="" && Nickname!="" && PreiwsContent!="" && reviewsData.score!=0 && reviewsData.recommend!=0 && Email!="" && reg.test(Email)){
        smart.track_event("pdp_review_submit", ljDataObj);
    }


});

/****
//  5、切换颜色事件采集 pdp_frame_color   放到detail.js文件中了 路径pdp-summary\js\pdp-revision\detail.js
****/

//   8、 PDP页面Save点击事件 分cancel取消和confirm确定
smart.time_event('pdp_save');
// smart.time_event('pdp_save_cancel');
// smart.time_event('pdp_save_cancel');
$("body").on("click","#save_x",function() {
    var ljDataObj = getPageData();

    if($(this).find(".fa-heart").hasClass("save_after_one")) {
        ljDataObj['saveType'] = 1;
    }
    if($(this).find(".fa-heart").hasClass("save_after_two")) {
        ljDataObj['saveType'] = 0;
    }
    smart.track_event("pdp_save", ljDataObj);
});

// 16、 pdp_order_Addl_Clip-Ons pdp页面点击Order Addl.Clip-ons按钮事件
smart.time_event('pdp_order_Addl_Clip-Ons');
$("body").on("click","#clip_ons_click",function(e) {
    var ljDataObj = getPageData();
    smart.track_event("pdp_order_Addl_Clip-Ons", ljDataObj);
});

// 17、 pdp_order_Addl_Clip-Ons   addToCart按钮事件
smart.time_event('pdp_order_Addl_Clip-Ons_addToCart');
$("body").on("click","#clipCard_1034",function(e) {
    var ljDataObj = getPageData();
    // ClipOns数据
    ljDataObj["clipConfigid"] = $(this).attr("configid");
    ljDataObj["clipSimpleid"] = $(this).attr("simpleid");
    ljDataObj["clipColor"] = $(this).attr("color");
    ljDataObj["clipSku"] = $(this).parents(".modal-clip").find(".modal-clip-sku span").eq(0).text();

    smart.track_event("pdp_order_Addl_Clip-Ons_addToCart", ljDataObj);
});

// 18、 pdp_recommend_swiperSlide 推荐产品点击事件
smart.time_event('pdp_recommend_swiperSlide');
$("body").on("click",".recommend_swiperSlide img",function(e) {
    var ljDataObj = getPageData();
    // 推荐产品url
    ljDataObj["recommendUrl"] = $(this).parent().attr("href");
    ljDataObj["recommendColor"] = $(this).parent().attr("color");
    ljDataObj["recommendEntityId"] = $(this).parent().attr("entity_id");
    ljDataObj["recommendProductId"] = $(this).parent().attr("product_id");
    ljDataObj["recommendParentId"] = $(this).parent().attr("parent_id");
    ljDataObj["recommendSku"] = $(this).parent().attr("sku");
    ljDataObj["recommendPrice"] = $(this).parent().attr("price");
    ljDataObj["recommendSalesPrice"] = $(this).parent().attr("special_price");
    ljDataObj["recommendName"] = $(this).parent().attr("name");

    smart.track_event("pdp_recommend_swiperSlide", ljDataObj);
});
// 19、 pdp_recommend_swiperSlide_color  推荐产品颜色切换点击事件
smart.time_event('pdp_recommend_swiperSlide_color');
$("body").on("click",".recommend_swiperSlide .color_value_click",function(e) {
    var ljDataObj = getPageData();

    // 推荐产品url
    ljDataObj["recommendUrl"] = $(this).attr("color_url");
    ljDataObj["recommendColor"] = $(this).find("span").eq(0).css("background");
    ljDataObj["recommendPrice"] = $(this).attr("price");
    ljDataObj["recommendSalesPrice"] = $(this).attr("special_price");
    ljDataObj["recommendEntityId"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("entity_id");
    ljDataObj["recommendProductId"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("product_id");
    ljDataObj["recommendParentId"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("parent_id");
    ljDataObj["recommendSku"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("sku");
    ljDataObj["recommendName"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("name");

    smart.track_event("pdp_recommend_swiperSlide_color", ljDataObj);
});
//  Order as Sunglasses
smart.time_event('pdp_order_as_sunOrEye');
$("body").on("click","#relate_product a",function(e) {
    var ljDataObj = getPageData();

    if($(this).text()=="Order as Sunglasses") {
        ljDataObj["sunOrEyeGlassesType"] = "as_sunglasses";
    }
    if($(this).text()=="Order as Eyeglasses") {
        ljDataObj["sunOrEyeGlassesType"] = "as_eyeglasses";
    }
    smart.track_event("pdp_order_as_sunOrEye", ljDataObj);
});

//  12、pdp页面Like按钮事件
smart.time_event('pdp_like');
$("body").on("click",".link_class",function() {
    var ljProductColor = $(".btn-color-click.active").attr("simple_product_color");
    var ljColorValue = $(".btn-color-click.active").attr("color_value");
    var ljDestSku = $("label.active").attr("dest_sku");
    var ljPrice = $("label.active").attr("price");
    var ljSalesPrice = $("label.active").attr("sales_price");
    var ljLensType = $("label.active").attr("lens-type");
    var ljFrameGroup = $("label.active").attr("frame_group");
    ljFrameGroup = ljRemoveEnterOrSpace(ljFrameGroup) ;
    var ljSimpleUrl = $("label.active").attr("simple_url");
    var ljStockSku = $("#orderRx").attr("stock_sku");
    var reviewSku = $("#fix-fluid div.container-fluid.reveiws-container").attr("sku");
    var ljEntityID = $("#orderRx").attr("simple_product_id");
    var ljConfigurableID = $("#orderRx").attr("configure_product_id");
    var ljConfigureProductSku = $("#orderRx").attr("configure_product_sku");
    var ljCategoryID = $("#orderRx").attr("category_id");
    var ljCatetoryType = $("#orderRx").attr("catetory_type");
    var ljStock = $("body").find("#orderRx").attr("ljstock");

    var ljDataObj = {
        "productColor": ljProductColor,
        "productColorValue": ljColorValue,
        "destSku": ljDestSku,
        "price": ljPrice,
        "salesPrice": ljSalesPrice,
        "lensType": ljLensType,
        "frameGroup": ljFrameGroup,
        "simpleUrl": ljSimpleUrl,
        "stockSku": ljStockSku,
        "reviewSku":reviewSku,
        "entityID": ljEntityID,
        "configurableID": ljConfigurableID,
        "configureProductSku": ljConfigureProductSku,
        "categoryID": ljCategoryID,
        "catetoryType": ljCatetoryType,
        "stock": ljStock
    };
    // console.log(ljDataObj);
    smart.track_event("pdp_like", ljDataObj);
});



