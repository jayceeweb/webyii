(function(){
    /* 原登录页逻辑 start  */
    var cache_storage_pg=JSON.parse(localStorage['mage-cache-storage-pg']||'null');
    if(cache_storage_pg){
        if(cache_storage_pg['guest-profile']){
            var localPorfile = cache_storage_pg['guest-profile']
            /**判断存储为json对象的时候*/
            if(typeof localPorfile != "object"){
                localPorfile = JSON.parse(localPorfile);
            }
            // console.log(document.querySelector(".Yourself_Text"));
            document.querySelector(".YOURSELF_text").innerText=localPorfile.nickname;
            document.querySelector(".Yourself_s1").innerText=localPorfile.nickname;
        }
    }
    var setCookie=function(cname,cvalue,time,path){
        var d = new Date();
        d.setTime(d.getTime()+time);
        var expires = "expires="+d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires+"; path="+path;
    };
    var userLogin={
        sendData:function(e){
            if(!cache_storage_pg)return;
            var cache_cookie_pg={};
            for(var key in cache_storage_pg){
                if(key=='guest-profile')
                    cache_cookie_pg['profile']=cache_storage_pg[key];
                else if(key=='guest-prescription')
                    cache_cookie_pg['prescription']=cache_storage_pg[key];
                else if(key!='global_closed_message')
                    cache_cookie_pg[key]=cache_storage_pg[key];
            }
            //add by niu 2019-9-16 修改cookie过期时间由1秒改为10分钟
            setCookie('mage-cache-cookie-pg',JSON.stringify(cache_cookie_pg),1000*600,'/');
        },
    };
    document.querySelectorAll('.pslogin-button-auto').forEach(function(item){
        item.addEventListener('click', function () {
            //Login page click the third party plug-in button triggers the event to write the enterprise student number that is not logged in andlastName Start
            // console.log("第三方点击");
            var orgId = window.localStorage.getItem("org_id");
            var id_orgCustomerId = window.localStorage.getItem("org_customer_id");
            if(orgId && id_orgCustomerId){
                setCookie("org_id",orgId,1000*600,'/');
                setCookie("org_customer_id",id_orgCustomerId,1000*600,'/');
            }
            //Login page click the third party plug-in button triggers the event to write the enterprise student number that is not logged in andlastName End
            //amazon,facebook,twitter第三方登录按钮点击事件(/customer/account/login/页面) by niu 2019-9-16
            userLogin.sendData();
        });
    });
    /* 原登录页逻辑 end  */

    // 创建用户验证
    $("body").on("click",".creatButton",function(e) {
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var email_address = $("#email_address").val();
        var password = $("#password").val();
        var password_confirmation = $("input[name='password_confirmation']").val();
        var persistent_remember_me = $("input[name='persistent_remember_me']").val();
        var is_subscribed = $("input[name='is_subscribed']").val();
        var referer = $("input[name='referer']").val();

        var reg = /^[0-9a-zA-Z_.-]+[@][0-9a-zA-Z_.-]+([.][a-zA-Z]+){1,2}$/;
        if(firstname.length <= 0) {
            if($("#firstname").next().hasClass("hiddenLJ")) {
                $("#firstname").next().removeClass("hiddenLJ");
            }
            $("#firstname").addClass("mage-error");
        }
        if(lastname.length <= 0) {
            if($("#lastname").next().hasClass("hiddenLJ")) {
                $("#lastname").next().removeClass("hiddenLJ");
            }
            $("#lastname").addClass("mage-error");
        }
        if((email_address.length <= 0) || (!reg.test(email_address))) {
            if($("#email_address").next().hasClass("hiddenLJ")) {
                $("#email_address").next().removeClass("hiddenLJ");
            }
            $("#email_address").addClass("mage-error");
        }
        if(password.length < 6) {
            if($("#password").next().hasClass("hiddenLJ")) {
                $("#password").next().removeClass("hiddenLJ");
            }
            $("#password").addClass("mage-error");
        }
        if((password_confirmation.length < 6) || (password_confirmation != password)) {
            if($("#password-confirmation").next().hasClass("hiddenLJ")) {
                $("#password-confirmation").next().removeClass("hiddenLJ");
            }
            $("#password-confirmation").addClass("mage-error");
        }
        if( firstname.length>1 && lastname.length>1 && (email_address.length>0&&reg.test(email_address)) && (password.length>=6 &&password_confirmation==password) ){

            // 原登录页逻辑 start
            userLogin.sendData();
            delete localStorage.loginUserPwd;   //delete localstory password

            var orgId = window.localStorage.getItem("org_id");
            var id_orgCustomerId = window.localStorage.getItem("org_customer_id");
            if(orgId && id_orgCustomerId){
                setCookie("org_id",orgId,1000*600,'/');
                setCookie("org_customer_id",id_orgCustomerId,1000*600,'/');
            }
            // 原登录页逻辑 end


            $(".errorMessage").addClass("hiddenLJ");
            $(".input-text").removeClass("mage-error");
            var data = {
                "firstname" : firstname,
                "lastname" : lastname,
                "email": email_address,
                "password" : password,
                "password_confirmation" : password_confirmation,
                "persistent_remember_me" : persistent_remember_me,
                "is_subscribed" : is_subscribed,
                "referer" : referer
            };

            $.ajax({
                url: "/customer/account/createpost/",
                type: 'post',
                data: data,
                beforeSend:function(){
                    // alert('远程调用开始...');
                    $("#loading").css("display","block");
                },
                success:function(res,textStatus){
                    // alert('开始回调，状态文本值：'+textStatus+' 返回数据：'+res);
                    var resObj = JSON.parse(res);
                    if(resObj.msg == "success") {
                        $("errorMessage").addClass("hiddenLJ");
                        window.location.href = resObj.redirect_url;
                    }else{
                        alert("系统繁忙，稍后在做！");
                    }
                },
                complete:function(XMLHttpRequest,textStatus){
                    // alert('远程调用成功，状态文本值：'+textStatus);
                    $("#loading").css("display","none");
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){
                    console.log('error...状态文本值：'+textStatus+" 异常信息："+errorThrown);
                }
            });

        }
    });

    // firstname
    $("#firstname").on("blur",function() {
        if($(this).val().length <= 0) {
            if($("#firstname").next().hasClass("hiddenLJ")) {
                $("#firstname").next().removeClass("hiddenLJ");
            }
            $("#firstname").addClass("mage-error");
        }else{
            $("#firstname").removeClass("mage-error");
            $("#firstname").next().addClass("hiddenLJ");
        }
    });
    $("#lastname").on("blur",function() {
        if($(this).val().length <= 0) {
            if($("#lastname").next().hasClass("hiddenLJ")) {
                $("#lastname").next().removeClass("hiddenLJ");
            }
            $("#lastname").addClass("mage-error");
        }else{
            $("#lastname").removeClass("mage-error");
            $("#lastname").next().addClass("hiddenLJ");
        }
    });
    $("#email_address").on("blur",function() {
        var email_address = $(this).val();
        var reg = /^[0-9a-zA-Z_.-]+[@][0-9a-zA-Z_.-]+([.][a-zA-Z]+){1,2}$/;
        if((email_address.length <= 0) || (!reg.test(email_address))) {
            if($("#email_address").next().hasClass("hiddenLJ")) {
                $("#email_address").next().removeClass("hiddenLJ");
            }
            $("#email_address").addClass("mage-error");
        }else{
            $("#email_address").removeClass("mage-error");
            $("#email_address").next().addClass("hiddenLJ");
        }
    });

    // password验证
    $('[name="password"]').on('keyup', function () {
        $("#password").removeClass("mage-error");
        $("#password").next().addClass("hiddenLJ");

        var password = $(this).val();
        if(password.length<6) {
            $(".tipLength").addClass("tip-error");
            $(".tipLength").find(".glyphicon").removeClass("glyphicon-ok");
            $(".tipLength").find(".glyphicon").addClass("glyphicon-remove");
        }else{
            if($(".tipLength").hasClass('tip-error')) {
                $(".tipLength").removeClass("tip-error");
                $(".tipLength").find(".glyphicon").addClass("glyphicon-ok");
                $(".tipLength").find(".glyphicon").removeClass("glyphicon-remove");
            }
            $(".tipLength").addClass("tip-success");

            //定义验证规则，由于字符串数字和字母的顺序可能不同。
            //也有可能字母和数字中间还包含了其他字符。故将验证规则分开定义。
            var regNumber = /\d+/;          //验证0-9的任意数字最少出现1次。
            var regString = /[a-zA-Z]+/;    //验证大小写26个字母任意字母最少出现1次。
            if (regNumber.test(password) && regString.test(password)) {
                // console.log("验证成功");
                if($(".tipRule").hasClass('tip-error')) {
                    $(".tipRule").removeClass("tip-error");
                    $(".tipRule").find(".glyphicon").addClass("glyphicon-ok");
                    $(".tipRule").find(".glyphicon").removeClass("glyphicon-remove");
                }
                $(".tipRule").addClass("tip-success");
            }else{
                $(".tipRule").addClass("tip-error");
                $(".tipRule").find(".glyphicon").removeClass("glyphicon-ok");
                $(".tipRule").find(".glyphicon").addClass("glyphicon-remove");
            }
        }
    });

    // confirmPassword验证
    $('[name="password_confirmation"]').on('blur', function () {
        var password = $("#password").val();
        if($(this).val() != password) {
            if($("input[name='password_confirmation']").next().hasClass("hiddenLJ")) {
                $("input[name='password_confirmation']").next().removeClass("hiddenLJ");
            }
            $("input[name='password_confirmation']").addClass("mage-error");
        }else{
            $("input[name='password_confirmation']").next().addClass("hiddenLJ");
            $("input[name='password_confirmation']").removeClass("mage-error");
        }
    });


    //  亚马逊登录
    var authRequest;
    OffAmazonPayments.Button("AmazonLoginButton", "A32U22DDJM8453", {
        type: "LwA",
        color: "Gold",
        authorization: function () {
            loginOptions = { scope: "profile postal_code payments:widget payments:shipping_address", popup: true };
            authRequest = amazon.Login.authorize(loginOptions, "/amazon/login/authorize");
        },
        onError: function (error) {
            // something bad happened
            console.log(error,"------------------------error");
        }
    });

})();
