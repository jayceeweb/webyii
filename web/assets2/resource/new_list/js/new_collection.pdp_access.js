


// 将获得的数据去除回车、空格
    function ljRemoveEnterOrSpace(strObj) {
        // console.log(strObj);
        var res = strObj.replace(/[\r\n]/g, "");
        res = res.replace(/[ ]/g, "");
        res = res.replace(/\ +/g, "");
        return res;
    }

//  只有选中的颜色div才有.FrameColor_select这个类
    function getDataFun() {
        // 进入页面就要带SKU，知道是哪个产品
        var accessEntryID = $(".FrameColor_select").parent().attr("entryid");
        var accessProductID = $(".FrameColor_select").parent().attr("product");
        var accessColorID = $(".FrameColor_select").parent().attr("color_id");
        var accessName = $(".FrameColor_select").parent().attr("name");
        var accessPrice = $(".FrameColor_select").parent().attr("price");
        var accessSalesPrice = $(".FrameColor_select").parent().attr("special_price");
        var accessSku = $(".FrameColor_select").parent().attr("sku");
        var accessUrl = $(".FrameColor_select").parent().attr("url");
        var accessStock = $(".FrameColor_select").parent().attr("stock");
        var reviewSku = $("#fix-fluid div.container-fluid reveiws-container").attr("sku");

        var dataAccess = {
            "entityID": accessEntryID,
            "productID": accessProductID,
            "productColor": accessColorID,
            "productName": accessName,
            "price": accessPrice,
            "salesPrice": accessSalesPrice,
            "destSku": accessSku,
            "simpleUrl": accessUrl,
            "stock": accessStock,
            'reviewSku': reviewSku
        };
        return dataAccess;
    }

    var dataAccess = getDataFun();
    smart.init('collectionPdp', {
        local_storage: {
            type: 'localStorage'
        },
        SPA: {
            is: true,
            mode: 'hash'
        },
        // pageview: false,
        debug: true,

        loaded: function (sdk) {
            // luanjue_3  清除super_properties
            sdk.clear_event_super_properties();
            sdk.register_event_super_properties(dataAccess);
        }
    });


//  用户点击事件获取的当前页面数据 ，除了color和已进入页面之外，因为这里的数据都是页面加载完成之后显示的数据才可加载到
    function getPageData() {
        var accessEntryID = $(".FrameColor_select").parent().attr("entryid");
        var accessProductID = $(".FrameColor_select").parent().attr("product");
        var accessColorID = $(".FrameColor_select").parent().attr("color_id");
        var accessName = $(".FrameColor_select").parent().attr("name");
        var accessPrice = $(".FrameColor_select").parent().attr("price");
        var accessSalesPrice = $(".FrameColor_select").parent().attr("special_price");
        var accessSku = $(".FrameColor_select").parent().attr("sku");
        var accessUrl = $(".FrameColor_select").parent().attr("url");
        var accessStock = $(".FrameColor_select").parent().attr("stock");
        var reviewSku = accessSku;

        var ljDataObj = {
            "entityID": accessEntryID,
            "productID": accessProductID,
            "productColor": accessColorID,
            "productName": accessName,
            "price": accessPrice,
            "salesPrice": accessSalesPrice,
            "destSku": accessSku,
            "simpleUrl": accessUrl,
            "stock": accessStock,
            "reviewSku": reviewSku
        };
        return ljDataObj;
    }


//   1、 PDP配件页面Save点击事件 分cancel取消和confirm确定
    smart.time_event('pdp_access_save');
    $("body").on("click", "#save_x", function () {
        var dataSave = getPageData();
        // console.log(JSON.stringify(dataSave));

        if ($(this).hasClass("save_after_one")) {
            dataSave['accessSaveType'] = 1;
        }
        if ($(this).hasClass("save_after_two")) {
            dataSave['accessSaveType'] = 0;
        }
        smart.track_event("pdp_access_save", dataSave);
    });

    /*****
     //  2、 PDP配件页面Color切换事件    放到new_swiper.js文件中了 路径 resource\js\new_swiper.js
     smart.time_event('pdp_access_color');
     $("body").on("click",".fram_list",function() {
    smart.track_event("pdp_access_color", dataSave);
});
     ******/

//   3、添加监听点击Write A Review评论事件
    smart.time_event('pdp_access_review');
    $("body").on("click", ".reviews_a, .btn-Write", function () {
        var dataSave = getPageData();
        smart.track_event("pdp_access_review", dataSave);
    });

//   4、添加监听点击Write A Review写的评论提交
    smart.time_event('pdp_access_review_submit');
    $("body").on("click", "#reveiws-sub", function () {
        var dataSave = getPageData();
        // 填写评论相关内容数据
        dataSave['star'] = $(".selFa").length;
        dataSave['recommend'] = $(".acs_fieldset.acs_fieldset_click").find(".fa").eq(0).hasClass("fa-thumbs-up") ? "yes" : "no";

        /***  验证评论正确性， 正确发送采集数据   ***/
        var reviewsData = {};
        //评论标题填写val值
        var YourReview = $("#YourReview").val();
        //评论填写当前名字
        var Nickname = $("#Nickname").val();
        //评论内容获取
        var PreiwsContent = $("#describe").val();
        //邮箱内容
        var Email = $("#Email_S").val();
        var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        reviewsData.reviewHeader = YourReview;
        reviewsData.nickname = Nickname;
        reviewsData.email = Email;

        if(YourReview!="" && Nickname!="" && PreiwsContent!="" && reviewsData.score!=0 && reviewsData.recommend!=0 && Email!="" && reg.test(Email)){
            smart.track_event("pdp_access_review_submit", dataSave);
        }
    });

//   5、add to cart事件
    smart.time_event('pdp_access_addToCart');
    $("body").on("click", "#orderRx", function () {
        var dataSave = getPageData();
        smart.track_event("pdp_access_addToCart", dataSave);
    });

// 6、 pdp_access_recommend_swiperSlide 推荐产品点击事件
    smart.time_event('pdp_access_recommend_swiperSlide');
    $("body").on("click", ".recommend_swiperSlide img", function (e) {
        var dataSave = getPageData();
        // 推荐产品url
        dataSave["recommendUrl"] = $(this).parent().attr("href");
        dataSave["recommendColor"] = $(this).parent().attr("color");
        dataSave["recommendEntityId"] = $(this).parent().attr("entity_id");
        dataSave["recommendProductId"] = $(this).parent().attr("product_id");
        dataSave["recommendParentId"] = $(this).parent().attr("parent_id");
        dataSave["recommendSku"] = $(this).parent().attr("sku");
        dataSave["recommendPrice"] = $(this).parent().attr("price");
        dataSave["recommendSalesPrice"] = $(this).parent().attr("special_price");
        // dataSave["recommendSalesPrice"] = $(this).parents(".recommend_swiperSlide").find("li").eq(0).find("span").eq(0).text();
        dataSave["recommendName"] = $(this).parent().attr("name");

        smart.track_event("pdp_access_recommend_swiperSlide", dataSave);
    });

// 7、 pdp_access_recommend_swiperSlide_color 推荐产品颜色切换事件
    smart.time_event('pdp_access_recommend_swiperSlide_color');
    $("body").on("click", ".recommend_swiperSlide .color_value_click", function (e) {
        var dataSave = getPageData();
        // 推荐产品url
        dataSave["recommendUrl"] = $(this).attr("color_url");
        dataSave["recommendColor"] = $(this).find("span").eq(0).css("background");
        dataSave["recommendEntityId"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("entity_id");
        dataSave["recommendProductId"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("product_id");
        dataSave["recommendParentId"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("parent_id");
        dataSave["recommendSku"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("sku");
        dataSave["recommendPrice"] = $(this).attr("price");
        dataSave["recommendSalesPrice"] = $(this).attr("special_price");
        dataSave["recommendName"] = $(this).parents(".recommend_swiperSlide").find("a").eq(0).attr("name");

        smart.track_event("pdp_access_recommend_swiperSlide_color", dataSave);
    });


//  8、pdp页面Out Of Stock时，signUp 提交confirm按钮事件
    smart.time_event('pdp_outofstock_signup_confirm');
//  9、pdp页面Out Of Stock时，signUp 提交cancel按钮事件
    smart.time_event('pdp_outofstock_signup_cancel');


