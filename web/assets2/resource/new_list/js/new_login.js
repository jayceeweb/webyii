(function(){
    /* 原登录页逻辑 start  */
    var cache_storage_pg=JSON.parse(localStorage['mage-cache-storage-pg']||'null');
    if(cache_storage_pg){
        if(cache_storage_pg['guest-profile']){
            var localPorfile = cache_storage_pg['guest-profile']
            /**判断存储为json对象的时候*/
            if(typeof localPorfile != "object"){
                localPorfile = JSON.parse(localPorfile);
            }
            // console.log(document.querySelector(".Yourself_Text"));
            document.querySelector(".YOURSELF_text").innerText=localPorfile.nickname;
            document.querySelector(".Yourself_s1").innerText=localPorfile.nickname;
        }
    }
    var setCookie=function(cname,cvalue,time,path){
        var d = new Date();
        d.setTime(d.getTime()+time);
        var expires = "expires="+d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires+"; path="+path;
    };
    var userLogin={
        sendData:function(e){
            if(!cache_storage_pg)return;
            var cache_cookie_pg={};
            for(var key in cache_storage_pg){
                if(key=='guest-profile')
                    cache_cookie_pg['profile']=cache_storage_pg[key];
                else if(key=='guest-prescription')
                    cache_cookie_pg['prescription']=cache_storage_pg[key];
                else if(key!='global_closed_message')
                    cache_cookie_pg[key]=cache_storage_pg[key];
            }
            //add by niu 2019-9-16 修改cookie过期时间由1秒改为10分钟
            setCookie('mage-cache-cookie-pg',JSON.stringify(cache_cookie_pg),1000*600,'/');
        },
    };
    document.querySelectorAll('.pslogin-button-auto').forEach(function(item){
        item.addEventListener('click', function () {
            //Login page click the third party plug-in button triggers the event to write the enterprise student number that is not logged in andlastName Start
            // console.log("第三方点击");
            var orgId = window.localStorage.getItem("org_id");
            var id_orgCustomerId = window.localStorage.getItem("org_customer_id");
            if(orgId && id_orgCustomerId){
                setCookie("org_id",orgId,1000*600,'/');
                setCookie("org_customer_id",id_orgCustomerId,1000*600,'/');
            }
            //Login page click the third party plug-in button triggers the event to write the enterprise student number that is not logged in andlastName End
            //amazon,facebook,twitter第三方登录按钮点击事件(/customer/account/login/页面) by niu 2019-9-16
            userLogin.sendData();
        });
    });
    /* 原登录页逻辑 end  */


    // 登录Ajax逻辑
    $("body").on("click", "#loginLJ", function(e) {
        var email = $("#email").val();
        var pass = $("#pass").val();
        var persistent_remember_me = $("input[name='persistent_remember_me']").val();
        var form_key = $("input[name='form_key']").val();

        // 账号密码不能为空
        if(email.length==0 || pass.length==0) {
            if($(".warningDesc").hasClass('hiddenLJ')) {
                $(".warningDesc").removeClass("hiddenLJ");
            }
        }else{
            // 验证邮箱
            var reg = /^[0-9a-zA-Z_.-]+[@][0-9a-zA-Z_.-]+([.][a-zA-Z]+){1,2}$/;
            if (!reg.test(email)) {
                if($(".warningDesc").hasClass('hiddenLJ')) {
                    $(".warningDesc").removeClass("hiddenLJ");
                }
            }else{
                // 原登录页逻辑 start
                userLogin.sendData();
                delete localStorage.loginUserPwd;   //delete localstory password

                var orgId = window.localStorage.getItem("org_id");
                var id_orgCustomerId = window.localStorage.getItem("org_customer_id");
                if(orgId && id_orgCustomerId){
                    setCookie("org_id",orgId,1000*600,'/');
                    setCookie("org_customer_id",id_orgCustomerId,1000*600,'/');
                }
                // 原登录页逻辑 end

                var data = {
                    "login[username]": email,
                    "login[password]": pass,
                    "form_key" : form_key,
                    "persistent_remember_me": persistent_remember_me
                };
                $.ajax({
                    url: "/customer/account/loginPost",
                    type: 'post',
                    data: data,
                    beforeSend:function(){
                        // alert('远程调用开始...');
                        $("#loading").css("display","block");
                    },
                    success:function(res,textStatus){
                        $(".warningDesc").addClass("hiddenLJ");
                        // alert('开始回调，状态文本值：'+textStatus+' 返回数据：'+res);
                        var resObj = JSON.parse(res);
                        if(resObj.msg == "success") {
                            $(".checkOutLJ").addClass("hiddenLJ");
                            window.location.href = resObj.redirectUrl;
                        }else{
                            if($(".checkOutLJ").hasClass('hiddenLJ')) {
                                $(".checkOutLJ").removeClass("hiddenLJ");
                            }
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){
                        // alert('远程调用成功，状态文本值：'+textStatus);
                        $("#loading").css("display","none");
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                        console.log('error...状态文本值：'+textStatus+" 异常信息："+errorThrown);
                    }
                });
            }
        }
    });

    // 关闭错误提示消息.descClose
    $("body").on("click", ".descClose", function() {
        $(this).parent().addClass("hiddenLJ");
    });



    //  亚马逊登录 PC
    var authRequest;
    OffAmazonPayments.Button("AmazonLoginButton", "A32U22DDJM8453", {
        type: "LwA",
        color: "Gold",
        authorization: function () {
            loginOptions = { scope: "profile postal_code payments:widget payments:shipping_address", popup: true };
            // userLogin.sendData();
            authRequest = amazon.Login.authorize(loginOptions, "/amazon/login/authorize");
        },
        onError: function (error) {
            // something bad happened
            console.log(error,"------------------------error");
        }
    });

    //  亚马逊登录 Mobile
    OffAmazonPayments.Button("AmazonLoginButton2", "A32U22DDJM8453", {
        type: "LwA",
        color: "Gold",
        authorization: function () {
            loginOptions = { scope: "profile postal_code payments:widget payments:shipping_address", popup: true };
            // userLogin.sendData();
            authRequest = amazon.Login.authorize(loginOptions, "/amazon/login/authorize");
        },
        onError: function (error) {
            // something bad happened
            console.log(error,"------------------------error");
        }
    });
})();
