/*   test 示例
localStorage.setItem("lastname", "Smith");  // 存储
console.log(localStorage.getItem("lastname"));  // 检索
*/
$(function() {
    /*  list展示足迹数据  start   */
    var historyData = localStorage.getItem("historyData");
    var historyDataArr = JSON.parse(historyData);
    if(historyDataArr){
        if(historyDataArr.length>0) {
            // ljGlassesSave下data为entityID值
            var historyHtml = "";
            if(historyDataArr.length<=3) {
                for(var i=0; i<historyDataArr.length; i++) {    // 页面展示9条数据
                    historyHtml += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">';
                    historyHtml += '<div class="border_color_LJ">'
                    historyHtml += '<div class="ljOnlyForSavedData" name="'+historyDataArr[i].name+'" imgurl="'+historyDataArr[i].img+'" price="'+historyDataArr[i].price+'" linkurl="'+historyDataArr[i].link+'" style="display: none;"></div>';
                    historyHtml += '<div class="save_box_LJ ljGlassesSave" data="'+historyDataArr[i].entry_id+'" arr_simple="" skudata="'+historyDataArr[i].skudata+'">';
                    historyHtml += '<p>Saved</p>';
                    historyHtml += '<span class="plp-heart-empty"></span>';
                    historyHtml += '</div>';
                    historyHtml += '<a href="/'+historyDataArr[i].link+'">';
                    historyHtml += '<img class="ljLazyImage" src="'+historyDataArr[i].img+'" alt="'+historyDataArr[i].name+'" />';
                    historyHtml += '</a>';
                    historyHtml += '<p class="name_class_LJ">'+historyDataArr[i].name+'</p>';
                    historyHtml += '<p class="price_class_LJ">$'+historyDataArr[i].price+'</p>';
                    historyHtml += '<div class="save_Order_LJ">';
                    historyHtml += '<a href="/'+historyDataArr[i].link+'">Order</a>';
                    historyHtml += '</div>';
                    historyHtml += '</div>';
                    historyHtml += '</div>';
                }
            }else{
                for(var i=0; i<3; i++) {    // 页面展示3条数据 其他的看更多 more
                    historyHtml += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">';
                    historyHtml += '<div class="border_color_LJ">'
                    historyHtml += '<div class="ljOnlyForSavedData" name="'+historyDataArr[i].name+'" imgurl="'+historyDataArr[i].img+'" price="'+historyDataArr[i].price+'" linkurl="'+historyDataArr[i].link+'" style="display: none;"></div>';
                    historyHtml += '<div class="save_box_LJ ljGlassesSave" data="'+historyDataArr[i].entry_id+'" arr_simple="" skudata="'+historyDataArr[i].skudata+'">';
                    historyHtml += '<p>Saved</p>';
                    historyHtml += '<span class="plp-heart-empty"></span>';
                    historyHtml += '</div>';
                    historyHtml += '<a href="/'+historyDataArr[i].link+'">';
                    historyHtml += '<img class="ljLazyImage" src="'+historyDataArr[i].img+'" alt="'+historyDataArr[i].name+'" />';
                    historyHtml += '</a>';
                    historyHtml += '<p class="name_class_LJ">'+historyDataArr[i].name+'</p>';
                    historyHtml += '<p class="price_class_LJ">$'+historyDataArr[i].price+'</p>';
                    historyHtml += '<div class="save_Order_LJ">';
                    historyHtml += '<a href="/'+historyDataArr[i].link+'">Order</a>';
                    historyHtml += '</div>';
                    historyHtml += '</div>';
                    historyHtml += '</div>';
                }
            }

            $("#append_history").html(historyHtml);
        }
    }else{
        $("#recentlyDisplay_LJ").css("display","none");
    }
    /*  list展示足迹数据  end   */
});




    /****  匿名收藏以及登录收藏处理 start   *****/
        /* 是否已登錄 調取ajax查看一下   */
        var is_login = 0;
        save_Ajax("/e/customer/Exchange/CustomerInfo","GET",{},function(res){
            // console.log(res);
            if(res.customer.islogged){
                is_login = res.customer.islogged;
                /*  已登录用户  SavedFrames跳转地址 */
                $("#SavedFrames").attr("href","/wishlist/index/index/");
                $("#SavedFrames_mobel").attr("href","/wishlist/index/index/");
            }else{
                /*   匿名收藏  SavedFrames跳转地址 */
                $("#SavedFrames").attr("href","/anonymouswishlist");
                $("#SavedFrames_mobel").attr("href","/anonymouswishlist");
            }
        });


        /*  .ljGlassesSave 遍历得到simple_id 和storage中的数据对比 相等就是收藏的  start */
        var ljGlassesSaveObj = $(".ljGlassesSave");
        for(var ljk in ljGlassesSaveObj) {
            var current_Id = $(".ljGlassesSave").eq(ljk).attr('data');
            nm_save_fn(current_Id,$(".ljGlassesSave").eq(ljk));
        }
        /*  .ljGlassesSave 遍历得到simple_id 和storage中的数据对比 相等就是收藏的  end */

        /*未登录时 加载favorite  start  */
        function nm_save_fn(current_Id,nmObj){
            var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
            // console.log(favorite_product_unknownCustemersJsonString);
            /* 匿名收藏 start   */
            var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
            $.each(favorite_product_unknownCustemers,function(n,v){
                if(current_Id === v.id){
                    // 添加收藏
                    nmObj.find(".plp-heart-empty").addClass("saveAdded");
                    return false;
                }else{
                    // 取消收藏
                    nmObj.find(".plp-heart-empty").removeClass("saveAdded");
                }
            });
            /* 匿名收藏 end   */

            // yourself中saved Frames链接要到什么地方处理
            var save_length = favorite_product_unknownCustemers.length;
            // var is_login  = loginCustomer.customer.islogged;
            if (is_login==0) {
                if(save_length>=1){
                    $(".SavedFrames").text("Saved Frames("+save_length+")");
                }else{
                    $(".SavedFrames").text("Saved Frames");
                }
                $(".SavedFrames").attr("href","/anonymouswishlist");
            }
        }
        /*未登录时 加载favorite  end  */


        /*匿名收藏 Start
         *
         *  在未登陆时，将收藏的产品保存到本地缓存中
         *
         *
         * cookie name is pg-unlogin-wishlist
         * */
        // setUnknownCustomerFavorite:function(favoriteId,_this){
        function setUnknownCustomerFavorite(favoriteInfo,favoriteId,savedObj){
                var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
                var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
                var index_existFavoriteId = -1;
                for(var i =0;i<favorite_product_unknownCustemers.length;i++){
                    if(favorite_product_unknownCustemers[i].id===favoriteId){
                        index_existFavoriteId = i;
                    }
                }

                if( index_existFavoriteId>=0){
                    //取消收藏
                    favorite_product_unknownCustemers.splice(index_existFavoriteId,1);
                    savedObj.find(".plp-heart-empty").removeClass("saveAdded");
                }
                else{
                    favorite_product_unknownCustemers.push(favoriteInfo);
                    savedObj.find(".plp-heart-empty").addClass("saveAdded");
                }

                var productids =[];
                $.each(favorite_product_unknownCustemers,function(n,v){
                    productids.push(v.id);
                });
                var productidsString =JSON.stringify(productids);
                // $.cookie('pg-unlogin-wishlist',productidsString,{expires:3650,path:'/'});
                document.cookie="pg-unlogin-wishlist="+productidsString+"; expires=3650; path=/";
                favorite_product_unknownCustemersJsonString = JSON.stringify(favorite_product_unknownCustemers);
                window.localStorage.setItem('favorite_product_unknownCustemers',favorite_product_unknownCustemersJsonString);
                var favorite_product_unknownCustemersJsonString = window.localStorage.getItem('favorite_product_unknownCustemers')||'[]';
                var favorite_product_unknownCustemers = JSON.parse(favorite_product_unknownCustemersJsonString);
                var save_length = favorite_product_unknownCustemers.length;
                // var is_login  = loginCustomer.customer.islogged;
                if (is_login==0) {
                    if(save_length>=1){
                        $(".SavedFrames").text("Saved Frames("+save_length+")");
                    }else{
                        $(".SavedFrames").text("Saved Frames");
                    }
                }

            };
        /* 匿名收藏 End   */


        function save_Ajax(url,type,data,fn){
            $.ajax({
                url:url,
                type:type,
                data:data,
                success:fn
            })
        }

        /* 登录状态先查找wishlist数据 加载favorite  start  */
        save_Ajax("/catalogextend/wishlist/loadwishlist","POST",{},function(res){
            // console.log(res);   // {...},{...},{...}
            // console.log(Object.keys(res[0]));   // ['"510100C01B"']

            // 判断是否有数据
            if(res.length > 0) {
                /*  得到本页面所有save按钮对象 遍历比对sku的值来判断是否是收藏数据 skudata  */
                var ljGlassesSaveObjLoadwishlist = $(".ljGlassesSave");
                for(var ljk3 in ljGlassesSaveObjLoadwishlist) {
                    var ljSku = $(".ljGlassesSave").eq(ljk3).attr('skudata');
                    for(var wishListKey in res) {
                        /*  存在>0 收藏显示红色，将arr_simple数据写入 不存在不做处理。 */
                        if(jQuery.inArray(ljSku,Object.keys(res[wishListKey])) > -1) {
                            $(".ljGlassesSave").eq(ljk3).find(".plp-heart-empty").addClass("saveAdded");
                            $(".ljGlassesSave").eq(ljk3).attr("arr_simple",JSON.stringify(res[wishListKey]));
                        }
                    }
                }
            }
            /* 没有数据 显示匿名收藏的产品   */
            else{
                var ljGlassesSaveObj = $(".ljGlassesSave");
                for(var ljk2 in ljGlassesSaveObj) {
                    var current_Id = $(".ljGlassesSave").eq(ljk2).attr('data');
                    nm_save_fn(current_Id,$(".ljGlassesSave").eq(ljk2));
                }
            }
        });
        /* 登录状态先查找wishlist数据 加载favorite   end   */


        $("body").on("click",".ljGlassesSave",function() {
            // var is_login = loginCustomer.customer.islogged;
            //已登录状态
            var $ljThis = $(this);
            if(is_login!=0){
                var currentid = $(this).attr("data");
                var ljSku2 = $(this).attr("skudata");
                var arr_simple  = $(this).attr("arr_simple");

                /* 如果有值说明要删除该收藏产品  */
                if(arr_simple !== ""){
                    if(typeof(arr_simple)=="string"){
                        arr_simple  = JSON.parse(arr_simple);
                    }
                    // console.log(arr_simple);

                    var current_item = arr_simple;
                    var obj = {
                        item:current_item['wishlist_item_id'],
                        uenc:current_item['sharing_code']
                    };
                    save_Ajax("/wishlist/index/remove/?ajax=true","POST",obj,function(res){
                        // console.log("ajax返回值："+res.success);
                        if(res.success){
                            var save_count = res.profile.count;
                            if(save_count==0){
                                $(".SavedFrames").text("Saved Frames");
                            }else{
                                $(".SavedFrames").text("Saved Frames("+save_count+")");
                            }

                            arr_simple = null;
                            $ljThis.attr("arr_simple","");
                            $ljThis.find(".plp-heart-empty").removeClass("saveAdded");
                        }
                    });

                }
                else{
                    save_Ajax("/wishlist/index/add/?ajax=true","POST",{product:currentid},function(res){
                        // console.log(JSON.parse(res));
                        if(res){
                            res = JSON.parse(res);
                            var save_count = res.data.count;
                            if(save_count==0){
                                $(".SavedFrames").text("Saved Frames");
                            }else{
                                $(".SavedFrames").text("Saved Frames("+save_count+")");
                            }
                        }

                        var obj = {
                            product_id:currentid,
                            wishlist_item_id:res.data.item,
                            sharing_code:res.data.uenc
                        };

                        $ljThis.attr("arr_simple",JSON.stringify(obj));
                    });

                    $ljThis.find(".plp-heart-empty").addClass("saveAdded");
                }


            }
            //未登录状态
            else{
                //匿名收藏 Start
                var productInfoObject  = {};
                productInfoObject  = {
                    name:$(this).parent().find(".ljOnlyForSavedData").attr("name"),
                    img:$(this).parent().find(".ljOnlyForSavedData").attr("imgUrl"),
                    price:$(this).parent().find(".ljOnlyForSavedData").attr("price"),
                    link:$(this).parent().find(".ljOnlyForSavedData").attr("linkUrl"),
                    id:$(this).attr("data")
                };

                setUnknownCustomerFavorite(productInfoObject,productInfoObject.id,$ljThis);
                //匿名收藏 End
            }
        });
    /****  匿名收藏以及登录收藏处理 end   *****/


    /*****  PC端红心hover事件  *****/
    // $("body").on("mouseenter",".ljGlassesSave",function () {
    //     $(this).find('.ljSave').css("color","#FFF");
    // });
    // $("body").on("mouseleave",".ljGlassesSave",function () {
    //     $(this).find('.ljSave').css("color","#f90");
    // });
    /*****  PC端红心hover事件  *****/




/* 轮播效果以及pjax实现 start */
    /*  进入页面就执行的swiper轮播实现 start  */
    /*
        function init_swiper() {
            var swiperContainerLength = $(".swiper-container").length;
            for(var p=1;p<swiperContainerLength+1;p++) {
                var lj_swiper_num = '.lj_swiper'+p;
                var ljSelectLi_num = '.ljSelectLi'+p;
                var swiper = new Swiper(lj_swiper_num, {
                    pagination: {
                        el: ljSelectLi_num,
                        clickable: true,
                        renderBullet: function (index, className) {

                            var temp = $(lj_swiper_num).find(".ljSelectColorList").eq(index).html();
                            return '<span class="' + className + '">' + temp + '</span>';
                        },
                    },
                });
            }
        }
        init_swiper();
     */
    /*  进入页面就执行的swiper轮播实现 end  */


    /* 判断浏览器前进后退进来的吗？ 都要执行swiper  start */
    /*    if (window.history && window.history.pushState) {
            //监听浏览器前进后退事件
            $(window).on('popstate', function () {
                init_swiper();
                start();
            });
        }
    */
    /* 判断浏览器前进后退进来的吗？ 都要执行swiper  end */
/* 轮播效果以及pjax实现 end */




