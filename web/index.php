<?php
if (!function_exists("msectime")) {
    function msectime() {
        list($msec, $sec) = explode(' ', microtime());
        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }
}
// comment out the following two lines when deployed to production
//defined('YII_DEBUG') or define('YII_DEBUG', true);
//defined('YII_ENV') or define('YII_ENV', 'dev');
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

$t1=msectime();
$execDone = true;
function my_shutdown_function()
{
    global $execDone;
    global $t1;

    $end=msectime();
    //记录系统执行信息
    include_once Yii::$app->params["magento_root_path"]."/lib/internal/Pg/Monitor/LogTrace.php";
    $logTrace=new \Pg\Monitor\LogTrace();
    $log=$logTrace->getLogTrace();
    $log["life_time"]=$end - $t1;
    $logTrace->writeTraceLog($log);
}
register_shutdown_function('my_shutdown_function');

(new yii\web\Application($config))->run();
