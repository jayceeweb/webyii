
layui.use(['layer','layedit'], function(){
    var layedit = layui.layedit;
    //获取当前页数函数：
    function current_page_fn(){
        var Current_page = window.localStorage.getItem("Current_page");
        if(Current_page){
            Current_page = parseInt(Current_page);
            return Current_page;
        }else{
            window.localStorage.setItem("Current_page",1);
            return 1;
        }
    }
    function color16(){//十六进制颜色随机
        var r = Math.floor(Math.random()*256);
        var g = Math.floor(Math.random()*256);
        var b = Math.floor(Math.random()*256);
        var color = '#'+r.toString(16)+g.toString(16)+b.toString(16);
        return color;
    }

    //当先筛选值获取函数:
    function current_filter_fn(){
        var filter_email = $(".filter_email").val();
        var filter_profile_name = $(".filter_profile_name").val();
        var filter_status = $('#filter_status').val();
        var filter_date = $('#filter_date option:selected').val();
        var help_pd = $('#Pd_status option:selected').val();
        var Archive_Search = $('#Archive_Search').val();
        var help_group = $("#myCheck").is(":checked");
        var sort = $(".sort_jl").attr("state");
        var orderby = $(".sort_jl").attr("state_val");
        if(help_group){
            help_group = 1;
        }else{
            help_group = 0;
        }
        var filter_obj = {
            filter_email:filter_email,
            filter_profile_name:filter_profile_name,
            filter_status:filter_status,
            filter_date:filter_date,
            help_pd:help_pd,
            Archive_Search:Archive_Search,
            help_group:help_group,
            orderby:orderby,
            sort:sort
        };
        return filter_obj;
    }

    //判断是否为json字符串函数:
    function exhibits(str){
        if (typeof   str == 'string') {
            try {
                var obj=JSON.parse(str);
                return true;
            } catch(e) {
                return false;
            }
        }
    }



    $("select[name='Archive_Search']").multipleSelect({
        placeholder: "Please select",
        selectAllText:'Select all',
        allSelected:'Select all',
        width:"200px", //宽度
        DefSelectMode:0,// 1 默认第一项  2 没有默认项

    });


    $("select[name='filter_status']").multipleSelect({
        placeholder: "Please select",
        selectAllText:'Select all',
        allSelected:'Select all',
        width:"200px", //宽度
        selectedList:2,// 1 默认第一项  2 没有默认项
    });
    $("select[name='Archive_Search']").multipleSelect('setSelects', [0]);

    //初始化获取所有求助客户列表函数：
    function init_rx_ajax(page,filter_obj){
        var Archive_Search = filter_obj['Archive_Search'];
       if(Archive_Search instanceof Array){
           Archive_Search = Archive_Search.toString();
       }
        var filter_status = filter_obj['filter_status'];
        if(filter_status instanceof Array){
            filter_status = filter_status.toString();
        }
        var data = {
            page: page,
            help_email: filter_obj['filter_email'],
            help_status:filter_status,
            help_name:filter_obj['filter_profile_name'],
            help_start_time:filter_obj['filter_date'],
            help_pd:filter_obj['help_pd'],
            pigeonhole:Archive_Search,
            help_group:filter_obj['help_group'],
            orderby:filter_obj['orderby'],
            sort:filter_obj['sort']
        };
        $.ajax({
            method: 'POST',
            url:"/webyiiapi/agent/gethelpdata.html",
            data:JSON.stringify(data),
            success: function (res) {
                layer.closeAll('loading');
                var help_data = res["data"];
                if(help_data.length==0){
                    if(data['help_email']=="" && data['help_status']=="" && data['help_name']=="" && data['help_start_time']==""){
                        layer.alert('The current help user list is empty', {
                            title:"Help user list tips"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }else{
                        layer.alert('Your current filter is empty, please filter again', {
                            title:"Mailbox filtering tips"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }
                }

                var tr_html_help = "";
                var td_html_help = "";
                var viewRx = {};
                var count_page = res["count_page"];
                help_data.forEach(function(val,idx){
                    viewRx["rsph"] = val["rsph"];
                    viewRx["lsph"] = val["lsph"];
                    viewRx["rcyl"] = val["rcyl"];
                    viewRx["lcyl"] = val["lcyl"];
                    viewRx["rax"] = val["rax"];
                    viewRx["lax"] = val["lax"];
                    viewRx["radd"] = val["radd"];
                    viewRx["ladd"] = val["ladd"];
                    viewRx["pd"] = val["pd"];
                    viewRx["lpd"] = val["lpd"];
                    viewRx["rpd"] = val["rpd"];
                    viewRx["rpri"] = val["rpri"];
                    viewRx["lpri"] = val["lpri"];
                    viewRx["rbase"] = val["rbase"];
                    viewRx["lbase"] = val["lbase"];
                    viewRx["rpri_1"] = val["rpri_1"];
                    viewRx["lpri_1"] = val["lpri_1"];
                    viewRx["rbase_1"] = val["rbase_1"];
                    viewRx["lbase_1"] = val["lbase_1"];
                    viewRx["single_pd"] = val["single_pd"];
                    viewRx["prescription_img"] = val["prescription_img"];
                    viewRx["prescription_img"] = val["prescription_img"];
                    viewRx["exam_date"] = val["exam_date"];
                    viewRx["expire_date"] = val["expire_date"];
                    var new_prescription_name = val["prescription_name"];
                    var new_prescription_img = val["prescription_img"];
                    var new_ahc_prescription_img = val["ahc_prescription_img"];
                    if(new_prescription_name){
                        new_prescription_name = new_prescription_name.replace(/"/g,"'");
                        new_prescription_name = new_prescription_name.replace(/ /g,"\n");
                    }else{
                        new_prescription_name = "";
                    }
                    if(new_prescription_img){
                        new_prescription_img = new_prescription_img.replace(/ /g,"%20");
                    }else{
                        new_prescription_img = "";
                    }

                    if(new_ahc_prescription_img){
                        new_ahc_prescription_img = new_ahc_prescription_img.replace(/ /g,"%20");
                    }else{
                        new_ahc_prescription_img = "";
                    }
                    viewRx["prescription_name"] = new_prescription_name;
                    viewRx["prescription_img"] = new_prescription_img;
                    viewRx["ahc_prescription_img"] = new_ahc_prescription_img;

                    var ProfileName = val["help_name"];
                    var Email = val["email"];
                    var sku = val["product_sku"];
                    var Remarks = val["description"];
                    var RequestedDate = val["ahc_created_at"];
                    var CompletedDate = val["ape_created_at"];
                    var prescription_img = val["prescription_img"];
                    var ahc_prescription_img = val["ahc_prescription_img"];
                    var entity_id = val['entity_id'];
                    var help_cart_id = val['id'];
                    if(!CompletedDate){
                        CompletedDate="unfinished"
                    }else{
                        var CompletedDate = CompletedDate.split("-");
                        if(CompletedDate[2]){
                            var new_CompletedDate = CompletedDate[2].split(" ");
                            new_CompletedDate = new_CompletedDate[0];
                        }
                        var year = CompletedDate[0];
                        CompletedDate = CompletedDate[1]+"/"+new_CompletedDate+"/"+year;
                    }
                    var Status = val["prescription_status"];
                    var Status_text="";
                    if(!Status){
                        Status_text = "new"
                    }else if(Status==1){
                        Status_text = "rx added"
                    }else if(Status==2){
                        Status_text = "verified"
                    }else if(Status==3){
                        Status_text = "cust notified"
                    }
                    var a_help_html = "";
                    if(Status<1){
                        a_help_html="<a class='layui-btn layui-btn-sm layui-btn-xs-add' lay-event='detail' addRx_email="+Email+" help_cart_id_add="+help_cart_id+" addRx="+JSON.stringify(viewRx)+">add rx</a>";
                    }
                    if(Status>=1 && Status<=3){
                        a_help_html="<a class='layui-btn layui-btn-sm layui-btn-xs-rx' lay-event='edit' viewRx="+JSON.stringify(viewRx)+" viewRx_email="+Email+" help_cart_id="+help_cart_id+">confirm rx</a><a class='layui-btn layui-btn-normal layui-btn-sm layui-btn-xs-editRx' editRx="+JSON.stringify(viewRx)+" editRx_email="+Email+" entity_id="+entity_id+">edit rx</a>";
                    }
                    if(Status<=3 && Status>=2){
                        a_help_html="<a class='layui-btn layui-btn-sm layui-btn-xs-rx' lay-event='edit' viewRx="+JSON.stringify(viewRx)+" viewRx_email="+Email+" help_cart_id="+help_cart_id+">confirm rx</a><a class='layui-btn layui-btn-normal layui-btn-sm layui-btn-xs-editRx' editRx="+JSON.stringify(viewRx)+" editRx_email="+Email+" entity_id="+entity_id+">edit rx</a><a class='layui-btn layui-btn-warm layui-btn-sm btn_send_email' help_cart_id_sendEmail="+help_cart_id+" sendEmail_val="+Email+">send email</a>";
                    }
                    ProfileName = ProfileName.replace(/"/g, "'");
                    Remarks = Remarks.replace(/"/g,'');
                    Remarks = Remarks.replace(/;/g,'');
                    var viewUser_obj={
                        prescription_img:prescription_img,
                        ahc_prescription_img:ahc_prescription_img,
                        profileName:ProfileName,
                        email:Email,
                        helpsku:sku,
                        remarks:Remarks
                    };
                    if(RequestedDate){
                        var RequestedDate = RequestedDate.split("-");
                        if(RequestedDate[2]){
                            var new_CompletedDate = RequestedDate[2].split(" ");
                            new_CompletedDate = new_CompletedDate[0];
                        }
                        var year = RequestedDate[0];
                        RequestedDate = RequestedDate[1]+"/"+new_CompletedDate+"/"+year;
                    }

                    var pd_states = "";
                    if(val["help_pd"]=="1"){
                        pd_states = "Yes"
                    }else if(val["help_pd"]=="2"){
                        pd_states = "No"
                    }else if(val["help_pd"]=="3"){
                        pd_states = "Never"
                    }

                    var anon_regd = "anon";
                    if(val['is_login']=="1"){
                        anon_regd = "regd";
                    }
                    var more_val = "";
                    if(val['data_count']>1){
                        more_val = "<a class='layui-btn layui-btn-danger layui-btn-sm More_click' email="+Email+" help_cart_id="+help_cart_id+" more='false'><span class='child_text'>view the same user</span><span class='layui-badge num_user'>"+val['data_count']+"</span></a>";
                    }

                    var acknowledging_time = val['acknowledging_time'];
                    var send_created_time = val['send_created_time'];
                    if(acknowledging_time){
                        var acknowledging_time = acknowledging_time.split("-");
                        if(acknowledging_time[2]){
                            var new_acknowledging_time = acknowledging_time[2].split(" ");
                            new_acknowledging_time = new_acknowledging_time[0];
                        }
                        var year = acknowledging_time[0];
                        if(!parseInt(year)){
                            acknowledging_time = "unfinished"
                        }else{
                            acknowledging_time = acknowledging_time[1]+"/"+new_acknowledging_time+"/"+year;
                        }

                    }else{
                        acknowledging_time = "unfinished"
                    }

                    if(send_created_time){
                        var send_created_time = send_created_time.split("-");
                        if(acknowledging_time[2]){
                            var new_send_created_time = send_created_time[2].split(" ");
                            new_send_created_time = new_send_created_time[0];
                        }
                        var year = send_created_time[0];
                        if(!parseInt(year)){
                            send_created_time = "unfinished"
                        }else{
                            send_created_time = send_created_time[1]+"/"+new_send_created_time+"/"+year;
                        }
                    }else{
                        send_created_time = "unfinished"
                    }

                    function getDistanceSpecifiedTime(dateTime) {
                        // 指定日期和时间
                        var EndTime = new Date(dateTime);
                        // 当前系统时间
                        var NowTime = new Date();
                        var t = EndTime.getTime() - NowTime.getTime();
                        var d = Math.floor(t / 1000 / 60 / 60 / 24);
                        var h = Math.floor(t / 1000 / 60 / 60 % 24);
                        var m = Math.floor(t / 1000 / 60 % 60);
                        var s = Math.floor(t / 1000 % 60);
                        return d;
                    }
                    var jl_ts = getDistanceSpecifiedTime(RequestedDate);
                    jl_ts=Math.abs(jl_ts);
                    var gd_a_click = "";
                    //归档
                    var pigeonhole = val['pigeonhole'];
                    if(pigeonhole=="0"){
                        if(jl_ts>=7){
                            gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Archive</a>"
                        }else{
                            gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm layui-btn-disabled'>Archive</a>"
                        }

                    } else if(pigeonhole=="1"){
                        gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Lack of PD</a>"
                    } else if(pigeonhole=="2"){
                        gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Lack of prescription photos</a>"
                    } else if(pigeonhole=="3"){
                        gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Email sent</a>";
                    }
                    td_html_help = "<td>"+gd_a_click+"</td><td width='200px'>"+anon_regd+": "+ProfileName+"</td><td><a class='email_more'>"+Email+"</a></td><td>"+sku+"</td><td>"+pd_states+"</td><td>"+RequestedDate+"</td><td>"+CompletedDate+"</td><td>"+acknowledging_time+"</td><td>"+send_created_time+"</td><td>"+Status_text+"</td><td><a class='layui-btn layui-btn-primary layui-btn-sm layui-btn-xs-view' lay-event='detail' viewUser_obj='"+JSON.stringify(viewUser_obj)+"'>view</a>"+a_help_html+more_val+"</td>"
                    tr_html_help+="<tr>"+td_html_help+"</tr>";
                });
                $("#help_html").html(tr_html_help);

                var current_tr_idx = window.localStorage.getItem("current_tr_idx");

                setTimeout(function(){
                    if(current_tr_idx){
                        current_tr_idx = parseInt(current_tr_idx);
                        $("#help_html").find("tr").eq(current_tr_idx).css("backgroundColor","#f1f16b44").siblings("tr").css("backgroundColor","#fff");
                    }
                    if(!count_page){
                        $(".parents_ys").hide();
                    }else{
                        $(".parents_ys").show();
                        $(".Pages_Current").text(page);
                        $(".PageCount").text(count_page);
                        $(".Current_number").text(help_data.length);
                    }

                    if(count_page>1){
                        //翻页
                        $("#kkpager").show();
                        kkpager.generPageHtml({
                            //当前页数
                            pno: page,
                            //总页数
                            total: count_page,
                            mode: 'click',//默认值是link，可选link或者click
                            click: function (n) {
                                this.selectPage(n);
                                return false;
                            }
                        },true);
                    }else{
                        //翻页
                        $("#kkpager").hide();
                    }
                },200)
            }
        })
    }

    //为客户添加处方/查看客服处方函数
    function view_add_help(type,new_this_viewUser_obj){
        var this_viewUser_obj = {};
        var add_help_img="";
        if(type=="edit"){
            this_viewUser_obj = new_this_viewUser_obj;
            if(this_viewUser_obj['prescription_img']){
                this_viewUser_obj['prescription_img'] = this_viewUser_obj['prescription_img'].replace(/ /g,"%20");

                if( this_viewUser_obj['prescription_img'].indexOf("payneglasses")==-1){
                    try{
                        this_viewUser_obj['prescription_img']= staticUrl+"/media/customer/prescriptions" +  this_viewUser_obj['prescription_img']
                    }catch(e){
                        this_viewUser_obj['prescription_img'] = "/media/customer/prescriptions" +  this_viewUser_obj['prescription_img']
                    }
                }else{
                    this_viewUser_obj['prescription_img'] =  this_viewUser_obj['prescription_img']
                }

                add_help_img = '<img class="view_help_img"  src='+this_viewUser_obj['prescription_img']+' /><p class="tip_img"><i class="layui-icon layui-icon-tips"></i>Click on the picture to view the larger picture</p>';
            }else if(this_viewUser_obj['ahc_prescription_img']){
                this_viewUser_obj['ahc_prescription_img'] = this_viewUser_obj['ahc_prescription_img'].replace(/ /g,"%20");

                if( this_viewUser_obj['ahc_prescription_img'].indexOf("payneglasses")==-1){
                    try{
                        this_viewUser_obj['ahc_prescription_img']= staticUrl+"/media/customer/prescriptions" +  this_viewUser_obj['ahc_prescription_img']
                    }catch(e){
                        this_viewUser_obj['ahc_prescription_img'] = "/media/customer/prescriptions" +  this_viewUser_obj['ahc_prescription_img']
                    }
                }else{
                    this_viewUser_obj['ahc_prescription_img'] =  this_viewUser_obj['ahc_prescription_img']
                }

                add_help_img = '<img class="view_help_img"  src='+this_viewUser_obj['ahc_prescription_img']+' /><p class="tip_img"><i class="layui-icon layui-icon-tips"></i>Click on the picture to view the larger picture</p>';
            }else{
                add_help_img = "";
            }
        }else{
            add_help_img =  "<form id='uploadForm'>"+
                "<input type='file' accept='image/*' name='uploadFile' id='uploadFile' style='visibility:hidden;position:absolute;top:0px;width:0px'/>"+
                "</form>"+
                "<input type='button' id='btn' class='btn_add btn_add_file' value='Upload prescription image'/>"+
                "<img id='portrait' src='' title='click to enlarge' class='view_help_img' style='display:none;' /><p class='portrait_img tip_img' style='display:none;text-align:left;margin-left: 100px;margin-right: 40px;'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
        }

        var help_html = "<div class='help_ipt help_ipt_center'>"
            +add_help_img+
            "<form class='layui-form information_class' action=''>"+
            "<div class='layui-form-item'>"+
            "<label class='layui-form-label'>Profile Name</label>"+
            "<div class='layui-input-block'>"+
            "<input value='' type='text' name='title' lay-verify='title' autocomplete='off' class='layui-input help_profile_ipt'></div></div>"+
            "<div class='layui-form-item'>"+
            "<label class='layui-form-label'>Email<span class='Email_bt'>*</span></label>"+
            "<div class='layui-input-block'>"+
            "<input value='' type='text' name='title' lay-verify='title' autocomplete='off' class='layui-input help_email_ipt'></div></div>"+
            "<div class='layui-form-item'>"+
            "<label class='layui-form-label'>Help Sku</label>"+
            "<div class='layui-input-block'>"+
            "<input value='' type='text' name='title' lay-verify='title' autocomplete='off' class='layui-input help_sku_ipt'></div></div>"+
            "<div class='layui-form-item layui-form-text'>"+
            "<label class='layui-form-label'>Remarks</label>"+
            "<div class='layui-input-block'>"+
            "<textarea class='layui-textarea layui-textarea-help help_remarks_ipt' style='resize: none;height: 200px'></textarea></div></div></form></div>";

        if(type=="edit"){
            setTimeout(function(){
                $(".layui-textarea-help").val(this_viewUser_obj['remarks']?this_viewUser_obj['remarks']:"");
                $(".help_remarks_ipt,.help_profile_ipt,.help_email_ipt,.help_sku_ipt").attr("disabled",true);
                $(".help_profile_ipt").val(this_viewUser_obj['profileName']?this_viewUser_obj['profileName']:"");
                $(".help_email_ipt").val(this_viewUser_obj['email']?this_viewUser_obj['email']:"");
                $(".help_sku_ipt").val(this_viewUser_obj['helpsku']?this_viewUser_obj['helpsku']:"");
            },200)
        }else{
            setTimeout(function(){
                $(".help_remarks_ipt,.help_profile_ipt,.help_email_ipt,.help_sku_ipt").attr("disabled",false);
                $(".help_remarks_ipt,.help_profile_ipt,.help_email_ipt,.help_sku_ipt").val("");
            },200)
        }
        return help_html;
    }

    //获取select框中的选中值函数：
    function new_mr_options(ele,ele2,select_val){
        var opt = document.querySelectorAll(ele);
        for(var i=0; i<opt.length; i++){
            if(opt[i].value==select_val){
                $(ele2)[0].selectedIndex = i;
            }
        }
    };

    //add rx【新增眼光单】/ edit[编辑验光单] 展示 函数：
    function edit_add_rx(type,editrx_obj){
        var EditRx =
            "<div class='editRx_class'>"+
            "<div class='edit_add_img'></div>"+
            "<table class='layui-table' id='layerDemo-editrx'>"+
            "<colgroup>"+
            "<col width='150' />"+
            "<col width='200' />"+
            "<col />"+
            "</colgroup>"+
            "<thead>"+
            "<tr>"+
            "<th></th>"+
            "<th>SPH(Sphere)</th>"+
            "<th>CYL(Cylinder)</th>"+
            "<th>AXIS</th>"+
            "<th>ADD(NV-ADD)</th>"+
            "<th>"+
            "PD(Pupillary Distance)"+
            "<div class='pd_class pd_class_select'>"+
            "<p class='binocular_pd'>Binocular PD</p>"+
            "<p class='monocular_pd pd_class_active'>Monocular PD</p>"+
            "</div>"+
            "</th>"+
            "</tr>"+
            "</thead>"+
            "<tbody>"+
            "<tr>"+
            "<td>OD(Right Eye)</td>"+
            "<td>"+
            "<select name='rsph' class='padding-left5-ss padding-right10-ss prescription_val prescription_rsph'> <option value='-16.00'>-16.00</option> <option value='-15.75'>-15.75</option> <option value='-15.50'> -15.50 </option> <option value='-15.25'> -15.25 </option> <option value='-15.00'> -15.00 </option> <option value='-14.75'> -14.75 </option> <option value='-14.50'> -14.50 </option> <option value='-14.25'> -14.25 </option> <option value='-14.00'> -14.00 </option> <option value='-13.75'> -13.75 </option> <option value='-13.50'> -13.50 </option> <option value='-13.25'> -13.25 </option> <option value='-13.00'> -13.00 </option> <option value='-12.75'> -12.75 </option> <option value='-12.50'> -12.50 </option> <option value='-12.25'> -12.25 </option> <option value='-12.00'> -12.00 </option> <option value='-11.75'> -11.75 </option> <option value='-11.50'> -11.50 </option> <option value='-11.25'> -11.25 </option> <option value='-11.00'> -11.00 </option> <option value='-10.75'> -10.75 </option> <option value='-10.50'> -10.50 </option> <option value='-10.25'> -10.25 </option> <option value='-10.00'> -10.00 </option> <option value='-9.75'> -9.75 </option> <option value='-9.50'> -9.50 </option> <option value='-9.25'> -9.25 </option> <option value='-9.00'> -9.00 </option> <option value='-8.75'> -8.75 </option> <option value='-8.50'> -8.50 </option> <option value='-8.25'> -8.25 </option> <option value='-8.00'> -8.00 </option> <option value='-7.75'> -7.75 </option> <option value='-7.50'> -7.50 </option> <option value='-7.25'> -7.25 </option> <option value='-7.00'> -7.00 </option> <option value='-6.75'> -6.75 </option> <option value='-6.50'> -6.50 </option> <option value='-6.25'> -6.25 </option> <option value='-6.00'> -6.00 </option> <option value='-5.75'> -5.75 </option> <option value='-5.50'> -5.50 </option> <option value='-5.25'> -5.25 </option> <option value='-5.00'> -5.00 </option> <option value='-4.75'> -4.75 </option> <option value='-4.50'> -4.50 </option> <option value='-4.25'> -4.25 </option> <option value='-4.00'> -4.00 </option> <option value='-3.75'> -3.75 </option> <option value='-3.50'> -3.50 </option> <option value='-3.25'> -3.25 </option> <option value='-3.00'> -3.00 </option> <option value='-2.75'> -2.75 </option> <option value='-2.50'> -2.50 </option> <option value='-2.25'> -2.25 </option> <option value='-2.00'> -2.00 </option> <option value='-1.75'> -1.75 </option> <option value='-1.50'> -1.50 </option> <option value='-1.25'> -1.25 </option> <option value='-1.00'> -1.00 </option> <option value='-0.75'> -0.75 </option> <option value='-0.50'> -0.50 </option> <option value='-0.25'> -0.25 </option> <option value='0.00' selected='selected'> 0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> <option value='5.25'> +5.25 </option> <option value='5.50'> +5.50 </option> <option value='5.75'> +5.75 </option> <option value='6.00'> +6.00 </option> <option value='6.25'> +6.25 </option> <option value='6.50'> +6.50 </option> <option value='6.75'> +6.75 </option> <option value='7.00'> +7.00 </option> <option value='7.25'> +7.25 </option> <option value='7.50'> +7.50 </option> <option value='7.75'> +7.75 </option> <option value='8.00'> +8.00 </option> <option value='8.25'> +8.25 </option> <option value='8.50'> +8.50 </option> <option value='8.75'> +8.75 </option> <option value='9.00'> +9.00 </option> <option value='9.25'> +9.25 </option> <option value='9.50'> +9.50 </option> <option value='9.75'> +9.75 </option> <option value='10.00'> +10.00 </option> <option value='10.25'> +10.25 </option> <option value='10.50'> +10.50 </option> <option value='10.75'> +10.75 </option> <option value='11.00'> +11.00 </option> <option value='11.25'> +11.25 </option> <option value='11.50'> +11.50 </option> <option value='11.75'> +11.75 </option> <option value='12.00'> +12.00 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='rcyl' class='padding-left5-ss padding-right10-ss prescription_val prescription_rcyl'> <option value='-6.00'> -6.00 </option> <option value='-5.75'> -5.75 </option> <option value='-5.50'> -5.50 </option> <option value='-5.25'> -5.25 </option> <option value='-5.00'> -5.00 </option> <option value='-4.75'> -4.75 </option> <option value='-4.50'> -4.50 </option> <option value='-4.25'> -4.25 </option> <option value='-4.00'> -4.00 </option> <option value='-3.75'> -3.75 </option> <option value='-3.50'> -3.50 </option> <option value='-3.25'> -3.25 </option> <option value='-3.00'> -3.00 </option> <option value='-2.75'> -2.75 </option> <option value='-2.50'> -2.50 </option> <option value='-2.25'> -2.25 </option> <option value='-2.00'> -2.00 </option> <option value='-1.75'> -1.75 </option> <option value='-1.50'> -1.50 </option> <option value='-1.25'> -1.25 </option> <option value='-1.00'> -1.00 </option> <option value='-0.75'> -0.75 </option> <option value='-0.50'> -0.50 </option> <option value='-0.25'> -0.25 </option> <option value='0.00' selected='selected'> 0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> <option value='5.25'> +5.25 </option> <option value='5.50'> +5.50 </option> <option value='5.75'> +5.75 </option> <option value='6.00'> +6.00 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='rax' class='padding-left5-ss padding-right10-ss prescription_val prescription_rax'> <option value='000' selected='selected'>000</option> <option value='1'> 001 </option> <option value='2'> 002 </option> <option value='3'> 003 </option> <option value='4'> 004 </option> <option value='5'> 005 </option> <option value='6'> 006 </option> <option value='7'> 007 </option> <option value='8'> 008 </option> <option value='9'> 009 </option> <option value='10'> 010 </option> <option value='11'> 011 </option> <option value='12'> 012 </option> <option value='13'> 013 </option> <option value='14'> 014 </option> <option value='15'> 015 </option> <option value='16'> 016 </option> <option value='17'> 017 </option> <option value='18'> 018 </option> <option value='19'> 019 </option> <option value='20'> 020 </option> <option value='21'> 021 </option> <option value='22'> 022 </option> <option value='23'> 023 </option> <option value='24'> 024 </option> <option value='25'> 025 </option> <option value='26'> 026 </option> <option value='27'> 027 </option> <option value='28'> 028 </option> <option value='29'> 029 </option> <option value='30'> 030 </option> <option value='31'> 031 </option> <option value='32'> 032 </option> <option value='33'> 033 </option> <option value='34'> 034 </option> <option value='35'> 035 </option> <option value='36'> 036 </option> <option value='37'> 037 </option> <option value='38'> 038 </option> <option value='39'> 039 </option> <option value='40'> 040 </option> <option value='41'> 041 </option> <option value='42'> 042 </option> <option value='43'> 043 </option> <option value='44'> 044 </option> <option value='45'> 045 </option> <option value='46'> 046 </option> <option value='47'> 047 </option> <option value='48'> 048 </option> <option value='49'> 049 </option> <option value='50'> 050 </option> <option value='51'> 051 </option> <option value='52'> 052 </option> <option value='53'> 053 </option> <option value='54'> 054 </option> <option value='55'> 055 </option> <option value='56'> 056 </option> <option value='57'> 057 </option> <option value='58'> 058 </option> <option value='59'> 059 </option> <option value='60'> 060 </option> <option value='61'> 061 </option> <option value='62'> 062 </option> <option value='63'> 063 </option> <option value='64'> 064 </option> <option value='65'> 065 </option> <option value='66'> 066 </option> <option value='67'> 067 </option> <option value='68'> 068 </option> <option value='69'> 069 </option> <option value='70'> 070 </option> <option value='71'> 071 </option> <option value='72'> 072 </option> <option value='73'> 073 </option> <option value='74'> 074 </option> <option value='75'> 075 </option> <option value='76'> 076 </option> <option value='77'> 077 </option> <option value='78'> 078 </option> <option value='79'> 079 </option> <option value='80'> 080 </option> <option value='81'> 081 </option> <option value='82'> 082 </option> <option value='83'> 083 </option> <option value='84'> 084 </option> <option value='85'> 085 </option> <option value='86'> 086 </option> <option value='87'> 087 </option> <option value='88'> 088 </option> <option value='89'> 089 </option> <option value='90'> 090 </option> <option value='91'> 091 </option> <option value='92'> 092 </option> <option value='93'> 093 </option> <option value='94'> 094 </option> <option value='95'> 095 </option> <option value='96'> 096 </option> <option value='97'> 097 </option> <option value='98'> 098 </option> <option value='99'> 099 </option> <option value='100'> 100 </option> <option value='101'> 101 </option> <option value='102'> 102 </option> <option value='103'> 103 </option> <option value='104'> 104 </option> <option value='105'> 105 </option> <option value='106'> 106 </option> <option value='107'> 107 </option> <option value='108'> 108 </option> <option value='109'> 109 </option> <option value='110'> 110 </option> <option value='111'> 111 </option> <option value='112'> 112 </option> <option value='113'> 113 </option> <option value='114'> 114 </option> <option value='115'> 115 </option> <option value='116'> 116 </option> <option value='117'> 117 </option> <option value='118'> 118 </option> <option value='119'> 119 </option> <option value='120'> 120 </option> <option value='121'> 121 </option> <option value='122'> 122 </option> <option value='123'> 123 </option> <option value='124'> 124 </option> <option value='125'> 125 </option> <option value='126'> 126 </option> <option value='127'> 127 </option> <option value='128'> 128 </option> <option value='129'> 129 </option> <option value='130'> 130 </option> <option value='131'> 131 </option> <option value='132'> 132 </option> <option value='133'> 133 </option> <option value='134'> 134 </option> <option value='135'> 135 </option> <option value='136'> 136 </option> <option value='137'> 137 </option> <option value='138'> 138 </option> <option value='139'> 139 </option> <option value='140'> 140 </option> <option value='141'> 141 </option> <option value='142'> 142 </option> <option value='143'> 143 </option> <option value='144'> 144 </option> <option value='145'> 145 </option> <option value='146'> 146 </option> <option value='147'> 147 </option> <option value='148'> 148 </option> <option value='149'> 149 </option> <option value='150'> 150 </option> <option value='151'> 151 </option> <option value='152'> 152 </option> <option value='153'> 153 </option> <option value='154'> 154 </option> <option value='155'> 155 </option> <option value='156'> 156 </option> <option value='157'> 157 </option> <option value='158'> 158 </option> <option value='159'> 159 </option> <option value='160'> 160 </option> <option value='161'> 161 </option> <option value='162'> 162 </option> <option value='163'> 163 </option> <option value='164'> 164 </option> <option value='165'> 165 </option> <option value='166'> 166 </option> <option value='167'> 167 </option> <option value='168'> 168 </option> <option value='169'> 169 </option> <option value='170'> 170 </option> <option value='171'> 171 </option> <option value='172'> 172 </option> <option value='173'> 173 </option> <option value='174'> 174 </option> <option value='175'> 175 </option> <option value='176'> 176 </option> <option value='177'> 177 </option> <option value='178'> 178 </option> <option value='179'> 179 </option> <option value='180'> 180 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='radd' class='padding-left5-ss padding-right10-ss radd-temp-select radd_margin_top_lin prescription_val prescription_change_radd prescription_radd' style='margin-top: 0px;'> <option value='0.00' selected='selected'>0.00</option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> </select>"+
            "</td>"+
            "<td class='pd'>"+
            "<select class='col-5 margin-left15 padding-left5-ss padding-right10-ss prescription_val prescription_pd_change prescription_pd' name='pd'> <option selected='selected' value=''>Select</option> <option value='35.0'> 35.0 </option> <option value='35.5'> 35.5 </option> <option value='36.0'> 36.0 </option> <option value='36.5'> 36.5 </option> <option value='37.0'> 37.0 </option> <option value='37.5'> 37.5 </option> <option value='38.0'> 38.0 </option> <option value='38.5'> 38.5 </option> <option value='39.0'> 39.0 </option> <option value='39.5'> 39.5 </option> <option value='40.0'> 40.0 </option> <option value='40.5'> 40.5 </option> <option value='41.0'> 41.0 </option> <option value='41.5'> 41.5 </option> <option value='42.0'> 42.0 </option> <option value='42.5'> 42.5 </option> <option value='43.0'> 43.0 </option> <option value='43.5'> 43.5 </option> <option value='44.0'> 44.0 </option> <option value='44.5'> 44.5 </option> <option value='45.0'> 45.0 </option> <option value='45.5'> 45.5 </option> <option value='46.0'> 46.0 </option> <option value='46.5'> 46.5 </option> <option value='47.0'> 47.0 </option> <option value='47.5'> 47.5 </option> <option value='48.0'> 48.0 </option> <option value='48.5'> 48.5 </option> <option value='49.0'> 49.0 </option> <option value='49.5'> 49.5 </option> <option value='50.0'> 50.0 </option> <option value='50.5'> 50.5 </option> <option value='51.0'> 51.0 </option> <option value='51.5'> 51.5 </option> <option value='52.0'> 52.0 </option> <option value='52.5'> 52.5 </option> <option value='53.0'> 53.0 </option> <option value='53.5'> 53.5 </option> <option value='54.0'> 54.0 </option> <option value='54.5'> 54.5 </option> <option value='55.0'> 55.0 </option> <option value='55.5'> 55.5 </option> <option value='56.0'> 56.0 </option> <option value='56.5'> 56.5 </option> <option value='57.0'> 57.0 </option> <option value='57.5'> 57.5 </option> <option value='58.0'> 58.0 </option> <option value='58.5'> 58.5 </option> <option value='59.0'> 59.0 </option> <option value='59.5'> 59.5 </option> <option value='60.0'> 60.0 </option> <option value='60.5'> 60.5 </option> <option value='61.0'> 61.0 </option> <option value='61.5'> 61.5 </option> <option value='62.0'> 62.0 </option> <option value='62.5'> 62.5 </option> <option value='63.0'> 63.0 </option> <option value='63.5'> 63.5 </option> <option value='64.0'> 64.0 </option> <option value='64.5'> 64.5 </option> <option value='65.0'> 65.0 </option> <option value='65.5'> 65.5 </option> <option value='66.0'> 66.0 </option> <option value='66.5'> 66.5 </option> <option value='67.0'> 67.0 </option> <option value='67.5'> 67.5 </option> <option value='68.0'> 68.0 </option> <option value='68.5'> 68.5 </option> <option value='69.0'> 69.0 </option> <option value='69.5'> 69.5 </option> <option value='70.0'> 70.0 </option> <option value='70.5'> 70.5 </option> <option value='71.0'> 71.0 </option> <option value='71.5'> 71.5 </option> <option value='72.0'> 72.0 </option> <option value='72.5'> 72.5 </option> <option value='73.0'> 73.0 </option> <option value='73.5'> 73.5 </option> <option value='74.0'> 74.0 </option> <option value='74.5'> 74.5 </option> <option value='75.0'> 75.0 </option> <option value='75.5'> 75.5 </option> <option value='76.0'> 76.0 </option> <option value='76.5'> 76.5 </option> <option value='77.0'> 77.0 </option> <option value='77.5'> 77.5 </option> <option value='78.0'> 78.0 </option> <option value='78.5'> 78.5 </option> <option value='79.0'> 79.0 </option> </select>"+
            "</td>"+
            "<td class='lpd_rpd'>"+
            "<select name='rpd' class='pg-margin-top5 padding-left5-ss padding-right10-ss prescription_val prescription_rpd_change prescription_rpd'> <option selected='selected' value=''>Select</option><option value='17.5'> 17.5 </option> <option value='18.0'> 18.0 </option> <option value='18.5'> 18.5 </option> <option value='19.0'> 19.0 </option> <option value='19.5'> 19.5 </option> <option value='20.0'> 20.0 </option> <option value='20.5'> 20.5 </option> <option value='21.0'> 21.0 </option> <option value='21.5'> 21.5 </option> <option value='22.0'> 22.0 </option> <option value='22.5'> 22.5 </option> <option value='23.0'> 23.0 </option> <option value='23.5'> 23.5 </option> <option value='24.0'> 24.0 </option> <option value='24.5'> 24.5 </option> <option value='25.0'> 25.0 </option> <option value='25.5'> 25.5 </option> <option value='26.0'> 26.0 </option> <option value='26.5'> 26.5 </option> <option value='27.0'> 27.0 </option> <option value='27.5'> 27.5 </option> <option value='28.0'> 28.0 </option> <option value='28.5'> 28.5 </option> <option value='29.0'> 29.0 </option> <option value='29.5'> 29.5 </option> <option value='30.0'> 30.0 </option> <option value='30.5'> 30.5 </option> <option value='31.0'> 31.0 </option> <option value='31.5'> 31.5 </option> <option value='32.0'> 32.0 </option> <option value='32.5'> 32.5 </option> <option value='33.0'> 33.0 </option> <option value='33.5'> 33.5 </option> <option value='34.0'> 34.0 </option> <option value='34.5'> 34.5 </option> <option value='35.0'> 35.0 </option> <option value='35.5'> 35.5 </option> <option value='36.0'> 36.0 </option> <option value='36.5'> 36.5 </option> <option value='37.0'> 37.0 </option> <option value='37.5'> 37.5 </option> <option value='38.0'> 38.0 </option> <option value='38.5'> 38.5 </option> <option value='39.0'> 39.0 </option> <option value='39.5'> 39.5 </option> <option value='40.0'> 40.0 </option> </select>"+
            "</td>"+
            "</tr>"+
            "<tr>"+
            "<td>OS(Left Eye)</td>"+
            "<td>"+
            "<select name='rsph' class='padding-left5-ss padding-right10-ss prescription_val prescription_lsph'> <option value='-16.00'>-16.00</option> <option value='-15.75'>-15.75</option> <option value='-15.50'> -15.50 </option> <option value='-15.25'> -15.25 </option> <option value='-15.00'> -15.00 </option> <option value='-14.75'> -14.75 </option> <option value='-14.50'> -14.50 </option> <option value='-14.25'> -14.25 </option> <option value='-14.00'> -14.00 </option> <option value='-13.75'> -13.75 </option> <option value='-13.50'> -13.50 </option> <option value='-13.25'> -13.25 </option> <option value='-13.00'> -13.00 </option> <option value='-12.75'> -12.75 </option> <option value='-12.50'> -12.50 </option> <option value='-12.25'> -12.25 </option> <option value='-12.00'> -12.00 </option> <option value='-11.75'> -11.75 </option> <option value='-11.50'> -11.50 </option> <option value='-11.25'> -11.25 </option> <option value='-11.00'> -11.00 </option> <option value='-10.75'> -10.75 </option> <option value='-10.50'> -10.50 </option> <option value='-10.25'> -10.25 </option> <option value='-10.00'> -10.00 </option> <option value='-9.75'> -9.75 </option> <option value='-9.50'> -9.50 </option> <option value='-9.25'> -9.25 </option> <option value='-9.00'> -9.00 </option> <option value='-8.75'> -8.75 </option> <option value='-8.50'> -8.50 </option> <option value='-8.25'> -8.25 </option> <option value='-8.00'> -8.00 </option> <option value='-7.75'> -7.75 </option> <option value='-7.50'> -7.50 </option> <option value='-7.25'> -7.25 </option> <option value='-7.00'> -7.00 </option> <option value='-6.75'> -6.75 </option> <option value='-6.50'> -6.50 </option> <option value='-6.25'> -6.25 </option> <option value='-6.00'> -6.00 </option> <option value='-5.75'> -5.75 </option> <option value='-5.50'> -5.50 </option> <option value='-5.25'> -5.25 </option> <option value='-5.00'> -5.00 </option> <option value='-4.75'> -4.75 </option> <option value='-4.50'> -4.50 </option> <option value='-4.25'> -4.25 </option> <option value='-4.00'> -4.00 </option> <option value='-3.75'> -3.75 </option> <option value='-3.50'> -3.50 </option> <option value='-3.25'> -3.25 </option> <option value='-3.00'> -3.00 </option> <option value='-2.75'> -2.75 </option> <option value='-2.50'> -2.50 </option> <option value='-2.25'> -2.25 </option> <option value='-2.00'> -2.00 </option> <option value='-1.75'> -1.75 </option> <option value='-1.50'> -1.50 </option> <option value='-1.25'> -1.25 </option> <option value='-1.00'> -1.00 </option> <option value='-0.75'> -0.75 </option> <option value='-0.50'> -0.50 </option> <option value='-0.25'> -0.25 </option> <option value='0.00' selected='selected'> 0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> <option value='5.25'> +5.25 </option> <option value='5.50'> +5.50 </option> <option value='5.75'> +5.75 </option> <option value='6.00'> +6.00 </option> <option value='6.25'> +6.25 </option> <option value='6.50'> +6.50 </option> <option value='6.75'> +6.75 </option> <option value='7.00'> +7.00 </option> <option value='7.25'> +7.25 </option> <option value='7.50'> +7.50 </option> <option value='7.75'> +7.75 </option> <option value='8.00'> +8.00 </option> <option value='8.25'> +8.25 </option> <option value='8.50'> +8.50 </option> <option value='8.75'> +8.75 </option> <option value='9.00'> +9.00 </option> <option value='9.25'> +9.25 </option> <option value='9.50'> +9.50 </option> <option value='9.75'> +9.75 </option> <option value='10.00'> +10.00 </option> <option value='10.25'> +10.25 </option> <option value='10.50'> +10.50 </option> <option value='10.75'> +10.75 </option> <option value='11.00'> +11.00 </option> <option value='11.25'> +11.25 </option> <option value='11.50'> +11.50 </option> <option value='11.75'> +11.75 </option> <option value='12.00'> +12.00 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='rcyl' class='padding-left5-ss padding-right10-ss prescription_val prescription_lcyl'> <option value='-6.00'> -6.00 </option> <option value='-5.75'> -5.75 </option> <option value='-5.50'> -5.50 </option> <option value='-5.25'> -5.25 </option> <option value='-5.00'> -5.00 </option> <option value='-4.75'> -4.75 </option> <option value='-4.50'> -4.50 </option> <option value='-4.25'> -4.25 </option> <option value='-4.00'> -4.00 </option> <option value='-3.75'> -3.75 </option> <option value='-3.50'> -3.50 </option> <option value='-3.25'> -3.25 </option> <option value='-3.00'> -3.00 </option> <option value='-2.75'> -2.75 </option> <option value='-2.50'> -2.50 </option> <option value='-2.25'> -2.25 </option> <option value='-2.00'> -2.00 </option> <option value='-1.75'> -1.75 </option> <option value='-1.50'> -1.50 </option> <option value='-1.25'> -1.25 </option> <option value='-1.00'> -1.00 </option> <option value='-0.75'> -0.75 </option> <option value='-0.50'> -0.50 </option> <option value='-0.25'> -0.25 </option> <option value='0.00' selected='selected'> 0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> <option value='5.25'> +5.25 </option> <option value='5.50'> +5.50 </option> <option value='5.75'> +5.75 </option> <option value='6.00'> +6.00 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='rax' class='padding-left5-ss padding-right10-ss prescription_val prescription_lax'> <option value='000' selected='selected'>000</option> <option value='1'> 001 </option> <option value='2'> 002 </option> <option value='3'> 003 </option> <option value='4'> 004 </option> <option value='5'> 005 </option> <option value='6'> 006 </option> <option value='7'> 007 </option> <option value='8'> 008 </option> <option value='9'> 009 </option> <option value='10'> 010 </option> <option value='11'> 011 </option> <option value='12'> 012 </option> <option value='13'> 013 </option> <option value='14'> 014 </option> <option value='15'> 015 </option> <option value='16'> 016 </option> <option value='17'> 017 </option> <option value='18'> 018 </option> <option value='19'> 019 </option> <option value='20'> 020 </option> <option value='21'> 021 </option> <option value='22'> 022 </option> <option value='23'> 023 </option> <option value='24'> 024 </option> <option value='25'> 025 </option> <option value='26'> 026 </option> <option value='27'> 027 </option> <option value='28'> 028 </option> <option value='29'> 029 </option> <option value='30'> 030 </option> <option value='31'> 031 </option> <option value='32'> 032 </option> <option value='33'> 033 </option> <option value='34'> 034 </option> <option value='35'> 035 </option> <option value='36'> 036 </option> <option value='37'> 037 </option> <option value='38'> 038 </option> <option value='39'> 039 </option> <option value='40'> 040 </option> <option value='41'> 041 </option> <option value='42'> 042 </option> <option value='43'> 043 </option> <option value='44'> 044 </option> <option value='45'> 045 </option> <option value='46'> 046 </option> <option value='47'> 047 </option> <option value='48'> 048 </option> <option value='49'> 049 </option> <option value='50'> 050 </option> <option value='51'> 051 </option> <option value='52'> 052 </option> <option value='53'> 053 </option> <option value='54'> 054 </option> <option value='55'> 055 </option> <option value='56'> 056 </option> <option value='57'> 057 </option> <option value='58'> 058 </option> <option value='59'> 059 </option> <option value='60'> 060 </option> <option value='61'> 061 </option> <option value='62'> 062 </option> <option value='63'> 063 </option> <option value='64'> 064 </option> <option value='65'> 065 </option> <option value='66'> 066 </option> <option value='67'> 067 </option> <option value='68'> 068 </option> <option value='69'> 069 </option> <option value='70'> 070 </option> <option value='71'> 071 </option> <option value='72'> 072 </option> <option value='73'> 073 </option> <option value='74'> 074 </option> <option value='75'> 075 </option> <option value='76'> 076 </option> <option value='77'> 077 </option> <option value='78'> 078 </option> <option value='79'> 079 </option> <option value='80'> 080 </option> <option value='81'> 081 </option> <option value='82'> 082 </option> <option value='83'> 083 </option> <option value='84'> 084 </option> <option value='85'> 085 </option> <option value='86'> 086 </option> <option value='87'> 087 </option> <option value='88'> 088 </option> <option value='89'> 089 </option> <option value='90'> 090 </option> <option value='91'> 091 </option> <option value='92'> 092 </option> <option value='93'> 093 </option> <option value='94'> 094 </option> <option value='95'> 095 </option> <option value='96'> 096 </option> <option value='97'> 097 </option> <option value='98'> 098 </option> <option value='99'> 099 </option> <option value='100'> 100 </option> <option value='101'> 101 </option> <option value='102'> 102 </option> <option value='103'> 103 </option> <option value='104'> 104 </option> <option value='105'> 105 </option> <option value='106'> 106 </option> <option value='107'> 107 </option> <option value='108'> 108 </option> <option value='109'> 109 </option> <option value='110'> 110 </option> <option value='111'> 111 </option> <option value='112'> 112 </option> <option value='113'> 113 </option> <option value='114'> 114 </option> <option value='115'> 115 </option> <option value='116'> 116 </option> <option value='117'> 117 </option> <option value='118'> 118 </option> <option value='119'> 119 </option> <option value='120'> 120 </option> <option value='121'> 121 </option> <option value='122'> 122 </option> <option value='123'> 123 </option> <option value='124'> 124 </option> <option value='125'> 125 </option> <option value='126'> 126 </option> <option value='127'> 127 </option> <option value='128'> 128 </option> <option value='129'> 129 </option> <option value='130'> 130 </option> <option value='131'> 131 </option> <option value='132'> 132 </option> <option value='133'> 133 </option> <option value='134'> 134 </option> <option value='135'> 135 </option> <option value='136'> 136 </option> <option value='137'> 137 </option> <option value='138'> 138 </option> <option value='139'> 139 </option> <option value='140'> 140 </option> <option value='141'> 141 </option> <option value='142'> 142 </option> <option value='143'> 143 </option> <option value='144'> 144 </option> <option value='145'> 145 </option> <option value='146'> 146 </option> <option value='147'> 147 </option> <option value='148'> 148 </option> <option value='149'> 149 </option> <option value='150'> 150 </option> <option value='151'> 151 </option> <option value='152'> 152 </option> <option value='153'> 153 </option> <option value='154'> 154 </option> <option value='155'> 155 </option> <option value='156'> 156 </option> <option value='157'> 157 </option> <option value='158'> 158 </option> <option value='159'> 159 </option> <option value='160'> 160 </option> <option value='161'> 161 </option> <option value='162'> 162 </option> <option value='163'> 163 </option> <option value='164'> 164 </option> <option value='165'> 165 </option> <option value='166'> 166 </option> <option value='167'> 167 </option> <option value='168'> 168 </option> <option value='169'> 169 </option> <option value='170'> 170 </option> <option value='171'> 171 </option> <option value='172'> 172 </option> <option value='173'> 173 </option> <option value='174'> 174 </option> <option value='175'> 175 </option> <option value='176'> 176 </option> <option value='177'> 177 </option> <option value='178'> 178 </option> <option value='179'> 179 </option> <option value='180'> 180 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='radd' class='padding-left5-ss padding-right10-ss radd-temp-select radd_margin_top_lin prescription_val prescription_change_radd prescription_ladd' style='margin-top: 0px;'> <option value='0.00' selected='selected'>0.00</option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> </select>"+
            "</td>"+
            "<td class='lpd_rpd'>"+
            "<select name='rpd' class='pg-margin-top5 padding-left5-ss padding-right10-ss prescription_val prescription_rpd_change prescription_lpd'> <option selected='selected' value=''>Select</option><option value='17.5'> 17.5 </option> <option value='18.0'> 18.0 </option> <option value='18.5'> 18.5 </option> <option value='19.0'> 19.0 </option> <option value='19.5'> 19.5 </option> <option value='20.0'> 20.0 </option> <option value='20.5'> 20.5 </option> <option value='21.0'> 21.0 </option> <option value='21.5'> 21.5 </option> <option value='22.0'> 22.0 </option> <option value='22.5'> 22.5 </option> <option value='23.0'> 23.0 </option> <option value='23.5'> 23.5 </option> <option value='24.0'> 24.0 </option> <option value='24.5'> 24.5 </option> <option value='25.0'> 25.0 </option> <option value='25.5'> 25.5 </option> <option value='26.0'> 26.0 </option> <option value='26.5'> 26.5 </option> <option value='27.0'> 27.0 </option> <option value='27.5'> 27.5 </option> <option value='28.0'> 28.0 </option> <option value='28.5'> 28.5 </option> <option value='29.0'> 29.0 </option> <option value='29.5'> 29.5 </option> <option value='30.0'> 30.0 </option> <option value='30.5'> 30.5 </option> <option value='31.0'> 31.0 </option> <option value='31.5'> 31.5 </option> <option value='32.0'> 32.0 </option> <option value='32.5'> 32.5 </option> <option value='33.0'> 33.0 </option> <option value='33.5'> 33.5 </option> <option value='34.0'> 34.0 </option> <option value='34.5'> 34.5 </option> <option value='35.0'> 35.0 </option> <option value='35.5'> 35.5 </option> <option value='36.0'> 36.0 </option> <option value='36.5'> 36.5 </option> <option value='37.0'> 37.0 </option> <option value='37.5'> 37.5 </option> <option value='38.0'> 38.0 </option> <option value='38.5'> 38.5 </option> <option value='39.0'> 39.0 </option> <option value='39.5'> 39.5 </option> <option value='40.0'> 40.0 </option> </select>"+
            "</td>"+
            "</tr>"+
            "</tbody>"+
            "</table>"+
            "<div class='Prism_title'>Need Prism Correction</div>"+
            "<table class='layui-table' id='layerDemo-prism-edit_Rx'>"+
            "<colgroup>"+
            "<col width='150' />"+
            "<col width='200' />"+
            "<col />"+
            "</colgroup>"+
            "<thead>"+
            "<tr>"+
            "<th></th>"+
            "<th>Prism Value</th>"+
            "<th>Base Direction</th>"+
            "<th>Prism Value</th>"+
            "<th>Base Direction</th>"+
            "</tr>"+
            "</thead>"+
            "<tbody>"+
            "<tr>"+
            "<td>OD(Right Eye)</td>"+
            "<td>"+
            "<select name='rpri' class='prescription_val prescription_val_rpri prescription_rpri' style='padding-left:5px;'> <option value='0.00'> +0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> <option value='5.25'> +5.25 </option> <option value='5.50'> +5.50 </option> <option value='5.75'> +5.75 </option> <option value='6.00'> +6.00 </option> </select>"+
            "</td>"+
            "<td><select name='rbase' style='padding-left:5px;' class='base_direction_change_top_one prescription_val prescription_val_rbase prescription_rbase'>"+
            "<option value='' selected='selected'>Choose</option>"+
            "<option value='In'>In</option>"+
            "<option value='Out'>Out</option>"+
            "<option value='Up'>Up</option>"+
            "<option value='Down'>Down</option>"+
            "</select></td>"+
            "<td>"+
            "<select name='rpri_1' class='prescription_val prescription_val_rpri_1 prescription_rpri_1' style='padding-left: 5px;'> <option value='0.00'> +0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='rbase_1' style='padding-left: 5px;' class='base_direction_change_top_two prescription_val prescription_val_rbase_1 prescription_rbase_1'>"+
            "<option value='' selected='selected'>Choose</option>"+
            "<option value='In'>In</option>"+
            "<option value='Out'>Out</option>"+
            "<option value='Up'>Up</option>"+
            "<option value='Down'>Down</option>"+
            "</select>"+
            "</td>"+
            "</tr>"+
            "<tr>"+
            "<td>OS(Left Eye)</td>"+
            "<td>"+
            "<select name='rpri' class='prescription_val prescription_val_rpri prescription_lpri' style='padding-left:5px;'> <option value='0.00'> +0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> <option value='5.25'> +5.25 </option> <option value='5.50'> +5.50 </option> <option value='5.75'> +5.75 </option> <option value='6.00'> +6.00 </option> </select>"+
            "</td>"+
            "<td><select name='rbase' style='padding-left:5px;' class='base_direction_change_top_one prescription_val prescription_val_rbase prescription_lbase'>"+
            "<option value='' selected='selected'>Choose</option>"+
            "<option value='In'>In</option>"+
            "<option value='Out'>Out</option>"+
            "<option value='Up'>Up</option>"+
            "<option value='Down'>Down</option>"+
            "</select></td>"+
            "<td>"+
            "<select name='rpri_1' class='prescription_val prescription_val_rpri_1 prescription_lpri_1' style='padding-left: 5px;'> <option value='0.00'> +0.00 </option> <option value='0.25'> +0.25 </option> <option value='0.50'> +0.50 </option> <option value='0.75'> +0.75 </option> <option value='1.00'> +1.00 </option> <option value='1.25'> +1.25 </option> <option value='1.50'> +1.50 </option> <option value='1.75'> +1.75 </option> <option value='2.00'> +2.00 </option> <option value='2.25'> +2.25 </option> <option value='2.50'> +2.50 </option> <option value='2.75'> +2.75 </option> <option value='3.00'> +3.00 </option> <option value='3.25'> +3.25 </option> <option value='3.50'> +3.50 </option> <option value='3.75'> +3.75 </option> <option value='4.00'> +4.00 </option> <option value='4.25'> +4.25 </option> <option value='4.50'> +4.50 </option> <option value='4.75'> +4.75 </option> <option value='5.00'> +5.00 </option> </select>"+
            "</td>"+
            "<td>"+
            "<select name='rbase_1' style='padding-left: 5px;' class='base_direction_change_top_two prescription_val prescription_val_rbase_1 prescription_lbase_1'>"+
            "<option value='' selected='selected'>Choose</option>"+
            "<option value='In'>In</option>"+
            "<option value='Out'>Out</option>"+
            "<option value='Up'>Up</option>"+
            "<option value='Down'>Down</option>"+
            "</select>"+
            "</td>"+
            "</tr>"+
            "</tbody>"+
            "</table>"+
            "<blockquote class='layui-elem-quote'><p><span>Date of Prescription: </span><input type='text' class='layui-input' id='exam_date_ipt' /></p></blockquote>"+
            "<blockquote class='layui-elem-quote'><p><span>Expiration Time: </span><input type='text' class='layui-input' id='expire_date_ipt'></p></blockquote>"+
            "<blockquote class='layui-elem-quote'><p><span>Prescription Nickname: </span><input type='text' class='PrescriptionNickname'></p></blockquote>"+
            "</div>";

        if(type=="edit"){ //编辑验光单
            setTimeout(function(){
                var edit_add_img = "";
                if(editrx_obj['prescription_img']){
                    if(editrx_obj['prescription_img'].indexOf("payneglasses")==-1){
                        try{
                            editrx_obj['prescription_img'] = staticUrl+"/media/customer/prescriptions" + editrx_obj['prescription_img'];
                        }catch(e){
                            editrx_obj['prescription_img'] = "/media/customer/prescriptions" + editrx_obj['prescription_img'];
                        }
                    }else{
                        editrx_obj['prescription_img'] = editrx_obj['prescription_img'];
                    }
                    edit_add_img="<img  class='prescription_img prescription_img_edit' src="+editrx_obj['prescription_img']+" alt=''><p class='tip_img'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p><form id='uploadForm'>"+
                        "<input type='file' accept='image/*' name='uploadFile' id='uploadFile' style='visibility:hidden;position:absolute;top:0px;width:0px'/>"+
                        "</form>"+
                        "<input type='button' id='btn' class='btn_add btn_add_file' value='Upload prescription image'/>"+
                        "<img id='portrait' src='' style='display:none;' /><p class='portrait_img tip_img' style='display:none;text-align:left'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
                }else{
                    edit_add_img ="<form id='uploadForm'>"+
                        "<input type='file' accept='image/*' name='uploadFile' id='uploadFile' style='visibility:hidden;position:absolute;top:0px;width:0px'/>"+
                        "</form>"+
                        "<input type='button' id='btn' class='btn_add btn_add_file' value='Upload prescription image'/>"+
                        "<img id='portrait' src='' style='display:none;' /><p class='portrait_img tip_img' style='display:none;text-align:left'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
                }
                $(".edit_add_img").html(edit_add_img);

                new_mr_options('.prescription_rsph option','.prescription_rsph',editrx_obj["rsph"]);
                new_mr_options('.prescription_lsph option','.prescription_lsph',editrx_obj["lsph"]);
                new_mr_options('.prescription_rcyl option','.prescription_rcyl',editrx_obj["rcyl"]);
                new_mr_options('.prescription_lcyl option','.prescription_lcyl',editrx_obj["lcyl"]);
                new_mr_options('.prescription_rax option','.prescription_rax',editrx_obj["rax"]);
                new_mr_options('.prescription_lax option','.prescription_lax',editrx_obj["lax"]);
                new_mr_options('.prescription_radd option','.prescription_radd',editrx_obj["radd"]);
                new_mr_options('.prescription_ladd option','.prescription_ladd',editrx_obj["ladd"]);

                if(editrx_obj["single_pd"]=="1"){
                    $(".pd").attr("rowspan",2);
                    $(".pd").show();
                    $(".lpd_rpd").hide();
                    $(".binocular_pd").addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                    new_mr_options('.prescription_pd option','.prescription_pd',editrx_obj["pd"]);
                }else{
                    $(".pd").removeAttr("rowspan");
                    $(".pd").hide();
                    $(".lpd_rpd").show();
                    $(".monocular_pd").addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                    new_mr_options('.prescription_rpd option','.prescription_rpd',editrx_obj["rpd"]);
                    new_mr_options('.prescription_lpd option','.prescription_lpd',editrx_obj["lpd"]);
                }


                new_mr_options('.prescription_rpri option','.prescription_rpri',editrx_obj["rpri"]);
                new_mr_options('.prescription_lpri option','.prescription_lpri',editrx_obj["lpri"]);
                new_mr_options('.prescription_rbase option','.prescription_rbase',editrx_obj["rbase"]);
                new_mr_options('.prescription_lbase option','.prescription_lbase',editrx_obj["lbase"]);
                new_mr_options('.prescription_rpri_1 option','.prescription_rpri_1',editrx_obj["rpri_1"]);
                new_mr_options('.prescription_lpri_1 option','.prescription_lpri_1',editrx_obj["lpri_1"]);
                new_mr_options('.prescription_rbase_1 option','.prescription_rbase_1',editrx_obj["rbase_1"]);
                new_mr_options('.prescription_lbase_1 option','.prescription_lbase_1',editrx_obj["lbase_1"]);
                $(".PrescriptionNickname").val(editrx_obj["prescription_name"]);

                if(editrx_obj["exam_date"]){
                    var new_exam_date = editrx_obj["exam_date"].split("-");
                    var year = new_exam_date[0];
                    var str_new_exam_date = new_exam_date[1]+"/"+new_exam_date[2]+"/"+year;
                    $("#exam_date_ipt").val(str_new_exam_date);
                    layui.use('laydate', function(){
                        var laydate = layui.laydate;
                        laydate.render({
                            elem: '#exam_date_ipt'
                            ,lang: 'en'
                            ,format: 'MM/dd/yyyy'
                        });
                    })
                }
                if(editrx_obj["expire_date"]){
                    var new_expire_date = editrx_obj["expire_date"].split("-");
                    var year = new_expire_date[0];
                    var str_new_expire_date = new_expire_date[1]+"/"+new_expire_date[2]+"/"+year;
                    $("#expire_date_ipt").val(str_new_expire_date);
                    layui.use('laydate', function(){
                        var laydate = layui.laydate;
                        laydate.render({
                            elem: '#expire_date_ipt'
                            ,lang: 'en'
                            ,format: 'MM/dd/yyyy'
                        });
                    })
                }

            },200)
        }else{  //新增验光单
            setTimeout(function(){
                $(".pd").attr("rowspan",2);
                $(".pd").show();
                $(".lpd_rpd").hide();
                $(".binocular_pd").addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                var edit_add_img = "";
                if(editrx_obj['prescription_img']){
                    if(editrx_obj['prescription_img'].indexOf("payneglasses")==-1){
                        try{
                            editrx_obj['prescription_img']= staticUrl+"/media/customer/prescriptions" + editrx_obj['prescription_img']
                        }catch(e){
                            editrx_obj['prescription_img'] = "/media/customer/prescriptions" + editrx_obj['prescription_img']
                        }
                    }else{
                        editrx_obj['prescription_img'] = editrx_obj['prescription_img'];
                    }

                    edit_add_img="<img  class='prescription_img prescription_img_edit' src="+editrx_obj['prescription_img']+" alt=''><p class='tip_img'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p><form id='uploadForm'>"+
                        "<input type='file' accept='image/*' name='uploadFile' id='uploadFile' style='visibility:hidden;position:absolute;top:0px;width:0px'/>"+
                        "</form>"+
                        "<input type='button' id='btn' class='btn_add btn_add_file' value='Upload prescription image'/>"+
                        "<img id='portrait' src='' style='display:none;' /><p class='portrait_img tip_img' style='display:none;text-align:center'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
                }else if(editrx_obj['ahc_prescription_img']){
                    if(editrx_obj['ahc_prescription_img'].indexOf("payneglasses")==-1){
                        try{
                            editrx_obj['ahc_prescription_img']= staticUrl+"/media/customer/prescriptions" + editrx_obj['ahc_prescription_img']
                        }catch(e){
                            editrx_obj['ahc_prescription_img'] = "/media/customer/prescriptions" + editrx_obj['ahc_prescription_img']
                        }
                    }else{
                        editrx_obj['ahc_prescription_img'] = editrx_obj['ahc_prescription_img'];
                    }

                    edit_add_img="<img  class='prescription_img prescription_img_edit' src="+editrx_obj['ahc_prescription_img']+" alt=''><p class='tip_img'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p><form id='uploadForm'>"+
                        "<input type='file' accept='image/*' name='uploadFile' id='uploadFile' style='visibility:hidden;position:absolute;top:0px;width:0px'/>"+
                        "</form>"+
                        "<input type='button' id='btn' class='btn_add btn_add_file' value='Upload prescription image'/>"+
                        "<img id='portrait' src='' style='display:none;' /><p class='portrait_img tip_img' style='display:none;text-align:center'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
                }else{
                    edit_add_img ="<form id='uploadForm'>"+
                        "<input type='file' accept='image/*' name='uploadFile' id='uploadFile' style='visibility:hidden;position:absolute;top:0px;width:0px'/>"+
                        "</form>"+
                        "<input type='button' id='btn' class='btn_add btn_add_file' value='Upload prescription image'/>"+
                        "<img id='portrait' src='' style='display:none;' /><p class='portrait_img tip_img' style='display:none;text-align:center'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
                }





                $(".edit_add_img").html(edit_add_img);


                // 点击add rx后给当前验光单[Date of Prescription:/Expiration Time:] 赋值


                var exam_date_item = new Date();
                var getYear = exam_date_item.getFullYear(); //获取当前年份
                var getMonth = exam_date_item.getMonth(); //获取当前月份0-11,0代表1月
                var getDate = exam_date_item.getDate(); //获取当前日1-31
                var current_exam_date_item = (getMonth+1)+"/"+getDate+"/"+getYear;
                $("#exam_date_ipt").val(current_exam_date_item);
                layui.use('laydate', function(){
                    var laydate = layui.laydate;
                    laydate.render({
                        elem: '#exam_date_ipt'
                        ,lang: 'en'
                        ,format: 'MM/dd/yyyy'
                    });
                })

                var current_expire_date_item = (getMonth+1)+"/"+getDate+"/"+(getYear+2);
                $("#expire_date_ipt").val(current_expire_date_item);
                layui.use('laydate', function(){
                    var laydate = layui.laydate;
                    laydate.render({
                        elem: '#expire_date_ipt'
                        ,lang: 'en'
                        ,format: 'MM/dd/yyyy'
                    });
                })
            },200)
        }
        return EditRx;
    }

    //add rx【新增眼光单】/ edit[编辑验光单] 收集参数 函数：
    function edit_add_ajax_obj(type,rx_obj){
        var rsph_val = $('.prescription_rsph option:selected').val();
        var lsph_val = $('.prescription_lsph option:selected').val();
        var rcyl_val = $('.prescription_rcyl option:selected').val();
        var lcyl_val = $('.prescription_lcyl option:selected').val();
        var rax_val = $('.prescription_rax option:selected').val();
        var lax_val = $('.prescription_lax option:selected').val();
        var radd_val = $('.prescription_radd option:selected').val();
        var ladd_val = $('.prescription_ladd option:selected').val();
        var pd_val = $('.prescription_pd option:selected').val();
        var lpd_val = $('.prescription_lpd option:selected').val();
        var rpd_val = $('.prescription_rpd option:selected').val();
        var rpri_val = $('.prescription_rpri option:selected').val();
        var lpri_val = $('.prescription_lpri option:selected').val();
        var rbase_val = $('.prescription_rbase option:selected').val();
        var lbase_val = $('.prescription_lbase option:selected').val();
        var rpri_1_val = $('.prescription_rpri_1 option:selected').val();
        var lpri_1_val = $('.prescription_lpri_1 option:selected').val();
        var rbase_1_val = $('.prescription_rbase_1 option:selected').val();
        var lbase_1_val = $('.prescription_lbase_1 option:selected').val();
        var exam_date_val = $('#exam_date_ipt').val();
        var expire_date = $('#expire_date_ipt').val();
        var prescription_name_val = $('.PrescriptionNickname').val();

        var current_rpri = rpri_val?rpri_val:"0.00";
        var current_lpri = lpri_val?lpri_val:"0.00";
        var current_rbase = rbase_val;
        var current_lbase = lbase_val;
        var current_rpri_1 = rpri_1_val?rpri_1_val:"0.00";
        var current_lpri_1 = lpri_1_val?lpri_1_val:"0.00";
        var current_rbase_1 = rbase_1_val;
        var current_lbase_1 = lbase_1_val;
        current_rpri = parseFloat(current_rpri);
        current_lpri = parseFloat(current_lpri);
        current_rpri_1 = parseFloat(current_rpri_1);
        current_lpri_1 = parseFloat(current_lpri_1);

        var is_prism = "";
        var prism_more_check = "";
        if(current_rpri || current_lpri || current_rbase || current_lbase || current_rpri_1 || current_lpri_1 || current_rbase_1 || current_lbase_1){
            if(current_rpri || current_lpri || current_rbase || current_lbase){
                is_prism = "1"
            }else{
                is_prism = ""
            }

            if(current_rpri_1 || current_lpri_1 || current_rbase_1 || current_lbase_1){
                prism_more_check = "no"
            }else{
                prism_more_check = ""
            }

        }else{
            is_prism = "";
            prism_more_check = ""
        }


        //验证Need Prism Correction栏 一对Prism和Base必须都具有值或都为空 判断
        var rpri_rbase_if = (!((current_rpri && current_rbase) || ((!current_rpri) && (!current_rbase))));
        var lpri_lbase_if = (!((current_lpri && current_lbase) || ((!current_lpri) && (!current_lbase))));
        var rpri_1_rbase_1_if = (!((current_rpri_1 && current_rbase_1) || ((!current_rpri_1) && (!current_rbase_1))));
        var lpri_1_lbase_1_if = (!((current_lpri_1 && current_lbase_1) || ((!current_lpri_1) && (!current_lbase_1))));
        //验证Need Prism Correction栏 全部为空条件
        var all_prism_clear = ((!current_rpri) && (!current_rbase) && (!current_lpri) && (!current_lbase) && (!current_rpri_1) && (!current_rbase_1) && (!current_lpri_1) && (!current_lbase_1));
        if(rpri_rbase_if){
            if(!current_rpri){
                $(".prescription_rpri").css("borderColor","red");
            }else{
                $(".prescription_rbase").css("borderColor","red");
            }
        }else if(lpri_lbase_if){
            if(!current_lpri){
                $(".prescription_lpri").css("borderColor","red");
            }else{
                $(".prescription_lbase").css("borderColor","red");
            }
        }else if(rpri_1_rbase_1_if){
            if(!current_rpri_1){
                $(".prescription_rpri_1").css("borderColor","red");
            }else{
                $(".prescription_rbase_1").css("borderColor","red");
            }
        }else if(lpri_1_lbase_1_if){
            if(!current_lpri_1){
                $(".prescription_lpri_1").css("borderColor","red");
            }else{
                $(".prescription_lbase_1").css("borderColor","red");
            }
        }else{
            var rpi_1 = (((current_rpri_1 && current_rbase_1) && ((current_rpri && current_rbase) || (current_lpri && current_lbase))));
            var lpi_1 =  (((current_lpri_1 && current_lbase_1)) && ((current_rpri && current_rbase) || (current_lpri && current_lbase)));
            //验证后四个值都为空 条件成立
            var hou_prism_clear = (!current_rpri_1 && !current_rbase_1 && !current_lpri_1 && !current_lbase_1);
            if(!all_prism_clear && (!hou_prism_clear)){
                if(!(rpi_1 || lpi_1)){
                    $(".prescription_rpri").css("borderColor","red");
                    $(document).on("change",".prescription_lpri,.prescription_lbase,.prescription_rbase",function(){
                        $(".prescription_rpri").css("borderColor","#ccc");
                    });
                    return false;
                }
            }
        }
        if(rpri_rbase_if || lpri_lbase_if || rpri_1_rbase_1_if || lpri_1_lbase_1_if){
            layer.alert("A pair of Prism and Base must both have values or both being empty. Please check your entries.",{
                title:"Please confirm your Prism and Base !"
                ,area: ['520px', 'auto']
                ,btn: ['Close']
                ,icon: 5
            });
            return false;
        }
        //棱镜验证结束


        //CYL和AXIS成对存在验证
        var new_rax_val = parseFloat(rax_val);
        var new_rcyl_val = parseFloat(rcyl_val);
        var new_lax_val = parseFloat(lax_val);
        var new_lcyl_val = parseFloat(lcyl_val);
        var rax_rcyl_if = (!((new_rax_val && new_rcyl_val) || ((!new_rax_val) && (!new_rcyl_val))));
        var lax_lcyl_if = (!((new_lax_val && new_lcyl_val) || ((!new_lax_val) && (!new_lcyl_val))));
        if(rax_rcyl_if){
            if(!new_rax_val){
                $(".prescription_rax").css("borderColor","red");
            }else{
                $(".prescription_rcyl").css("borderColor","red");
            }
        }else if(lax_lcyl_if){
            if(!new_lax_val){
                $(".prescription_lax").css("borderColor","red");
            }else{
                $(".prescription_lcyl").css("borderColor","red");
            }
        }
        if(rax_rcyl_if || lax_lcyl_if){
            layer.alert("A pair of CYL and AXIS must both have values or both being empty. Please check your entries.",{
                title:"Please confirm your CYL and AXIS !"
                ,area: ['520px', 'auto']
                ,btn: ['Close']
                ,icon: 5
            });
            return false;
        }
        //CYL和AXIS成对存在验证 结束


        var prescription = {
            is_active: "1",
            rsph:rsph_val,
            rcyl:rcyl_val,
            rax:rax_val=="000"?'0':rax_val,
            lsph:lsph_val,
            lcyl:lcyl_val,
            lax:lax_val=="000"?'0':lax_val,
            radd:radd_val,
            ladd:ladd_val,
            prism:is_prism,
            prism_more_check:prism_more_check,
            lpri:lpri_val,
            rbase:rbase_val,
            rpri:rpri_val,
            lbase:lbase_val,
            rpri_1:rpri_1_val,
            rbase_1:rbase_1_val,
            lpri_1:lpri_1_val,
            lbase_1:lbase_1_val,
            pd:pd_val?pd_val:"",
            rpd:rpd_val,
            lpd:lpd_val,
            exam_date:exam_date_val,
            expire_date:expire_date,
            prescription_name:prescription_name_val,
            single_pd:pd_val?'1':'0',
            prescription_img:$("#uploadFile").attr("src")
        };

        if(type=="edit"){
            edit_entity_id = rx_obj["this_entity_id"];
            prescription["entity_id"] = edit_entity_id;
        }else{
            help_cart_id = rx_obj["help_cart_id_add"];
            prescription["help_cart_id"] = help_cart_id;
        }




        //除了pd 没有填写其他任何值判断
        var new_radd_val = parseFloat(radd_val);
        var new_ladd_val = parseFloat(ladd_val);
        var new_rsph_val = parseFloat(rsph_val);
        var new_lsph_val = parseFloat(lsph_val);
        var all_val_rx = ((!new_rsph_val) && (!new_lsph_val) && (!new_rcyl_val) && (!new_lcyl_val) && (!new_rax_val) && (!new_lax_val) && (!new_radd_val) && (!new_ladd_val) && (!current_rpri) && (!current_lpri) && (!current_rbase) && (!current_lbase) && (!current_rpri_1) && (!current_lpri_1) && (!current_rbase_1) && (!current_lbase_1));
        //除了pd 没有填写其他任何值判断 结束


        //判断pd为空不可保存
        if((!pd_val) && (!lpd_val) && (!rpd_val)){
            layer.alert(' Your PD is missing. PD (Pupillary distance) is the distance in millimeters between the centers of the pupils in each eye.', {
                title:"Your PD is missing !"
                ,area: ['520px', 'auto']
                ,btn: ['Close']
                ,icon: 5
            });
            $('.prescription_pd,.prescription_rpd,.prescription_lpd').css("borderColor","red");
            return false;
        }else if(all_val_rx){ //除了pd 没有填写其他任何不可报错
            layer.alert("Do you want to order lenses without a prescription? Please select non-prescription type instead!",{
                title:"Your prescription has no value, please confirm your prescription!"
                ,area: ['520px', 'auto']
                ,btn: ['Close']
                ,icon: 5
            });
            return false;
        }else if(!prescription_name_val){
            layer.alert('Please enter the name of this prescription', {
                title:"Your prescription name is missing"
                ,area: ['520px', 'auto']
                ,btn: ['Close']
                ,icon: 5
            });
            $('.PrescriptionNickname').css("borderColor","red");
            return false;
        }else{
            return prescription;
        }
    }

    //初始化调用获取所有求助客户列表函数
    init_rx_ajax(current_page_fn(),current_filter_fn());

    //点击图片放大函数：
    function imgShow(outerdiv, innerdiv, bigimg, _this){
        var src = _this.attr("src");//获取当前点击的pimg元素中的src属性
        $(bigimg).attr("src", src);//设置#bigimg元素的src属性
        /*获取当前点击图片的真实大小，并显示弹出层及大图*/
        $("<img/>").attr("src", src).load(function(){
            var windowW = $(window).width();//获取当前窗口宽度
            var windowH = $(window).height();//获取当前窗口高度
            var realWidth = this.width;//获取图片真实宽度
            var realHeight = this.height;//获取图片真实高度
            var imgWidth, imgHeight;
            var scale = 0.8;//缩放尺寸，当图片真实宽度和高度大于窗口宽度和高度时进行缩放
            if(realHeight>windowH*scale) {//判断图片高度
                imgHeight = windowH*scale;//如大于窗口高度，图片高度进行缩放
                imgWidth = imgHeight/realHeight*realWidth;//等比例缩放宽度
                if(imgWidth>windowW*scale) {//如宽度扔大于窗口宽度
                    imgWidth = windowW*scale;//再对宽度进行缩放
                }
            } else if(realWidth>windowW*scale) {//如图片高度合适，判断图片宽度
                imgWidth = windowW*scale;//如大于窗口宽度，图片宽度进行缩放
                imgHeight = imgWidth/realWidth*realHeight;//等比例缩放高度
            } else {//如果图片真实高度和宽度都符合要求，高宽不变
                imgWidth = realWidth;
                imgHeight = realHeight;
            }
            $(bigimg).css("width",imgWidth);//以最终的宽度对图片缩放
            var w = (windowW-imgWidth)/2;//计算图片与窗口左边距
            var h = (windowH-imgHeight)/2;//计算图片与窗口上边距
            $(innerdiv).css({"top":h, "left":w});//设置#innerdiv的top和left属性
            $(outerdiv).fadeIn("fast");//淡入显示#outerdiv及.pimg
        });
        $(outerdiv).click(function(){//再次点击淡出消失弹出层
            $(this).fadeOut("fast");
        });
    }

    //查看当前求助人信息【点击view按钮】
    $(document).on('click','#layerDemo .layui-btn-xs-view',function(){
        $(this).parents("tr").css("backgroundColor","#f1f16b44").siblings("tr").css("backgroundColor","#fff");
        var current_tr_idx = $(this).parents("tr").index();
        window.localStorage.setItem("current_tr_idx",current_tr_idx);
        var this_viewUser_obj = $(this).attr("viewUser_obj");
        if(exhibits(this_viewUser_obj)){
            this_viewUser_obj = JSON.parse(this_viewUser_obj);
        }else{
            this_viewUser_obj = {};
        }
        var view_help = view_add_help("edit",this_viewUser_obj);
        layer.open({
            type: 1
            ,title: "<p class='help_view'>View in user information</p>"
            ,closeBtn: 1,
            shadeClose:true
            ,area: ['90%', '90%']
            ,shade: 0.8
            ,id: 'LAY_layuipro'
            ,btn: ['Cancel']
            ,btnAlign: 'c'
            ,moveType: 0
            ,move:false
            ,content: view_help
        })
    });

    // 查看当前求助人的验光单【点击confirm rx按钮】
    $(document).on('click','#layerDemo .layui-btn-xs-rx',function(){
        var viewRx_email = $(this).attr("viewRx_email");
        var help_cart_id = $(this).attr("help_cart_id");
        $(this).parents("tr").css("backgroundColor","#f1f16b44").siblings("tr").css("backgroundColor","#fff");
        var current_tr_idx = $(this).parents("tr").index();
        window.localStorage.setItem("current_tr_idx",current_tr_idx);
        var this_viewRx = $(this).attr("viewRx");
        if(exhibits(this_viewRx)){
            this_viewRx = JSON.parse(this_viewRx);
        }else{
            this_viewRx = {};
        }
        var help_view_img = "";

        if(this_viewRx['prescription_img']){
            if(this_viewRx['prescription_img'].indexOf("payneglasses")==-1){
                try{
                    this_viewRx['prescription_img']= staticUrl+"/media/customer/prescriptions" + this_viewRx['prescription_img'];
                }catch(e){
                    this_viewRx['prescription_img'] = "/media/customer/prescriptions" + this_viewRx['prescription_img'];
                }
            }else{
                this_viewRx['prescription_img'] = this_viewRx['prescription_img']
            }
            help_view_img="<img alt='' style='' id='displayImg' src="+this_viewRx['prescription_img']+" /><p class='tip_img'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
        }


        var rx_html = "<div class='rx_parents_class'>"+
            help_view_img+
            "<table class='layui-table'  id='layerDemo-rx'>"+
            "<colgroup><col width='150' /><col width='200' /><col />"+
            "</colgroup>"+
            "<thead>"+
            "<tr>"+
            "<th></th>"+
            "<th>SPH(Sphere)</th>"+
            "<th>CYL(Cylinder)</th>"+
            "<th>AXIS</th>"+
            "<th>ADD(NV-ADD)</th>"+
            "<th>PD(Pupillary Distance)"+
            "</th>"+
            "</tr>"+
            "</thead>"+
            "<tbody>"+
            "<tr>"+
            "<td>OD(Right Eye)</td>"+
            "<td class='rsph'></td>"+
            "<td class='rcyl'></td>"+
            "<td class='rax'></td>"+
            "<td class='radd'></td>"+
            "<td class='rpd rpd_pd'></td>"+
            "</tr>"+
            "<tr>"+
            "<td>OS(Left Eye)</td>"+
            "<td class='lsph'></td>"+
            "<td class='lcyl'></td>"+
            "<td class='lax'></td>"+
            "<td class='ladd'></td>"+
            "<td class='lpd lpd_pd'></td>"+
            "</tr>"+
            "</tbody>"+
            "</table>"+
            "<div class='Prism_title'>Need Prism Correction</div>"+
            "<table class='layui-table'  id='layerDemo-prism'>"+
            "<colgroup>"+
            "<col width='150' />"+
            "<col width='200' />"+
            "<col />"+
            "</colgroup>"+
            "<thead>"+
            "<tr>"+
            "<th></th>"+
            "<th>Prism Value</th>"+
            "<th>Base Direction</th>"+
            "<th>Prism Value</th>"+
            "<th>Base Direction</th>"+
            "</tr>"+
            "</thead>"+
            "<tbody>"+
            "<tr>"+
            "<td>OD(Right Eye)</td>"+
            "<td class='rpri'></td>"+
            "<td class='rbase'></td>"+
            "<td class='rpri_1'></td>"+
            "<td class='rbase_1'></td>"+
            "</tr>"+
            "<tr>"+
            "<td>OS(Left Eye)</td>"+
            "<td class='lpri'></td>"+
            "<td class='lbase'></td>"+
            "<td class='lpri_1'></td>"+
            "<td class='lbase_1'></td>"+
            "</tr>"+
            "</tbody>"+
            "</table>"+
            "<blockquote class='layui-elem-quote'><p><span>Date of Prescription: </span><span class='DatePrescriptio_time'>12/16/2020</sapn></p></blockquote>"+
            "<blockquote class='layui-elem-quote'><p><span>Expiration Time: </span><span class='Expiration_time'>12/16/2022</sapn></p></blockquote>"+
            "<blockquote class='layui-elem-quote'><p><span>Prescription Nickname: </span><span class='Prescriptio_name'>NEW NAME_11</sapn></p></blockquote>"+
            "</div>";
        setTimeout(function(){

            var view_lax = this_viewRx['lax'];
            if(view_lax==0){
                view_lax = "000";
            }else if(view_lax<=9){
                view_lax = ("00"+view_lax);
            }else if(view_lax<=99){
                view_lax = ("0"+view_lax);
            }

            var view_rax= this_viewRx['rax'];
            if(view_rax==0){
                view_rax = "000";
            }else if(view_rax<=9){
                view_rax = ("00"+view_rax);
            }else if(view_rax<=99){
                view_rax = ("0"+view_rax);
            }


            $(".rx_parents_class .lsph").text(this_viewRx['lsph']>0?("+"+this_viewRx['lsph']):this_viewRx['lsph']);
            $(".rx_parents_class .lcyl").text(this_viewRx['lcyl']>0?("+"+this_viewRx['lcyl']):this_viewRx['lcyl']);
            $(".rx_parents_class .lax").text(view_lax);
            $(".rx_parents_class .ladd").text(this_viewRx['ladd']>0?("+"+this_viewRx['ladd']):this_viewRx['ladd']);
            $(".rx_parents_class .rsph").text(this_viewRx['rsph']>0?("+"+this_viewRx['rsph']):this_viewRx['rsph']);
            $(".rx_parents_class .rcyl").text(this_viewRx['rcyl']>0?("+"+this_viewRx['rcyl']):this_viewRx['rcyl']);
            $(".rx_parents_class .rax").text(view_rax);
            $(".rx_parents_class .radd").text(this_viewRx['radd']>0?("+"+this_viewRx['radd']):this_viewRx['radd']);
            $(".rx_parents_class .rpd").text(this_viewRx['rpd']);
            $(".rx_parents_class .rpri").text(this_viewRx['rpri']>0?("+"+this_viewRx['rpri']):this_viewRx['rpri']);
            $(".rx_parents_class .rbase").text(this_viewRx['rbase']);
            $(".rx_parents_class .rpri_1").text(this_viewRx['rpri_1']>0?("+"+this_viewRx['rpri_1']):this_viewRx['rpri_1']);
            $(".rx_parents_class .rbase_1").text(this_viewRx['rbase_1']);
            $(".rx_parents_class .lpri").text(this_viewRx['lpri']>0?("+"+this_viewRx['lpri']):this_viewRx['lpri']);
            $(".rx_parents_class .lbase").text(this_viewRx['lbase']);
            $(".rx_parents_class .lpri_1").text(this_viewRx['lpri_1']>0?("+"+this_viewRx['lpri_1']):this_viewRx['lpri_1']);
            $(".rx_parents_class .lbase_1").text(this_viewRx['lbase_1']);
            if(this_viewRx['exam_date']){
                var new_exam_date = this_viewRx['exam_date'].split("-");
                var year = new_exam_date[0];
                var str_new_exam_date = new_exam_date[1]+"/"+new_exam_date[2]+"/"+year;
                $(".DatePrescriptio_time").text(str_new_exam_date);
            }

            if(this_viewRx['expire_date']) {
                var new_expire_date = this_viewRx['expire_date'].split("-");
                var year = new_expire_date[0];
                var str_new_expire_date = new_expire_date[1] + "/" + new_expire_date[2] + "/" + year;
                $(".Expiration_time").text(str_new_expire_date);
            }
            $(".Prescriptio_name").text(this_viewRx['prescription_name']);
            if(this_viewRx['single_pd']=="1"){
                //单
                $(".rpd_pd").attr("rowspan",2);
                $(".lpd_pd").hide();
                $(".binocular_pd_view").addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                $(".rpd_pd").text(this_viewRx['pd'])
            }else{
                //双
                $(".rpd_pd").removeAttr("rowspan");
                $(".lpd_pd").show();
                $(".monocular_pd_view").addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                $(".rpd").text(this_viewRx['rpd'])
                $(".lpd").text(this_viewRx['lpd'])
            }
        },200);

        var layui_btn_xs_editRx = $(this).siblings(".layui-btn-xs-editRx");

        layer.open({
            type: 1
            ,title: "<p class='title_rx'>View "+viewRx_email+" Prescriptions</p>"
            ,closeBtn: 1,
            shadeClose:true
            ,area: ['98%', '95%']
            ,shade: 0.8
            ,id: 'LAY_layuipro'
            ,btn: ['Confirm','Edit','Cancel']
            ,btnAlign: 'c'
            ,moveType: 0
            ,move:false
            ,content: rx_html
            ,btn1:function(){
                var help_cart_data = {id:help_cart_id};
                $.ajax({
                    type: "POST",
                    data: JSON.stringify(help_cart_data),
                    url:'/webyiiapi/agent/agree-prescription.html',
                    success:function(res) {
                        if(res){
                            res = JSON.parse(res);
                            if(res.code==0){
                                layer.alert('You have confirmed that the current refraction sheet is correct', {
                                    title:"Confirm the current refraction sheet"
                                    ,area: ['520px', 'auto']
                                    ,btn: ['Close']
                                    ,icon: 1
                                });
                                layer.closeAll();
                                init_rx_ajax(current_page_fn(),current_filter_fn());
                            }else{
                                layer.alert('Do you confirm that the current optometry sheet is wrong', {
                                    title:"Confirm the current refraction sheet"
                                    ,area: ['520px', 'auto']
                                    ,btn: ['Close']
                                    ,icon: 5
                                });
                            }
                        }
                    }
                })
            }
            ,btn2:function(index1, layero){
                layer.close(index1);
                setTimeout(function(){
                    $(layui_btn_xs_editRx).trigger('click');
                },200)
            }
        });

    });

    //上传图片点击事件【第一步】
    $(document).on("click","#btn",function () {
        $('#uploadFile').click();
    });

    //上传图片点击事件【第二步】
    $(document).on("change","#uploadFile",function(){
        layer.load(2, {shade: 0.6});
        var source = this;
        var file = source.files[0];
        if(window.FileReader) {
            var fr = new FileReader();
            var portrait = document.getElementById('portrait');
            var portrait_img = document.querySelector('.portrait_img');
            fr.onloadend = function(e) {
                if(edit_rx_img.length){
                    $(edit_rx_img).attr("src",e.target.result)
                }else{
                    portrait.src = e.target.result;
                }
            };


            try{
                fr.readAsDataURL(file);
            }catch(e){
                layer.closeAll('loading');
            }


            //编辑rx 上传
            var edit_rx_img = $(source).parents(".edit_add_img").find(".prescription_img_edit");
            if(!edit_rx_img.length){
                portrait.style.display = 'block';
                portrait_img.style.display = 'block';
            }

            var formData = new FormData();
            formData.append("file",file);
            $("#Loading_Fill").show();
            $.ajax({
                type:"POST",
                url:"/webyiiapi/agent/file.html",
                dataType:"JSON",
                contentType:false,
                data:formData,
                processData:false,
                success:function(res){
                    layer.closeAll('loading');
                    $("#Loading_Fill").hide();
                    if(!edit_rx_img.length){
                        portrait.style.display = 'block';
                        portrait_img.style.display = 'block';
                    }
                    var prescription_img= res.data["prescription_img"];
                    if(res.code==0){
                        $("#uploadFile").attr("src",prescription_img);
                        layer.alert("Upload optometry sheet image successfully",{
                            title:"Upload pictures"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 1
                        });
                    }else{
                        layer.alert("Failed to upload optometry sheet image", {
                            title:"Upload pictures"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }
                },
                beforeSend:function(){

                },
                error:function(err){
                    layer.closeAll('loading');
                    if(edit_rx_img.length){
                        $(edit_rx_img).hide();
                        $(".edit_add_img").find("tip_img").hide();
                    }else{
                        portrait.style.display = 'none';
                        portrait_img.style.display = 'none';
                    }

                    layer.alert("Failed to upload optometry sheet image", {
                        title:"Upload pictures"
                        ,area: ['520px', 'auto']
                        ,btn: ['Close']
                        ,icon: 5
                    });
                }
            });
        }
    });

    // 编辑当前求助人的验光单【点击edit rx按钮】
    $(document).on('click','#layerDemo .layui-btn-xs-editRx',function(){
        var editRx_email = $(this).attr("editRx_email");
        var this_entity_id = $(this).attr("entity_id");
        var editrx_obj = $(this).attr("editrx");
        if(exhibits(editrx_obj)){
            editrx_obj = JSON.parse(editrx_obj);
        }else{
            editrx_obj = {};
        }
        $(this).parents("tr").css("backgroundColor","#f1f16b44").siblings("tr").css("backgroundColor","#fff");
        var current_tr_idx = $(this).parents("tr").index();
        window.localStorage.setItem("current_tr_idx",current_tr_idx);
        var EditRx_html = edit_add_rx("edit",editrx_obj);
        layer.open({
            type: 1
            ,title: "<p class='title_editrx'>Edit "+editRx_email+" Prescriptions</p>"
            ,closeBtn: 1,
            shadeClose:true
            ,area: ['98%', '95%']
            ,shade: 0.8
            ,id: 'LAY_layuipro'
            ,btn: ['Save Prescription','Cancel']
            ,btnAlign: 'c'
            ,moveType: 0
            ,move:false
            ,content:EditRx_html,
            yes:function(){
                // editRx点击Save_Prescription回调函数
                var edit_obj = {
                    this_entity_id:this_entity_id
                };
                var edit_prescription = edit_add_ajax_obj("edit",edit_obj);
                if(edit_prescription){
                    layer.load(2, {shade: 0.6});
                    //编辑验光单发送接口
                    $.ajax({
                        type: "POST",
                        data: JSON.stringify(edit_prescription),
                        url: '/webyiiapi/agent/update-audit-prescription.html',
                        success: function(res) {
                            if(res){
                                res = JSON.parse(res);
                                if(res.code==0){
                                    layer.closeAll();
                                    layer.alert("Edit refraction sheet successfully", {
                                        title:"Edit Prescriptions"
                                        ,area: ['520px', 'auto']
                                        ,btn: ['Close']
                                        ,icon: 1
                                    });
                                    init_rx_ajax(current_page_fn(),current_filter_fn());
                                }else{
                                    layer.alert(res.msg, {
                                        title:"New prescription Tips"
                                        ,area: ['520px', 'auto']
                                        ,btn: ['Close']
                                        ,icon: 5
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });
        // setTimeout(function(){
        //     var rx_height = $(window).height();
        //     $(".title_editrx").parents("#layui-layer1").css("height",(rx_height-30)+'px');
        // },200)
    })

    // 添加当前求助人的验光单【点击add rx按钮】
    $(document).on('click','#layerDemo .layui-btn-xs-add',function(){
        var addRx = $(this).attr("addRx");
        var help_cart_id_add = $(this).attr("help_cart_id_add");
        var addRx_email = $(this).attr("addRx_email");
        $(this).parents("tr").css("backgroundColor","#f1f16b44").siblings("tr").css("backgroundColor","#fff");
        if(exhibits(addRx)){
            addRx = JSON.parse(addRx);
        }else{
            addRx = {};
        }
        var AddRx_html = edit_add_rx("add",addRx);
        var current_tr_idx = $(this).parents("tr").index();
        window.localStorage.setItem("current_tr_idx",current_tr_idx);
        layer.open({
            type: 1
            ,title: "<p class='title_addrx'>Add "+addRx_email+" Prescriptions</p>"
            ,closeBtn: 1,
            shadeClose:true
            ,area: ['98%', '95%']
            ,shade: 0.8
            ,id: 'LAY_layuipro'
            ,btn: ['Save Prescription','Cancel']
            ,btnAlign: 'c'
            ,moveType: 0
            ,move:false
            ,content:AddRx_html,
            yes:function(){
                // add rx点击Save_Prescription回调函数
                var add_obj = {
                    help_cart_id_add:help_cart_id_add
                };
                var add_prescription = edit_add_ajax_obj("add",add_obj);
                if(add_prescription){
                    layer.load(2, {shade: 0.6});
                    //编辑验光单发送接口
                    $.ajax({
                        type: "POST",
                        data: JSON.stringify(add_prescription),
                        url: '/webyiiapi/agent/audit-prescription.html',
                        success: function(res) {
                            if(res){
                                res = JSON.parse(res);
                                if(res.code==0){
                                    layer.closeAll();
                                    layer.alert("Add refraction sheet successfully", {
                                        title:"Add Prescriptions"
                                        ,area: ['520px', 'auto']
                                        ,btn: ['Close']
                                        ,icon: 1
                                    });
                                    init_rx_ajax(current_page_fn(),current_filter_fn());
                                }else{
                                    layer.alert(res.msg, {
                                        title:"New prescription Tips"
                                        ,area: ['520px', 'auto']
                                        ,btn: ['Close']
                                        ,icon: 5
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });

        setTimeout(function(){
            var rx_height = $(window).height();
            $(".title_addrx").parents("#layui-layer1").css("height",(rx_height-30)+'px');
        },200)
    })

    //编辑rx页面，添加rx页面，验光单pd切换 【单】
    $(document).on("click",".binocular_pd",function(){
        $(".pd").attr("rowspan",2);
        $(".pd").show();
        $(".lpd_rpd").hide();
        $(this).addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
        $('.prescription_pd')[0].selectedIndex = 0;
        $('.prescription_rpd')[0].selectedIndex = 0;
        $('.prescription_lpd')[0].selectedIndex = 0;
        $('.prescription_pd,.prescription_rpd,.prescription_lpd').css("borderColor","#ccc");
    });

    //编辑rx页面，添加rx页面，验光单pd切换 【双】
    $(document).on("click",".monocular_pd",function(){
        $(".pd").removeAttr("rowspan");
        $(".pd").hide();
        $(".lpd_rpd").show();
        $(this).addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
        $('.prescription_pd')[0].selectedIndex = 0;
        $('.prescription_rpd')[0].selectedIndex = 0;
        $('.prescription_lpd')[0].selectedIndex = 0;
        $('.prescription_pd,.prescription_rpd,.prescription_lpd').css("borderColor","#ccc");
    });

    //pd select框触发 边框颜色恢复默认
    $(document).on("change",".prescription_pd,.prescription_rpd,.prescription_lpd",function(){
        $('.prescription_pd,.prescription_rpd,.prescription_lpd').css("borderColor","#ccc");
    });

    //Prescription nickname 输入框触发 边框颜色恢复默认
    $(document).on("keyup",".PrescriptionNickname",function(){
        $('.PrescriptionNickname').css("borderColor","#ccc");
    });

    //Add Prescriptions For Customers点击页面里的email输入框触发 边框颜色恢复默认
    $(document).on('keyup','.help_email_ipt',function(){
        $(this).css("borderColor","#ccc");
    });

    //编辑验光单，添加验光单，select触发elect框还原
    $(document).on("change",".editRx_class select",function(){
        $(this).css("borderColor","#ccc");
    });

    //点击send email发送邮件
    $(document).on("click",".btn_send_email",function(){
        $(this).parents("tr").css("backgroundColor","#f1f16b44").siblings("tr").css("backgroundColor","#fff");
        var current_tr_idx = $(this).parents("tr").index();
        window.localStorage.setItem("current_tr_idx",current_tr_idx);
        var help_cart_id_sendEmail = $(this).attr("help_cart_id_sendEmail");
        var send_email_data = {help_cart_id:help_cart_id_sendEmail};
        var sendEmail_val = $(this).attr("sendEmail_val");
        layer.load(2, {shade: 0.6});
        $.ajax({
            type: "POST",
            data: JSON.stringify(send_email_data),
            url:'/webyiiapi/agent/agree-send-email-detail.html',
            success:function(res) {
                layer.closeAll('loading');
                if(exhibits(res)){
                    res = JSON.parse(res);
                }
                if(res.code==0){
                    var email_detail = res["data"]['email_detail'];
                    var send_email_detail = res['data']['send_email_detail'];
                    var callYourDoctor_html = "";
                    var fieldset_html = "";
                    var text_Send_Again = "Send Email"
                    if(send_email_detail.length>0){
                        send_email_detail.forEach(function(val,idx){
                            callYourDoctor_html+="<p class='callYourDoctor'>"+val["created_at"]+"</p>";
                        });
                        fieldset_html = "<fieldset class='layui-elem-field'>"+
                            "<legend class='email_bold'>Previous Records:</legend>"+
                            "<div class='layui-field-box field-box-class'>"+
                            callYourDoctor_html+
                            "</div>"+
                            "</fieldset>";
                        text_Send_Again = "Send Again";
                    }else{
                        fieldset_html="";
                    }

                    var sendEmail_html =
                        "<div class='modal-content modal-content-email' data-role='content'>"+
                        fieldset_html+
                        "<fieldset class='layui-elem-field'>"+
                        "<legend class='email_bold'>Email:</legend>"+
                        "<div class='layui-field-box field-box-class'>"+
                        "<p>"+sendEmail_val+"</p>"+
                        "</div>"+
                        "</fieldset>"+
                        "<fieldset class='layui-elem-field'>"+
                        "<legend class='email_bold'>Subject:</legend>"+
                        "<div class='layui-field-box field-box-class'>"+
                        "<textarea name='' id='' cols='30' rows='10' class='subject_email' style='resize:none;'></textarea>"+
                        "</div>"+
                        "</fieldset>"+
                        "<fieldset class='layui-elem-field'>"+
                        "<legend class='email_bold'>Content:</legend>"+
                        "<div class='layui-field-box field-box-class'>"+
                        "<textarea name='' id='demo_textarea' style='resize:none;display: none;'></textarea>"+
                        "</div>"+
                        "</fieldset>"+
                        "</div>";


                    // var index1_tex = "";
                    setTimeout(function(){
                        $(".subject_email").val("Your prescription has been verified and added to your account");
                        var email_a = "";
                        if(email_detail['help_sku'] && email_detail['product_sku_name']){
                            email_a = "<a href="+email_detail['help_sku']+" class='email_a' style='color:#1e5494;text-decoration: underline'>Order "+email_detail['product_sku_name']+"</a>";
                        }else{
                            if(email_detail['help_sku']){
                                email_a = "<a href="+email_detail['help_sku']+" class='email_a' style='color:#1e5494;text-decoration: underline'>Order</a>";
                            }else if(email_detail['product_sku_name']){
                                email_a = "<a href='/customer/account/auditPrescription' class='email_a' style='color:#1e5494;text-decoration: underline'>Order "+email_detail['product_sku_name']+"</a>";
                            }else{
                                email_a = "<a href='/' class='email_a' style='color:#1e5494;text-decoration: underline'>Order</a>";
                            }
                        }
                        var contenHtml =
                            "<div class='greeting' style='margin-top: 0; margin-bottom: 9px;'>" +
                            "<p style='margin-bottom: 5px'>Hi "+email_detail['prescription_name']+"</p>"+
                            "<P style='margin-bottom: 5px'>Your prescription has been reviewed by one of our opticians and added to your account under person "+email_detail['prescription_name']+"</P>"
                            +email_a+
                            "<P style='margin-top: 5px'>Sincerely"+
                            "<br/>Payne Glasses</P>";
                        "</div>";

                        setTimeout(function(){
                            $("#demo_textarea").val(contenHtml);

                            // var sum_content = content + email_a;
                            var index1_tex  = layedit.build('demo_textarea',{
                                height: 280
                            }); //建立编辑器
                            $(".subject_email").attr("index_ww",index1_tex);
                            $(".layui-layedit-tool").hide();
                        },200);
                    },200);
                    layer.open({
                        type: 1
                        ,title: "<p class='sendEmail_p'>Send email confirmation</p>"
                        ,closeBtn: 1,
                        shadeClose:true
                        ,area: ['90%', '95%']
                        ,shade: 0.8
                        ,id: 'LAY_layuipro'
                        ,btn: [text_Send_Again,'Close']
                        ,btnAlign: 'c'
                        ,moveType: 0
                        ,move:false
                        ,content: sendEmail_html
                        ,yes:function(){
                            layer.load(2, {shade: 0.6});
                            var title_email = $(".subject_email").val();

                            var index_ww = $(".subject_email").attr("index_ww");
                            var content_email =layedit.getContent(index_ww);
                            var send_data = {
                                help_cart_id:help_cart_id_sendEmail,
                                title:title_email,
                                content:content_email
                            };
                            $.ajax({
                                type: "POST",
                                data: JSON.stringify(send_data),
                                url:'/webyiiapi/agent/agree-send-email.html',
                                success:function(res) {
                                    layer.closeAll('loading');
                                    if(res){
                                        if(exhibits(res)){
                                            res = JSON.parse(res);
                                        }
                                        if(res.code==0){
                                            layer.closeAll();
                                            layer.alert(res.msg,{
                                                title:"Confirm sending mail"
                                                ,area: ['520px', 'auto']
                                                ,btn: ['Close']
                                                ,icon: 1
                                            });
                                        }else{
                                            layer.alert(res.msg,{
                                                title:"Confirm sending mail"
                                                ,area: ['520px', 'auto']
                                                ,btn: ['Close']
                                                ,icon: 5
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    });
                }else{
                    layer.alert(res.msg, {
                        title:"Send email confirmation"
                        ,area: ['520px', 'auto']
                        ,btn: ['Close']
                        ,icon: 5
                    });
                }
            }
        })

    })

    //点击Add Prescriptions For Customers添加help用户信息
    $(document).on("click",".add_Customers_click",function(){
        var add_help = view_add_help("add");
        layer.open({
            type: 1
            ,title:"Add user information"
            ,closeBtn: 1,
            shadeClose:true
            ,area: ['98%', '95%']
            ,shade: 0.8
            ,id: 'LAY_layuipro'
            ,btn:['Confirm','Cancel']
            ,btnAlign: 'c'
            ,moveType: 0
            ,move:false
            ,content:add_help
            ,yes:function(){
                var help_profile = $(".help_profile_ipt").val();
                var help_email = $(".help_email_ipt").val();
                var help_sku = $(".help_sku_ipt").val();
                var help_remarks = $(".help_remarks_ipt").val();
                var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                var new_data = {
                    prescription_name:help_profile?help_profile:"",
                    prescription_img:$("#uploadFile").attr("src")?$("#uploadFile").attr("src"):"",
                    description:help_remarks?help_remarks:"",
                    email:help_email,
                    product_sku:help_sku?help_sku:"",
                    service_status:4
                };

                if(email.test(help_email)){
                    $.ajax({
                        url: '/webyiiapi/agent/kf-agent.html',
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(new_data),
                        success:function(res){
                            if(res.code==0){
                                layer.closeAll();
                                layer.alert('User information added successfully', {
                                    title:"Add user information"
                                    ,area: ['520px', 'auto']
                                    ,btn: ['Close']
                                    ,icon: 1
                                });
                                window.localStorage.setItem("current_tr_idx",0);
                                window.localStorage.setItem("Current_page",1);
                                init_rx_ajax(current_page_fn(),current_filter_fn());
                                $('body,html').animate({'scrollTop':0},500);
                            }else{
                                layer.alert(res.msg, {
                                    title:"Add user information"
                                    ,area: ['520px', 'auto']
                                    ,btn: ['Close']
                                    ,icon: 5
                                });
                            }
                        }
                    });
                }else{
                    $(".help_email_ipt").css("borderColor","red");
                    if(help_email){
                        layer.alert('Invalid email address.', {
                            title:"Verify mailbox"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }else{
                        layer.alert('Please enter your email address.', {
                            title:"Verify mailbox"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }
                }

            }
        });

    });

    //筛选点击Confirm按钮
    $(document).on("click",".Confirm_filter",function(){
        var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var filter_email = $(".filter_email").val();
        var filter_profile_name = $(".filter_profile_name").val();
        var filter_status = $('#filter_status').val();
        var filter_date = $('#filter_date option:selected').val();
        if(!(email.test(filter_email))){
            if(filter_email){
                layer.alert('The format of your filtered mailbox is not correct', {
                    title:"Mailbox filtering tips"
                    ,area: ['520px', 'auto']
                    ,btn: ['Close']
                    ,icon: 5
                });
            }else{
                window.localStorage.setItem("Current_page",1);
                init_rx_ajax(1,current_filter_fn());
                layer.load(2, {shade: 0.6});
            }
        }else{
            window.localStorage.setItem("Current_page",1);
            init_rx_ajax(1,current_filter_fn());
            layer.load(2, {shade: 0.6});
        }
    });

    //清空筛选点击Clear All按钮
    $(document).on("click",".Clear_All_filter",function(){
        $(".filter_email").val("");
        $(".filter_profile_name").val("");
        // $('#filter_status')[0].selectedIndex = 0;
        $('#filter_date')[0].selectedIndex = 0;
        $('#Pd_status')[0].selectedIndex = 0;
        $("select[name='Archive_Search']").multipleSelect('setSelects', [0]);
        $("select[name='filter_status']").multipleSelect('setSelects', [0,1,2,3]);
        init_rx_ajax(current_page_fn(),current_filter_fn());
        layer.load(2, {shade: 0.6});
    });



    //点击页数切换函数
    function page_fn(types,current_ys){
        $('body,html').animate({'scrollTop':0},500);
        layer.load(2, {shade: 0.6});
        current_ys = parseInt(current_ys);
        if(types=="next"){
            if(current_ys){
                window.localStorage.setItem("Current_page",current_ys+1);
                window.localStorage.setItem("current_tr_idx","");
                init_rx_ajax(current_ys+1,current_filter_fn());
            }
        }else if(types=="prev"){
            if(current_ys){
                window.localStorage.setItem("Current_page",current_ys-1);
                window.localStorage.setItem("current_tr_idx","");
                init_rx_ajax(current_ys-1,current_filter_fn());
            }
        }else{
            if(current_ys){
                init_rx_ajax(current_ys,current_filter_fn());
                window.localStorage.setItem("Current_page",current_ys);
                init_rx_ajax(current_ys,current_filter_fn());
            }
        }
    }

    //分页点击每个页数切换
    $(document).on("click",".pageBtnWrap a",function(){
        var current_ys = $(this).text();
        if(current_ys=="prev"){
            current_ys = $(this).siblings(".curr").text();
            page_fn("prev",current_ys);
        }else if(current_ys=="next"){
            current_ys = $(this).siblings(".curr").text();
            page_fn("next",current_ys);
        }else if(current_ys){
            window.localStorage.setItem("current_tr_idx","");
            page_fn("",current_ys);
        }

    });


    //点击图片放大事件
    $(document).on('click','.view_help_img,#displayImg,.prescription_img_edit,#portrait',function(){
        var _this = $(this);//将当前的pimg元素作为_this传入函数
        imgShow("#outerdiv", "#innerdiv", "#bigimg", _this);
    });

    //点击查看当前用户组更多信息
    $(document).on("click",".More_click",function(){
        $(".tr_none").remove();
        var more_flag = $(this).attr("more");
        if(more_flag=="true"){
            $(this).find(".child_text").text("view the same user");
            $(this).attr("more",false);
            return false;
        }else{
            $(this).find(".child_text").text("close the same user").parents("tr").siblings("tr").find(".child_text").text("view the same user");
            $(this).attr("more",true).parents("tr").siblings("tr").find(".More_click").attr("more","false");
        }
        var help_cart_id = $(this).attr("help_cart_id");
        var email = $(this).attr("email");
        // var Pd_status = $('#Pd_status option:selected').val();
        var more_data = {
            help_cart_id:help_cart_id,
            email:email,
            // order_by:Pd_status
        };
        var that = this;
        layer.load(2, {shade: 0.6});
        $.ajax({
            method: 'POST',
            url:"/webyiiapi/agent/get-help-detail.html",
            data:JSON.stringify(more_data),
            success: function (res) {
                layer.closeAll('loading');
                if(res.code==0){
                    $(that).attr("more",true);
                    var resData = res.data;
                    var tr_html_help = "";
                    var td_html_help = "";
                    var viewRx = {};
                    resData.forEach(function(val,idx){
                        viewRx["rsph"] = val["rsph"];
                        viewRx["lsph"] = val["lsph"];
                        viewRx["rcyl"] = val["rcyl"];
                        viewRx["lcyl"] = val["lcyl"];
                        viewRx["rax"] = val["rax"];
                        viewRx["lax"] = val["lax"];
                        viewRx["radd"] = val["radd"];
                        viewRx["ladd"] = val["ladd"];
                        viewRx["pd"] = val["pd"];
                        viewRx["lpd"] = val["lpd"];
                        viewRx["rpd"] = val["rpd"];
                        viewRx["rpri"] = val["rpri"];
                        viewRx["lpri"] = val["lpri"];
                        viewRx["rbase"] = val["rbase"];
                        viewRx["lbase"] = val["lbase"];
                        viewRx["rpri_1"] = val["rpri_1"];
                        viewRx["lpri_1"] = val["lpri_1"];
                        viewRx["rbase_1"] = val["rbase_1"];
                        viewRx["lbase_1"] = val["lbase_1"];
                        viewRx["single_pd"] = val["single_pd"];
                        viewRx["prescription_img"] = val["prescription_img"];
                        viewRx["prescription_img"] = val["prescription_img"];
                        viewRx["exam_date"] = val["exam_date"];
                        viewRx["expire_date"] = val["expire_date"];
                        var new_prescription_name = val["prescription_name"];
                        var new_prescription_img = val["prescription_img"];
                        var new_ahc_prescription_img = val["ahc_prescription_img"];
                        if(new_prescription_name){
                            new_prescription_name = new_prescription_name.replace(/"/g,"'");
                            new_prescription_name = new_prescription_name.replace(/ /g,"\n");
                        }else{
                            new_prescription_name = "";
                        }
                        if(new_prescription_img){
                            new_prescription_img = new_prescription_img.replace(/ /g,"%20");
                        }else{
                            new_prescription_img = "";
                        }

                        if(new_ahc_prescription_img){
                            new_ahc_prescription_img = new_ahc_prescription_img.replace(/ /g,"%20");
                        }else{
                            new_ahc_prescription_img = "";
                        }
                        viewRx["prescription_name"] = new_prescription_name;
                        viewRx["prescription_img"] = new_prescription_img;
                        viewRx["ahc_prescription_img"] = new_ahc_prescription_img;
                        var ProfileName = val["help_name"];
                        var Email = val["email"];
                        var sku = val["product_sku"];
                        var Remarks = val["description"];
                        var RequestedDate = val["ahc_created_at"];
                        var CompletedDate = val["ape_created_at"];
                        var prescription_img = val["prescription_img"];
                        var entity_id = val['entity_id'];
                        var help_cart_id = val['id'];
                        if(!CompletedDate){
                            CompletedDate="unfinished"
                        }else{
                            var CompletedDate = CompletedDate.split("-");
                            if(CompletedDate[2]){
                                var new_CompletedDate = CompletedDate[2].split(" ");
                                new_CompletedDate = new_CompletedDate[0];
                            }
                            var year = CompletedDate[0];
                            CompletedDate = CompletedDate[1]+"/"+new_CompletedDate+"/"+year;
                        }
                        var Status = val["prescription_status"];
                        var Status_text="";
                        if(!Status){
                            Status_text = "new"
                        }else if(Status==1){
                            Status_text = "rx added"
                        }else if(Status==2){
                            Status_text = "verified"
                        }else if(Status==3){
                            Status_text = "cust notified"
                        }
                        var a_help_html = "";
                        if(Status<1){
                            a_help_html="<a class='layui-btn layui-btn-sm layui-btn-xs-add' lay-event='detail' addRx_email="+Email+" help_cart_id_add="+help_cart_id+" addRx="+JSON.stringify(viewRx)+">add rx</a>";
                        }
                        if(Status>=1 && Status<=3){
                            a_help_html="<a class='layui-btn layui-btn-sm layui-btn-xs-rx' lay-event='edit' viewRx="+JSON.stringify(viewRx)+" viewRx_email="+Email+" help_cart_id="+help_cart_id+">confirm rx</a><a class='layui-btn layui-btn-normal layui-btn-sm layui-btn-xs-editRx' editRx="+JSON.stringify(viewRx)+" editRx_email="+Email+" entity_id="+entity_id+">edit rx</a>";
                        }
                        if(Status<=3 && Status>=2){
                            a_help_html="<a class='layui-btn layui-btn-sm layui-btn-xs-rx' lay-event='edit' viewRx="+JSON.stringify(viewRx)+" viewRx_email="+Email+" help_cart_id="+help_cart_id+">confirm rx</a><a class='layui-btn layui-btn-normal layui-btn-sm layui-btn-xs-editRx' editRx="+JSON.stringify(viewRx)+" editRx_email="+Email+" entity_id="+entity_id+">edit rx</a><a class='layui-btn layui-btn-warm layui-btn-sm btn_send_email' help_cart_id_sendEmail="+help_cart_id+" sendEmail_val="+Email+">send email</a>";
                        }


                        ProfileName = ProfileName.replace(/"/g, "'");
                        Remarks = Remarks.replace(/"/g,'');
                        Remarks = Remarks.replace(/;/g,'');
                        var viewUser_obj={
                            prescription_img:prescription_img,
                            profileName:ProfileName,
                            email:Email,
                            helpsku:sku,
                            remarks:Remarks
                        };
                        if(RequestedDate){
                            var RequestedDate = RequestedDate.split("-");
                            if(RequestedDate[2]){
                                var new_CompletedDate = RequestedDate[2].split(" ");
                                new_CompletedDate = new_CompletedDate[0];
                            }
                            var year = RequestedDate[0];
                            RequestedDate = RequestedDate[1]+"/"+new_CompletedDate+"/"+year;
                        }
                        var anon_regd = "anon";
                        if(val['is_login']=="1"){
                            anon_regd = "regd";
                        }


                        var pd_states = "";
                        if(val["service_status"]=="1" || val["service_status"]=="4"){
                            pd_states = "Yes"
                        }else{
                            if(val["help_pd"]=="1"){
                                pd_states = "Yes"
                            }else if(val["help_pd"]=="2"){
                                pd_states = "No"
                            }else{
                                pd_states = "Never"
                            }
                        }

                        var acknowledging_time = val['acknowledging_time'];
                        var send_created_time = val['send_created_time'];
                        if(acknowledging_time){
                            var acknowledging_time = acknowledging_time.split("-");
                            if(acknowledging_time[2]){
                                var new_acknowledging_time = acknowledging_time[2].split(" ");
                                new_acknowledging_time = new_acknowledging_time[0];
                            }
                            var year = acknowledging_time[0];
                            if(!parseInt(year)){
                                acknowledging_time = "unfinished"
                            }else{
                                acknowledging_time = acknowledging_time[1]+"/"+new_acknowledging_time+"/"+year;
                            }
                        }else{
                            acknowledging_time = "unfinished"
                        }

                        if(send_created_time){
                            var send_created_time = send_created_time.split("-");
                            if(acknowledging_time[2]){
                                var new_send_created_time = send_created_time[2].split(" ");
                                new_send_created_time = new_send_created_time[0];
                            }
                            var year = send_created_time[0];
                            if(!parseInt(year)){
                                send_created_time = "unfinished"
                            }else{
                                send_created_time = send_created_time[1]+"/"+new_send_created_time+"/"+year;
                            }
                        }else{
                            send_created_time = "unfinished"
                        }

                        function getDistanceSpecifiedTime(dateTime) {
                            // 指定日期和时间
                            var EndTime = new Date(dateTime);
                            // 当前系统时间
                            var NowTime = new Date();
                            var t = EndTime.getTime() - NowTime.getTime();
                            var d = Math.floor(t / 1000 / 60 / 60 / 24);
                            var h = Math.floor(t / 1000 / 60 / 60 % 24);
                            var m = Math.floor(t / 1000 / 60 % 60);
                            var s = Math.floor(t / 1000 % 60);
                            return d;
                        }
                        var jl_ts = getDistanceSpecifiedTime(RequestedDate);
                        jl_ts=Math.abs(jl_ts);
                        var gd_a_click = "";
                        //归档
                        var pigeonhole = val['pigeonhole'];
                        if(pigeonhole=="0"){
                            if(jl_ts>=7){
                                gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Archive</a>"
                            }else{
                                gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm layui-btn-disabled'>Archive</a>"
                            }

                        } else if(pigeonhole=="1"){
                            gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Lack of PD</a>"
                        } else if(pigeonhole=="2"){
                            gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Lack of prescription photos</a>"
                        } else if(pigeonhole=="3"){
                            gd_a_click = "<a class='layui-btn layui-btn-primary layui-btn-sm pigeonhole_click' pigeonhole="+pigeonhole+" help_cart_id="+help_cart_id+">Email sent</a>";
                        }
                        td_html_help = "<td>"+gd_a_click+"</td><td width='200px'>"+anon_regd+": "+ProfileName+"</td><td style='color:#000;background:rgba(241, 241, 107, 0.267)'><a class='email_more'>"+Email+"</a></td><td>"+sku+"</td><td>"+pd_states+"</td><td>"+RequestedDate+"</td><td>"+CompletedDate+"</td><td>"+acknowledging_time+"</td><td>"+send_created_time+"</td><td>"+Status_text+"</td><td><a class='layui-btn layui-btn-primary layui-btn-sm layui-btn-xs-view' lay-event='detail' viewUser_obj='"+JSON.stringify(viewUser_obj)+"'>view</a>"+a_help_html+"</td>";
                        tr_html_help+="<tr class='tr_none'>"+td_html_help+"</tr>";
                    });
                    $(that).parents("tr").find("td").eq(1).css("color","#000").parents("tr").siblings("tr").find("td").css("color","#666");
                    $(that).parents("tr").find("td").eq(1).css("background","rgba(241, 241, 107, 0.267)").parents("tr").siblings("tr").find("td").css("background","#fff");
                    $(that).parents("tr").after(tr_html_help);
                }else{
                    layer.alert("Failed to view user groups", {
                        title:"View user groups"
                        ,area: ['520px', 'auto']
                        ,btn: ['Close']
                        ,icon: 5
                    });
                }
            },error:function(){
                layer.alert("Failed to view user groups", {
                    title:"View user groups"
                    ,area: ['520px', 'auto']
                    ,btn: ['Close']
                    ,icon: 5
                });
            }
        });
    });


    //排序
    $(document).on("click",".sort i",function(){
        $(this).addClass("layui-table-sort-asc_active").siblings("i").removeClass("layui-table-sort-asc_active");
        $(this).addClass("layui-table-sort-asc_active").parents("th").siblings("th").find("i").removeClass("layui-table-sort-asc_active");
        var state = $(this).attr("state");
        var state_val = $(this).attr("state_val");
        $("#myCheck").prop("checked",false);
        $(".same_text").text("view the same user");
        $(".sort_jl").attr("state",state);
        $(".sort_jl").attr("state_val",state_val);
        layer.load(2, {shade: 0.6});
        window.localStorage.setItem("Current_page",1);
        init_rx_ajax(current_page_fn(),current_filter_fn());
    });


    //开启用户组
    $(document).on("click","#myCheck",function(){
        layer.load(2, {shade: 0.6});
        var is_cheked = $(this).is(":checked");
        if(is_cheked){
            $(".same_text").text("view the same user");
            $(".sort i").removeClass("layui-table-sort-asc_active");
            $(".sort_jl").attr("state","");
            $(".sort_jl").attr("state_val","");
        }else{
            $(".same_text").text("view the same user");
        }
        window.localStorage.setItem("Current_page",1);
        init_rx_ajax(current_page_fn(),current_filter_fn());
    })

    //点击查看当前用户更多信息
    $(document).on("click",".email_more",function(){
        var email_text = $(this).text();
        // 获得frame索引
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
        //关闭当前frame
        parent.xadmin.add_tab('User Details','/manager/customerhelp/userdetails/index.html?email='+email_text,true);
    });


    //点击按钮弹出归档选项
    $(document).on("click",".pigeonhole_click",function(){
        var help_cart_id = $(this).attr("help_cart_id");
        var this_pigeonhole = $(this).attr("pigeonhole");
        $("#help_html").attr("pigeonhole",this_pigeonhole);

        var lin_gd_html = "<div pigeonhole='0'>"+
                                "<input type='radio' name='archive' value='archive' title='archive'>"+
                                        "Archive"+
                                "</div>";

        if(this_pigeonhole=="0"){
            lin_gd_html = "";
        }

        var gd_html = "<div class='layui-form-item gd_parents_box'>"+
            "<div class='layui-input-block readio_gd'>"
                +lin_gd_html+
            "<div pigeonhole='1'>"+
            "<input type='radio' name='Lack of PD' value='Lack of PD' title='Lack of PD'>"+
            "Lack of PD"+
            "</div>"+
            "<div pigeonhole='2'>"+
            "<input type='radio' name='Lack of prescription photos' value='Lack of prescription photos' title='Lack of prescription photos'>"+
            "Lack of prescription photos"+
            "</div>"+
            "<div pigeonhole='3'>"+
            "<input type='radio' name='Email sent' value='Email sent' title='Email sent'>"+
            "Email sent"+
            "</div>"+
            "</div>"+
            "</div>";


        var flag_Confirm = true;
        setTimeout(function(){
            $(".readio_gd div").each(function(idx,ele){
                var pigeonhole_idx = $(ele).attr("pigeonhole");
                if(pigeonhole_idx == this_pigeonhole){
                    $(ele).find("input").prop('checked',true);
                    flag_Confirm = false;
                }
            });
        },200);


        layui_close_gd = layer.open({
            type: 1
            ,title: "<p class='Archive_parent'>Archive selection</p>"
            ,closeBtn: 1,
            shadeClose:true
            ,area: ['500px', '300px']
            ,shade: 0.8
            ,id: 'LAY_layuiproww'
            ,btn: ['Confirm']
            ,shadeClose:false
            ,btnAlign: 'c'
            ,moveType: 0
            ,move:false
            ,content: gd_html,
            yes:function(){
                layer.load(2, {shade: 0.6});
                var this_pigeonhole = $(".pigeonhole_click").attr("pigeonhole");
                var data_gd_s = {
                    'help_cart_id':help_cart_id,
                    'pigeonhole':this_pigeonhole
                };
                $.ajax({
                    type: "POST",
                    url: "/webyiiapi/agent/pigeonhole.html",
                    data: JSON.stringify(data_gd_s),
                    success: function (res) {
                        layer.closeAll('loading');
                        if(exhibits(res)){
                            res = JSON.parse(res);
                        }
                        if(res.code==0){
                            init_rx_ajax(current_page_fn(),current_filter_fn());
                            layer.close(layui_close_gd);
                        }else{
                            layer.alert(res.msg, {
                                title: "Archive selection error"
                                , area: ['520px', 'auto']
                                , btn: ['Close']
                                , icon: 5
                            });
                        }
                    }
                    , error: function (err) {
                        layer.closeAll('loading');
                        layer.alert("Server internal error", {
                            title: "Archive selection error"
                            , area: ['520px', 'auto']
                            , btn: ['Close']
                            , icon: 5
                        });

                    }
                })
            },
            cancel: function(){
                var frist_num = $("#help_html").attr("pigeonhole");
                $(".pigeonhole_click").attr("pigeonhole",frist_num);
            }


        });


        setTimeout(function(){
            if(flag_Confirm){
                $(".Archive_parent").parents(".layui-layer-page").find(".layui-layer-btn0").addClass("gd_disabled");
            }
        },210);
    });

    $(document).on("click",".readio_gd>div",function(){
        var this_pigeonhole = $(this).attr("pigeonhole");
            $(this).find("input").prop('checked',true).parents(".readio_gd>div").siblings('.readio_gd>div').find("input").prop('checked',false);
            $(".pigeonhole_click").attr("pigeonhole",this_pigeonhole);
            $(".Archive_parent").parents(".layui-layer-page").find(".layui-layer-btn0").removeClass("gd_disabled");
    });







});
