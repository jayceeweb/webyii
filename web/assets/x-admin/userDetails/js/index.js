
layui.use(['layer','layedit'],function() {
        var layedit = layui.layedit;


    //获取指定url中的参数值

    function getvar(url,par) {
        try{
            var urlsearch = url.split('?');
            var pstr = urlsearch[1].split('&');
            for (var i = pstr.length - 1; i >= 0; i--) {
                var tep = pstr[i].split("=");
                if (tep[0] == par) {
                    return tep[1];
                }
            }
            return (false);
        }catch(e){
            return (false);
        }

    }
    //end




        //点击图片放大函数：
        function imgShow(outerdiv, innerdiv, bigimg, _this){
            var src = _this.attr("src");//获取当前点击的pimg元素中的src属性
            $(bigimg).attr("src", src);//设置#bigimg元素的src属性
            /*获取当前点击图片的真实大小，并显示弹出层及大图*/
            $("<img/>").attr("src", src).load(function(){
                var windowW = $(window).width();//获取当前窗口宽度
                var windowH = $(window).height();//获取当前窗口高度
                var realWidth = this.width;//获取图片真实宽度
                var realHeight = this.height;//获取图片真实高度
                var imgWidth, imgHeight;
                var scale = 0.8;//缩放尺寸，当图片真实宽度和高度大于窗口宽度和高度时进行缩放
                if(realHeight>windowH*scale) {//判断图片高度
                    imgHeight = windowH*scale;//如大于窗口高度，图片高度进行缩放
                    imgWidth = imgHeight/realHeight*realWidth;//等比例缩放宽度
                    if(imgWidth>windowW*scale) {//如宽度扔大于窗口宽度
                        imgWidth = windowW*scale;//再对宽度进行缩放
                    }
                } else if(realWidth>windowW*scale) {//如图片高度合适，判断图片宽度
                    imgWidth = windowW*scale;//如大于窗口宽度，图片宽度进行缩放
                    imgHeight = imgWidth/realWidth*realHeight;//等比例缩放高度
                } else {//如果图片真实高度和宽度都符合要求，高宽不变
                    imgWidth = realWidth;
                    imgHeight = realHeight;
                }
                $(bigimg).css("width",imgWidth);//以最终的宽度对图片缩放
                var w = (windowW-imgWidth)/2;//计算图片与窗口左边距
                var h = (windowH-imgHeight)/2;//计算图片与窗口上边距
                $(innerdiv).css({"top":h, "left":w});//设置#innerdiv的top和left属性
                $(outerdiv).fadeIn("fast");//淡入显示#outerdiv及.pimg
            });
            $(outerdiv).click(function(){//再次点击淡出消失弹出层
                $(this).fadeOut("fast");
            });
        }
        //日期转换函数 【带有时分秒的】
        function date_fn(date_s){
            if(date_s) {
                var new_created_at = date_s.split("-");
                var year = new_created_at[0];
                var yue = new_created_at[1];
                var ri = new_created_at[2];
                ri=ri.split(" ");
                ri = ri[0];
                var str_new_created_at = yue + "/" + ri + "/" + year;
               return str_new_created_at;
            }else{
                return date_s;
            }
        }
        //日期转换函数 【不带有时分秒的】
        function date_no_fn(date_s){
            if(date_s) {
                var new_created_at = date_s.split("-");
                var year = new_created_at[0];
                var yue = new_created_at[1];
                var ri = new_created_at[2];
                var str_new_created_at = yue + "/" + ri + "/" + year;
                return str_new_created_at;
            }else{
                return date_s;
            }
        }
        //默认展示用户ajax
        function init_customer_data(page,filter_val){
            layer.load(3, {shade: 0.7});
            var datas = {
                page:page,
                email:filter_val['email'],
                increment_id:filter_val['increment_id']
            };
            $.ajax({
                method: 'POST',
                url:'/webyiiapi/customer-detail/customer.html',
                data:JSON.stringify(datas),
                success : function(res) {
                    if(res.code==1) {
                        $("#layerDemo").show();
                        var rows = res['data']['rows'];
                        var count = res['data']['count'];
                        layer.closeAll('loading');
                        var tr_html_help = "";
                        var td_html_help = "";

                        if (rows.length <= 0) {
                            $(".parents_ys").hide();
                            layer.alert('The user list of the current query is empty.', {
                                title:"Tips"
                                ,area: ['520px', 'auto']
                                ,btn: ['Close']
                                ,icon: 5
                            });

                        } else {
                            $(".parents_ys").show();
                            $(".Current_number").text(rows.length);
                            $(".Pages_Current").text(page);
                            $(".PageCount").text(count);
                        }
                        rows.forEach(function (val, idx) {
                            val['created_at'] = date_fn(val['created_at']);
                            td_html_help = "<td width='200px'>" + val['name'] + "</td><td>" + val['email'] + "</td><td>" + val['created_at'] + "</td>" +
                                "<td>" +
                                "<a class='layui-btn layui-btn-xs View_Cart' customer_id=" + val['entity_id'] + " current_email=" + val['email'] + ">View Cart</a>" +
                                "<a class='layui-btn layui-btn-normal layui-btn-xs View_Order' customer_id=" + val['entity_id'] + " current_email=" + val['email'] + ">View Order</a>" +
                                "<a class='layui-btn layui-btn-warm layui-btn-xs View_Profile' customer_id=" + val['entity_id'] + " current_email=" + val['email'] + ">View Profile</a>" +
                                "<a class='layui-btn layui-btn-xs View_Prescription' customer_id=" + val['entity_id'] + " current_email=" + val['email'] + ">View Prescription</a>" +
                                "</td>"
                            tr_html_help += "<tr>" + td_html_help + "</tr>";
                        });
                        $("#help_html").html(tr_html_help);

                        setTimeout(function () {
                            $(".View_Profile").each(function (idx, ele) {
                                $(ele).attr("ajax_event", rows[idx]["name"]);
                                if (rows[idx]["profile_num"] <= 0) {
                                    $(ele).addClass("layui-btn-disabled");
                                }
                                $(ele).attr('num', rows[idx]["profile_num"]);
                                $(ele).append("<span class='layui-badge num_user'>" + rows[idx]["profile_num"] + "</span>");
                            });
                            $(".View_Prescription").each(function (idx, ele) {
                                $(ele).attr("ajax_event", rows[idx]["name"]);
                                if (rows[idx]["Prescription_num"] <= 0) {
                                    $(ele).addClass("layui-btn-disabled");
                                }
                                $(ele).attr('num', rows[idx]["Prescription_num"]);
                                $(ele).append("<span class='layui-badge num_user'>" + rows[idx]["Prescription_num"] + "</span>");
                            });
                            $(".View_Order").each(function (idx, ele) {
                                $(ele).attr("ajax_event", rows[idx]["name"]);
                                if (rows[idx]["order_num"] <= 0) {
                                    $(ele).addClass("layui-btn-disabled");
                                }
                                $(ele).attr('num', rows[idx]["order_num"]);
                                $(ele).append("<span class='layui-badge num_user'>" + rows[idx]["order_num"] + "</span>");
                            });
                            $(".View_Cart").each(function (idx, ele) {
                                $(ele).attr("ajax_event", rows[idx]["name"]);
                                if (rows[idx]["cart_num"] <= 0) {
                                    $(ele).addClass("layui-btn-disabled");
                                }
                                $(ele).attr('num', rows[idx]["cart_num"]);
                                $(ele).append("<span class='layui-badge num_user'>" + rows[idx]["cart_num"] + "</span>");
                            });
                        }, 200);

                        if (count >= 2) {
                            $("#kkpager").show();
                            kkpager.generPageHtml({
                                //当前页数
                                pno: page,
                                //总页数
                                total: count,
                                mode: 'click',//默认值是link，可选link或者click
                                click: function (n) {
                                    this.selectPage(n);
                                    return false;
                                }
                            },true);
                        } else {
                            $("#kkpager").hide();
                        }
                    }
                },error:function(){
                    layer.closeAll('loading');
                    layer.alert('An internal server error occurred.', {
                        title:"server"
                        ,area: ['520px', 'auto']
                        ,btn: ['Close']
                        ,icon: 5
                    });
                }
            });
        }
        function current_filter_fn(){
            var email_filter = $(".email_filter").val();
            var email_orderId = $(".email_orderId").val();
            return {
                email:email_filter,
                increment_id:email_orderId
            }
        }

        var current_href = window.location.href;
        var current_email = getvar(current_href+"&","email");
        if(current_email){
            $(".email_filter").val(current_email);
        }
        init_customer_data(1,current_filter_fn());
        //查看profile
        $(document).on("click",".View_Profile",function(){
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            var customer_id = $(this).attr("customer_id");
            var ajax_event = $(this).attr("ajax_event");
            var email_parent = $(this).attr("current_email");
            var current_width = $(window).width();
            current_width-=50;
            var num = current_width/6;
            var active_width = "width:"+num+"px;display:inline-block";
            var parent_width = "width:"+current_width+"px;margin:auto;";
            var data_customer_id = {customer_id:customer_id};
            var num = $(this).attr("num");
            if(num){
                num  = parseInt(num);
                if(num<=0){
                    return false;
                }
            }
            layer.load(2, {shade: 0.6});
            $.ajax({
                url: "/webyiiapi/customer-detail/profile.html",
                type: 'POST',
                dataType: 'json',
                data:JSON.stringify(data_customer_id),
                success:function(res){
                    layer.closeAll('loading');
                    if(res.code==1){
                        layer.closeAll('loading');
                        var data = res['data']['rows'];
                        var frist_profile  = "";
                        var tr_profile = "";
                        var td_profile = "";
                        data.forEach(function(val,idx){
                            frist_profile = "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['profile_id']+"</div>" +
                                "</td>";
                            val['created_at'] = date_fn(val['created_at']);

                            var date=new Date;
                            var y = date.getFullYear();

                            if(!val['birthyear'] || val['birthyear']=="0"){
                                val['birthyear'] = y;
                            }

                            if(val['gender']=="1" || (!val['birthyear'])){
                                val['gender'] = "Male"
                            }else{
                                val['gender'] = "Female"
                            }
                            td_profile= frist_profile+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell nickname_more'>"+val['nickname']+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['birthyear']+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['gender']+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['created_at']+"</div>" +
                                "</td>";
                            var Prescription_num_html = "";

                            var new_nickname = encodeURI(val['nickname']);

                            if(val["Prescription_num"]<=0){
                                Prescription_num_html = "<a class='layui-btn layui-btn-warm layui-btn-xs profile_View_Prescription layui-btn-disabled' >View Prescription<span class='layui-badge num_user'>"+(val["Prescription_num"])+"</span></a>";
                            }else{
                                Prescription_num_html = "<a class='layui-btn layui-btn-warm layui-btn-xs profile_View_Prescription' nickname="+new_nickname+" profile_id="+val['profile_id']+" current_email="+email_parent+">View Prescription<span class='layui-badge num_user'>"+(val["Prescription_num"])+"</span></a>";
                            }
                            tr_profile+="<tr data-index='0' class='parent_prescript'>"+td_profile+"" +
                                "<td align='center' style="+active_width+">"+
                                "<div class='layui-table-cell'>"
                                +Prescription_num_html+
                                "<div>"+
                                "</td>";
                            "</tr>";
                        });
                        var profile_html =
                            "<div  class='layui-table-cell profile_color' style="+parent_width+">" +
                            "<div class='layui-form layui-border-box' lay-filter='LAY-table-2' style=''>" +
                            "<div class=''>" +
                            "<div class='layui-table-header'>" +
                            "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>" +
                            "<thead>" +
                            "<tr class=''>" +
                            "<th data-field='profileId' data-key='2-0-0' class='' style="+active_width+">" +
                            "<div class='layui-table-cell laytable-cell-2-0-0'>" +
                            "<span>ID</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Nickname' data-key='2-0-1' class='' style="+active_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Profile Name</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Birthyear' data-key='2-0-2' class='' style="+active_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Birthyear</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Gender' data-key='2-0-3' class='' style="+active_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Gender</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='createTime' data-key='2-0-4' class='' style="+active_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Created Date</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='5' data-key='2-0-5' class=' layui-table-col-special' style="+active_width+">" +
                            "<div class='layui-table-cell' align='center'>" +
                            "<span>Action</span>" +
                            "</div>" +
                            "</th>" +
                            "</tr>" +
                            "</thead>" +
                            "</table>" +
                            "</div>" +
                            "<div class='layui-table-body layui-table-main' id='over_hide'>" +
                            "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>"+
                            "<tbody>" +
                            tr_profile+
                            "</tbody>" +
                            "</table>"+
                            "</div>"+
                            "</div>"+
                            "</div>"+
                            "</div>";

                        layer.open({
                            type: 1
                            , title: "<p class='sendEmail_p'>"+email_parent+" All Profile</p>"
                            , closeBtn: 1,
                            shadeClose: true
                            , area: ["99%", '95%']
                            , shade: 0.8
                            , id: 'LAY_layuipro'
                            , btnAlign: 'c'
                            , moveType: 0
                            , move: false
                            ,btn:["close"]
                            , content:profile_html
                        })
                    }
                }
            });
        });
        //查看验光单
        $(document).on("click",".View_Prescription",function(){
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            var customer_id = $(this).attr("customer_id");
            var email_parent = $(this).attr("current_email");
            var ajax_event = $(this).attr("ajax_event");
            var current_width = $(window).width();
            current_width-=100;
            var num = current_width/6;
            var active_width = "width:"+num+"px;display:inline-block";
            var parent_width = "width:"+current_width+"px;margin:auto;";
            var num = $(this).attr("num");
            if(num){
                num  = parseInt(num);
                if(num<=0){
                    return false;
                }
            }
            layer.load(2, {shade: 0.6});
            var data_customer_id = {customer_id:customer_id};
            $.ajax({
                url: "/webyiiapi/customer-detail/customer-prescription.html",
                type: 'POST',
                dataType: 'json',
                data:JSON.stringify(data_customer_id),
                success:function(res){
                    layer.closeAll('loading');
                    if(res.code==1){
                        var data_s= res['data'];
                        var prescription_data = [];
                        for(k in data_s){
                            var new_data_s = data_s[k];
                            for(k1 in new_data_s){
                                prescription_data.push(new_data_s[k1])
                            }
                        }
                        var frist_profile  = "";
                        var tr_profile = "";
                        var td_profile = "";
                        prescription_data.forEach(function(val,idx){
                            var prescription_data_arr = [];
                            var prescription_name = val['prescription_name'];
                            var nickname = val['nickname'];
                             if (escape(prescription_name).indexOf( "%u" )<0){
                               }else {
                                 val['prescription_name'] = encodeURI(val['prescription_name']);
                                }
                            if (escape(nickname).indexOf( "%u" )<0){
                            }else {
                                val['nickname'] = encodeURI(val['nickname']);
                            }
                            prescription_data_arr.push(val);
                            var prescription_data_arr_s = JSON.stringify(prescription_data_arr);
                            prescription_data_arr_s = btoa(prescription_data_arr_s);
                            frist_profile = "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['entity_id']+"</div>" +
                                "</td>";
                            val['exam_date'] = date_no_fn(val['exam_date']);
                            val['expire_date'] = date_no_fn(val['expire_date']);
                            var prescription_name = val['prescription_name'];
                            var nickname = val['nickname'];
                            val['prescription_name'] = decodeURI(prescription_name);
                            val['nickname'] = decodeURI(nickname);
                            td_profile= frist_profile+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['prescription_name']+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['nickname']+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['exam_date']+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+active_width+">" +
                                "<div class='layui-table-cell'>"+val['expire_date']+"</div>" +
                                "</td>";
                            tr_profile+="<tr data-index='0' class='parent_order'>"+td_profile+"" +
                                "<td align='center' style="+active_width+">"+
                                "<div class='layui-table-cell'>"+
                                "<a class='layui-btn layui-btn-primary layui-btn-xs Prescription_view_click' prescriptiondata="+prescription_data_arr_s+" current_profile="+val['nickname']+" current_email="+email_parent+">View</a>"+
                                "<div>"+
                                "</td>";
                            "</tr>";

                        });
                        var profile_html = "<tr data-index='undefined-0-children' style='display: table-row;' class='prescript_parent'>" +
                            "<td colspan='6'>" +
                            "<div  class='layui-table-cell prescript_color' style="+parent_width+">" +
                            "<div class='layui-form layui-border-box ' lay-filter='LAY-table-2' style=''>" +
                            "<div class=''>" +
                            "<div class='layui-table-header'>" +
                            "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>" +
                            "<thead>" +
                            "<tr class=''>" +
                            "<th data-field='profileId' data-key='2-0-0' class='' style="+active_width+">" +
                            "<div class='layui-table-cell laytable-cell-2-0-0'>" +
                            "<span>Prescription Id</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Nickname' data-key='2-0-1' class='' style="+active_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Prescription Name</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Birthyear' data-key='2-0-2' class=''  style="+active_width+">" +
                            "<div class='layui-table-cell' >" +
                            "<span>Profile Name</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Gender' data-key='2-0-3' class=''  style="+active_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Date Of Prescription</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='createTime' data-key='2-0-4' class=''  style="+active_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Expired Date</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='5' data-key='2-0-5' class=' layui-table-col-special'  style="+active_width+">" +
                            "<div class='layui-table-cell' align='center'>" +
                            "<span>Action</span>" +
                            "</div>" +
                            "</th>" +
                            "</tr>" +
                            "</thead>" +
                            "</table>" +
                            "</div>" +
                            "<div class='layui-table-body layui-table-main' id='over_hide'>" +
                            "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>"+
                            "<tbody>" +
                            tr_profile+
                            "</tbody>" +
                            "</table>"+
                            "</div>"+
                            "</div>"+
                            "</div>"+
                            "</div>";

                            layer.open({
                                type: 1
                                , title: "<p class='sendEmail_p'>"+email_parent+" All Prescription</p>"
                                , closeBtn: 1,
                                shadeClose: true
                                , area: ["99%", '95%']
                                , shade: 0.8
                                , id: 'LAY_layuipro'
                                , btnAlign: 'c'
                                , moveType: 0
                                , move: false
                                ,btn:["close"]
                                , content:profile_html
                            })
                        }
                }
            });

        });
        //查看订单
        $(document).on("click",".View_Order",function(){
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            var customer_id = $(this).attr("customer_id");
            var ajax_event = $(this).attr("ajax_event");
            var email_parent = $(this).attr("current_email");
            var current_width = $(window).width();
            current_width-=100;
            var order_num = current_width/5;
            var val_width = "width:"+order_num+"px;display:inline-block";
            var parent_width = "width:"+current_width+"px;margin:auto;";
            var data_customer_id = {customer_id:customer_id};
            var num = $(this).attr("num");
            if(num){
                num  = parseInt(num);
                if(num<=0){
                    return false;
                }
            }
            layer.load(2, {shade: 0.6});
            $.ajax({
                url: "/webyiiapi/customer-detail/customer-all-order.html",
                type: 'POST',
                dataType: 'json',
                data:JSON.stringify(data_customer_id),
                success:function(res){
                    layer.closeAll('loading');
                    if(res.code==1){
                        var order_data = res['data'];
                        var sum_profile = "";
                        var frist_profile  = "";
                        var tr_profile = "";
                        var td_profile = "";
                        order_data.forEach(function(val,idx){
                            var data_obj_order = {
                                subtotal:val['subtotal'],
                                shippingAmount:val['shipping_amount'],
                                hasWarranty:val['has_warranty'],
                                warranty:val['warranty'],
                                grandTotal:val['grand_total'],
                                pointPrice:val['point_price'],
                                pointAmount:val['point_amount'],
                                gavingScore:val['gaving_score'],
                                discountAmount:val['discount_amount'],
                                discountDescription:val['discount_description']
                            };
                            data_obj_order = JSON.stringify(data_obj_order);

                            data_obj_order = encodeURI(data_obj_order);

                            frist_profile = "<td data-field='' data-key='2-0-1' class='' style="+val_width+">" +
                                "<div class='layui-table-cell'>"+val['increment_id']+"</div>" +
                                "</td>";

                            val['created_at'] = date_fn(val['created_at']);

                            td_profile= frist_profile+
                                "<td data-field='' data-key='2-0-1' class='' style="+val_width+">" +
                                "<div class='layui-table-cell'>"+parseInt(val['total_qty_ordered'])+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+val_width+">" +
                                "<div class='layui-table-cell'>$"+(parseFloat(val['grand_total']).toFixed(2))+"</div>" +
                                "</td>"+
                                "<td data-field='' data-key='2-0-1' class='' style="+val_width+">" +
                                "<div class='layui-table-cell'>"+val['created_at']+"</div>" +
                                "</td>"

                            tr_profile+="<tr data-index='0' class=''>"+td_profile+"" +
                                "<td align='center' style="+val_width+">"+
                                "<div class='layui-table-cell'>"+
                                "<a class='layui-btn layui-btn-normal layui-btn-xs order_click' order_id="+val['entity_id']+" current_email="+email_parent+" orderId="+val['increment_id']+" data_obj_order="+data_obj_order+">View</a>"+
                                "<div>"+
                                "</td>";
                            "</tr>";
                        });
                        var profile_html = "<tr data-index='undefined-0-children' style='display: table-row;' class='order_parent'>" +
                            "<td colspan='6'>" +
                            "<div class='layui-table-cell order_cart_color' style="+parent_width+">" +
                            "<div class='layui-form layui-border-box ' lay-filter='LAY-table-2' style=''>" +
                            "<div class=''>" +
                            "<div class='layui-table-header'>" +
                            "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>" +
                            "<thead>" +
                            "<tr class=''>" +
                            "<th data-field='profileId' data-key='2-0-0' class='' style="+val_width+">" +
                            "<div class='layui-table-cell laytable-cell-2-0-0'>" +
                            "<span>Order Number</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Birthyear' data-key='2-0-2' class=''  style="+val_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Quantity</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='Gender' data-key='2-0-3' class='' style="+val_width+">" +
                            "<div class='layui-table-cell' >" +
                            "<span>Subtotal</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='createTime' data-key='2-0-4' class='' style="+val_width+">" +
                            "<div class='layui-table-cell'>" +
                            "<span>Created Date</span>" +
                            "</div>" +
                            "</th>" +
                            "<th data-field='5' data-key='2-0-5' class=' layui-table-col-special' style="+val_width+">" +
                            "<div class='layui-table-cell' align='center' >" +
                            "<span>Action</span>" +
                            "</div>" +
                            "</th>" +
                            "</tr>" +
                            "</thead>" +
                            "</table>" +
                            "</div>" +
                            "<div class='layui-table-body layui-table-main' id='over_hide'>" +
                            "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>"+
                            "<tbody>" +
                            tr_profile+
                            "</tbody>" +
                            "</table>"+
                            "</div>"+
                            "</div>"+
                            "</div>"+
                            "</div>";
                        layer.open({
                            type: 1
                            , title: "<p class='sendEmail_p'>"+email_parent+" All Order</p>"
                            , closeBtn: 1,
                            shadeClose: true
                            , area: ["99%", '95%']
                            , shade: 0.8
                            , id: 'LAY_layuipro'
                            , btnAlign: 'c'
                            , moveType: 0
                            , move: false
                            ,btn:["close"]
                            , content:profile_html
                        })
                    }
                }
            });
        });
        //查看购物车
        $(document).on("click",".View_Cart",function(){
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            var customer_id = $(this).attr("customer_id");
            var email_parent = $(this).attr("current_email");
            var current_width = $(window).width();
            current_width-=100;
            var cart_num = current_width/7;
            var cart_val_width = "width:"+cart_num+"px;display:inline-block";
            var parent_width = "width:"+current_width+"px;margin:auto;";
            var data_customer_id = {customer_id:customer_id};
            var num = $(this).attr("num");
            if(num){
                num  = parseInt(num);
                if(num<=0){
                    return false;
                }
            }
            layer.load(2, {shade: 0.6});
            $.ajax({
                url: "/webyiiapi/customer-detail/customer-all-cart.html",
                type: 'POST',
                dataType: 'json',
                data:JSON.stringify(data_customer_id),
                success:function(res){
                    layer.closeAll('loading');
                    var cart_data = res['data'];
                    var frist_profile  = "";
                    var tr_profile = "";
                    var td_profile = "";
                    cart_data.forEach(function(val,idx){
                        frist_profile = "<td data-field='' data-key='2-0-1' class='' style="+cart_val_width+">" +
                            "<div class='layui-table-cell' >"+val['entity_id']+"</div>" +
                            "</td>"+
                            "<td data-field='' data-key='2-0-1' class='' style="+cart_val_width+">" +
                            "<div class='layui-table-cell' >"+val['is_active']+"</div>" +
                            "</td>";
                        val['created_at'] = date_fn(val['created_at']);
                        td_profile= frist_profile+
                            "<td data-field='' data-key='2-0-1' class='' style="+cart_val_width+">" +
                            "<div class='layui-table-cell' >"+val['customer_firstname'] +" "+ val['customer_lastname']+"</div>" +
                            "</td>"+
                            "<td data-field='' data-key='2-0-1' class='' style="+cart_val_width+">" +
                            "<div class='layui-table-cell'>"+parseInt(val['items_qty'])+"</div>" +
                            "</td>"+
                            "<td data-field='' data-key='2-0-1' class='' style="+cart_val_width+">" +
                            "<div class='layui-table-cell'>$"+(parseFloat(val['subtotal']).toFixed(2))+"</div>" +
                            "</td>"+
                            "<td data-field='' data-key='2-0-1' class='' style="+cart_val_width+">" +
                            "<div class='layui-table-cell'>"+val['created_at']+"</div>" +
                            "</td>";

                        tr_profile+="<tr data-index='0' class=''>"+td_profile+"" +
                            "<td align='center' style="+cart_val_width+">"+
                            "<div class='layui-table-cell'>"+
                            "<a class='layui-btn layui-btn layui-btn-xs current_cart_click' cart_id="+val['entity_id']+" current_email = "+email_parent+">View</a>"+
                            "<div>"+
                            "</td>";
                        "</tr>";
                    });



                    var profile_html =
                        "<div class='layui-table-cell order_cart_color' style="+parent_width+">" +
                        "<div class='layui-form layui-border-box ' lay-filter='LAY-table-2' style=''>" +
                        "<div class=''>" +
                        "<div class='layui-table-header'>" +
                        "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>" +
                        "<thead>" +
                        "<tr class=''>" +
                        "<th data-field='profileId' data-key='2-0-0' class='' style="+cart_val_width+">" +
                        "<div class='layui-table-cell laytable-cell-2-0-0'>" +
                        "<span>entity_id</span>" +
                        "</div>" +
                        "</th>" +
                        "<th data-field='profileId' data-key='2-0-0' class='' style="+cart_val_width+">" +
                        "<div class='layui-table-cell laytable-cell-2-0-0'>" +
                        "<span>is_active</span>" +
                        "</div>" +
                        "</th>" +
                        "<th data-field='Nickname' data-key='2-0-1' class='' style="+cart_val_width+">" +
                        "<div class='layui-table-cell'>" +
                        "<span>Name</span>" +
                        "</div>" +
                        "</th>" +
                        "<th data-field='createTime' data-key='2-0-4' class='' style="+cart_val_width+">" +
                        "<div class='layui-table-cell'>" +
                        "<span>Quantity</span>" +
                        "</div>" +
                        "</th>" +
                        "<th data-field='createTime' data-key='2-0-4' class='' style="+cart_val_width+">" +
                        "<div class='layui-table-cell'>" +
                        "<span>Subtotal</span>" +
                        "</div>" +
                        "</th>" +
                        "<th data-field='createTime' data-key='2-0-4' class='' style="+cart_val_width+">" +
                        "<div class='layui-table-cell'>" +
                        "<span>Created Date</span>" +
                        "</div>" +
                        "</th>" +
                        "<th data-field='5' data-key='2-0-5' class=' layui-table-col-special' style="+cart_val_width+">" +
                        "<div class='layui-table-cell' align='center'>" +
                        "<span>Action</span>" +
                        "</div>" +
                        "</th>" +
                        "</tr>" +
                        "</thead>" +
                        "</table>" +
                        "</div>" +
                        "<div class='layui-table-body layui-table-main' id='over_hide'>" +
                        "<table cellspacing='0' cellpadding='0' border='0' class='layui-table'>"+
                        "<tbody>" +
                        tr_profile+
                        "</tbody>" +
                        "</table>"+
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "</div>";
                    layer.open({
                        type: 1
                        , title: "<p class='sendEmail_p'>"+email_parent+" All Cart</p>"
                        , closeBtn: 1,
                        shadeClose: true
                        , area: ["99%", '95%']
                        , shade: 0.8
                        , id: 'LAY_layuipro'
                        , btnAlign: 'c'
                        , moveType: 0
                        , move: false
                        ,btn:["close"]
                        , content:profile_html
                    })
                }
            });
        });
        //判断是否为json字符串函数:
        function exhibits(str){
            if (typeof   str == 'string') {
                try {
                    var obj=JSON.parse(str);
                    return true;
                } catch(e) {
                    return false;
                }
            }
        }
        function Prescription_fn(this_data,current_text,current_email,nickname) {
            var this_viewRx = this_data;



            if(this_viewRx){
                this_viewRx = atob(this_viewRx);
                if(exhibits(this_viewRx)){
                    this_viewRx = JSON.parse(this_viewRx);
                }else{
                    this_viewRx = [];
                }
            }else{
                this_viewRx = [];
            }
            var rx_html ="";
            this_viewRx.forEach(function(val,idx){
                var help_view_img = "";
                if(val['prescription_img']){
                    var prescription_img=  val['prescription_img'];
                    if(prescription_img.indexOf("payneglasses")==-1){
                        try{
                            prescription_img= staticUrl+"/media/customer/prescriptions" + prescription_img;
                        }catch(e){
                            prescription_img = "/media/customer/prescriptions" + prescription_img;
                        }
                    }
                    help_view_img="<img alt='' style='' id='displayImg' src="+prescription_img+" /><p class='tip_img'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
                }

                var order_data_len = parseInt(val['order_num']);
                var cart_data_len = parseInt(val['cart_num']);
                var sum_btn = "";
                if(order_data_len<=0){
                    sum_btn+="<a class='layui-btn layui-btn-normal layui-btn-disabled' current_email="+current_email+">View Current Prescription Order<span class='layui-badge num_user'>"+order_data_len+"</span></a>";
                }else{
                    sum_btn+="<a class='layui-btn layui-btn-normal Prescription_Order' prescription_id="+val['entity_id']+" current_email="+current_email+">View Current Prescription Order<span class='layui-badge num_user'>"+order_data_len+"</span></a>";
                }

                if(cart_data_len<=0){
                    sum_btn+="<a class='layui-btn layui-btn-disabled'>View Current Prescription Cart<span class='layui-badge num_user'>"+cart_data_len+"</span></a>";
                }else{
                    sum_btn+="<a class='layui-btn Prescription_Cart' prescription_id="+val['entity_id']+">View Current Prescription Cart<span class='layui-badge num_user'>"+cart_data_len+"</span></a>";
                }

                val['nickname'] = decodeURI( val['nickname']);

                var new_nickname = "";
                if(nickname){
                    new_nickname = decodeURI(nickname);
                }else{
                    new_nickname = val['nickname'];
                }

                rx_html += "<div class='rx_parents_class'>"+
                    "<h2 class='shopping-cart-profile-name'><span>For "+new_nickname+"</span></h2>"+
                    "<blockquote class='layui-elem-quote' style='padding: 10px'>" +
                    "<div class='Prescription_name' style='font-weight: bold;font-size: 16px'>Prescription: "+val['prescription_name']+"</div>" +
                    "</blockquote>"+
                    "<table class='layui-table'  id='layerDemo-rx'>"+
                    "<colgroup><col width='150' /><col width='200' /><col />"+
                    "</colgroup>"+
                    "<thead>"+
                    "<tr>"+
                    "<th></th>"+
                    "<th>SPH(Sphere)</th>"+
                    "<th>CYL(Cylinder)</th>"+
                    "<th>AXIS</th>"+
                    "<th>ADD(NV-ADD)</th>"+
                    "<th>PD(Pupillary Distance)"+
                    "</th>"+
                    "<th>PRESCRIPTION IMG"+
                    "</th>"+
                    "</tr>"+
                    "</thead>"+
                    "<tbody>"+
                    "<tr>"+
                    "<td>OD(Right Eye)</td>"+
                    "<td class='rsph'></td>"+
                    "<td class='rcyl'></td>"+
                    "<td class='rax'></td>"+
                    "<td class='radd'></td>"+
                    "<td class='rpd rpd_pd'></td>"+
                    "<td class='Prescription_img' rowspan=2>"+help_view_img+"</td>"+
                    "</tr>"+
                    "<tr>"+
                    "<td>OS(Left Eye)</td>"+
                    "<td class='lsph'></td>"+
                    "<td class='lcyl'></td>"+
                    "<td class='lax'></td>"+
                    "<td class='ladd'></td>"+
                    "<td class='lpd lpd_pd'></td>"+
                    "</tr>"+
                    "</tbody>"+
                    "</table>"+
                    "<div class='Prism_title'>Need Prism Correction</div>"+
                    "<table class='layui-table'  id='layerDemo-prism'>"+
                    "<colgroup>"+
                    "<col width='150' />"+
                    "<col width='200' />"+
                    "<col />"+
                    "</colgroup>"+
                    "<thead>"+
                    "<tr>"+
                    "<th></th>"+
                    "<th>Prism Value</th>"+
                    "<th>Base Direction</th>"+
                    "<th>Prism Value</th>"+
                    "<th>Base Direction</th>"+
                    "</tr>"+
                    "</thead>"+
                    "<tbody>"+
                    "<tr>"+
                    "<td>OD(Right Eye)</td>"+
                    "<td class='rpri'></td>"+
                    "<td class='rbase'></td>"+
                    "<td class='rpri_1'></td>"+
                    "<td class='rbase_1'></td>"+
                    "</tr>"+
                    "<tr>"+
                    "<td>OS(Left Eye)</td>"+
                    "<td class='lpri'></td>"+
                    "<td class='lbase'></td>"+
                    "<td class='lpri_1'></td>"+
                    "<td class='lbase_1'></td>"+
                    "</tr>"+
                    "</tbody>"+
                    "</table>"+
                    "<blockquote class='layui-elem-quote'><p><span>Date of Prescription: </span><span class='DatePrescriptio_time'>"+val['exam_date']+"</sapn></p></blockquote>"+
                    "<blockquote class='layui-elem-quote'><p><span>Expiration Time: </span><span class='Expiration_time'>"+val['expire_date']+"</sapn></p></blockquote>"+
                    "<blockquote class='layui-elem-quote'><p><span>Prescription Nickname: </span><span class='Prescriptio_name'>"+val['prescription_name']+"</sapn></p></blockquote>"+
                    "<blockquote class='layui-elem-quote'>"
                    +sum_btn+
                    "</blockquote>"+
                    "</div>";
            });

            layer.open({
                type: 1
                , title: "<p class='sendEmail_p'>"+current_text+"</p>"
                , closeBtn: 1,
                shadeClose: true
                , area: ["99%", '95%']
                , shade: 0.8
                , id: 'LAY_layuipro22'
                , btnAlign: 'c'
                , moveType: 0
                , move: false
                ,btn:["close"]
                , content:rx_html
            })


            setTimeout(function(){
                this_viewRx.forEach(function(val,idx){
                    this_viewRx = val;
                    var view_lax = this_viewRx['lax'];
                    if(view_lax==0){
                        view_lax = "000";
                    }else if(view_lax<=9){
                        view_lax = ("00"+view_lax);
                    }else if(view_lax<=99){
                        view_lax = ("0"+view_lax);
                    }

                    var view_rax= this_viewRx['rax'];
                    if(view_rax==0){
                        view_rax = "000";
                    }else if(view_rax<=9){
                        view_rax = ("00"+view_rax);
                    }else if(view_rax<=99){
                        view_rax = ("0"+view_rax);
                    }

                    $(".rx_parents_class .lsph").eq(idx).text(this_viewRx['lsph']>0?("+"+this_viewRx['lsph']):this_viewRx['lsph']);
                    $(".rx_parents_class .lcyl").eq(idx).text(this_viewRx['lcyl']>0?("+"+this_viewRx['lcyl']):this_viewRx['lcyl']);
                    $(".rx_parents_class .lax").eq(idx).text(view_lax);
                    $(".rx_parents_class .ladd").eq(idx).text(this_viewRx['ladd']>0?("+"+this_viewRx['ladd']):this_viewRx['ladd']);
                    $(".rx_parents_class .rsph").eq(idx).text(this_viewRx['rsph']>0?("+"+this_viewRx['rsph']):this_viewRx['rsph']);
                    $(".rx_parents_class .rcyl").eq(idx).text(this_viewRx['rcyl']>0?("+"+this_viewRx['rcyl']):this_viewRx['rcyl']);
                    $(".rx_parents_class .rax").eq(idx).text(view_rax);
                    $(".rx_parents_class .radd").eq(idx).text(this_viewRx['radd']>0?("+"+this_viewRx['radd']):this_viewRx['radd']);
                    $(".rx_parents_class .rpd").eq(idx).text(this_viewRx['rpd']);
                    $(".rx_parents_class .rpri").eq(idx).text(this_viewRx['rpri']>0?("+"+this_viewRx['rpri']):this_viewRx['rpri']);
                    $(".rx_parents_class .rbase").eq(idx).text(this_viewRx['rbase']);
                    $(".rx_parents_class .rpri_1").eq(idx).text(this_viewRx['rpri_1']>0?("+"+this_viewRx['rpri_1']):this_viewRx['rpri_1']);
                    $(".rx_parents_class .rbase_1").eq(idx).text(this_viewRx['rbase_1']);
                    $(".rx_parents_class .lpri").eq(idx).text(this_viewRx['lpri']>0?("+"+this_viewRx['lpri']):this_viewRx['lpri']);
                    $(".rx_parents_class .lbase").eq(idx).text(this_viewRx['lbase']);
                    $(".rx_parents_class .lpri_1").eq(idx).text(this_viewRx['lpri_1']>0?("+"+this_viewRx['lpri_1']):this_viewRx['lpri_1']);
                    $(".rx_parents_class .lbase_1").eq(idx).text(this_viewRx['lbase_1']);
                    if(this_viewRx['exam_date']){
                        var new_exam_date = this_viewRx['exam_date'].split("-");
                        var year = new_exam_date[0];
                        var str_new_exam_date = new_exam_date[1]+"/"+new_exam_date[2]+"/"+year;
                        $(".DatePrescriptio_time").eq(idx).text(str_new_exam_date);
                    }

                    if(this_viewRx['expire_date']) {
                        var new_expire_date = this_viewRx['expire_date'].split("-");
                        var year = new_expire_date[0];
                        var str_new_expire_date = new_expire_date[1] + "/" + new_expire_date[2] + "/" + year;
                        $(".Expiration_time").eq(idx).text(str_new_expire_date);
                    }

                    if(this_viewRx['single_pd']=="1"){
                        //单
                        $(".rx_parents_class .rpd_pd").eq(idx).attr("rowspan",2);
                        $(".rx_parents_class .lpd_pd").eq(idx).hide();
                        $(".rx_parents_class .binocular_pd_view").eq(idx).addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                        $(".rx_parents_class .rpd_pd").eq(idx).text(this_viewRx['pd'])
                    }else{
                        //双
                        $(".rx_parents_class .rpd_pd").eq(idx).removeAttr("rowspan");
                        $(".rx_parents_class .lpd_pd").eq(idx).show();
                        $(".rx_parents_class .monocular_pd_view").eq(idx).addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                        $(".rx_parents_class .rpd").eq(idx).text(this_viewRx['rpd']);
                        $(".rx_parents_class .lpd").eq(idx).text(this_viewRx['lpd']);
                    }
                });
            },400);

        }
        //profile里查看当前的验光单
        $(document).on("click",".profile_View_Prescription",function(){
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            layer.load(2, {shade: 0.6});
            var current_email = $(this).attr("current_email");
            var nickname = $(this).attr("nickname");
            var profile_id = $(this).attr("profile_id");
            var profile_id_data = {
                profile_id:profile_id
            };
            $.ajax({
                method: 'POST',
                url: '/webyiiapi/customer-detail/profile-prescription.html',
                data: JSON.stringify(profile_id_data),
                success: function (res) {
                    layer.closeAll('loading');
                    if(res.code==1){
                        var prescription_data_arr = res['data'];
                        prescription_data_arr.forEach(function(val,idx){
                            var nickname = val['nickname'] = nickname;
                            var prescription_name = val['prescription_name'];
                            if (escape(prescription_name).indexOf( "%u" )<0){
                            }else {
                                val['prescription_name'] = encodeURI(val['prescription_name']);
                            }
                            if (escape(nickname).indexOf( "%u" )<0){
                            }else {
                                val['nickname'] = encodeURI(val['nickname']);
                            }
                        });

                        var prescription_data_arr_s = JSON.stringify(prescription_data_arr);
                        prescription_data_arr_s = btoa(prescription_data_arr_s);

                        Prescription_fn(prescription_data_arr_s,(current_email+" of prescription"),current_email,nickname);
                    }
                }
            });
        });
        //全部验光单展示只查看当前验光单
        $(document).on("click",".Prescription_view_click",function(){
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            var current_email = $(this).attr("current_email");
            var current_profile = $(this).attr("current_profile");
            var prescriptiondata = $(this).attr("prescriptiondata");
            Prescription_fn(prescriptiondata,(current_email+" of prescription"),current_email);
        });
        function current_cart_fn(type,cart_data,current_text,yr_text,data_obj_order) {
            var order_html ="<div class='order_parent'>"+
                "<ul class='order_class'>"+
                "<li>Shipping Address</li>"+
                "<div>"+
                "<p class='Shipping_name'></p>"+
                "<p class='street'></p>"+
                "<p class='city'></p>"+
                "<p class='country_name'></p>"+
                "<p class='telephone'></p>"+
                "</div>"+
                "</ul>"+
                "<ul class='order_class'>"+
                "<li>Shipping Method</li>"+
                "<div>"+
                "<p class='ShippingMethod'></p>"+
                "</div>"+
                "</ul>"+
                "<ul class='order_class'>"+
                "<li>Warranty Selected</li>"+
                "<div>"+
                "<p class='warranty'></p>"+
                "</div>"+
                "</ul>"+
                "<ul class='order_class' style='float:revert;'>"+
                "<li>Payment Method</li>"+
                "<div>"+
                "<p>Stripe Cards (Credit and Debit)</p>"+
                "<ul class='order_ul_add'>"+
                "<li>"+
                "<span>Credit Card Type</span><span class='card_type'></span>"+
                "</li>"+
                "<li>"+
                "<span>Credit Card Number</span><span class='maskedCC'></span>"+
                "</li>"+
                "</ul>"+
                "</div>"+
                "</ul>"+
                "</div>";




            //订单信息和小计
            var order_xj =
                    "<div class='Order_sum_xj'>"+
                        "<h5 class='Order_title'>Order Information</h5>"+
                        "<ul class='Order_types'>"+
                            "<li>"+
                                "<p>Estimated Total</p>"+
                                "<span class='EstimatedTotal'></span>"+
                            "</li>"+
                            "<li>"+
                                "<p>Shipping Address</p>"+
                                "<span class='Shipping_Address'></span>"+
                            "</li>"+
                            "<li>"+
                                "<p>Billing Address</p>"+
                                "<span class='Billing_Address'></span>"+
                            "</li>"+
                            "<li>"+
                                "<p>Payment Method</p>"+
                                "<span class='Payment_Method'>Stripe Cards (Credit and Debit)</span>"+
                            "</li>"+
                        "</ul>" +
                        "<ul class='xj_types'>" +
                            "<li>" +
                                "<span>Subtotal:</span>" +
                                "<span class='Subtotal_order_s'></span>" +
                            "</li>" +
                            "<li>" +
                                "<span>Shipping & Handling:</span>" +
                                "<span class='Shipping_Handling_s'></span>" +
                            "</li>" +
                            "<li class='discount_parent'>" +
                                "<span>Discount:<a class='Discount_a'></a></span>" +
                                "<span class='discount_s'></span>" +
                            "</li>" +
                            "<li class='Warranty_s_parent'>" +
                                "<span>Warranty:</span>" +
                                "<span class='Warranty_s'></span>" +
                            "</li>" +
                            "<li class='redeem_Points_parent'>" +
                                "<span>redeem <a class='redeem_s'>6000</a> Points:</span>"+
                                "<span class='redeem_Points'></span>"+
                            "</li>"+
                            "<li>" +
                                "<span>Grand Total:</span>" +
                                "<span class='Grand_Total'></span>" +
                            "</li>" +
                            "<li class='You_will_earn_parent'>" +
                                "<span>You will earn:</span>" +
                                "<span class='You_will_earn'></span>" +
                            "</li>" +
                        "</ul>" +
                "</div>";



            if(type=="cart"){
                order_html = "";
                order_xj = "";
            }
            var cart_html = order_html;

            if(data_obj_order){
                data_obj_order = decodeURI(data_obj_order);
                if(exhibits(data_obj_order)){
                    data_obj_order = JSON.parse(data_obj_order);
                }else{
                    data_obj_order = {};
                }
            }else{
                data_obj_order = {};
            }


            cart_data.forEach(function(val,idx){
                var order_address_data = val["order_address_data"];
                if(yr_text=="prescription_order"){
                    order_address_data = val["order_address_detail"];
                }
                var cart_detail_lin  = val['cart_detail'];
                var profile_prescription_lin  = val['profile_prescription'];
                var prescription_html ="";



                if(type!="cart" && order_address_data){
                    setTimeout(function(){
                        var Shipping_name = order_address_data['firstname']+" "+order_address_data['lastname'];
                        $(".Shipping_name").eq(idx).text(Shipping_name);
                        $(".street").eq(idx).text(order_address_data['street']);
                        var city_sum = order_address_data['city']+","+order_address_data['postcode'];
                        var telephone = order_address_data['telephone'];
                        var ShippingMethod = order_address_data['shipping_description'];
                        var has_warranty = order_address_data['has_warranty'];
                        var country_name = order_address_data['country_name'];
                        var card_type = order_address_data['card_type'];
                        var maskedCC = order_address_data['maskedCC'];
                        $(".city").eq(idx).text(city_sum);
                        $(".country_name").eq(idx).text(country_name);
                        $(".telephone").eq(idx).text("T : "+telephone);
                        $(".ShippingMethod").eq(idx).text(ShippingMethod);
                        $(".card_type").eq(idx).text(card_type);
                        $(".maskedCC").eq(idx).text(maskedCC);
                        if(has_warranty=='1'){
                            has_warranty  = "Risk-Free Warranty"
                        }else{
                            has_warranty  = "Standard Warranty (No Full Refund)"
                        }
                        $(".warranty").eq(idx).text(has_warranty);
                        if(yr_text=="prescription_order"){
                            data_obj_order  = {};
                            data_obj_order['subtotal'] =  order_address_data['subtotal'];
                            data_obj_order['shippingAmount'] =  order_address_data['shipping_amount'];
                            data_obj_order['warranty'] =  order_address_data['warranty'];
                            data_obj_order['hasWarranty'] =  order_address_data['has_warranty'];
                            data_obj_order['grandTotal'] =  order_address_data['grand_total'];
                            data_obj_order['pointPrice'] =  order_address_data['point_price'];
                            data_obj_order['pointAmount'] =  order_address_data['point_amount'];
                            data_obj_order['gavingScore'] =  order_address_data['gaving_score'];
                            data_obj_order['discountAmount'] =  order_address_data['discount_amount'];
                            data_obj_order['discountDescription'] =  order_address_data['discount_description'];
                        }





                        var subtotal_order = data_obj_order['subtotal'];
                        var shipping_amount = data_obj_order['shippingAmount'];
                        var warranty_s = data_obj_order['warranty'];
                        var is_warranty = data_obj_order['hasWarranty'];
                        var grand_total_s = data_obj_order['grandTotal'];

                        var point_price = data_obj_order['pointPrice'];
                        var point_amount = data_obj_order['pointAmount'];
                        var gaving_score = data_obj_order['gavingScore'];

                        var discount_amount = data_obj_order['discountAmount'];
                        var discount_description = data_obj_order['discountDescription'];



                        if(gaving_score){
                            gaving_score = parseInt(gaving_score);
                            if(gaving_score){
                                $(".You_will_earn_parent").show();
                                $(".You_will_earn").text(gaving_score+" Points");
                            }else{
                                $(".You_will_earn_parent").hide();
                            }
                        }else{
                            $(".You_will_earn_parent").hide();
                        }
                        if(point_amount){
                            point_amount = parseInt(point_amount);
                            if(point_amount){
                                $(".redeem_Points_parent").show();
                                $(".redeem_s").text(point_amount);
                                point_price = parseFloat(point_price);
                                point_price = point_price.toFixed(2);
                                $(".redeem_Points").text("-$"+point_price);
                            }else{
                                $(".redeem_Points_parent").hide();
                            }
                        }else{
                            $(".redeem_Points_parent").hide();
                        }


                        if(discount_amount){
                            discount_amount = parseFloat(discount_amount);
                            if(discount_amount){
                                discount_amount = discount_amount.toFixed(2);
                                $(".discount_parent").show();
                                $(".Discount_a").text("("+discount_description+")");
                                var discount_amount = Math.abs(discount_amount);
                                $(".discount_s").text("-$"+discount_amount);
                            }else{
                                $(".discount_parent").hide();
                            }
                        }else{
                            $(".discount_parent").hide();
                        }


                        if(is_warranty=="1"){
                            $(".Warranty_s_parent").show();
                        }else{
                            $(".Warranty_s_parent").hide();
                        }
                        if(subtotal_order){
                            subtotal_order = parseFloat(subtotal_order).toFixed(2);
                            $(".EstimatedTotal").text("$"+subtotal_order);
                            $(".Subtotal_order_s").text("$"+subtotal_order);
                        }
                        if(shipping_amount){
                            shipping_amount = parseFloat(shipping_amount).toFixed(2);
                            $(".Shipping_Handling_s").text("$"+shipping_amount);
                        }
                        if(warranty_s){
                            warranty_s = parseFloat(warranty_s).toFixed(2);
                            $(".Warranty_s").text("$"+warranty_s);
                        }
                        if(grand_total_s){
                            grand_total_s = parseFloat(grand_total_s).toFixed(2);
                            $(".grand_total").text("$"+grand_total_s);
                        }
                        $(".Shipping_Address").text(order_address_data['street']+" , "+order_address_data['city']+" , "+order_address_data['country_id']);
                        $(".Billing_Address").text(order_address_data['street']+" , "+order_address_data['city']+" , "+order_address_data['country_id']);
                    })
                }

                if(typeof profile_prescription_lin != "object" || !profile_prescription_lin){
                    var profile_prescription_lin_html = "";
                    if(!profile_prescription_lin){
                        profile_prescription_lin_html = "<h2 class='shopping-cart-profile-name'>"+
                        "<div class='nickname_cart'></div>"+
                        "</h2>"+
                        "<div class='chlid_order_list'><h2 class='padd_border_Prescription'>Prescription : <span>Non RX</span></h2>";
                    }else{
                        profile_prescription_lin_html="<h2 class='shopping-cart-profile-name'>"+
                        "<div class='nickname_cart'></div>"+
                        "</h2>"+
                        "<div class='chlid_order_list'><h2 class='padd_border_Prescription'><span>"+profile_prescription_lin+"</span></h2>";
                    }
                    prescription_html =
                        "<h2 class='shopping-cart-profile-name'>" +
                        "</h2>"+
                        "<div class='rx_parents_class_s'>" +
                            profile_prescription_lin_html;
                }else{
                    prescription_html =
                    "<div class='rx_parents_class_s'>" +
                            "<h2 class='shopping-cart-profile-name'>" +
                            "<div class='nickname_cart'></div>" +
                            "</h2>"+
                     "<div class='chlid_order_list'>"+
                    "<h2 class='padd_border_Prescription'>Prescription : <span>"+profile_prescription_lin['prescription_name']+"</span></h2>" +
                    "<table class='layui-table'  id='layerDemo-rx'>" +
                    "<colgroup><col width='150' /><col width='200' /><col />" +
                    "</colgroup>" +
                    "<thead>" +
                    "<tr>" +
                    "<th></th>" +
                    "<th>SPH(Sphere)</th>" +
                    "<th>CYL(Cylinder)</th>" +
                    "<th>AXIS</th>" +
                    "<th>ADD(NV-ADD)</th>" +
                    "<th>PD(Pupillary Distance)</th>" +
                    "<th>PRESCRIPTION IMG</th>"+
                    "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                    "<tr>" +
                    "<td>OD(Right Eye)</td>" +
                    "<td class='rsph'>"+profile_prescription_lin['rsph']+"</td>" +
                    "<td class='rcyl'></td>" +
                    "<td class='rax'></td>" +
                    "<td class='radd'></td>" +
                    "<td class='rpd rpd_pd'></td>" +
                    "<td class='Prescription_img' rowspan='2'>"+
                    "</td>"+
                    "</tr>" +
                    "<tr>" +
                    "<td>OS(Left Eye)</td>" +
                    "<td class='lsph'></td>" +
                    "<td class='lcyl'></td>" +
                    "<td class='lax'></td>" +
                    "<td class='ladd'></td>" +
                    "<td class='lpd lpd_pd'></td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>"+
                    "<div class='Prism_title'>Need Prism Correction</div>" +
                    "<table class='layui-table'  id='layerDemo-prism'>" +
                    "<colgroup>" +
                    "<col width='150' />" +
                    "<col width='200' />" +
                    "<col />" +
                    "</colgroup>" +
                    "<thead>" +
                    "<tr>" +
                    "<th></th>" +
                    "<th>Prism Value</th>" +
                    "<th>Base Direction</th>" +
                    "<th>Prism Value</th>" +
                    "<th>Base Direction</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                    "<tr>" +
                    "<td>OD(Right Eye)</td>" +
                    "<td class='rpri'></td>" +
                    "<td class='rbase'></td>" +
                    "<td class='rpri_1'></td>" +
                    "<td class='rbase_1'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>OS(Left Eye)</td>" +
                    "<td class='lpri'></td>" +
                    "<td class='lbase'></td>" +
                    "<td class='lpri_1'></td>" +
                    "<td class='lbase_1'></td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>";
                }


                cart_html +="<div class='parents_cart'>" +
                    prescription_html+
                    "<div class='Subtotal_cart_pad'>" +
                    "<ul class='Subtotal_cart_img'>" +
                    "<li><img class='cart_img' src=''/></li>" +
                    "<li class='product_name_sku_li'><span class='product_name_sku'></span></li>" +
                    "<li><span class='product_color'>Pink</span></li>" +
                    "<li class='quantity_li'><span>Ordered Quantity: </span><span class='quantity'></span></li>" +
                    "</ul>" +
                    "<li class='Subtotal_cart'><span>Subtotal: </span><span class='cart_Subtotal'></span></li>" +
                    "<ul class='Subtotal_cart_num'>" +
                    "</ul>" +
                    "</div>" +
                    "</div>"+
                    "</div></div>";
            });

            cart_html+=order_xj;


            setTimeout(function(){
                cart_data.forEach(function(val,idx) {
                    var cart_detail_frist  = {};
                    var cart_detail_all  = {};
                    var qty = "";
                    if(type=="cart"){
                        cart_detail_frist = val['cart_detail'][0];
                        cart_detail_all= val['cart_detail'];
                        qty = cart_detail_frist['qty'];
                    }else{
                        cart_detail_frist = val['order_detail'][0];
                        cart_detail_all= val['order_detail'];
                        qty = cart_detail_frist['qty_ordered'];
                    }
                    var row_total = cart_detail_frist['row_total'];
                    row_total = parseFloat(row_total).toFixed(2);
                    qty = parseInt(qty);
                    var color_value = cart_detail_frist["color_value"];
                    var image = cart_detail_frist["image"];
                    var nickname = cart_detail_frist["nickname"];
                    if(nickname){
                        $(".nickname_cart").eq(idx).html("<span>FOR "+nickname+"</span>");
                    }


                    var current_href = window.location.href;
                    if(current_href.indexOf("pgadm")!=-1){

                    }
                    try{
                        $(".cart_img").eq(idx).attr("src",staticUrl+"/media/catalog/product/cache/600x300"+image);
                    }catch(e){
                        $(".cart_img").eq(idx).attr("src","/media/catalog/product/cache/600x300"+image);
                    }
                    $(".product_color").eq(idx).text(color_value);
                    $(".cart_Subtotal").eq(idx).text("$"+row_total);
                    $(".product_name_sku").eq(idx).html(cart_detail_frist['name']+"<span> ("+cart_detail_frist['sku']+")</span>");
                    $(".quantity").eq(idx).text(qty);
                    var gy_li_html = "";
                    cart_detail_all.forEach(function(val1,idx1){
                        var frame_price = parseFloat(val1['price']).toFixed(2);

                        if(frame_price=="0.00"){
                            frame_price = "included"
                        }else{
                            frame_price = "$"+frame_price
                        };

                        if(idx1!=0){
                            if(idx1==1){
                                val1['name'] = "Frame ("+cart_detail_frist['sku']+")";
                            }
                            gy_li_html+="<li class='Subtotal_gy'><span>"+val1['name']+"</span><span class='frame_price'>"+frame_price+"</span></li>";
                        }

                    });
                    $(".Subtotal_cart_num").eq(idx).html(gy_li_html);

                    //验光单展示
                    var this_viewRx  = val['profile_prescription'];


                    if(typeof this_viewRx != "object" || !this_viewRx){
                        return false;
                    }


                    var view_lax = this_viewRx['lax'];
                    if (view_lax == 0) {
                        view_lax = "000";
                    } else if (view_lax <= 9) {
                        view_lax = ("00" + view_lax);
                    } else if (view_lax <= 99) {
                        view_lax = ("0" + view_lax);
                    }
                    var view_rax = this_viewRx['rax'];
                    if (view_rax == 0) {
                        view_rax = "000";
                    } else if (view_rax <= 9) {
                        view_rax = ("00" + view_rax);
                    } else if (view_rax <= 99) {
                        view_rax = ("0" + view_rax);
                    }
                    $(".rx_parents_class_s .lsph").eq(idx).text(this_viewRx['lsph'] > 0 ? ("+" + this_viewRx['lsph']) : this_viewRx['lsph']);
                    $(".rx_parents_class_s .lcyl").eq(idx).text(this_viewRx['lcyl'] > 0 ? ("+" + this_viewRx['lcyl']) : this_viewRx['lcyl']);
                    $(".rx_parents_class_s .lax").eq(idx).text(view_lax);
                    $(".rx_parents_class_s .ladd").eq(idx).text(this_viewRx['ladd'] > 0 ? ("+" + this_viewRx['ladd']) : this_viewRx['ladd']);
                    $(".rx_parents_class_s .rsph").eq(idx).text(this_viewRx['rsph'] > 0 ? ("+" + this_viewRx['rsph']) : this_viewRx['rsph']);
                    $(".rx_parents_class_s .rcyl").eq(idx).text(this_viewRx['rcyl'] > 0 ? ("+" + this_viewRx['rcyl']) : this_viewRx['rcyl']);
                    $(".rx_parents_class_s .rax").eq(idx).text(view_rax);
                    $(".rx_parents_class_s .radd").eq(idx).text(this_viewRx['radd'] > 0 ? ("+" + this_viewRx['radd']) : this_viewRx['radd']);
                    $(".rx_parents_class_s .rpd").eq(idx).text(this_viewRx['rpd']);
                    $(".rx_parents_class_s .rpri").eq(idx).text(this_viewRx['rpri'] > 0 ? ("+" + this_viewRx['rpri']) : this_viewRx['rpri']);
                    $(".rx_parents_class_s .rbase").eq(idx).text(this_viewRx['rbase']);
                    $(".rx_parents_class_s .rpri_1").eq(idx).text(this_viewRx['rpri_1'] > 0 ? ("+" + this_viewRx['rpri_1']) : this_viewRx['rpri_1']);
                    $(".rx_parents_class_s .rbase_1").eq(idx).text(this_viewRx['rbase_1']);
                    $(".rx_parents_class_s .lpri").eq(idx).text(this_viewRx['lpri'] > 0 ? ("+" + this_viewRx['lpri']) : this_viewRx['lpri']);
                    $(".rx_parents_class_s .lbase").eq(idx).text(this_viewRx['lbase']);
                    $(".rx_parents_class_s .lpri_1").eq(idx).text(this_viewRx['lpri_1'] > 0 ? ("+" + this_viewRx['lpri_1']) : this_viewRx['lpri_1']);
                    $(".rx_parents_class_s .lbase_1").eq(idx).text(this_viewRx['lbase_1']);


                    if(this_viewRx['prescription_img']){
                        if(this_viewRx['prescription_img'].indexOf("payneglasses")==-1){
                            try{
                                this_viewRx['prescription_img']= staticUrl+"/media/customer/prescriptions" + this_viewRx['prescription_img'];
                            }catch(e){
                                this_viewRx['prescription_img'] = "/media/customer/prescriptions" + this_viewRx['prescription_img'];
                            }
                        }
                    }

                    var Prescription_img =
                                "<img alt='' style='' id='displayImg' src="+this_viewRx['prescription_img']+">"+
                                "<p class='tip_img'><i class='layui-icon layui-icon-tips'></i>Click on the picture to view the larger picture</p>";
                    if(this_viewRx['prescription_img']){
                        $(".rx_parents_class_s .Prescription_img").html(Prescription_img);
                    }
                    if(this_viewRx['single_pd']=="1"){
                        //单
                        $(".rx_parents_class_s .rpd_pd").eq(idx).attr("rowspan",2);
                        $(".rx_parents_class_s .lpd_pd").eq(idx).hide();
                        $(".rx_parents_class_s .binocular_pd_view").eq(idx).addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                        $(".rx_parents_class_s .rpd_pd").eq(idx).text(this_viewRx['pd'])
                    }else{
                        //双
                        $(".rx_parents_class_s .rpd_pd").eq(idx).removeAttr("rowspan");
                        $(".rx_parents_class_s .lpd_pd").eq(idx).show();
                        $(".rx_parents_class_s .monocular_pd_view").eq(idx).addClass("pd_class_active").siblings("p").removeClass("pd_class_active");
                        $(".rx_parents_class_s .rpd").eq(idx).text(this_viewRx['rpd']);
                        $(".rx_parents_class_s .lpd").eq(idx).text(this_viewRx['lpd']);
                    }

                })
            },200);
            layer.open({
                type: 1
                , title: "<p class='sendEmail_p'>"+current_text+"</p>"
                , closeBtn: 1,
                shadeClose: true
                , area: ["99%", '95%']
                , shade: 0.8
                , id: 'LAY_layuipro_cart'
                , btnAlign: 'c'
                , moveType: 0
                , move: false
                , btn: ["close"]
                , content: cart_html
            });
        }
        //在夜光单页面查看点击Prescription_Cart
        $(document).on("click",".Prescription_Cart",function(){
            var current_email = $(this).attr("current_email");
            var current_text = current_email + " Shopping Cart";
            var prescription_id=  $(this).attr("prescription_id");
            var data_prescription_id = {prescription_id:prescription_id};
            layer.load(2, {shade: 0.6});
            $.ajax({
                method: 'POST',
                url: '/webyiiapi/customer-detail/prescription-cart.html',
                data: JSON.stringify(data_prescription_id),
                success: function (res) {
                    layer.closeAll('loading');
                    if(res.code==1){
                        var cart_data = res['data'];
                        var new_cart_data = [];
                        for (var i in cart_data) {
                            new_cart_data = new_cart_data.concat(cart_data[i]);
                        }
                        if(new_cart_data.length>=1){
                            current_cart_fn("cart",new_cart_data,current_text);
                        }else{
                            layer.alert('The requested data is empty.', {
                                title:"Data request"
                                ,area: ['520px', 'auto']
                                ,btn: ['Close']
                                ,icon: 5
                            });
                        }

                    }else{
                        layer.alert(res.message, {
                            title:"Data request"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }
                }
            });
        });
        //在夜光单页面查看点击Prescription_Order
        $(document).on("click",".Prescription_Order",function(){
            var current_email = $(this).attr("current_email");
            var current_text = current_email + " of order [ #10039467 ]";
            var prescription_id=  $(this).attr("prescription_id");
            var data_prescription_id = {prescription_id:prescription_id};
            layer.load(2, {shade: 0.6});
            $.ajax({
                method: 'POST',
                url: '/webyiiapi/customer-detail/prescription-order.html',
                data: JSON.stringify(data_prescription_id),
                success: function (res) {
                    layer.closeAll('loading');
                    if(res.code==1){
                        var cart_data = res['data'];
                        var new_cart_data = [];
                        for (var i in cart_data) {
                            new_cart_data = new_cart_data.concat(cart_data[i]);
                        }
                        if(new_cart_data.length>=1){
                            current_cart_fn("order",new_cart_data,current_text,"prescription_order");
                        }else{
                            layer.alert('The requested data is empty.', {
                                title:"Data request"
                                ,area: ['520px', 'auto']
                                ,btn: ['Close']
                                ,icon: 5
                            });
                        }
                    }else{
                        layer.alert(res.message, {
                            title:"Data request"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }
                }
            });

        });
        //查看当前购物车
        $(document).on("click",".current_cart_click",function(){
            layer.load(2, {shade: 0.6});
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            var current_email = $(this).attr("current_email");
            var current_text = current_email + " Shopping Cart";
            var cart_id = $(this).attr("cart_id");
            var cart_data = {
                cart_id:cart_id
            };
            $.ajax({
                method: 'POST',
                url: '/webyiiapi/customer-detail/cartid-cart.html',
                data: JSON.stringify(cart_data),
                success: function (res) {
                    layer.closeAll('loading');
                    if(res.code==1){
                        var cart_data = res['data'];
                        var new_cart_data = [];
                        for (var i in cart_data) {
                            new_cart_data = new_cart_data.concat(cart_data[i]);
                        }
                        if(new_cart_data.length>=1){
                            current_cart_fn("cart",new_cart_data,current_text);
                        }else{
                            layer.alert('The requested data is empty.', {
                                title:"Data request"
                                ,area: ['520px', 'auto']
                                ,btn: ['Close']
                                ,icon: 5
                            });
                        }

                    }else{
                        layer.alert(res.message, {
                            title:"Error Tips"
                            ,area: ['520px', 'auto']
                            ,btn: ['Close']
                            ,icon: 5
                        });
                    }
                }
            });
        });
        //查看当前order
        $(document).on("click",".order_click",function(){
            var data_obj_order = $(this).attr("data_obj_order");
            layer.load(2, {shade: 0.6});
            $(this).parents("td").css("background","#eae9e9").siblings("td").css("background","#eae9e9").parents("tr").siblings("tr").find("td").css("background","#fff");
            var current_email = $(this).attr("current_email");
            var orderId = $(this).attr("orderId");
            var current_text = current_email + " Order # "+orderId;
            var order_id = $(this).attr("order_id");
            var order_id_data = {
                order_id:order_id
            };
            $.ajax({
                method: 'POST',
                url: '/webyiiapi/customer-detail/orderid-order.html',
                data: JSON.stringify(order_id_data),
                success: function (res) {
                    layer.closeAll('loading');
                    if(res.code==1){
                        var order_data = res['data'];
                        var new_order_data = [];
                        for (var i in order_data) {
                            new_order_data = new_order_data.concat(order_data[i]);
                        }
                        current_cart_fn("order",new_order_data,current_text,"",data_obj_order);
                    }
                }
            });





            // current_cart_fn("order",current_text);
        });
        //点击图片放大事件
        $(document).on('click','.view_help_img,#displayImg,.prescription_img_edit,#portrait',function(){
            var _this = $(this);//将当前的pimg元素作为_this传入函数
            imgShow("#outerdiv", "#innerdiv", "#bigimg", _this);
        });
        //点击页数切换函数
        function page_fn(types,current_ys){
            $('body,html').animate({'scrollTop':0},500);
            layer.load(2, {shade: 0.6});
            current_ys = parseInt(current_ys);
            if(types=="next"){
                if(current_ys){
                    init_customer_data(current_ys+1,current_filter_fn());
                }
            }else if(types=="prev"){
                if(current_ys){
                    init_customer_data(current_ys-1,current_filter_fn());
                }
            }else{
                if(current_ys){
                    init_customer_data(current_ys,current_filter_fn());
                }
            }
        }
        //分页点击每个页数切换
        $(document).on("click",".pageBtnWrap a",function(){
            var current_ys = $(this).text();
            if(current_ys=="prev"){
                current_ys = $(this).siblings(".curr").text();
                page_fn("prev",current_ys);
            }else if(current_ys=="next"){
                current_ys = $(this).siblings(".curr").text();
                page_fn("next",current_ys);
            }else if(current_ys){
                page_fn("",current_ys);
            }

        });
        //搜索
        $("#Search_filter").on("click",function(){
            var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var email_filter = $(".email_filter").val();
            $(".email_orderId").val();
            $(".email_filter").css("borderColor","#ccc");
            if(email.test(email_filter)){
                $(".email_filter").css("borderColor","#ccc");
                init_customer_data(1,current_filter_fn());
            }else{
                if(email_filter){
                    $(".email_filter").css("borderColor","red");
                    layer.alert('Invalid email address.', {
                        title:"Verify mailbox"
                        ,area: ['520px', 'auto']
                        ,btn: ['Close']
                        ,icon: 5
                    });
                }else{
                    init_customer_data(1,current_filter_fn());
                }
            }
        });
        //清空搜索
        $("#Search_clearall").on("click",function(){
            $(".email_filter").val("");
            $(".email_orderId").val("");
            init_customer_data(1,current_filter_fn());
        })

        //点击查看更多
        $(document).on("click",".nickname_more",function(){
            var current_text = $(this).text();
            layer.alert(current_text, {
                title:"Profile Name"
                ,area: ['520px', 'auto']
                ,btn: ['Close']
                ,icon: 1
            });
        })
    });
