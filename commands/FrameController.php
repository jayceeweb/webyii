<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\FileHelper;
use app\models\UrlRewrite;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrameController extends Controller
{
    private $cache_index_dingding_message="";

    public function actionFrameUrlrewriteCheck() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $productHelper=$objectManager->get("Pg\CatalogExtend\Helper\Product");

        $FRAME_CATEGORY_IDS=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["FRAME_CATEGORY_IDS"];
        $sql="SELECT
`e`.`entity_id` AS configurable_id,
`e`.`attribute_set_id` AS configurable_attribute_set_id,
`e`.`type_id` AS configurable_type_id,
`e`.`sku` AS configurable_sku,
`e`.`name` AS configurable_name,
`cat_index`.`category_id`,
`s_e`.`entity_id` AS simple_entity_id,
`s_e`.`type_id` AS simple_type_id,
`s_e`.`sku` AS simple_sku,
`s_e`.`name` AS simple_name
FROM
`catalog_product_flat_all_1` AS e
INNER JOIN `catalog_category_product_index` AS cat_index ON `cat_index`.`product_id` = `e`.`entity_id` AND `cat_index`.`store_id` = 1 AND `cat_index`.`visibility` IN (2, 4)
INNER JOIN `catalog_product_super_link` parent ON parent.`parent_id`=e.`entity_id`
INNER JOIN `catalog_product_flat_all_1` s_e ON s_e.`entity_id`=parent.`product_id`
WHERE
`cat_index`.`category_id` IN (%s)
#and `s_e`.`entity_id`=9680
order by `s_e`.`entity_id`
";
        $sql=sprintf($sql,join(",",$FRAME_CATEGORY_IDS));
        $simples=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $i=0;
        $count=0;
        foreach ($simples as $key=>$simple) {
            $i = $i + 1;
            $sql = sprintf("select * from url_rewrite where (entity_type='pg_frame' or entity_type='pg_grant_frame') and entity_id=%s", $simple["simple_entity_id"]);
            $urlRewrites = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            foreach ($urlRewrites as $urlRewrite) {
                $count = $count + 1;
                $url_rewrite_id = $urlRewrite['url_rewrite_id'];
                $targetPathStr = $urlRewrite["target_path"];
                $targetPathParsedArr = UrlRewrite::parseParams($targetPathStr, 0);
                $simple_id = $targetPathParsedArr['simple_id'];
                $lens_type = $targetPathParsedArr['lens_type'];
                $frame_group = $targetPathParsedArr['frame_group'];
                $category_id = $targetPathParsedArr['category_id'];
                $tag = $targetPathParsedArr['tag'];
                $simple_rewrite["simple_id"] = $simple_id;
                $simple_rewrite["lens_type"] = $lens_type;
                $simple_rewrite["frame_group"] = $frame_group;
                $simple_rewrite["category_id"] = $category_id;
                $simple_rewrite["tag"] = $tag;
                $product=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($simple_id);
                $real_frame_group=$productHelper->get_frame_group($category_id,$product,$tag);
                $log=sprintf("count=%s,index=%s,simple_id=%s,frame_group=%s,real_frame_group=%s".PHP_EOL,count($simples),$i,$simple_id,$frame_group,$real_frame_group);
                echo $log;
                if ($frame_group!=$real_frame_group) {
                    //保存值与实际值不符，说明有录错的数据，删除
                    $sql=sprintf("delete from url_rewrite where url_rewrite_id=%s",$url_rewrite_id);
                    \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
                    $log=sprintf("url_rewrite_id=%s,simple_id=%s,frame_group=%s is error".PHP_EOL,$url_rewrite_id,$simple_id,$frame_group);
                    echo $log;
                }
                //检查是否有此lens_type
                $sql=sprintf("select count(0) as cnt from pg_lens_type WHERE lens_type_code='%s' AND frame_group='%s'",$lens_type,$frame_group);
                $cnt = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryScalar();
                if ($cnt==0) {
                    //说明已经没有此lens_type，删除
                    $sql=sprintf("delete from url_rewrite where url_rewrite_id=%s",$url_rewrite_id);
                    \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
                    $log=sprintf("url_rewrite_id=%s,simple_id=%s,frame_group=%s is error".PHP_EOL,$url_rewrite_id,$simple_id,$frame_group);
                    echo $log;
                }
            }
        }
    }

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionCacheIndex($div=10,$mod=0)
    {
        $param = ["div"=>$div,"mod"=>$mod];
        return $this->cache_index($param);
    }

    public function actionCacheClean() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $dr=\Magento\Framework\App\ObjectManager::getInstance()->get("Magento\Framework\Filesystem\DirectoryList");
        $root_path=$dr->getRoot()."/pub/accessories";
    }

    public function actionT1() {
        $sql="select id,price_included from pg_lens_type";
        $as=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($as as $r) {
            $r_e=json_decode($r["price_included"],true);
            $r_e[]="Polarized clip-on accessories/accessories in the picture";
            $r_s=json_encode($r_e);
            Yii::$app->db->createCommand(sprintf("update pg_lens_type set clipon_price_included='%s'",json_encode($r_s)))->execute();
        }
    }

    public function actionCacheIndex2() {
        $root_path=Yii::$app->params["magento_root_path"]."/pub";
        if (!file_exists($root_path."/eyeglasses")) {
            @mkdir($root_path."/eyeglasses",0777,true);
        }
        if (!file_exists($root_path."/eyeglasses/women")) {
            @mkdir($root_path."/eyeglasses/women",0777,true);
        }
        if (!file_exists($root_path."/eyeglasses/men")) {
            @mkdir($root_path."/eyeglasses/men",0777,true);
        }
        if (!file_exists($root_path."/eyeglasses/kids")) {
            @mkdir($root_path."/eyeglasses/kids",0777,true);
        }
        if (!file_exists($root_path."/sunglasses")) {
            @mkdir($root_path."/sunglasses",0777,true);
        }
        if (!file_exists($root_path."/sunglasses/women")) {
            @mkdir($root_path."/sunglasses/women",0777,true);
        }
        if (!file_exists($root_path."/sunglasses/men")) {
            @mkdir($root_path."/sunglasses/men",0777,true);
        }
        if (!file_exists($root_path."/sunglasses/kids")) {
            @mkdir($root_path."/sunglasses/kids",0777,true);
        }

        $t1=date('Y-m-d h:i:s', time());
        $workers = 10;
        $pids = array();
        for($i = 0; $i < $workers; $i++){
            $pids[$i] = pcntl_fork();
            switch ($pids[$i]) {
                case -1:
                    echo "fork error : {$i} \r\n";
                    exit;
                case 0:
                    $param = ["div"=>$workers,"mod"=>$i];
                    $this->cache_index($param);
                    exit;
                default:
                    break;
            }
        }
        foreach ($pids as $i => $pid) {
            if($pid) {
                pcntl_waitpid($pid, $status);
            }
        }
        $t2=date('Y-m-d h:i:s', time());
        echo sprintf("t1=%s,t2=%s".PHP_EOL,$t1,$t2);
        if (!empty($this->cache_index_dingding_message)) {
            $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
            $now=$localeDate->date()->format('Y-m-d H:i:s');
            $monitor_log=$objectManager->create("Pg\Monitor\Log");
            $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
            $dingding_yunying_chat_id = $_scopeConfig->getValue('config/general/dingding_yunying_chat_id',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
            $monitor_log->sendDingDingMessage(sprintf("%s 生成新PDP错误记录%s%s",$now,PHP_EOL,$this->cache_index_dingding_message),$dingding_yunying_chat_id);
        }
    }

    private function memory_info($header) {
//        echo $header.':运行后内存：'.memory_get_usage(true).PHP_EOL;
    }

    public function cache_index($param) {
        $div=$param["div"];
        $mod=$param["mod"];
        $this->memory_info("1");
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $VCAR=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("VCAR1");
        $LRD50=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LRD50");
        $LRD59=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LRD59");
        $LFC56=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LFC56");
        $LFC59=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LFC59");
        $LSB592=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LSB592");
        $LSB591=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProductPriceInfo("LSB591");
        $dp_data=[
            "VCAR"=>$VCAR,
            "LRD50"=>$LRD50,
            "LRD59"=>$LRD59,
            "LFC56"=>$LFC56,
            "LFC59"=>$LFC59,
            "LSB592"=>$LSB592,
            "LSB591"=>$LSB591,
        ];
        $param_info["dp_data"]=$dp_data;

        $dr=$objectManager->get("Magento\Framework\Filesystem\DirectoryList");
        $root_path=$dr->getRoot()."/pub";

        $FRAME_CATEGORY_IDS=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["FRAME_CATEGORY_IDS"];
        $sql="SELECT
`e`.`entity_id` AS configurable_id,
`e`.`attribute_set_id` AS configurable_attribute_set_id,
`e`.`type_id` AS configurable_type_id,
`e`.`sku` AS configurable_sku,
`e`.`name` AS configurable_name,
`cat_index`.`category_id`,
`s_e`.`entity_id` AS simple_entity_id,
`s_e`.`type_id` AS simple_type_id,
`s_e`.`sku` AS simple_sku,
`s_e`.`name` AS simple_name
FROM
`catalog_product_flat_all_1` AS e
INNER JOIN `catalog_category_product_index` AS cat_index ON `cat_index`.`product_id` = `e`.`entity_id` AND `cat_index`.`store_id` = 1 AND `cat_index`.`visibility` IN (2, 4)
INNER JOIN `catalog_product_super_link` parent ON parent.`parent_id`=e.`entity_id`
INNER JOIN `catalog_product_flat_all_1` s_e ON s_e.`entity_id`=parent.`product_id`
WHERE
`cat_index`.`category_id` IN (%s) 
#and `s_e`.`entity_id` in (5497)
and `s_e`.`entity_id` %% %s = %s
order by `s_e`.`entity_id`";
        $sql=sprintf($sql,join(",",$FRAME_CATEGORY_IDS),$div,$mod);
        $simples=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $i=0;
        $count=0;
        $this->memory_info("2");
        foreach ($simples as $key=>$simple) {
            $i=$i+1;
            $this->memory_info("3");
            $sql=sprintf("select * from url_rewrite where (entity_type='pg_frame' or entity_type='pg_grant_frame') and entity_id=%s",$simple["simple_entity_id"]);
            $urlRewrites=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
            $this->memory_info("4");
            foreach ($urlRewrites as $urlRewrite) {
                $count=$count+1;
                $this->memory_info("5");
                try {
                    $url_rewrite_id = $urlRewrite['url_rewrite_id'];
                    $targetPathStr = $urlRewrite["target_path"];
                    $targetPathParsedArr = UrlRewrite::parseParams($targetPathStr,0);
                    $simple_id = $targetPathParsedArr['simple_id'];
                    $lens_type = $targetPathParsedArr['lens_type'];
                    $frame_group = $targetPathParsedArr['frame_group'];
                    $category_id = $targetPathParsedArr['category_id'];
                    $tag = $targetPathParsedArr['tag'];
                    $simple_rewrite["simple_id"]=$simple_id;
                    $simple_rewrite["lens_type"]=$lens_type;
                    $simple_rewrite["frame_group"]=$frame_group;
                    $simple_rewrite["category_id"]=$category_id;
                    $simple_rewrite["tag"]=$tag;
                    $this->memory_info("6");
                    echo sprintf("div=%s,mod=%s,count=%s,index=%s,count=%s,request_path=%s,simple_id=%s,category_id=%s,".PHP_EOL,$div,$mod,count($simples),$i,$count,$urlRewrite["request_path"],$simple_id,$category_id);
                    $data=Yii::$container->get("app\components\helper\FrameHelper")->renderHtml($simple_rewrite,$param_info);
                    $html=$this->renderPartial("view",$data);
                    $file=$data["simple_url"];
                    $path=dirname($file);
                    $file_name=basename($file);
//                    echo sprintf("url=%s\r\nfilename=%s,simple_id=%s\r\n\r\n",$data["simple_url"],$file_name,$simple["simple_entity_id"]);
                    $cache_file_path=$root_path."/".$path;
                    $cache_file_name=$cache_file_path."/".$file_name;
                    if(!file_exists($cache_file_path)) {
                        @mkdir($cache_file_path,0777,true);
                    }
                    file_put_contents($cache_file_name,$html);
                    $data=null;
                    $targetPathParsedArr=null;
                    $targetPathStr=null;
                    $this->memory_info("7");
                } catch (\Exception $e) {
                    $log=sprintf("message=%s%sfile=%s,line=%s".PHP_EOL,$e->getMessage(),PHP_EOL,$e->getFile(),$e->getLine());
                    echo $e->getTraceAsString().PHP_EOL;
                    echo $log;
                    $this->cache_index_dingding_message=$this->cache_index_dingding_message.sprintf("div=%s,mod=%s,count=%s,index=%s,count=%s,request_path=%s,simple_id=%s,category_id=%s".PHP_EOL,$div,$mod,count($simples),$i,$count,$urlRewrite["request_path"],$simple_id,$category_id);
                }
            }
            $urlRewrites=null;
        }
        return ExitCode::OK;
    }
}
