<?php
namespace app\commands;

use app\components\helper\ESHelper;
use app\models\CoreConfigData;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\components\helper\MagentoProductHelper;

class ESController extends Controller
{
    private $ACCESSORIES_ATTRIBUTE_SET_IDS;
    private $FRAME_CATEGORY_IDS;
    private $ACCESSORIES_CATEGORY_ID;
    private $all_tags;
    private $frame_simple_urls;
    private $all_colors;

    private function getES(){
        $esClient=ESHelper::getInstance()->getES();
        return $esClient;
    }

    public function actionIndex() {
        $client=$this->getES();
        $params = [
            'index' => 'pg_catalog_product',
            'body' => [
                "settings"=>[
                    "analysis"=>[
                        "normalizer"=>[
                            "lowercase"=>[
                                "type"=> "custom",
                                "filter"=>[
                                    "lowercase"
                                ]
                            ]
                        ]
                    ]
                ],
                "mappings"=>[
                    'properties' => [
                        'configurable_id' => [
                            'type' => 'long'
                        ],
                        'attribute_set_id' => [
                            'type' => 'integer'
                        ],
                        'type_id' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'sku' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'category_id' => [
                            'type' => 'integer'
                        ],
                        'catetory' => [
                            'type' => 'text'
                        ],
                        'parent_catetory' => [
                            'type' => 'text'
                        ],
                        'cat_index_position' => [
                            'type' => 'integer'
                        ],
                        'name' => [
                            'type' => 'text'
                        ],
                        'short_description' => [
                            'type' => 'text'
                        ],
                        'description' => [
                            'type' => 'text'
                        ],
                        'image' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'small_image' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'thumbnail' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'bridge' => [
                            'type' => 'integer'
                        ],
                        'bridge_value' => [
                            'type' => 'integer'
                        ],
                        'temple_length_value' => [
                            'type' => 'integer'
                        ],
                        'rim_type' => [
                            'type' => 'keyword'
                        ],
                        'rim_type_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'frame_shape' => [
                            'type' => 'keyword'
                        ],
                        'frame_shape_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase",
                        ],
                        'frame_type_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'min_pd' => [
                            'type' => 'integer'
                        ],
                        'max_pd' => [
                            'type' => 'integer'
                        ],
                        'min_sph_value' => [
                            'type' => 'integer'
                        ],
                        'max_sph_value' => [
                            'type' => 'integer'
                        ],
                        'frame_weight_value' => [
                            'type' => 'integer'
                        ],
                        'support_progressive' => [
                            'type' => 'boolean'
                        ],
                        'lens_width_value' => [
                            'type' => 'integer'
                        ],
                        'lens_height_value' => [
                            'type' => 'integer'
                        ],
                        'frame_size_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'simeple_material_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'material' => [
                            'type' => 'keyword'
                        ],
                        'material_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'frame_width_value' => [
                            'type' => 'integer'
                        ],
                        'other_colors' => [
                            'type' => 'text'
                        ],
                        'age_range_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'nose_bridge_value' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'created_at' => [
                            "type"=>"date",
                            "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                        ],
                        'only_blue_light_blocking' => [
                            'type' => 'boolean'
                        ],
                        'has_spring_hinges' => [
                            'type' => 'boolean'
                        ],
                        'nose_pad' => [
                            'type' => 'boolean'
                        ],
                        'is_clip_on' => [
                            'type' => 'boolean'
                        ],
                        'support_rx' => [
                            'type' => 'boolean'
                        ],
                        'color_changing' => [
                            'type' => 'boolean'
                        ],
                        'low_bridge_fit' => [
                            'type' => 'boolean'
                        ],
                        'recommend_value' => [
                            'type' => 'boolean'
                        ],
                        'is_retired' => [
                            'type' => 'boolean'
                        ],
                        'create_time' => [
                            "type"=>"date",
                            "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                        ],
                        'is_goggles' => [
                            'type' => 'boolean'
                        ],
                        'row_number' => [
                            'type' => 'integer'
                        ],
                        'url' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'product_type' => [
                            'type' => 'keyword',
                            "normalizer"=> "lowercase"
                        ],
                        'try_on' => [
                            'type' => 'boolean'
                        ],
                        'is_retired' => [
                            'type' => 'boolean'
                        ],
                        'tags1' => [
                            "properties"=>[
                                'tag' => [
                                    'type' => 'text'
                                ]
                            ]
                        ],
                        'tags2' => [
                            "properties"=>[
                                'tag' => [
                                    'type' => 'text'
                                ]
                            ]
                        ],
                        "simples"=>[
                            "properties"=>[
                                'entity_id' => [
                                    'type' => 'long'
                                ],
                                'sku' => [
                                    'type' => 'keyword',
                                    "normalizer"=> "lowercase"
                                ],
                                'parent_id' => [
                                    'type' => 'long'
                                ],
                                'type_id' => [
                                    'type' => 'keyword',
                                    "normalizer"=> "lowercase"
                                ],
                                'color_value' => [
                                    'type' => 'keyword',
                                    "normalizer"=> "lowercase"
                                ],
                                'color_id' => [
                                    'type' => 'keyword'
                                ],
                                'color' => [
                                    "properties"=>[
                                        'option_id' => [
                                            'type' => 'text'
                                        ],
                                        'color' => [
                                            'type' => 'text'
                                        ],
                                        'color_value' => [
                                            'type' => 'keyword',
                                            "normalizer"=> "lowercase"
                                        ],
                                        'type' => [
                                            'type' => 'keyword',
                                            "normalizer"=> "lowercase"
                                        ]
                                    ]
                                ],
                                'name' => [
                                    'type' => 'text'
                                ],
                                'price' => [
                                    'type' => 'float'
                                ],
                                'sales_price' => [
                                    'type' => 'float'
                                ],
                                'special_from_date' => [
                                    "type"=>"date",
                                    "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                                ],
                                'special_to_date' => [
                                    "type"=>"date",
                                    "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                                ],
                                'short_description' => [
                                    'type' => 'text'
                                ],
                                'image' => [
                                    'type' => 'keyword',
                                    "normalizer"=> "lowercase"
                                ],
                                'stock_sku' => [
                                    'type' => 'keyword',
                                    "normalizer"=> "lowercase"
                                ],
                                'support_php_value' => [
                                    'type' => 'boolean'
                                ],
                                'create_time' => [
                                    "type"=>"date",
                                    "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                                ],
                                'visible_plp_list' => [
                                    'type' => 'boolean'
                                ],
                                'is_retired' => [
                                    'type' => 'boolean'
                                ],
                                'is_in_stock' => [
                                    'type' => 'boolean'
                                ],
                                'try_on' => [
                                    'type' => 'boolean'
                                ],
                                "frame_simple_url"=>[
                                    "type"=>"nested"
                                ],
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $response=$client->indices()->exists(['index' => 'pg_catalog_product']);
        if ($response) {
            $response = $client->indices()->delete(['index' => 'pg_catalog_product']);
        }
        $response = $client->indices()->create($params);
        print_r($response);
    }

    private function get_all_product_sql() {
        $sql="SELECT
`e`.`entity_id`,
`e`.`attribute_set_id`,
`e`.`type_id`,
`e`.`sku`,
`cat_index`.`category_id`,
`cat`.`name` AS category,
`parent_cat`.name AS parent_category,
`cat_index`.`position` AS `cat_index_position`,
`e`.`name`,
`e`.`description`,
`e`.`short_description`,
`e`.`special_price`,
`e`.`image`,
`e`.`small_image`,
`e`.`thumbnail`,
`e`.`url_key`,
`e`.`bridge`,
`e`.`bridge_value`,
`e`.`temple_length`,
`e`.`temple_length_value`,
`e`.`rim_type`,
`e`.`rim_type_value`,
`e`.`frame_shape`,
`e`.`frame_shape_value`,
`e`.`frame_type`,
`e`.`frame_type_value`,
`e`.`min_pd`,
`e`.`max_pd`,
`e`.`min_sph`,
`e`.`min_sph_value`,
`e`.`max_sph`,
`e`.`max_sph_value`,
`e`.`frame_weight`,
`e`.`frame_weight_value`,
`e`.`lens_width`,
`e`.`lens_width_value`,
`e`.`lens_height`,
`e`.`lens_height_value`,
`e`.`frame_size`,
`e`.`frame_size_value`,
`e`.`simeple_material`,
`e`.`simeple_material_value`,
`e`.`material`,
`e`.`material_value`,
`e`.`frame_width`,
`e`.`frame_width_value`,
`e`.`other_colors`,
`e`.`age_range`,
`e`.`age_range_value`,
`e`.`nose_bridge`,
`e`.`nose_bridge_value`,
`e`.`color_details`,
`e`.`created_at`,
`e`.`sw_featured`,
`e`.`featured_position`,
`e`.`sun_featured`,
`e`.`sun_featured_position`,
`e`.`trendy`,
`e`.`trendy_position`,
if (`e`.`support_progressive` is not null,1,0) as `support_progressive`,
if (`e`.`only_blue_light_blocking` = 1,1,0) as `only_blue_light_blocking`,
if (`e`.`has_spring_hinges` = 1,1,0) as `has_spring_hinges`,
if (`e`.`nose_pad` = 1,1,0) as `nose_pad`,
if (`e`.`is_clip_on` = 1,1,0) as `is_clip_on`,
if (`e`.`support_rx` = 1,1,0) as `support_rx`,
if (`e`.`color_changing` = 1,1,0) as `color_changing`,
if (`e`.`low_bridge_fit` = 1,1,0) as `low_bridge_fit`,
if (`e`.`recommend_value` is not null,`e`.`recommend_value`,'No') as `recommend_value`,
IF (`e`.`retired` = 1,1,0) AS `is_retired`,
`product`.`created_at` as create_time,
`product`.`updated_at` as update_time,
`price_index`.`price`,
`price_index`.`final_price`,
IF(`price_index`.`tier_price` IS NOT NULL, LEAST(`price_index`.`min_price`, `price_index`.`tier_price`), `price_index`.`min_price`) AS 'minimal_price',
`price_index`.`min_price`,
`price_index`.`max_price`,
`price_index`.`customer_group_id`,
if (`e`.`attribute_set_id` = 33,1,0) as `is_goggles`
FROM
`catalog_product_flat_all_1` AS e
INNER JOIN `catalog_category_product_index` AS cat_index ON `cat_index`.`product_id` = `e`.`entity_id` AND `cat_index`.`store_id` = 1 AND `cat_index`.`visibility` IN (2, 4) 
INNER JOIN `catalog_product_index_price` AS price_index ON `price_index`.`entity_id` = `e`.`entity_id` AND `price_index`.`website_id` = 1 AND `price_index`.`customer_group_id` = 0
INNER JOIN catalog_product_entity AS product on  e.entity_id = product.entity_id
INNER JOIN cataloginventory_stock_item AS stock ON stock.product_id=e.`entity_id`
INNER JOIN `catalog_category_flat_store_1` cat ON cat.`entity_id`=cat_index.`category_id`
INNER JOIN `catalog_category_flat_store_1` parent_cat ON parent_cat.`entity_id`=cat.`parent_id`
LEFT JOIN catalog_product_entity_int parent_attr2 on parent_attr2.entity_id=e.entity_id and parent_attr2.attribute_id=(SELECT attribute_id FROM eav_attribute WHERE entity_type_id=4 AND attribute_code='retired')
WHERE
e.`entity_id` IN (SELECT entity_id FROM `catalog_product_entity_int` WHERE attribute_id=(select attribute_id from eav_attribute where entity_type_id=4 and attribute_code='status') AND store_id=0)
AND e.type_id='configurable'
#AND (parent_attr2.value is null or parent_attr2.value!=1)
AND `cat_index`.`category_id` in (%s)";
        $FRAME_CATEGORY_IDS=$this->FRAME_CATEGORY_IDS;
        $ACCESSORIES_CATEGORY_ID=$this->ACCESSORIES_CATEGORY_ID;
        $sql=sprintf($sql,join(",",array_merge($FRAME_CATEGORY_IDS,[$ACCESSORIES_CATEGORY_ID])));
        return $sql;
    }

    private function get_simple_products_sql($configure_ids) {
        $sql="SELECT
  `e`.`entity_id`,
  `e`.`sku`,
  `link_table`.`parent_id`,
  `stock_table`.`qty`,
  `stock_table`.`is_in_stock`,
  `simple_prod`.`type_id`,
  `simple_prod`.`color`,
  `simple_prod`.`color_value`,
  `simple_prod`.`name`,
  `simple_prod`.`price`,
  `simple_prod`.`special_price`,
  `simple_prod`.`special_from_date`,
  `simple_prod`.`special_to_date`,
  `simple_prod`.`price_type`,
  `simple_prod`.`description`,
  `simple_prod`.`short_description`,
  `simple_prod`.`image`,
  `simple_prod`.`url_key`,
  `simple_prod`.`support_ditto`,
  `simple_prod`.`stock_sku`,
  `simple_prod`.`support_php`,
  if (`simple_prod`.`only_blue_light_blocking` = 1,1,0) as `only_blue_light_blocking`,
  if (`simple_prod`.`support_php` = 1,1,0) as `support_php_value`,
  if (`simple_prod`.`try_on` = 1,1,0) as `try_on`,
  if (`simple_prod`.`retired` = 1,1,0) as `is_retired`,
  if (`simple_prod`.`visible_plp_list` = 1,1,0) as `visible_plp_list`,
  if ( stock_table.`is_in_stock` = 1,1,0) as `is_in_stock`,
  `cpe`.`created_at` as create_time,
  `cpe`.`updated_at` as update_time
FROM
  `catalog_product_entity` AS e
    INNER JOIN `catalog_product_super_link` AS link_table ON `link_table`.`product_id` = `e`.`entity_id`
    INNER JOIN `cataloginventory_stock_item` AS stock_table ON `stock_table`.`product_id` = `e`.`entity_id`
    INNER JOIN `catalog_product_flat_all_1` AS simple_prod ON `simple_prod`.`entity_id` = `e`.`entity_id`
    INNER JOIN `catalog_product_entity_int` AS ext ON `ext`.`entity_id` = `link_table`.`product_id` AND store_id=0 AND ext.`attribute_id`=(SELECT attribute_id FROM eav_attribute WHERE entity_type_id=4 AND attribute_code='status') AND ext.`value`=1
    INNER JOIN `catalog_product_entity` AS cpe ON cpe.entity_id=simple_prod.entity_id
    LEFT JOIN catalog_product_entity_int simple_attr2 on simple_attr2.entity_id=e.entity_id and simple_attr2.store_id=0 and simple_attr2.attribute_id=(SELECT attribute_id FROM eav_attribute WHERE entity_type_id=4 AND attribute_code='retired')
WHERE 1=1
and `link_table`.`parent_id` IN (%s)
order by `stock_table`.`is_in_stock` desc
";
        $sql=sprintf($sql,join(",",$configure_ids));
        return $sql;
    }

    private function __in_tag_array($tag,$tags) {
        foreach ($tags as $t) {
            if ($t["tag"]==$tag) {
                return true;
            }
        }
        return false;
    }

    private function get_pg_product_tag() {
        $this->all_tags=[];
        $sql=sprintf("SELECT product_id,tag FROM `pg_product_tag`");
        $rows=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($rows as $row) {
            $this->all_tags[$row["product_id"]][]["tag"]=$row["tag"];
            if (!$this->__in_tag_array(str_replace("-","",$row["tag"]),$this->all_tags[$row["product_id"]])) {
                $this->all_tags[$row["product_id"]][]["tag"]=str_replace("-","",$row["tag"]);
            }
            if (!$this->__in_tag_array(str_replace(" ","",$row["tag"]),$this->all_tags[$row["product_id"]])) {
                $this->all_tags[$row["product_id"]][]["tag"]=str_replace("-","",$row["tag"]);
            }
        }
        return $this->all_tags;
    }

    private function get_tags_by_configurable_id($id) {
        if (isset($this->all_tags[$id])) {
            return $this->all_tags[$id];
        } else {
            return null;
        }
    }

    private function load_env_cfg() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->FRAME_CATEGORY_IDS=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["FRAME_CATEGORY_IDS"];
        $this->ACCESSORIES_ATTRIBUTE_SET_IDS=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["ACCESSORIES_ATTRIBUTE_SET_IDS"];
        $this->ACCESSORIES_CATEGORY_ID=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["ACCESSORIES_CATEGORY_ID"];
    }

    private function get_product_type($configurable) {
        if (in_array($configurable["category_id"],$this->FRAME_CATEGORY_IDS)) {
            return "frame";
        } else if (in_array($configurable["attribute_set_id"],$this->ACCESSORIES_ATTRIBUTE_SET_IDS)) {
            return "accessories";
        }
    }

    private function get_frame_simple_urls() {
        $this->frame_simple_urls=[];
        $sql=sprintf("SELECT entity_id,request_path as url,lens_type FROM `url_rewrite` WHERE entity_type='pg_frame'");
        $urls=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($urls as $url) {
            $this->frame_simple_urls[$url["entity_id"]][]=["url"=>$url["url"],"lens_type"=>$url["lens_type"]];
        }
        return $this->frame_simple_urls;
    }

    private function get_frame_simple_url($simple_id) {
        $result=[];
        if (isset($this->frame_simple_urls[$simple_id])) {
            $urls=$this->frame_simple_urls[$simple_id];
            foreach ($urls as $url) {
                $result[$url["lens_type"]]=$url;
            }
            return $result;
        } else {
            return null;
        }
    }

    private function get_all_colors() {
        $sql="
 SELECT
  `main_table`.`option_id`, `tsv`.`value` AS 'color', `swatch_table`.`value` AS 'color_value',`swatch_table`.type
FROM
  `eav_attribute_option` AS main_table
    LEFT JOIN `eav_attribute_option_value` AS tsv ON `tsv`.`option_id` = `main_table`.`option_id` AND `tsv`.`store_id` = 0
    LEFT JOIN `eav_attribute_option_swatch` AS swatch_table ON `swatch_table`.`option_id` = `main_table`.`option_id` AND `swatch_table`.`store_id` = 0
WHERE
  (`attribute_id` = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='color')) AND (`tsv`.`store_id` = 0)
ORDER BY `tsv`.`value` ASC";
        $colors=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($colors as $color) {
            $this->all_colors[$color["option_id"]]=$color;
        }
    }

    public function actionPutIndex(){
        $this->load_env_cfg();
        $this->get_pg_product_tag();
        $this->get_frame_simple_urls();
        $this->get_all_colors();

        $all_product_sql=$this->get_all_product_sql();
        $configurables=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($all_product_sql)->queryAll();
        $i=0;
        foreach ($configurables as $key=>$configurable) {
            $i++;
            $log=sprintf("loaddb.count=%s,index=%s".PHP_EOL,count($configurables),$i);
            echo $log;
            $this->get_tags_by_configurable_id($configurable["entity_id"]);
            $configurables[$key]["tags"]=$this->get_tags_by_configurable_id($configurable["entity_id"]);
            $simple_product_sql=$this->get_simple_products_sql([$configurable["entity_id"]]);
            $configurables[$key]["simples"]=\app\components\helper\DbHelper::getSlaveDb()->createCommand($simple_product_sql)->queryAll();
        }

        $bodys=[];
        $client=$this->getES();
        $i=0;
        foreach ($configurables as $configurable) {
            $i=$i+1;
            $log=sprintf("esindex.count=%s,index=%s".PHP_EOL,count($configurables),$i);
            echo $log;
            $product_type=$this->get_product_type($configurable);
            $tags2=[];
            if ($configurable["has_spring_hinges"]==1) {
                $tags2[count($tags2)]["tag"]="spring hinges";
            }
            if ($configurable["nose_pad"]==1) {
                $tags2[count($tags2)]["tag"]="nose pad";
            }
            if ($configurable["is_clip_on"]==1) {
                $tags2[count($tags2)]["tag"]="clipon";
            }
            if ($configurable["color_changing"]==1) {
                $tags2[count($tags2)]["tag"]="color changing";
            }
            if ($configurable["low_bridge_fit"]==1) {
                $tags2[count($tags2)]["tag"]="low bridge fit";
            }
            if ($configurable["is_goggles"]==1) {
                $tags2[count($tags2)]["tag"]="goggles";
            }
            $configurable["try_on"]=0;
            foreach ($configurable["simples"] as $simple) {
                if ($simple["try_on"]==1) {
                    $tags2[count($tags2)]["tag"]="tryon";
                    $tags2[count($tags2)]["tag"]="try_on";
                    $tags2[count($tags2)]["tag"]="try on";
                    $configurable["try_on"]=1;
                    break;
                }
            }
            reset($configurable["simples"]);
            $body=[
                "index"=>[
                    "_id"=>$configurable["entity_id"]
                ]
            ];
            $body1=[
                "configurable_id" => $configurable["entity_id"],
                "attribute_set_id" => $configurable["attribute_set_id"],
                "type_id" => $configurable["type_id"],
                "sku" => $configurable["sku"],
                "category_id" => $configurable["category_id"],
                "category" => $configurable["category"],
                "parent_category" => $configurable["parent_category"],
                "cat_index_position" => $configurable["cat_index_position"],
                "name" => $configurable["name"],
                "description" => $configurable["description"],
                "short_description" => $configurable["short_description"],
                "image" => $configurable["image"],
                "small_image" => $configurable["small_image"],
                "thumbnail" => $configurable["thumbnail"],
                "bridge_value" => $configurable["bridge_value"],
                "temple_length_value" => $configurable["temple_length_value"],
                "rim_type" => $configurable["rim_type"],
                "rim_type_value" => $configurable["rim_type_value"],
                "frame_shape" => $configurable["frame_shape"],
                "frame_shape_value" => $configurable["frame_shape_value"],
                "frame_type_value" => $configurable["frame_type_value"],
                "min_pd" => $configurable["min_pd"],
                "max_pd" => $configurable["max_pd"],
                "min_sph_value" => $configurable["min_sph_value"],
                "frame_weight_value" => $configurable["frame_weight_value"],
                "lens_width_value" => $configurable["lens_width_value"],
                "lens_height_value" => $configurable["lens_height_value"],
                "frame_size_value" => $configurable["frame_size_value"],
                "simeple_material_value" => $configurable["simeple_material_value"],
                "material" => $configurable["material"],
                "material_value" => $configurable["material_value"],
                "frame_width_value" => $configurable["frame_width_value"],
                "other_colors" => $configurable["other_colors"],
                "age_range_value" => $configurable["age_range_value"],
                "nose_bridge_value" => $configurable["nose_bridge_value"],
                "color_details" => $configurable["color_details"],
                "created_at" => $configurable["created_at"],
                "support_progressive" => ($configurable["support_progressive"]==1? true:false),
                "has_spring_hinges" => ($configurable["has_spring_hinges"]==1? true:false),
                "nose_pad" => ($configurable["nose_pad"]==1? true:false),
                "is_clip_on" => ($configurable["is_clip_on"]==1? true:false),
                "support_rx" => ($configurable["support_rx"]==1? true:false),
                "color_changing" => ($configurable["color_changing"]==1? true:false),
                "low_bridge_fit" => ($configurable["low_bridge_fit"]==1? true:false),
                "recommend_value" => (strtolower($configurable["recommend_value"])==strtolower("Yes")? true:false),
                "create_time" => $configurable["create_time"],
                "is_goggles" => ($configurable["is_goggles"]==1? true:false),
                "row_number" => $i,
                "product_type" => $product_type,
                "is_retired"=>$configurable["is_retired"]==1? true:false,
                "try_on"=>$configurable["try_on"]==1? true:false,
                "tags1"=>$configurable["tags"],
                "tags2"=>$tags2
            ];
            $param = [
                'index' => 'pg_catalog_product',
                'type' => '_doc',
                'id' => $configurable["entity_id"],
                'body'=>$body1
            ];
            $configurable_visible_plp_list=false;
            foreach ($configurable["simples"] as $simple) {
                if ($simple["visible_plp_list"]) {
                    $configurable_visible_plp_list=true;
                    break;
                }
            }
            foreach ($configurable["simples"] as $simple) {
                $dp=MagentoProductHelper::calcProductPriceByArray($simple);
                $s=[
                    'simple_id'=>$simple["entity_id"],
                    'sku'=>$simple["sku"],
                    'parent_id'=>$simple["parent_id"],
                    'type_id'=>$simple["type_id"],
                    'color_value'=>$simple["color_value"],
                    'color_id'=>$simple["color"],
                    'color'=>$this->all_colors[$simple["color"]],
                    'name'=>$simple["name"],
                    'price'=>round($simple["price"],2),
                    'special_price'=>round($simple["special_price"],2),
                    'sales_price'=>round($dp["sales_price"],2),
                    'special_from_date'=>$dp["special_from_date"],
                    'special_to_date'=>$dp["special_to_date"],
                    "description" => $simple["description"],
                    'short_description'=>$simple["short_description"],
                    'image'=>$simple["image"],
                    'stock_sku'=>$simple["stock_sku"],
                    'support_php_value'=>strtolower($simple["support_php_value"])==strtolower("Yes")? true:false,
                    'create_time'=>$simple["create_time"],
                    "is_retired"=>$simple["is_retired"]==1? true:false,
                    "visible_plp_list"=>$configurable_visible_plp_list? ($simple["visible_plp_list"]==1? true:false):true,
                    "is_in_stock"=>$simple["is_in_stock"]==1? true:false,
                    "try_on"=>$simple["try_on"]==1? true:false,
                    'frame_simple_url'=>$this->get_frame_simple_url($simple["entity_id"])
                ];
//                if ($simple["entity_id"]==3281) {
//                    print_r($s);
//                    exit;
//                }
                $param["body"]["simples"][]=$s;
                $body1["simples"][]=$s;
            }
            $bodys[]=$body;
            $bodys[]=$body1;
//            $response = $client->index($param);
//            print_r($response);
        }
        $params=[
            'index' => 'pg_catalog_product',
            'type' => '_doc',
            'body'=>$bodys
        ];
//        $delete_params = [
//            'index' => 'pg_catalog_product',
//            'type'  => '_doc',
//            'body' => [
//                "query"=> [
//                    "match_all" => new \stdClass()
//                ],
//            ]
//        ];
//        $client->deleteByQuery($delete_params);
        $response = $client->bulk($params);
    }

    public function actionDeleteAll(){
        $client=$this->getES();
        $delete_params = [
            'index' => 'pg_catalog_product',
            'type'  => '_doc',
            'body' => [
                "query"=> [
                    "match_all" => new \stdClass()
                ],
            ]
        ];
        $client->deleteByQuery($delete_params);
    }

    /**
     * 5.95价格搜索
     */
    public function actionSearch1(){
        $client=$this->getES();
        $params = [
            'index' => 'pg_catalog_product',
            'type' => '_doc',
            'body' => [
                "from"=>0,
                "size"=>2,
                "query"=>json_decode('{
    "query": {
        "function_score": {
            "query": {
                "bool": {
                    "filter": [
                        {
                            "term": {
                                "product_type": "frame"
                            }
                        },
                        {
                            "term": {
                                "simples.is_in_stock": "true"
                            }
                        },
                        {
                            "terms": {
                                "simples.is_in_stock": [
                                    "false"
                                ]
                            }
                        }
                    ],
                    "should": [
                        {
                            "match": {
                                "name": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "description": {
                                    "query": "14319U09",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "short_description": {
                                    "query": "14319U09",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "material_value": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "category": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "term": {
                                "sku": "14319U09"
                            }
                        },
                        {
                            "match": {
                                "rim_type_value": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "frame_shape_value": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "frame_type_value": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "frame_size_value": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "simeple_material_value": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "parent_category": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "simples.name": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "term": {
                                "simples.sku": "14319U09"
                            }
                        },
                        {
                            "match": {
                                "simples.color_value": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "simples.stock_sku": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "simples.description": {
                                    "query": "14319U09",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "simples.short_description": {
                                    "query": "14319U09",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "tags1.tag": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        },
                        {
                            "match": {
                                "tags2.tag": {
                                    "query": "14319U09",
                                    "fuzziness": "0",
                                    "boost": 1
                                }
                            }
                        }
                    ],
                    "minimum_should_match": 1
                }
            }
        }
    },
    "aggs": {
        "frame_shape_values": {
            "terms": {
                "field": "frame_shape",
                "size": 10000
            }
        },
        "colors": {
            "terms": {
                "field": "simples.color_id",
                "size": 10000
            }
        },
        "rim_types": {
            "terms": {
                "field": "rim_type",
                "size": 10000
            }
        },
        "materials": {
            "terms": {
                "field": "material",
                "size": 10000
            }
        }
    },
    "sort": {
        "_script": {
            "type": "number",
            "script": {
                "source": "if (doc[\'category_id\'].value==6 || doc[\'category_id\'].value==8 || doc[\'category_id\'].value==10) {return 0;} else {return 1;}"
            },
            "order": "asc"
        }
    }
}',true)["query"],
                "sort"=>[
                    [
                        "configurable_id"=>["order"=>"asc"]
                    ]
                ]
            ]
        ];
        $results = $client->search($params);
        foreach ($results["hits"]["hits"] as $result) {
            echo $result["_id"].PHP_EOL;
            print_r($result["_source"]["simples"]);
            echo PHP_EOL;
        }
    }

    /**
     * 搜索子表
     */
    public function actionSearch2(){
        $client=$this->getES();
        $params = [
            'index' => 'pg_catalog_product',
            'type' => '_doc',
            'body' => [
                "from"=>0,
                "size"=>200,
                "query"=> [
                    "bool" => [
                        "must"=>[
                            "match"=>["simples.color_value"=>"#000000"]
                        ],
                        "filter"=>[
                            "match"=>["simples.name"=>"Oval"],
                        ]
                    ]
                ],
                "sort"=>[
                    [
                        "configurable_id"=>["order"=>"asc"]
                    ]
                ]
            ]
        ];
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $results = $client->search($params);
        $t2=\app\components\helper\TimezoneHelper::msectime();
        $log=sprintf("t2-t1=%s".PHP_EOL,$t2-$t1);
        echo $log;
    }

    /**
     * select * from pg_catalog_product where is_retired=true and name like '%Bee%'
     */
    public function actionSearch3(){
        $client=$this->getES();
        $query = json_decode(trim('
{
  "query": {
    "bool": {
      "must": [
        {"term": {"is_retired":"true"}},
        {"match": {"name":"Bee"}}
      ]
    }
  }
}
'),true);
        $params = [
            'index' => 'pg_catalog_product',
//            'type' => '_doc',
            '_source' => ['name','is_retired','type_id'], // 请求指定的字段
            'body' => array_merge([
                'from' => 0,
                'size' => 5
            ],$query)
        ];
//        print_r($params);
        $results = $client->search($params);
//        echo json_encode($results).PHP_EOL;exit;
        print_r($results);exit;
        foreach ($results["hits"]["hits"] as $result) {
            echo $result["_id"].PHP_EOL;
            print_r($result["_source"]["simples"]);
            echo PHP_EOL;
        }
    }

    /**
     * select name,is_retired,type_id from pg_catalog_product where (is_retired=false and simples.is_in_stock=true and product_type='frame')
     * and (name like '%Bee%' or material_value like '%Bee%' or short_description like '%Bee%' or simeple_material_value like '%Bee%' or simples.name like '%Bee%')
     * order by configurable_id asc
     */
    public function actionSearch4(){
        $client=$this->getES();
        $query = json_decode(trim('
{
  "query": {
    "bool": {
      "must": [
        {"term": {"is_retired":"false"}},
        {"match":{"simples.is_in_stock":"true"}},
        {"term":{"product_type":"frame"}}
      ],
      "should": [
        {"match":{"name":"Pure"}},
        {"match":{"material_value":"Pure"}},
        {"match":{"short_description":"Pure"}},
        {"match":{"simeple_material_value":"Pure"}},
        {"match":{"simples.name":"Pure"}}
      ]
    }
  },
  "sort": [
    {
      "configurable_id": {"order":"asc"}
    }
  ]
}
'),true);
        $params = [
            'index' => 'pg_catalog_product',
            'type' => '_doc',
            '_source' => ['name','is_retired','type_id'], // 请求指定的字段
            'body' => array_merge([
                'from' => 0,
                'size' => 5
            ],$query)
        ];
        $t1=\app\components\helper\TimezoneHelper::msectime();
        $results = $client->search($params);
        $t2=\app\components\helper\TimezoneHelper::msectime();
        $log=sprintf("t2-t1=%s".PHP_EOL,$t2-$t1);
        echo $log;
        print_r($results);exit;
    }

    /**
     * select count(0) as cnt from pg_catalog_product where (is_retired=false and simples.is_in_stock=true and product_type='frame' and category_id=6)
     * and (name like '%black%' or material_value like '%black%' or short_description like '%black%' or simeple_material_value like '%black%' or simples.name like '%black%')
     */
    public function actionSearch5()
    {
        $client = $this->getES();
        $query = json_decode(trim('
{
  "query": {
    "bool": {
      "must": [
        {"term": {"is_retired":"false"}},
        {"match":{"simples.is_in_stock":"true"}},
        {"term":{"product_type":"frame"}},
        {"term":{"category_id":"6"}}
      ],
      "should": [
        {"match":{"name":"black"}},
        {"match":{"material_value":"black"}},
        {"match":{"short_description":"black"}},
        {"match":{"simeple_material_value":"black"}},
        {"match":{"simples.name":"black"}}
      ],
      "minimum_should_match":1
    }
  }
}
'), true);
        $params = [
            'index' => 'pg_catalog_product',
//            'type' => '_doc',
            'body' => array_merge([
            ], $query)
        ];
        $t1 = \app\components\helper\TimezoneHelper::msectime();
        $results = $client->count($params);
        $t2 = \app\components\helper\TimezoneHelper::msectime();
        $log = sprintf("t2-t1=%s" . PHP_EOL, $t2 - $t1);
        echo $log;
        print_r($results);
        exit;
    }

    public function actionSearch6()
    {
        $client = $this->getES();
        $query = json_decode(trim('
{
  "query": {
    "bool": {
      "filter": [
        {"term": {"is_retired":"false"}},
        {"match":{"simples.is_in_stock":"true"}},
        {"term":{"product_type":"frame"}},
        {"term":{"category_id":"8"}},
        {"range": {"price":{"gte": 5.95,"lte": 13.95}}}
      ], 
      "should": [
        {"match": {"name":{"query": "Oval","fuzziness":"Auto"}}},
        {"match": {"material_value":{"query": "Oval","fuzziness":"Auto"}}},
        {"match": {"short_description":{"query": "Ovl","fuzziness":"Auto"}}},
        {"match": {"simeple_material_value":{"query": "Ovl","fuzziness":"Auto"}}},
        {"match": {"simples.name":{"query": "Ovl","fuzziness":"Auto"}}},
        
        {"match": {"frame_shape_value":{"query": "round","boost": 1}}},
        {"match": {"material_value":{"query": "TR","boost": 1}}},
        {"match": {"simples.color_value":{"query": "Red","boost": 1}}}
      ],
      "minimum_should_match":2
    }
  }
}
'), true);
        $params = [
            'index' => 'pg_catalog_product',
            'body' => $query
        ];
        $t1 = \app\components\helper\TimezoneHelper::msectime();
        print_r($params);
        $results = $client->count($params);

        $params["_source"] =["configurable_id","name","simples.color_value","frame_shape_value","material_value","price","product_type"];
        $params["body"]["from"] = 0;
        $params["body"]["size"] = 2;
        $results = $client->search($params);
        $t2 = \app\components\helper\TimezoneHelper::msectime();
        $log = sprintf("t2-t1=%s" . PHP_EOL, $t2 - $t1);
        echo $log;
        print_r($results);
        exit;
    }

    public function actionSearch7(){
        $client=$this->getES();
        $query_str='
{
  "query": {
    "match_all":{"name","*"}
  }
}
        ';
        $query = json_decode(trim($query_str), true);
        $params = [
            'index' => 'pg_collection',
            'body' => $query,
//            "from"=>0,
//            "size"=>1
        ];
//        $results = $client->search($params);
//        print_r($results["hits"]["total"]);
        $results = $client->count($params);
        print_r($results);
    }
}
