<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\components\helper\AuthcodeHelper;
use app\components\helper\DbHelper;
use app\models\PgExchangeData;
use GuzzleHttp\Client as GuzzleClient;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class KlaviyoController extends Controller
{
    const STATUS_SUBSCRIBED = 1;//订阅
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;//取消订阅
    const STATUS_UNCONFIRMED = 4;
    const STATUS_CLEANED = 5;
    const STATUS_MAILCHIMP_ERROR = 6;//同步mailchimp出错
    const STATUS_HAS_CHINESE = 7;//有中文

    private $log_file="";
    private $start_subscriber_id=0;
    private $page_size=2000;
    private $every_time_size=100;
    private $total_count=0;
    private $c=[];
    private $authcode_secret="";

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    public function getWriteDb()
    {
        return DbHelper::getMasterDb();
    }

    private function savelog($log,$output=true) {
        if (!empty($this->log_file)) {
            file_put_contents($this->log_file,$log.PHP_EOL,FILE_APPEND);
        }
        if ($output) {
            $datetime = new \DateTime();
            echo $datetime->format('Y-m-d H:i:s')." -> ".$log.PHP_EOL;
        }
    }

    private function get_magento_member_list_sql($is_count=false,$param=["div"=>1,"mod"=>0]) {
        $div=$param["div"];
        $mod=$param["mod"];

        $sql = "SELECT
`t1`.`subscriber_id`,`t1`.`subscriber_email`, `t1`.`customer_id`
%s
FROM
`newsletter_subscriber` t1
  #LEFT JOIN `sales_order` s ON `s`.`customer_id` = `t1`.`customer_id`
  LEFT JOIN `customer_entity` c1 ON `c1`.`entity_id` = `t1`.`customer_id`
WHERE
`t1`.`subscriber_id` >= %s
and (t1.subscriber_status=1 or t1.subscriber_status=6 or t1.subscriber_status=7)
and (t1.subscriber_id %% {$mod} = {$div})
# and t1.subscriber_status=6
# AND t1.`subscriber_email`='dchernyshev@gmail.com'
#GROUP BY `subscriber_email`, `subscriber_id`, `customer_id` 
%s";
        $count_sql="SELECT count(0) as cnt
FROM
`newsletter_subscriber` t1
WHERE
`t1`.`subscriber_id` >= %s
and (t1.subscriber_status=1 or t1.subscriber_status=6 or t1.subscriber_status=7)
and (t1.subscriber_id %% {$mod} = {$div})
# and t1.subscriber_status=6
# AND t1.`subscriber_email`='dchernyshev@gmail.com'
";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $GLASSES_ATTRIBUTE_SET_IDS=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["GLASSES_ATTRIBUTE_SET_IDS"];
        $select_fields = ",`c1`.`firstname`
,`c1`.`lastname`
,`c1`.`gender`
,`c1`.`group_id`
,DATE_FORMAT(`c1`.`created_at`, '%Y-%m-%d %H:%i:%s') AS 'reg_date'
,(select DATE_FORMAT(MIN(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'first_purchase_date'
,(select DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'last_purchase_date'
,(SELECT DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') FROM sales_order WHERE sales_order.`customer_id`=`c1`.`entity_id` AND coupon_code IS NULL AND discount_amount=0 HAVING sum(subtotal)>5.95) AS last_non_exchange_order_date
,(SELECT COUNT(0) FROM `sales_order` so WHERE `so`.`customer_id` = `c1`.`entity_id`) AS 'orders_count'
,(SELECT COUNT(0) FROM sales_order_item
WHERE product_type='configurable'
AND glasses_prescription_id IN (SELECT entity_id FROM glasses_prescription WHERE ladd !=0.00 OR radd !=0.00)
AND order_id IN(SELECT entity_id FROM sales_order WHERE sales_order.customer_id=c1.entity_id)) AS progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND profile_prescription_id IN (SELECT entity_id FROM prescription_entity WHERE ladd !=0.00 OR radd !=0.00)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_count
,(select sum(qi.row_total)  as grand_total
from quote q
INNER JOIN quote_item qi on qi.quote_id=q.entity_id
inner join catalog_product_flat_1 flat on flat.entity_id=qi.product_id
where q.customer_id=c1.entity_id and qi.parent_item_id is null and flat.attribute_set_id in (".implode(",",$GLASSES_ATTRIBUTE_SET_IDS).")) AS quote_grand_total
,(select count(0)  as items_count
from quote q
INNER JOIN quote_item qi on qi.quote_id=q.entity_id
inner join catalog_product_flat_1 flat on flat.entity_id=qi.product_id
where q.customer_id=c1.entity_id and qi.parent_item_id is null and flat.attribute_set_id in (".implode(",",$GLASSES_ATTRIBUTE_SET_IDS).")) AS quote_items_count
,t1.subscriber_status
,(SELECT MAX(created_at) AS created_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_created_at
,(SELECT MAX(updated_at) AS updated_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_updated_at
,(SELECT COUNT(0) AS cnt FROM `sales_order` WHERE customer_id=c1.entity_id AND store_id=1 AND is_virtual=0 AND (state='processing' OR state='closed') AND created_at >= DATE_SUB(NOW(), INTERVAL 21 DAY)) AS three_weeks_purchase
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='simple') as frame_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='configurable') as glasses_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND soi.is_virtual=1) as craft_avg_price
,(select enabled_score from customer_entity where entity_id=c1.entity_id) as enabled_score
";
        $order_sql="ORDER BY `t1`.`subscriber_id` ASC";
        if ($is_count) {
            $sql = sprintf($count_sql, $this->start_subscriber_id, "");
        } else {
            $sql = sprintf($sql, $select_fields, $this->start_subscriber_id, $order_sql);
        }
        return $sql;
    }

    /**
     * 计算同步的总数量
     */
    private function get_magento_member_count($param){
        $count_sql=$this->get_magento_member_list_sql(true,$param);
        $cnt=$this->getDb()->createCommand($count_sql)->queryScalar();
        $total_page = ceil($cnt / $this->page_size);
        $this->total_count = (int)$cnt;
        $log=sprintf("total count=%s,total page=%s,page size=%s",$this->total_count,$total_page,$this->page_size);
        $this->savelog($log);
        return $total_page;
    }

    private function get_magento_member_list($param) {
        $sql = $this->get_magento_member_list_sql( False,$param);
        $sql = sprintf("%s limit %s,%s",$sql, 0, $this->page_size);
        $data=$this->getDb()->createCommand($sql)->queryAll();
        return $data;
    }

    private function toTime($d) {
        if (!empty($d)) {
            $pos=strrpos($d,"-");
            $fetch=substr($d,$pos,1);
            $fetch_v=(int)explode(":",substr($d,$pos+1,strlen($d)-$pos-1))[0];
            return date("Y-m-d H:i:s",strtotime(sprintf("%s%s hours",$fetch,$fetch_v),strtotime($d)));
        } else {
            return "";
        }
    }

    public function authcode_encode($data) {
        return base64_encode(AuthcodeHelper::encode($data,$this->authcode_secret));
    }

    public function authcode_decode($data) {
        return AuthcodeHelper::decode_ex($data);
    }

    public function update_subscriber_status($subscriber_id,$status) {
        $update_sql=sprintf("update newsletter_subscriber set subscriber_status=%s where subscriber_id=%s",$status,$subscriber_id);
        $this->getWriteDb()->createCommand($update_sql)->execute();
    }

    private function sync_by_list($list,$param) {
        $profiles=[];
        $i=0;//每次列表计数，每100次上传一次
        foreach ($list as $member) {
            $t1=msectime();
            $i=$i+1;
            $key="key_".$param["mod"]."_".$param["div"];
            if (!isset($this->c[$key])) {
                $this->c[$key]=0;
            }
            $this->c[$key]=$this->c[$key]+1;
            $this->start_subscriber_id = $member["subscriber_id"];
            $log = sprintf("total=%s,current=%s,subscriber_id=%s,email=%s,mod=%s,div=%s", $this->total_count,$this->c[$key],$member["subscriber_id"],$member["subscriber_email"],$param["mod"],$param["div"]);
            $this->savelog($log);

            $customer_id=$member["customer_id"];
            $customer_id_auth_code=$this->authcode_encode($customer_id);
            $reg_date=$member["reg_date"];
            $firstname = $member["firstname"];
            $lastname=$member["lastname"];
            $gender=$member["gender"];
            $genderstr="";
            if ($member["gender"]==null) {
                $gender="3";
                $genderstr = "Not Specified";
            } else if ($member["gender"]=="1") {
                $genderstr="Male";
            } else if ($member["gender"]=="2") {
                $genderstr="Female";
            } else if ($member["gender"]=="3") {
                $genderstr="Not Specified";
            } else {
                $gender = "3";
                $genderstr = "Not Specified";
            }
            $subscriber_email = strtolower($member["subscriber_email"]);
            $email_auth_code=$this->authcode_encode($subscriber_email);
            $subscriber_id = $member["subscriber_id"];
            $subscriber_id_auth_code=$this->authcode_encode($subscriber_id);
            $first_purchase_date = $member["first_purchase_date"];
            $last_purchase_date = $member["last_purchase_date"];
            $last_non_exchange_order_date = $member["last_non_exchange_order_date"];
            $orders_count = $member["orders_count"];
            $progressive_count = $member["progressive_count"];
            $quote_progressive_count = $member["quote_progressive_count"];
            $quote_count=$member["quote_count"];
            $quote_grand_total=$member["quote_grand_total"];
            $quote_items_count=$member["quote_items_count"];
            $magento_subscriber_status=$member["subscriber_status"];
            $quote_created_at=$member["quote_created_at"];
            $quote_updated_at = $member["quote_updated_at"];
            $frame_avg_price = $member["frame_avg_price"];
            $glasses_avg_price = $member["glasses_avg_price"];
            $craft_avg_price = $member["craft_avg_price"];
            $enabled_score = empty($member["enabled_score"])? 0:1;
            $log=sprintf("subscriber_id=%s,reg_date=%s,firstname=%s,lastname=%s,gender=%s,subscriber_email=%s,customer_id=%s,first_purchase_date=%s,last_purchase_date=%s,orders_count=%s,progressive_count=%s,genderstr=%s,subscriber_status=%s,quote_progressive_count=%s,quote_count=%s,quote_created_at=%s,quote_updated_at=%s,enabled_score=%s",
                $subscriber_id,$reg_date,$firstname,$lastname,$gender,$subscriber_email,$customer_id,$first_purchase_date,$last_purchase_date,$orders_count,$progressive_count,$genderstr,$magento_subscriber_status,$quote_progressive_count,$quote_count,$quote_created_at,$quote_updated_at,$enabled_score);
//            if ($enabled_score==1) {
//                $this->savelog($log);
//            }

            if (
                (empty($firstname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $firstname, $match))
                && (empty($lastname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $lastname, $match))
                && ($magento_subscriber_status==static::STATUS_SUBSCRIBED
                && $this->check_email($subscriber_email)
                )
            ) {
                $can_synd=true;
            } else {
                //有中文，回写到同步表
                $can_synd=false;
                $this->update_subscriber_status($subscriber_id,self::STATUS_HAS_CHINESE);
            }
            $can_synd=true;
            if ($can_synd) {
                //加入打开和点击次数
//                $maropost_data=Yii::$container->get("app\components\helper\KlaviyoHelper")->load_maropost_info($subscriber_email);
                $profiles[]=[
                    "email"=>$subscriber_email,
                    "craft_avg_price"=>round($craft_avg_price,2),
                    "customer_id"=>$customer_id,
                    "first_purchase_date"=>$first_purchase_date,
                    "frame_avg_price"=>$frame_avg_price,
                    "gender_str"=>$genderstr,
                    "glasses_avg_price"=>round($glasses_avg_price,2),
                    "last_purchase_date"=>$last_purchase_date,
                    "order_count"=>$orders_count,
                    "progressive_count"=>$progressive_count,
                    "quote_count"=>$quote_count,
                    "quote_grand_total"=>$quote_grand_total,
                    "quote_items_count"=>$quote_items_count,
                    "quote_progressive_count"=>$quote_progressive_count,
                    "quote_created_at"=>$quote_created_at,
                    "quote_updated_at"=>$quote_updated_at,
                    "register_date"=>$reg_date,
                    "subscriber_id"=>$subscriber_id,
                    "customer_id_auth_code"=>$customer_id_auth_code,
                    "email_auth_code"=>$email_auth_code,
                    "subscriber_id_auth_code"=>$subscriber_id_auth_code,
                    "subscribe_status"=>true,
                    "email"=>$subscriber_email,
                    "first_name"=>$firstname,
                    "last_name"=>$lastname,
                    "last_non_exchange_order_date"=>$last_non_exchange_order_date,
                    "enabled_score"=>$enabled_score,
//                    "last_visit_time"=>$maropost_data["last_visit_time"],
//                    "contract_clicks_cnt"=>$maropost_data["contract_clicks_cnt"],
//                    "contract_opens_cnt"=>$maropost_data["contract_opens_cnt"],
//                    "contract_clicks_recent_date"=>$maropost_data["contract_clicks_recent_date"],
//                    "contract_opens_recent_date"=>$maropost_data["contract_opens_recent_date"]
                ];
            }
            if ($i==$this->every_time_size) {
                $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->createOrUpdateContract($profiles);
                $profiles=[];
                $i=0;
            }
        }
        if (!empty($profiles)) {
            $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->createOrUpdateContract($profiles);
        }
    }

    private function check_email($email) {
        $result = trim($email);
        if (filter_var($result, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * 同步
     */
    private function sync($param) {
//        $this->start_subscriber_id=237310;
        $total_page=$this->get_magento_member_count($param);
        for ($page_no=0; $page_no<$total_page;$page_no++) {
            $list=$this->get_magento_member_list($param);
            $this->sync_by_list($list,$param);
        }
    }

    public function actionSync($log_file="/lihf/klaviyo.log",$mod=1,$div=0) {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        if ($log_file!="") {
            $this->log_file=$log_file;
        }

        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $this->savelog("start sync ...",true);

        $param = ["mod"=>$mod,"div"=>$div];
        $this->sync($param);

        return ExitCode::OK;
    }

    public function actionSync2($log_file="/lihf/klaviyo.log",$mod=5) {
//        $this->start_subscriber_id=231967;
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        if ($log_file!="") {
            $this->log_file=$log_file;
        }

        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $this->savelog("start sync ...",true);

        $t1=date('Y-m-d h:i:s', time());
        $workers = $mod;
        $pids = array();
        for($i = 0; $i < $workers; $i++){
            $pids[$i] = pcntl_fork();
            switch ($pids[$i]) {
                case -1:
                    echo "fork error : {$i} \r\n";
                    exit;
                case 0:
                    $param = ["mod"=>$workers,"div"=>$i];
                    $this->sync($param);
                    exit;
                default:
                    break;
            }
        }
        foreach ($pids as $i => $pid) {
            if($pid) {
                pcntl_waitpid($pid, $status);
            }
        }
        $t2=date('Y-m-d h:i:s', time());

        return ExitCode::OK;
    }

    public function actionInitAllEvent() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $customField=[];

        $customer_properties=array_merge(['$email'=>"lihaifeng832008@163.com",'$first_name'=>"li",'$last_name'=>"haifeng"],$customField);
        $properties=$customField;

        $events=[
//            ["event_id"=>"rewards delievered time","description"=>"发送妥投邮件给积分用户"],
            ["event_id"=>"rewards invitation","description"=>"邀请下单用户成功积分用户(字段变更触发)"],
            ["event_id"=>"rewards_verify","description"=>"join now，积分用户申请"],
            ["event_id"=>"rewards active more than 6000","description"=>"订单生效，积分大于等于6000时邮件"],
            ["event_id"=>"rewards active less than 6000","description"=>"订单生效，积分小于6000时邮件"],
//            ["event_id"=>"review-reward","description"=>"积分用户，订单妥投后，没有投诉，给用户发邮件评论"],
//            ["event_id"=>"review-non reward","description"=>"非积分用户，订单妥投后，没有投诉，给用户发邮件评论"],
            ["event_id"=>"out_for_delivery","description"=>"美国out for delivery时发送邮件"],
            ["event_id"=>"cross_country_transfer","description"=>""],
//            ["event_id"=>"new user registration","description"=>"积分用户接受协议"],
            ["event_id"=>"forget-password","description"=>"忘记密码"],
        ];
        foreach ($events as $key=>$event) {
            $event_id=$event["event_id"];
            $description=$event["description"];
            $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->track($event_id,$customer_properties,$properties);
            echo sprintf("event_id=%s,ret=%s".PHP_EOL,$event_id,$ret);
        }
    }
}
