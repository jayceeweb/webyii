<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use \Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use app\components\helper\AuthcodeHelper;
use app\components\helper\DbHelper;
use app\models\PgExchangeData;
use app\models\CustomerEntity;
use app\components\helper\SftpHelper;
use GuzzleHttp\Client as GuzzleClient;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MaropostController extends Controller
{
    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
    const LIST_ID=3;

    const STATUS_SUBSCRIBED = 1;//订阅
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;//取消订阅
    const STATUS_UNCONFIRMED = 4;
    const STATUS_CLEANED = 5;
    const STATUS_MAILCHIMP_ERROR = 6;//同步mailchimp出错
    const STATUS_HAS_CHINESE = 7;//有中文

    private $log_file="";
    private $divisor=0;
    private $remainder=0;
    private $start_subscriber_id=0;
    private $page_size=2000;
    private $total_count=0;
    private $c=0;
    private $authcode_secret="";

    private $maropost_api=null;

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    private function savelog($log,$output=true) {
        if (!empty($this->log_file)) {
            file_put_contents($this->log_file,$log.PHP_EOL,FILE_APPEND);
        }
        if ($output) {
            $datetime = new \DateTime();
            echo $datetime->format('Y-m-d H:i:s')." -> ".$log.PHP_EOL;
        }
    }

    private function get_magento_member_list_sql($is_count=false) {
        $sql = "SELECT
`t1`.`subscriber_id`,`t1`.`subscriber_email`, `t1`.`customer_id`
%s
FROM
`newsletter_subscriber` t1
  #LEFT JOIN `sales_order` s ON `s`.`customer_id` = `t1`.`customer_id`
  LEFT JOIN `customer_entity` c1 ON `c1`.`entity_id` = `t1`.`customer_id`
WHERE
`t1`.`subscriber_id` >= %s
and (t1.subscriber_status=1 or t1.subscriber_status=6)
# and t1.subscriber_status=6
# AND t1.`subscriber_email`='skwagen@aol.com'
#GROUP BY `subscriber_email`, `subscriber_id`, `customer_id` 
%s";
        $count_sql="SELECT count(0) as cnt
FROM
`newsletter_subscriber` t1
WHERE
`t1`.`subscriber_id` >= %s
and (t1.subscriber_status=1 or t1.subscriber_status=6)
# and t1.subscriber_status=6
# AND t1.`subscriber_email`='skwagen@aol.com'
";

        $select_fields = ",`c1`.`firstname`
,`c1`.`lastname`
,`c1`.`gender`
,`c1`.`group_id`
,DATE_FORMAT(`c1`.`created_at`, '%Y-%m-%d %H:%i:%s') AS 'reg_date'
,(select DATE_FORMAT(MIN(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'first_purchase_date'
,(select DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'last_purchase_date'
,(SELECT DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') FROM sales_order WHERE sales_order.`customer_id`=`c1`.`entity_id` AND coupon_code IS NULL AND discount_amount=0 HAVING sum(subtotal)>5.95) AS last_non_exchange_order_date
,(SELECT COUNT(0) FROM `sales_order` so WHERE `so`.`customer_id` = `c1`.`entity_id`) AS 'orders_count'
,(SELECT COUNT(0) FROM sales_order_item
WHERE product_type='configurable'
AND glasses_prescription_id IN (SELECT entity_id FROM glasses_prescription WHERE ladd !=0.00 OR radd !=0.00)
AND order_id IN(SELECT entity_id FROM sales_order WHERE sales_order.customer_id=c1.entity_id)) AS progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND profile_prescription_id IN (SELECT entity_id FROM prescription_entity WHERE ladd !=0.00 OR radd !=0.00)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_count
,t1.subscriber_status
,(SELECT MAX(created_at) AS created_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_created_at
,(SELECT MAX(updated_at) AS updated_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_updated_at
,(SELECT COUNT(0) AS cnt FROM `sales_order` WHERE customer_id=c1.entity_id AND store_id=1 AND is_virtual=0 AND (state='processing' OR state='closed') AND created_at >= DATE_SUB(NOW(), INTERVAL 21 DAY)) AS three_weeks_purchase
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='simple') as frame_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='configurable') as glasses_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND soi.is_virtual=1) as craft_avg_price";
        $order_sql="ORDER BY `t1`.`subscriber_id` ASC";
        if ($is_count) {
            $sql = sprintf($count_sql, $this->start_subscriber_id, "");
        } else {
            $sql = sprintf($sql, $select_fields, $this->start_subscriber_id, $order_sql);
        }
        return $sql;
    }

    /**
     * 计算同步的总数量
     */
    private function get_magento_member_count(){
        $count_sql=$this->get_magento_member_list_sql(true);
        $cnt=$this->getDb()->createCommand($count_sql)->queryScalar();
        $total_page = ceil($cnt / $this->page_size);
        $this->total_count = (int)$cnt;
        $log=sprintf("total count=%s,total page=%s,page size=%s",$this->total_count,$total_page,$this->page_size);
        $this->savelog($log);
        return $total_page;
    }

    private function get_magento_member_list() {
        $sql = $this->get_magento_member_list_sql( False);
        $sql = sprintf("%s limit %s,%s",$sql, 0, $this->page_size);
        $data=$this->getDb()->createCommand($sql)->queryAll();
        return $data;
    }

    private function toTime($d) {
        if (!empty($d)) {
            $pos=strrpos($d,"-");
            $fetch=substr($d,$pos,1);
            $fetch_v=(int)explode(":",substr($d,$pos+1,strlen($d)-$pos-1))[0];
            return date("Y-m-d H:i:s",strtotime(sprintf("%s%s hours",$fetch,$fetch_v),strtotime($d)));
        } else {
            return "";
        }
    }

    private function compareDateEqual($d1,$d2) {
        if ((empty($d1) || ($d1=="0000-00-00 00:00:00"))
            || (empty($d2) || ($d2=="0000-00-00 00:00:00"))
        ) {
            return true;
        }
        if (empty($d1)) {
            $d1="0000-00-00 00:00:00";
        }
        if (empty($d2)) {
            $d2="0000-00-00 00:00:00";
        }
        $d1=$this->toTime($d1);
        if (empty($d1)) {
            $d1="0000-00-00 00:00:00";
        }
        if (empty($d2)) {
            $d2="0000-00-00 00:00:00";
        }
        if ($d1==$d2) {
            return true;
        } else {
            return false;
        }
    }

    public function authcode_encode($data) {
        return base64_encode(AuthcodeHelper::encode($data,$this->authcode_secret));
    }

    public function authcode_decode($data) {
        return AuthcodeHelper::decode_ex($data);
    }

    private function sync_by_list($list,$file_name) {
        foreach ($list as $member) {
            $t1=msectime();
            $this->c=$this->c+1;
            $this->start_subscriber_id = $member["subscriber_id"];
            $log = sprintf("total %s records,now is process the %s record...", $this->total_count,$this->c);
            $this->savelog($log);

            $customer_id=$member["customer_id"];
            $customer_id_auth_code=$this->authcode_encode($customer_id);
            $reg_date=$member["reg_date"];
            $firstname = $member["firstname"];
            $lastname=$member["lastname"];
            $gender=$member["gender"];
            $genderstr="";
            if ($member["gender"]==null) {
                $gender="3";
                $genderstr = "Not Specified";
            } else if ($member["gender"]=="1") {
                $genderstr="Male";
            } else if ($member["gender"]=="2") {
                $genderstr="Female";
            } else if ($member["gender"]=="3") {
                $genderstr="Not Specified";
            } else {
                $gender = "3";
                $genderstr = "Not Specified";
            }
            $subscriber_email = strtolower($member["subscriber_email"]);
            $email_auth_code=$this->authcode_encode($subscriber_email);
            $subscriber_id = $member["subscriber_id"];
            $subscriber_id_auth_code=$this->authcode_encode($subscriber_id);
            $first_purchase_date = $member["first_purchase_date"];
            $last_purchase_date = $member["last_purchase_date"];
            $last_non_exchange_order_date = $member["last_non_exchange_order_date"];
            $orders_count = $member["orders_count"];
            $progressive_count = $member["progressive_count"];
            $quote_progressive_count = $member["quote_progressive_count"];
            $quote_count=$member["quote_count"];
            $magento_subscriber_status=$member["subscriber_status"];
            $quote_created_at=$member["quote_created_at"];
            $quote_updated_at = $member["quote_updated_at"];
            $frame_avg_price = $member["frame_avg_price"];
            $glasses_avg_price = $member["glasses_avg_price"];
            $craft_avg_price = $member["craft_avg_price"];
            $log=sprintf("subscriber_id=%s,reg_date=%s,firstname=%s,lastname=%s,gender=%s,subscriber_email=%s,customer_id=%s,first_purchase_date=%s,last_purchase_date=%s,orders_count=%s,progressive_count=%s,genderstr=%s,subscriber_status=%s,quote_progressive_count=%s,quote_count=%s,quote_created_at=%s,quote_updated_at=%s",
                $subscriber_id,$reg_date,$firstname,$lastname,$gender,$subscriber_email,$customer_id,$first_purchase_date,$last_purchase_date,$orders_count,$progressive_count,$genderstr,$magento_subscriber_status,$quote_progressive_count,$quote_count,$quote_created_at,$quote_updated_at);
            $this->savelog($log);

            $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
//            $contract=$this->maropost_api->getForEmail($subscriber_email);
//            $can_synd=false;
//            $contractData=null;
//            if ($contract->isSuccess!=1) {
//                $can_synd=true;
//            } else if (
//                (empty($firstname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $firstname, $match))
//                && (empty($lastname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $lastname, $match))
//                && ($magento_subscriber_status==static::STATUS_SUBSCRIBED)
//            ) {
//                $contractData=$contract->getData();
//                if (
//                    $contractData->first_name!=$firstname
//                    || $contractData->last_name!=$lastname
//                    || (int)$contractData->customer_id!=(int)$customer_id
//                    || $contractData->subscriber_id!=$subscriber_id
//                    || !$this->compareDateEqual($contractData->last_purchase_date,$last_purchase_date)
//                    || !$this->compareDateEqual($contractData->last_non_exchange_order_date,$last_non_exchange_order_date)
//                    || !$this->compareDateEqual($contractData->register_date,$reg_date)
//                    || $contractData->gender_str!=$genderstr
//                    || (int)$contractData->order_count!=(int)$orders_count
//                    || (int)$contractData->progressive_count!=(int)$progressive_count
//                    || !$this->compareDateEqual($contractData->first_purchase_date,$first_purchase_date)
//                    || (int)$contractData->quote_progressive_count!=(int)$quote_progressive_count
//                    || (int)$contractData->quote_count!=(int)$quote_count
//                    || !$this->compareDateEqual($contractData->quote_updated_at,$quote_updated_at)
//                    || round($contractData->frame_avg_price,2)!=round($frame_avg_price,2)
//                    || round($contractData->glasses_avg_price,2)!=round($glasses_avg_price,2)
//                    || round($contractData->craft_avg_price,2)!=round($craft_avg_price,2)
//                    || !$this->compareDateEqual($contractData->quote_created_at,$quote_created_at)
//                ) {
//                    $can_synd=true;
//                }
//            }
            if (
                (empty($firstname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $firstname, $match))
                && (empty($lastname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $lastname, $match))
                && ($magento_subscriber_status==static::STATUS_SUBSCRIBED)
            ) {
                $can_synd=true;
            } else {
                $can_synd=false;
            }
            //对比是否需要更新
//            if ($contract->isSuccess) {
//                $contractData=$contract->getData();
//            }
            if ($can_synd) {
                $customField=[
                    "craft_avg_price"=>round($craft_avg_price,2),
                    "customer_id"=>$customer_id,
                    "first_purchase_date"=>$first_purchase_date,
                    "frame_avg_price"=>$frame_avg_price,
                    "gender_str"=>$genderstr,
                    "glasses_avg_price"=>round($glasses_avg_price,2),
                    "last_purchase_date"=>$last_purchase_date,
                    "order_count"=>$orders_count,
                    "progressive_count"=>$progressive_count,
                    "quote_count"=>$quote_count,
                    "quote_progressive_count"=>$quote_progressive_count,
                    "quote_created_at"=>$quote_created_at,
                    "quote_updated_at"=>$quote_updated_at,
                    "register_date"=>$reg_date,
                    "subscriber_id"=>$subscriber_id,
                    "customer_id_auth_code"=>$customer_id_auth_code,
                    "email_auth_code"=>$email_auth_code,
                    "subscriber_id_auth_code"=>$subscriber_id_auth_code,
                    "subscribe_status"=>true,
                    "email"=>$subscriber_email,
                    "first_name"=>$firstname,
                    "last_name"=>$lastname
                ];
                $t2=msectime();
                echo sprintf("t2-t1=%s".PHP_EOL,($t2-$t1));
                foreach ($customField as $key => $value){
                    file_put_contents('/data/magento/cache/'.$file_name,$value.',',FILE_APPEND);
                }
                file_put_contents('/data/magento/cache/'.$file_name,"\n",FILE_APPEND);
//                $contract=$this->maropost_api->createOrUpdateForList(static::LIST_ID,$subscriber_email,$firstname,$lastname,null,null,$subscriber_id,$customField);
                $t3=msectime();
                echo sprintf("t3-t2=%s".PHP_EOL,($t3-$t2));
            }
        }
        $this->actionTest($file_name);
    }

    private function check_subscribe_status ($contractData) {
        if (!empty($contractData)) {
            if (isset($contractData->list_subscriptions) && is_array($contractData->list_subscriptions)) {

            }
        }
    }

    private function sync_csv() {
        $field="email\tfirstname\tlast_name\tphone\tstatus\tcraft_avg_price\tcustomer_id\tfirst_purchase_date\tframe_avg_price\t\gender_str";
        $field=$field."\tglasses_avg_price\tlast_purchase_date\torder_count\tprogressive_count\tquote_count\tquote_progressive_count\tquote_created_at\tquote_updated_at\tregister_date\tsubscriber_id";
        file_put_contents("/lihf/maropost.csv",$field.PHP_EOL);
    }

    /**
     * 同步
     */
    private function sync() {
        $total_page=$this->get_magento_member_count();
        for ($page_no=0; $page_no<$total_page;$page_no++) {
            $filename = $page_no.'.csv';
            $str_content="craft_avg_price,customer_id,first_purchase_date,frame_avg_price,gender_str,glasses_avg_price,last_purchase_date,order_count,progressive_count,quote_count,quote_progressive_count,quote_created_at,quote_updated_at,register_date,subscriber_id,customer_id_auth_code,email_auth_code,subscriber_id_auth_code,subscribe_status,email,first_name,last_name\n";
            file_put_contents('/data/magento/cache/'.$filename,$str_content);
            $list=$this->get_magento_member_list();
            $this->sync_by_list($list,$filename);
        }
    }

    public function actionSync($log_file="/lihf/maropost.log",$divisor="5",$remainder="0") {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        if ($log_file!="") {
            $this->log_file=$log_file;
        }
        if ($divisor!="") {
            $this->divisor=$divisor;
        }
        if ($remainder!="") {
            $this->remainder=$remainder;
        }
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();
        $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
        $this->savelog("start sync ...",true);

//        $this->sync_csv();
        $this->sync();

        return ExitCode::OK;
    }
    public function actionTest($file) {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

//        echo $this->authcode_decode("MDFmYldRRDd3Y2RuQzMwcktHYzNXeFQrWDZLVlFwdmZnRUZMaE5pTWgxaz0=").PHP_EOL;

//        $ftp=new SftpHelper();
//        $ftp->login("ftp.maropost.com",8410,"username","ftpuser_2160","e868707e0859e158f779");
//        $ftp->upload_file("/data/webyii/www/commands/list_export_14.csv","/uploads/list_export_14.csv");

        $srcFile = "/data/magento/cache/".$file;
        $dstFile = "/uploads/list_export_14.csv";

        $host = 'ftp.maropost.com';
        $port = '8410';
        $username = 'ftpuser_2160';
        $password = 'e868707e0859e158f779';

        $conn = ssh2_connect($host, $port);
        ssh2_auth_password($conn, $username, $password);
        $sftp = ssh2_sftp($conn);

        $sftpStream = fopen('ssh2.sftp://'.$sftp.$dstFile, 'w');
        try {
            if (!$sftpStream) {
                throw new Exception("Could not open remote file: $dstFile");
            }
            $data_to_send = file_get_contents($srcFile);
            if ($data_to_send === false) {
                throw new Exception("Could not open local file: $srcFile.");
            }
            if (fwrite($sftpStream, $data_to_send) === false) {
                throw new Exception("Could not send data from file: $srcFile.");
            }
            fclose($sftpStream);
        } catch (Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            fclose($sftpStream);
        }

        $url=sprintf("https://api.maropost.com/accounts/2160/data_journeys/2/trigger/57407792072717.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ");
        $client = new GuzzleClient();
        $res = $client->request('POST', $url);
        $err_no=$res->getBody()->getContents();
        echo $file."ftp传完".PHP_EOL;
//        $monitor_log=new \Pg\Monitor\Log();
//        $monitor_log->log_start("lihaifeng-test");
//        $monitor_log->log(["tags"=>"asdf"]);
//        $monitor_log->log_stop("asdf");

//        $contact=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
//        $c=$contact->getForEmail("email-marketing@payneglasses.com")->getData();
//        print_r($c);

//        $emails="email-marketing@payneglasses.com";
//        $emails=explode("\r\n",$emails);
//        foreach ($emails as $email) {
//            if (!empty($email)) {
//                echo $email.PHP_EOL;
//                $errno=$contact->createOrUpdateForList(3,$email,
//                    null,
//                    null,
//                    null,null,null,
//                    [
//                        "tmp_event_type"=>"remake"
//                    ]
//                );
//                $errno=$contact->getForEmail("zhangyaqin_0@163.com")->getData();
//                print_r($errno);
//            }
//        }


//        $b=new TransactionalCampaigns(static::ACCOUNT_ID, static::AUTHTOKEN);
//        $err_no=$b->sendEmail(21,null,null,null,null,null,null,null,$c->getData()->id);
//        print_r($err_no);
    }

    private function getForList($page) {
        $list=$this->maropost_api->getForList(static::LIST_ID,$page);
        while (!$list->isSuccess) {
            usleep(1000);
            $list=$this->maropost_api->getForList(static::LIST_ID,$page);
        }
        return $list;
    }

    private function getClicks($id,$page) {
        $times=0;
        $clicks=$this->maropost_api->getClicks($id,$page);
        while (!$clicks->isSuccess) {
            $times=$times+1;
            usleep(1000);
            $clicks=$this->maropost_api->getClicks($id,$page);
            if ($times>10) {
                $clicks=null;
            }
        }
        return $clicks;
    }

    private function getOpens($id,$page) {
        $times=0;
        $opens=$this->maropost_api->getOpens($id,$page);
        while (!$opens->isSuccess) {
            $times=$times+1;
            usleep(1000);
            $opens=$this->maropost_api->getOpens($id,$page);
            if ($times>10) {
                $opens=null;
            }
        }
        if ($opens->errorMessage=="Contact is not present!") {
            $opens=null;
        }
        return $opens;
    }

    private function getForEmail($email) {
        $times=0;
        $contract=$this->maropost_api->getForEmail($email);
        while (!$contract->isSuccess && ($contract->errorMessage!="Contact is not present!")) {
            $times=$times+1;
            usleep(1000);
            $contract=$this->maropost_api->getForEmail($email);
            if ($times>10) {
                $contract=null;
            }
        }
        if ($contract->errorMessage=="Contact is not present!") {
            $contract=null;
        }
        return $contract;
    }

    /**
     * 下载maropost数据
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionDownloadContractData() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);

        $contract_total_pages=0;

        $list=$this->getForList(1);
        if ($list->isSuccess==1) {
            $contracts=$list->getData();
            $contract_total_pages=$contracts[0]->total_pages;
        }
        //循环每一页
        for ($i=762;$i<=$contract_total_pages;$i++) {
            $list=$this->getForList($i);
            if ($list->isSuccess==1) {
                $contracts=$list->getData();
                $contract_cnt=0;
                $current_page_i=0;//当前页的条数索引
                foreach ($contracts as $key=>$contract) {
                    $current_page_i=$current_page_i+1;
                    $contract_cnt=$contract_cnt+1;
                    $log=sprintf("email=%s,contract_id=%s,当前页=%s,共%s页,每页200条，当前第%s条",$contract->email,$contract->id,$i,$contract_total_pages,$current_page_i);
                    echo $log.PHP_EOL;
                    if ($contract_cnt==1) {
                        $contract_total_pages=$contract->total_pages;
                    }
                    $contract_for_email=$this->getForEmail($contract->email);
//                    echo print_r($contract,true).PHP_EOL;
//                    echo print_r($c,true).PHP_EOL;
//                    exit();

                    //取点击数
                    $contract_clicks_data=[];//要记录的点击数
                    $contract_clicks_page=0;
                    $contract_clicks_d=$this->getClicks($contract->id,1);
                    if (!empty($contract_clicks_d)) {
                        $contract_clicks_d_t=$contract_clicks_d;
                        if ($contract_clicks_d->isSuccess==1 && !empty($contract_clicks_d->getData())) {
                            $contract_clicks_page=$contract_clicks_d->getData()[0]->total_pages;
                        }
                        for ($click_page=1;$click_page<=$contract_clicks_page;$click_page++) {
                            if ($click_page==1) {
                                $contract_clicks_d=$contract_clicks_d_t;
                            } else {
                                $contract_clicks_d=$this->getClicks($contract->id,$click_page);
                            }
                            if ($contract_clicks_d->isSuccess==1) {
                                $contract_clicks=$contract_clicks_d->getData();
                                if (!empty($contract_clicks)) {
                                    $contract_clicks_data[]=$contract_clicks;
//                                echo print_r($contract_clicks,true).PHP_EOL;
                                }
                            }
                        }
                    }
                    //取打开数
                    $contract_opens_data=[];//要记录的打开数
                    $contract_opens_page=0;
                    $contract_opens_d=$this->getOpens($contract->id,1);
                    if (!empty($contract_opens_d)) {
                        $contract_opens_d_t=$contract_opens_d;
                        if ($contract_opens_d->isSuccess==1 && !empty($contract_opens_d->getData())) {
                            $contract_opens_page=$contract_opens_d->getData()[0]->total_pages;
                        }
                        for ($openk_page=1;$openk_page<=$contract_opens_page;$openk_page++) {
                            if ($openk_page==1) {
                                $contract_opens_d=$contract_opens_d_t;
                            } else {
                                $contract_opens_d=$this->getOpens($contract->id,$openk_page);
                            }
                            if ($contract_opens_d->isSuccess==1) {
                                $contract_opens=$contract_opens_d->getData();
                                if (!empty($contract_opens)) {
                                    $contract_opens_data[]=$contract_opens;
//                                echo print_r($contract_opens,true).PHP_EOL;
                                }
                            }
                        }
                    }
                    $db_data=[
                        "contract"=>$contract,
                        "contract_for_email"=>!empty($contract_for_email)? $contract_for_email->getData():[],
                        "contract_clicks_data"=>$contract_clicks_data,
                        "contract_opens_data"=>$contract_opens_data,
                    ];
//                    if (!empty($db_data["contract_clicks_data"]) && !empty($db_data["contract_opens_data"])) {
//                        echo print_r($db_data,true).PHP_EOL;
//                        exit();
//                    }
                    $cnt=Yii::$app->db->createCommand("select count(0) as cnt from pg_maropost_data where email=:email")->bindParam(":email",$contract->email)->queryScalar();
                    if ($cnt==0) {
                        $result =Yii::$app->db->createCommand()->insert('pg_maropost_data', [
                            'email'=>$contract->email,
                            'content'=> json_encode($db_data),
                        ])->execute();
                    } else {
                        $result =Yii::$app->db->createCommand()->update('pg_maropost_data', [
                            'email'=>$contract->email,
                            'content'=> json_encode($db_data),
                        ],"email=:email",array(':email'=>$contract->email))->execute();
                    }
//                    echo print_r($result,true).PHP_EOL;
                }
            }
        }
    }
}
