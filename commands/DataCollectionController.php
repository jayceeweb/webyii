<?php
namespace app\commands;

use app\components\helper\ESHelper;
use app\models\CoreConfigData;
use app\models\EventsCollection;
use app\models\EventsUsersMap;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\components\helper\MagentoProductHelper;
use Magento\Quote\Api\CartItemRepositoryInterface as QuoteItemRepository;

class DataCollectionController extends Controller
{
    private function getES(){
        $esClient=ESHelper::getInstance()->getES();
        return $esClient;
    }

    public function actionProcess(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        while (true) {
            $list_len=Yii::$app->redis_cache->llen("LIST_DATA_COLLECTION");
            $list=Yii::$app->redis_cache->brpop("LIST_DATA_COLLECTION",10);
            if (!empty($list)) {
                foreach ($list as $key=>$value) {
                    $d=json_decode($value,true);
                    try {
                        if (strtolower($d["action"])==strtolower("productview")) {
                            $r=$this->productview_index($d["data"]);
                            echo print_r($r,true).PHP_EOL;
                        }
                    } catch (\Exception $e) {
                        echo $e->getMessage().PHP_EOL;
                    }
                }
            }
        }
    }

    private function productview_index($data){
        foreach ($data as $k1=>$v1) {
            foreach ($v1 as $vk_1 =>$vv1) {
                $body[$k1][$vk_1]=$vv1;
            }
        }
        $body["base"]["action"]="productview";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $body["base"]["deal_time"]=$localeDate->date()->format('Y-m-d H:i:s');

        $client=$this->getES();
        $params=[
            'index' => 'pg_collection',
            'type' => '_doc',
            "body"=>$body
        ];
        $response = $client->index($params);
    }

    //20201124数据埋点事件数据处理
    public function actionOpCollection() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $redis = $this->redisObj();
        $listKey = "COLLECTION_LISTS";
        while (true) {
//            $list_len=Yii::$app->redis_cache->llen($listKey);
//            $list=Yii::$app->redis_cache->brpop($listKey,1);
            $list_len = $redis->llen($listKey);
            $list = $redis->brPop($listKey, 1);
            echo count($list).PHP_EOL;
            if (count($list) > 1 && !empty($list[1])) {
                try {
                    //解析json事件数据转换为数组格式
                    $collectionJson = json_decode($list[1], true);
                    $collectionData = json_decode($collectionJson['data'], true);
                    //整理入库字段
                    $cData = $this->_formatCollectionData($collectionData);
                    if($cData['customer_id'] > 0) {
                        //查询是否已匹配，未匹配则进行匹配
                        $where = ['device_id'=>$cData['device_id'], 'customer_id'=>$cData['customer_id']];
                        $eventsUsersMapRes = EventsUsersMap::getList($where);
                        if(empty($eventsUsersMapRes)) {
                            $eventsUsersMapData = array(
                                'device_id' =>  $cData['device_id'],
                                'customer_id'   =>  $cData['customer_id'],
                                'date'  =>  date("Y-m-d H:i:s")
                            );
                            //记录匹配信息
                            EventsUsersMap::addData($eventsUsersMapData);
                        }
                    }
                    //处理商品sku关联数据
                    if(!empty($cData['sku'])) {
                        $goodsInfo = '';
                    }
                    if(!empty($cData['device_id'])) {
                        //埋点事件数据入库
                        EventsCollection::addData($cData);
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage().PHP_EOL;
                }
            }
        }
        return true;
    }

    //整理匹配埋点事件入库字段
    public function _formatCollectionData($data)
    {
        if (empty($data)) return false;
        $newData = [];
        //前端本地唯一标记(可理解为设备id)
        if (isset($data['deviceId'])) $newData['device_id'] = $data['deviceId'];
        //用户中心ID
        if (isset($data['customerId'])) $newData['customer_id'] = $data['customerId'];
        //验光单ID
        if (isset($data['profileId'])) $newData['profile_id'] = $data['profileId'];
        //浏览器（只显示"chrome"、"opera"、"ie"、"qq"、"firefox"）
        if (isset($data['browser'])) $newData['browser'] = $data['browser'];
        //浏览器版本
        if (isset($data['browserVersion'])) $newData['browser_version'] = $data['browserVersion'];
        //当前访问页面的域名
        if (isset($data['currentDomain'])) $newData['current_domain'] = $data['currentDomain'];
        //当前访问页面的url
        if (isset($data['currentUrl'])) $newData['current_url'] = $data['currentUrl'];
        //当前访问页面的标题
        if (isset($data['title'])) $newData['title'] = $data['title'];
        //当前访问页面的路径
        if (isset($data['urlPath'])) $newData['url_path'] = $data['urlPath'];
        //客户端操作系统
        if (isset($data['deviceOs'])) $newData['device_os'] = $data['deviceOs'];
        //客户端操作系统版本
        if (isset($data['deviceOsVersion'])) $newData['device_os_version'] = $data['deviceOsVersion'];
        //客户端平台（桌面、安卓、ios）
        if (isset($data['devicePlatform'])) $newData['device_platform'] = $data['devicePlatform'];
        //上报事件的id
        if (isset($data['eventId'])) $newData['event_id'] = $data['eventId'];
        //事件类型
//        if (isset($data['dataType'])) $newData['data_type'] = $data['dataType'];
        //本地客户端语言
        if (isset($data['language'])) $newData['language'] = $data['language'];
        //网页打开场景（浏览器、APP）
//        if (isset($data['pageOpenScene'])) $newData['page_open_scene'] = $data['pageOpenScene'];
        //用户首次访问网站时间戳--格式化后的日期格式
        if (isset($data['persistedTime'])) $newData['persisted_datetime'] = $data['persistedTime'];
        //上一页url（来源页url）
        if (isset($data['referrer'])) $newData['referrer'] = $data['referrer'];
        //上一页域名（来源页域名）
        if (isset($data['referringDomain'])) $newData['referring_domain'] = $data['referringDomain'];
        //本地客户端屏幕高度（像素）
        if (isset($data['screenHeight'])) $newData['screen_height'] = $data['screenHeight'];
        //本地客户端屏幕宽度（像素）
        if (isset($data['screenWidth'])) $newData['screen_width'] = $data['screenWidth'];
        //当前会话的id(前端浏览器)
        if (isset($data['sessionUuid'])) $newData['session_uuid'] = $data['sessionUuid'];
        //当前上报事件用户触发的时间--单位：s
        if (isset($data['costTime'])) $newData['cost_time'] = $data['costTime'];
        //上报数据凭证（通过它来归类数据）
        if (isset($data['token'])) $newData['token'] = $data['token'];
        //访问次数
        if (isset($data['sessionCount'])) $newData['session_count'] = $data['sessionCount'];
        //整个网站停留时间
        if (isset($data['stayTime'])) $newData['stay_time'] = intval($data['stayTime'] / 1000);
        //网站页面停留时间
        if (isset($data['stayPageTime'])) $newData['stay_page_time'] = intval($data['stayPageTime'] / 1000);
        //广告 (google:1)
        if (isset($data['gclid'])) $newData['gclid'] = $data['gclid'];
        //广告 (google: Cj0KCQiAk53-BRD0ARIsAJuNhps7O4PgHbmhNN3vYTerWkMZX7SL6jjhVn9a9L8msuWTVWA1AlH11X0aAiNmEALw_wcB
        if (isset($data['brand'])) $newData['brand'] = $data['brand'];
        //渠道推广的广告id（本地若有数据的话）
        if (isset($data['promotionalID'])) $newData['promotional_id'] = $data['promotionalID'];
        //渠道推广的广告名称（本地若有数据的话）
        if (isset($data['utmCampaign'])) $newData['utm_campaign'] = $data['utmCampaign'];
        //渠道推广的广告媒介（本地若有数据的话）
        if (isset($data['utmMedium'])) $newData['utm_medium'] = $data['utmMedium'];
        //渠道推广的广告来源（本地若有数据的话）
        if (isset($data['utmSource'])) $newData['utm_source'] = $data['utmSource'];
        //渠道推广的广告内容（本地若有数据的话）
        if (isset($data['utmContent'])) $newData['utm_content'] = $data['utmContent'];
        //渠道推广的广告关键词（本地若有数据的话）
        if (isset($data['utmTerm'])) $newData['utm_term'] = $data['utmTerm'];

        /***处理自定义参数attributes Start*****/
        if (!empty($data['attributes'])) {
            $attributesData = $data['attributes'];
            /**pdp 页面字段**/
            //pdp页面当前产品所选LensType
            if (isset($attributesData['lensType'])) $newData['lens_type'] = $attributesData['lensType'];
            //pdp页面产品price值
            if (isset($attributesData['price'])) $newData['price'] = $attributesData['price'];
            //pdp页面salesPrice值
            if (isset($attributesData['salesPrice'])) $newData['sales_price'] = $attributesData['salesPrice'];
            //框架组： full_rim\single
            if (isset($attributesData['frameGroup'])) $newData['frame_group'] = $attributesData['frameGroup'];
            //当前LensType切换的各个访问页面url
            if (isset($attributesData['simpleUrl'])) $newData['simple_url'] = $attributesData['simpleUrl'];
            //ppdp页面orderGlasses的Sku值、拼出来显示的SKU值:11319C09NRX
            if (isset($attributesData['destSku'])) $newData['dest_sku'] = $attributesData['destSku'];
            //当前产品真实SKU值： 11319C09
            if (isset($attributesData['stockSku'])) $newData['stock_sku'] = $attributesData['stockSku'];
            //当前产品的具体sku  catalog_product_entity 表中的sku
            if (isset($attributesData['reviewSku'])) $newData['sku'] = $attributesData['reviewSku'];
            //当前产品的ID
            if (isset($attributesData['entityID'])) $newData['entity_id'] = $attributesData['entityID'];
            //产品归属ID  眼镜的configurable_id  总ID
            if (isset($attributesData['configurableID'])) $newData['configurable_id'] = $attributesData['configurableID'];
            //根据simple sku和configurableId 获取商品详情
            if(!empty($attributesData['reviewSku']) && !empty($attributesData['configurableID'])) {
                $newData['item_data'] = $this->_getItemDataBySku($attributesData['reviewSku'], $attributesData['configurableID'], $isJson = 1);
            }
            //购物车id
            if (isset($attributesData['quoteId'])) $newData['quote_id'] = $attributesData['quoteId'];
            //根据购物车id获取购物车详情
            if(!empty($attributesData['quoteId'])) {
                $newData['quote_data'] = $this->_getQuoteDataByQuoteId($attributesData['quoteId'], $isJson = 1);
            }
            //本类产品的sku值：11319
            if (isset($attributesData['configureProductSku'])) $newData['configure_product_sku'] = $attributesData['configureProductSku'];
            //产品目录ID：6女.8男.10儿童
            if (isset($attributesData['categoryID'])) $newData['category_id'] = $attributesData['categoryID'];
            //产品类别:eyeglasses...
            if (isset($attributesData['catetoryType'])) $newData['catetory_type'] = $attributesData['catetoryType'];
            //THIS PRICE INCLUDES数据
//            if (isset($attributesData['included'])) $newData['included'] = $attributesData['included'];
            //评论得分
            if (isset($attributesData['star'])) $newData['star'] = $attributesData['star'];
            //是否会推荐给朋友
            if (isset($attributesData['recommend'])) $newData['recommend'] = $attributesData['recommend'];
            //产品颜色
//            if (isset($attributesData['productColor'])) $newData['product_color'] = $attributesData['productColor'];
            //产品颜色text
//            if (isset($attributesData['productColorValue'])) $newData['product_color_value'] = $attributesData['productColorValue'];
            //夹片产品的类别ID
            if (isset($attributesData['configid'])) $newData['configid'] = $attributesData['configid'];
            //夹片产品的ID
            if (isset($attributesData['simpleid'])) $newData['simpleid'] = $attributesData['simpleid'];
            //夹片的颜色
            if (isset($attributesData['color'])) $newData['color'] = $attributesData['color'];
            //夹片SKU
            if (isset($attributesData['clipSku'])) $newData['clip_sku'] = $attributesData['clipSku'];
            //stock库存
            if (isset($attributesData['stock'])) $newData['stock'] = $attributesData['stock'];
            //推荐的产品URL
            if (isset($attributesData['recommendUrl'])) $newData['recommend_url'] = $attributesData['recommendUrl'];
            //推荐的产品Color
            if (isset($attributesData['recommendColor'])) $newData['recommend_color'] = $attributesData['recommendColor'];
            //推荐的产品entity_id
            if (isset($attributesData['recommendEntityId'])) $newData['recommend_entityId'] = $attributesData['recommendEntityId'];
            //推荐的产品product_id
            if (isset($attributesData['recommendProductId'])) $newData['recommend_productId'] = $attributesData['recommendProductId'];
            //推荐的产品parent_id
            if (isset($attributesData['recommendParentId'])) $newData['recommend_parentId'] = $attributesData['recommendParentId'];
            //推荐的产品sku
            if (isset($attributesData['recommendSku'])) $newData['recommend_sku'] = $attributesData['recommendSku'];
            //推荐产品的价格
            if (isset($attributesData['recommendPrice'])) $newData['recommend_price'] = $attributesData['recommendPrice'];
            //推荐产品的名字
            if (isset($attributesData['recommendName'])) $newData['recommend_name'] = $attributesData['recommendName'];
            //推荐产品卖价
            if (isset($attributesData['recommendSalesPrice'])) $newData['recommend_sales_price'] = $attributesData['recommendSalesPrice'];
            //pdp页面收藏事件  1收藏 2取消
            if (isset($attributesData['saveType'])) $newData['save_type'] = $attributesData['saveType'];
            //pdp页面分享按钮点击事件  1：facebook 2：twitter 3：pinterest
            if (isset($attributesData['shareType'])) $newData['share_type'] = $attributesData['shareType'];
            //pdp页面Lenstype=blue时，1=Prescription Glasses  0=Non-Prescription Glasses
            if (isset($attributesData['blueType'])) $newData['blue_type'] = $attributesData['blueType'];
            //pdp页面out of stock时，sign up按钮点击事件。  1=confirm提交  0=cancel取消
            if (isset($attributesData['signupType'])) $newData['signup_type'] = $attributesData['signupType'];
            //配件页面收藏事件  1收藏 2取消
            if (isset($attributesData['accessSaveType'])) $newData['access_save_type'] = $attributesData['accessSaveType'];
            //配件页面分享按钮点击事件  1：facebook 2：twitter 3：pinterest
            if (isset($attributesData['accessShareType'])) $newData['access_share_type'] = $attributesData['accessShareType'];
            //点击详情页有个order as eyeglasses按钮事件
            if (isset($attributesData['sunOrEyeGlassesType'])) $newData['sun_or_eyeglasses_type'] = $attributesData['sunOrEyeGlassesType'];
            //镜框类别 rx_non_progressive_semi_rimless、goggles、full_rim
            if (isset($attributesData['frameGroupHref'])) $newData['frame_group_href'] = $attributesData['frameGroupHref'];
            /**pdp 页面字段**/
            /**buy index 页面字段**/
            //值和dataTinitcolorPrice一样，应该是镜片+功效之后的总价
            if (isset($attributesData['dataTotalprice'])) $newData['data_total_price'] = $attributesData['dataTotalprice'];
            //选择的镜片的价格
            if (isset($attributesData['dataTinitcolorPrice'])) $newData['data_tinit_color_price'] = $attributesData['dataTinitcolorPrice'];
            //lensType走到哪一步的记录(title)
            if (isset($attributesData['lensHeader'])) $newData['lens_header'] = $attributesData['lensHeader'];
            //镜片价格
            if (isset($attributesData['lens_price'])) $newData['lens_price'] = $attributesData['lens_price'];
            //涂层价格
            if (isset($attributesData['coating_price'])) $newData['coating_price'] = $attributesData['coating_price'];
            //染色价格
            if (isset($attributesData['tint_price'])) $newData['tint_price'] = $attributesData['tint_price'];
            //渐进设计价格
            if (isset($attributesData['pal_design_price'])) $newData['pal_design_price'] = $attributesData['pal_design_price'];
            //default价格
            if (isset($attributesData['defa_price'])) $newData['defa_price'] = $attributesData['defa_price'];
            /**buy index 页面字段**/
            /**列表页面字段**/
            if (isset($attributesData['skuLists']) && !empty($attributesData['skuLists'])) {
                $skuData = [];
                foreach($attributesData['skuLists'] as $skuKey => $skuVal) {
                    $skuData[$skuKey] = $this->_getItemDataBySku($skuVal['sku'], $skuVal['configureableId'], $isJson = 0);
                }
                $newData['item_data'] = json_encode($skuData);
            }
            /**列表页面字段**/

            unset($attributesData);
        }
        /***处理自定义参数attributes End*****/

        return $newData;
    }

    //根据SKU获取商品详情
    public function _getItemDataBySku($sku, $configurableID, $isJson = 1) {
        $data = [];
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager=\Magento\Framework\App\ObjectManager::getInstance();
        $simpleObj=$objectManager->get("\Magento\Catalog\Api\ProductRepositoryInterface")->get($sku);
        $configurableObj=$objectManager->get("\Magento\Catalog\Api\ProductRepositoryInterface")->getById($configurableID);
//        $data["additional_obj"]=unserialize($data["additional_data"]);
        $data['stock_sku'] = $simpleObj->getData("stock_sku");
        $data['new_name'] = $simpleObj->getData("name");//$product_obj['name'];
        $data['new_sku'] = $simpleObj->getData("sku");//$product_obj['sku'];
        $data['tg_category_name'] = $objectManager->get("\Pg\CatalogExtend\Helper\Product")->getProductCategoryTagName($configurableObj->getData("category_ids"));
        $data['color'] = $simpleObj->getData("color");
        $data['color_string'] = $simpleObj->getResource()->getAttribute('color')->getFrontend()->getValue($simpleObj);
        $data['is_clip_on'] = $configurableObj->getData("is_clip_on");
        $data['is_clip_on_value'] = $configurableObj->getResource()->getAttribute('is_clip_on')->getFrontend()->getValue($configurableObj);
        $data['frame_shape'] = $configurableObj->getData("frame_shape");
        $data['frame_shape_value'] = $configurableObj->getResource()->getAttribute('frame_shape')->getFrontend()->getValue($configurableObj);
        $data['frame_size'] = $configurableObj->getData("frame_size");
        $data['frame_size_value'] = $configurableObj->getResource()->getAttribute('frame_size')->getFrontend()->getValue($configurableObj);
        $data['rim_type'] = $configurableObj->getData("rim_type");
        $data['rim_type_value'] = $configurableObj->getResource()->getAttribute('rim_type')->getFrontend()->getValue($configurableObj);
        $data['material'] = $configurableObj->getData("material");
        $data['material_value'] = $configurableObj->getResource()->getAttribute('material')->getFrontend()->getValue($configurableObj);
        $data['nose_pad'] = $configurableObj->getData("nose_pad");
        $data['nose_pad_value'] = $configurableObj->getResource()->getAttribute('nose_pad')->getFrontend()->getValue($configurableObj);
        $data['has_spring_hinges'] = $configurableObj->getData("has_spring_hinges");
        $data['has_spring_hinges_value'] = $configurableObj->getResource()->getAttribute('has_spring_hinges')->getFrontend()->getValue($configurableObj);
        $data['lens_width'] = $configurableObj->getData("lens_width");
        $data['lens_width_value'] = $configurableObj->getResource()->getAttribute('lens_width')->getFrontend()->getValue($configurableObj);
        $data['bridge'] = $configurableObj->getData("bridge");
        $data['bridge_value'] = $configurableObj->getResource()->getAttribute('bridge')->getFrontend()->getValue($configurableObj);
        $data['temple_length'] = $configurableObj->getData("temple_length");
        $data['temple_length_value'] = $configurableObj->getResource()->getAttribute('temple_length')->getFrontend()->getValue($configurableObj);
        $data['frame_width'] = $configurableObj->getData("frame_width");
        $data['frame_width_value'] = $configurableObj->getResource()->getAttribute('frame_width')->getFrontend()->getValue($configurableObj);
        $data['lens_height'] = $configurableObj->getData("lens_height");
        $data['lens_height_value'] = $configurableObj->getResource()->getAttribute('lens_height')->getFrontend()->getValue($configurableObj);
        $data['frame_weight'] = $configurableObj->getData("frame_weight");
        $data['frame_weight_value'] = $configurableObj->getResource()->getAttribute('frame_weight')->getFrontend()->getValue($configurableObj);
        $data['min_pd'] = $configurableObj->getData("min_pd");
        $data['max_pd'] = $configurableObj->getData("max_pd");
        $data['min_sph'] = $configurableObj->getData("min_sph");
        $data['min_sph_value'] = $configurableObj->getResource()->getAttribute('min_sph')->getFrontend()->getValue($configurableObj);
        $data['max_sph'] = $configurableObj->getData("max_sph");
        $data['max_sph_value'] = $configurableObj->getResource()->getAttribute('max_sph')->getFrontend()->getValue($configurableObj);
        $data['other_colors'] = $configurableObj->getData("other_colors");
        $data['other_colors_value'] = $configurableObj->getResource()->getAttribute('other_colors')->getFrontend()->getValue($configurableObj);
        $data['age_range'] = $configurableObj->getData("age_range");
        $data['age_range_value'] = $configurableObj->getResource()->getAttribute('age_range')->getFrontend()->getValue($configurableObj);
        $data['support_progressive'] = $configurableObj->getData("support_progressive");
        $data['support_progressive_value'] = $configurableObj->getResource()->getAttribute('support_progressive')->getFrontend()->getValue($configurableObj);
        $data['recommend'] = $configurableObj->getData("recommend");
        $data['recommend_value'] = $configurableObj->getResource()->getAttribute('recommend')->getFrontend()->getValue($configurableObj);
        $data['support_ditto'] = $simpleObj->getData("support_ditto");
        $data['support_ditto_value'] = $simpleObj->getResource()->getAttribute('support_ditto')->getFrontend()->getValue($simpleObj);
        $data['support_php'] = $simpleObj->getData("support_php");
        $data['support_php_value'] = $simpleObj->getResource()->getAttribute('support_php')->getFrontend()->getValue($simpleObj);
        $data['nose_bridge'] = $configurableObj->getData("nose_bridge");
        $data['nose_bridge_value'] = $configurableObj->getResource()->getAttribute('nose_bridge')->getFrontend()->getValue($configurableObj);
        $data['nose_bridge'] = $configurableObj->getData("nose_bridge");
        $data['nose_bridge_value'] = $configurableObj->getResource()->getAttribute('nose_bridge')->getFrontend()->getValue($configurableObj);
        $data['support_rx_value'] = $configurableObj->getResource()->getAttribute('support_rx')->getFrontend()->getValue($configurableObj);

        if($isJson) $data = json_encode($data);
        return $data;
    }

    //根据购物车id获取购物车数据
    public function _getQuoteDataByQuoteId($quoteId, $isJson = 1) {
        $quoteItemData = [];
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $quoteItemRepository = $objectManager->create("Magento\Quote\Api\CartItemRepositoryInterface");
        $quoteItemList = $quoteItemRepository->getList($quoteId);
        foreach ($quoteItemList as $index => $quoteItem) {
            $item = $quoteItem->toArray();
            $framePrice = number_format($item['product']['price'],2);
            $lensPrice = 0;
            $coatingPrice = 0;
            $tintPrice = 0;
            $palDesignPrice = 0;
            $defaPrice = 0;
            $quoteItemData[$index]['frame_price'] = $framePrice;
            if(!empty($item['additional_data'])) {
                $additionalData = unserialize($item['additional_data']);
                if(!empty($additionalData['orderglass']['Lenss'])) $lensPrice = $additionalData['orderglass']['Lenss']['price'];
                if(!empty($additionalData['orderglass']['Coating'])) $coatingPrice = $additionalData['orderglass']['Coating']['price'];
                if(!empty($additionalData['orderglass']['Tint'])) $tintPrice = $additionalData['orderglass']['Tint']['price'];
                if(!empty($additionalData['orderglass']['PAL_Design'])) $palDesignPrice = $additionalData['orderglass']['PAL_Design']['price'];
                if(!empty($additionalData['orderglass']['Default'])) $defaPrice = $additionalData['orderglass']['Default']['price'];
            }
            $quoteItemData[$index]['lens_price'] = $lensPrice;
            $quoteItemData[$index]['coating_price'] = $coatingPrice;
            $quoteItemData[$index]['tint_price'] = $tintPrice;
            $quoteItemData[$index]['pal_design_price'] = $palDesignPrice;
            $quoteItemData[$index]['defa_price'] = $defaPrice;
            $itemData = $this->_getItemDataBySku($item['sku'], $item['product']['entity_id'], 0);
            $quoteItemData[$index] += $itemData;
        }
        if($isJson) $quoteItemData = json_encode($quoteItemData);
        return $quoteItemData;
    }


    //切换到magento的redis服务
    public function redisObj() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["cache"];
        $server=$cfg["frontend"]["default"]["backend_options"]["server"];
        $port=$cfg["frontend"]["default"]["backend_options"]["port"];
        $database=$cfg["frontend"]["default"]["backend_options"]["database"];
        $redis=new \Credis_Client($server, $port, 30,false, $database, null);
        return $redis;
    }
}
