<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\FileHelper;
use Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use GuzzleHttp\Client as GuzzleClient;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class KlaviyoDeliveryController extends Controller
{
    function is_json($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function actionProcess() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        while(true) {
            $before_list_len=Yii::$app->redis_cache->llen("CACHE_MAROPOST_DELIVERY_LIST");
            $maropost_delivery_data=Yii::$app->redis_cache->brpop("CACHE_MAROPOST_DELIVERY_LIST",10);
            $after_list_len=Yii::$app->redis_cache->llen("CACHE_MAROPOST_DELIVERY_LIST");
            $monitor_log=new \Pg\Monitor\Log();//$objectManager->create("Pg\Monitor\Log");
            try {
                if (!empty($maropost_delivery_data)) {
                    $monitor_log->log_start("maropost-process");
                    $log_data=[
                        "before_list_len"=>$before_list_len,
                        "after_list_len"=>$after_list_len,
                        "maropost_delivery_data"=>$maropost_delivery_data
                    ];
                    $monitor_log->log($log_data);
                    $maropost_delivery_data=$maropost_delivery_data[1];
                    if ($this->is_json($maropost_delivery_data)) {
                        $maropost_delivery_data=json_decode($maropost_delivery_data,true);
                        if ($maropost_delivery_data["event_type"] == "OUT_FOR_DELIVERY"
                            || $maropost_delivery_data["event_type"] == "CROSS_COUNTRY_TRANSFER"
                            || $maropost_delivery_data["event_type"] == "REMAKE"
                            || $maropost_delivery_data["event_type"] == "REMAKE1"
                            || $maropost_delivery_data["event_type"] == "TEST"
                        ) {
                            if (!empty($maropost_delivery_data["email"]) && $this->check_email($maropost_delivery_data["email"])) {
                                //delete lee 2020.12.7 改为klaviyo
//                                $maropost_api=new Contacts($maropost_delivery_data["account_id"], $maropost_delivery_data["authtoken"]);
//                                $errno=$maropost_api->createOrUpdateForList($maropost_delivery_data["list_id"],$maropost_delivery_data["email"],
//                                    $maropost_delivery_data["customer_firstname"],$maropost_delivery_data["customer_lastname"],null,null,null,
//                                    $maropost_delivery_data["customer_data"]);
//                                $c_data=$maropost_api->getForEmail($maropost_delivery_data["email"]);
//                                $data=$c_data->getData();
//                                echo $data->tmp_so_increment_id;
//                                echo PHP_EOL;
//                                sleep(2);
//                                $monitor_log->log(["getForEmail"=>$c_data->getData()]);
//                                $client = new GuzzleClient();
//                                $url=$maropost_delivery_data["url"];
//                                $res = $client->request('POST', $url);
//                                $errno=$res->getBody()->getContents();
                                $customer_properties=array_merge(['$email'=>$maropost_delivery_data["email"],'$first_name'=>$maropost_delivery_data["customer_firstname"],'$last_name'=>$maropost_delivery_data["customer_lastname"]],$maropost_delivery_data["customer_data"]);
                                $properties=$maropost_delivery_data["customer_data"];
                                $event_id=strtolower($maropost_delivery_data["event_type"]);
                                $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->track($event_id,$customer_properties,$properties);
                                //end
                                $monitor_log->log_stop("");
                            } else {
                                $monitor_log->log(["exception_1"=>"email is null"]);
                            }
                        }
                    } else {
                        $monitor_log->log(["exception_2"=>"data is not json"]);
                    }
                }
            } catch (\Exception $e) {
                $err_no="Failed!";
                $monitor_log->log(["maropost_delivery_data"=>$maropost_delivery_data,"exception_msg"=>$e->getMessage()]);
                echo "exception=".$e->getMessage().PHP_EOL;
            }
//            sleep(1);
        }
    }

    private function check_email($email) {
        $result = trim($email);
        if (filter_var($result, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else{
            return false;
        }
    }

    public function actionTest(){
        Yii::$app->redis_cache->lpush("CACHE_MAROPOST_DELIVERY_LIST_temp","lihiafeng");

//        $data=Yii::$app->redis_cache->brpop("CACHE_MAROPOST_DELIVERY_LIST_temp",10);
//        echo print_r($data);;
//        echo PHP_EOL;
    }
}
