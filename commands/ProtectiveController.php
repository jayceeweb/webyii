<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\FileHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ProtectiveController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionCacheIndex()
    {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $PRODUCT_CATEGORY=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["PRODUCT_CATEGORY"];

        $sql=sprintf("SELECT
`e`.`entity_id` AS configurable_id,
`e`.`attribute_set_id` AS configurable_attribute_set_id,
`e`.`type_id` AS configurable_type_id,
`e`.`sku` AS configurable_sku,
`e`.`name` AS configurable_name,
`cat_index`.`category_id`,
`s_e`.`type_id` AS configurable_type_id,
`s_e`.`entity_id` AS simple_entity_id,
`s_e`.`type_id` AS simple_type_id,
`s_e`.`sku` AS simple_sku,
`s_e`.`name` AS simple_name
FROM
`catalog_product_flat_all_1` AS e
INNER JOIN `catalog_category_product_index` AS cat_index ON `cat_index`.`product_id` = `e`.`entity_id` AND `cat_index`.`store_id` = 1 AND `cat_index`.`visibility` IN (2, 4)
INNER JOIN `catalog_product_super_link` parent ON parent.`parent_id`=e.`entity_id`
INNER JOIN `catalog_product_flat_all_1` s_e ON s_e.`entity_id`=parent.`product_id`
WHERE
`cat_index`.`category_id` IN (%s)",$PRODUCT_CATEGORY["PROTECTIVE"]);
        $simples=$results=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($simples as $key=>$simple) {
            echo sprintf("simple_entity_id=%s,simple_name=%s".PHP_EOL,$simple["simple_entity_id"],$simple["simple_name"]);
            $data=Yii::$container->get("app\components\helper\ProtectiveHelper")->renderHtml($simple["simple_entity_id"]);
            $html=$this->renderPartial("view",$data);
            $file=$data["simple_url"];
            $path=dirname($file);
            $file_name=basename($file);
            $dr=\Magento\Framework\App\ObjectManager::getInstance()->get("Magento\Framework\Filesystem\DirectoryList");
            $root_path=$dr->getRoot()."/pub";
            echo sprintf("url=%s\r\nfilename=%s\r\n\r\n",$data["simple_url"],$file_name);
            $cache_file_name=$root_path."/".$path."/".$file_name;
            file_put_contents($cache_file_name,$html);
        }
        return ExitCode::OK;
    }

    public function actionCacheClean() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $dr=\Magento\Framework\App\ObjectManager::getInstance()->get("Magento\Framework\Filesystem\DirectoryList");
        $root_path=$dr->getRoot()."/pub/protective";
        $fileArray = scandir($root_path);//将文件夹中的文件升序排列
        foreach($fileArray as $fileName) {
            if($fileName != "." && $fileName != ".."){
                if(is_dir($root_path . '/' . $fileName)) {
                    //递归地删除一个目录(及其所有内容)yii2.0
                    FileHelper::removeDirectory($root_path . '/' . $fileName);
                } else {
                    unlink($root_path . '/' . $fileName);
                }
            }
        }
    }
}
