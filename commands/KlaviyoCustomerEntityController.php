<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\components\helper\AuthcodeHelper;
use app\components\helper\DbHelper;
use app\models\PgExchangeData;
use GuzzleHttp\Client as GuzzleClient;

class KlaviyoCustomerEntityController extends Controller
{
    const STATUS_SUBSCRIBED = 1;//订阅
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;//取消订阅
    const STATUS_UNCONFIRMED = 4;
    const STATUS_CLEANED = 5;
    const STATUS_MAILCHIMP_ERROR = 6;//同步mailchimp出错
    const STATUS_HAS_CHINESE = 7;//有中文

    private $log_file="";
    private $start_entity_id=0;
    private $page_size=2000;
    private $every_time_size=100;
    private $total_count=0;
    private $c=[];
    private $authcode_secret="";

    private $helper;

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    private function savelog($log,$output=true) {
        if (!empty($this->log_file)) {
            file_put_contents($this->log_file,$log.PHP_EOL,FILE_APPEND);
        }
        if ($output) {
            $datetime = new \DateTime();
            echo $datetime->format('Y-m-d H:i:s')." -> ".$log.PHP_EOL;
        }
    }

    private function get_magento_member_list_sql($is_count=false,$param=["div"=>1,"mod"=>0]) {
        $div=$param["div"];
        $mod=$param["mod"];

        $sql = "SELECT `c1`.`entity_id`,`c1`.`email`
%s
FROM customer_entity c1
WHERE email NOT IN (
	SELECT subscriber_email FROM newsletter_subscriber where subscriber_email is not null
)
AND `c1`.`entity_id` >= %s
and (c1.entity_id %% {$mod} = {$div})
# AND c1.`subscriber_email`='skwagen@aol.com'
%s";

        $count_sql="
SELECT COUNT(0) AS cnt FROM customer_entity c1
WHERE 
`c1`.`entity_id` >= %s
and email NOT IN (
	SELECT subscriber_email FROM newsletter_subscriber where subscriber_email is not null
)
and (c1.entity_id %% {$mod} = {$div})
";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $GLASSES_ATTRIBUTE_SET_IDS=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["GLASSES_ATTRIBUTE_SET_IDS"];
        $select_fields = ",`c1`.`firstname`
,`c1`.`lastname`
,`c1`.`gender`
,`c1`.`group_id`
,DATE_FORMAT(`c1`.`created_at`, '%Y-%m-%d %H:%i:%s') AS 'reg_date'
,(select DATE_FORMAT(MIN(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'first_purchase_date'
,(select DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'last_purchase_date'
,(SELECT DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') FROM sales_order WHERE sales_order.`customer_id`=`c1`.`entity_id` AND coupon_code IS NULL AND discount_amount=0 HAVING sum(subtotal)>5.95) AS last_non_exchange_order_date
,(SELECT COUNT(0) FROM `sales_order` so WHERE `so`.`customer_id` = `c1`.`entity_id`) AS 'orders_count'
,(SELECT SUM(total_qty_ordered) FROM `sales_order` so WHERE `so`.`customer_id` = `c1`.`entity_id`) AS 'total_ordered'
,(SELECT SUM(subtotal) FROM `sales_order` so WHERE `so`.`customer_id` = `c1`.`entity_id`) AS 'so_subtotal'
,(SELECT COUNT(0) FROM sales_order_item
WHERE product_type='configurable'
AND glasses_prescription_id IN (SELECT entity_id FROM glasses_prescription WHERE ladd !=0.00 OR radd !=0.00)
AND order_id IN(SELECT entity_id FROM sales_order WHERE sales_order.customer_id=c1.entity_id)) AS progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND profile_prescription_id IN (SELECT entity_id FROM prescription_entity WHERE ladd !=0.00 OR radd !=0.00)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_count
,(SELECT MAX(created_at) AS created_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_created_at
,(SELECT MAX(updated_at) AS updated_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_updated_at
,(select sum(qi.row_total)  as grand_total
from quote q
INNER JOIN quote_item qi on qi.quote_id=q.entity_id
inner join catalog_product_flat_1 flat on flat.entity_id=qi.product_id
where q.customer_id=c1.entity_id and qi.parent_item_id is null and flat.attribute_set_id in (".implode(",",$GLASSES_ATTRIBUTE_SET_IDS).")) AS quote_grand_total
,(select count(0)  as items_count
from quote q
INNER JOIN quote_item qi on qi.quote_id=q.entity_id
inner join catalog_product_flat_1 flat on flat.entity_id=qi.product_id
where q.customer_id=c1.entity_id and qi.parent_item_id is null and flat.attribute_set_id in (".implode(",",$GLASSES_ATTRIBUTE_SET_IDS).")) AS quote_items_count
,(SELECT COUNT(0) AS cnt FROM `sales_order` WHERE customer_id=c1.entity_id AND store_id=1 AND is_virtual=0 AND (state='processing' OR state='closed') AND created_at >= DATE_SUB(NOW(), INTERVAL 21 DAY)) AS three_weeks_purchase
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='simple') as frame_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='configurable') as glasses_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND soi.is_virtual=1) as craft_avg_price
,(select enabled_score from customer_entity where entity_id=c1.entity_id) as enabled_score
";
        $order_sql="ORDER BY `c1`.`entity_id` ASC";
        if ($is_count) {
            $sql = sprintf($count_sql, $this->start_entity_id, "");
        } else {
            $sql = sprintf($sql, $select_fields, $this->start_entity_id, $order_sql);
        }
        return $sql;
    }

    /**
     * 计算同步的总数量
     */
    private function get_magento_member_count($param){
        $count_sql=$this->get_magento_member_list_sql(true,$param);
        $cnt=$this->getDb()->createCommand($count_sql)->queryScalar();
        $total_page = ceil($cnt / $this->page_size);
        $this->total_count = (int)$cnt;
        $log=sprintf("total count=%s,total page=%s,page size=%s",$this->total_count,$total_page,$this->page_size);
        $this->savelog($log);
        return $total_page;
    }

    private function get_magento_member_list($param) {
        $sql = $this->get_magento_member_list_sql( False,$param);
        $sql = sprintf("%s limit %s,%s",$sql, 0, $this->page_size);
        $data=$this->getDb()->createCommand($sql)->queryAll();
        return $data;
    }

    private function toTime($d) {
        if (!empty($d)) {
            $pos=strrpos($d,"-");
            $fetch=substr($d,$pos,1);
            $fetch_v=(int)explode(":",substr($d,$pos+1,strlen($d)-$pos-1))[0];
            return date("Y-m-d H:i:s",strtotime(sprintf("%s%s hours",$fetch,$fetch_v),strtotime($d)));
        } else {
            return "";
        }
    }

    public function authcode_encode($data) {
        return base64_encode(AuthcodeHelper::encode($data,$this->authcode_secret));
    }

    public function authcode_decode($data) {
        return AuthcodeHelper::decode_ex($data);
    }

    private function sync_by_list($list,$param) {
        $profiles=[];
        $i=0;//每次列表计数，每100次上传一次
        foreach ($list as $member) {
            $t1=msectime();
            $i=$i+1;
            $key="key_".$param["mod"]."_".$param["div"];
            if (!isset($this->c[$key])) {
                $this->c[$key]=0;
            }
            $this->c[$key]=$this->c[$key]+1;
            $this->start_entity_id = $member["entity_id"];
            $log = sprintf("total=%s,current=%s,customer_id=%s,email=%s,mod=%s,div=%s", $this->total_count,$this->c[$key],$member["entity_id"],$member["email"],$param["mod"],$param["div"]);
            $this->savelog($log);

            $customer_id=$member["entity_id"];
            $customer_id_auth_code=$this->authcode_encode($customer_id);
            $reg_date=$member["reg_date"];
            $firstname = $member["firstname"];
            $lastname=$member["lastname"];
            $gender=$member["gender"];
            $genderstr="";
            if ($member["gender"]==null) {
                $gender="3";
                $genderstr = "Not Specified";
            } else if ($member["gender"]=="1") {
                $genderstr="Male";
            } else if ($member["gender"]=="2") {
                $genderstr="Female";
            } else if ($member["gender"]=="3") {
                $genderstr="Not Specified";
            } else {
                $gender = "3";
                $genderstr = "Not Specified";
            }
            $subscriber_email = strtolower($member["email"]);
            $email_auth_code=$this->authcode_encode($subscriber_email);
            $first_purchase_date = $member["first_purchase_date"];
            $last_purchase_date = $member["last_purchase_date"];
            $last_non_exchange_order_date = $member["last_non_exchange_order_date"];
            $orders_count = $member["orders_count"];
            $total_ordered = $member["total_ordered"];
            $so_subtotal = $member["so_subtotal"];
            $progressive_count = $member["progressive_count"];
            $quote_progressive_count = $member["quote_progressive_count"];
            $quote_count=$member["quote_count"];
            $quote_grand_total=$member["quote_grand_total"];
            $quote_items_count=$member["quote_items_count"];
            $quote_created_at=$member["quote_created_at"];
            $quote_updated_at = $member["quote_updated_at"];
            $frame_avg_price = $member["frame_avg_price"];
            $glasses_avg_price = $member["glasses_avg_price"];
            $craft_avg_price = $member["craft_avg_price"];
            $enabled_score = empty($member["enabled_score"])? 0:1;
            $log=sprintf("reg_date=%s,firstname=%s,lastname=%s,gender=%s,subscriber_email=%s,customer_id=%s,first_purchase_date=%s,last_purchase_date=%s,orders_count=%s,progressive_count=%s,genderstr=%s,quote_progressive_count=%s,quote_count=%s,quote_created_at=%s,quote_updated_at=%s",
                $reg_date,$firstname,$lastname,$gender,$subscriber_email,$customer_id,$first_purchase_date,$last_purchase_date,$orders_count,$progressive_count,$genderstr,$quote_progressive_count,$quote_count,$quote_created_at,$quote_updated_at);
//            $this->savelog($log);

            if (
//                (empty($firstname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $firstname, $match))
//                && (empty($lastname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $lastname, $match))
                true
                && (!$this->helper->isFakeMail($subscriber_email)
                && $this->check_email($subscriber_email)
                )
            ) {
                $can_synd=true;
            } else {
                $can_synd=false;
            }
//            $can_synd=true;
            //对比是否需要更新
            if ($can_synd) {
                //加入打开和点击次数
//                $maropost_data=Yii::$container->get("app\components\helper\KlaviyoHelper")->load_maropost_info($subscriber_email);
                $profiles[]=[
                    "craft_avg_price"=>round($craft_avg_price,2),
                    "customer_id"=>$customer_id,
                    "first_purchase_date"=>$first_purchase_date,
                    "frame_avg_price"=>$frame_avg_price,
                    "gender_str"=>$genderstr,
                    "glasses_avg_price"=>round($glasses_avg_price,2),
                    "last_purchase_date"=>$last_purchase_date,
                    "order_count"=>$orders_count,//总订单数
                    "total_ordered"=>$total_ordered,//所有订单的总副数
                    "so_subtotal"=>$so_subtotal,//所有订单的总金额
                    "so_avg_subtotal"=>$total_ordered!=0? round($so_subtotal / $total_ordered,2):0,//平均每单金额
                    "so_avg_qty"=>$orders_count!=0? round($total_ordered / $orders_count,1):0,//平均每单购买数量
                    "progressive_count"=>$progressive_count,
                    "quote_count"=>$quote_count,
                    "quote_grand_total"=>$quote_grand_total,
                    "quote_items_count"=>$quote_items_count,
                    "quote_progressive_count"=>$quote_progressive_count,
                    "quote_created_at"=>$quote_created_at,
                    "quote_updated_at"=>$quote_updated_at,
                    "register_date"=>$reg_date,
                    "customer_id_auth_code"=>$customer_id_auth_code,
                    "email_auth_code"=>$email_auth_code,
                    "email"=>$subscriber_email,
                    "first_name"=>$firstname,
                    "last_name"=>$lastname,
                    "last_non_exchange_order_date"=>$last_non_exchange_order_date,
                    "enabled_score"=>$enabled_score,
//                    "last_visit_time"=>$maropost_data["last_visit_time"],
//                    "contract_clicks_cnt"=>$maropost_data["contract_clicks_cnt"],
//                    "contract_opens_cnt"=>$maropost_data["contract_opens_cnt"],
//                    "contract_clicks_recent_date"=>$maropost_data["contract_clicks_recent_date"],
//                    "contract_opens_recent_date"=>$maropost_data["contract_opens_recent_date"]
                ];
            }
            if ($i==$this->every_time_size) {
                $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->createOrUpdateContract($profiles);
                $profiles=[];
                $i=0;
            }
        }
        if (!empty($profiles)) {
            $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->createOrUpdateContract($profiles);
        }
    }

    private function check_email($email) {
        $result = trim($email);
        if (filter_var($result, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * 同步
     */
    private function sync($param) {
//        $this->start_entity_id=200000;
        $total_page=$this->get_magento_member_count($param);
        for ($page_no=0; $page_no<$total_page;$page_no++) {
            $list=$this->get_magento_member_list($param);
            $this->sync_by_list($list,$param);
        }
    }

    public function actionSync($log_file="/lihf/klaviyo_customer_entity.log",$mod=1,$div=0) {
        if ($log_file!="") {
            $this->log_file=$log_file;
        }
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $this->helper=$objectManager->get('Plumrocket\SocialLoginFree\Helper\Data');

        $this->savelog("start sync ...",true);

        $param = ["mod"=>$mod,"div"=>$div];
        $this->sync($param);

        return ExitCode::OK;
    }

    public function actionSync2($log_file="/lihf/klaviyo_customer_entity.log",$mod=5) {
        if ($log_file!="") {
            $this->log_file=$log_file;
        }
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $this->helper=$objectManager->get('Plumrocket\SocialLoginFree\Helper\Data');

        $this->savelog("start sync ...",true);

        $t1=date('Y-m-d h:i:s', time());
        $workers = $mod;
        $pids = array();
        for($i = 0; $i < $workers; $i++){
            $pids[$i] = pcntl_fork();
            switch ($pids[$i]) {
                case -1:
                    echo "fork error : {$i} \r\n";
                    exit;
                case 0:
                    $param = ["mod"=>$workers,"div"=>$i];
                    $this->sync($param);
                    exit;
                default:
                    break;
            }
        }
        foreach ($pids as $i => $pid) {
            if($pid) {
                pcntl_waitpid($pid, $status);
            }
        }
        $t2=date('Y-m-d h:i:s', time());

        return ExitCode::OK;
    }
}
