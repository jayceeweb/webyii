<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use \Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use app\components\helper\AuthcodeHelper;
use app\components\helper\DbHelper;
use app\models\PgExchangeData;
use app\models\CustomerEntity;
use app\components\helper\SftpHelper;
use GuzzleHttp\Client as GuzzleClient;
use yii\db\Exception;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LastVisitorController extends Controller
{
    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
    const LIST_ID=3;

    const STATUS_SUBSCRIBED = 1;
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;
    const STATUS_UNCONFIRMED = 4;
    const STATUS_CLEANED = 5;
    const STATUS_MAILCHIMP_ERROR = 6;
    const STATUS_HAS_CHINESE = 7;
    private $log_file="";
    private $divisor=0;
    private $remainder=0;
    private $start_subscriber_id=0;
    private $page_size=2000;
    private $pwd='/data/web452/sjc/';
    private $total_count=0;
    private $c=0;
    private $authcode_secret="";

    private $maropost_api=null;

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    //第一步
    public function actionSync($log_file="/lihf/maropost.log",$divisor="5",$remainder="0") {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        if ($log_file!="") {
            $this->log_file=$log_file;
        }
        if ($divisor!="") {
            $this->divisor=$divisor;
        }
        if ($remainder!="") {
            $this->remainder=$remainder;
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();
        $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
        $this->savelog("start sync ...",true);

//        $this->sync_csv();
        $this->sync();

        return ExitCode::OK;
    }
    /**
     * 同步
     */
    //第二步：数据获取
    private function sync() {
        $total_page=$this->get_magento_member_count();
        for ($page_no=0; $page_no<$total_page;$page_no++) {
            $list=$this->get_magento_member_list();
            $this->sync_by_list($list);
            echo sprintf("第%s次同步完成".PHP_EOL,$page_no+1);
        }
    }

    /**
     * 计算同步的总数量
     */
    private function get_magento_member_count(){
        $count_sql=$this->get_magento_member_list_sql(true);
        $cnt=$this->getDb()->createCommand($count_sql)->queryScalar();
        $total_page = ceil($cnt / $this->page_size);
        $this->total_count = (int)$cnt;
        $log=sprintf("total count=%s,total page=%s,page size=%s",$this->total_count,$total_page,$this->page_size);
        $this->savelog($log);
        return $total_page;
    }

    /**
     * 获取同步的数据
     */
    private function get_magento_member_list() {
        $sql = $this->get_magento_member_list_sql( False);
        $sql = sprintf("%s limit %s,%s",$sql, 0, $this->page_size);
        $data=$this->getDb()->createCommand($sql)->queryAll();
        return $data;
    }

    private function savelog($log,$output=true) {
        if (!empty($this->log_file)) {
            file_put_contents($this->log_file,$log.PHP_EOL,FILE_APPEND);
        }
        if ($output) {
            $datetime = new \DateTime();
            echo $datetime->format('Y-m-d H:i:s')." -> ".$log.PHP_EOL;
        }
    }

    //获取customer_visitor中用户的最后访问时间
    private function get_magento_member_list_sql($is_count=false) {
        if($is_count){
            $sql = 'select count(0) from (SELECT email, firstname, lastname, max(last_visit_at) AS last_visit_at, session_id, customer_id FROM ( SELECT ce.email, ce.firstname, ce.lastname, cv.last_visit_at, cv.session_id, cv.customer_id FROM customer_visitor cv INNER JOIN customer_entity ce ON cv.customer_id = ce.entity_id ) new GROUP BY email) as count';
        }else{
            $sql = 'SELECT email, firstname, lastname, max(last_visit_at) AS last_visit_at, session_id, customer_id,visitor_id FROM ( SELECT ce.email, ce.firstname, ce.lastname, cv.last_visit_at, cv.session_id, cv.customer_id, cv.visitor_id FROM customer_visitor cv INNER JOIN customer_entity ce ON cv.customer_id = ce.entity_id ) new GROUP BY email';
        }
        return $sql;
    }

    //第三步：更新maropost
    private function sync_by_list($list) {
        //获取更新的所有customer_id
        $sql_data = '';
        $contacts=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
        //更新maropost数据
        var_dump('更新maropost');
        foreach ($list as $key => $value){
            var_dump($value['email']);
            var_dump($key+1);
            $sql_data .= $value['customer_id'].',';
            $data = ["last_visit_time"=>$value['last_visit_at']];
            $contacts->createOrUpdateForList(static::LIST_ID,$value['email'],$value['firstname'],$value['lastname'],null,null,null,$data);
        }
        var_dump('maropost结束');
        $sql_data = rtrim($sql_data,',');
        $sql_data = $sql_data?$sql_data:'\'\'';

        //开启事物
        $transaction = Yii::$app->getDb()->beginTransaction();
        try{
            $sql = "insert into customer_visitor_old select * from customer_visitor where customer_id in ($sql_data)";
            Yii::$app->db->createCommand($sql)->execute();
            $sql = "delete from customer_visitor where customer_id in ($sql_data)";
            Yii::$app->db->createCommand($sql)->execute();
            $transaction->commit();
            var_dump('success');
        } catch (Exception $e) {
            $transaction->rollBack();
            //如果有问题，记录错误日志
            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $monitor_log=$objectManager->create("Pg\Monitor\Log");
            $monitor_log->log_error(["code"=>-1,"data"=>$list,"model"=>'last_visit_time']);
            var_dump('false');
        }
    }
}
