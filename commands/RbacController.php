<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class RbacController
 * @package app\commands
 */
class RbacController extends Controller
{
    private function getRuleName($rbac_permission) {
        $auth = Yii::$app->authManager;

        if (isset($rbac_permission['rule_class']) && !empty($rbac_permission['rule_class'])) {
            $rule=new $rbac_permission['rule_class']();
            if (empty($auth->getRule($rule->name))) {
                $auth->add($rule);
            } else {
                $auth->update($rule->name,$rule);
            }
            return $rule->name;
        } else {
            return null;
        }
    }

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
//        $auth->removeAllPermissions();
//        $auth->removeAllRules();

        $rbac_permissions=Yii::$app->params['rbac_permissions'];
        foreach ($rbac_permissions as $key=>$rbac_permission) {
            $name=$rbac_permission["name"];
            $description=$rbac_permission["description"];
            if (!empty($name)) {
                $permission=$auth->getPermission($name);
                if (empty($permission)) {
                    $permission=$auth->createPermission($name);
                    $permission->description=$description;
                    $permission->ruleName=$this->getRuleName($rbac_permission);
                    $auth->add($permission);
                } else {
                    $permission->description=$description;
                    $permission->ruleName=$this->getRuleName($rbac_permission);
                    $auth->update($name,$permission);
                }
                if (!empty($permission) && isset($rbac_permission["children"])) {
                    $children=$rbac_permission["children"];
                    foreach ($children as $key=>$child) {
                        $type=$child["type"];
                        $child_name=$child["name"];
                        if ($type=="permission") {
                            if (!$auth->hasChild($permission,$auth->getPermission($child_name))) {
                                $auth->addChild($permission, $auth->getPermission($child_name));
                            }
                        }
                    }
                }
            }
        }
        return ExitCode::OK;
    }
}
