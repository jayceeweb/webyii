<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use GuzzleHttp\Client as GuzzleClient;
use app\models\PgScoreBill;
use app\models\PgExchangeData;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ScoreCommandController extends Controller
{
    const ORDER_DELIVERED_API_URL="http://47.101.47.249";
    private $retry_times=0;

    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
    const LIST_ID=3;

    public function get_30days_before_delivered_url() {
        $today=Yii::$container->get("app\components\helper\TimezoneHelper")->getDate();
        $start_date=date("Y-m-d", strtotime("-31 days", strtotime($today)));
        $end_date=date("Y-m-d", strtotime("-31 days", strtotime($today)));
        $url=sprintf(self::ORDER_DELIVERED_API_URL."/api/get_order_delivered_list/?start_date=%s&end_date=%s",$start_date,$end_date);
//        $url=sprintf(self::ORDER_DELIVERED_API_URL."/api/get_order_delivered_list/?order_number=%s","100178920");
        //http://47.101.47.249//api/get_order_delivered_list/?order_number=100178920
//        echo $url.PHP_EOL;
        //order_number:订单号
        //ship_direction=EXPRESS，运输方式
        return $url;
    }

    public function get_delivered_url() {
        $today=Yii::$container->get("app\components\helper\TimezoneHelper")->getDate();
        $start_date=date("Y-m-d", strtotime("-2 days", strtotime($today)));
        $end_date=date("Y-m-d", strtotime("0 days", strtotime($today)));
        $url=sprintf(self::ORDER_DELIVERED_API_URL."/api/get_order_delivered_list/?start_date=%s&end_date=%s",$start_date,$end_date);
        return $url;
    }

    public function json_validate($string) {
        if (is_string($string)) {
            @json_decode($string);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }



    public function read_delivered_orders($url) {
        $ret=null;//妥投订单号
        $order_numbers=[];

        try {
            $client = new GuzzleClient();
            $r=$client->get($url,["timeout"=>20]);
            $content=$r->getBody()->getContents();
            if ($r->getStatusCode()==200) {
                if ($this->json_validate($content)) {
                    $json_content=json_decode($content,true);
                    $delivered_code=$json_content["code"];
                    $delivered_message=$json_content["message"];
                    $delivered_data=$json_content["data"];
                    if ($delivered_code=200) {
                        foreach ($delivered_data as $delivered) {
                            $order_numbers[$delivered["order_number"]]=1;
                        }
                        $ret=$json_content;
                    } else {
                        $title = sprintf("妥投接口状态错误，状态码为%s",$delivered_code);
                        $code = "-1";
                        $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s", $url, $content);
                        $this->error_process($title, $code, $message);
                    }
                } else {
                    $title = sprintf("妥投接口返回内容错误，不是json格式");
                    $code = "-2";
                    $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s",$url,$content);
                    $this->error_process($title, $code, $message);
                }
            } else {
                $title = sprintf("妥投接口错误,{0}",$r->getStatusCode());
                $code = "-3";
                $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s",$url,$content);
                $this->error_process($title, $code, $message);
            }
            $this->retry_times=0;
            return ["order_numbers"=>array_keys($order_numbers),"content"=>$ret];
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            $this->retry_times=$this->retry_times + 1;
            if ($this->retry_times<=10) {
                return $this->read_delivered_orders($url);
            }
        } catch (\Exception $e) {
            echo get_class($e).PHP_EOL;
            echo $e->getMessage().PHP_EOL;
        }
    }

    public function error_process($title,$code,$message) {
    }

    //delete lee 2020.12.7 改为klaviyo
//    public function getDeliveryJourneysUrl($email){
//        $url=sprintf("https://api.maropost.com/accounts/%s/journeys/%s/trigger/%s.json?auth_token=%s&email=%s",
//            static::ACCOUNT_ID,21,"27579871241115",static::AUTHTOKEN,$email);
//        return $url;
//    }
    //end

    private function send_delievered_email($order_numbers) {
        $c=0;
        foreach ($order_numbers["content"]["data"] as $order_row) {
            $c=$c+1;
            $order_number=$order_row["order_number"];
            $sql=sprintf("select so.entity_id,so.increment_id,so.customer_id,ce.email,ce.firstname,ce.lastname,ce.enabled_score,ps.all_score,ps.total_score,so.gaving_score,DATE_FORMAT(so.created_at,'%%m/%%d/%%Y') as created_at,DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 31 DAY),'%%m/%%d/%%Y') as active_at
from sales_order so
inner join customer_entity ce on ce.entity_id=so.customer_id
inner join pg_score ps on ps.entity_id=so.customer_id
where so.increment_id='%s'",$order_number);
            $row=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $customer_id=$row["customer_id"];
            $customer_email=$row["email"];
            $order_id=$row["entity_id"];
            $enabled_score=$row["enabled_score"];
            $firstname=$row["firstname"];
            $lastname=$row["lastname"];
            $tmp_point_active_date=date('m/d/Y',strtotime(date('Y-m-d H:i:s',strtotime($order_row["delivered_at"]))." + 31 day"));
            if ($this->check_email($customer_email)) {
                $customField=[
                    "tmp_so_gaving_points"=>$row["gaving_score"],
                    "tmp_so_increment_id"=>$row["increment_id"],
                    "tmp_point_active_date"=>$tmp_point_active_date,
                    "avail_total_points"=>$row["total_score"],//生效的积分
                    "total_points"=>$row["all_score"],//总积分
                ];
                $exchange_code=sprintf("delivery_sendmail_%s",$order_id);
                echo sprintf("send_delievered_email,count=%s,current=%s,order_number=%s,entity_id=%s,exchange_code=%s,enabled_score=%s".PHP_EOL,count($order_numbers["content"]["data"]),$c,$order_number,$order_id,$exchange_code,$enabled_score);
                $pgExchangeModel=PgExchangeData::findOne(["exchange_code"=>$exchange_code,"customer_id"=>$customer_id]);
                if ($enabled_score && empty($pgExchangeModel) && (int)$row["gaving_score"]>0)
                {
                    $recommend_list=Yii::$container->get("app\components\helper\RecommendHelper")->get_customer_recommend_products($customer_id);
                    foreach ($recommend_list as $key=>$recommend) {
                        $customField["recommend_product_image".($key+1)]=$recommend["image"];
                        $customField["recommend_product_name".($key+1)]=$recommend["name"];
                        $customField["recommend_product_price".($key+1)]=$recommend["product_price"];
                        $customField["recommend_product_url".($key+1)]=$recommend["url"];
                    }
                    $customer_properties=array_merge(['$email'=>$customer_email,'$first_name'=>$firstname,'$last_name'=>$lastname],$customField);
                    $properties=$customField;
                    $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("rewards delievered time",$customer_properties,$properties);
                    $pgExchangeModel=new PgExchangeData();
                    $pgExchangeModel->customer_id=$customer_id;
                    $pgExchangeModel->exchange_code=$exchange_code;
                    $exchange_data=[
                        "data"=>$customField,
                        "customer_email"=>$customer_email,
                        "firstname"=>$firstname,
                        "lastname"=>$lastname,
                    ];
                    $pgExchangeModel->data_content=json_encode($exchange_data);
                    $pgExchangeModel->save();
                }
            }
        }
    }

    private function check_email($email) {
        $result = trim($email);
        if (filter_var($result, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * 积分生效
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionActive()
    {
        //取30天前妥投的订单
        $url=$this->get_30days_before_delivered_url();
        $order_numbers=$this->read_delivered_orders($url);
        if (count($order_numbers)==0) {
            return ExitCode::OK;
        }

        $c=0;
        foreach ($order_numbers["order_numbers"] as $order_number) {
            $c=$c+1;
            echo sprintf("order_number=%s".PHP_EOL,$order_number);
            $ret=PgScoreBill::soScoreActiveByIncrementId($order_number);
            echo sprintf("积分生效,count=%s,current=%s,order_number=%s,ret=%s".PHP_EOL,count($order_numbers["order_numbers"]),$c,$order_number,$ret);
        }

        //检查30天已妥投仍未生效
        $sql="select so.increment_id 
from pg_score_bill bill
inner join sales_order so on so.entity_id=bill.src_id and bill.src_type='sales_order'
where bill.src_type='sales_order' and delievered_time < DATE_SUB(now(), INTERVAL 30 DAY) and score_status=0";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($rows as $row) {
            $c=$c+1;
            $order_number=$row["increment_id"];
            echo sprintf("order_number=%s".PHP_EOL,$order_number);
            $ret=PgScoreBill::soScoreActiveByIncrementId($order_number);
            echo sprintf("30妥投仍未生效，count=%s,current=%s,order_number=%s,ret=%s".PHP_EOL,count($order_numbers["order_numbers"]),$c,$order_number,$ret);
        }

        return ExitCode::OK;
    }

    /**
     * 妥投超时处理
     */
    public function actionDeliveryTimtout() {
        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);

        $sql="SELECT ssg.order_id,ssg.order_increment_id,ssg.increment_id AS shipping_increment_id,ssg.order_created_at
,so.state,ssg.billing_address,ssg.billing_name,ssg.shipping_address,ssg.shipping_name,ssg.customer_email
,ssg.payment_method,ssg.shipping_information,ssg.created_at AS shipping_created_at ,bill.bill_time
,ssg.created_at
FROM sales_shipment_grid ssg
INNER JOIN sales_order so ON so.entity_id=ssg.order_id
INNER JOIN pg_score_bill bill ON bill.src_id=ssg.order_id AND bill.type_id='sales' AND bill.src_type='sales_order'
WHERE 
bill.delievered_time IS NULL
AND ssg.created_at<DATE_SUB(NOW(), INTERVAL 7 DAY)
ORDER BY ssg.`created_at`";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        if (count($rows)>0) {
            $fp = fopen("/lihf/a.csv", "w");
            fputcsv($fp,array_keys($rows[0]));
            foreach ($rows as $key=>$row) {
                fputcsv($fp,$row);
            }
            fclose($fp);

            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
            $now=$localeDate->date()->format('Y-m-d H:i:s');
            $monitor_log=$objectManager->create("Pg\Monitor\Log");
            $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
            $dingding_yunying_chat_id = $_scopeConfig->getValue('config/general/dingding_yunying_chat_id',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
            $monitor_log->sendDingDingMessage(sprintf("%s单shipping超过7天未妥投",count($rows)),$dingding_yunying_chat_id
                ,[
                    "file"=>"/lihf/a.csv",
                    "filename"=>sprintf("%s单shipping超过7天未妥投.csv",count($rows))
                ]
            );
        }

        //积分未妥投
        $sql="SELECT * FROM (
SELECT so.customer_id,ce.`email`,ce.firstname,ce.lastname,ssg.order_id,ssg.order_increment_id,ssg.increment_id AS shipping_increment_id,ssg.order_created_at
,so.increment_id,so.state,ssg.billing_address,ssg.billing_name,ssg.shipping_address,ssg.shipping_name,ssg.customer_email
,ssg.payment_method,ssg.shipping_information,ssg.created_at AS shipping_created_at ,bill.bill_time
,(SELECT COUNT(`score`) AS sum_score FROM pg_score_bill WHERE src_type='sales_order' AND src_id=bill.src_id) AS sum_score
,ssg.created_at,so.gaving_score,ps.all_score
FROM sales_shipment_grid ssg
INNER JOIN sales_order so ON so.entity_id=ssg.order_id
INNER JOIN pg_score_bill bill ON bill.src_id=ssg.order_id AND bill.type_id='sales' AND bill.src_type='sales_order'
INNER JOIN customer_entity ce ON ce.`entity_id`=so.`customer_id`
INNER JOIN pg_score ps ON ps.`entity_id`=ce.`entity_id`
WHERE 
ce.`enabled_score`=1
AND bill.delievered_time IS NULL
AND ssg.created_at<DATE_SUB(NOW(), INTERVAL 7 DAY)
) t
WHERE t.sum_score>0
ORDER BY created_at DESC";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        if (count($rows)>0) {
            $fp = fopen("/lihf/a.csv", "w");
            fputcsv($fp,array_keys($rows[0]));
            foreach ($rows as $key=>$row) {
                fputcsv($fp,$row);
            }
            fclose($fp);

            $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
            $magento_autoload->loadMagentoFrameword();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
            $now=$localeDate->date()->format('Y-m-d H:i:s');
            $monitor_log=$objectManager->create("Pg\Monitor\Log");
            $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
            $dingding_yunying_chat_id = $_scopeConfig->getValue('config/general/dingding_yunying_chat_id',\Magento\Store\Model\ScopeInterface::SCOPE_STORE,'default');
            $monitor_log->sendDingDingMessage(sprintf("%s单积分用户shipping超过7天未妥投",count($rows)),$dingding_yunying_chat_id
                ,[
                    "file"=>"/lihf/a.csv",
                    "filename"=>sprintf("%s单积分用户shipping超过7天未妥投.csv",count($rows))
                ]
            );
            //发邮件
            $customer_id=$row["customer_id"];
//            $customer_email=$row["email"];
            $customer_email="zhuqiuying@zhijingoptical.com";
            $order_id=$row["order_id"];
            $firstname=$row["firstname"];
            $lastname=$row["lastname"];
            $customField=[
                "tmp_so_gaving_points"=>$row["gaving_score"],
                "tmp_so_increment_id"=>$row["increment_id"],
                "avail_total_points"=>$row["sum_score"],//生效的积分
                "total_points"=>$row["all_score"],//总积分
            ];
            $exchange_code=sprintf("delivery_sendmail_%s",$order_id);
            $pgExchangeModel=PgExchangeData::findOne(["exchange_code"=>$exchange_code,"customer_id"=>$customer_id]);
            if (empty($pgExchangeModel) && (int)$row["gaving_score"]>0) {
                $recommend_list = Yii::$container->get("app\components\helper\RecommendHelper")->get_customer_recommend_products($customer_id);
                foreach ($recommend_list as $key => $recommend) {
                    $customField["recommend_product_image" . ($key + 1)] = $recommend["image"];
                    $customField["recommend_product_name" . ($key + 1)] = $recommend["name"];
                    $customField["recommend_product_price" . ($key + 1)] = $recommend["product_price"];
                    $customField["recommend_product_url" . ($key + 1)] = $recommend["url"];
                }
                //delete lee 2020.12.7 改为klaviyo
//                $contract = $maropost_api->createOrUpdateForList(static::LIST_ID, $customer_email, $firstname, $lastname, ",", null, null, $customField);
//                usleep(2000);
//                $client = new GuzzleClient();
//                $url = $this->getDeliveryJourneysUrl($customer_email);
//                $res = $client->request('POST', $url);
                //end
                $pgExchangeModel = new PgExchangeData();
                $pgExchangeModel->customer_id = $customer_id;
                $pgExchangeModel->exchange_code = $exchange_code;
                $exchange_data = [
                    "data" => $customField,
                    "customer_email" => $customer_email,
                    "firstname" => $firstname,
                    "lastname" => $lastname,
                ];
                $pgExchangeModel->data_content = json_encode($exchange_data);
                $pgExchangeModel->save();
            }
        }
    }

    /**
     * 回写妥投时间
     * @return int
     * @throws \yii\db\Exception
     */
    public function actionDelieveredCaiji() {
        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $url=$this->get_delivered_url();
//        $order_numbers=$this->read_delivered_orders($url);
//        if (count($order_numbers)==0) {
//            return ExitCode::OK;
//        }

        $order_numbers=[
            "order_numbers"=>["100228491","100228488"],
            "content"=>["message"=>"OK","code"=>200,"data"=>
                ["lab_number"=>"01128-228491-9T11","delivered_at"=>"2020-12-08T22:35:16.863113","order_number"=>"100228491","id"=>353266,"order_datetime"=>"2020-11-23T12:58:32"],
                ["lab_number"=>"01128-228488-9T11","delivered_at"=>"2020-12-09T18:43:42.250026","order_number"=>"100228488","id"=>339990,"order_datetime"=>"2020-11-23T12:58:32"]
            ]
        ];

        //发送妥投邮件给积分用户
//        $this->send_delievered_email($order_numbers);

        //回写妥投时间
        $c=0;
        foreach ($order_numbers["content"]["data"] as $order) {
            $c=$c+1;
            $order_number=$order["order_number"];
            $delivered_at=$order["delivered_at"];
            $delivered_at= str_replace(array('T','Z'),' ',$delivered_at);
            //echo sprintf("order_number=%s".PHP_EOL,$order_number);
            $sql=sprintf("update pg_score_bill set delievered_time='%s' where src_type='sales_order' and src_id=(select entity_id from sales_order where increment_id='%s') and delievered_time is null",$delivered_at,$order_number);
            \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
        }

        //根据更新时间补充妥把风时间
        $today=Yii::$container->get("app\components\helper\TimezoneHelper")->getDate();
        $start_date=date("Y-m-d", strtotime("-2 days", strtotime($today)));
        $end_date=date("Y-m-d", strtotime("0 days", strtotime($today)));
        $url=sprintf(self::ORDER_DELIVERED_API_URL."/api/get_order_delivered_list/?update_start_date=%s&update_end_date=%s",$start_date,$end_date);
        $order_numbers=$this->read_delivered_orders($url);
        $c=0;
        foreach ($order_numbers["content"]["data"] as $order) {
            $c=$c+1;
            $order_number=$order["order_number"];
            $delivered_at=$order["delivered_at"];
            $delivered_at= str_replace(array('T','Z'),' ',$delivered_at);
            //echo sprintf("order_number=%s".PHP_EOL,$order_number);
            $sql=sprintf("update pg_score_bill set delievered_time='%s' where src_type='sales_order' and src_id=(select entity_id from sales_order where increment_id='%s') and delievered_time is null",$delivered_at,$order_number);
            \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
        }

        //所有未补充的全部执行
        $this->deliveryFix();
        return ExitCode::OK;
    }

    private function get_so_delivery_status($so_increment_id) {
        $url=sprintf(self::ORDER_DELIVERED_API_URL."/api/get_order_delivered_list/?order_number=%s",$so_increment_id);

        try {
            $client = new GuzzleClient();
            $r=$client->get($url,["timeout"=>20]);
            $content=$r->getBody()->getContents();
            if ($r->getStatusCode()==200) {
                if ($this->json_validate($content)) {
                    $json_content=json_decode($content,true);
                    return $json_content;
                }
            }
            $this->retry_times=0;
            return null;
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            $this->retry_times=$this->retry_times + 1;
            if ($this->retry_times<=10) {
                return $this->get_so_delivery_status($url);
            }
        } catch (\Exception $e) {
            echo get_class($e).PHP_EOL;
            echo $e->getMessage().PHP_EOL;
        }
    }

    public function deliveryFix() {
        $sql="select so.entity_id,so.state,so.increment_id,so.created_at,bill.bill_time,bill.delievered_time
from pg_score_bill bill
inner join sales_order so on so.entity_id=bill.src_id and bill.src_type='sales_order'
where
(so.state='processing' or so.state='complete')
and bill.bill_time<DATE_SUB(NOW(), INTERVAL 20 DAY)
and bill.bill_time>DATE_SUB(NOW(), INTERVAL 90 DAY)
and bill.delievered_time is null order by id";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($rows as $key=>$row) {
            $log=sprintf("count=%s,index=%s,entity_id=%s,increment_id=%s".PHP_EOL,count($rows),$key,$row["entity_id"],$row["increment_id"]);
            echo $log;
            $content=$this->get_so_delivery_status($row["increment_id"]);
            if (!empty($content) && isset($content["data"][0])) {
                $delivered_at=$content["data"][0]["delivered_at"];
                if (!empty($delivered_at)) {
                    $delivered_at=str_replace(array('T','Z'),' ',$delivered_at);
                    $log=sprintf("increment_id=%s,delivered_at=%s".PHP_EOL,$row["increment_id"],$delivered_at);
                    echo $log;
                    $sql=sprintf("update pg_score_bill set delievered_time='%s' where src_type='sales_order' and src_id=(select entity_id from sales_order where increment_id='%s') and delievered_time is null",$delivered_at,$row["increment_id"]);
                    \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
                }
            }
        }

        $sql="select ssg.order_id,ssg.order_increment_id,ssg.increment_id as shipping_increment_id,ssg.order_created_at,so.state,so.status,ssg.billing_address,ssg.billing_name,ssg.shipping_address,ssg.shipping_name,ssg.customer_email,ssg.payment_method,ssg.shipping_information,ssg.created_at as shipping_created_at 
,bill.bill_time,bill.delievered_time
from sales_shipment_grid ssg
inner join sales_order so on so.entity_id=ssg.order_id
inner join pg_score_bill bill on bill.src_id=ssg.order_id and bill.src_type='sales_order'
where 
bill.delievered_time is null
and ssg.created_at<DATE_SUB(now(), INTERVAL 2 DAY)
order by ssg.created_at desc";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($rows as $key=>$row) {
            $log=sprintf("count=%s,index=%s,order_id=%s,increment_id=%s".PHP_EOL,count($rows),$key,$row["order_id"],$row["order_increment_id"]);
            echo $log;
            $content=$this->get_so_delivery_status($row["order_increment_id"]);
            if (!empty($content) && isset($content["data"][0])) {
                $delivered_at=$content["data"][0]["delivered_at"];
                if (!empty($delivered_at)) {
                    $delivered_at=str_replace(array('T','Z'),' ',$delivered_at);
                    $log=sprintf("increment_id=%s,delivered_at=%s".PHP_EOL,$row["order_increment_id"],$delivered_at);
                    echo $log;
                    $sql=sprintf("update pg_score_bill set delievered_time='%s' where src_type='sales_order' and src_id=(select entity_id from sales_order where increment_id='%s') and delievered_time is null",$delivered_at,$row["order_increment_id"]);
                    \app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
                }
            }
        }
    }

    private function getSoGavingScoreByIncrement($increment_id) {
        $sql=sprintf("select * from pg_score_bill where operation=0 and src_type='sales_order' and src_id=(select entity_id from sales_order where increment_id='%s') order by id desc limit 1",$increment_id);
        $score=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
        return $score;
    }

    /**
     * 取消的订单处理
     * @return int
     * @throws \yii\db\Exception
     */
    public function actionSoCancelProcess() {
        $today=Yii::$container->get("app\components\helper\TimezoneHelper")->getDate();
        $now=Yii::$container->get("app\components\helper\TimezoneHelper")->getTime();
        $sql="select entity_id,increment_id,customer_id,created_at
from sales_order_grid 
where created_at>'2020-01-01'
and status='closed' 
and entity_id not in (select src_id from pg_score_bill where type_id='so_closed' and src_type='sales_order')
order by entity_id";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($rows as $key => $row) {
            $src_bill=$this->getSoGavingScoreByIncrement($row["increment_id"]);
            if (!empty($src_bill)) {
                $bill=new PgScoreBill();
                $bill->customer_id=$row["customer_id"];
                $bill->type_id="so_closed";
                $bill->src_type="sales_order";
                $bill->src_id=$row["entity_id"];
                $bill->operation=1;//减积分
                $bill->score = 0 - abs($src_bill["score"]);
                $additional_data=[];
                $bill->additional_data=json_encode($additional_data);
                $bill->message="";
                $bill->score_status=0;
                $bill->bill_time=$now;
                $ret=$bill->save();
                //重算用户积分
                PgScoreBill::soScoreActive($row["entity_id"]);
                PgScoreBill::calcCustomerScore($row["customer_id"]);
            }
        }

        return ExitCode::OK;
    }

    public function actionRmSoCancelProcess() {
        $today=Yii::$container->get("app\components\helper\TimezoneHelper")->getDate();
        $now=Yii::$container->get("app\components\helper\TimezoneHelper")->getTime();
        $sql="select * from pg_score_bill where type_id='so_closed'";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($rows as $key => $row) {
            $sql=sprintf("delete from pg_score_bill where id=%s",$row["id"]);
            Yii::$app->db->createCommand($sql)->execute();
            PgScoreBill::calcCustomerScore($row["customer_id"]);
        }

        return ExitCode::OK;
    }

    /**
     * 初始化订单推荐表
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionInitRecommendProductOrder() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        //初始化推荐表
        $sql="DELETE FROM recommend_product_order";
        $errno=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
        echo sprintf("errno=%s".PHP_EOL,$errno);

        $GLASSES_ATTRIBUTE_SET_ID=$objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["GLASSES_ATTRIBUTE_SET_ID"];
        $sql=sprintf("INSERT INTO recommend_product_order(product_id,sku,cnt,`name`,product_price,product_img_url,product_url)
SELECT soi.`product_id`,soi.`sku`,COUNT(0) AS cnt,
product.`name`,product.`price` AS product_price,
CONCAT((SELECT `value` FROM core_config_data WHERE path='config/general/static_url'),'/media/catalog/product/cache/230x115',product.image) AS product_img_url,
CONCAT((SELECT `value` FROM core_config_data WHERE path='web/secure/base_url'),(SELECT request_path FROM url_rewrite WHERE entity_id=sl.`product_id` and entity_type='pg_frame' AND sitemap=1 and lens_type in ('Single','Blue_Non_Rx','Non_Rx') limit 1)) AS product_url
#CONCAT((SELECT `value` FROM core_config_data WHERE path='web/secure/base_url'),(SELECT request_path FROM url_rewrite WHERE entity_id=sl.`parent_id` AND sitemap=1),\"?s=\",soi.sku) AS product_url
FROM sales_order_item soi
INNER JOIN `catalog_product_flat_1` product ON soi.product_id=product.entity_id
INNER JOIN `cataloginventory_stock_item` stock ON stock.product_id=product.entity_id
INNER JOIN `catalog_product_super_link` sl ON sl.`product_id`=soi.`product_id`
WHERE 
product.`type_id`='simple'
AND stock.website_id=0
AND soi.`created_at`>DATE_SUB(NOW(), INTERVAL 60 DAY)
AND stock.`is_in_stock`=1 AND stock.`qty`>=30
AND product.`attribute_set_id`=%s
GROUP BY soi.`product_id`,soi.`sku`,sl.`parent_id`
ORDER BY cnt DESC
LIMIT 100",$GLASSES_ATTRIBUTE_SET_ID);
        $errno=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
        echo sprintf("errno=%s".PHP_EOL,$errno);

        //初始化推荐产品最终表
        $sql="DELETE FROM recommend_product";
        $errno=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
        echo sprintf("errno=%s".PHP_EOL,$errno);

        $sql=sprintf("INSERT INTO recommend_product(configurable_id,simple_id,`name`,sku,qty,is_in_stock,product_price,image)
SELECT DISTINCT link.parent_id AS configurable_id,item.product_id AS simple_id,item.`name`,product.sku,stock.qty,stock.is_in_stock,product.price AS product_price,product.image
FROM sales_order_item item
INNER JOIN catalog_product_flat_1 product ON item.product_id=product.entity_id
INNER JOIN cataloginventory_stock_item stock ON stock.product_id=product.entity_id
INNER JOIN `catalog_product_super_link` link ON link.product_id=product.entity_id
INNER JOIN recommend_product_order soo ON soo.product_id=product.entity_id
WHERE stock.website_id=0
AND product.type_id='simple'
AND item.`created_at`>DATE_SUB(NOW(), INTERVAL 30 DAY)
AND stock.`is_in_stock`=1 AND stock.`qty`>=30");
        $errno=\app\components\helper\DbHelper::getMasterDb()->createCommand($sql)->execute();
        echo sprintf("errno=%s".PHP_EOL,$errno);
    }
}
