<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\FileHelper;
use app\models\PgScore;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RemakeOrderController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */

    public function actionRemake(){
        $sql = "select order_number,item_id,remake_order,remake_qty,id from pg_order_remake where is_reduce = 0";
        $pg_order_remake_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($pg_order_remake_data as $key => $value){
            $order_number = $value['order_number'];
            $item_id = $value['item_id'];
            $remake_order = $value['remake_order'];
            $remake_qty = $value['remake_qty'];
            $id = $value['id'];
            $sql = "select sum(remake_qty) as sum_remake_qty from pg_order_remake where order_number = $order_number and item_id = $item_id and is_reduce = 1";
            $pg_order_remake_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $sum_remake_qty = $pg_order_remake_data['sum_remake_qty'];
            $sql = "select qty_ordered,(gaving_score/qty_ordered) as one_gaving_score from sales_order_item where item_id = $item_id";
            $sales_order_item_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $qty_ordered = $sales_order_item_data['qty_ordered'];
            $one_gaving_score = $sales_order_item_data['one_gaving_score'];
            $sql = "select entity_id from sales_order where increment_id = $order_number";
            $sales_order_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $entity_id = $sales_order_data['entity_id'];
            $sql = "select customer_id,src_type,src_id,score_status from pg_score_bill where src_id = $entity_id and remake_code = 0";
            $sales_order_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $score = 0;
            $tr = Yii::$app->db->beginTransaction();
            try {
                if($qty_ordered - $sum_remake_qty >0){
                    if(($qty_ordered - $sum_remake_qty) > $remake_qty){
                        $score = $remake_qty * $one_gaving_score;
                    }else{
                        $score = ($qty_ordered - $sum_remake_qty) * $one_gaving_score;
                    }
                }else{
                    $score = 0;
                }
                $keys = '';
                $values = '';
                $sql = "select count(*) as count from pg_score_bill where remake_id = $id";
                $cnt = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
                if($cnt['count'] == 0){
                    if($sales_order_data){
                        foreach ($sales_order_data as $k => $v){
                            $keys .= '`'.$k.'`,';
                            $values .= '\''.$v.'\',';
                        }
                        $keys = '('.$keys.'`score`,`additional_data`,`remake_code`,`operation`,`type_id`,`remake_id`)';
                        $additional_data = array("order_id"=>array("order_id"=> $entity_id),"quote"=>array("quote_id"=>$item_id));
                        $additional_data = json_encode($additional_data);
                        $values = '('.$values.'\''.-$score.'\',\''.$additional_data.'\',1,1,\'so_remake\','.$id.')';
                        $sql = "insert into pg_score_bill $keys values $values";
                        $sales_order_insert_code = Yii::$app->db->createCommand($sql)->execute();
                        if($score != 0){
                            $customer_id = $sales_order_data['customer_id'];
                            $cnts=PgScore::revise($customer_id);
                        }
                    }
                }
                $sql = "update pg_order_remake set is_reduce = 1 where id = $id";
                $data = Yii::$app->db->createCommand($sql)->execute();
                $tr->commit();
            } catch (Exception $e) {
                //回滚
                $tr->rollBack();
                continue;
            }
        }
        return ExitCode::OK;
    }
}
