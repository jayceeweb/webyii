<?php
namespace app\commands;

use GuzzleHttp\Client as GuzzleClient;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use \Maropost\Api\Contacts;
use app\components\helper\DbHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ReviewController extends Controller
{
    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
    const LIST_ID=3;
    const POINTS_USER_JOURNESID=26;
    const NON_POINTS_USER_JOURNESID=27;
    const TRIGGER="13426068036880";
    const order_delivered_api_url = "http://oms.zhijingoptical.cn";
    const order_complain_api_url="https://feedback.zhijingoptical.cn/api/get_order_complain_list/";
    private $retry_times=0;

    private $maropost_api=null;

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    private function get_delivered_url() {
        $start_date = date("Y-m-d", strtotime("-3 day"));
        $end_date = date("Y-m-d", strtotime("-3 day"));
        $url=sprintf(static::order_delivered_api_url."/api/get_order_delivered_list/?start_date=%s&end_date=%s",$start_date,$end_date);
        return $url;
    }

    public function read_delivered_orders($url) {
        $ret=null;//妥投订单号
        $order_numbers=[];

        try {
            $client = new GuzzleClient();
            $r=$client->get($url,["timeout"=>20]);
            $content=$r->getBody()->getContents();
            if ($r->getStatusCode()==200) {
                if ($this->json_validate($content)) {
                    $json_content=json_decode($content,true);
                    $delivered_code=$json_content["code"];
                    $delivered_message=$json_content["message"];
                    $delivered_data=$json_content["data"];
                    if ($delivered_code=200) {
                        foreach ($delivered_data as $delivered) {
                            $order_numbers[$delivered["order_number"]]=1;
                        }
                        $ret=$json_content;
                    } else {
                        $title = sprintf("妥投接口状态错误，状态码为%s",$delivered_code);
                        $code = "-1";
                        $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s", $url, $content);
                        $this->error_process($title, $code, $message);
                    }
                } else {
                    $title = sprintf("妥投接口返回内容错误，不是json格式");
                    $code = "-2";
                    $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s",$url,$content);
                    $this->error_process($title, $code, $message);
                }
            } else {
                $title = sprintf("妥投接口错误,{0}",$r->getStatusCode());
                $code = "-3";
                $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s",$url,$content);
                $this->error_process($title, $code, $message);
            }
            $this->retry_times=0;
            return ["order_numbers"=>array_keys($order_numbers),"content"=>$ret];
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            $this->retry_times=$this->retry_times + 1;
            if ($this->retry_times<=10) {
                return $this->read_delivered_orders($url);
            }
        } catch (\Exception $e) {
            echo get_class($e).PHP_EOL;
            echo $e->getMessage().PHP_EOL;
        }
    }

    private function read_order_complain_list($order_numbers){
        try {
            $client = new GuzzleClient();
            $url=static::order_complain_api_url;
            $r=$client->request('POST', $url, [
                'timeout' => 60,
                'json'=>["order_number_list"=>$order_numbers]
            ]);
            $content=$r->getBody()->getContents();
            if ($r->getStatusCode()==200) {
                if ($this->json_validate($content)) {
                    $json_content=json_decode($content,true);
                    $delivered_code=$json_content["code"];
                    $delivered_message=$json_content["message"];
                    $delivered_data=$json_content["data"];
                    if ($delivered_code=200) {
                        foreach ($delivered_data as $delivered) {
                            $order_numbers[$delivered["order_number"]]=1;
                        }
                        $ret=$json_content;
                    } else {
                        $title = sprintf("投诉接口状态错误，状态码为%s",$delivered_code);
                        $code = "-1";
                        $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s", $url, $content);
                        $this->error_process($title, $code, $message);
                    }
                } else {
                    $title = sprintf("投诉接口返回内容错误，不是json格式");
                    $code = "-2";
                    $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s",$url,$content);
                    $this->error_process($title, $code, $message);
                }
            } else {
                $title = sprintf("投诉接口错误,{0}",$r->getStatusCode());
                $code = "-3";
                $message = sprintf("请求内容为：%s<br>返回内容为：<br>%s",$url,$content);
                $this->error_process($title, $code, $message);
            }
            $this->retry_times=0;
            return ["order_numbers"=>array_keys($order_numbers),"content"=>$ret];
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            $this->retry_times=$this->retry_times + 1;
            if ($this->retry_times<=10) {
                return $this->read_order_complain_list($order_numbers);
            }
        } catch (\Exception $e) {
            echo get_class($e).PHP_EOL;
            echo $e->getMessage().PHP_EOL;
        }
    }

    public function error_process($title,$code,$message) {
    }

    public function json_validate($string) {
        if (is_string($string)) {
            @json_decode($string);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }

    private function get_orders_info($order_numbers) {
        $sql=sprintf("SELECT distinct so.increment_id,so.`entity_id` AS order_id,so.`created_at`,so.customer_id,ce.`enabled_score`,ce.`firstname`,ce.`lastname`,ce.`email`,soi.`product_id` AS configurable_id,product.`entity_id` AS simple_id,product.`sku`,p1.`value` AS `name`,so.`gaving_score`
FROM sales_order so
INNER JOIN customer_entity ce ON ce.`entity_id`=so.`customer_id`
INNER JOIN sales_order_item soi ON soi.`order_id`=so.`entity_id`
INNER JOIN catalog_product_flat_all_1 product ON product.`sku`=soi.`sku`
INNER JOIN catalog_product_entity_varchar p1 ON p1.entity_id=product.entity_id
WHERE 
so.`increment_id` IN (%s)
AND p1.`attribute_id`=(SELECT attribute_id FROM eav_attribute WHERE attribute_code='name' AND entity_type_id=4)
AND soi.item_id IN (
	SELECT MAX(item_id) FROM sales_order_item WHERE order_id=so.`entity_id` AND product_type='configurable'
)
AND (so.`is_clone` IS NULL OR so.`is_clone`=0) and so.gaving_score>0","'".implode("','",$order_numbers)."'");
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        return $rows;
    }

    private function send_review_mail($order){
        $email=$order["email"];
        if ($this->check_email($email)) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productHelper=$objectManager->get("Pg\CatalogExtend\Helper\Product");
            $sku_url=$productHelper->getPDPUrl($order["simple_id"]);
            $sql=sprintf("SELECT image,price FROM `catalog_product_flat_all_1` WHERE entity_id=%s",$order["simple_id"]);
            $product_info=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            $image_url=$productHelper->getBaseMediaUrl()."catalog/product" . $product_info["image"];
            $customField=[
                "firstname"=>$order["firstname"],
                "lastname"=>$order["lastname"],
                "tmp_first_product_url"=>$sku_url,
                "tmp_first_product_img_url"=>$image_url,
                "tmp_first_product_name"=>$order["name"],
                "tmp_first_product_price"=>$product_info["price"],
                "tmp_first_product_sku"=>$order["sku"],
                "tmp_so_increment_id"=>$order["increment_id"],
                "tmp_enabled_score"=>$order["enabled_score"],
                "tmp_gaving_score"=>$order["gaving_score"],
            ];

            //delete lee 2020.12.7 改为klaviyo
//        if ($order["enabled_score"]==1) {
//            //启用积分，且积分大于0
//            $url=sprintf("https://api.maropost.com/accounts/%s/journeys/%s/trigger/%s.json?auth_token=%s&email=%s",static::ACCOUNT_ID,static::POINTS_USER_JOURNESID,static::TRIGGER,static::AUTHTOKEN,$email);
//        } else {
//            $url=sprintf("https://api.maropost.com/accounts/%s/journeys/%s/trigger/%s.json?auth_token=%s&email=%s",static::ACCOUNT_ID,static::NON_POINTS_USER_JOURNESID,static::TRIGGER,static::AUTHTOKEN,$email);
//        }
//        $maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
//        $contract=$maropost_api->createOrUpdateForList(static::LIST_ID,$email,$order["firstname"],$order["lastname"],null,null,null,$customField);
//        $client = new GuzzleClient();
//        $res = $client->request('POST', $url);

            //klaviyo暂停此功能
//            $customer_properties=array_merge(['$email'=>$email,'$first_name'=>$order["firstname"],'$last_name'=>$order["lastname"]],$customField);
//            $properties=$customField;
//            if ($order["enabled_score"]==1) {
//                //启用积分，且积分大于0
//                $event_id="review-reward";
//            } else {
//                $event_id="review-non reward";
//            }
//            $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->track($event_id,$customer_properties,$properties);
//            echo $ret.PHP_EOL;
        }
    }

    private function check_email($email) {
        $result = trim($email);
        if (filter_var($result, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * 积分用户，订单妥投后，没有投诉，给用户发邮件评论
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionSendMessage() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        //查询妥投订单
        $order_number_list=$this->read_delivered_orders($this->get_delivered_url());
        $order_numbers=$order_number_list["order_numbers"];
        if (count($order_numbers)==0) {
            return;
        }

        //查询投诉订单
        $complains=$this->read_order_complain_list($order_numbers);
        $complains=$complains["content"];
        $complains_keys=array_keys($complains);

        //已妥股，没有投诉
        $no_complains_numberss=[];
        foreach ($order_numbers as $o) {
            if (!in_array($o,$complains_keys)) {
                $no_complains_numberss[]=$o;
            }
        }
        if (count($no_complains_numberss)==0) {
            return;
        }

        //测试代码
//        $no_complains_numberss=['100228488'];

        //获取订单信息
        $orderlist=$this->get_orders_info($no_complains_numberss);

        $send_count=0;
        foreach($orderlist as $order) {
            $this->send_review_mail($order);
//            $send_count=$send_count + 1;
//            if ($send_count>50) {
//                break;
//            }
        }
    }
}