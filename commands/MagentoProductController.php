<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\components\helper\MagentoCacheHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MagentoProductController extends Controller
{
    public function actionMagentoProductCache()
    {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $cache=MagentoCacheHelper::getInstance();

        $sql="SELECT entity.entity_id,entity.sku,entity.type_id,entity.attribute_set_id
FROM `catalog_product_entity` entity
INNER JOIN catalog_product_entity_int attr ON attr.entity_id=entity.entity_id AND attr.attribute_id=(SELECT attribute_id FROM eav_attribute WHERE entity_type_id=4 AND attribute_code='status')
WHERE 
attr.value=1";
        $rows=\app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $i=0;
        foreach ($rows as $row) {
            $i=$i+1;
            $entity_id=$row["entity_id"];
            $sku=$row["sku"];
            $p=$objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface")->getById($entity_id);
            $key=sprintf("MAGENTO_PRODUCT_CACHE_BY_ID_%s",md5($entity_id));
            $cache->setCache($key,$p->toJson());
            $key=sprintf("MAGENTO_PRODUCT_CACHE_BY_KEY_%s",md5($sku));
            $cache->setCache($key,$p->toJson());
            $log=sprintf("count=%s,index=%s,id=%s,key=%s".PHP_EOL,count($rows),$i,$entity_id,$key);
            echo $log;
        }
    }
}
