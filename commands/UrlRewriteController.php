<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\FileHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UrlRewriteController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        $i = 0;
        $sql = "select entity_id from url_rewrite where entity_type='pg_grant_frame'";
        $entity_id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        foreach ($entity_id as $key => $val){
            $parent_id = array();
            $data = array();
            $sql = "select parent_id from catalog_product_super_link where product_id=$val[entity_id]";
            $parent_id = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            if(!$parent_id['parent_id']){
                $parent_id['parent_id'] = 0;
            }
            $sql = "select * from pg_product_tag where product_id=$parent_id[parent_id] and tag='grant'";
            $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryOne();
            if(!$data){
                $sql = "delete from url_rewrite where entity_type='pg_grant_frame' and entity_id=$val[entity_id]";
                $data = Yii::$app->db->createCommand($sql)->execute();
                $i += 1;
            }
        }
        return ExitCode::OK;
    }
}
