<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\PgExchangeData;
use app\components\helper\AuthcodeHelper;
use app\components\helper\DbHelper;
use app\models\CustomerEntity;

/**
 * 客户下订单了就给推送邮件邀请加入积分用户，只给非积分用户推送
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ScoreInvitationController extends Controller
{
    private $public_api_key;
    private $private_api_key;
    private $default_klaviyo_list;

    private $log_file="";
    private $start_entity_id=0;
    private $page_size=2000;
    private $every_time_size=100;
    private $total_count=0;
    private $c=0;
    private $authcode_secret="";

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    private function savelog($log,$output=true) {
        if (!empty($this->log_file)) {
            file_put_contents($this->log_file,$log.PHP_EOL,FILE_APPEND);
        }
        if ($output) {
            $datetime = new \DateTime();
            echo $datetime->format('Y-m-d H:i:s')." -> ".$log.PHP_EOL;
        }
    }

    private function get_magento_so_member_list_sql($is_count=false) {
        $sql = "SELECT 
s.`entity_id`,`s`.`customer_email` AS `customer_email`, `s`.`customer_id`
%s
FROM
  `sales_order` s
  INNER JOIN `customer_entity` c1 ON `c1`.`entity_id` = `s`.`customer_id`
WHERE
`s`.`entity_id` > %s
AND s.created_at >= DATE_SUB(now(), INTERVAL 5 DAY)
#AND s.`customer_email`='zhuqiuying@zhijingoptical.com'
%s";
        $select_fields = ",`c1`.`firstname`
,`c1`.`lastname`
,`c1`.`gender`
,`c1`.`group_id`
,DATE_FORMAT(`c1`.`created_at`, '%Y-%m-%d %H:%i:%s') AS 'reg_date'
,(select enabled_score from customer_entity where entity_id=c1.entity_id) as enabled_score
";
        $order_sql="ORDER BY `s`.`entity_id` ASC";
        if ($is_count) {
            $sql = sprintf($sql, "", $this->start_entity_id, "");
        } else {
            $sql = sprintf($sql, $select_fields, $this->start_entity_id, $order_sql);
        }
        return $sql;
    }

    private function get_magento_so_member_count(){
        $sql=$this->get_magento_so_member_list_sql(true);
        $count_sql = sprintf("SELECT count(0) as cnt from (%s) t",$sql);
        $cnt=Yii::$app->getDb()->createCommand($count_sql)->queryScalar();
        $total_page = ceil($cnt / $this->page_size);
        $this->total_count = (int)$cnt;
        $log=sprintf("total count=%s,total page=%s,page size=%s",$this->total_count,$total_page,$this->page_size);
        $this->savelog($log);
        return $total_page;
    }

    private function get_magento_so_member_list() {
        $sql = $this->get_magento_so_member_list_sql( False);
        $sql = sprintf("SELECT * from (%s) t limit %s,%s",$sql, 0, $this->page_size);
        $data=Yii::$app->getDb()->createCommand($sql)->queryAll();
        return $data;
    }

    private function toTime($d) {
        if (!empty($d)) {
            $pos=strrpos($d,"-");
            $fetch=substr($d,$pos,1);
            $fetch_v=(int)explode(":",substr($d,$pos+1,strlen($d)-$pos-1))[0];
            return date("Y-m-d H:i:s",strtotime(sprintf("%s%s hours",$fetch,$fetch_v),strtotime($d)));
        } else {
            return "";
        }
    }

    private function sync_by_list($list) {
        $sql = "select * from customer_group";
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $group_arr = array("Wholesale","WeCommerce","Volume","Low Income","Rewards","VIP","Preferred");
        $arr = array();
        foreach ($data as $key => $value){
            foreach ($group_arr as $k => $v){
                if($value['customer_group_code'] == $v){
                    $arr[] = $value['customer_group_id'];
                }
            }
        }
        $i=0;//每次列表计数，每100次上传一次
        $profiles=[];
        foreach ($list as $member) {
            $i=$i+1;
            $this->c=$this->c+1;
            $this->start_entity_id = $member["entity_id"];
            $log = sprintf("total=%s,current=%s,customer_id=%s,email=%s,entity_id=%s", $this->total_count,$this->c,$member["customer_id"],$member["customer_email"],$member["entity_id"]);
            $this->savelog($log);

            $customer_id=$member["customer_id"];
            $customer_id_auth_code=$this->authcode_encode($customer_id);
            $reg_date=$member["reg_date"];
            $firstname = $member["firstname"];
            $lastname=$member["lastname"];
            $gender=$member["gender"];
            $genderstr="";
            if ($member["gender"]==null) {
                $gender="3";
                $genderstr = "Not Specified";
            } else if ($member["gender"]=="1") {
                $genderstr="Male";
            } else if ($member["gender"]=="2") {
                $genderstr="Female";
            } else if ($member["gender"]=="3") {
                $genderstr="Not Specified";
            } else {
                $gender = "3";
                $genderstr = "Not Specified";
            }
            $customer_email = strtolower($member["customer_email"]);
            $email_auth_code=$this->authcode_encode($customer_email);
            $log=sprintf("customer_id=%s,reg_date=%s,firstname=%s,lastname=%s,entity_id=%s,customer_email=%s",
                $customer_id,$reg_date,$firstname,$lastname,$customer_id,$customer_email);
            $this->savelog($log);
            $exchange_code=sprintf("so_rewards_invitation_%s_%s",$customer_id,$member["entity_id"]);
            $sql=sprintf("select count(0) as cnt from pg_exchange_data where exchange_code='%s'",$exchange_code);
            echo $sql.PHP_EOL;
            $cnt=$this->getDb()->createCommand($sql)->queryScalar();
            if ($cnt==0) {
                $can_synd=true;
            } else {
                $can_synd=false;
            }
            //对比是否需要更新
            if ($can_synd) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
                $email_expiration_time=$localeDate->date("+7 days")->format('m/d/Y');
                if ($this->check_email($customer_email)) {
                    $customField=[
                        '$email'=>$customer_email,
                        '$first_name'=>$member["firstname"],
                        '$last_name'=>$member["lastname"],
                        'email'=>$customer_email,
                        'first_name'=>$member["firstname"],
                        'last_name'=>$member["lastname"],
                        "register_date"=>$reg_date,
                        "so_customer_id"=>$customer_id,
                        "customer_id_auth_code"=>$customer_id_auth_code,
                        "email_auth_code"=>$email_auth_code,
                        "email_expiration_time"=>$email_expiration_time
                    ];
                    if (!empty($customer_id)) {
                        $exchange_ret=$this->save_exchange_id($member,$customer_id);
                        $exchange_id=$exchange_ret["exchange_id"];
                        if (!empty($exchange_id)) {
                            $customField["tmp_rewards_invitation_id"]=$exchange_id;
                            $customField["tmp_rewards_invitation_id_auth_code"]=$this->authcode_encode($exchange_id);
                            $update_sql=sprintf("update customer_entity set confirm_email='%s',update_confirm_email_time=now(),confirm_email_operation=2 where entity_id=%s",$exchange_ret["confirm_email"],$customer_id);
                            Yii::$app->db->createCommand($update_sql)->execute();
                            $sql=sprintf("select count(0) as cnt from pg_score where entity_id=%s",$customer_id);
                            $cnt=$this->getDb()->createCommand($sql)->queryScalar();
                            if ($cnt==0) {
                                $insert_sql="insert into pg_score(entity_id,recommended) values ($customer_id,1)";
                                Yii::$app->db->createCommand($insert_sql)->execute();
                            } else {
                                $update_sql = "update pg_score set recommended=1 where entity_id = $customer_id";
                                Yii::$app->db->createCommand($update_sql)->execute();
                            }
                        }
                    }
                    $customer_properties=$customField;
                    $properties=$customField;
                    echo print_r($customer_properties,true).PHP_EOL;
                    $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("rewards invitation",$customer_properties,$properties);
                }
            }
        }
    }

    private function save_exchange_id($member,$customer_id) {
        if (!empty($customer_id)) {
            $objectManager=\Magento\Framework\App\ObjectManager::getInstance();
            $customerDo=CustomerEntity::findOne(["entity_id"=>$customer_id]);
            $customer_entity_email=$customerDo->getAttribute("email");
            $confirm_email=$customer_entity_email;
            $exchange_code=sprintf("so_rewards_invitation_%s_%s",$customer_id,$member["entity_id"]);
            $pgExchangeModel=PgExchangeData::findOne(["exchange_code"=>$exchange_code]);
            if (empty($pgExchangeModel)) {
                $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
                $now=$localeDate->date()->format('Y-m-d H:i:s');
                $now_day=$localeDate->date()->format('m/d/Y');
                $catime = strtotime($now);
                $catime += 86400*7;
                $expiration_now=date('Y-m-d H:i:s',$catime);
                $expiration_now_day=date('m/d/Y',$catime);

                $pgExchangeModel=new PgExchangeData();
                $pgExchangeModel->customer_id=$customer_id;
                $pgExchangeModel->exchange_code=$exchange_code;
                $exchange_data=[
                    "email"=>$customer_entity_email,
                    "confirm_email"=>$confirm_email,
                    "email_send_time"=>$now,
                    "expiration_now"=>$expiration_now,
                    "tmp_email_send_time_str"=>$now_day,
                    "tmp_email_expiration_time_str"=>$expiration_now_day,
                ];
                $pgExchangeModel->data_content=json_encode($exchange_data);
                $pgExchangeModel->save();
            }
            $exchange_id=$pgExchangeModel->getAttribute("exchange_id");
            return ["exchange_id"=>$exchange_id,"confirm_email"=>$confirm_email];
        } else {
            return ["exchange_id"=>0,"confirm_email"=>""];
        }
    }

    private function sync_so() {
        $total_page=$this->get_magento_so_member_count();
        for ($page_no=0; $page_no<$total_page;$page_no++) {
            $list=$this->get_magento_so_member_list();
            $this->sync_by_list($list);
        }
    }

    public function authcode_encode($data) {
        return base64_encode(AuthcodeHelper::encode($data,$this->authcode_secret));
    }

    private function check_email($email) {
        $result = trim($email);
        if (filter_var($result, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        else{
            return false;
        }
    }

    public function actionSync($log_file="/lihf/maropost.log") {
        if ($log_file!="") {
            $this->log_file=$log_file;
        }
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_scopeConfig = $objectManager->create("Magento\Framework\App\Config");
        $this->public_api_key = $_scopeConfig->getValue('klaviyo_reclaim_general/general/public_api_key');
        $this->private_api_key = $_scopeConfig->getValue('klaviyo_reclaim_general/general/private_api_key');
        $objectManager->get("Magento\Framework\Encryption\EncryptorInterface")->decrypt($this->private_api_key);
        $this->default_klaviyo_list = $_scopeConfig->getValue('config/general/default_klaviyo_list');

        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $this->savelog("start sync ...",true);

        //计算开始订单ID
//        $sql="select max(entity_id) as entity_id from pg_maropost_pg_so_log";
//        $max_entity_id=$this->getDb()->createCommand($sql)->queryScalar();
//        if (empty($max_entity_id)) {
//            $max_entity_id=0;
//        }
//        $this->start_entity_id=$max_entity_id;

        $this->sync_so();

        return ExitCode::OK;
    }
}
