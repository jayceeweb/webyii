<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class ES2Controller extends Controller
{
    private function getES(){
        $hosts = [
            '127.0.0.1:9200'
        ];
        $client = \Elasticsearch\ClientBuilder::create()
            ->setRetries(2)
            ->setHosts($hosts)
            ->build();
        return $client;
    }

    public function actionCreateIndex() {
        $client=$this->getES();
        $params = [
            'index' => 'customer',
            'body' => [
                'mappings'=>[
                    'properties' => [
                        'first_name' => [
                            'type' => 'text'
                        ],
                        'lastname' => [
                            'type' => 'text'
                        ],
                        'age'=>[
                            'type'=>'integer'
                        ]
                    ]
                ]
            ]
        ];
        $response=$client->indices()->exists(['index' => 'customer']);
        if ($response) {
            $response = $client->indices()->delete(['index' => 'customer']);
        }
        $response = $client->indices()->create($params);
    }

    public function actionPutMap() {
        $client=$this->getES();
        $params = [
            'index' => 'customer',
            'body' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'text'
                    ],
                    'lastname' => [
                        'type' => 'text'
                    ],
                    'age'=>[
                        'type'=>'integer'
                    ],
                    'address'=>[
                        'type'=>'text'
                    ]
                ]
            ]
        ];
        $client->indices()->putMapping($params);
    }
}
