<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\FileHelper;
use app\models\CoreConfigData;
use app\modules\manager\models\AdminUser;
use Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use Maropost\Api\Journeys;
use GuzzleHttp\Client as GuzzleClient;
use app\components\helper\AuthcodeHelper;
use app\models\PgExchangeData;

/**
 * 此功能暂时没有使用
 * Class SendCoupCodeController
 * @package app\commands
 */
class SendCoupCodeController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($all_data)
    {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();
        //获取url前缀
        $MediaUrl=CoreConfigData::getMedia();
        //图片
        $BaseUrl=CoreConfigData::getBaseUrl();
        //网站
        //邮件配置
        $ACCOUNT_ID=2160;
        $AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
        $REWARD_MEMBERS_LIST_ID=3;
        //连接magento，邮件发送和日志

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dr=$objectManager->get("Magento\Framework\Filesystem\DirectoryList");
        //邮件
        $monitor_log=$objectManager->create("Pg\Monitor\Log");
        $monitor_log->log_start("cart");
        //日志

//        1.获取2020年1月1日以后注册并且没有订单的用户；
//        2.获取这些用户有效的购物车id
//        3.连接购物车详情，获取商品的图片，链接，名称，价格
//        4.筛选出最先加入购物车的商品
        //请求多次数据库  start

        $sql = "SELECT entity_id FROM customer_entity WHERE customer_entity.created_at > \"2020-01-11\" AND NOT EXISTS ( SELECT * FROM sales_order WHERE sales_order.customer_id = customer_entity.entity_id )";
//        1.获取2020年1月1日以后注册并且没有订单的用户；
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $customer_data = array();
        foreach ($data as $key => $val){
            $customer_data[] = $val['entity_id'];
        }
        $customer_data = array_flip($customer_data);
        $sql = "select customer_firstname as firstname,customer_lastname as lastname,customer_id,customer_email as email,entity_id from quote where is_active = 1 and subtotal>50";
//        2.获取这些用户有效的购物车id
        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $quote_data = array();
        foreach ($data as $key => $value){
            if($value['customer_id']){
                $quote_data[$value['customer_id']] = $value;
            }
        }
        $customers_data = array_intersect_key($quote_data,$customer_data);
//        筛选出条件1,2都相同的用户
        $quote_item_data = array();
        $quote_id = '';
        foreach ($customers_data as $key => $val){
            $quote_item_data[$val['entity_id']] = $val;
            $quote_id .= $val['entity_id'].',';
        }
        $quote_id = rtrim($quote_id,',');
        $sql = "SELECT * FROM ( SELECT product_id, sku, `name`, ROUND((row_total / qty), 2) AS price, quote_id, created_at FROM quote_item WHERE quote_id IN ($quote_id) AND product_type = 'configurable' ) AS datas WHERE ( SELECT COUNT(0) FROM ( SELECT created_at, quote_id FROM quote_item WHERE quote_id IN ($quote_id) AND product_type = 'configurable' ) AS dataa WHERE dataa.quote_id = datas.quote_id AND dataa.created_at > datas.created_at ) < 2 ORDER BY datas.created_at DESC";
        $quote_items_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
//        连接购物车详情，获取商品的名称，价格
        $sku = '';
        foreach ($quote_items_data as $key => $value){
            $sku .= '\''.$value['sku'].'\',';
        }
        $sku = rtrim($sku,',');
        $sql = "select entity_id,image,sku from catalog_product_flat_1 where sku in ($sku)";
//        连接商品详情，获取商品的图片
        $catalog_product_flat_1_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $catalog_product_flat_1_datas = array();
        $catalog_product_flat_1_str = '';
        foreach ($catalog_product_flat_1_data as $key =>$value){
            $catalog_product_flat_1_datas[$value['entity_id']]['image'] = $value['image'];
            $catalog_product_flat_1_datas[$value['entity_id']]['sku'] = $value['sku'];
            $catalog_product_flat_1_str .= $value['entity_id'].',';
        }
        $catalog_product_flat_1_str = rtrim($catalog_product_flat_1_str,',');
        $sql = "SELECT entity_id, request_path as url FROM url_rewrite WHERE entity_id in ($catalog_product_flat_1_str) group by entity_id";
//        连接商品url表，获取商品的链接
        $url_rewrite_data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
        $url_rewrite_datas = array();
//        数据处理，数据合并 start
        foreach ($url_rewrite_data as $key =>$value){
            $url_rewrite_datas[$value['entity_id']]['url'] = $value['url'];
        }
        $url_data = array();
        foreach ($catalog_product_flat_1_datas as $key => $value){
            foreach ($url_rewrite_datas as $k => $val){
                if($key == $k){
                    $url_data[$k] = array_merge($value,$val);
                }
            }
        }
        foreach ($quote_items_data as $key => $value){
            foreach ($url_data as $k => $v){
                if($value['sku'] == $v['sku']){
                    $quote_items_data[$key] = array_merge($value,$v);
                }
            }
        }
        $cart_data = array();
        foreach ($quote_items_data as $key => $value){
            foreach ($quote_item_data as $k => $v){
                if($value['quote_id'] == $k){
                    $cart_data[$v['email']][] = array_merge($value,$v);
                }
            }
        }
//        数据处理，数据合并 end $cart_data
        //请求多次数据库  end
        //请求一次数据库  start
//        $sql = "SELECT * FROM ( SELECT customer_entity.email, firstname, lastname, qi.product_id, qi.sku, qi.`name`, qi.row_total / qi.qty AS price, qi.quote_id, qi.created_at, catalog_product_flat_1.image, url_rewrite.request_path AS url FROM ( SELECT customer_entity.email, firstname, lastname, entity_id FROM customer_entity WHERE customer_entity.created_at > \"2020-01-01\" AND NOT EXISTS ( SELECT * FROM sales_order WHERE sales_order.customer_id = customer_entity.entity_id )) AS `customer_entity` INNER JOIN quote ON customer_entity.entity_id = quote.customer_id AND quote.subtotal > 50 INNER JOIN quote_item AS qi ON qi.quote_id = quote.entity_id AND qi.product_type = 'configurable' AND is_active = 1 INNER JOIN catalog_product_flat_1 ON catalog_product_flat_1.sku = qi.sku INNER JOIN ( SELECT entity_id, request_path FROM url_rewrite GROUP BY entity_id ) `url_rewrite` ON url_rewrite.entity_id = catalog_product_flat_1.entity_id ) AS datas WHERE ( SELECT count(*) FROM ( SELECT qi.created_at, qi.quote_id FROM ( SELECT customer_entity.email, firstname, lastname, entity_id FROM customer_entity WHERE customer_entity.created_at > \"2020-01-11\" AND NOT EXISTS ( SELECT * FROM sales_order WHERE sales_order.customer_id = customer_entity.entity_id )) AS `customer_entity` INNER JOIN quote ON customer_entity.entity_id = quote.customer_id AND quote.subtotal > 50 AND is_active = 1 INNER JOIN quote_item AS qi ON qi.quote_id = quote.entity_id AND qi.product_type = 'configurable' ) AS dataa WHERE dataa.quote_id = datas.quote_id AND dataa.created_at > datas.created_at ) < 2 ORDER BY datas.created_at DESC";
//        $data = \app\components\helper\DbHelper::getSlaveDb()->createCommand($sql)->queryAll();
//
//        //根据用户邮箱数据重组
//        $cart_data1 = array();
//        foreach ($data as $key => $value){
//            $cart_data1[$value['email']][] = $value;
//        }
//        var_dump($cart_data);
//        var_dump($cart_data1);
//        die();
        //请求一次数据库  end
        //邮件发送
        $email_data = array();
        $log = array();
        $error_log = array();
        foreach ($cart_data as $key => $value){
            for($i=0;$i<count($value);$i++){
                $email_data[$key]['email'] = $key;
                $email_data[$key]['firstname'] = $value[$i]['firstname'];
                $email_data[$key]['lastname'] = $value[$i]['lastname'];
                $num = $i+1;
                if(!isset($value[$i]['image']) || !isset($value[$i]['name']) || !isset($value[$i]['price']) || !isset($value[$i]['url'])){
                    continue;
                }
                $email_data[$key]['data']['activation_code_expiration_time'] = date("Y-m-d", strtotime("+7 days"));
                $email_data[$key]['data']['quote_product_image'.$num] = $MediaUrl.'catalog/product/cache/600x300'.$value[$i]['image'];
                $email_data[$key]['data']['quote_product_name'.$num] = $value[$i]['name'];
                $email_data[$key]['data']['quote_product_price'.$num] = $value[$i]['price'];
                $email_data[$key]['data']['quote_product_url'.$num] = $BaseUrl.'/'.$value[$i]['url'];
            }
        }
        $document_root=$dr->getRoot();
        include_once $document_root."/lib/internal/Pg/Maropost/tests/autoloader.php";
        $contacts=new Contacts($ACCOUNT_ID, $AUTHTOKEN);

        $Daily_output = 1;
        // 每日发送数量

        foreach ($email_data as $key => $value){
            if(!isset($email_data[$key]['data'])){
                continue;
            }
            $email_data[$key]['email'] = $key;
            //delete lee 2020.12.4
//            $contacts->createOrUpdateForList($REWARD_MEMBERS_LIST_ID,$email_data[$key]['email'],$email_data[$key]['firstname'],$email_data[$key]['lastname'],null,null,null,$email_data[$key]['data']);
//            $client = new GuzzleClient();
//            $url=sprintf("https://api.maropost.com/accounts/2160/journeys/18/trigger/8181040588408.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ&email=%s",$email_data[$key]['email']);
//            $res = $client->request('POST', $url);
//            $err_no=$res->getBody()->getContents();
//            $data = array("email"=>$email_data[$key]['email'],"data"=>$email_data[$key]['data']);
//            if (strtolower($err_no)=="success!") {
//                array_push($log,$data);
//            } else {
//                array_push($error_log,$data);
//            }
            //end
            if($log){
                $monitor_log->log(json_encode($log));
            }
            if($error_log){
                $monitor_log->log_error(["code"=>-1,"data"=>json_encode($error_log)]);
            }
            $Daily_output ++;
            if($Daily_output > $all_data){
                break;
            }
            var_dump($key);
        }
    }
}
