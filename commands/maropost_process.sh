#!/bin/bash

source /etc/profile
LENGTH=`ps aux | grep 'php yii maropost-delivery/process' | grep -v grep | wc -l`
if test $LENGTH -eq 0
then
set -i
cd /data/webyii/www
/usr/local/php/bin/php yii maropost-delivery/process
fi

exit 0
