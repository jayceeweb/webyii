<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use \Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use app\components\helper\AuthcodeHelper;
use app\components\helper\DbHelper;
use app\models\PgExchangeData;
use app\models\CustomerEntity;

/**
 * 暂时先不使用
 * Class MaropostSoController
 * @package app\commands
 */
class MaropostSoController extends Controller
{
    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
    const LIST_ID=3;

    const STATUS_SUBSCRIBED = 1;
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;
    const STATUS_UNCONFIRMED = 4;
    const STATUS_CLEANED = 5;
    const STATUS_MAILCHIMP_ERROR = 6;
    const STATUS_HAS_CHINESE = 7;

    private $log_file="";
    private $divisor=0;
    private $remainder=0;
    private $start_customer_id=0;
    private $page_size=2000;
    private $total_count=0;
    private $c=0;
    private $authcode_secret="";

    private $maropost_api=null;

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    private function savelog($log,$output=true) {
        if (!empty($this->log_file)) {
            file_put_contents($this->log_file,$log.PHP_EOL,FILE_APPEND);
        }
        if ($output) {
            $datetime = new \DateTime();
            echo $datetime->format('Y-m-d H:i:s')." -> ".$log.PHP_EOL;
        }
    }

    private function get_magento_so_member_list_sql($is_count=false) {
        $sql = "SELECT DISTINCT
`s`.`customer_email` AS `customer_email`, `s`.`customer_id`
%s
FROM
  `sales_order` s
  INNER JOIN `customer_entity` c1 ON `c1`.`entity_id` = `s`.`customer_id`
WHERE
customer_email NOT IN (
    SELECT subscriber_email FROM newsletter_subscriber
)
AND 
`s`.`customer_id` >= %s
AND s.entity_id %% %s = %s
#AND s.`customer_email`='skwagen@aol.com'
%s";
        $select_fields = ",`c1`.`firstname`
,`c1`.`lastname`
,`c1`.`gender`
,`c1`.`group_id`
,(SELECT DATE_FORMAT(`created_at`, '%Y-%m-%d %H:%i:%s') FROM customer_entity WHERE customer_entity.`entity_id`=s.`customer_id`) AS 'reg_date'
,(SELECT DATE_FORMAT(MIN(`created_at`), '%Y-%m-%d %H:%i:%s') FROM sales_order WHERE sales_order.`customer_id`=s.`customer_id`) AS first_purchase_date
,(SELECT DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') FROM sales_order WHERE sales_order.`customer_id`=s.`customer_id`) AS last_purchase_date
,(SELECT DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') FROM sales_order WHERE sales_order.`customer_id`=s.`customer_id` AND coupon_code IS NULL AND discount_amount=0 AND discount_amount=0 HAVING sum(subtotal)>5.95) AS last_non_exchange_order_date
,(SELECT COUNT(0) FROM `sales_order` so WHERE `so`.`customer_id` = `c1`.`entity_id`) AS 'orders_count'
,(SELECT COUNT(0) FROM sales_order_item
WHERE product_type='configurable'
AND glasses_prescription_id IN (SELECT entity_id FROM glasses_prescription WHERE ladd !=0.00 OR radd !=0.00)
AND order_id IN(SELECT entity_id FROM sales_order WHERE sales_order.customer_id=c1.entity_id)) AS progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND profile_prescription_id IN (SELECT entity_id FROM prescription_entity WHERE ladd !=0.00 OR radd !=0.00)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_count
,(SELECT MAX(created_at) AS created_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_created_at
,(SELECT MAX(updated_at) AS updated_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_updated_at
,(SELECT COUNT(0) AS cnt FROM `sales_order` WHERE customer_id=c1.entity_id AND store_id=1 AND is_virtual=0 AND (state='processing' OR state='closed') AND created_at >= DATE_SUB(NOW(), INTERVAL 21 DAY)) AS three_weeks_purchase
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='simple') as frame_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='configurable') as glasses_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND soi.is_virtual=1) as craft_avg_price";
        $order_sql="ORDER BY `s`.`customer_id` ASC";
        if ($is_count) {
            $sql = sprintf($sql, "", $this->start_customer_id, $this->divisor, $this->remainder, "");
        } else {
            $sql = sprintf($sql, $select_fields, $this->start_customer_id, $this->divisor, $this->remainder, $order_sql);
        }
        return $sql;
    }

    private function get_magento_so_member_count(){
        $sql=$this->get_magento_so_member_list_sql(true);
        $count_sql = sprintf("SELECT count(0) as cnt from (%s) t",$sql);
        $cnt=$this->getDb()->createCommand($count_sql)->queryScalar();
        $total_page = ceil($cnt / $this->page_size);
        $this->total_count = (int)$cnt;
        $log=sprintf("total count=%s,total page=%s,page size=%s",$this->total_count,$total_page,$this->page_size);
        $this->savelog($log);
        return $total_page;
    }

    private function get_magento_so_member_list() {
        $sql = $this->get_magento_so_member_list_sql( False);
        $sql = sprintf("SELECT * from (%s) t limit %s,%s",$sql, 0, $this->page_size);
        $data=$this->getDb()->createCommand($sql)->queryAll();
        return $data;
    }

    private function toTime($d) {
        if (!empty($d)) {
            $pos=strrpos($d,"-");
            $fetch=substr($d,$pos,1);
            $fetch_v=(int)explode(":",substr($d,$pos+1,strlen($d)-$pos-1))[0];
            return date("Y-m-d H:i:s",strtotime(sprintf("%s%s hours",$fetch,$fetch_v),strtotime($d)));
        } else {
            return "";
        }
    }

    private function compareDateEqual($d1,$d2) {
        if ((empty($d1) || ($d1=="0000-00-00 00:00:00"))
            || (empty($d2) || ($d2=="0000-00-00 00:00:00"))
        ) {
            return true;
        }
        if (empty($d1)) {
            $d1="0000-00-00 00:00:00";
        }
        if (empty($d2)) {
            $d2="0000-00-00 00:00:00";
        }
        $d1=$this->toTime($d1);
        if (empty($d1)) {
            $d1="0000-00-00 00:00:00";
        }
        if (empty($d2)) {
            $d2="0000-00-00 00:00:00";
        }
        if ($d1==$d2) {
            return true;
        } else {
            return false;
        }
    }

    private function sync_by_list($list) {
        foreach ($list as $member) {
            $this->c=$this->c+1;
            $this->start_customer_id = $member["customer_id"];
            $log = sprintf("total %s records,now is process the %s record...", $this->total_count,$this->c);
            $this->savelog($log);

            $customer_id=$member["customer_id"];
            $customer_id_auth_code=$this->authcode_encode($customer_id);
            $reg_date=$member["reg_date"];
            $firstname = $member["firstname"];
            $lastname=$member["lastname"];
            $gender=$member["gender"];
            $genderstr="";
            if ($member["gender"]==null) {
                $gender="3";
                $genderstr = "Not Specified";
            } else if ($member["gender"]=="1") {
                $genderstr="Male";
            } else if ($member["gender"]=="2") {
                $genderstr="Female";
            } else if ($member["gender"]=="3") {
                $genderstr="Not Specified";
            } else {
                $gender = "3";
                $genderstr = "Not Specified";
            }
            $customer_email = strtolower($member["customer_email"]);
            $email_auth_code=$this->authcode_encode($customer_email);
            $first_purchase_date = $member["first_purchase_date"];
            $last_purchase_date = $member["last_purchase_date"];
            $last_non_exchange_order_date = $member["last_non_exchange_order_date"];
            $orders_count = $member["orders_count"];
            $progressive_count = $member["progressive_count"];
            $quote_progressive_count = $member["quote_progressive_count"];
            $quote_count=$member["quote_count"];
            $quote_created_at=$member["quote_created_at"];
            $quote_updated_at = $member["quote_updated_at"];
            $frame_avg_price = $member["frame_avg_price"];
            $glasses_avg_price = $member["glasses_avg_price"];
            $craft_avg_price = $member["craft_avg_price"];
            $log=sprintf("customer_id=%s,reg_date=%s,firstname=%s,lastname=%s,gender=%s,customer_email=%s,first_purchase_date=%s,last_purchase_date=%s,orders_count=%s,progressive_count=%s,genderstr=%s,quote_progressive_count=%s,quote_count=%s,quote_created_at=%s,quote_updated_at=%s",
                $customer_id,$reg_date,$firstname,$lastname,$gender,$customer_email,$first_purchase_date,$last_purchase_date,$orders_count,$progressive_count,$genderstr,$quote_progressive_count,$quote_count,$quote_created_at,$quote_updated_at);
            $this->savelog($log);
            $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
            $contract=$this->maropost_api->getForEmail($customer_email);
            $can_synd=false;
            $contractData=null;
            if ($contract->isSuccess!=1) {
                $can_synd=true;
            } else if (
                (empty($firstname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $firstname, $match))
                && (empty($lastname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $lastname, $match))
            ) {
                $contractData=$contract->getData();
                if (
                    $contractData->first_name!=$firstname
                    || $contractData->last_name!=$lastname
                    || !$this->compareDateEqual($contractData->last_purchase_date,$last_purchase_date)
                    || !$this->compareDateEqual($contractData->register_date,$reg_date)
                    || !$this->compareDateEqual($contractData->last_non_exchange_order_date,$last_non_exchange_order_date)
                    || $contractData->gender_str!=$genderstr
                    || (int)$contractData->order_count!=(int)$orders_count
                    || (int)$contractData->progressive_count!=(int)$progressive_count
                    || !$this->compareDateEqual($contractData->first_purchase_date,$first_purchase_date)
                    || (int)$contractData->quote_progressive_count!=(int)$quote_progressive_count
                    || (int)$contractData->quote_count!=(int)$quote_count
                    || !$this->compareDateEqual($contractData->quote_updated_at,$quote_updated_at)
                    || round($contractData->frame_avg_price,2)!=round($frame_avg_price,2)
                    || round($contractData->glasses_avg_price,2)!=round($glasses_avg_price,2)
                    || round($contractData->craft_avg_price,2)!=round($craft_avg_price,2)
                    || !$this->compareDateEqual($contractData->quote_created_at,$quote_created_at)
                ) {
                    $can_synd=true;
                }
            }
            $can_synd=true;
            if ($contract->isSuccess) {
                $contractData=$contract->getData();
            }
            //对比是否需要更新
            if ($can_synd) {
                $customField=[
                    "craft_avg_price"=>round($craft_avg_price,2),
                    "customer_id"=>$customer_id,
                    "first_purchase_date"=>$first_purchase_date,
                    "frame_avg_price"=>$frame_avg_price,
                    "gender_str"=>$genderstr,
                    "glasses_avg_price"=>round($glasses_avg_price,2),
                    "last_purchase_date"=>$last_purchase_date,
                    "order_count"=>$orders_count,
                    "progressive_count"=>$progressive_count,
                    "quote_count"=>$quote_count,
                    "quote_progressive_count"=>$quote_progressive_count,
                    "quote_created_at"=>$quote_created_at,
                    "quote_updated_at"=>$quote_updated_at,
                    "register_date"=>$reg_date,
                    "so_customer_id"=>$customer_id,
                    "customer_id_auth_code"=>$customer_id_auth_code,
                    "email_auth_code"=>$email_auth_code,
                    "subscribe_status"=>false,
                ];
                $contract=$this->maropost_api->createOrUpdateForList(static::LIST_ID,$customer_email,$firstname,$lastname,",",null,null,$customField);
            }
        }
    }

    private function sync_so() {
        $total_page=$this->get_magento_so_member_count();
        for ($page_no=0; $page_no<$total_page;$page_no++) {
            $list=$this->get_magento_so_member_list();
            $this->sync_by_list($list);
        }
    }

    public function authcode_encode($data) {
        return base64_encode(AuthcodeHelper::encode($data,$this->authcode_secret));
    }

    public function actionSync($log_file="/lihf/maropost.log",$divisor="5",$remainder="0") {
        if ($log_file!="") {
            $this->log_file=$log_file;
        }
        if ($divisor!="") {
            $this->divisor=$divisor;
        }
        if ($remainder!="") {
            $this->remainder=$remainder;
        }
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();
        $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
        $this->savelog("start sync ...",true);

        $this->sync_so();

        return ExitCode::OK;
    }

    public function actionTest() {
        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $contact=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
        $c=$contact->getForEmail("zhangyaqin_0@163.com");
        print_r($c);

        $b=new TransactionalCampaigns(static::ACCOUNT_ID, static::AUTHTOKEN);
        $err_no=$b->sendEmail(21,null,null,null,null,null,null,null,$c->getData()->id);
        print_r($err_no);
    }
}
