<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use \Maropost\Api\Contacts;
use app\components\helper\AuthcodeHelper;
use \app\models\PgScoreBill;
use app\components\helper\MagentoCacheHelper;
use app\models\PgS3File;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    function getplaintextintrofromhtml($html) {
        $re1 = new \RegExp("<.+?>","g");
        $msg = $html.replace($re1,'');
        return $msg;
    }

    public function actionT1() {
        $html=file_get_contents("/lihf/1.html");
        echo $this->getplaintextintrofromhtml($html).PHP_EOL;
    }

    public function actionT2() {
//        \Yii::$app->cache->set("li","li");
//        echo \Yii::$app->cache->get("li").PHP_EOL;

        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager=\Magento\Framework\App\ObjectManager::getInstance();

        $localeDate=$objectManager->get("\Magento\Framework\Stdlib\DateTime\TimezoneInterface");
        $order_createat="2020-03-09 04:21:26";
        $d1=date('Y-m-d h:i:s',strtotime($localeDate->formatDateTime($order_createat)));//->format('Y-m-d h:i:s');
        $now_time=$localeDate->date()->format('Y-m-d h:i:s');

        echo sprintf("d1=%s,now_time=%s",$d1,$now_time).PHP_EOL;
    }

    public function actionT3() {
        $excludes=[
            [
                "origin_order_number"=>"100072063",
                "origin_item_id"=>"14497",
                "created_at"=>"2020.02.12",
                "frame"=>"32707I07",
            ],
            [
                "origin_order_number"=>"100072063",
                "origin_item_id"=>"14497",
                "created_at"=>"2020.02.12",
                "frame"=>"32707I07",
            ],
            [
                "origin_order_number"=>"100072063",
                "origin_item_id"=>"14499",
                "created_at"=>"2020.02.12",
                "frame"=>"22710G07",
            ]
        ];
        $excludes=json_decode(file_get_contents("/data/web450/www/commands/1.txt"),true);
        $orders=[];
        foreach ($excludes as $item_id) {
            if(!isset($orders[$item_id])) {
                $orders[$item_id]=1;
            } else {
                $orders[$item_id]=$orders[$item_id]+1;
            }
        }
    }

    public function actionT4() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $data="NWJmZVhLODJCSVRkVyt5N09DRVJLb1lmdFJhMkUzNURxOTlIaXRJZlUzN3NaZz09";
        echo AuthcodeHelper::decode_ex($data).PHP_EOL;

        $data="15513";
        echo AuthcodeHelper::encode_ex($data).PHP_EOL;
    }

    public function actionTest() {
        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $contacts=new Contacts(2160, "TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ");

//        $sql="SELECT email FROM customer_entity WHERE confirm_email_confirm=1";
//        $lists=Yii::$app->getDb()->createCommand($sql)->queryAll();
//        foreach ($lists as $row) {
//            $email=$row["email"];
//            echo $email.PHP_EOL;
//            $contacts->createOrUpdateForList(5,$email);
//        }
        $email="geekcui@gmail.com";
//        $contacts=new Contacts(2160, "TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ");
//        $contacts->createOrUpdateForList(5,$email);
        $e=$contacts->getForEmail($email);

//        $e=$contacts->createOrUpdateForList(null,"asdfsadf@163.com","li","haifeng",null,null,null,[
//            "gender_str"=>"未知"
//        ]);

        print_r($e);
//        print_r($e->getData()->list_subscriptions);
    }

    public function actionT5() {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

//        $current_product=Yii::$container->get("app\components\helper\FrameHelper")->getSimpleObject(1528);
//        $progressive_product_info=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getProgressiveProductInfo($current_product);
//        $bifocal_product_info=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getBifocalProductInfo($current_product);
//        $antireflective_product_info=$objectManager->get("Pg\CatalogExtend\Helper\Product")->getAntiReflectiveProductInfo($current_product);
//        print_r($progressive_product_info);
//        echo PHP_EOL;
//        print_r($bifocal_product_info);
//        echo PHP_EOL;
//        print_r($antireflective_product_info);
//        echo PHP_EOL;

        $product=$objectManager->get("Pg\CatalogExtend\Helper\Product");
        $dp=$product->getFramePrice('2686');
        print_r($dp);
    }

    public function actionT6() {
        $objFile=new PgS3File();
        $objFile->content_type="png";
        $objFile->obj_type="a";
        $objFile->obj_id=1;
        $objFile->customer_id=0;
        $objFile->profile_id=0;
        $objFile->profile_name="";
        $objFile->additional_data="";
        $objFile->save();
        print_r($objFile->getAttributes());
    }

    public function actionT7(){
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $customer_properties=[
            '$email'=>'zhuqiuying@zhijingoptical.com',
            'test_key'=>'lihaifeng3'
        ];
        $properties=[
            "YearElected"=>"1804",
            "PreviouslyVicePresident"=>"true",
            "VicePresidents"=>[
                "Aaron Burr","George Clinton"
            ],
            "items"=>[
                "name"=>"milk carton",
                "link"=>"https://www.netclipart"
            ]
        ];
        $ret=Yii::$container->get("app\components\helper\KlaviyoHelper")->track("Elected President4",$customer_properties,$properties);
        echo $ret;
    }
}
