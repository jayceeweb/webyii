1、权限初始化表
php yii migrate --migrationPath=@yii/rbac/migrations

2、根据配置进行初始化
php yii rbac/init

3、积分妥投
yii score-command/active
php yii score-command/delievered-caiji  #妥投数据采集

4、maropost发送邮件
yii maropost-delivery/process

5、初始化订单推荐表
yii score-command/init-recommend-product-order

6、同步customer_entity到maropost
sudo /usr/local/php/bin/php /data/webyii/www/yii maropost-customer-entity/sync


7、ES搜索测试
php yii e-s/t1

php yii maropost/sync /lihf/maropost.log 5 0
php yii maropost/sync /lihf/maropost.log 5 1
php yii maropost/sync /lihf/maropost.log 5 2
php yii maropost/sync /lihf/maropost.log 5 3
php yii maropost/sync /lihf/maropost.log 5 4

nohup /opt/web4/php/bin/php yii maropost/sync /lihf/maropost0.log 5 0 &
nohup /opt/web4/php/bin/php yii maropost/sync /lihf/maropost1.log 5 1 &
nohup /opt/web4/php/bin/php yii maropost/sync /lihf/maropost2.log 5 2 &
nohup /opt/web4/php/bin/php yii maropost/sync /lihf/maropost3.log 5 3 &
nohup /opt/web4/php/bin/php yii maropost/sync /lihf/maropost4.log 5 4 &

nohup /usr/local/php/bin/php yii so-contact-to-maropost/sync &

nohup /usr/local/php/bin/php yii maropost-so/sync /lihf/maropost0.log 5 0 &
nohup /usr/local/php/bin/php yii maropost-so/sync /lihf/maropost1.log 5 1 &
nohup /usr/local/php/bin/php yii maropost-so/sync /lihf/maropost2.log 5 2 &
nohup /usr/local/php/bin/php yii maropost-so/sync /lihf/maropost3.log 5 3 &
nohup /usr/local/php/bin/php yii maropost-so/sync /lihf/maropost4.log 5 4 &

/usr/local/php/bin/php yii maropost/sync /lihf/maropost.log 5 0

/usr/local/php/bin/php /data/webyii/www/yii frame/frame-urlrewrite-check

frame静态页生成
sudo /usr/local/php/bin/php /data/webyii/www/yii frame/cache-index 5 0

产品缓存生成
/usr/local/php/bin/php yii magento-product/magento-product-cache

8、
php yii klaviyo/sync /lihf/klaviyo.log 5 0
php yii klaviyo/sync2 /lihf/klaviyo.log 5   #同步klaviyo订阅表
php yii klaviyo/klaviyo/init-all-event  初始化所有事件
php yii klaviyo-customer-entity/sync /lihf/customer_entity_klaviyo.log 1 0
php yii klaviyo-customer-entity/sync2 /lihf/customer_entity_klaviyo.log 5       #同步klaviyo用户表
php yii so-contact-to-klaviyo/sync /lihf/so_contact_klaviyo.log         #同步klaviyo用户表订单数据
php yii last-visitor-klaviyo/sync /lihf/last_visitor_klaviyo.log        #同步klaviyo用户最近访问时间
php yii score-invitation/sync               #户下订单了就给推送邮件邀请加入积分用户，只给非积分用户推送

sudo /usr/local/php/bin/php /data/webyii/www/yii score-command/init-recommend-product-order  #初始化订单推荐表
sudo /usr/local/php/bin/php /data/webyii/www/yii score-command/active           #每天积分生效
sudo /usr/local/php/bin/php /data/webyii/www/yii score-command/delievered-caiji                         #订单妥投数据采集
#sudo /usr/local/php/bin/php /data/webyii/www/yii score-command/so-cancel-process                               #取消订单写入
sudo /usr/local/php/bin/php /data/webyii/www/yii score-command/delivery-timtout         #shipping超过7天未妥投
yii klaviyo-delivery/process        #klaviyo发送邮件
=======
/usr/local/php/bin/php yii magento-product/magento-product-cache

maropost同步
/usr/local/php/bin/php yii maropost/sync /lihf/maropost0.log 3 0

生成配件静态页
php yii accessories/cache-index

生成护目镜静态页
php yii protective/cache-index