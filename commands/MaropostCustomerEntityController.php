<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use \Maropost\Api\Contacts;
use Maropost\Api\TransactionalCampaigns;
use app\components\helper\AuthcodeHelper;
use app\components\helper\DbHelper;
use app\models\PgExchangeData;
use app\models\CustomerEntity;
use app\components\helper\SftpHelper;
use GuzzleHttp\Client as GuzzleClient;

class MaropostCustomerEntityController extends Controller
{
    const ACCOUNT_ID=2160;
    const AUTHTOKEN="TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ";
    const LIST_ID=3;

    const STATUS_SUBSCRIBED = 1;//订阅
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;//取消订阅
    const STATUS_UNCONFIRMED = 4;
    const STATUS_CLEANED = 5;
    const STATUS_MAILCHIMP_ERROR = 6;//同步mailchimp出错
    const STATUS_HAS_CHINESE = 7;//有中文

    private $log_file="";
    private $divisor=0;
    private $remainder=0;
    private $start_entity_id=0;
    private $page_size=2000;
    private $total_count=0;
    private $c=0;
    private $authcode_secret="";

    private $maropost_api=null;

    private $helper;

    public function getDb()
    {
        return DbHelper::getWebReadDb();
    }

    private function savelog($log,$output=true) {
        if (!empty($this->log_file)) {
            file_put_contents($this->log_file,$log.PHP_EOL,FILE_APPEND);
        }
        if ($output) {
            $datetime = new \DateTime();
            echo $datetime->format('Y-m-d H:i:s')." -> ".$log.PHP_EOL;
        }
    }

    private function get_magento_member_list_sql($is_count=false) {
        $sql = "SELECT `c1`.`entity_id`,`c1`.`email`
%s
FROM customer_entity c1
WHERE email NOT IN (
	SELECT subscriber_email FROM newsletter_subscriber
)
AND
`c1`.`entity_id` >= %s
# AND c1.`subscriber_email`='skwagen@aol.com'
%s";

        $count_sql="
SELECT COUNT(0) AS cnt FROM customer_entity ce
WHERE 
`ce`.`entity_id` >= %s
and email NOT IN (
	SELECT subscriber_email FROM newsletter_subscriber
)";

        $select_fields = ",`c1`.`firstname`
,`c1`.`lastname`
,`c1`.`gender`
,`c1`.`group_id`
,DATE_FORMAT(`c1`.`created_at`, '%Y-%m-%d %H:%i:%s') AS 'reg_date'
,(select DATE_FORMAT(MIN(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'first_purchase_date'
,(select DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') from sales_order where sales_order.`customer_id`=`c1`.`entity_id`) as 'last_purchase_date'
,(SELECT DATE_FORMAT(MAX(`created_at`), '%Y-%m-%d %H:%i:%s') FROM sales_order WHERE sales_order.`customer_id`=`c1`.`entity_id` AND coupon_code IS NULL AND discount_amount=0 HAVING sum(subtotal)>5.95) AS last_non_exchange_order_date
,(SELECT COUNT(0) FROM `sales_order` so WHERE `so`.`customer_id` = `c1`.`entity_id`) AS 'orders_count'
,(SELECT COUNT(0) FROM sales_order_item
WHERE product_type='configurable'
AND glasses_prescription_id IN (SELECT entity_id FROM glasses_prescription WHERE ladd !=0.00 OR radd !=0.00)
AND order_id IN(SELECT entity_id FROM sales_order WHERE sales_order.customer_id=c1.entity_id)) AS progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND profile_prescription_id IN (SELECT entity_id FROM prescription_entity WHERE ladd !=0.00 OR radd !=0.00)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_progressive_count
,(SELECT COUNT(0) FROM quote_item
WHERE product_type='configurable'
AND quote_id IN(SELECT entity_id FROM `quote` WHERE is_active=1)
AND quote_id IN(SELECT entity_id FROM `quote` WHERE quote.customer_id=c1.entity_id)) AS quote_count
,(SELECT MAX(created_at) AS created_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_created_at
,(SELECT MAX(updated_at) AS updated_at FROM `quote` WHERE is_active=1 AND quote.customer_id=c1.entity_id ORDER BY created_at DESC LIMIT 1) AS quote_updated_at
,(SELECT COUNT(0) AS cnt FROM `sales_order` WHERE customer_id=c1.entity_id AND store_id=1 AND is_virtual=0 AND (state='processing' OR state='closed') AND created_at >= DATE_SUB(NOW(), INTERVAL 21 DAY)) AS three_weeks_purchase
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='simple') as frame_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND product_type='configurable') as glasses_avg_price
,(SELECT ROUND(SUM(soi.row_total)/SUM(qty_ordered),2) FROM sales_order_item soi
INNER JOIN sales_order so ON so.`entity_id`=soi.`order_id`
INNER JOIN customer_entity c ON c.`entity_id`=so.`customer_id`
WHERE c.`entity_id`=c1.entity_id
AND soi.is_virtual=1) as craft_avg_price";
        $order_sql="ORDER BY `c1`.`entity_id` ASC";
        if ($is_count) {
            $sql = sprintf($count_sql, $this->start_entity_id, "");
        } else {
            $sql = sprintf($sql, $select_fields, $this->start_entity_id, $order_sql);
        }
        return $sql;
    }

    /**
     * 计算同步的总数量
     */
    private function get_magento_member_count(){
        $count_sql=$this->get_magento_member_list_sql(true);
        $cnt=$this->getDb()->createCommand($count_sql)->queryScalar();
        $total_page = ceil($cnt / $this->page_size);
        $this->total_count = (int)$cnt;
        $log=sprintf("total count=%s,total page=%s,page size=%s",$this->total_count,$total_page,$this->page_size);
        $this->savelog($log);
        return $total_page;
    }

    private function get_magento_member_list() {
        $sql = $this->get_magento_member_list_sql( False);
        $sql = sprintf("%s limit %s,%s",$sql, 0, $this->page_size);
        $data=$this->getDb()->createCommand($sql)->queryAll();
        return $data;
    }

    private function toTime($d) {
        if (!empty($d)) {
            $pos=strrpos($d,"-");
            $fetch=substr($d,$pos,1);
            $fetch_v=(int)explode(":",substr($d,$pos+1,strlen($d)-$pos-1))[0];
            return date("Y-m-d H:i:s",strtotime(sprintf("%s%s hours",$fetch,$fetch_v),strtotime($d)));
        } else {
            return "";
        }
    }

    private function compareDateEqual($d1,$d2) {
        if ((empty($d1) || ($d1=="0000-00-00 00:00:00"))
            || (empty($d2) || ($d2=="0000-00-00 00:00:00"))
        ) {
            return true;
        }
        if (empty($d1)) {
            $d1="0000-00-00 00:00:00";
        }
        if (empty($d2)) {
            $d2="0000-00-00 00:00:00";
        }
        $d1=$this->toTime($d1);
        if (empty($d1)) {
            $d1="0000-00-00 00:00:00";
        }
        if (empty($d2)) {
            $d2="0000-00-00 00:00:00";
        }
        if ($d1==$d2) {
            return true;
        } else {
            return false;
        }
    }

    public function authcode_encode($data) {
        return base64_encode(AuthcodeHelper::encode($data,$this->authcode_secret));
    }

    public function authcode_decode($data) {
        return AuthcodeHelper::decode_ex($data);
    }

    private function sync_by_list($list,$file_name) {
        foreach ($list as $member) {
            $t1=msectime();
            $this->c=$this->c+1;
            $this->start_entity_id = $member["entity_id"];
            $log = sprintf("total %s records,now is process the %s record...", $this->total_count,$this->c);
            $this->savelog($log);

            $customer_id=$member["entity_id"];
            $customer_id_auth_code=$this->authcode_encode($customer_id);
            $reg_date=$member["reg_date"];
            $firstname = $member["firstname"];
            $lastname=$member["lastname"];
            $gender=$member["gender"];
            $genderstr="";
            if ($member["gender"]==null) {
                $gender="3";
                $genderstr = "Not Specified";
            } else if ($member["gender"]=="1") {
                $genderstr="Male";
            } else if ($member["gender"]=="2") {
                $genderstr="Female";
            } else if ($member["gender"]=="3") {
                $genderstr="Not Specified";
            } else {
                $gender = "3";
                $genderstr = "Not Specified";
            }
            $subscriber_email = strtolower($member["email"]);
            $email_auth_code=$this->authcode_encode($subscriber_email);
            $first_purchase_date = $member["first_purchase_date"];
            $last_purchase_date = $member["last_purchase_date"];
            $last_non_exchange_order_date = $member["last_non_exchange_order_date"];
            $orders_count = $member["orders_count"];
            $progressive_count = $member["progressive_count"];
            $quote_progressive_count = $member["quote_progressive_count"];
            $quote_count=$member["quote_count"];
            $quote_created_at=$member["quote_created_at"];
            $quote_updated_at = $member["quote_updated_at"];
            $frame_avg_price = $member["frame_avg_price"];
            $glasses_avg_price = $member["glasses_avg_price"];
            $craft_avg_price = $member["craft_avg_price"];
            $log=sprintf("reg_date=%s,firstname=%s,lastname=%s,gender=%s,subscriber_email=%s,customer_id=%s,first_purchase_date=%s,last_purchase_date=%s,orders_count=%s,progressive_count=%s,genderstr=%s,quote_progressive_count=%s,quote_count=%s,quote_created_at=%s,quote_updated_at=%s",
                $reg_date,$firstname,$lastname,$gender,$subscriber_email,$customer_id,$first_purchase_date,$last_purchase_date,$orders_count,$progressive_count,$genderstr,$quote_progressive_count,$quote_count,$quote_created_at,$quote_updated_at);
            $this->savelog($log);

            $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
            if (
                (empty($firstname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $firstname, $match))
                && (empty($lastname) || !preg_match("/([\x81-\xfe][\x40-\xfe])/", $lastname, $match))
                && (!$this->helper->isFakeMail($subscriber_email))
            ) {
                $can_synd=true;
            } else {
                $can_synd=false;
            }
            //对比是否需要更新
//            if ($contract->isSuccess) {
//                $contractData=$contract->getData();
//            }
            if ($can_synd) {
                $customField=[
                    "craft_avg_price"=>round($craft_avg_price,2),
                    "customer_id"=>$customer_id,
                    "first_purchase_date"=>$first_purchase_date,
                    "frame_avg_price"=>$frame_avg_price,
                    "gender_str"=>$genderstr,
                    "glasses_avg_price"=>round($glasses_avg_price,2),
                    "last_purchase_date"=>$last_purchase_date,
                    "order_count"=>$orders_count,
                    "progressive_count"=>$progressive_count,
                    "quote_count"=>$quote_count,
                    "quote_progressive_count"=>$quote_progressive_count,
                    "quote_created_at"=>$quote_created_at,
                    "quote_updated_at"=>$quote_updated_at,
                    "register_date"=>$reg_date,
                    "customer_id_auth_code"=>$customer_id_auth_code,
                    "email_auth_code"=>$email_auth_code,
                    "email"=>$subscriber_email,
                    "first_name"=>$firstname,
                    "last_name"=>$lastname
                ];
                $t2=msectime();
                echo sprintf("t2-t1=%s".PHP_EOL,($t2-$t1));
                foreach ($customField as $key => $value){
                    file_put_contents('/data/magento/cache/'.$file_name,$value.',',FILE_APPEND);
                }
                file_put_contents('/data/magento/cache/'.$file_name,"\n",FILE_APPEND);
                $t3=msectime();
                echo sprintf("t3-t2=%s".PHP_EOL,($t3-$t2));
            }
        }
        $this->upload_csv($file_name);
    }

    private function sync_csv() {
        $field="email\tfirstname\tlast_name\tphone\tstatus\tcraft_avg_price\tcustomer_id\tfirst_purchase_date\tframe_avg_price\t\gender_str";
        $field=$field."\tglasses_avg_price\tlast_purchase_date\torder_count\tprogressive_count\tquote_count\tquote_progressive_count\tquote_created_at\tquote_updated_at\tregister_date\t";
        file_put_contents("/lihf/maropost.csv",$field.PHP_EOL);
    }

    /**
     * 同步
     */
    private function sync() {
        $total_page=$this->get_magento_member_count();
        for ($page_no=0; $page_no<$total_page;$page_no++) {
            $filename = "customer_entity_maropost_".$page_no.'.csv';
            $str_content="craft_avg_price,customer_id,first_purchase_date,frame_avg_price,gender_str,glasses_avg_price,last_purchase_date,order_count,progressive_count,quote_count,quote_progressive_count,quote_created_at,quote_updated_at,register_date,customer_id_auth_code,email_auth_code,email,first_name,last_name\n";
            file_put_contents('/data/magento/cache/'.$filename,$str_content);
            $list=$this->get_magento_member_list();
            $this->sync_by_list($list,$filename);
        }
    }

    public function actionSync($log_file="/lihf/maropost.log",$divisor="5",$remainder="0") {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        if ($log_file!="") {
            $this->log_file=$log_file;
        }
        if ($divisor!="") {
            $this->divisor=$divisor;
        }
        if ($remainder!="") {
            $this->remainder=$remainder;
        }
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $this->helper=$objectManager->get('Plumrocket\SocialLoginFree\Helper\Data');

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();
        $this->maropost_api=new Contacts(static::ACCOUNT_ID, static::AUTHTOKEN);
        $this->savelog("start sync ...",true);

        $this->sync();

        return ExitCode::OK;
    }

    public function upload_csv($file) {
        $magento_autoload=Yii::$container->get("app\components\magento\MagentoAutoload");
        $magento_autoload->loadMagentoFrameword();

        $marohelper=Yii::$container->get("app\components\helper\MaropostHelper");
        $marohelper->loadClass();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cfg = $objectManager->get('Magento\Framework\App\DeploymentConfig\Reader')->load("app_env")["authcode"];
        $this->authcode_secret=$cfg["secret_key"];

        $srcFile = "/data/magento/cache/".$file;
        $dstFile = "/uploads/list_export_15.csv";

        $host = 'ftp.maropost.com';
        $port = '8410';
        $username = 'ftpuser_2160';
        $password = 'e868707e0859e158f779';

        $conn = ssh2_connect($host, $port);
        ssh2_auth_password($conn, $username, $password);
        $sftp = ssh2_sftp($conn);

        $sftpStream = fopen('ssh2.sftp://'.$sftp.$dstFile, 'w');
        try {
            if (!$sftpStream) {
                throw new Exception("Could not open remote file: $dstFile");
            }
            $data_to_send = file_get_contents($srcFile);
            if ($data_to_send === false) {
                throw new Exception("Could not open local file: $srcFile.");
            }
            if (fwrite($sftpStream, $data_to_send) === false) {
                throw new Exception("Could not send data from file: $srcFile.");
            }
            fclose($sftpStream);
        } catch (Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            fclose($sftpStream);
        }

        $url=sprintf("https://api.maropost.com/accounts/2160/data_journeys/6/trigger/57407792072717.json?auth_token=TvfWeM5OeJlAa3nbtYA7cxa4Zeh7YbpUQUtI8zhzaNRm4wQMRRmYfQ");
        $client = new GuzzleClient();
        $res = $client->request('POST', $url);
        $err_no=$res->getBody()->getContents();
        echo $file."ftp传完".PHP_EOL;
    }
}
