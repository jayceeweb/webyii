<?php
namespace app\commands;

use app\components\helper\ESHelper;
use app\models\CoreConfigData;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\components\helper\MagentoProductHelper;

class CollectionController extends Controller
{
    private function getES(){
        $esClient=ESHelper::getInstance()->getES();
        return $esClient;
    }

    public function actionIndex() {
        $client=$this->getES();
        $params = [
            'index' => 'pg_collection',
            'body' => [
                "mappings"=>[
                    'properties' => [
                        'event' => [
                            'type' => 'keyword'
                        ],
                        'product' => [
                            "properties"=>[
                                'type' => [
                                    'type' => 'keyword'
                                ],
                                'cid' => [
                                    'type' => 'keyword'
                                ],
                                'attribute_set_id' => [
                                    'type' => 'integer'
                                ],
                                'stock_sku' => [
                                    'type' => 'keyword'
                                ],
                                'name' => [
                                    'type' => 'text'
                                ],
                                'category_id' => [
                                    'type' => 'integer'
                                ],
                                'catetory' => [
                                    'type' => 'text'
                                ],
                                'parent_catetory' => [
                                    'type' => 'text'
                                ],
                                'price' => [
                                    'type' => 'float'
                                ],
                                'sales_price' => [
                                    'type' => 'float'
                                ],
                                'brand' => [
                                    'type' => 'keyword'
                                ],
                                'lens_width_value' => [
                                    'type' => 'integer'
                                ],
                                'bridge_value' => [
                                    'type' => 'integer'
                                ],
                                'temple_length_value' => [
                                    'type' => 'integer'
                                ],
                                'frame_size_value' => [
                                    'type' => 'keyword'
                                ],
                                'frame_width_value' => [
                                    'type' => 'integer'
                                ],
                                'lens_height_value' => [
                                    'type' => 'integer'
                                ],
                                'frame_weight_value' => [
                                    'type' => 'integer'
                                ],
                                'frame_shape_value' => [
                                    'type' => 'keyword'
                                ],
                                'material_value' => [
                                    'type' => 'keyword'
                                ],
                                'min_pd' => [
                                    'type' => 'integer'
                                ],
                                'max_pd' => [
                                    'type' => 'integer'
                                ],
                                'rim_type_value' => [
                                    'type' => 'keyword'
                                ],
                                'frame_type_value' => [
                                    'type' => 'keyword'
                                ],
                                'support_progressive' => [
                                    'type' => 'boolean'
                                ],
                                'nose_bridge_value' => [
                                    'type' => 'keyword'
                                ],
                                'created_at' => [
                                    "type"=>"date",
                                    "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                                ],
                                'has_spring_hinges' => [
                                    'type' => 'boolean'
                                ],
                                'nose_pad' => [
                                    'type' => 'boolean'
                                ],
                                'support_rx' => [
                                    'type' => 'boolean'
                                ],
                                'color_changing' => [
                                    'type' => 'boolean'
                                ],
                                'low_bridge_fit' => [
                                    'type' => 'boolean'
                                ],
                                'is_goggles' => [
                                    'type' => 'keyword'
                                ],
                                'try_on' => [
                                    'type' => 'boolean'
                                ],
                                'color_value' => [
                                    'type' => 'keyword'
                                ]
                            ]
                        ],
                        'customer' => [
                            "properties"=>[
                                'id' => [
                                    'type' => 'keyword'
                                ],
                                'uid' => [
                                    'type' => 'keyword'
                                ],
                                'name' => [
                                    'type' => 'text'
                                ],
                                'group_id' => [
                                    'type' => 'keyword'
                                ],
                                'app_version' => [
                                    'type' => 'keyword'
                                ],
                                'cookie_enabled' => [
                                    'type' => 'keyword'
                                ],
                                'language' => [
                                    'type' => 'keyword'
                                ],
                                'platform' => [
                                    'type' => 'keyword'
                                ],
                                'screen_width' => [
                                    'type' => 'integer'
                                ],
                                'screen_height' => [
                                    'type' => 'integer'
                                ],
                            ]
                        ],
                        'base' => [
                            "properties"=>[
                                'time' => [
                                    "type"=>"date",
                                    "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                                ],
                                'deal_time' => [
                                    "type"=>"date",
                                    "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                                ],
                                'action' => [
                                    'type' => 'keyword'
                                ]
                            ]
                        ],
                        'tags1' => [
                            "properties"=>[
                                'tag' => [
                                    'type' => 'keyword'
                                ]
                            ]
                        ],
                        'tags2' => [
                            "properties"=>[
                                'tag' => [
                                    'type' => 'text'
                                ]
                            ]
                        ],
                    ]
                ]
            ]
        ];
        $response=$client->indices()->exists(['index' => 'pg_collection']);
        if ($response) {
            $response = $client->indices()->delete(['index' => 'pg_collection']);
        }
        $response = $client->indices()->create($params);
        print_r($response);
    }

}
