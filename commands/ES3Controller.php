<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class ES3Controller extends Controller
{
    private function getES(){
        $hosts = [
            '127.0.0.1:9200'
        ];
        $client = \Elasticsearch\ClientBuilder::create()
            ->setRetries(2)
            ->setHosts($hosts)
            ->build();
        return $client;
    }

    public function actionIndex() {
        $client=$this->getES();
        $params = [
            'index' => 'payneglasses',
            'body' => [
                "mappings"=>[
                    'properties' => [
                        'configurable' => [
                            'type' => 'long'
                        ],
                        'attribute_set_id' => [
                            'type' => 'integer'
                        ],
                        'type_id' => [
                            'type' => 'keyword'
                        ],
                        'sku' => [
                            'type' => 'keyword'
                        ],
                        'category_id' => [
                            'type' => 'integer'
                        ],
                        'cat_index_position' => [
                            'type' => 'integer'
                        ],
                        'name' => [
                            'type' => 'text'
                        ],
                        'short_description' => [
                            'type' => 'text'
                        ],
                        'special_price' => [
                            'type' => 'float'
                        ],
                        'image' => [
                            'type' => 'keyword'
                        ],
                        'small_image' => [
                            'type' => 'keyword'
                        ],
                        'thumbnail' => [
                            'type' => 'keyword'
                        ],
                        'bridge' => [
                            'type' => 'integer'
                        ],
                        'bridge_value' => [
                            'type' => 'integer'
                        ],
                        'temple_length_value' => [
                            'type' => 'integer'
                        ],
                        'rim_type_value' => [
                            'type' => 'keyword'
                        ],
                        'frame_shape_value' => [
                            'type' => 'keyword'
                        ],
                        'frame_type_value' => [
                            'type' => 'keyword'
                        ],
                        'min_pd' => [
                            'type' => 'integer'
                        ],
                        'max_pd' => [
                            'type' => 'integer'
                        ],
                        'min_sph_value' => [
                            'type' => 'integer'
                        ],
                        'max_sph_value' => [
                            'type' => 'integer'
                        ],
                        'frame_weight_value' => [
                            'type' => 'integer'
                        ],
                        'support_progressive_value' => [
                            'type' => 'keyword'
                        ],
                        'lens_width_value' => [
                            'type' => 'integer'
                        ],
                        'lens_height_value' => [
                            'type' => 'integer'
                        ],
                        'frame_size_value' => [
                            'type' => 'keyword'
                        ],
                        'simeple_material_value' => [
                            'type' => 'keyword'
                        ],
                        'material_value' => [
                            'type' => 'keyword'
                        ],
                        'frame_width_value' => [
                            'type' => 'integer'
                        ],
                        'other_colors' => [
                            'type' => 'text'
                        ],
                        'age_range_value' => [
                            'type' => 'keyword'
                        ],
                        'nose_bridge_value' => [
                            'type' => 'keyword'
                        ],
                        'created_at' => [
                            "type"=>"date",
                            "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                        ],
                        'has_spring_hinges' => [
                            'type' => 'keyword'
                        ],
                        'nose_pad' => [
                            'type' => 'keyword'
                        ],
                        'is_clip_on' => [
                            'type' => 'keyword'
                        ],
                        'support_rx' => [
                            'type' => 'keyword'
                        ],
                        'color_changing' => [
                            'type' => 'keyword'
                        ],
                        'low_bridge_fit' => [
                            'type' => 'keyword'
                        ],
                        'recommend_value' => [
                            'type' => 'keyword'
                        ],
                        'create_time' => [
                            "type"=>"date",
                            "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                        ],
                        'price' => [
                            'type' => 'float'
                        ],
                        'final_price' => [
                            'type' => 'float'
                        ],
                        'is_goggles' => [
                            'type' => 'keyword'
                        ],
                        'row_number' => [
                            'type' => 'integer'
                        ],
                        'url' => [
                            'type' => 'keyword'
                        ],
                        'product_type' => [
                            'type' => 'keyword'
                        ],
                        'product_type' => [
                            'type' => 'keyword'
                        ],
                        'is_new' => [
                            'type' => 'boolean'
                        ],
                        "simples"=>[
                            "properties"=>[
                                'entity_id' => [
                                    'type' => 'long'
                                ],
                                'sku' => [
                                    'type' => 'keyword'
                                ],
                                'parent_id' => [
                                    'type' => 'long'
                                ],
                                'type_id' => [
                                    'type' => 'keyword'
                                ],
                                'color_value' => [
                                    'type' => 'keyword'
                                ],
                                'color_value_background' => [
                                    'type' => 'keyword'
                                ],
                                'name' => [
                                    'type' => 'text'
                                ],
                                'price' => [
                                    'type' => 'float'
                                ],
                                'special_price' => [
                                    'type' => 'float'
                                ],
                                'short_description' => [
                                    'type' => 'text'
                                ],
                                'image' => [
                                    'type' => 'keyword'
                                ],
                                'stock_sku' => [
                                    'type' => 'keyword'
                                ],
                                'support_php_value' => [
                                    'type' => 'keyword'
                                ],
                                'create_time' => [
                                    "type"=>"date",
                                    "format"=>"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                                ],
                                'is_new' => [
                                    'type' => 'boolean'
                                ],
                                "frame_simple_url"=>[
                                    'type'=>"keyword"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response=$client->indices()->exists(['index' => 'payneglasses']);
        if ($response) {
            $response = $client->indices()->delete(['index' => 'payneglasses']);
        }
        $response = $client->indices()->create($params);
        print_r($response);
    }

    public function actionPutMapping(){
        $client=$this->getES();
        $params = [
            'index' => 'payneglasses',
//            'type' => 'my_type2',
            'body' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'text'
                    ],
                    'age' => [
                        'type' => 'integer'
                    ],
                    'test'=>[
                        'type'=>'text'
                    ]
                ]
            ]
        ];
        $client->indices()->putMapping($params);
    }

    public function actionUpdateDoc(){
        $client=$this->getES();
        $params = [
            'index' => 'payneglasses',
            'id' => '4188',
            'type'=>'_doc',
            'body' => [
                'doc' => [
                    'name' => 'abc'
                ]
            ]
        ];

        $response = $client->update($params);

        $params = [
            'index' => 'payneglasses',
            'id' => '4188',
            'type'=>'_doc',
            'body' => [
                'script' => 'ctx._source.attribute_set_id += 1'
            ]
        ];
        $response = $client->update($params);
    }

    public function actionPutIndex(){
        $json_file="/data/magento446/pub/plp/plp_6_0_position_asc.json";
        $json_list=json_decode(file_get_contents($json_file),true);
        $client=$this->getES();
        $i=0;
        foreach ($json_list as $configurable) {
            $i=$i+1;
            $params = [
                'index' => 'payneglasses',
                'type' => '_doc',
                'id' => $configurable["entity_id"],
                'body'=>[
                    "configurable_id" => $configurable["entity_id"],
                    "attribute_set_id" => $configurable["attribute_set_id"],
                    "type_id" => $configurable["type_id"],
                    "sku" => $configurable["sku"],
                    "category_id" => $configurable["category_id"],
                    "cat_index_position" => $configurable["cat_index_position"],
                    "name" => $configurable["name"],
                    "short_description" => $configurable["short_description"],
                    "special_price" => $configurable["special_price"],
                    "image" => $configurable["image"],
                    "small_image" => $configurable["small_image"],
                    "thumbnail" => $configurable["thumbnail"],
                    "bridge_value" => $configurable["bridge_value"],
                    "temple_length_value" => $configurable["temple_length_value"],
                    "rim_type_value" => $configurable["rim_type_value"],
                    "frame_shape_value" => $configurable["frame_shape_value"],
                    "frame_type_value" => $configurable["frame_type_value"],
                    "min_pd" => $configurable["min_pd"],
                    "max_pd" => $configurable["max_pd"],
                    "min_sph_value" => $configurable["min_sph_value"],
                    "frame_weight_value" => $configurable["frame_weight_value"],
                    "support_progressive_value" => $configurable["support_progressive_value"],
                    "lens_width_value" => $configurable["lens_width_value"],
                    "lens_height_value" => $configurable["lens_height_value"],
                    "frame_size_value" => $configurable["frame_size_value"],
                    "simeple_material_value" => $configurable["simeple_material_value"],
                    "material_value" => $configurable["material_value"],
                    "frame_width_value" => $configurable["frame_width_value"],
                    "other_colors" => $configurable["other_colors"],
                    "age_range_value" => $configurable["age_range_value"],
                    "nose_bridge_value" => $configurable["nose_bridge_value"],
                    "color_details" => $configurable["color_details"],
                    "created_at" => $configurable["created_at"],
                    "has_spring_hinges" => $configurable["has_spring_hinges"],
                    "nose_pad" => $configurable["nose_pad"],
                    "is_clip_on" => $configurable["is_clip_on"],
                    "support_rx" => $configurable["support_rx"],
                    "color_changing" => $configurable["color_changing"],
                    "low_bridge_fit" => $configurable["low_bridge_fit"],
                    "recommend_value" => $configurable["recommend_value"],
                    "create_time" => $configurable["create_time"],
                    "price" => $configurable["price"],
                    "final_price" => $configurable["final_price"],
                    "is_goggles" => $configurable["is_goggles"],
                    "row_number" => $configurable["row_number"],
                    "final_price" => $configurable["final_price"],
                    "product_type" => $configurable["product_type"],
                    "is_new" => empty($configurable["is_new"])? false:true,
                ]
            ];
            foreach ($configurable["simples"] as $simple) {
                $params["body"]["simples"][]=[
                    'simple_id'=>$simple["entity_id"],
                    'sku'=>$simple["sku"],
                    'parent_id'=>$simple["parent_id"],
                    'type_id'=>$simple["type_id"],
                    'color_value'=>$simple["color_value"],
                    'name'=>$simple["name"],
                    'price'=>$simple["price"],
                    'special_price'=>$simple["special_price"],
                    'short_description'=>$simple["short_description"],
                    'image'=>$simple["image"],
                    'stock_sku'=>$simple["stock_sku"],
                    'support_php_value'=>strtolower($simple["support_php_value"])==strtolower("Yes")? true:false,
                    'create_time'=>$simple["create_time"],
                    'color_value'=>$simple["color_val"]["color_value"],
                    'is_new'=>empty($configurable["is_new"])? false:true,
                    'frame_simple_url'=>$simple["frame_simple_url"]
                ];
            }
            $t1=\app\components\helper\TimezoneHelper::msectime();
            $response = $client->index($params);
            $t2=\app\components\helper\TimezoneHelper::msectime();
            $log=sprintf("count=%s,index=%s,configurable_id=%s,t2-t1=%s".PHP_EOL,count($json_list),$i,$configurable["entity_id"],$t2-$t1);
            echo $log;
        }
    }

    public function actionSearch1(){
        $client=$this->getES();
        $params = [
            'index' => 'payneglasses',
            'type' => '_doc',
            'body' => [
//                "from"=>1,
//                "size"=>2,
                "query"=> [
                    "match" => [
                        "simples.price"=>"5.95"
                    ]
                ],
                "sort"=>[
                    [
                        "configurable_id"=>["order"=>"asc"]
                    ]
                ]
            ]
        ];
        $results = $client->search($params);
        foreach ($results["hits"]["hits"] as $result) {
            echo $result["_id"].PHP_EOL;
            print_r($result["_source"]["simples"]);
            echo PHP_EOL;
        }
    }

    public function actionSearch2(){
        $client=$this->getES();
        $params = [
            'index' => 'payneglasses',
            'type' => '_doc',
            'body' => [
                "from"=>0,
                "size"=>200,
                "query"=> [
                    "bool" => [
                        "must"=>[
                            "match"=>["simples.color_value"=>"#000000"]
                        ],
                        "filter"=>[
                            "match"=>["simples.name"=>"Oval"],
                        ]
                    ]
                ],
                "sort"=>[
                    [
                        "configurable_id"=>["order"=>"asc"]
                    ]
                ]
            ]
        ];
        //"term"=>["simples.color_value"=>"#deab03"],
        $results = $client->search($params);
        print_r($results["hits"]["total"]);
        echo PHP_EOL;
        foreach ($results["hits"]["hits"] as $result) {
            echo $result["_id"].PHP_EOL;
//            print_r($result["_source"]["simples"]);
//            echo PHP_EOL;
        }
    }

    public function actionT1() {
        $hosts = [
            '127.0.0.1:9200'
        ];
        $client = \Elasticsearch\ClientBuilder::create()
            ->setRetries(2)
            ->setHosts($hosts)
            ->build();

        $params = [
            'index' => 'c',
            'body' => [
                'query' => [
                    'ids' => [
                        'values'=>[1,4,100]
                    ]
                ],
                "sort"=>[
                    "_id"=>"desc"
                ]
            ]
        ];

        $d=$client->search($params);
        print_r($d);
    }
}
